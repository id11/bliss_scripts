
"""
 Compute the true Bragg angles from the various motor positions
 for a single crystal bender and rotation axis.

https://docs.google.com/presentation/d/1lYgPLldDJUbVW9DIwxCdsFKOheoN2SNToVjCANLmb_g/edit?usp=sharing

 Input motor positions are:
  Axis 1:  llrz1 / llbend1 / llty1
  Axis 2:  llrz2 / llbend2 / llty2

 Model is:
   llrz1 / llrz2 = linear pusher (radius + offset)
   llbend1 / llbend1 = linear pushed on end of crystal bending it
   llty1 / llty1 = translation of curved crystal across the beam

   clamp --------------------------------------------
                   \..........                      |
                              \.......              | bend
                                      \......       |
                 ------->    y0              \......V
 
  Angle offset is proportional to (bend x y0 = distance_to_clamp)

  Angle is nominally a linear pusher. Set to make roughly degrees.

       |
       |       b
       +f--------------xp    xp = fixed pivot under axis
     c |                     +f = moving flexor
       |        a            xm = fixed motor / lvdt
       xm

 Cosine Rule : angle is A for +f - xp - xm
  c*c = a*a + b*b - 2*a*b*cos(C)

  c = measured LVDT position, roughly degrees.
  b = deg2rad ~ 57
  a = sqrt( 3*3 + b*b )   # if perpendicular at 3 degrees
  angle = arccos( (a*a + b*b - c*c)/(2*a*b)
   effect is about 0.003 offset with 0.002 error over range.

                          | lvdt rz1
                          V
        |        z1-------    
        V       /
        -----z2
    
  rz1 axis: LVDT becomes shorter : angle is higher. steps/unit are +
  rz2 axis: LVDT becomes shorter : angle is lower. steps/unit are -


  Constants to be fitted or saved:
   - zero position of the bender motor 
      - where it starts to push on the crystal (zero)
      - where we think the crystal is flat
   - effective distance of bender compared to clamped end of crystal
   - effective zero of the clamped end of the crystal
   - zero position of the rz pusher (planes parallel to beam)
   - position where rz pusher is at 90 degrees to axis (about mid-range)

  Number being computed:
   - Bragg angle in degrees
"""

import numpy as np

try:
    from bliss.controllers.motor import CalcController
    from bliss.common.logtools import log_info
    run_test = False
except:
    run_test = True
    class CalcController(object):
        pass
    def log_info(self,s):
        print(s)


def mono_cosine_rule( x, x0, radius=np.degrees(1) ):
    # angle = arccos( (a*a + b*b - c*c)/(2*a*b)
    c = x
    a = radius
    b = np.sqrt( a*a + x0*x0 )
    cosC = np.clip( (a*a + b*b - c*c)/(2*a*b), -1, 1)
    
    return np.sign(x)*np.degrees( np.arccos( cosC ) )

def inverse_mono_cosine_rule( theta, x0, radius=np.degrees(1) ):
    # c*c = a*a + b*b - 2*a*b*cos(C)
    cosC = np.cos( np.radians( theta ) )
    a = radius
    b = np.sqrt( a*a + x0*x0 )
    c = np.sqrt( abs(a*a + b*b - 2*a*b*cosC ))
    return np.sign(theta)*c

        
class LLAngle(CalcController):
    
    def calc_from_real( self, positions_dict ):
        log_info(self, "LLAngle calc_from_real()")
        log_info(self, "LLAngle real: %s"%(positions_dict))
        # Angle due to pusher not being a rotation
        rz  = positions_dict["rz"] 
        # Effect of bending
        bend = positions_dict[ "bend" ]
        # Effect of translation
        ty  = positions_dict["ty"]      # llty1 / llty2
        # Constants:
        rzp = self.config["rz_normalangle"]   # about 3 degrees 
        rz0 = self.config["rz_zero"]          # determines sign
        bzero  = self.config["bend_zero"]
        yb  = self.config["bend_y"]
        y0  = self.config["ty_zero"]
        #
        # Case of rz1: rz0 ~ 0
        #         rz2: rz0 ~ 6
        xangle = rz0 - mono_cosine_rule( rz0 - rz, rzp )
        bend_offset = np.degrees( ( bend - bzero ) * ( ty - y0 ) / yb )
        # only for bent crystal and mono in beam
        valid = (bend > bzero) & (abs(ty-y0) < 50.)
        angle = np.where( valid, xangle + bend_offset, xangle )
        calc_dict = { "angle"  : angle , # computed
                      "bend" : bend,     # pass through
                      "ty" : ty,         # pass through
        }
        log_info(self, "LLangle %s"%(angle))
        return calc_dict
                      
    def calc_to_real(self, positions_dict ):
        log_info(self, "LLBragg calc_to_real()")
        log_info(self, "LLBragg calc: %s"%(positions_dict))
        #
        angle = positions_dict["angle"]
        # Effect of bending
        bend = positions_dict[ "bend" ]
        # Effect of translation
        ty  = positions_dict["ty"]      # llty1 / llty2
        # Assume we go to the destination ty / bend.
        # Compute the effect for the angle only
        calc_dict = { "bend" : bend,
                      "ty"   : ty }
        rzp = self.config["rz_normalangle"]  # about 3 degrees 
        rz0 = self.config["rz_zero"]         # about zero
        bzero  = self.config["bend_zero"]
        yb  = self.config["bend_y"]
        y0  = self.config["ty_zero"]

        bend_offset = np.degrees( ( bend - bzero ) * ( ty - y0 ) / yb )
        # only for bent crystal and mono in beam
        valid = (bend > bzero) & (abs(ty-y0) < 50.)
        # - versus + above:
        xangle = np.where( valid, angle - bend_offset, angle )
        # reverse the cosine correction:
        rz = rz0 - inverse_mono_cosine_rule( rz0 - xangle, rzp )
        calc_dict["rz"] = rz
        log_info(self,"LLrz: %s"%(str(rz)))
        return calc_dict
        
"""
# ID11 LaueLaue Monochromator

LL_BEND1_FLAT = -0.90
LL_BEND1_BENT = 1.525 #
LL_BEND2_FLAT = 1.000
LL_BEND2_BENT = 3.000 
#                   bent    -   flat
LL_BEND1_ANGDIFF = 3.893346 - 3.8324  # at Sn edge
LL_BEND2_ANGDIFF = 2.58085  - 2.6370 # at Nd edge
"""
        
def test_ll_angle_rz1(plot=True):
    if plot : import pylab as pl
    cfg = {
        'bend_zero' : 0.0,
        'bend_y' : 48300.,  # to be fitted
        'ty_zero' : 30.,
        'rz_normalangle' : 3.5,
        'rz_zero' : 7.0
        }
    rz1 = np.linspace(0.1,6,10)
    o = LLAngle() # fails within bliss env for now
    o.config = cfg
    for bend in [ 0.1, 1.525]:
        for ty in [0,2]: # ,1:
            pd = { 'rz' : rz1,
                   'bend' : bend,
                   'ty' : ty }
            r = o.calc_from_real( pd )
            if plot:
                pl.plot( pd['rz'], pd['rz']-r['angle'],"-",
                     label="bend %f ty %f"%(bend,ty))
            # check reverse is also matching
            t = o.calc_to_real(r)
            for k in t.keys():
                assert np.allclose( t[k], pd[k] ), k
    if plot:
        pl.legend()
        pl.show()
    return "OK"

def test_ll_angle_rz2(plot=True):
    if plot : import pylab as pl
    cfg = {
        'bend_zero' : 1.0,
        'bend_y' : 48300.,  # to be fitted
        'ty_zero' : 30.,    # depends on tx1 also
        'rz_normalangle' : 3.5,
        'rz_zero' : 0.0
        }
    rz1 = np.linspace(0.1,6,10)
    o = LLAngle() # fails within bliss env for now
    o.config = cfg
    for bend in [ 1.,1.1, 3.]:
        for ty in [14,10]: # ,1:
            pd = { 'rz' : rz1,
                   'bend' : bend,
                   'ty' : ty }
            r = o.calc_from_real( pd )
            if plot:
                pl.plot( pd['rz'], pd['rz']-r['angle'],"-",
                     label="bend %f ty %f"%(bend,ty))
            # check reverse is also matching
            t = o.calc_to_real(r)
#            for k in t.keys():
#                assert np.allclose( t[k], pd[k] ), k
    if plot:
        pl.legend()
        pl.show()
    return "OK"

def diff_rz1_rz2( plot=True ):
    if plot : import pylab as pl
    o1 = LLAngle()
    o1.config = {
        'bend_zero' : 1.0,
        'bend_y' : 48300.,  # to be fitted
        'ty_zero' : 30.,    # depends on tx1 also
        'rz_normalangle' : 3.,
        'rz_zero' : 7.0
        }
    o2 = LLAngle()
    o2.config = {
        'bend_zero' : 0.0,
        'bend_y' : 48300.,  # to be fitted
        'ty_zero' : 30.,    # depends on tx1 also
        'rz_normalangle' : 3.5,
        'rz_zero' : 0.0
        }
    rz = np.linspace(0.1,6,100)
    th1 = o1.calc_from_real( {'rz':rz, 'bend':2.0, 'ty': 14 } )['angle']
    th2 = o2.calc_from_real( {'rz':rz, 'bend':1.0, 'ty': 0. } )['angle']
    pl.plot(rz, th1-th2,"-", label="Theory")
    obsz1  = (2.54534, 1.66602, 1.57011)
    obsz2  = (2.67672, 1.81691, 1.71518)
    pl.plot( obsz1,[z1-z2 for z1,z2 in zip(obsz1, obsz2)], "o", label="wiki")
    pl.show()

    


demos = [test_ll_angle_rz1,
         test_ll_angle_rz2,
         diff_rz1_rz2,
]
    
if __name__=="__main__" and run_test:
    for func in demos:
        func()

        
    
