

import time
import numpy as np
#from bliss.config.settings import Parameters
from bliss.common.cleanup import cleanup, axis as cleanup_axis
from bliss.common import plot

#raytrace_params = Parameters("raytrace",
#                             default_values = {'m1pos' : None,
#                                               'm2pos' : None})
#raytrace_counter = mca.counters.AU_det0

raytrace_plot = None
"""
def raytrace_cleanup '{
 global M1POS M2POS M1 M2
 umv M1 M1POS M2 M2POS
}'

def ncvert '{
global M1POS M2POS M1 M2 MSCAN RAYPOS[]
local shift
M1 = s9vo
M2 = s9vg
MSCAN = "pz"
raytrace $*
# estimate the shift in x needed to correct the aberation:
shift = RAYPOS[2]-RAYPOS[0]
print "Difference = ",shift
print "Divide by beam offset at lens (25 um) = ",shift/25
print "Multiply by distance to lens (shnee+axmo) = ",shift*(A[shnee]+A[axmo])/25
print "Shift by", (A[shnee]+A[axmo]) * pyoffset("s9vo")
}'

def nchoriz '{
global M1POS M2POS M1 M2 MSCAN RAYPOS[]
M1 = s9ho
M2 = s9hg
MSCAN = "py"
if(fabs(A[rot]>10)){
        umv rot 0
}
raytrace $*
# estimate the shift in x needed to correct the aberation:
shift = RAYPOS[2]-RAYPOS[0]
print "Difference = ",shift
print "Divide by beam offset at lens (25 um) = ",shift/25
print "Multiply by distance to lens (shnee) = ",shift*(A[shnee])/25
print "Shift by", A[shnee] * pyoffset("s9ho")
}'
"""

def alvert(rng = 1, count_time=0.1, nbpoints = 40, ctr='eiger:roi_counters:roi3_sum', gap=0.05):
   scans_data, raypos = raytrace( s9vo,s9vg,pz,rng,count_time,nbpoints,ctr=ctr, gap=gap)
   shift = raypos[2] - raypos[0]
   xpos = (-gap/2, 0, gap/2)
   p = np.polyfit( xpos, raypos, 1)
   for i in range(3):
      print('motor: %.6f    peak: %.6f'%(xpos[i],raypos[i]))
   print('Polynomial fit dmotor/dpk, offset:', p)
   # Factor appears to be 0.4 (about 400 mm to slits ?)
   print("Suggested alx shift",p[0]*0.4/2)

def alhoriz_px(rng = 1, count_time=0.1, nbpoints = 40, ctr='eiger:roi_counters:roi3_sum', gap=0.05):
   scans_data, raypos = raytrace( s9ho,s9hg,px,rng,count_time,nbpoints,ctr=ctr, gap=gap)
   shift = raypos[2] - raypos[0]
   xpos = (-gap/2, 0, gap/2)
   p = np.polyfit( xpos, raypos, 1)
   for i in range(3):
      print('motor: %.6f    peak: %.6f'%(xpos[i],raypos[i]))
   print('Polynomial fit dmotor/dpk, offset:', p)
   # Factor appears to be 0.4 (about 400 mm to slits ?)
   print("Suggested alx shift",p[0]*0.4)   
   
def alhoriz_py(rng = 1, count_time=0.1, nbpoints = 40, ctr='eiger:roi_counters:roi3_sum', gap=0.05):
   scans_data, raypos = raytrace( s9ho,s9hg,py,rng,count_time,nbpoints,ctr=ctr, gap=gap)
   shift = raypos[2] - raypos[0]
   xpos = (-gap/2, 0, gap/2)
   p = np.polyfit( xpos, raypos, 1)
   for i in range(3):
      print('motor: %.6f    peak: %.6f'%(xpos[i],raypos[i]))
   print('Polynomial fit dmotor/dpk, offset:', p)
   # Factor appears to be 0.4 (about 400 mm to slits ?)
   print("Suggested alx shift",p[0]*0.4)   


def ncvert(rng = 1, count_time = 1, nbpoints = 40,ctr='mca:Au_det0', mot='pz'):
   scans_data,raypos = raytrace(s9vo,s9vg,config.get(mot),rng,count_time,nbpoints,ctr=ctr)
   # estimate the shift in x needed to correct the aberation:
   shift = raypos[2]-raypos[0]
   print( "Difference = ",shift)
   print( "Divide by beam offset at lens (25 um) = ",shift/25)
   print( "Multiply by distance to lens (shnee+axmo) = ",shift*(shnee.position+axmo.position)/25)
   #print "Shift by", shnee.position() * pyoffset("s9ho")


def nchoriz_px(rng = 1, count_time = 1, nbpoints = 40,ctr='mca:Au_det0'):
   if(abs(rot.position - 90) > 10):
        rot.move(90)
   scans_data,raypos = raytrace(s9ho,s9hg,px,rng,count_time,nbpoints,ctr=ctr)
   # estimate the shift in x needed to correct the aberation:
   shift = raypos[2]-raypos[0]
   print( "Difference = ",shift)
   print( "Divide by beam offset at lens (25 um) = ",shift/25)
   print( "Multiply by distance to lens (shnee) = ",shift*shnee.position/25)
   #print "Shift by", shnee.position() * pyoffset("s9ho")

def nchoriz_py(rng = 1, count_time = 1, nbpoints = 40,ctr='mca:Au_det0'):
   if(abs(rot.position) > 10):
        rot.move(0)
   scans_data,raypos = raytrace(s9ho,s9hg,py,rng,count_time,nbpoints,ctr=ctr)
   # estimate the shift in x needed to correct the aberation:
   shift = raypos[2]-raypos[0]
   print( "Difference = ",shift)
   print( "Divide by beam offset at lens (25 um) = ",shift/25)
   print( "Multiply by distance to lens (shnee) = ",shift*shnee.position/25)
   #print "Shift by", shnee.position() * pyoffset("s9ho")

def nchoriz_dty(rng = 1, count_time = 1, nbpoints = 40,ctr='mca:Au_det0'):
   scans_data,raypos = raytrace(s9ho,s9hg,dty,rng,count_time,nbpoints,ctr=ctr)
   # estimate the shift in x needed to correct the aberation:
   shift = raypos[2]-raypos[0]
   print( "Difference = ",shift)
   print( "Divide by beam offset at lens (25 um) = ",shift/25)
   print( "Multiply by distance to lens (shnee) = ",shift*shnee.position/25)
   #print "Shift by", shnee.position() * pyoffset("s9ho")

def nchoriz_lens(rng = 0.002, count_time = 1, nbpoints = 40,ctr='mca:Au_det0'):
   scans_data,raypos = raytrace_lens(s9ho,s9hg,hly,rng,count_time,nbpoints,ctr=ctr)
   # estimate the shift in x needed to correct the aberation:
   shift = raypos[2]-raypos[0]
   print( "Difference = ",shift)
   print( "Divide by beam offset at lens (25 um) = ",shift/25)
   print( "Multiply by distance to lens (shnee) = ",shift*shnee.position/25)
   #print "Shift by", shnee.position() * pyoffset("s9ho")

def ncvert_lens(rng = 0.002, count_time = 1, nbpoints = 40,ctr='mca:Au_det0'):
   scans_data,raypos = raytrace_lens(s9vo,s9vg,vlz,rng,count_time,nbpoints,ctr=ctr)
   # estimate the shift in x needed to correct the aberation:
   shift = raypos[2]-raypos[0]
   print( "Difference = ",shift)
   print( "Divide by beam offset at lens (25 um) = ",shift/25)
   print( "Multiply by distance to lens (shnee+axmo) = ",shift*(shnee.position+axmo.position)/25)
   #print "Shift by", shnee.position() * pyoffset("s9ho")


def jcen4( xarg, yarg ):
    """4th attempt"""
    clean = np.isfinite(xarg) & np.isfinite(yarg)
    cls = clean.sum()
    if cls < len(xarg) or cls < 2:
        if cls == 0:
            return xarg[0]
        if cls < 3:
            return xarg[clean].mean()
        x, y = xarg[clean], yarg[clean]
    else:
        x, y = xarg, yarg
    # check if x is sorted:
    xgt = x[1:] > x[:-1]
    if xgt.all() or (~xgt).all():
        pass  # no need to sort
    else:
        order = np.argsort(x)
        x = x[order]
        y = y[order]
    # Limits and center in y (susceptible to noise - can you do better?)
    iymax = np.argmax(y)
    iymin = np.argmin(y)
    if y[iymin] == y[iymax]:
        return x.mean()
    cut = 0.5*(y[iymax] + y[iymin])
    below = y < cut
    # crossing points, even or odd as classifier
    cross = below[1:] != below[:-1]
    crs = cross.sum()
    inds = np.arange( 1,len(x), dtype=int )[cross]
    if crs % 2 == 0: # even crossings, peak or dip
        if crs > 2:  # which peak to choose?
            if y[inds[0]-1] < y[inds[0]]:  # peak
                iyp = iymax
            else:                          # dip
                iyp = iymin
            ipk = np.searchsorted( inds, iyp )
            xpk = x[inds[ipk-1]:inds[ipk]]
            ypk = y[inds[ipk-1]:inds[ipk]] - cut 
        else:
            xpk = x[inds[0]:inds[1]]
            ypk = y[inds[0]:inds[1]] - cut
        return np.average( xpk, weights = ypk )
    # steps
    else:            # odd, step up or down
        if crs == 1:
            return np.interp( cut,
                              (y[inds[0]],y[inds[0]]),
                              (x[inds[0]],x[inds[0]]))
        # several cuts, just try to average in the center
        yc = y[ inds[0] : inds[-1]]
        xc = x[ inds[0] : inds[-1]]
        p = np.polyfit( yc, xc, 3)
        return np.polyval( p, cut )



def mycen( scan, mot, ctr ):
   x = scan.get_data( mot )
   y = scan.get_data( ctr )
   pos = jcen4( x, y )
   return pos


class pars:
   pass

raytrace_params=pars()
   
def raytrace(m1,m2,mscan,rng = 1,count_time = 1,nbpoints = 40,
             ctr=None, gap=0.025, st=0.05):
   raytrace_params.m1pos = m1.position   # vo
   raytrace_params.m2pos = m2.position   # vg
   if ctr is None:
      import bliss.common.plot
      ctr = bliss.common.plot.get_plotted_counters()[0]
      print("Using counter",ctr)

   
   with cleanup(m1,m2, restore_list=(cleanup_axis.POS,)):
        # top
        m2.move(gap)
        m1.move(raytrace_params.m1pos-gap/2)
        first_scan = dscan(mscan,-rng,rng,nbpoints,count_time, stab_time=st)
# nfg        c0 = first_scan.cen(ctr)
        c0 = mycen( first_scan, mscan.name, ctr )

        raypos = list()
        raypos.append(c0)

        # middle
        m1.move(raytrace_params.m1pos)
        second_scan = dscan(mscan,-rng,rng,nbpoints,count_time, stab_time=st)
# nfg        c1 = second_scan.cen(ctr)
        c1 = mycen( second_scan, mscan.name, ctr )
        raypos.append(c1)

        # bottom
        m1.move(raytrace_params.m1pos+gap/2)
        last_scan= dscan(mscan,-rng,rng,nbpoints,count_time, stab_time=st)
# nfg        c2 =  last_scan.cen(ctr)
        c2 = mycen( last_scan, mscan.name, ctr )
        raypos.append(c2)
        print ('Positions:',c0,c1,c2)
        av = (c0+c1+c2)/3
        print ('Difference to mean:',c0-av,c1-av,c2-av)
        first_data = first_scan.get_data()
        second_data = second_scan.get_data()
        last_data = last_scan.get_data()

        def n(y):
            return (y - y.min()) / (y.max()-y.min())
        
        
        datas = {'first': n(first_data[ctr]),
                 'second': n(second_data[ctr]),
                 'last' : n(last_data[ctr]),
                 'x' : first_data[mscan.name]}
        global raytrace_plot
        #
        if raytrace_plot is not None:
           try:
              raytrace_plot.plot(datas)
           except:
              raytrace_plot = plot.plot(datas)
        else:
           raytrace_plot = plot.plot(datas)
           
           #        for cen_name,value in (('c0',c0),
           #                         ('c1',c1),
           #                         ('c2',c2)):
           #           raytrace_plot.qt.addXMarker(value,legend=cen_name,text=cen_name)
            
        xpos = (-gap/2, 0, gap/2)
        p = np.polyfit( xpos, raypos, 1)
        for i in range(3):
            print('motor: %.6f    peak: %.6f'%(xpos[i],raypos[i]))
        print('Polynomial fit dmotor/dpk, offset:', p)
        return (first_scan.get_data(),
                second_scan.get_data(),
                last_scan.get_data()),raypos

#p = plot.plot({"first":first_data["Au"],"second":second_data["Au"],"last":last_data['Au'],'x':first_data['px']})
#p.qt.addXMarker(44.023388307549524,legend='first_cen',text='cen')
#p = plot.plot({"first":first_data["Au"],"second":second_data["Au"]},xaxis=first_data['px'])

def raytrace_lens(m1,m2,mscan,rng = 1,count_time = 1,nbpoints = 40,
             ctr=None):
   raytrace_params.m1pos = m1.position
   raytrace_params.m2pos = m2.position
   if ctr is None:
      import bliss.common.plot
      ctr = bliss.common.plot.get_plotted_counters()[0]
      print("Using counter",ctr)

   with cleanup(m1,m2, restore_list=(cleanup_axis.POS,)):
        # top
        m2.move(0.025)
        m1.move(-0.0125,relative=True)
        first_scan = d2scan(mscan,-rng,rng,m1,-rng,rng,nbpoints,count_time)
# nfg        c0 = first_scan.cen(ctr)
        c0 = mycen( first_scan, mscan.name, ctr )

        raypos = list()
        raypos.append(c0)

        # middle
        m1.move(raytrace_params.m1pos)
        second_scan = d2scan(mscan,-rng,rng,m1,-rng,rng,nbpoints,count_time)
# nfg        c1 = second_scan.cen(ctr)
        c1 = mycen( second_scan, mscan.name, ctr )
        raypos.append(c1)

        # bottom
        m1.move(raytrace_params.m1pos+0.0125)
        last_scan= d2scan(mscan,-rng,rng,m1,-rng,rng,nbpoints,count_time)
# nfg        c2 =  last_scan.cen(ctr)
        c2 = mycen( last_scan, mscan.name, ctr )
        raypos.append(c2)
        print (c0,c1,c2)
        av = (c0+c1+c2)/3
        print (c0-av,c1-av,c2-av)
        first_data = first_scan.get_data()
        second_data = second_scan.get_data()
        last_data = last_scan.get_data()

        def n(y):
           return (y - y.min()) / (y.max()-y.min())
        
        
        datas = {'first': n(first_data[ctr]),
                 'second': n(second_data[ctr]),
                 'last' : n(last_data[ctr]),
                 'x' : first_data[mscan.name]}
        global raytrace_plot
        #
        if raytrace_plot is not None:
           try:
              raytrace_plot.plot(datas)
           except:
              raytrace_plot = plot.plot(datas)
        else:
           raytrace_plot = plot.plot(datas)
           
#        for cen_name,value in (('c0',c0),
#                         ('c1',c1),
#                         ('c2',c2)):
#           raytrace_plot.qt.addXMarker(value,legend=cen_name,text=cen_name)
        return (first_scan.get_data(),
                second_scan.get_data(),
                last_scan.get_data()),raypos


def shsave( filename = None ):
   import time
   pos = ["%s"%(repr(x)) for x in (shtx.position,
                                   shty.position,
                                   shtz.position,
                                   shrx.position,
                                   shry.position,
                                   shrz.position)]
   names = 'shtx shty shtz shrx shry shrz'.split()
   cmd = []
   for i in range(6):
      cmd.append( names[i] )
      cmd.append( pos[i] )
   cmd = ",".join( cmd )          
   if filename is None:
      filename = "/users/opid11/logfiles/shexa_"+time.strftime("%Y%m%dT%Hh%M")
   with open(filename,"w") as f:
      f.write("umv( %s )\n"%(cmd))
   print("Wrote",filename)

def shload( filename = None ):
   if filename is None:
      import glob
      filename = sorted(glob.glob("/users/opid11/logfiles/shexa_*"))[-1]
      print("Last was",filename)
   command = open(filename,"r").read()
   print(command)
   if input("OK to do that?? ")[0] in 'yY':
      exec(command)
   

def piezo_scanmode_on( ):
   piezo_scanmode( 1 )

def piezo_scanmode_off( ):
   piezo_scanmode( 0 )
   
def piezo_scanmode( toggle = None ):
   PI_E712_channels = [1, 2, 3] #From Bliss config: 1:X 2:Y 3:Z
   piezo_I = [5.09554e-04, 5.09554e-04, 3.62344e-04]
   """ Puts the piezo into a slow feedback mode or not
   to suppress problems that we did not yet fix """
   if toggle is None:
      print("Reading piezo PID I term")
      s=0
      for channel in PI_E712_channels:#range(1,4):
         x = float(pz.controller.command("SPA? %d 0x7000301"%(channel)))
         t = float(pz.controller.command("SPA? %d 0x7000900"%(channel)))
         if x < 1e-3 and t < 0.011:
            print(x,"FAST, tol", t,s)
            s-=1
         else:
            print(x,"SLOW, tol", t,s)
            s+=1
      print("rot state",rot.state)
      if rot.state.READY:
         s+=1
      else:
         s-=1
      if s == 4:
         print("OK: rot scanning mode")
         return False
      elif s == -4:
         print("OK: piezo scanning mode")
         return True
      else:
         print("HELP!!! neither mode")
         raise Exception("bad piezo workaround!!")
      return
   if toggle: # go to the piezo scanning
      print("Setting piezo PID I term to:")
      for i,channel in enumerate(PI_E712_channels):#range(1,4):  
         pz.controller.command("SPA %d 0x7000301 %s"%(channel,piezo_I[i])) #Was 3.3e-4
         pz.controller.command("SPA %d 0x7000900 0.01"%(channel)) 
         print( pz.controller.command("SPA? %d 0x7000301"%(channel)) )
      print("Turning rot off")
      time.sleep(3)
      rot.off()
   else:
      print("Setting piezo PID I term to:")
      for i,channel in enumerate(PI_E712_channels):  
         pz.controller.command("SPA %d 0x7000301 %s"%(channel,piezo_I[i]*100)) #Was 3.3e-2
         pz.controller.command("SPA %d 0x7000900 0.1"%(channel)) 
         print( pz.controller.command("SPA? %d 0x7000301"%(channel)) )
      print("Turned rot back on")
      rot.on()
      time.sleep(1)
      rot.sync_hard()
      if not rot.state.READY:
         print("problems turning rot on...")
         print(rot.state)




import requests

def eiger_hv():
    s = requests.get("http://lid11eiger2dcu.esrf.fr/logs/temperature_humidity.INFO").text[-170:].split("\n")[-3]
    print(time.strftime("%H:%M:%S :")+s)
    v = float(s.split()[-1])
    return v

def eiger_wait_hv():
    with bench():
        while eiger_hv() > -450:
            time.sleep(61)
    eiger.proxy.set_timeout_millis(60*1000)
    print("Set the photon energy!!! eiger.camera.photon_energy= ???")

def eiger_restart():
    import time
    phe = eiger.camera.photon_energy
    print("Photon energy was set to",phe)
    with bench():
        eiger.camera.initialize()
        print("Wait to stabilise",eiger_hv())
        time.sleep(61)
        while eiger_hv() > -450:
            time.sleep(61)
    print("Photon energy reset to",phe)
    eiger.camera.photon_energy = phe
    print("Print currently:",    eiger.camera.photon_energy )

    

def eiger_mon_hv():
   import requests
   a = "\n".join(requests.get("http://lid11eiger2dcu.esrf.fr/logs/temperature_humidity.INFO").text.split('\n')[-6:])
   print(a)
   open("logfiles/eigercurrent.log","a").write(a)

def eiger_current_check():
    import time
    phe = eiger.camera.photon_energy
    print("Photon energy was set to",phe)
    eiger.camera.initialize()
    print("Wait to stabilise",eiger_hv())
    while 1:
        try:
            time.sleep(60.1)
        except:
            break
        eiger_mon_hv()
    eiger.camera.photon_energy = phe
    print("Print currently:",    eiger.camera.photon_energy )
   
   
   
def dfscan( mot, lo, hi, npt, tim, **kwds): 
    b4 = mot.position
    stepssize = (hi - lo)/npt
    spd = stepssize/tim
    if spd > mot.velocity:
        print("Too fast")
        return
    try:
        fscan(mot, b4 + lo, stepssize, npt, tim, **kwds)
    except Exception as e:
        print("scan failed",str(e))
    finally:
        print("return to start position")
        umv(mot, b4)


def afscan( mot, lo, hi, npt, tim, **kwds):
    st = (hi - lo)/npt
    spd = st/tim
    if spd > mot.velocity:
        print("Too fast")
        return
    try:
        fscan(mot, lo, st, npt, tim, **kwds)
    except Exception as e:
        print("scan failed",str(e))


def set_dioptas_roi( cx = 1024,
                     cy = 1020,
                     xsize = 11,
                     ysize = 11,
                     name = 'roidioptas' ):
   x0 = cx - xsize // 2
   y0 = (2162 - cy ) - ysize // 2
   eiger.roi_counters.set( name, (x0, y0, xsize, ysize ) )
                     
