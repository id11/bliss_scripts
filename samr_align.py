#In TDXRD macros already
from bliss.common import plot as bl_plot
import numpy as np

def samrx_align(start, stop, npts, ctim, ctr):
    lastscan = ascan(samrx, start, stop, npts, ctim, ctr)
    sleep(1)
    A = lastscan.get_data( )
    y = A['image']
    summed = np.mean(y,axis=0)
    dataplot1 = bl_plot.plot_image(name='align samrx 1')
    dataplot1.plot({'y': summed}) 
    summedm = summed - y[int(npts/2)]
    dataplot2 = bl_plot.plot_image(name='align samrx 2')
    dataplot2.plot({'y': summedm}) 

def samry_align(start, stop, npts, ctim, ctr):
    lastscan = ascan(samry, start, stop, npts, ctim, ctr)
    sleep(1)
    A = lastscan.get_data( )
    y = A['image']
    summed = np.mean(y,axis=0)
    dataplot1 = bl_plot.plot_image(name='align samry 1')
    dataplot1.plot({'y': summed}) 
    summedm = summed - y[int(npts/2)]
    dataplot2 = bl_plot.plot_image(name='align samry 2')
    dataplot2.plot({'y': summedm}) 
