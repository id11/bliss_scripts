"""
DCT acquisition class macro

James Ball
Haixing Fang
Charles Romain
03/09/2024
"""


import os
import json

import numpy as np

import bliss.setup_globals as bliss_globals

class DCTScan:
    """
    # attributes:
    self.pars - dictionary of acquisition parameters
    
    # methods:
    self.ref_scan(...) - take a number of reference projections without moving sample (e.g flats, darks)
    self.run_single(...) - run a single scan
    self.run_zseries(...) - run multiple scans over a z series
    
    Usage in Bliss:
    
    ```
    (align sample...)


    dct = DCTScan()
    # check parameters...
    dct.pars
    # maybe change a parameter
    dct.pars['slit_hg'] = 1.0
    # happy with pars?
    dct.run_single()
    ```

    """
    
    def __init__(self):
        self.pars = self.default_pars()
        print('Default pars loaded:')
        pprint(self.pars)
    
    @staticmethod
    def default_pars():
        """Return a typical dict of DCT parameters - should be normally fine for standard samples"""
        
        pars = {}
        pars['start_pos'] = 0
        pars['step_size'] = 0.1
        pars['num_proj'] = 360 / pars['step_size']
        pars['exp_time'] = 0.1
        pars['refon'] = pars['num_proj']
        pars['ref_step'] = -2
        pars['scan_mode'] = 'CAMERA'
        pars['nref'] = 41
        pars['slit_hg'] = s7hg.position # horizontal beam size
        pars['slit_vg'] = s7vg.position # vertical beam size
        pars['ref_mot'] = samy
        pars['zmot'] = samtz
        pars['samtz_cen'] = pars['zmot'].position
        pars['shift_step_size'] = 0.2
        pars['nof_shifts'] = 3
        pars['slit8_offset'] = 0.05
        pars['use_slit8'] = True
        
        return pars
    
    def check_pars(self):
        num_proj = 360 / self.pars['step_size']
        if num_proj < self.pars['num_proj']:
            print('Updating step_size')
            self.pars['step_size'] = 360 / self.pars['num_proj']
            self.pars['refon'] = self.pars['num_proj']
        elif num_proj > self.pars['num_proj']:
            print('Updating the num_proj')
            self.pars['num_proj'] = 360 / self.pars['step_size']
            self.pars['refon'] = self.pars['num_proj']
        else:
            print('Good ! The parameters are self consistent')
        
    def get_serialisable_pars(self):
        """Serialise all pars - mainly converts axes to strings"""
        
        motor_pars = ['ref_mot', 'zmot']
        
        serial_pars = self.pars.copy()
        for key, value in serial_pars.items():
            if key in motor_pars:
                serial_pars[key] = value.name
        
        return serial_pars
    
    def export_pars(self, filename):
       """Export the parameters used to a file on disk."""
       
       pars_to_dump = self.get_serialisable_pars()
       with open(filename, 'w') as fout:
           json.dump(pars_to_dump, fout, indent = 2)
    
    @staticmethod
    def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
        """
        Move ref_mot by ref_step
        Take a number of reference images
        Move ref_mot back to start
        """
        refpos = ref_mot.position
        umv(ref_mot, refpos + ref_step)
        print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
        ftimescan(exp_time, nref, scan_mode=scanmode)
        umv(ref_mot, refpos)
    
    def set_slits(self):
        umv(s7vg, self.pars['slit_vg'], s7hg, self.pars['slit_hg'])
        if self.pars['use_slit8']:
            umv(s8vg, self.pars['slit_vg'] + self.pars['slit8_offset'], s8hg, self.pars['slit_hg'] + self.pars['slit8_offset'])
    
    def run_single(self, scanname='dct'):
        """Collect single DCT scan"""
        self.check_pars()
        newdataset(scanname)
        # set s7 slits
        self.set_slits()
        # rotate to start angle
        umv(diffrz, self.pars['start_pos'])
        num_im_grp = self.pars['refon']
        # this is to take reference images every N chunks of images
        # this is calculated by num_proj/refon
        # normally refon == num_proj
        for ref_grp in np.arange(0, self.pars['num_proj']/self.pars['refon'], 1):
            # starting angle for this group of projections
            startpos_grp = ref_grp * self.pars['refon'] * self.pars['step_size'] + self.pars['start_pos']
            # Take flat for this group of projections
            self.ref_scan(self.pars['ref_mot'], self.pars['exp_time'], self.pars['nref'], self.pars['ref_step'], startpos_grp, self.pars['scan_mode'])
            print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, self.pars['step_size'], num_im_grp, self.pars['exp_time'], self.pars['scan_mode']))
            # Take fscan - the actual rotation for this group of projections
            fscan(diffrz, startpos_grp, self.pars['step_size'], num_im_grp, self.pars['exp_time'], scan_mode = self.pars['scan_mode'])
        # Take flat at the end
        self.ref_scan(self.pars['ref_mot'], self.pars['exp_time'], self.pars['nref'], self.pars['ref_step'], self.pars['start_pos'] + self.pars['num_proj']*self.pars['step_size'], self.pars['scan_mode'])
        # Disables fast shutter so it doesn't open while collecting darks
        fsh.disable()
        # Take dark
        self.ref_scan(self.pars['ref_mot'], self.pars['exp_time'], self.pars['nref'], 0, self.pars['start_pos'] + self.pars['num_proj']*self.pars['step_size'], self.pars['scan_mode'])
        # Re-enable fast shutter at the end
        fsh.enable()
        # Move back to start omega angle
        umv(diffrz, self.pars['start_pos'])
        # Save pars to disk
        # get pars filename
        scan = SCANS[-1]
        par_file_url = scan.scan_saving.filename.replace('.h5', '_pars.json')
        self.export_pars(filename=par_file_url)
        print('Single DCT scan done')
    
    def run_zseries(self, scanname='dct'):
        """Collect zseries of DCT scans"""
        self.check_pars()
        shift_step_size = abs(self.pars['shift_step_size'])
        if(self.pars['nof_shifts'] < 1):
            print('The number of shifts is incorrect')
            return('The number of shifts is incorrect')
        offset_samtz_pos = self.pars['samtz_cen'] - (self.pars['nof_shifts'] - 1) * shift_step_size / 2
        print('Central samtz:' + str(self.pars['samtz_cen']))
        if(offset_samtz_pos < self.pars['zmot'].low_limit or offset_samtz_pos + (self.pars['nof_shifts'] - 1) * shift_step_size > self.pars['zmot'].high_limit):
            print('Exceed the limits of samtz')
            return('Exceed the limits of samtz')
        for iter_i in range(int(self.pars['nof_shifts'])):
            umv(self.pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
            self.run_single(scanname=scanname + str(iter_i + 1))
        umv(self.pars['zmot'], self.pars['samtz_cen'])
        print('Zseries DCT scans done')
