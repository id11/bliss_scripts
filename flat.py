import numpy as np
import pylab as pl

def flat( expo_time = 0.2,
          nframes = 2,
          ymotname = "samy",
          ydist = -3,
          cameraname = "frelon1",
          shuttername = "sheh3",
          ):
    
    # get current position
    ymot = config.get(ymotname)
    shutter = config.get(shuttername)
    camera = config.get(cameraname)
    y0 = ymot.position
    
    for x in ymotname, shuttername:
        assert x in current_session.object_names, x

    from bliss.scanning.group import Sequence
    seq = Sequence(title="take_flatfield", scan_info={"type":"flatfield_sequence"})

    with seq.sequence_context() as scan_seq:
        print("Closing shutter for the dark")
        shutter.close()
        d = loopscan( nframes, expo_time, camera.image,
                        title="dark", run=False )
        scan_seq.add( d )
        d.run()
        shutter.open()
        y0 = samy.position
        umv( samy, y0+ydist )
        print("Taking the flat")
        f = loopscan( nframes, expo_time, camera.image,
                        title="flat", run=False )
        scan_seq.add( f )
        f.run()
        umv( samy, y0 )
        print("Taking the images of object")
        print("you will have to close the new plot window in order to continue...")
        im = loopscan( nframes, expo_time, camera.image,
                       title="p0", run=False )
        scan_seq.add( im )
        im.run()
        
    return process_flat(d, f, im, nframes, cameraname)
       
def process_flat(d, f, im, nframes, cameraname):   
    camera = cameraname + ":image"
    dark = d.get_data()[camera].get_image(0).astype(np.float32)
    flat = f.get_data()[camera].get_image(0).astype(np.float32)
    image   = im.get_data()[camera].get_image(0).astype(np.float32)
    for i in range(1,nframes):
        dark +=  d.get_data()[camera].get_image(i).astype(np.float32)
        flat +=  f.get_data()[camera].get_image(i).astype(np.float32)
        image   +=   im.get_data()[camera].get_image(i).astype(np.float32)
    m = np.where(flat != dark , 1./(flat-dark), 1.)
    image = m*(image - dark)
   
    pl.subplot(1,1,1)
    pl.imshow(image,vmax=1,vmin=0)
    pl.show()
   
    return
    
