

import time
from numpy import sin, tan, arcsin, arctan, degrees, radians, cos, sqrt, interp
import numpy as np

def jtop(xarg, yarg):
    x = xarg
    y = yarg
    ymin = np.nanmin(y)
    ymax = np.nanmax(y)
    if ymin == ymax:
        return np.nanmean(x)
    cut = 0.5*(ymin+ymax)
    m = y >= cut
    return np.average( x[m], weights=abs(y[m] - cut))



def retune_pico6(cpm):
    """ give the target cpm18 position to tune at"""
    fsh = config.get('fsh_nscope')
    fsh.open()
    try:
        ACTIVE_MG.enable('pico6')
        plotselect(pico6)
        dscan(llrx2, -0.005, 0.005, 30, .1)
        goto_peak()
        dscan(llrx2, -0.005, 0.0, 15, .1)
        a2scan(cpm18, cpm-0.05, cpm+0.05, cpm18t, 0.07, 0.07, 20, .1)
        cpm18_goto( peak()[cpm18] )
        time.sleep(1)
        ct(1)
    except:
        print("got an error")
        raise
    finally:
        fsh.close()

def feclose():
    print("Closing front end")
    try:
        fe.close()
    except:
        print("Exception : could be a problem")

def feopen():
    print("Opening front end")
    try:
        fe.open()
    except:
        print("Exception : could be a problem")


def pflux():
    d = SCANS[-1].get_data()
    for k in list(d.keys()):
        if(k.find('pico')>=0):
            print(k,"%g"%(d[k].max()))
            
    
def bladescans( slt, rng=1, counter_name='pico3', blades='udfb' ):
    counter=config.get(counter_name)
    for blade in 'udfb':
        mot = globals()[slt+blade]
        mv(mot,0)
        s = dscan(mot, -rng, rng, 20, 0.1, counter )
        mv(mot, middle( mot, counter, scan=s ) )
        mot.position=0.
        mvr(mot,2)

def bladefind( mot, cnt, step=-1):
    top = ct(0.1,cnt).get_data()[cnt]
    while 1:
        umvr(mot, step)
        val = ct(0.1,cnt).get_data()[cnt]
        if( val < top / 2):
            break

def middle(mot, cnt='pico4', scan=None):
    if scan is None:
        scan = SCANS[-1]
    x,y = scan.get_data()[mot],scan.get_data()[cnt]
    order = np.argsort(y)
    return interp( 0.5*(y.min()+y.max()), y[order], x[order])

# ID11 LaueLaue Monochromator

LL_TRUEBEND1_FLAT = 0.0
LL_TRUEBEND2_FLAT = 0.00

# LL_TRUEBEND1_BENT = 1.525
#LL_TRUEBEND2_BENT = 2.000
# August 2023 restart JW/POA
LL_TRUEBEND1_BENT = 1.225
LL_TRUEBEND2_BENT = 1.52





#                   bent    -   flat
# LL_BEND1_ANGDIFF = 3.893346 - 3.8324  # at Sn edge
# LL_BEND2_ANGDIFF = 2.58085  - 2.6370 # at Nd edge

def ll_goto_flat( ):
    if abs( lltruebend1.position - LL_TRUEBEND1_FLAT ) < 0.05 and \
       abs( lltruebend1.position - LL_TRUEBEND2_FLAT ) < 0.05:
        print("Already bent, leave it along please")
        return
    umv(lltruebend1, LL_TRUEBEND1_FLAT-0.1, lltruebend2, LL_TRUEBEND2_FLAT-0.1)
    umv(lltruebend1, LL_TRUEBEND1_FLAT, lltruebend2, LL_TRUEBEND2_FLAT)
    print("You will need to retune an angle or edge now!")
    
def ll_goto_bent( ):
    if abs( lltruebend1.position - LL_TRUEBEND1_BENT ) < 0.05 and \
       abs( lltruebend1.position - LL_TRUEBEND2_BENT ) < 0.05:
        print("Already bent, leave it along please")
        return
    umv(lltruebend1, LL_TRUEBEND1_BENT-0.1, lltruebend2, LL_TRUEBEND2_BENT-0.1)
    umv(lltruebend1, LL_TRUEBEND1_BENT, lltruebend2, LL_TRUEBEND2_BENT)
    print("You will need to retune an angle or edge now!")

def ll_foils_and_diode():
    """ move foils/diode to monochromatic beam """
    umv(llfoils,10,lldiode,5)
    umv(llbragg2, llbragg1.position)


def _lrock( mot, rng=0.01, ctime=0.1, npts=20, counter_name='pico0', scanmove=True ):
    """ tunes second crystal for rocking curve """
    cnt=config.get(counter_name)
    plotselect(cnt)
    ACTIVE_MG.enable(counter_name)
    pos0 = mot.position
    scn = dscan(mot, rng, -rng, npts, ctime)
    umvr(mot,-rng)
    pkf = jtop( scn.get_data()[mot], scn.get_data()[cnt] )
    if scanmove:
        ascan(mot, pos0+rng, pkf, npts//2+1, ctime)
    else:
        safemove(mot, pkf)
    red = cnt.conversion_function(cnt.raw_read)
    print("Moved",mot.name,"to",mot.position)
    print("Peak flux was %g"%(scn.get_data()[cnt].max()))
    print("Flux now  is  %g"%(red))
    where()

def lrock2( rng=0.01, ctime=0.1, npts=20, counter_name='pico0' ):
    if (lldiode.position > 6) or (llfoils.position)<9:
        print("Did you forget to do ll_foils_and_diode ?")
    _lrock( llbragg2, rng=rng, ctime=ctime, npts=npts,  counter_name=counter_name )

def lrock1( rng=0.01, ctime=0.1, npts=20, counter_name='pico0' ):
    _lrock( llbragg1, rng=rng, ctime=ctime, npts=npts,  counter_name=counter_name )


def checkrock( mot, rng = 0.005, ctime=0.1, npts=20, counter_name = 'pico0'):
    cnt = config.get(counter_name)
    plotselect( cnt )
    ACTIVE_MG.enable(counter_name)
    pos0 = mot.position
    scn1 = ascan( mot, pos0+rng, pos0-rng, npts, ctime )
    pkf = jtop( scn1.get_data()[mot], scn1.get_data()[cnt] )
    maxval = scn1.get_data()[cnt].max()
    scn2 = ascan( mot, pkf+rng, pkf, npts//2+1, ctime )
    valnow = cnt.conversion_function( cnt.raw_read )
    print( f'Peak flux was {maxval}' )
    print( f'flux is now {valnow}' )
    print( f'Motor {mot.name} aiming for {pkf} is at {mot.position}' )

ll_foils_elements = ( 'Ag', 'Sn', 'Nd', 'Gd', 'Hf', 'W', 'Pt', 'Pb' )

## avoid dependency on xraylib
#    xraylib.EdgeEnergy(xraylib.SymbolToAtomicNumber(symbol),
#                                         xraylib.K_SHELL)
#                    for symbol in ll_foils_elements }

ll_foils_energies = {  # These values from xraylib
    'Ag': 25.514, 
    'Sn': 29.2001,
    'Nd': 43.5689,
    'Gd': 50.2391,
    'Hf': 65.3508,
    'W' : 69.525,
    'Pt': 78.3948,
    'Pb': 88.0045,
    }

LL_SI111 = 5.43094/sqrt(3.0) # 111 reflection
LL_FOILS_X = 115.0
LL_DIODE_X = 43. + LL_FOILS_X

ll_foils_position ={
    'Ag': -98.3,
    'Sn': -83.9,
    'Nd': -70.9,
    'Gd': -58.9,
    'Hf': -43.,
    'W' : -31.,
    'Pt': -17.9,
    'Pb': -3.1,
    }

ll_foils_rz1enc = { # with the crystal optimally bent
    'Sn' : 143424, # 143447 
    'Nd' : 119568,
    'Gd' : 113022,
    'Hf' : 103274,
    'W'  : 101384,
    'Pt' :  97820,
    'Pb' :  94863,
    }


# 1.e7*scipy.constants.h*scipy.constants.c/scipy.constants.e
# bliss.diffraction has confusing units (Joules/metres)?
ECONST_KEV_ANG = 12.398419843320028

def llrz1_move_to_lvdt( encpos ):
    delta = llrz1_enc.read() - encpos
    while abs(delta)>1.5:
        print("Current delta",delta)
        umvr(llrz1, -delta*16/llrz1.steps_per_unit)
        time.sleep(1)
        delta = llrz1_enc.read() - encpos
    print("Current delta",delta)


def ll_init_lvdt( ):
    """ FIXME !!! llrz2 as well when connected !!! """
    # print(llrz1,llrz2,llrz1_enc,llrz2_enc) 
    a1 = llrz1.config.get('address')
    a2 = llrz2.config.get('address')
    try:
        previous_value = llrz1_enc.counter.raw_read*llrz1_enc.steps_per_unit
        print("Current llrz1 enc",previous_value, llrz1_enc.read())
        previous_value = llrz2_enc.counter.raw_read*llrz2_enc.steps_per_unit   
        print("Current llrz2 enc",previous_value, llrz2_enc.read())    
    except:AttributeError
        #print: ("fix me!")
        
    from bliss.controllers.motors import icepap  
    # power off encoder
    icepap._ackcommand(llrz1.controller._cnx,
                       "%s:auxps off"%(a1) )
    icepap._ackcommand(llrz2.controller._cnx,
                       "%s:auxps off"%(a2) )
    print("Resetting...")
    time.sleep(3)
    icepap._ackcommand(llrz1.controller._cnx,
                       "%s:power on"%(a1) )
    icepap._ackcommand(llrz2.controller._cnx,
                       "%s:power on"%(a2) )
    time.sleep(3)
    icepap._ackcommand(llrz1.controller._cnx,
                       "%s:enc encin 0"%(a1) )
    icepap._ackcommand(llrz2.controller._cnx,
                       "%s:enc encin 0"%(a2) )
    time.sleep(10)
    new_value = llrz1_enc.read()*llrz1_enc.steps_per_unit
    print("New llrz1 enc",new_value, llrz1_enc.read())
    new_value = llrz2_enc.read()*llrz2_enc.steps_per_unit
    print("New llrz2 enc",new_value, llrz2_enc.read())
    llrz1.controller.set_position(llrz1, llrz1_enc.read()*llrz1.steps_per_unit)
    llrz2.controller.set_position(llrz2, llrz2_enc.read()*llrz2.steps_per_unit)
    llrz1.sync_hard()
    llrz2.sync_hard()    


def ll_angle_rad( en ):
    """ Bragg angle in radians for Silicon (111) """
    return arcsin( ECONST_KEV_ANG / (2 * LL_SI111 * en ) )

def ll_angle_deg( en ):
    """ Bragg angle in degrees for Silicon (111) """
    return degrees( ll_angle_rad(en) )

def ll_energy_now():
    e1 = ECONST_KEV_ANG/(2*LL_SI111*sin(radians(llbragg1.position)))
    e2 = ECONST_KEV_ANG/(2*LL_SI111*sin(radians(llbragg2.position)))
    print("From llbragg1: %.3f keV"%(e1))
    print("From llbragg2: %.3f keV"%(e2))


def ll_energy( en, offset = 'llty2' ):
    """ angle and distance for fixed exit beam position in ID11 LL mono
    offset = motor name or number in mm for the beam offset """
    rad_angle = ll_angle_rad( en )
    tan2th = tan( rad_angle * 2 )
    if offset in config.names_list:
        beamoffset = globals()[ offset ].position
        if beamoffset > 35:
            print("Guessing mono is out, using 14 mm offset instead")
            beamoffset=14.
    else:
        beamoffset = float( offset )
    x_th = beamoffset / tan2th , degrees( rad_angle)
    print("tx %.4f angle %.5f"% x_th )
    return x_th

def ll_goto_energy( en, offset='llty2' ):
    if ll_is_out():
        ll_insert()
    if not ll_is_in() or not ll_bs_is_in():
        print("LL is in a strange position, should never happen")
        raise
    tx, theta = ll_energy( en, offset )
    umv( lltx1, tx, llbragg1, theta, llbragg2, theta )
    #Attempt to save energy in RAW_DATA POA+CR 29/05/24
    #en_metadata = {"positioners":{"energy": en, "energy@units":"keV"}}
    #scan_meta_obj.instrument.set("energy", en_metadata)


def ll_is_in( ):
    return (-10 < llty1.position and
            llty1.position < 10. and
            5. < llty2.position and
            llty2.position < 35.)

def ll_bs_is_in( ):
    return (llbs.position < (llty2.position-5)) and ( (llbs.position+llty2.position) > 3)

def ll_is_out( ):
    """ is the mono in or out ? """
    return llty1.position > 135. and llty2.position > 120.
    
def ll_insert( en=None, offset=14.0 ):
    """ put the mono back in """
    feclose()
    umv(llty1, 0, llty2, offset, llbs, 4-offset )
    if en is not None:
        ll_goto_energy( en, offset )
    
def encmotor_is_ok( mne, tolerance=1. ):
    ''' check encoder / motor agree '''
    m = globals()[mne]
    if (m.position - m.measured_position) > tolerance :
        print("Problem: encoder does not match position for ",mne)
        return None # raise exception ?
    return m

def ll_remove( xmin=200., y1=136.4, y2=120.6 ):
    # raise Exception("llty2 limit problem - cannot remove today" )
    feclose()
    # verify the x motor has not died
    xm = encmotor_is_ok( 'lltx1' ) 
    if xm.position < xmin:
        umv(xm, xmin)
        if encmotor_is_ok( 'lltx1' ).position < xmin:
            print("lltx1 is not moving. Giving up")
            return
    mv(llty1,136,llty2,74)
    print("Checking we hit the limits")
    # Verify by hitting the limit switches
    mot_findlimit( llty1, direction=-1 )
    print("llty2 is at 74 shooting through a small gap in mechanics")
    print("check the beam comes through with open gaps before using!!!")
    print("ll removed. Use ll_insert to put it back""")


def user_confirm(question): # find in stdlib ?
    response = input(question)
    return len(response) > 0 and response[0] in 'yY'

def ll_edge_scan( element ):
    assert element in ll_foils_elements
    en = ll_foils_energies[ element ]
    tx, theta = ll_energy( en )
    # Ask user if all is OK ??
    # if user_confirm( "Move lltx1 to %f and llrz1 to %f:"%(tx,theta)):
    umv( lltx1,  tx)
    umv( llbragg1, theta, llbragg2, theta+0.1 )
    f0 = LL_FOILS_X * tan( radians( 2*theta )) + ll_foils_position[ element ]
    # place to put the diode should be:
    # ... zero is the edge of the copper block
    #     +6 goes to the middle of active area
    #     moving ty2 +ve means moving diode -ve to cancel out
    d0 = LL_DIODE_X * tan( radians( 2*theta )) + 6
    umv( lldiode, d0, llfoils, f0)
    g18, g22 = guess_unds_edge( en )
    umv( cpm18, g18 )
    umv( u22, g22 )
    plotselect( pico0 )
    dscan( llbragg1, 0.05, -0.05, 40, 0.1, pico0 )
    #Attempt to save energy in RAW_DATA POA+CR 29/05/24
    #en_metadata = {"positioners":{"energy": en, "energy@units":"keV"}}
    #scan_meta_obj.instrument.set("energy", en_metadata)


def guess_unds_edge(en, gap=10, dN = 0.5):
    """
    Guess the undulator gaps that is near gap value
    dN = goto to 1/2 way
    """
    from id11.undulator import id11_cpm18, id11_u22
    n18 = en/id11_cpm18.ef_from_gap( gap )
    g18 = id11_cpm18.gap_from_ef( en / (int(n18) + dN ) )
    n22 = en/id11_u22.ef_from_gap( gap )
    g22 = id11_u22.gap_from_ef( en / (int(n22) + dN ) )
    return g18, g22

def ll_print_config():
    for motorname in ("llrx1 llry1 llrz1  llrx2 llry2 llrz2 "+
                      "lltx1 llty1 llty2 "+
                      "llbend1 llbend2 "+
                      "llbragg1 llbragg2 "+
                      "lltruebend1 lltruebend2 "+
                      "llbeamy1 llbeamy2 " +
                      "llbs lldiode llfoils").split():
        ax = globals()[motorname]
        print("%s : %s"%( motorname, ax.config.get("description")))
        
        
        
############# testing / commissioning scripts # clean up below 



def ll_bend_check_macro( ofs = 0.8 ):
    s1_0 = s1ho.position
    s2_0 = s2ho.position
    umv( s1ho, s1_0-ofs, s2ho, s2_0-ofs )
    dscan( llbragg1, 0.005, -0.005, 40, 0.1, pico0)
    x0 = find_position(jcen)
    umv( s1ho, s1_0+ofs, s2ho,  s2_0+ofs )
    dscan( llbragg1, 0.005, -0.005, 40, 0.1, pico0)
    x1 = find_position(jcen)
    umv( s1ho, s1_0, s2ho, s2_0 )
    print(x0, x1, x0-x1)

def ll_bend_check_macro2(    ofs = 0.8 ):
    s1_0 = s1ho.position
    s2_0 = s2ho.position
    umv( s1ho, s1_0-ofs, s2ho, s2_0-ofs )
    dscan( llbragg2, 0.005, -0.005, 40, 0.1, pico0)
    x0 = cen()
    umv( s1ho, s1_0+ofs, s2ho,  s2_0+ofs )
    dscan( llbragg2, 0.005, -0.005, 40, 0.1, pico0)
    x1 = cen()
    umv( s1ho, s1_0, s2ho, s2_0 )
    print(x0, x1, x0-x1)


def ll_rz2bend():
    import numpy as np
    umv(llrz2,2.665)
    for b2 in np.arange( -0.6, 4.01, 0.1):
        umv(llbend2, b2)
        dscan(llrz2, -0.03, 0.02, 50, 0.1, pico0)
        umvr(llrz2, -0.03)
        goto_peak()
        dscan(llrz2, -0.005, 0.005, 100, 0.1, pico0)
    
def ll_rz1bend():
    import numpy as np
    umv(llrz1,2.54553)
    for b2 in np.arange( -0.3, -0.91, -0.1):
        umv(llbend1, b2)
        dscan(llrz1, -0.03, 0.02, 50, 0.1, pico0)
        z0 = peak()
        umv(llrz1, z0-0.01)
        ascan(llrz1, z0-0.005, z0+0.005, 100, 0.1, pico0)
        
    

# Is this bad? Should be gevent.sleep instead
import time
def cpm18_goto( pos, post=0.075, tol=0.004, tolt=0.01, max_try=10 ):
    mv( cpm18, pos, cpm18t, post)
    time.sleep(1)
    itry=0
    while itry < max_try and \
          abs(cpm18.position - pos)>tol or \
          abs(cpm18t.position - post)>tolt:
        print("Made it to", cpm18.position, cpm18t.position)
        try:
            mv( cpm18t, post, cpm18, pos)
        except RuntimeError: 
            print("Got an error, not sure why") 
        time.sleep(1)
        itry+=1
        print("try ",itry)
    
    print("\nMade it to", cpm18.position, cpm18t.position)


def safemove( mot, pos, tol=0.0001, ntries=10):
    print("%s: safemove %s to %f tol %f"%(__file__, mot.name, pos, tol))
    mot.sync_hard()
    ntry = 0 
    while abs( mot.position - pos ) > tol:
        mv(mot, pos)
        time.sleep(0.1)
        mot.sync_hard()
        ntry += 1
        if ntry == ntries:
            print("Gave up, left at", mot.position)
            break
    
    
def measure_lvdt():
    a2scan(llrz1,0.8,5, llrz2,0.8,5,42000, 0.001, llrz1_enc, llrz2_enc)
    a2scan(llrz1,5,0.8, llrz2,5,0.8,42000, 0.001, llrz1_enc, llrz2_enc)
    a2scan(llrz1,0.8,5, llrz2,0.8,5,42000, 0.001, llrz1_enc, llrz2_enc)
    a2scan(llrz1,5,0.8, llrz2,5,0.8,42000, 0.001, llrz1_enc, llrz2_enc)
