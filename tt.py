import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime
from bliss.common import cleanup


def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)


def test_loop_grains(grain_list):
    """Loops through a list of Topotomo grains  and performs refinement + TT scan_acquisition
    :param dict grain_list:  list of grain structures containing Topotomo alignment values
    :param dict tt_pars: Topotomo scan parameters
    :param int step: running number of load step in load-sequence  
    """
    for i in range(len(grain_list)):
        grain_list[i]['toto']=0
    return(grain_list)
    



def read_tt_infos(gid):
    #data_dir = '/data/visitor/ihma342/id11/20230302/DCT_Analysis/AlCu_8wt_March_dct1'
    #data_dir = '/data/projects/3dipolyplast/ma4925/DCT_Analysis/Ni_s20_dct2_0002'
    #data_dir = '/data/visitor/ma5609/id11/20230706/PROCESSED_DATA/saa_CuCrZr_7/saa_CuCrZr_7_load_0p8N_dct2'
    data_dir = '/data/visitor/ma5830/id11/20231031/PROCESSED_DATA/epr5_def2_nov/epr5_def2_nov_dct5'
    data_path = os.path.join(data_dir, 'grain_%04d.mat' % gid)
    data = loadmat(data_path)
    t = data['out'][0][0][0][0]
    values = t[0][0][0][0][0]
    d = {}
    d['gr_id'] = int(values[0][0][0])
    # double check that the grain id is right
    if d['gr_id'] != gid:
        raise(ValueError('grain id mismatch, check your matlab output in file %s' % data_path))
    d['nfdtx'] = float(values[1][0][0])
    d['d3tz'] = float(values[2][0][0])
    d['diffry'] = float(values[3][0][0])
    d['samrx'] = float(values[4][0][0])
    d['samry'] = float(values[5][0][0])
    d['samtx'] = float(values[6][0][0])
    d['samty'] = float(values[7][0][0])
    d['samtz'] = float(values[8][0][0])
    d['samrx_offset'] = float(values[9][0][0])
    d['samry_offset'] = float(values[10][0][0])
    d['samtx_offset'] = float(values[11][0][0])
    d['samty_offset'] = float(values[12][0][0])
    d['samtz_offset'] = float(values[13][0][0])
    d['diffrz_offset'] = float(values[14][0][0])
    d['int_factor'] = float(values[15][0][0])
    print(d)
    return d
    
atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
bigy = config.get('bigy')
u22 = config.get('u22')

cpm18_tuned = 10.086
u22_tuned = 8.309

dct_dist = 8
pct_dist = 30
nfdtx_savedist = 20
ffdtx1_savedist = 1190
ffdtx1_savedist_tt = 1190
tt_x_offset = -28.64

# after loading adjust samtz according to surface features and reset samtz.position = 0
samtx_offset = 1.715
samty_offset = 0.0166
samtz_offset = -4.9733
samrx_offset = -1.1
samry_offset = 1.5
difftz_offset = -0.5
diffty_offset = 15.08

ff_offset = 350
tt_offset = 0
tt_dist = 8
ff_z = 0
ff_dist = 200

d1_out = -87
d1_in = 0
d1ty_bs_in = -77.74
d1ty_bs_out = -100
d2_out = -392
d2_in = -392

d3_out = 100
d3tz_pos_dct = -0.045
d3tz_pos_tt = 1.2
d3_bsty_in = 0
d3_bsty_out = 1000
bigy_in = 0.0
bigy_out = 22.0
furnace_out = 145

def sam_dct_pos():
    umv(samrx, samrx_offset, samry, samry_offset, diffry, 0)
    umv(samtx, samtx_offset, samty, samty_offset, samtz, samtz_offset)

def attyin():
    umv(atty, 0, attrz, -5.5)

def attyout():
    umv(atty, -10, attrz, 0)

def bsin(d3_bsty_in = 0):
	umv(d3_bsty, d3_bsty_in)

def bsout(d3_bsty_out=-1000):
	umv(d3_bsty, d3_bsty_out)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e4):
    c = ct(1,p201_20).get_data()['fpico3']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_20).get_data()['fpico3']

def frelon1in():
    umv(d3ty, d3_out)
    umv(ffdtx1, 500, nfdtx, 200, ffdtz1, 100)
    tfoh1.set(0,'Al')
    tfoh1.set(16,'Be')
    #for i in range (3):
    #    umv(tfz, -0.319, tfy, 14.55)
    attyout();
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon3*")
    ACTIVE_MG.enable("frelon1:image")
    umv(diffrz, 0)
    umv(d1ty, 0, d1tz, 0)
    print("ready to collect dct data")
    
def ffin(ff_pars):
    sheh3.close()
    tfoh1.set(18,'Be')
    tfoh1.set(0,'Al')
    for i in range (3):
        umv(tfz, -0.325, tfy, 14.71)
    umv(bigy, bigy_out)
    attyin();
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon16:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out, d2ty, d2_out)
    umvct(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umvct(s8hg, ff_pars['slit_vg'] + 0.2, s8hg, ff_pars['slit_hg'] + 0.2)
    umv(ffdtx1, ff_dist, nfdtx, dct_dist, ffdtz1, 0)
    print("ready to collect far-field data")
    sheh3.open()
 
def scanning_ff():
    if bigy.position > 20:
        umvr(bigy, -bigy_out-0.047)
    umv(atty, 0, attrz,-9)
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon16:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out)
    #umv(ffdtx1, ff_dist, nfdtx, dct_dist, ffdtz1, 0)
    print("ready to collect far-field data")

def marana_pct(pct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    umv(furnace_z, furnace_out)
    umv(nfdtx, pct_pars['dist'])
    centeredroi(marana3,1024,1024)
    bsout()
    sct(pct_pars['exp_time'])
    umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'])
    umvct(s8vg, pct_pars['slit_vg'] + 0.05, s8hg, pct_pars['slit_hg'] + 0.05)

def maranain(dist):
    tfoh1.set(18,'Be')
    tfoh1.set(0,'Al')
    for i in range (3):
        umv(tfz, -0.325, tfy, 14.71)
    sheh3.close()
    attyout()
    umv(bigy, bigy_out)
    ACTIVE_MG.enable("marana3:image")
    ACTIVE_MG.disable("frelon16*") 
    ACTIVE_MG.disable("frelon3*")
    #sam_dct_pos()
    umv(nfdtx, dist, ffdtz1, ff_z)
    umv(d2ty, d2_out, d3ty, 0, d3tz, d3tz_pos_dct)
    print("ready to collect marana3 data") 
    sheh3.open()
    ct(0.02)

def marana_large_beam(pct_pars):
    tfoh1.set(96,'Al')
    tfoh1.set(18,'Be')
    bsout()
    for i in range (3):
        umv(tfz, -0.325, tfy, 14.74)
    marana3.image.roi=[512,0,1024,2048]
    sct(0.05)
    umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'], nfdtx, pct_pars['dist'])
    umvct(s8vg, pct_pars['slit_vg'] + 0.5, s8hg, pct_pars['slit_hg'] + 0.5)
    umvct(samtz, samtz_offset)
    ct(pct_pars['exp_time'])

def marana_dct(dct_pars):
    tfoh1.set(0,'Al')
    #tfoh1.set(26,'Be')
    tfoh1.set(0,'Be')
    umv(furnace_z, furnace_out)
    umv(nfdtx, dct_pars['dist'])
    #for i in range (3):
    #    umv(tfz, -0.325, tfy, 14.71)
    marana3.image.roi=[0,0,2048,2048]
    bsin()
    umvct(s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umvct(s8vg, dct_pars['slit_vg'] + 0.05, s8hg, dct_pars['slit_hg'] + 0.05)
    assert furnace_z.position > 125
    umvct(nfdtx, dct_pars['dist'])
    sct(dct_pars['exp_time'])


def tt_in(theta, tt_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(24,'Be')
    for i in range (3):
        umv(tfy, 14.71)
    #attyout();
    ACTIVE_MG.disable("marana3:image")
    ACTIVE_MG.disable("frelon16*") 
    ACTIVE_MG.enable("frelon3:image")
    frelon3.image.flip=[False, True]
    d2tzpos = np.tan(2*np.deg2rad(theta))*tt_pars['dist']
    #print("moving nfdtx, ffdtx1 and ffdtz1 to save positions")
    #umvct(ffdtx1, ffdtx1_savedist_tt, ffdtz1, 200)
    print("moving d2tz to target position %g"%d2tzpos)
    umvct(d2tz, d2tzpos)
    umv(d2ty, 0, d3ty, d3_out)
    umv(nfdtx, tt_pars['dist'] + tt_x_offset)
    umvct(s7vg, tt_pars['slit_vg'], s7hg, tt_pars['slit_hg'])
    umvct(s8vg, tt_pars['slit_vg'] + 0.1, s8hg, tt_pars['slit_hg'] + 0.1)
    ct(0.1)
    print("ready to collect topotomo data")

def frelon16setCenteredRoi(roi_width=1920, roi_height=1920):
    frelon16.image.roi = [961 - int((roi_width+1)/2), 961 - int((roi_height+1)/2), roi_width, roi_height]
    print(frelon16.image.roi)

def read_positions():
    g = {}   
    g['samrx']=samrx.position
    g['samry']=samry.position
    g['samtx']=samtx.position
    g['samty']=samty.position
    g['samtz']=samtz.position
    g['d2tz']=d2tz.position
    return g

def topotomo_tilt_grain(gr_id):
    """drives diffractometer to grain alignment position for
    """
    topotomo_params = read_tt_infos(gr_id)
    print(topotomo_params)
    #if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
    #    return('Error')
    #if abs(d3ty.position - d3_out) > 0.1:
    #    return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d3tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')

def topotomo_tilt_grain_json(gr_id):
    """drives diffractometer to grain alignment position for
    """
    topotomo_params = get_json_grain_info(gr_id)
    print(topotomo_params)
    if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
        return('Error')
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d2tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')
    
def topotomo_tilt_grain_dict(topotomo_params):
    """drives diffractometer to current grain alignment position
    """
    print(topotomo_params)
    #if abs(d3ty.position - d3_out) > 0.1:
    #    return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    #umv(nfdtx, topotomo_params['nfdtx'])
    umv(d3tz, topotomo_params['d3tz'])
    #umv(diffrz, 90) #save position to change tilts for the diffractometer with Nanox during ma5178...
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')

# -*- coding: utf-8 -*-
import os
import numpy as np

def define_tt_pars():
    tt_pars={}
    tt_pars['start_pos'] = 5
    tt_pars['diffrz_step'] = 4
    tt_pars['num_proj'] = 360 / tt_pars['diffrz_step']
    tt_pars['diffry_step'] = 0.02
    tt_pars['exp_time'] = 0.1
    # tt_pars['search_range'] = 0.3 # 0, 44, 48
    # tt_pars['search_range'] = 0.4 # 52
    tt_pars['search_range'] = 0.45 # 56
    tt_pars['scan_mode'] = 'CAMERA'
    tt_pars['image_roi'] = [512, 512, 1024, 1024] 
    # tt_pars['counter_roi'] = [412, 412, 200, 200] # 0N
    # tt_pars['counter_roi'] = [412-10, 412-10, 200+20, 200+20] # 44N
    # tt_pars['counter_roi'] = [412-20, 412-20, 200+40, 200+40] # 48N
    tt_pars['counter_roi'] = [412-20, 412-20, 200+40, 200+40] # 48N
    tt_pars['slit_hg'] = 0.3
    tt_pars['slit_vg'] = 0.3
    tt_pars['dist'] = 7
    return tt_pars
    

def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.12
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -3
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 1.1
    dct_pars['slit_vg'] = 0.1
    dct_pars['dist'] = 10.5 #2.7S
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 1.61856
    dct_pars['shift_step_size'] = 0.08
    dct_pars['nof_shifts'] = 7
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars

def define_dct_pars_fine():
    dct_pars={}
    dct_pars['start_pos'] = -0.4
    dct_pars['step_size'] = 0.01
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.05
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -4
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.65
    dct_pars['slit_vg'] = 0.15
    dct_pars['dist'] = 8
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 0.829+0.017+0+0+0
    dct_pars['shift_step_size'] = 0.1
    dct_pars['nof_shifts'] = 5
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars

def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = -8
    pct_pars['step_size'] = 0.36
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.05
    pct_pars['refon'] = pct_pars['num_proj']
#    pct_pars['ref_int'] = 10
    pct_pars['ref_step'] = 0
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 100
    pct_pars['ref_mot'] = samy
 #   pct_pars['zmot'] = samtz
    pct_pars['slit_hg'] = 1.5
    pct_pars['slit_vg'] = 1.5
    pct_pars['dist'] = 100
    pct_pars['scan_type'] = 'fscan'
    return pct_pars
    
def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 0.4
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 1.1
    ff_pars['slit_vg'] = 0.1
    ff_pars['mode'] = 'ZIGZAG'  # FORWARD
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = 1.61856
    ff_pars['shift_step_size'] = 0.08
    ff_pars['nof_shifts'] = 7
    ff_pars['cpm18_detune'] = 0
    return ff_pars
    
def define_sff_pars():
    sff_pars={}
    sff_pars['start_pos'] = -0.4
    sff_pars['step_size'] = 0.8
    sff_pars['num_proj'] = 360 / sff_pars['step_size']
    sff_pars['exp_time'] = 0.08
    sff_pars['slit_hg'] = 0.01
    sff_pars['slit_vg'] = 0.01
    sff_pars['mode'] = 'FORWARD'
    sff_pars['zmot'] = samtz
    sff_pars['samtz_cen'] = 0.829 + 0.017+0+0+0
    sff_pars['shift_step_size'] = 0.05
    sff_pars['nof_shifts'] = 5
    sff_pars['cpm18_detune'] = 0
    return sff_pars

def load_grains_from_matlab(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = read_tt_infos(gr_id)
        grain_list.append(grain)
    return grain_list


def load_grains_from_txt(grain_ids, load = 52, prefix = '/data/visitor/ma5609/id11/20230706/RAW_DATA/saa_CuCrZr_7/saa_CuCrZr_7_g'): 
    grain_list = []
    for grain_id in grain_ids:
        filename = prefix + str(grain_id) + '_' + str(load) + 'N.txt'
        with open(filename) as f:
            d = {k: float(v) for line in f for (k, v) in [line.strip().split(':',1)]}
            d['gr_id'] = int(d['gr_id'])
        grain_list.append(d)
    return grain_list
   
def load_grains_from_json(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = get_json_grain_info(gr_id)
        grain_list.append(grain)
    return grain_list

def load_ramp_by_ascan(start, stop, npoints, exp_time, loadstep, pct_pars):
    marana_large_beam(pct_pars)
    newdataset('loadramp_%d' %  loadstep)
    ascan(stress, start, stop, npoints, exp_time, stress_adc, marana3)
    return

def sff_one_grain(scanname, sff_pars, grain):
    user.cpm18_goto(cpm18_tuned + sff_pars['cpm18_detune'])
    umv(s7vg, 0.5, s7hg, 0.5, s8vg, 0.05, s8hg, 0.05) 
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, bigy_in)
    umv(samtx, grain['samtx'], samty, grain['samty'], samtz, grain['samtz'])
    stage_diffty_zero = diffty_offset

    scanstarty= -0.1  # in mm
    scanendy=0.101
    scanstepy=0.01
    
    scanstartz= -0.15  # in mm
    scanendz=0.1501 
    scanstepz=0.01    

    umvr(samtz, scanstartz)
    z_scans_number = (scanendz - scanstartz) / scanstepz
    newdataset(scanname)
    for i in range(int(z_scans_number)):
        umvr(samtz, scanstepz)
        for ty in np.arange(scanstarty,scanendy,scanstepy):
            umv(diffty, stage_diffty_zero+ty)
            print(f"Scan pos: y: {ty} couche {i}")
            finterlaced( diffrz, sff_pars['start_pos'], acq_size = sff_pars['step_size'], npoints = sff_pars['num_proj'], acq_time = sff_pars['exp_time'], mode = sff_pars['mode'])

    umv(diffrz,0)      
    umv(diffty, stage_diffty_zero)
    umv(bigy, bigy_out)
    diffty_airpad.off()
    user.cpm18_goto(cpm18_tuned)
    
    umv(samtz,0)
    print("All done!")
    

def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def load_ramp_by_target(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    write_to_file = to_file if to_file is not None else sys.stdout
    #marana_large_beam(pct_pars)
    current_load = stress_adc.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return


def define_pars(load_list):
    """Produces default input parameters for a load sequence (see next function)
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    for load in load_list:
        step_pars['dct_pars'] = define_dct_pars()
        step_pars['pct_pars'] = define_pct_pars()
        step_pars['tt_pars'] = define_tt_pars()
        step_pars['ff_pars'] = define_ff_pars()
        step_pars['sff_pars']= define_sff_pars()
        step_pars['target'] = load
        step_pars['load_step'] = step
        step = step + 1
        par_list.append(step_pars)        
    return par_list

def define_pars_one_load():
    """Produces default input parameters for one load
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    step_pars['dct_pars'] = define_dct_pars()
    step_pars['pct_pars'] = define_pct_pars()
    step_pars['tt_pars'] = define_tt_pars()
    step_pars['ff_pars'] = define_ff_pars()
    step_pars['sff_pars']= define_sff_pars()
    step_pars['target'] = load
    step_pars['load_step'] = step
    par_list.append(step_pars)
    return par_list


def load_sequence(par_list, grain_list):
    """Load Experiment Master Sequence.
    
    This function will perform scan sequences (pct, dct, tt) for a list of target load values defined in "par_list" 
    Note: grain_list is currentatty produced by a matlab script and can be re-created / imported via:  grain_list = user.read_tt_info([list_of_grain_ids])
    par_list can be created via: par_list = user.define_pars()  and sub-parameters for dct, pct, tt can be adapted to cope with increasing mosaicity

    """
    for step_pars in par_list:
        dct_pars = step_pars['dct_pars']
        pct_pars = step_pars['pct_pars']
        tt_pars = step_pars['tt_pars']
        ff_pars = step_pars['ff_pars']
        sff_pars = step_pars['sff_pars']
        target = step_pars['target']
        load_step = step_pars['load_step']
        grain_list = load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, ff_pars, sff_pars, target, load_step)
    return grain_list


# saa_CuCrZr_7
# saa_CuCrZr_7_tt_test
# 44 N
def run_load_step_sequence():
    load_targets = [56,]
    grain_ids = [1, 3 ,55, 246]
    
    grain_list = load_grains_from_matlab(grain_ids)
    print(grain_list)
    for target in load_targets:
    
        dct_pars = define_dct_pars()
        dct_pars_fine = define_dct_pars_fine()
        pct_pars = define_pct_pars()
        tt_pars = define_tt_pars()
        ff_pars = define_ff_pars()
        sff_pars = define_sff_pars()
        
        #grain_list_updated = load_step_sequence(dct_pars, dct_pars_fine, pct_pars, tt_pars, ff_pars, sff_pars, grain_list, target)
        load_step_sequence(dct_pars, dct_pars_fine, pct_pars, tt_pars, ff_pars, sff_pars, grain_list, target)
    print('Done successfully !!!')
    return grain_list_updated

# for sample ssa_CuCrZr_3
def run_sample3():
    load_targets = [0.5,]
    #load_targets = [44, 46, 48, 50, 52, 53, 54, 55, 56, 57, 58]

    for target in load_targets:
    
        dct_pars = define_dct_pars()
        dct_pars_fine = define_dct_pars_fine()
        pct_pars = define_pct_pars()
        tt_pars = define_tt_pars()
        ff_pars = define_ff_pars()
        sff_pars = define_sff_pars()
        
        # loading        
        if target > 1:
            load_ramp_by_target(target)
            load_ramp_by_target(0.8)
            
        ## PCT
        maranain(dct_pars['dist'])
        marana_large_beam(pct_pars)
        scan_name = 'pct_%dN_' % target
        tomo_by_fscan_dict(scan_name, pct_pars)
    
        ## DCT
        marana_dct(dct_pars)
        scan_name = 'dct_%dN_' % target
        dct_zseries(dct_pars, datasetname = scan_name)
        #tomo_by_fscan_dict(scan_name, dct_pars)

        ## 3DXRD
        ffin(ff_pars)
        scan_name = 'ff_%dN_' % target    
        ff_zseries(ff_pars, scan_name)

    print('Done successfully !!!')
    return
    
def run_tt_only():
    load_targets = [56,]
#    grain_ids = [1, 3 ,55, 246]
    grain_ids = [1, 3, 55, 246]
    #grain_list = load_grains_from_matlab(grain_ids)
    grain_list = load_grains_from_txt(grain_ids, 52) # 0, 44, 48, 52, 56

    print(grain_list)
    for target in load_targets:
        dct_pars = define_dct_pars()
        dct_pars_fine = define_dct_pars_fine()
        pct_pars = define_pct_pars()
        tt_pars = define_tt_pars()
        ff_pars = define_ff_pars()
        sff_pars = define_sff_pars()
        
        # move marana in
        maranain(dct_pars['dist'])
        marana_dct(dct_pars)
        
        scan_name = 'tt_%dN' % target
        grain_list_updated = marana_tt(grain_list, tt_pars, target)

    print('Done successfully !!!')
    return grain_list_updated
   

def marana_tt_test(target = 56):
    sam_dct_pos()
    dct_pars = define_dct_pars()
    # move marana in
    maranain(dct_pars['dist'])
    marana_dct(dct_pars)
    
    grain_ids = [1, 3, 55, 246]
    grain_list = load_grains_from_txt([1,3,246], 52)
    print(grain_list)
    
    tfoh1.set(42,'Be')
    for i in range(3):
        umv(tfy, 14.781)
    
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    umv(s8hg, tt_pars['slit_hg'] + 0.2, s8vg, tt_pars['slit_vg'] + 0.2)
    umv(nfdtx, tt_pars['dist'])

    for i in range(len(grain_list)):
        if grain_list[i]['gr_id'] == 55:
            tt_pars['counter_roi'] = [452, 452, 120, 120]
        if grain_list[i]['gr_id'] == 246:
            tt_pars['counter_roi'] = [472, 472, 100, 100]
        marana3.image.roi = tt_pars['image_roi']
        marana3.roi_counters.set('roi1', tt_pars['counter_roi'])
        ACTIVE_MG.enable('marana3:roi_counters:roi1_avg')    

        newdataset('grain_%04d_%dN_test' % (grain_list[i]['gr_id'], target))
        umv(d3tz, grain_list[i]['d3tz'])
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time, tt_pars['scan_mode'])
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'], tt_pars['scan_mode'])
        print(diffry_start, n_acq_images)
        sleep(1)
        #update_grain_info(grain_list[i], target)
        #fscan2d(diffrz, tt_pars['start_pos'], tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
        #fscan2d(diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, diffrz, tt_pars['start_pos'], tt_pars['diffrz_step'], tt_pars['num_proj'], exp_time, scan_mode=tt_pars['scan_mode'])

    #sam_dct_pos()
    return grain_list




def load_step_sequence(dct_pars, dct_pars_fine, pct_pars, tt_pars, ff_pars, sff_pars, grain_list, target):
    """Performs a loadramp to the new target value and launches PCT, DCT and a series of TT scans at this new target load
    """
    
    
    ## PCT
    #if target > 1:
    #    load_ramp_by_target(target)
    #    load_ramp_by_target(1)
    maranain(dct_pars['dist'])
    marana_large_beam(pct_pars)
    scan_name = 'pct_%dN_' % target
    tomo_by_fscan_dict(scan_name, pct_pars)
    
    
    
    ## DCT
    marana_dct(dct_pars)
    scan_name = 'dct_%dN_' % target
    dct_zseries(dct_pars, datasetname = scan_name)
    #tomo_by_fscan_dict(scan_name, dct_pars)
    
    ## DCT fine scan
    #marana_dct(dct_pars)
    #scan_name = 'dct_%dN_fine' % target
    #dct_zseries(dct_pars_fine, datasetname = scan_name)
    #tomo_by_fscan_dict(scan_name, dct_pars_fine)
    
    '''
    ## TT
    scan_name = 'tt_%dN_' % target
    grain_list = marana_tt(grain_list, tt_pars, target)
    ##frelon16in(4, tt_pars)
    ##grain_list = loop_grains(grain_list, tt_pars, target)
    '''
    
    ## 3DXRD
    ffin(ff_pars)
    scan_name = 'ff_%dN_' % target    
    ff_zseries(ff_pars, scan_name)
    
    
    '''
    ## s-3DXRD
    scan_name = 'sff_%dN' % target
    
    #scan_name = 'sff_' + str(target) + 'N'
    
    scanning_ff()
    sff_zseries(sff_pars, scan_name)
    # prepare for PCT
    #maranain(pct_pars['dist'])
    #marana_large_beam(pct_pars)
    #sct(0.05)
    '''  
    return


def loop_grains(grain_list, tt_pars, target):
    """Loops through a list of Topotomo grains  and performs refinement + TT scan_acquisition
    :param dict grain_list:  list of grain structures containing Topotomo alignment values
    :param dict tt_pars: Topotomo scan parameters
    :param int step: running number of load step in load-sequence  
    """
 
    for i in range(len(grain_list)):
        topotomo_tilt_grain_dict(grain_list[i])
        newdataset('grain_%04d_%dN_' % (grain_list[i]['gr_id'], target))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time)
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time, step=target)
        sleep(1)
        update_grain_info(grain_list[i], target)
        print('values updated in tt_grain record')
        #n_acq_images = 10 
        #diffry_start = grain_list[i]['diffry'] - 0.25
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    sam_dct_pos()
    return grain_list

def marana_tt(grain_list, tt_pars, target):
    tfoh1.set(24,'Be')
    #for i in range(3):
    #    umv(tfy, 14.781)
    
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    umv(s8hg, tt_pars['slit_hg'] + 0.1, s8vg, tt_pars['slit_vg'] + 0.1)
    umv(nfdtx, tt_pars['dist'])
    marana3.image.roi = tt_pars['image_roi']
    marana3.roi_counters.set('roi1', tt_pars['counter_roi'])
    # ACTIVE_MG.enable('marana3:roi_counters:roi1_avg')
    for i in range(len(grain_list)): 
        newdataset('grain_%04d' % (grain_list[i]['gr_id']))
        umv(d3tz, grain_list[i]['d3tz'])
        exp_time = tt_pars['exp_time']  # / grain_list[i]['int_factor']
        #grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time, tt_pars['scan_mode'])
        #diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'], tt_pars['scan_mode'])
        sleep(1)
        #update_grain_info(grain_list[i], target)
        fscan2d(diffrz, tt_pars['start_pos'], tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
        #fscan2d(diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, diffrz, tt_pars['start_pos'], tt_pars['diffrz_step'], tt_pars['num_proj'], exp_time, scan_mode=tt_pars['scan_mode'])
    #sam_dct_pos()
    return grain_list

#grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time, tt_pars['scan_mode'])        
def refine_tt_alignment(tt_grain, ang_step=0.05, search_range=1, exp_time=0.5, scan_mode='TIME'):
    """Refine the topotomo samrx, samry alignment.
    
    This function runs 4 base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use (in degrees).
    :param float search_range: the base tilt angular search range in degrees.
    """
     # half base tilt range in degree
    n_search_images = 2 * search_range / ang_step
    #data_dir = '/data/id11/wolfgang/AlLi_mar/AlLi_mar_dct3'
    #sample_name = 'AlLi_mar_dct3'
    
    # open a script to gather all acquisition commands
    print(tt_grain)
    gid = tt_grain['gr_id']
    print('find range for grain %d\n' % gid)
    # check that diffry is negative (Bragg alignment)
    #if tt_grain['diffry'] > 0:
    #    raise(ValueError('diffry value should be negative, got %.3f, please check your data' % tt_grain['diffry']))
        
    # align our grain
    topotomo_tilt_grain_dict(tt_grain)

    # define the ROI automaticalatty as [710, 710, 500, 500]
    #frelon16.image.roi = [0, 0, 1920, 1920]
    #ct(0.1)
    #if 'roi' in tt_grain:
    #    marana3.roi_counters.set('roi1', tt_grain['roi'])
    #else:
    #    marana3.roi_counters.set('roi1', [200, 200, 400, 400])
    #ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    
    # first scan at 0 deg
    print(tt_grain['diffry'] - search_range, ang_step, n_search_images)
    umvct(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_000, end_angle_000, cen_angle_000 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # second scan at 180 deg
    umv(diffrz, 180)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_180, end_angle_180, cen_angle_180 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samry by half the difference
    samry_offset = 0.5 * (cen_angle_000 - cen_angle_180)
    print("moving samry by %.3f, final position: %.3f" % (samry_offset, tt_grain['samry'] + samry_offset))
    umvr(samry, samry_offset)

    # third scan at 270 deg
    umv(diffrz, 270)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_270, end_angle_270, cen_angle_270 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # fourth scan at 90 deg
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_090, end_angle_090, cen_angle_090 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samrx by half the difference
    samrx_offset = 0.5 * (cen_angle_090 - cen_angle_270)
    print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    #samrx_offset =  tt_grain['diffry'] - cen_angle_090
    #print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    umvr(samrx, samrx_offset)

    # update values in tt_grain
    tt_grain['samrx'] += samrx_offset
    tt_grain['samry'] += samry_offset
    return tt_grain

def find_range(tt_grain, ang_step=0.05, search_range=0.3, exp_time=0.1, scan_mode='CAMERA', step=1):
    """Find the topotomo angular range.
    
    This function runs two base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition. Note that this function 
    assumes that the grain has been aligned already.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use.
    :param float search_range: the base tilt angular search range in degrees.
    """
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/id11/wolfgang/AlLi_mar/'
    sample_name = 'AlLi_mar_dct3'
    
    # open a script to gather all acquisition commands
    gid = tt_grain['gr_id']
    #cmd_path = os.path.join(data_dir, 'tt_acq_grain_%d_step_%d.py' % (gid, step))
    #f = open(cmd_path, 'w')
    #f.write("def tt_acq():\n\n")
    #f.write("    # activate ROI\n")
    #f.write("    frelon16.image.roi = [561, 561, 800, 800]\n")
    #f.write("    frelon16.roi_counters.set('roi1', [200, 200, 400, 400])\n")
    #f.write("    ct(0.1)\n")
    #f.write("    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')\n\n")
    
    print('find range for grain %d\n' % gid)
    
    # see if we need to create a new dataset or not
    #dataset = 'grain_%04d_checkrange_step_%d' % (gid, step)
    #newdataset(dataset)

    # define the ROI automaticalatty as [710, 710, 500, 500]
    #frelon16.roi_counters.set('roi1', [710, 710, 500, 500])
    #ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    ACTIVE_MG.enable('marana3:roi_counters:roi1_avg')
    # first scan at 90 deg
    datasetname = SCAN_SAVING.dataset_name
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_90, end_angle_90 = get_limits(fscan.get_data())
    #start_angle_90, end_angle_90 = get_limits_by_seg(fscan.get_data(), data_dir, sample_name, datasetname, step, scan_fold_ind = 5)
    diffry_range_90 = max(tt_grain['diffry'] - start_angle_90, end_angle_90 - tt_grain['diffry'])

    # second scan at 0 deg
    umv(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode= scan_mode)
    start_angle_00, end_angle_00 = get_limits(fscan.get_data())
    #start_angle_00, end_angle_00 = get_limits_by_seg(fscan.get_data(), data_dir, sample_name, datasetname, step, scan_fold_ind = 6)
    diffry_range_00 = max(tt_grain['diffry'] - start_angle_00, end_angle_00 - tt_grain['diffry'])

    # use the upper range value to define topotomo bounds
    print('diffry_range_00=%.3f - diffry_range_90=%.3f' % (diffry_range_00, diffry_range_90))
    diffry_range = max(diffry_range_00, diffry_range_90)

    # now find out the largest range to cover the grain
    diffry_start = tt_grain['diffry'] - diffry_range
    diffry_end = tt_grain['diffry'] + diffry_range
    n_acq_images = (diffry_end - diffry_start) / ang_step

    # write out the topotomo command for bliss
    #f.write("    # create a new data set\n")
    #f.write("    newdataset('grain_%04d_step_%d')\n\n" % (gid, step))
    #f.write("    # acquisition for grain %d\n" % gid)
    #f.write("    user.topotomo_tilt_grain(%d)\n" % gid)
    #f.write("    umv(samrx, %.3f)\n" % tt_grain['samrx'])
    #f.write("    umv(samry, %.3f)\n" % tt_grain['samry'])
    #f.write("    fscan2d(diffrz, 0, 10, 36, diffry, %.3f, %.3f, %d, 0.1, scan_mode=tt_pars['scan_mode'])\n\n" % (diffry_start, ang_step, n_acq_images))
    #f.close()
    return diffry_start, n_acq_images

def get_limits(scan_data, thres=0.1, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry = scan_data['diffry']
    # work out the angular step
    ang_step = diffry[1] - diffry[0]
    intensity = scan_data['marana3:roi_counters:roi1_avg']
    # background correction
    bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) -1
    start_angle = diffry[start_pos] - padding * ang_step
    end_angle = diffry[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, number of images={}'.format(start_angle, end_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle
    
def get_limits_and_weighted_cen_angle(scan_data, thres=0.1, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry_vals = scan_data['diffry']
    # work out the angular step
    ang_step = diffry_vals[1] - diffry_vals[0]
    intensity = scan_data['marana3:roi_counters:roi1_avg']
    # background correction
    #bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    bg = np.min(intensity)
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) - 1
    end_pos = max(start_pos + 1, end_pos)
    weighted_cen_pos = 0;
    for i_pos in range(start_pos, end_pos, 1):
        weighted_cen_pos += i_pos * intensity[i_pos]
    weighted_cen_pos = weighted_cen_pos / np.sum(intensity[start_pos:end_pos])
    # compute the angle using linear interpolation
    weighted_cen_angle = (diffry_vals[start_pos] * (end_pos - weighted_cen_pos) + diffry_vals[end_pos] * (weighted_cen_pos - start_pos)) / (end_pos - start_pos)
    start_angle = diffry_vals[start_pos] - padding * ang_step
    end_angle = diffry_vals[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, weighted_cen_angle={}, number of images={}'.format(start_angle, end_angle, weighted_cen_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle, weighted_cen_angle

import h5py
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, TextBox
from matplotlib.colors import ListedColormap
from scipy.ndimage import label
""" The object is used for the UI of determining the diffry limits in TT.
(used by get_limits_by_seg(...)) """
class DiffryRangeDeterminator:
    def __init__(self, axs, fig_handle, image_stack, downsampling_ratio = 32, padding = 10, thres_log = 1):
        self.padding = padding
        self.ax = axs
        self.fig = fig_handle
        self.dsampling = downsampling_ratio
        self.image_stack = image_stack
        self.slices, rows, cols = image_stack.shape
        image_stack = self.downsample_image_xy()
        self.ds_image_stack = image_stack
        self.grain_mask = 2 * (image_stack > 10**thres_log)
        self.threshold_log = thres_log
        self.threshold_former = 0
        self.selected_labels = []
        self.slice_ind = 0
        self.im = [0, 0]
        self.fresh_image()
        self.updateSlice(1)
    def downsample_image_xy(self):
        slices, rows, cols = self.image_stack.shape
        row_pads = rows % self.dsampling
        col_pads = cols % self.dsampling
        row_padshalf = row_pads // 2
        col_padshalf = col_pads // 2
        dsimgstack = np.pad(self.image_stack, ((0, 0), (row_padshalf, row_pads - row_padshalf), (col_padshalf, col_pads - col_padshalf)))
        dsimgstack = dsimgstack.reshape((slices, (rows + row_pads) // self.dsampling, self.dsampling, (cols + col_pads) // self.dsampling, self.dsampling))
        dsimgstack = np.sum(np.sum(dsimgstack, 4), 2)
        return dsimgstack
    def updateSlice(self, slice_ind):
        slice_ind = int(slice_ind - 1)
        self.im[0].set_data(self.image_stack[slice_ind, :, :])
        self.im[1].set_data(self.grain_mask[slice_ind, :, :])
        self.ax[0].set_xlabel('slice %d' % (slice_ind + 1))
        self.slice_ind = slice_ind
        self.im[0].axes.figure.canvas.draw()
        self.im[1].axes.figure.canvas.draw()
    def updateThres(self, thres):
        self.threshold_log = thres
        thres = 10**thres
        self.grain_mask = 2 * (self.ds_image_stack > thres)
        self.im[1].set_data(self.grain_mask[self.slice_ind, :, :])
        self.ax[1].set_xlabel('threshold %.6s' % thres)
        self.im[1].axes.figure.canvas.draw()
    def rightClickSelSpots(self, event):
        if event.button != 3:
            return
        try:
            x_ind = int(event.xdata)
            y_ind = int(event.ydata)
        except:
            return
        x_ind = max(x_ind, 0)
        y_ind = max(y_ind, 0)
        if self.grain_mask[self.slice_ind, y_ind, x_ind] > 0:
            print([x_ind, y_ind, self.slice_ind])
            if self.threshold_log != self.threshold_former:
                self.reset_labels()
                self.threshold_former = self.threshold_log
            new_label = self.labeled_mask[self.slice_ind, y_ind, x_ind]
            if new_label in self.selected_labels:
                self.selected_labels.remove(new_label)
                self.grain_mask = np.uint(self.grain_mask) + np.uint(self.labeled_mask == new_label)
            else:
                self.selected_labels.append(new_label)
                self.grain_mask = np.uint(self.grain_mask) - np.uint(self.labeled_mask == new_label)
            self.im[1].set_data(self.grain_mask[self.slice_ind, :, :])
            self.im[1].axes.figure.canvas.draw()
    def confirmandclose(self, event):
        plt.close(self.fig)
    def read_padding(self, text):
        self.padding = int(text)
    def change_downsampling(self, text):
        self.dsampling = int(text)
        self.ds_image_stack = self.downsample_image_xy()
        self.updateThres(self.threshold_log)
        self.reset_labels()
        self.fresh_image()
    def reset_labels(self):
        self.labeled_mask, nof_features = label(self.grain_mask)
        self.selected_labels = []
    def fresh_image(self):
        self.im[0] = self.ax[0].imshow(self.image_stack[self.slice_ind, :, :])
        cmap_3color = [[0., 0., 0.], [0.5, 0., 0.], [1., 1., 1.]]
        cmap_3color = ListedColormap(cmap_3color)
        self.im[1] = self.ax[1].imshow(self.grain_mask[self.slice_ind, :, :], cmap = cmap_3color)
    def getSliceBounds(self):
        if len(self.selected_labels):
            slice_bounds = np.sum(np.sum(self.labeled_mask == self.selected_labels[0], 2), 1)
            for ii_label in range(1, len(self.selected_labels), 1):
                slice_bounds += np.sum(np.sum(self.labeled_mask == self.selected_labels[ii_label], 2), 1)
            slice_bounds = np.nonzero(slice_bounds)
            return [slice_bounds[0][0], slice_bounds[0][-1]]
        else:
            return []
""" Determine the limits of diffry according to the mask of the spot """
def get_limits_by_seg(scan_data, data_dir, sample_name, datasetname, step, scan_fold_ind = 5, thres=0.1):
    diffry_array = scan_data['diffry']
    h5file_path = os.path.join(data_dir, sample_name, sample_name + '_' + datasetname, sample_name + '_' + datasetname + '.h5')
    scan_fold_ind = str(scan_fold_ind)
    flag_readfile = 'y'
    while(flag_readfile != 'n'):
        scan_fold_ind = input('\nThe index of scan: [' + scan_fold_ind + '] ') or scan_fold_ind
        h5file_path = input('The path of data: [' + h5file_path + '] ') or h5file_path
        flag_confirm = input('Path: ' + h5file_path + '\nScan index: ' + scan_fold_ind + '\nAre you satisfied with the path and the scan index? y/n [y] ')
        if flag_confirm == 'n':
            continue
        try:
            img = h5py.File(h5file_path, 'r')
            keys_measurement = list(img[str(scan_fold_ind)+'.1']['measurement'].keys())
            if 'frelon16' in keys_measurement:
                camera_name = 'frelon16'
            elif 'marana3' in keys_measurement:
                camera_name = 'marana3'
            img = img[str(scan_fold_ind)+'.1']['measurement'][camera_name]
            flag_readfile = 'n'
        except:
            flag_readfile = input('Failed to read file. Try again? y/n [y]')
            
    bg = np.median(img)
    img = img - bg
    nof_slices, nof_rows, nof_cols = img.shape
    fig, axs = plt.subplots(1, 2)
    range_determinator = DiffryRangeDeterminator(axs, fig, img, downsampling_ratio = 32, padding = 10, thres_log = np.log10(thres * img.max()))
    ax_slice = plt.axes([0.2, 0.02, 0.6, 0.03])
    simgslice = Slider(
    ax_slice, "Slice", 1, nof_slices,
    valinit=1, valstep=1)
    simgslice.on_changed(range_determinator.updateSlice)
    ax_thres = plt.axes([0.05, 0.2, 0.03, 0.6])
    simgthres = Slider(
    ax_thres, "Threshold", 0, np.log10(range_determinator.ds_image_stack.max()),
    valinit=0, orientation = 'vertical')
    simgthres.on_changed(range_determinator.updateThres)
    fig.canvas.mpl_connect('button_press_event', range_determinator.rightClickSelSpots)
    dstextboxax = plt.axes([0.6, 0.85, 0.2, 0.075])
    dstextbox = TextBox(dstextboxax, 'Downsampling', initial=str(range_determinator.dsampling))
    dstextbox.on_submit(range_determinator.change_downsampling)
    paddingtextboxax = plt.axes([0.1, 0.85, 0.2, 0.075])
    paddingtextbox = TextBox(paddingtextboxax, 'Padding', initial=str(range_determinator.padding))
    paddingtextbox.on_submit(range_determinator.read_padding)
    confirmax = plt.axes([0.8, 0.025, 0.1, 0.04])
    button_confirm = Button(confirmax, 'Ok', hovercolor='0.975')
    button_confirm.on_clicked(range_determinator.confirmandclose)
    plt.show()
    slice_bounds = range_determinator.getSliceBounds()
    if not len(slice_bounds):
        flag_continue = input('No limit is determined. Would you like to re-run the code? y/n [y]: ')
        start_angle = 'nan'
        end_angle = 'nan'
    else:
        padding = range_determinator.padding
        start_pos = slice_bounds[0]
        end_pos = slice_bounds[1]
        ang_step = diffry_array[1] - diffry_array[0]
        start_angle = diffry_array[start_pos] - padding * ang_step
        end_angle = diffry_array[end_pos] + padding * ang_step
        print('start_angle={}, end angle={}, number of images={} (padding={}, length of mask={})'.format(start_angle, end_angle, int((end_angle - start_angle) / ang_step + 1), padding, slice_bounds[1] - slice_bounds[0] + 1))
        flag_continue = input('Would you like to re-run the code? y/n [y]: ')
    if flag_continue != 'n':
        start_angle, end_angle = get_limits_by_seg(scan_data, data_dir, sample_name, step, scan_fold_ind)
    return start_angle, end_angle



import json
import time
import os

class NumpyEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self,obj)


def writedictionaryfile(dictname,filename):
    """to write dictionary to file"""
    datatowrite = json.dumps(dictname)
    with open('%s'%filename, 'w') as f:
        f.write(datatowrite)
    print("Dictionary written as %s"%filename)
    return

def appenddictionaryfile(dictname,filename):
    """to write dictionary to file"""
    if os.path.exists('%s'%filename):
        with open('%s'%filename, 'r+') as f:
            data_to_write = [json.load(f)]
            dictname['time'] = str(time.ctime())
            data_to_write.append(dictname)
            f.seek(0)
            json.dump(data_to_write, f)
    else:
        newdict = dictname
        newdict['time'] = str(time.ctime())
        with open('%s'%filename, 'w') as f:
            json.dump(newdict, f)
            #f.write(datatowrite)
    print("Dictionary appended to %s"%filename)
    return

def readdictionaryfile(filename):
    """to read dictionary from file, returns dictionary"""
    with open('%s'%filename, 'r') as f:
        readdata = [json.loads(f.read())]
    print("Dictionary read from %s"%filename)
    return readdata[-1]


def update_grain_info_old(grain, target):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    writedictionaryfile(grain,'%s/%s_g%s_%dN'%(basedir,samplename,grain['gr_id'],target))
    #appenddictionaryfile(grain,'%s_g%s_history'%(SCAN_SAVING.collection_name,grain['gr_id']))
    return
    
def update_grain_info(grain, target):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s/RAW_DATA/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,SCAN_SAVING.date,samplename)
    filename = '%s/%s_g%s_%dN.txt'%(basedir,samplename,grain['gr_id'],target)
    print(filename)
    print('Write motor positions for grain id = ', grain['gr_id'])
    with open(filename, 'w') as f:
        for key, value in grain.items():
            f.write('%s:%s\n' % (key, value))
    return

def get_json_grain_info(grain_number):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    return readdictionaryfile('%s/%s_g%s'%(basedir,samplename,str(grain_number)))

def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'], s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umv(s8vg, dct_pars['slit_vg'] + 0.05, s8hg, dct_pars['slit_hg'] + 0.05)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])
    
def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.05, s8hg, pars['slit_hg'] + 0.05)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def tomo_by_finterlaced_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.2, s8hg, pars['slit_hg'] + 0.2)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        #fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)

def ref_scan_samy(exp_time, nref, ystep, omega, scanmode):
    umvr(samy, ystep)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umvr(samy, -ystep)
    
def ref_scan_difftz(exp_time, nref, zstep, omega, scanmode):
    difftz0 = difftz.position
    umv(difftz, difftz0 + zstep)
    print("fscan(diffrz, %6.2f, 0.1, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.1, nref, exp_time, scan_mode = scanmode)
    umv(difftz, difftz0)
    
class LoadLoop:
    def __init__(self):
        self._task = None
        self._stop = False
        self._sleep_time = 1
        self._filepath = None
        
    def start(self,target,loadstep,filepath,sleep_time=1.,time_step=0.5):
        self._filepath = filepath
        if self._task:
            self._stop = True
            self._task.get()

        self._sleep_time = sleep_time
        self._stop = False
        self._task = gevent.spawn(self._run,target,loadstep,time_step)

    def stop(self):
        self._stop = True
        if self._task: 
           self._task.get()

    def _run(self,target,loadstep,time_step):
        newdataset('loadramp_%d' %  loadstep)
        with open(self._filepath,'a') as f:
            while not self._stop:
                load_ramp_by_target(target,0,loadstep,time_step,to_file=f)
                gevent.sleep(self._sleep_time)

def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')
    
def dct_tiltscan(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    difftz_offset = -difftz.position
    samrx_offset = samrx.position
    samry_offset = samry.position
    samtx_offset = samtx.position
    samty_offset = samty.position
    samrx_tilts = np.linspace(-dct_pars['samrx_range'], dct_pars['samrx_range'], dct_pars['num_tiltx'])
    samry_tilts = np.linspace(-dct_pars['samry_range'], dct_pars['samry_range'], dct_pars['num_tilty'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        for tiltx in samrx_tilts:
            print('moving samrx to:' + str(tiltx))
            umv(samrx, samrx_offset + tiltx)
            umv(samty, samty_offset + difftz_offset * np.tan(np.deg2rad(tiltx)))
            for tilty in samry_tilts:
                print('moving samry to:' + str(tilty))
                umv(samry, samry_offset + tilty)
                umv(samtx, samtx_offset - difftz_offset * np.tan(np.deg2rad(tilty)))
                #sct(0.02)
                umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
                tomo_by_fscan_dict(datasetname + str(iter_i + 1) + '_' + str(tiltx) + '_' + str(tilty), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'], samrx, samrx_offset, samry, samry_offset, samtx, samtx_offset, samty, samty_offset)
    print('DCT Succeed')
    return('Succeed')
    
def ff_zseries(ff_pars, datasetname = 'ff'):
    #user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    #umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    #user.cpm18_goto(cpm18_tuned)
    #umv(u22, u22_tuned)
    print('ff_zseries succeed')
    return('Succeed')

def sff_zseries(sff_pars, datasetname = 'sff'):
    shift_step_size = abs(sff_pars['shift_step_size'])
    if(sff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = sff_pars['samtz_cen'] - (sff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(sff_pars['samtz_cen']))
    if(offset_samtz_pos < sff_pars['zmot'].low_limit or offset_samtz_pos + (sff_pars['nof_shifts'] - 1) * shift_step_size > sff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    umv(s7vg,0.01, s7hg, 0.01, s8vg, 0.01, s8hg, 0.01)
    tfoh1.set(8,'Be')
    tfoh1.set(32,'Al')
    for ii in range(3):
        umv(tfz, -0.415, tfy, 14.743)
    umvct(attrz,-9, atty,0)
    for iter_i in range(int(sff_pars['nof_shifts'])):
    #for iter_i in np.arange(6,21,1):
        umv(sff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        scan_name = (datasetname + str(iter_i + 1))
        if iter_i == 0:
            tdxrd_forward360_interrupted(0.01+14.581,0.3,
                  scan_name,
                  ystep = 0.01,
                  ycen = diffty_offset,
                  ymin = 0.,
                  rstep= sff_pars['step_size'],
                  expotime = sff_pars['exp_time'],
                  offset = sff_pars['start_pos']
                  )        
        else:
            tdxrd_forward360(0.3,
                  scan_name,
                  ystep = 0.01,
                  ycen = diffty_offset,
                  ymin = 0.,
                  rstep= sff_pars['step_size'],
                  expotime = sff_pars['exp_time'],
                  offset = sff_pars['start_pos']
                  )
      
    umv(sff_pars['zmot'], sff_pars['samtz_cen'])
    print('sff_zseries succeed')
    return('Succeed')


def tomo_series_cst_load(start, num_scans, target, sleep_time=0, pct_pars=None):
    for i in np.arange(start, start+num_scans):
        tomo.full_turn_scan(str(i))
        load_ramp_by_target(target, 0.05, 1)
	#dct_marana_dict(str(i), pct_pars)
        sleep(sleep_time)
    return

def ftomo_series(scanname, start, num_scans, sleep_time=0, pars=None):
    for i in np.arange(start, start+num_scans):
        newdataset(scanname + str(i))
        umv(diffrz, pars['start_pos']);
        fsh.disable()
        fsh.close()
        print("taking dark images")
        ftimescan(pars['exp_time'], pars['nref'],0)
        fsh.enable()
        if not(i%pars['ref_int']):
            print("taking flat images")
            ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'], pars['scan_mode'])
        else:
            print("skipping flat images - stay where we are")
            ftimescan(pars['exp_time'], 10,0)
        print("taking projections...")
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        print("resetting diffrz to 0")
        umv(shehe, pars['start_pos']+360);
        diffrz.position=pars['start_pos'];
        diffrz.dial = diffrz.position
        sleep(sleep_time)
    return
    
def in_situ_loading(pars, sample_name, voltage_ramprate = 0.01, voltage_target = 120, nscans=1000, last_scan = 0):
    
    if pars['dist'] < 90:
        print('You are dangerously close')
        return
    ACTIVE_MG.enable('stress*')
    print('Activated stress saving')
    stress_regul.plot()
    initialize_stress_regul()
    print('Initialized stress regulation')
    print('Will move nfdtx and slits')
    umv(nfdtx, pars['dist'], s7hg, pars['slit_hg'], s8hg, pars['slit_hg']+0.2, s7vg, pars['slit_vg'], s8vg, pars['slit_vg']+0.2)
    newsample(f'{sample_name}_insitu')
    newdataset('sct')
    sct(pars['exp_time'])
    #take darks
    #print('Taking darks')
    #newdataset('darks')
    #sheh3.close()
    #ftimescan(pars['exp_time'], pars['nref']/10)
    #sheh3.open()
    #print('Took darks')
    #take flats
    #print('Will take flat fields. Moving difftz away')
    #newdataset('flats')
    #umvr(pars['ref_mot'], pars['ref_step'])
    #ftimescan(pars['exp_time'], pars['nref'])
    #print('Took flats. Moving samy back')
    #umvr(pars['ref_mot'], -1 * pars['ref_step'])
    #print('Sleeping for 30s.')
    #sleep(60)
    print('Activating ramprate')
    regul_off(voltage_ramprate, voltage_target)
    #init_load = stress_adc.get_value()
    print('Started voltage ramprate')
    ii = 0
    if last_scan != 0:
        ii = last_scan + 1
    for _ in range(nscans):
        
        newdataset(f'{ii:04}')
        #take flats
        #if ii % 10 == 0:   
        #acquire projections
        print('Will acquire projections')
        if ii % 2 == 0:
            fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        else:
            fscan(diffrz, pars['start_pos'] + 360, -1 * pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        
        if stress_adc.get_value() <= 1:
            regul_off(0, stress.position)
            print('Your sample probably broke. Finishing.')
            break
        ii += 1        
    print('Finished')         
    


def tdxrd_boxscan(scanname, ff_pars):
    print("using slit_vg %f and slit_hg %f" % (ff_pars['slit_vg'], ff_pars['slit_hg']))
    umv(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.2, s8hg, ff_pars['slit_hg'] + 0.2)
    newdataset(scanname)
    finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])

def tdxrd_pointscan(scanname, sff_pars, grain):
    user.cpm18_goto(cpm18_tuned + sff_pars['cpm18_detune'])
    umv(s7vg, 0.5, s7hg, 0.5, s8vg, 0.05, s8hg, 0.05) 
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, bigy_in)
    #umv(samtx, grain['samtx'], samty, grain['samty'], samtz, grain['samtz'])
    stage_diffty_zero = diffty_offset
    scanstarty= -0.2  # in mm
    scanendy=0.201 
    scanstep=0.01
    
    newdataset(scanname)

    for ty in np.arange(scanstarty,scanendy,scanstep):
        umv(diffty, stage_diffty_zero+ty)
        print(f"Scan pos: y: {ty} ")
        finterlaced( diffrz, sff_pars['start_pos'], acq_size = sff_pars['step_size'], npoints = sff_pars['num_proj'], acq_time = sff_pars['exp_time'], mode = sff_pars['mode'])

    umv(diffrz,0)      
    umv(diffty, stage_diffty_zero)
    umv(bigy, bigy_out)
    diffty_airpad.off()
    user.cpm18_goto(cpm18_tuned)
    print("All done!")
 
def update_flat():
    image_corr.take_dark()
    image_corr.take_flat()
    image_corr.dark_on()
    image_corr.flat_on()

def flat_on():
    image_corr.dark_on()
    image_corr.flat_on()

def flat_off():
    image_corr.dark_off()
    image_corr.flat_off()
    
##############DEFINED BY PEDRO IN 02/09/2022###################

def acquire_pca(dset_name, ndarks = 500, nflats = 1000, exp_time = 0.04):

    newdataset(dset_name)
    umvr(samy, -4)
    sheh3.open()
    #take flats
    ftimescan(exp_time, nflats)
    sheh3.close()
    #take darks
    ftimescan(exp_time, ndarks)
    umvr(samy, 4)
    sheh3.open()
    ct(exp_time)
    
def acquire_pca_tseries(dset_name, nacq = 10, sleep_time = 60, ndarks = 500, nflats = 1000, exp_time = 0.04):

    newdataset(dset_name)
    
    for _ in range(nacq):
		
	    sheh3.open()
	#take flats
	    ftimescan(exp_time, nflats)
	    sheh3.close()
	#take darks
	    ftimescan(exp_time, ndarks)
	    sleep(sleep_time)
    
    sheh3.open()
    ct(exp_time)
    
def g8_s1():
    
    newsample('g8_s1')
    sct(0.25)
    dists = [80, 100, 120, 150]
    
    for dist in dists:
        newdataset(f'pct_{dist}mm_corr')
        umvct(nfdtx, dist)
        fulltomo.full_turn_scan(f'pct_{dist}mm_corr', start_pos = -5)
        
    sheh3.close()
    
    
    
    
def refine_tt_alignment_int(tt_grain, ang_step=0.05, search_range=1, exp_time=0.5, scan_mode='TIME'):
    """Refine the topotomo samrx, samry alignments with interactive roi selection
    
    This function runs 4 base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use (in degrees).
    :param float search_range: the base tilt angular search range in degrees.
    """
     # half base tilt range in degree
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/id11/wolfgang/AlLi_mar/'
    sample_name = 'AlLi_mar'
    
    gid = tt_grain['gr_id']
    print('find range for grain %d\n' % gid)
    
        
    # align our grain
    topotomo_tilt_grain_dict(tt_grain)
    
    # first scan at 0 deg
    umvct(diffrz, 0)
    if 'roi' in tt_grain:
        marana3.roi_counters.set('roi1', tt_grain['roi'])
    else:
	    fscan(diffry, tt_grain['diffry'] - search_range, 2*search_range, 1, exp_time * search_range / ang_step, scan_mode = scan_mode)
	    edit_roi_counters(marana3)
    ACTIVE_MG.enable('marana3:roi_counters:roi1_avg')
    
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_000, end_angle_000, cen_angle_000 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # second scan at 180 deg
    umv(diffrz, 180)
    if 'roi' in tt_grain:
        marana3.roi_counters.set('roi1', tt_grain['roi'])
    else:
	    fscan(diffry, tt_grain['diffry'] - search_range, 2*search_range, 1, exp_time * search_range / ang_step, scan_mode = scan_mode)
	    edit_roi_counters(marana3)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_180, end_angle_180, cen_angle_180 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samry by half the difference
    samry_offset = 0.5 * (cen_angle_000 - cen_angle_180)
    print("moving samry by %.3f, final position: %.3f" % (samry_offset, tt_grain['samry'] + samry_offset))
    umvr(samry, samry_offset)

    # third scan at 270 deg
    umv(diffrz, 270)
    if 'roi' in tt_grain:
        marana3.roi_counters.set('roi1', tt_grain['roi'])
    else:
	    fscan(diffry, tt_grain['diffry'] - search_range, 2*search_range, 1, exp_time * search_range / ang_step, scan_mode = scan_mode)
	    edit_roi_counters(marana3)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_270, end_angle_270, cen_angle_270 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # fourth scan at 90 deg
    umv(diffrz, 90)
    if 'roi' in tt_grain:
        marana3.roi_counters.set('roi1', tt_grain['roi'])
    else:
	    fscan(diffry, tt_grain['diffry'] - search_range, 2*search_range, 1, exp_time * search_range / ang_step, scan_mode = scan_mode)
	    edit_roi_counters(marana3)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_090, end_angle_090, cen_angle_090 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samrx by half the difference
    samrx_offset = 0.5 * (cen_angle_090 - cen_angle_270)
    print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    #samrx_offset =  tt_grain['diffry'] - cen_angle_090
    #print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    umvr(samrx, samrx_offset)

    # update values in tt_grain
    tt_grain['samrx'] += samrx_offset
    tt_grain['samry'] += samry_offset
    tt_grain['roi'] = marana3.roi_counters['roi1'].get_params()
    return tt_grain
    
    
def clicked_pos(x,y):
     c2mplot = flint().get_live_plot('default-curve')
     print("Select position in flint")
     pos = c2mplot.select_points(1)
     return pos[0][0]
    
###############################################################
# interrupted at diffty = 14.5899
def tdxrd_forward360_interrupted(y_start,ymax,
          datasetname,
          ystep = 0.01,
          ycen = 14.581,
          ymin = 0.,
          rstep=0.8,
          expotime=0.08,
          offset = 0
          ):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ycen = central position 
    ymin = low y values to skip, for restarting from a crash
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.tdxrd_forward(0.5, 'toto', ystep=0.1, ycen=14.65)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 11:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    # ny = int( ymax // ystep )
    ny = int( (ymax+ycen-y_start) // ystep )
    print(ny)
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( y_start, ylim+ycen, ny+1 )

    i = -1
    #
    num_proj = 360 / rstep
    diffty_airpad.on( 60*24*5 )
    with cleanup.cleanup( diffrz, restore_list=(cleanup.axis.POS,) ):
        while i < len(ypos)-1:
            i += 1        
            if ypos[i] < ymin: # skip podctsitions already done
                continue
            pause_for_refill( 25 )  # time for one scan
            umv( diffty, ypos[i] )
            if (i % 2) == 0: # forwards
                 finterlaced( diffrz,   0 + offset,  rstep, num_proj, expotime, mode='FORWARD' )
            else:
                 finterlaced( diffrz, 720 + offset, -rstep, num_proj, expotime, mode='FORWARD' )
    umv(diffty, ycen)
    diffty_airpad.off()
    
        
###############################################################
def tdxrd_forward360(ymax,
          datasetname,
          ystep = 0.01,
          ycen = 14.581,
          ymin = 0.,
          rstep=0.8,
          expotime=0.08,
          offset = 0
          ):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ycen = central position 
    ymin = low y values to skip, for restarting from a crash
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.tdxrd_forward(0.5, 'toto', ystep=0.1, ycen=14.65)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 11:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 ) + ycen
    i = -1
    #
    num_proj = 360 / rstep
    diffty_airpad.on( 60*24*5 )
    with cleanup.cleanup( diffrz, restore_list=(cleanup.axis.POS,) ):
        while i < len(ypos)-1:
            i += 1        
            if ypos[i] < ymin: # skip positions already done
                continue
            pause_for_refill( 25 )  # time for one scan
            umv( diffty, ypos[i] )
            if (i % 2) == 0: # forwards
                 finterlaced( diffrz,   0 + offset,  rstep, num_proj, expotime, mode='FORWARD' )
            else:
                 finterlaced( diffrz, 720 + offset, -rstep, num_proj, expotime, mode='FORWARD' )
    umv(diffty, ycen)
    diffty_airpad.off()

def anneal_cycle(pct_pars, dct_pars, time, step, Temp=465, tol=5):
    sheh3.open()
    nanodac3.setpoint = Temp
    while np.abs(nanodac3_temp.read() - Temp) > tol:
        print("waiting for furnace to reach setpoint")
        sleep(30)
    print("Switch to phase contrast configuration")
    marana_pct(pct_pars)
    print("moving down furnance...")
    umv(furnace_z, 0)
    print(f'annealing for {time} seconds')
    sleep(time)
    print('moving furnace out')
    umv(furnace_z, furnace_out)
    print('retracting furnace and waiting 5 min to stabilize')
    sleep(300)
    print('collecting phase contrast scan')
    fulltomo.full_turn_scan(f'pct_{Temp}C_{step:02}')
    #print('switching to dct configuration')
    #marana_dct(dct_pars)
    #print('acquiring dct scan...')
    #dct_marana_dict(f'dct_{Temp}C_{step:02}', dct_pars)
    print('annealing cycle finished...')
    
def do_cycles(pct_pars, dct_pars):
    sheh3.open()
    #anneal_cycle(pct_pars, dct_pars, 120, 1, 465)x
    #anneal_cycle(pct_pars, dct_pars, 240, 2, 465)x
    #anneal_cycle(pct_pars, dct_pars, 360, 3, 465)x
    #anneal_cycle(pct_pars, dct_pars, 480, 4, 465)x
    #anneal_cycle(pct_pars, dct_pars, 600, 5, 465)x
    #anneal_cycle(pct_pars, dct_pars, 1200, 6, 465)x
    #anneal_cycle(pct_pars, dct_pars, 1800, 7, 465)x
    #anneal_cycle(pct_pars, dct_pars, 3600, 8, 465)x
    sheh3.close

def thermal_dct_ramp(pct_pars, dct_pars, step, Temp_final, tol=1):
    sheh3.open()
    print("Switch to phase contrast configuration")
    marana_pct(pct_pars)
    print("moving down furnance...")
    umv(furnace_z, 0)
    nanodac3.ramp_speed = 2
    nanodac3.setpoint = Temp_final
    flag = 0
    while flag == 0:
        if np.abs(nanodac3_temp.read() - Temp_final) < tol:
                umv(furnace_z, furnace_out)
                flag = 1
                print('retracting furnace and waiting 5 min to stabilize')
                sleep(300)
                print('collecting phase contrast scan')
                fulltomo.full_turn_scan(f'pct_{Temp_final}C_{step:02}')
                print('switching to dct configuration')
                marana_dct(dct_pars)
                print('acquiring dct scan...')
                dct_marana_dict(f'dct_{Temp_final}C_{step:02}', dct_pars)
                print('annealing cycle finished...')
            
            
            
            
            
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    





