

from bliss.scanning.group import Sequence
from bliss.common.session import get_current_session
import time

def tomo_by_ascan(start, end, nproj, ctime,
                  ystep=1,
                  rot = 'diffrz',
                  ymotor='samty',
                  sleep_time=0.01,
                  ndark=21,
                  nflat=21,
                  sd_dis=100):
    sess = get_current_session().name
    object_names = config.get_config(sess)['config-objects']
    # this session only
    for m in rot, ymotor:
        if m not in object_names:
            print("Not that motor!",m)
            return        
    ymot = config.get(ymotor)
    rot  = config.get(rot)
    scan_info = { 'technique' : {
        'sample_detector_distance' : sd_dis,  # was nfdtx.position
        'sample_detector_distance@units' : 'mm',
        'energy' : 44.0,
        'energy@units' : 'keV',

    }}
    seq = Sequence(title='tomo', scan_info=scan_info)
    print("Created Sequence")
    with seq.sequence_context() as scan_seq:
        print("Enter sequence context")
        
        umv(rot, 0)
        y = ymot.position
        umvr(ymot, ystep )
        sheh3.open()
        time.sleep(5)
        s1 = loopscan(nflat, ctime, sleep_time=sleep_time,
                      name='ref', title='ref',
                      run=False )
        scan_seq.add( s1 )
        print("Added scan to sequence")
        s1.run()
        print("Run scan")
        umv(ymot, y )
        sheh3.close()
        s2 = loopscan(ndark, ctime, sleep_time=sleep_time,
                      title='dark',
                      name='dark',run=False ) 
        scan_seq.add( s2 )
        s2.run()
#        1/0
        sheh3.open()
        time.sleep(5)
        s3 = ascan(rot, start, end, nproj-1, ctime,
                   sleep_time=sleep_time,
                   title='projections',
                   name='projections',run=False )
        scan_seq.add( s3 )
        s3.run()
        y = ymot.position
        umvr(ymot , ystep )
        s4 = loopscan(nflat, ctime,
                      sleep_time=sleep_time,
                      title='refend',
                      name='refend',run=False )
        scan_seq.add( s4 )
        s4.run()
        umv(ymot, y )
        s5 = ascan(rot, end, start, 4, ctime,
                   sleep_time=sleep_time,
                   title='static images',
                   name='checks',run=False )
        scan_seq.add(s5)
        s5.run()
    print("That's all folks!")
    
def tomo_by_fscan(start, end, nproj, ctime,
                  ystep = 1,
                  rot = 'diffrz',
                  ymotor ='samty',
                  scan_mode ='TIME',
                  ndark = 21,
                  nflat = 21):
    sess = get_current_session().name
    object_names = config.get_config(sess)['config-objects']
    # this session only
    for m in rot, ymotor:
        if m not in object_names:
            print("Not that motor!",m)
            return        
    ymot = config.get(ymotor)
    rot  = config.get(rot)
    step = (end - start)/nproj;
    seq = Sequence()
    with seq.sequence_context() as scan_seq:
            # first flats
            umv(rot, 0)
            y = ymot.position
            umvr(ymot, ystep )
            ftimescan(ctime, nflat, scan_mode=scan_mode)
            scan_seq.add( ftimescan.scan )
            #ftimescan.run()
            umv(ymot, y )
            sheh3.close()
            ftimescan(ctime, ndark, scan_mode=scan_mode) 
            scan_seq.add( ftimescan.scan )
            #ftimescan.run()
            sheh3.open()
            time.sleep(5)
            fscan(rot, start, step, nproj, ctime, scan_mode=scan_mode)
            scan_seq.add( fscan.scan )
            #fscan.run()
            umv(rot,0)
            umvr(ymot , ystep )
            ftimescan(ctime, nflat, scan_mode=scan_mode)
            scan_seq.add( ftimescan.scan )
            #ftimescan.run()
            umv(ymot, y )
            #s5 = ascan(rot, end, start, 4, ctime,
            #           name='checks',run=False )
            #scan_seq.add(s5)
            #s5.run()
    print("That's all folks!")
    
    

