

import time, datetime
# from bliss.common.session 
from bliss import current_session
from fscan.musstscope import timescope

def collect( mins=1, h=9, m=59, wait=True):
    sess = current_session.name
    
    if sess == 'optics' :
        t0 = datetime.datetime.now()
        t1 = datetime.datetime(2024,9,30,h,m-1,58,0)
        if wait:
            print("Sleep until",t1)
            time.sleep( (t1-t0).total_seconds() )
        newdataset(sess + time.strftime("%Y%m%dT%Hh%M"))
        
        pico0.auto_range = False
        pico0.range = 0.0021
        timescope( musst_optic, int( mins*60/0.001), 0.001 )
        pico0.auto_range = True

    if sess == 'tdxrd' :
        t0 = datetime.datetime.now()
        t1 = datetime.datetime(2024,9,30,h,m-1,57,0)
        if wait:
            print("Sleep until",t1)
            time.sleep( (t1-t0).total_seconds() )
        pico3.auto_range = False
        pico3.range = 0.0021
        pico4.auto_range = False
        pico4.range = 0.0021
        newdataset(sess + time.strftime("%Y%m%dT%Hh%M"))
        ftimescan( 0.001, int(mins*60/0.001), 0.001)        
        pico3.auto_range = True
        pico4.auto_range = True



