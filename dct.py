import sys
import numpy as np
import time
import os
import json
from scipy.io import loadmat
import gevent
from datetime import datetime

# Import relevant motors from other sessions
ly = config.get('ly')
lfyaw = config.get('lfyaw')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
bigy = config.get('bigy')
u22 = config.get('u22')
cpm18 = config.get('cpm18')
s7hg = config.get('s7hg')
s7vg = config.get('s7vg')
# Detector positioning : becomes d1.IN, d1.OUT, d1.PARK, d2, d3, d4...
d1 = config.get('d1')
d2 = config.get('d2')
d3 = config.get('d3')
d4 = config.get('d4')

# Motor positions - to be checked & updated for each experiment !
samtz_cen = 0
ff_d3_min_x_offset = 200
nf_max_x = 50
ly_in = -2
ly_out = 5
bs_out = -159.36
bs_in = -155.43
bigy_in = 0.3292
bigy_out = 22
diffty_zero = 14.7205
u22_tuned = 8.3
u22_detuned = 10

# Data directory / fscan parfiles
datadir = '/data/visitor/ma5410/id11/'
force_sample_position = False
pct_parfile = datadir + 'pct.pars'
dct_parfile = datadir + 'dct.pars'
ff_parfile = datadir + 'ff.pars'
tt_parfile = datadir + 'tt.pars'

# dct Parameters
dct_pars={}
dct_pars['start_pos'] = -12
dct_pars['step_size'] = 0.01
dct_pars['num_proj'] = 360 / dct_pars['step_size']  
dct_pars['exp_time'] = 0.05
dct_pars['refon'] = dct_pars['num_proj']  
dct_pars['ref_step'] = -1
dct_pars['scan_mode'] = 'CAMERA'
dct_pars['nref'] = 41
dct_pars['ref_mot'] = samy
dct_pars['zmot'] = samtz
dct_pars['slit_hg'] = 0.8
dct_pars['slit_vg'] = 0.6
dct_pars['dist'] = 2
dct_pars['samtz_cen'] = samtz_cen
dct_pars['shift_step_size'] = 0.05
dct_pars['nof_shifts'] = 1
dct_pars['force_sample_position'] = False
dct_pars['fscan_par_file'] = None
dct_pars['nof_Be'] = 0
dct_pars['roi'] = [0,0,2048,2048]

def define_dct_pars(start_pos = dct_pars['start_pos'], step_size = dct_pars['step_size'], num_proj = dct_pars['num_proj'], 
    exp_time = dct_pars['exp_time'], ref_on = dct_pars['refon'], scan_mode = dct_pars['scan_mode'], nref = dct_pars['nref'],
    ref_mot = dct_pars['ref_mot'], zmot = dct_pars['zmot'], slit_hg = dct_pars['slit_hg'], slit_vg = dct_pars['slit_vg'],
    dist = dct_pars['dist']):
    dct_pars={}
    dct_pars['start_pos'] = start_pos
    dct_pars['step_size'] = step_size
    dct_pars['num_proj'] = num_proj
    dct_pars['exp_time'] = exp_time
    dct_pars['refon'] = refon
    dct_pars['ref_step'] = ref_step
    dct_pars['scan_mode'] = scan_mode
    dct_pars['nref'] = nref
    dct_pars['ref_mot'] = ref_mot
    dct_pars['zmot'] = zmot
    dct_pars['slit_hg'] = slit_hg
    dct_pars['slit_vg'] = slip_vg
    dct_pars['dist'] = dist
    dct_pars['samtz_cen'] = samtz_cen
    dct_pars['shift_step_size'] = 0
    dct_pars['nof_shifts'] = 1
    dct_pars['force_sample_position'] = force_sample_position
    dct_pars['fscan_par_file'] = dct_par_file
    return dct_pars


# pct Parameters
pct_pars={}
pct_pars['start_pos'] = -12
pct_pars['step_size'] = 0.2
pct_pars['num_proj'] = 360 / pct_pars['step_size']
pct_pars['exp_time'] = 0.02
pct_pars['refon'] = pct_pars['num_proj']
pct_pars['ref_step'] = -4
pct_pars['scan_mode'] = 'CAMERA'
pct_pars['nref'] = 41
pct_pars['ref_mot'] = samy
pct_pars['zmot'] = samtz
pct_pars['slit_hg'] = 1.2
pct_pars['slit_vg'] = 1.5
pct_pars['dist'] = 100
pct_pars['samtz_cen'] = samtz_cen
pct_pars['shift_step_size'] = 0
pct_pars['nof_shifts'] = 1
pct_pars['force_sample_position'] = False
pct_pars['fscan_par_file'] = pct_parfile # 'pct.pars'
pct_pars['nof_Be'] = 0
pct_pars['roi'] = [682,512,824,1024]

def define_pct_pars(start_pos = pct_pars['start_pos'], step_size = pct_pars['step_size'], num_proj = pct_pars['num_proj'], 
    exp_time = pct_pars['exp_time'], ref_on = pct_pars['refon'], scan_mode = pct_pars['scan_mode'], nref = pct_pars['nref'],
    ref_mot = pct_pars['ref_mot'], zmot = pct_pars['zmot'], slit_hg = pct_pars['slit_hg'], slit_vg = pct_pars['slit_vg'],
    dist = pct_pars['dist'], samtz_cen = pct_pars['samtz_cen'], shift_step_size = pct_pars['shift_step_size'], 
    nof_shifts = pct_pars['nof_shifts']):
    pct_pars={}
    pct_pars['start_pos'] = start_pos
    pct_pars['step_size'] = step_size
    pct_pars['num_proj'] = num_proj
    pct_pars['exp_time'] = exp_time
    pct_pars['refon'] = refon
    pct_pars['ref_step'] = ref_step
    pct_pars['scan_mode'] = scan_mode
    pct_pars['nref'] = nref
    pct_pars['ref_mot'] = ref_mot
    pct_pars['zmot'] = zmot
    pct_pars['slit_hg'] = slit_hg
    pct_pars['slit_vg'] = slip_vg
    pct_pars['dist'] = dist
    pct_pars['samtz_cen'] = samtz_cen
    pct_pars['shift_step_size'] = 0
    pct_pars['nof_shifts'] = 1
    pct_pars['force_sample_position'] = force_sample_position
    pct_pars['fscan_par_file'] = pct_par_file
    return pct_pars
   
# tt Parameters
tt_pars={}
tt_pars['diffrz_step'] = 0.1
tt_pars['num_proj'] = 360 / tt_pars['diffrz_step']
tt_pars['diffry_step'] = 0.05
tt_pars['exp_time'] = 0.1
tt_pars['search_range'] = 0.4
tt_pars['scan_mode'] = 'TIME'
tt_pars['image_roi'] = [0, 0, 1920, 1920] 
tt_pars['counter_roi'] = [760, 760, 400, 400] 
tt_pars['slit_hg'] = 0.7
tt_pars['slit_vg'] = 0.7
tt_pars['dist'] = 11
        
# ff Parameters
ff_pars={}
ff_pars['start_pos'] = -12
ff_pars['step_size'] = 0.1
ff_pars['num_proj'] = 360 / ff_pars['step_size']
ff_pars['exp_time'] = 0.08
ff_pars['slit_hg'] = 0.5
ff_pars['slit_vg'] = 0.08
ff_pars['dist'] = 150
ff_pars['mode'] = 'ZIGZAG'
ff_pars['zmot'] = samtz
ff_pars['samtz_cen'] = samtz_cen
ff_pars['shift_step_size'] = 0.05
ff_pars['nof_shifts'] = 3
ff_pars['cpm18_detune'] = 0.15
ff_pars['force_sample_position'] = False

# sff Parameters    
sff_pars={}
sff_pars['start_pos'] = -12
sff_pars['step_size'] = 1
sff_pars['num_proj'] = 180 / sff_pars['step_size']
sff_pars['exp_time'] = 0.1
sff_pars['slit_hg'] = 0.05
sff_pars['slit_vg'] = 0.05
sff_pars['dist'] = 150
sff_pars['mode'] = 'ZIGZAG'
sff_pars['force_sample_position'] = False

sff_pars['ystart'] = -0.38  # in mm
sff_pars['yend'] = 0.381 
sff_pars['ystep'] = 0.005
#sff_pars['cpm18_detune'] = 0.1

# Detector swapping / positioning macros
def d1in(dist):
    assert dist > d1_min_dist
    sheh3.close()
    ACTIVE_MG.disable("frelon16*","marana*", "frelon3*" )
    ACTIVE_MG.enable("frelon1")
    d2.PARK()
    d3.OUT()
    d4.OUT()
    d1.IN()
    umv(nfdtx, dist)
    print("ready to collect frelon1 data")
    sheh3.open()
    ct(0.02)

def d2in(dist):
    sheh3.close()
    ACTIVE_MG.disable("frelon1*","marana*", "frelon3*" )
    ACTIVE_MG.enable("frelon16")
    d1.PARK()
    d3.OUT()
    d4.PARK()
    umv(nfdtx, dist)
    d2.IN()
    print("ready to collect frelon16 data")
    sheh3.open()
    ct(0.02)

def d3in(dist):
    sheh3.close()
    ACTIVE_MG.disable("frelon1*","frelon16*", "frelon3*" )
    ACTIVE_MG.enable("marana3")
    d1.PARK()
    d2.OUT()
    d4.PARK()
    assert ffdtx1.position > (dist + ff_d3_min_x_offset)
    umv(nfdtx, dist)
    d3.IN()
    print("ready to collect marana3 data")
    sheh3.open()
    ct(0.02)

def d4in(ff_dist):
    if nfdtx.position > nf_max_x:
        print("you first have to move nfdtx to a position < %f" % nf_max_x)
        return   
    sheh3.close()
    ACTIVE_MG.disable("frelon1*","marana*", "frelon16*" )
    ACTIVE_MG.enable("frelon3")
    d1.PARK()
    d2.OUT()
    d3.OUT()
    umv(ffdtx1, ff_dist, ffdtz1, 0)
    print("ready to collect frelon3 data")
    sheh3.open()
    ct(0.02)

# Prepare different scan types
def prepare_dct(dct_pars):
    # move detector
    d3in(dct_pars['dist'])
    marana3.image.roi=dct_pars['roi']
    # prepare beam
    umv(u22, u22_tuned)
    lyout()
    umv(bigy, bigy_out)
    tfoh1.set(dct_pars['nof_Be'], 'Be')
    umv(d1ty, bs_in)
    umv(s7hg, dct_pars['slit_hg'], s7vg, dct_pars['slit_vg'])
    umv(s8hg, dct_pars['slit_hg'] + 0.1, s8vg, dct_pars['slit_vg'] + 0.1)
    #umv(d3_bsty, d3_bsty_in, d3_bstz, d3_bstz_in)
    # prepare sample
    if dct_pars['force_sample_position']:
	    sam_dct_pos.go()
	# load fscan / camera parameters from file
    if not dct_pars['fscan_par_file'] is None:
	    fscan.load_state(dct_pars['fscan_par_file'])
    print("ready to take a DCT data")

def prepare_pct(pct_pars):
    # move detector
    d3in(pct_pars['dist'])
    # prepare beam
    umv(u22, u22_detuned)
    lyout()
    umv(bigy, bigy_out)
    tfoh1.set(0, 'Be')
    umv(s7hg, pct_pars['slit_hg'], s7vg, pct_pars['slit_vg'])
    umv(s8hg, pct_pars['slit_hg'] + 0.1, s8vg, pct_pars['slit_vg'] + 0.1)
    umv(d1ty, bs_out)
    # prepare sample
    if pct_pars['force_sample_position']:
        sam_dct_pos.go()
    # load fscan / camera parameters from file
    if not pct_pars['fscan_par_file'] is None:
        fscan.load_state(pct_pars['fscan_par_file'])

    marana3.image.roi = pct_pars['roi'] 
    print("ready to take a PCT data")	

def prepare_tt(tt_pars):
    # move detector
    d2in(tt_pars['dist'])
    # prepare beam
    lyout()
    tfoh1.set(tt_pars['nof_Be'], 'Be')
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    umv(s8hg, tt_pars['slit_hg'] + 0.1, s8vg, tt_pars['slit_vg'] + 0.1)
    # prepare sample
    if tt_pars['force_position']:
        sam_dct_pos.go()
    # load fscan / camera parameters from file (will also save / load detector configuration)
    if not pct_pars['fscan_par_file'] is None:
        fscan.load_state(tt_pars['fscan_par_file'])
    print("ready to take a Topotomo data")	

def prepare_ff(ff_pars):
    # move detector
    d4in(ff_pars['dist'])
    # prepare beam
    #umv(u22, u22_tuned)
    lyin()
    tfoh1.set(0, 'Be')
    tfoh1.set(0, 'Al')
    umv(s7hg, ff_pars['slit_hg'], s7vg, ff_pars['slit_vg'])
    umv(s8hg, ff_pars['slit_hg'] + 0.1, s8vg, ff_pars['slit_vg'] + 0.1)
    # prepare sample
    if ff_pars['force_sample_position']:
        sam_dct_pos.go()
    # load fscan / camera parameters from file
    # fscan.load_state(ff_pars['fscan_par_file'])
    print("ready to take a FarField data")	

def prepare_sff(sff_pars):
    # move detector
    d4in(sff_pars['dist'])
    # prepare beam
    #umv(u22, u22_tuned)
    lyin()
    umv(bigy, bigy_in)
    tfoh1.set(0, 'Be')
    tfoh1.set(0, 'Al')
    umv(s7hg, 0.5, s7vg, 0.5, s8hg, 0.05, s8vg, 0.05)
    # prepare sample
    if sff_pars['force_sample_position']:
        sam_dct_pos.go()
    # load fscan / camera parameters from file
    # fscan.load_state(ff_pars['fscan_par_file'])
    print("ready to take a FarField data")	

sam_dct_pos = MemPos('sam_dct_pos', diffry, samrx, samry, samtx, samty, samtz)

def update_sam_dct_pos_from_file(sam_dct_pos):
    sam_dct_pos.set(diffry, 0.0)
    sam_dct_pos.set(samrx, samrx_offset)
    sam_dct_pos.set(samry, samry_offset)
    sam_dct_pos.set(samtx, samtx_offset)
    sam_dct_pos.set(samty, samty_offset)
    sam_dct_pos.set(samtz, samtz_offset)
    return sam_dct_pos

def lyin():
    umv(ly,ly_in)

def lyout():
    umv(ly, ly_out)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']

def topotomo_tilt_grain(gr_id):
    """drives diffractometer to grain alignment position
    """
    topotomo_params = read_tt_infos(gr_id)
    print(topotomo_params)
    assert d2.position == 'IN'
    topotomo_tilt_grain_dict(topotomo_params)
    return topotomo_params
   
def topotomo_tilt_grain_dict(topotomo_params, detz = d2tz):
    """drives diffractometer to current grain alignment position
       default detector arm for Topotomo is d2 !
    """
    print(topotomo_params)
    assert d2.position == 'IN'
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(detz, topotomo_params['detz'])
    umv(diffrz, 90) #save position to change tilts for the diffractometer with Nanox
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')


def sff_one_grain(scanname, sff_pars, grain):
    user.cpm18_goto(cpm18_tuned + sff_pars['cpm18_detune'])
    umv(s7vg, 0.5, s7hg, 0.5, s8vg, 0.05, s8hg, 0.05) 
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, bigy_in)
    umv(samtx, grain['samtx'], samty, grain['samty'], samtz, grain['samtz'])
    stage_diffty_zero = diffty_offset

    scanstarty= -0.1  # in mm
    scanendy=0.101
    scanstepy=0.01
    
    scanstartz= -0.15  # in mm
    scanendz=0.1501 
    scanstepz=0.01    

    umvr(samtz, scanstartz)
    z_scans_number = (scanendz - scanstartz) / scanstepz
    newdataset(scanname)
    for i in range(int(z_scans_number)):
        umvr(samtz, scanstepz)
        for ty in np.arange(scanstarty,scanendy,scanstepy):
            umv(diffty, stage_diffty_zero+ty)
            print(f"Scan pos: y: {ty} couche {i}")
            finterlaced( diffrz, sff_pars['start_pos'], acq_size = sff_pars['step_size'], npoints = sff_pars['num_proj'], acq_time = sff_pars['exp_time'], mode = sff_pars['mode'])

    umv(diffrz,0)      
    umv(diffty, stage_diffty_zero)
    umv(bigy, bigy_out)
    diffty_airpad.off()
    user.cpm18_goto(cpm18_tuned)
    
    umv(samtz,0)
    print("All done!")
    




# IO functions

def read_tt_infos(gid, grain_dir):
    data_path = os.path.join(grain_dir, 'grain_%04d.mat' % gid)
    data = loadmat(data_path)
    t = data['out'][0][0][0][0]
    values = t[0][0][0][0][0]
    d = {}
    d['gr_id'] = int(values[0][0][0])
    # double check that the grain id is right
    if d['gr_id'] != gid:
        raise(ValueError('grain id mismatch, check your matlab output in file %s' % data_path))
    d['nfdtx'] = float(values[1][0][0])
    d['d3tz'] = float(values[2][0][0])
    d['diffry'] = float(values[3][0][0])
    d['samrx'] = float(values[4][0][0])
    d['samry'] = float(values[5][0][0])
    d['samtx'] = float(values[6][0][0])
    d['samty'] = float(values[7][0][0])
    d['samtz'] = float(values[8][0][0])
    d['samrx_offset'] = float(values[9][0][0])
    d['samry_offset'] = float(values[10][0][0])
    d['samtx_offset'] = float(values[11][0][0])
    d['samty_offset'] = float(values[12][0][0])
    d['samtz_offset'] = float(values[13][0][0])
    d['diffrz_offset'] = float(values[14][0][0])
    d['int_factor'] = float(values[15][0][0])
    print(d)
    return d

def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def load_grains_from_matlab(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = read_tt_infos(gr_id)
        grain_list.append(grain)
    return grain_list
    
def load_grains_from_json(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = get_json_grain_info(gr_id)
        grain_list.append(grain)
    return grain_list

class NumpyEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self,obj)

def appenddictionaryfile(dictname,filename):
    """to write dictionary to file"""
    if os.path.exists('%s'%filename):
        with open('%s'%filename, 'r+') as f:
            data_to_write = [json.load(f)]
            dictname['time'] = str(time.ctime())
            data_to_write.append(dictname)
            f.seek(0)
            json.dump(data_to_write, f)
    else:
        newdict = dictname
        newdict['time'] = str(time.ctime())
        with open('%s'%filename, 'w') as f:
            json.dump(newdict, f)
            #f.write(datatowrite)
    print("Dictionary appended to %s"%filename)
    return

def update_grain_info(grain):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    writedictionaryfile(grain,'%s/%s_g%s'%(basedir,samplename,grain['gr_id']))
    #appenddictionaryfile(grain,'%s_g%s_history'%(SCAN_SAVING.collection_name,grain['gr_id']))
    return

def get_json_grain_info(grain_number):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    return readdictionaryfile('%s/%s_g%s'%(basedir,samplename,str(grain_number)))

def readdictionaryfile(filename):
    """to read dictionary from file, returns dictionary"""
    with open('%s'%filename, 'r') as f:
        readdata = [json.loads(f.read())]
    print("Dictionary read from %s"%filename)
    return readdata[-1]

def writedictionaryfile(dictname,filename):
    """to write dictionary to file"""
    datatowrite = json.dumps(dictname)
    with open('%s'%filename, 'w') as f:
        f.write(datatowrite)
    print("Dictionary written as %s"%filename)
    return



# tomo scan sequences

def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        #finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    umv(ref_mot, refpos)

def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')
    
def ff_zseries(ff_pars, datasetname = 'ff'):
    #user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    #umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    #user.cpm18_goto(cpm18_tuned)
    #umv(u22, u22_tuned)
    print('ff_zseries succeed')
    return('Succeed')

def tomo_series_cst_load(start, num_scans, target, sleep_time=0, pct_pars=None):
    for i in np.arange(start, start+num_scans):
        tomo.full_turn_scan(str(i))
        load_ramp_by_target(target, 0.05, 1)
        sleep(sleep_time)
    return

def ftomo_series(scanname, start, num_scans, sleep_time=0, pars=None):
    for i in np.arange(start, start+num_scans):
        newdataset(scanname + str(i))
        umv(diffrz, pars['start_pos']);
        fsh.disable()
        fsh.close()
        print("taking dark images")
        ftimescan(pars['exp_time'], pars['nref'],0)
        fsh.enable()
        print("taking flat images")
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'], pars['scan_mode'])
        print("taking projections...")
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        print("resetting diffrz to 0")
        umv(diffrz, pars['start_pos']+360);
        diffrz.position=pars['start_pos'];
        sleep(sleep_time)
    return

def tdxrd_boxscan(scanname, ff_pars):
    print("using slit_vg %f and slit_hg %f" % (ff_pars['slit_vg'], ff_pars['slit_hg']))
    umv(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.5, s8hg, ff_pars['slit_hg'] + 0.5)
    newdataset(scanname)
    finterlaced(diffrz, ff_pars['start_pos'], acq_size = ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])

def tdxrd_pointscan(scanname, sff_pars):
    #user.cpm18_goto(cpm18_tuned + sff_pars['cpm18_detune'])
    diffty_airpad.on(60*24*5)
    scanstarty = sff_pars['ystart'] #-0.4  # in mm
    scanendy = sff_pars['yend'] #0.401 
    scanstep= sff_pars['ystep'] #0.01
    
    newdataset(scanname)

    for ty in np.arange(scanstarty,scanendy,scanstep):
        umv(diffty, diffty_zero + ty)
        print(f"Scan pos: y: {ty} ")
        finterlaced( diffrz, sff_pars['start_pos'], acq_size = sff_pars['step_size'], npoints = sff_pars['num_proj'], acq_time = sff_pars['exp_time'], mode = sff_pars['mode'])

    umv(diffrz,0)      
    umv(diffty, diffty_zero)
    umv(bigy, bigy_out)
    diffty_airpad.off()
    #user.cpm18_goto(cpm18_tuned)
    print("All done!")
 
def update_flat():
    imagecorr.take_dark()
    imagecorr.take_flat()
    imagecorr.dark_on()
    imagecorr.flat_on()

def flat_on():
    imagecorr.dark_on()
    imagecorr.flat_on()

def flat_off():
    imagecorr.dark_off()
    imagecorr.flat_off()

# Scan sequences

def load_sequence(par_list, grain_list):
    """Load Experiment Master Sequence.
    
    This function will perform scan sequences (pct, dct, tt) for a list of target load values defined in "par_list" 
    Note: grain_list is currently produced by a matlab script and can be re-created / imported via:  grain_list = user.read_tt_info([list_of_grain_ids])
    par_list can be created via: par_list = user.define_pars()  and sub-parameters for dct, pct, tt can be adapted to cope with increasing mosaicity

    """
    for step_pars in par_list:
        dct_pars = step_pars['dct_pars']
        pct_pars = step_pars['pct_pars']
        tt_pars = step_pars['tt_pars']
        ff_pars = step_pars['ff_pars']
        sff_pars = step_pars['sff_pars']
        target = step_pars['target']
        load_step = step_pars['load_step']
        grain_list = load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, ff_pars, sff_pars, target, load_step)
    return grain_list

def load_step_sequence(dct_pars, pct_pars, sff_pars, target):
    """Performs a loadramp to the new target value and launches PCT, DCT and a series of TT scans at this new target load
    """
    samtz_cen = samtz.position
    pct_pars['samtz_cen'] = samtz_cen
    dct_pars['samtz_cen'] = samtz_cen
    ff_pars['samtz_cen'] = samtz_cen
    ## PCT
    prepare_pct(pct_pars)
    #load_ramp_by_target(target)
    scan_name = 'PCT_%s' % target
    tomo_by_fscan_dict(scan_name, pct_pars)
    ## DCT
    prepare_dct(dct_pars)
    scan_name = 'DCT_%s_' % target
    #tomo_by_fscan_dict(scan_name, dct_pars )
    dct_zseries(dct_pars, scan_name)
    ## TT
    #scan_name = 'tt_%dN_' % target
    #marana_tt(grain_list, tt_pars, target)
    ##frelon16in(4, tt_pars)
    ##grain_list = loop_grains(grain_list, tt_pars, target)
    ## 3DXRD
    #prepare_ff(ff_pars)
    #scan_name = 'FF_%s_' % target    
    #ff_zseries(ff_pars, scan_name)
    ## s-3DXRD
    prepare_sff(sff_pars)
    scan_name = 'sFF_%s_z1' %target
    tdxrd_pointscan(scan_name, sff_pars)
    ## 2eme point S-FF
    scan_name = 'sFF_%s_z2' %target
    umvr(samtz,0.003)
    tdxrd_pointscan(scan_name, sff_pars)
    ## 3eme point S-FF
    scan_name = 'sFF_%s_z3' %target
    umvr(samtz,-0.006)
    tdxrd_pointscan(scan_name, sff_pars)
    
    #scan_name = 'sff_2d_%dN_g%d' % (target, grain_list[0]['gr_id'])
    #sff_one_grain(scan_name, sff_pars, grain_list[0])
    #return grain_list
    prepare_pct(pct_pars)

# Nanox functions

def load_ramp_by_ascan(start, stop, npoints, exp_time, loadstep, pct_pars):
    prepare_pct(pct_pars)
    newdataset('loadramp_%d' %  loadstep)
    ascan(stress, start, stop, npoints, exp_time, stress_cnt, marana3)
    return

def load_ramp_by_target(target, time_step=0.1, voltage_step = 0.1, tol=0.1, to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached"""
    write_to_file = to_file if to_file is not None else sys.stdout
    current_load = stress_cnt.get_value()
    if abs(target - current_load) < tol:
        print('current load within tolerance of target load - stopping here', file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target > current_load:
        stress_increment = voltage_step
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -voltage_step
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return

 
	
class LoadLoop:
    def __init__(self):
        self._task = None
        self._stop = False
        self._sleep_time = 1
        self._filepath = None
        
    def start(self,target,loadstep,filepath,sleep_time=1.,time_step=0.5):
        self._filepath = filepath
        if self._task:
            self._stop = True
            self._task.get()

        self._sleep_time = sleep_time
        self._stop = False
        self._task = gevent.spawn(self._run,target,loadstep,time_step)

    def stop(self):
        self._stop = True
        if self._task: 
           self._task.get()

    def _run(self,target,loadstep,time_step):
        newdataset('loadramp_%d' %  loadstep)
        with open(self._filepath,'a') as f:
            while not self._stop:
                load_ramp_by_target(target,0,loadstep,time_step,to_file=f)
                gevent.sleep(self._sleep_time)
