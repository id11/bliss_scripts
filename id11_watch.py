# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


from datetime import date, datetime
import time
from gevent import sleep
import numpy as np


class JPit:
    def __init__(self):
        self.mratio = -1

    def opt_watch(self, sleeptime=1):
        """
        The famous optics watcher function to get the beamline optics components always in place
        """
        nn = 0
        mex_L = np.zeros([50])

        print(f"\n{fmt_date_now()}: Starting opt_watch - Cycle time is {sleeptime:g} s\n\n\n\n\n\n\n\n", end="")

        while True:

            start_time = time.time()

            all_machinfo = machinfo.all_information
            SR_mode = all_machinfo["SR_Mode"]
            # Possible SR modes (machinfo.SRMODE.   ): IdTest, MDT, mro, SafetyTest, Shutdown, Unknown, USM)

            # read the ID gaps
            try:
                U22 = u22.position
                U22isready = "READY" in u22.state
                CPM18 = cpm18.position
                CPM18isready = "READY" in cpm18.state
            except:
                pass

          
                ener1 = ECONST_KEV_ANG/(2*LL_SI111*sin(radians(llbragg1.position)))
                ener2 = ECONST_KEV_ANG/(2*LL_SI111*sin(radians(llbragg2.position)))
                
            except:
                pass
            date = fmt_date_now()
            sec_of_day = datetime.now().second



            if (
                all_machinfo["SR_Mode"] == "Shutdown"
                #or all_machinfo["SR_Mode"] == "MDT"
            ):
                print("\n  End of user mode, shutdown, exiting.")
                print(f"  Photon energy is approximately {ener1:7.4f} from llbragg1 keV and {ener2:7.4f} from llbragg2  ")
                print(f"Undulator gaps are cpm18 = {CPM18:5.3f} u22 = {U22:5.3f}.")
                #print(f"  Undulator gap is u26b = {U26:5.3f}.")
                #print("Restart with early_start.")
                return


                
            # read the saved ID gaps
            dat = open("/tmp/Usave.dat").readline().strip().split()
            self.U22 = float(dat[0])
            self.CPM18 = float(dat[1])

            if (
                all_machinfo["SR_Mode"] == "USM"
                #or all_machinfo["SR_Mode"] == "MDT"
            ):

                # Check the vacuum valves
                if rv0.is_closed:
                    rv0.open()
                if rv1.is_closed:
                    rv1.open()
                if rv2.is_closed:
                    rv2.open()
                if rv3.is_closed:
                    rv3.open()
                if rv4.is_closed:
                    rv4.open()

                # Check automatic mode of the FE
                if (
                    all_machinfo["Automatic_Mode"] is False
                    and sec_of_day + 1 < sleeptime + 1
                ):
                    machinfo.automatic_mode = True
                    sleep(5)

                if (
                    all_machinfo["Automatic_Mode"] is True
                    and all_machinfo["Auto_Mode_Time"] < 24 * 60 * 60
                ):
                    machinfo.automatic_mode = True


                # Check undulator positions and rectify
                if all_machinfo["SR_Refill_Countdown"] > sleeptime:

                    u26 = u26b.position
                    if self.U26 > 28.997:
                        self.U26 = 28.997

                    if self.U26 >= 6 and abs(u26 - self.U26) > 0.002 and U26isready == True:
                        try:
                            umv(u26b, self.U26)
                        except Exception as e:
                            pass
                        print("\n")


          
            # Check refill
            torefill = all_machinfo["SR_Refill_Countdown"]
            if torefill <= sleeptime + 1 and torefill > -1:
                refil = REVERSE("Refill")
            else:
                refil = "Refill"

            

            # Check  shutters
            if rsh_is_closed():
                rds = REVERSE("rsh: closed")
            else:
                rds = "           "

            



            print(
                f"\033[?7l\033[7A{date}: {SR_mode}  {refil}:  {int(torefill):5d}  {atten}   Mang: {mangle: 7.5f}  s2hg:{s2hg_pos:5.2f}   s2vg{s2vg_pos:5.2f}     \033[K\n\033[K\n",end=""
            )
            print(
                f"\033[K\r\033[17C u26b_gap: {U26:6.3f}  monin: {min: 6.3e}  monex: {mex:6.3e} op: {op:4.1f}%\033[K \n\033[K\n", end=""
            )
            print(
                f"\033[K\r\033[17C U26B_set: {self.U26:6.3f}\033[K\n\033[K\n", end=""
            )

            if all_machinfo["Automatic_Mode"] is False:
                print(f"\033[K\r\033[17C {rds} {bvs} {tfs}")
            else:
                print(f"\033[K\r\033[17C {rds} {bvs} {tfs} \033[K")


            # Check for remote commands
            user_script_homedir("/tmp/")
            command = "  "
            try:
                command = open("/tmp/remote_optics.py").readline()   #.strip().split()
                #action = float(check[0])
            except:
                pass

            if command != "  \n":
                user_script_run("remote_optics.py")
                with open("/tmp/remote_optics.py", "w") as file:
                    print(f"  ", file=file, end = '\n')
                


            #exit with ctrl_c
            delta_time = time.time() - start_time
            waittime = sleeptime - delta_time
            if waittime>0:
                sleep(waittime)

            # Look for q char
            # t = 0
            # while t <= 5 * sleeptime:
            #     # get_char() from sessions/scripts/utilities.py
            #     # get a char without waiting
            #     c = get_char()
            #     if c == "q":
            #         print("\033[K\n",end = "")
            #         return
            #     sleep(.2)
            #     t += 1

    def Usave(self):
        """
        Undulator save
        """
        
        # read the ID gaps
        U22 = u22.position
        CPM18 = cpm18.position
        
        ener1 = 0
        ener2 = 0
    
        try:
            ener1 = ECONST_KEV_ANG/(2*LL_SI111*sin(radians(llbragg1.position)))
            ener2 = ECONST_KEV_ANG/(2*LL_SI111*sin(radians(llbragg2.position))) 
        except:
            pass
        
        date = fmt_date_now()

 
        
        # open and truncate the file, Usave.dat is for saving current positions which will be used by opt_watch as defaults
        with open("/tmp/Usave.dat", "w") as file:
            print(f"{U22:6.3f}  {CPM18:6.3f}", file=file)

        with open("/tmp/Usave.logg", "a") as file:
            print(
                f"{date}   E1 = {ener1:8.5f}  E2 = {ener2:8.5f}: u22 = {U22:6.3f}, cpm18 = {CPM18:6.3f}",
                file=file,
            )
        

        self.U22 = U22
        self.CPM18 = CPM18


#    def early_start(self):
#        self.opt_watch()
#        pass

   


#def REVERSE2(arg):
#    if arg == 2:
#        return arg
#    else:
#        return REVERSE(arg)


jpit = JPit()
opt_watch = jpit.opt_watch
Usave = jpit.Usave
