
import numpy as np
import time
user_script_load("nc_bliss")


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico6']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico6']

def sample_ref():
    umv(dty,12.1)
    pause_for_refill(70)
    umv(rot, 0)
    plotselect('frelon4:roi_counters:roi2_std')
    user.piezo_scanmode_on()
    dscan(py, -.5, .5, 20, .08, save_images=False)
    goto_com()
    dscan(pz, -.5, .5, 20, .08, save_images=False)
    goto_com()
    user.piezo_scanmode_off()
    umv(rot,90)
    plotselect("frelon4:roi_counters:roi1_std")
    user.piezo_scanmode_on()
    dscan(px, -.5, .5, 20, 0.08, save_images=False)
    goto_com()
    user.piezo_scanmode_off()

class refscanner(object):
    def __init__(self, refpos): # refpos is a dict
        self.refpos = refpos
    def measure(self, mot, rng, npt=20, tim=0.05):
        if 'rot' in self.refpos:
            if user.piezo_scanmode():
                user.piezo_scanmode_off()
            umv(rot, self.refpos['rot'])
            user.piezo_scanmode_on()
        umv(px, self.refpos['px'],
            py, self.refpos['py'],
            pz, self.refpos['pz'])
        scan = dscan( mot, -rng, rng, npt, tim, save_images=False)
        c = cen()
        self.refpos[mot.name]=c
        umv(mot,c)
        where()
        return c
    def __getitem__(self, m):
        return self.refpos[m]
    def __setitem__(self, m, p):
        self.refpos[m] = p
    
        
    
def sample_ref_fluo():
    # TODO : put these scans in a different dataset for easier plotting?
    # Compared to cross/surface go to:
    # pz -30, px -2.5 py -2.5
    ## Failed on a bufer overrun ?
    pause_for_refill(66)
    ACTIVE_MG.enable("mca")
    plotselect("mca:WAU_det0")
    umv(dty,12.1)
    # measure y with z above
    oy = refscanner( { 'px': 52.05, 'py': 49.83, 'pz':40.79, 'rot' : 0 } )
    oy.measure( py, 2 )
    # now measure z at the correct y, 10 below
    oz = refscanner( oy.refpos.copy() )
    oz['pz']+=10
    oz['py']-=10
    oz.measure( pz, 2 )
    oy['pz']=oz['pz']-10
    # now recheck y at the ideal z
    oy.measure( py, 2 )
    ox = refscanner( oy.refpos.copy() )
    ox['rot']=70.
    ox.measure( px, 2 )
    user.piezo_scanmode_off()
    ACTIVE_MG.disable("mca*")
    
def difftyscan():
    newproposal("blc12352")
    newsample("WAuRF4") # restarts here ###
    # fpico6
    pico6.auto_range = False
    pico6.range=2e-4
    
    ACTIVE_MG.disable_all()
    ACTIVE_MG.enable("frelon4:roi*")
    ACTIVE_MG.enable("frelon4:image")
    ACTIVE_MG.enable("*fpico6")
    
    y0 = 12.1  # centre of rotation
    width = 3.6
    step  = 0.1
    # frelon4.roi_counters.set('roi1',(520,678,10,10))
    #frelon4.roi_counters.set('roi1',(689,1118,10,10))
    #frelon4.roi_counters.set('roi2',(721,991,10,10))

    for iz,my_z in enumerate(np.arange(0,3.61,.1)):
        newdataset("Z%03d"%(iz))
        sample_ref_fluo()
        umv(pz,pz.position - my_z, py, py.position+2.5, px, px.position+2.7 )
        plotselect("frelon4:roi1_avg")
        for my_y  in np.arange( y0-width, y0+width+step/10., step):
            umv(dty, my_y)
            pause_for_refill(30)
            finterlaced(rot,   0,  1.0, 180, 0.08, mode="ZIGZAG")
            #umv(dty, my_y+step/2.)
            #
            #finterlaced(rot, 720, -1.0, 360, 0.08, mode="ZIGZAG")
