import h5py, hdf5plugin, sys, os
import numpy as np
from PIL import Image
import multiprocessing, os


def convert(args):
    start, step, scanno, sdname, savelocation, fname = args
    with h5py.File(fname, 'r') as h:
        eiger = h['%i.1/measurement/eiger'%scanno]
        for img in range(start, len(eiger), step):
            oname = savelocation + '/%s_scan%i_frame%06i.tif'%(sdname,scanno,img)
            im = Image.fromarray(eiger[img,:,:])
            im.save(oname)


if __name__=="__main__":
    multiprocessing.set_start_method('forkserver')

    fname = str(sys.argv[1])
    savelocation = str(sys.argv[2])
    scanno = int(sys.argv[3])
    print(fname.split('/'))
    sname, sdname, hname = fname.split('/')[-3:]
    print(sname,sdname)

    print('Converting',fname,'scan',scanno)
    if not os.path.exists(savelocation):
        os.system("mkdir -p %s"%savelocation)
    print('Saving in',savelocation)
        

    nprocs = len(os.sched_getaffinity(os.getpid()))//4
    print("nprocs",nprocs)
    with multiprocessing.Pool(nprocs) as pool:
        for _ in pool.imap_unordered(convert,
                                     [( i, nprocs, scanno, sdname, savelocation, fname )
                                      for i in range(nprocs)] ):
            pass
        print('DONE!')




