

def checkfilts():
    results = ""
    for i in range(16):
        musst_tdxrd.putget("#chcfg ch6 adc +-10V filt %d"%(i))
        s = loopscan(100,0.01,musst_tdxrd.counters.mch6)
        y = s.get_data("musst_tdxrd:samp:mch6")
        results += "%d %f %f %f \n"%(i,y.max()-y.min(),y.std(),y.mean())
        print(results)


results = [[float(v) for v in line.split()]
           for line in """0 0.008850 0.001946 0.507919        
1 0.007728 0.001810 0.507883        
2 0.007711 0.001660 0.508080                                                                             
3 0.007089 0.001809 0.508202 
4 0.007442 0.001899 0.508159 
5 0.007395 0.001702 0.508217
6 0.007225 0.001730 0.507648
7 0.006724 0.001586 0.507906
8 0.006216 0.001602 0.507922
9 0.005153 0.001351 0.507993
10 0.003838 0.000956 0.507657
11 0.002642 0.000678 0.508074
12 0.001507 0.000358 0.507963
13 0.000833 0.000200 0.507984
14 0.000421 0.000101 0.507979
15 0.000223 0.000051 0.507938 """.split('\n')]


from  pylab import *
T = 1e-6
a=array(results)
int_time = 2**a[:,0]*T
plot(int_time,a[:,1],"-o")
figure()
plot(int_time,a[:,2],"-o")
show()

           
