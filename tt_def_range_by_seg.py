import os
import numpy as np

import h5py
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, TextBox
from matplotlib.colors import ListedColormap
from scipy.ndimage import label

class DiffryRangeDeterminator:
    def __init__(self, axs, fig_handle, image_stack, downsampling_ratio = 32, padding = 10, thres_log = 1):
        self.padding = padding
        self.ax = axs
        self.fig = fig_handle
        self.dsampling = downsampling_ratio
        self.image_stack = image_stack
        self.slices, rows, cols = image_stack.shape
        image_stack = self.downsample_image_xy()
        self.ds_image_stack = image_stack
        self.grain_mask = 2 * (image_stack > 10**thres_log)
        self.threshold_log = thres_log
        self.threshold_former = 0
        self.selected_labels = []
        self.slice_ind = 0
        self.im = [0, 0]
        self.fresh_image()
        self.updateSlice(1)
    def downsample_image_xy(self):
        slices, rows, cols = self.image_stack.shape
        row_pads = rows % self.dsampling
        col_pads = cols % self.dsampling
        row_padshalf = row_pads // 2
        col_padshalf = col_pads // 2
        dsimgstack = np.pad(self.image_stack, ((0, 0), (row_padshalf, row_pads - row_padshalf), (col_padshalf, col_pads - col_padshalf)))
        dsimgstack = dsimgstack.reshape((slices, (rows + row_pads) // self.dsampling, self.dsampling, (cols + col_pads) // self.dsampling, self.dsampling))
        dsimgstack = np.sum(np.sum(dsimgstack, 4), 2)
        return dsimgstack
    def updateSlice(self, slice_ind):
        slice_ind = int(slice_ind - 1)
        self.im[0].set_data(self.image_stack[slice_ind, :, :])
        self.im[1].set_data(self.grain_mask[slice_ind, :, :])
        self.ax[0].set_xlabel('slice %d' % (slice_ind + 1))
        self.slice_ind = slice_ind
        self.im[0].axes.figure.canvas.draw()
        self.im[1].axes.figure.canvas.draw()
    def updateThres(self, thres):
        self.threshold_log = thres
        thres = 10**thres
        self.grain_mask = 2 * (self.ds_image_stack > thres)
        self.im[1].set_data(self.grain_mask[self.slice_ind, :, :])
        self.ax[1].set_xlabel('threshold %.6s' % thres)
        self.im[1].axes.figure.canvas.draw()
    def rightClickSelSpots(self, event):
        if event.button != 3:
            return
        try:
            x_ind = int(round(event.xdata))
            y_ind = int(round(event.ydata))
        except:
            return
        x_ind = max(x_ind, 0)
        y_ind = max(y_ind, 0)
        if self.grain_mask[self.slice_ind, y_ind, x_ind] > 0:
            print([x_ind, y_ind, self.slice_ind])
            if self.threshold_log != self.threshold_former:
                self.reset_labels()
                self.threshold_former = self.threshold_log
            new_label = self.labeled_mask[self.slice_ind, y_ind, x_ind]
            if new_label in self.selected_labels:
                self.selected_labels.remove(new_label)
                self.grain_mask = np.uint(self.grain_mask) + np.uint(self.labeled_mask == new_label)
            else:
                self.selected_labels.append(new_label)
                self.grain_mask = np.uint(self.grain_mask) - np.uint(self.labeled_mask == new_label)
            self.im[1].set_data(self.grain_mask[self.slice_ind, :, :])
            self.im[1].axes.figure.canvas.draw()
    def confirmandclose(self, event):
        plt.close(self.fig)
    def read_padding(self, text):
        self.padding = int(text)
    def change_downsampling(self, text):
        self.dsampling = int(text)
        self.ds_image_stack = self.downsample_image_xy()
        self.updateThres(self.threshold_log)
        self.reset_labels()
        self.fresh_image()
    def reset_labels(self):
        self.labeled_mask, nof_features = label(self.grain_mask)
        self.selected_labels = []
    def fresh_image(self):
        self.im[0] = self.ax[0].imshow(self.image_stack[self.slice_ind, :, :])
        cmap_3color = [[0., 0., 0.], [0.5, 0., 0.], [1., 1., 1.]]
        cmap_3color = ListedColormap(cmap_3color)
        self.im[1] = self.ax[1].imshow(self.grain_mask[self.slice_ind, :, :], cmap = cmap_3color)
    def getSliceBounds(self):
        if len(self.selected_labels):
            slice_bounds = np.sum(np.sum(self.labeled_mask == self.selected_labels[0], 2), 1)
            for ii_label in range(1, len(self.selected_labels), 1):
                slice_bounds += np.sum(np.sum(self.labeled_mask == self.selected_labels[ii_label], 2), 1)
            slice_bounds = np.nonzero(slice_bounds)
            return [slice_bounds[0][0], slice_bounds[0][-1]]
        else:
            return []

def get_limits_by_seg(diffry_array = np.arange(-7.05,-6.29,0.05), data_dir='/data/visitor/ma4925/id11/', sample_name='Ni_s19', step=0, scan_fold_ind = 5):
    #datasetname = sample_name+'_grain_%04d_%dN' % (img_3d['gr_id'], step)
    datasetname = sample_name + '_grain_%04d_%dN__0002' % (7, step)
    #h5file_path = os.path.join(data_dir, sample_name, datasetname, 'scan%04d' % scan_fold_ind, 'frelon16_0000.h5')
    h5file_path = os.path.join(data_dir, sample_name, datasetname, datasetname + '.h5')
    scan_fold_ind = str(scan_fold_ind)
    flag_readfile = 'y'
    while(flag_readfile != 'n'):
        scan_fold_ind = input('\nThe index of scan: [' + scan_fold_ind + '] ') or scan_fold_ind
        h5file_path = input('The path of data: [' + h5file_path + '] ') or h5file_path
        flag_confirm = input('Path: ' + h5file_path + '\nScan index: ' + scan_fold_ind + '\nAre you satisfied with the path and the scan index? y/n [y] ')
        if flag_confirm == 'n':
            continue
        try:
            img = h5py.File(h5file_path, 'r')
            keys_measurement = list(img[str(scan_fold_ind)+'.1']['measurement'].keys())
            if 'frelon16' in keys_measurement:
                camera_name = 'frelon16'
            elif 'marana3' in keys_measurement:
                camera_name = 'marana3'
            img = img[str(scan_fold_ind)+'.1']['measurement'][camera_name]
            flag_readfile = 'n'
        except:
            flag_readfile = input('Failed to read file. Try again? y/n [y]')

    bg = np.median(img)
    img = img - bg
    nof_slices, nof_rows, nof_cols = img.shape
    fig, axs = plt.subplots(1, 2)
    range_determinator = DiffryRangeDeterminator(axs, fig, img)
    ax_slice = plt.axes([0.2, 0.02, 0.6, 0.03])
    simgslice = Slider(
    ax_slice, "Slice", 1, nof_slices,
    valinit=1, valstep=1)
    simgslice.on_changed(range_determinator.updateSlice)
    ax_thres = plt.axes([0.05, 0.2, 0.03, 0.6])
    simgthres = Slider(
    ax_thres, "Threshold", 0, np.log10(range_determinator.ds_image_stack.max()),
    valinit=0, orientation = 'vertical')
    simgthres.on_changed(range_determinator.updateThres)
    fig.canvas.mpl_connect('button_press_event', range_determinator.rightClickSelSpots)
    dstextboxax = plt.axes([0.6, 0.85, 0.2, 0.075])
    dstextbox = TextBox(dstextboxax, 'Downsampling', initial=str(range_determinator.dsampling))
    dstextbox.on_submit(range_determinator.change_downsampling)
    paddingtextboxax = plt.axes([0.1, 0.85, 0.2, 0.075])
    paddingtextbox = TextBox(paddingtextboxax, 'Padding', initial=str(range_determinator.padding))
    paddingtextbox.on_submit(range_determinator.read_padding)
    confirmax = plt.axes([0.8, 0.025, 0.1, 0.04])
    button_confirm = Button(confirmax, 'Ok', hovercolor='0.975')
    button_confirm.on_clicked(range_determinator.confirmandclose)
    plt.show()
    slice_bounds = range_determinator.getSliceBounds()
    if not len(slice_bounds):
        flag_continue = input('No limit is determined. Would you like to re-run the code? y/n [y]: ')
        start_angle = 'nan'
        end_angle = 'nan'
    else:
        padding = range_determinator.padding
        start_pos = slice_bounds[0]
        end_pos = slice_bounds[1]
        ang_step = diffry_array[1] - diffry_array[0]
        start_angle = diffry_array[start_pos] - padding * ang_step
        end_angle = diffry_array[end_pos] + padding * ang_step
        print('start_angle={}, end angle={}, number of images={} (padding={}, length of mask={})'.format(start_angle, end_angle, int((end_angle - start_angle) / ang_step + 1), padding, slice_bounds[1] - slice_bounds[0] + 1))
        flag_continue = input('Would you like to re-run the code? y/n [y]: ')
    if flag_continue != 'n':
        start_angle, end_angle = get_limits_by_seg(diffry_array, data_dir, sample_name, step, scan_fold_ind)
    return start_angle, end_angle

get_limits_by_seg()


'''
import h5py
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, TextBox
from matplotlib.colors import ListedColormap
from scipy.ndimage import label

class DiffryRangeDeterminator:
    def __init__(self, axs, fig_handle, image_stack, downsampling_ratio = 32, padding = 10, thres_log = 1):
        self.padding = padding
        self.ax = axs
        self.fig = fig_handle
        self.dsampling = downsampling_ratio
        self.image_stack = image_stack
        self.slices, rows, cols = image_stack.shape
        image_stack = self.downsample_image_xy()
        self.ds_image_stack = image_stack
        self.grain_mask = 2 * (image_stack > 10**thres_log)
        self.threshold_log = thres_log
        self.threshold_former = 0
        self.selected_labels = []
        self.slice_ind = 0
        self.im = [0, 0]
        self.fresh_image()
        self.updateSlice(1)
    def downsample_image_xy(self):
        slices, rows, cols = self.image_stack.shape
        row_pads = rows % self.dsampling
        col_pads = cols % self.dsampling
        row_padshalf = row_pads // 2
        col_padshalf = col_pads // 2
        dsimgstack = np.pad(self.image_stack, ((0, 0), (row_padshalf, row_pads - row_padshalf), (col_padshalf, col_pads - col_padshalf)))
        dsimgstack = dsimgstack.reshape((slices, rows // self.dsampling, self.dsampling, cols // self.dsampling, self.dsampling))
        dsimgstack = np.sum(np.sum(dsimgstack, 4), 2)
        return dsimgstack
    def updateSlice(self, slice_ind):
        slice_ind = int(slice_ind - 1)
        self.im[0].set_data(self.image_stack[slice_ind, :, :])
        self.im[1].set_data(self.grain_mask[slice_ind, :, :])
        self.ax[0].set_xlabel('slice %d' % (slice_ind + 1))
        self.slice_ind = slice_ind
        self.im[0].axes.figure.canvas.draw()
        self.im[1].axes.figure.canvas.draw()
    def updateThres(self, thres):
        self.threshold_log = thres
        thres = 10**thres
        self.grain_mask = 2 * (self.ds_image_stack > thres)
        self.im[1].set_data(self.grain_mask[self.slice_ind, :, :])
        self.ax[1].set_xlabel('threshold %.6s' % thres)
        self.im[1].axes.figure.canvas.draw()
    def rightClickSelSpots(self, event):
        if event.button != 3:
            return
        try:
            x_ind = int(event.xdata)
            y_ind = int(event.ydata)
        except:
            return
        x_ind = max(x_ind, 0)
        y_ind = max(y_ind, 0)
        if self.grain_mask[self.slice_ind, y_ind, x_ind] > 0:
            print([x_ind, y_ind, self.slice_ind])
            if self.threshold_log != self.threshold_former:
                self.reset_labels()
                self.threshold_former = self.threshold_log
            new_label = self.labeled_mask[self.slice_ind, y_ind, x_ind]
            if new_label in self.selected_labels:
                self.selected_labels.remove(new_label)
                self.grain_mask = np.uint(self.grain_mask) + np.uint(self.labeled_mask == new_label)
            else:
                self.selected_labels.append(new_label)
                self.grain_mask = np.uint(self.grain_mask) - np.uint(self.labeled_mask == new_label)
            self.im[1].set_data(self.grain_mask[self.slice_ind, :, :])
            self.im[1].axes.figure.canvas.draw()
    def confirmandclose(self, event):
        plt.close(self.fig)
    def read_padding(self, text):
        self.padding = int(text)
    def change_downsampling(self, text):
        self.dsampling = int(text)
        self.ds_image_stack = self.downsample_image_xy()
        self.updateThres(self.threshold_log)
        self.reset_labels()
        self.fresh_image()
    def reset_labels(self):
        self.labeled_mask, nof_features = label(self.grain_mask)
        self.selected_labels = []
    def fresh_image(self):
        self.im[0] = self.ax[0].imshow(self.image_stack[self.slice_ind, :, :])
        cmap_3color = [[0., 0., 0.], [0.5, 0., 0.], [1., 1., 1.]]
        cmap_3color = ListedColormap(cmap_3color)
        self.im[1] = self.ax[1].imshow(self.grain_mask[self.slice_ind, :, :], cmap = cmap_3color)
    def getSliceBounds(self):
        if len(self.selected_labels):
            slice_bounds = np.sum(np.sum(self.labeled_mask == self.selected_labels[0], 2), 1)
            for ii_label in range(1, len(self.selected_labels), 1):
                slice_bounds += np.sum(np.sum(self.labeled_mask == self.selected_labels[ii_label], 2), 1)
            slice_bounds = np.nonzero(slice_bounds)
            return [slice_bounds[0][0], slice_bounds[0][-1]]
        else:
            return []

def get_limits_by_seg(scan_data, data_dir, sample_name, step, scan_fold_ind = 5, thres=0.1):
    diffry_array = scan_data['diffry']
    #datasetname = sample_name+'_grain_%04d_%dN' % (img_3d['gr_id'], step)
    datasetname = sample_name + '_grain_%04d_%dN__0002' % (7, step)
    h5file_path = os.path.join(data_dir, sample_name, datasetname, 'scan%04d' % scan_fold_ind, 'frelon16_0000.h5')
    img = h5py.File(h5file_path, 'r')
    img = img['entry_0000']['measurement']['data']
    img = img[()]#.value
    bg = np.median(img)
    img = img - bg
    nof_slices, nof_rows, nof_cols = img.shape
    fig, axs = plt.subplots(1, 2)
    range_determinator = DiffryRangeDeterminator(axs, fig, img, downsampling_ratio = 32, padding = 10, thres_log = np.log10(thres * img.max()))
    ax_slice = plt.axes([0.2, 0.02, 0.6, 0.03])
    simgslice = Slider(
    ax_slice, "Slice", 1, nof_slices,
    valinit=1, valstep=1)
    simgslice.on_changed(range_determinator.updateSlice)
    ax_thres = plt.axes([0.05, 0.2, 0.03, 0.6])
    simgthres = Slider(
    ax_thres, "Threshold", 0, np.log10(range_determinator.ds_image_stack.max()),
    valinit=0, orientation = 'vertical')
    simgthres.on_changed(range_determinator.updateThres)
    fig.canvas.mpl_connect('button_press_event', range_determinator.rightClickSelSpots)
    dstextboxax = plt.axes([0.6, 0.85, 0.2, 0.075])
    dstextbox = TextBox(dstextboxax, 'Downsampling', initial=str(range_determinator.dsampling))
    dstextbox.on_submit(range_determinator.change_downsampling)
    paddingtextboxax = plt.axes([0.1, 0.85, 0.2, 0.075])
    paddingtextbox = TextBox(paddingtextboxax, 'Padding', initial=str(range_determinator.padding))
    paddingtextbox.on_submit(range_determinator.read_padding)
    confirmax = plt.axes([0.8, 0.025, 0.1, 0.04])
    button_confirm = Button(confirmax, 'Ok', hovercolor='0.975')
    button_confirm.on_clicked(range_determinator.confirmandclose)
    plt.show()
    slice_bounds = range_determinator.getSliceBounds()
    if not len(slice_bounds):
        flag_continue = input('No limit is determined. Would you like to re-run the code? y/n [y]: ')
        start_angle = 'nan'
        end_angle = 'nan'
    else:
        padding = range_determinator.padding
        start_pos = slice_bounds[0]
        end_pos = slice_bounds[1]
        ang_step = diffry_array[1] - diffry_array[0]
        start_angle = diffry_array[start_pos] - padding * ang_step
        end_angle = diffry_array[end_pos] + padding * ang_step
        print('start_angle={}, end angle={}, number of images={} (padding={}, length of mask={})'.format(start_angle, end_angle, int((end_angle - start_angle) / ang_step + 1), padding, slice_bounds[1] - slice_bounds[0] + 1))
        flag_continue = input('Would you like to re-run the code? y/n [y]: ')
    if flag_continue != 'n':
        start_angle, end_angle = get_limits_by_seg(diffry_array, data_dir, sample_name, step, scan_fold_ind)
    return start_angle, end_angle
'''
