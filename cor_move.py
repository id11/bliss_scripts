from silx.io.url import DataUrl
import os, glob, h5py
from pathlib import Path
import bliss.setup_globals as bliss_globals
import numpy as np


def _get_tomo_slice_rec(path):
    if path and os.path.exists(path):
        print(f"Using scan: {path}")
        #return DataUrl(path)
        raw_data_url = path
    else:
        scan = SCANS[-1]
        raw_data_url = os.path.dirname(f"{scan.scan_saving.filename}")
        print(f"Defaulting to last scan: {raw_data_url}")
    processed_data_url = raw_data_url.replace("RAW_DATA", "PROCESSED_DATA")
    slice_data_url = DataUrl(
        max(
            glob.glob(f"{processed_data_url}/reconstructed_volumes/*slice*5"),
            key=os.path.getmtime,
        )
    )
    print(f"Loading slice from: {slice_data_url.file_path()}")
    with h5py.File(slice_data_url.file_path()) as h5In:
        image = h5In["/entry0000/reconstruction/results/data"][:]
    return slice_data_url.file_path(), np.squeeze(image)


def _get_tomo_volume_rec(path, slice_number):
    if path and os.path.exists(path):
        print(f"Using scan: {path}")
        #return DataUrl(path)
        raw_data_url = path
    else:
        scan = SCANS[-1]
        raw_data_url = os.path.dirname(f"{scan.scan_saving.filename}")
        print(f"Defaulting to last scan: {raw_data_url}")
    processed_data_url = raw_data_url.replace("RAW_DATA", "PROCESSED_DATA")
    print(processed_data_url)
    volume_data_url = DataUrl(
        max(
            glob.glob(f"{processed_data_url}/reconstructed_volumes/*vol*hdf5"),
            key=os.path.getmtime,
        )
    )
    print(f"Loading volume from: {volume_data_url.file_path()}")
    with h5py.File(volume_data_url.file_path()) as h5In:
        image = h5In["/entry0000/reconstruction/results/data"][slice_number, :, :]
        dim_z = h5In["/entry0000/reconstruction/results/data"].shape[0]
    return volume_data_url.file_path(), np.squeeze(image), dim_z


def _get_coordinates_from_image(image,image_path):
    f = flint()
    dataset_name = os.path.basename(os.path.dirname(os.path.dirname(image_path)))
    image_display = f.add_plot(plot_class="image",name=f"CoR Selection: {dataset_name}")
    image_display.side_histogram_displayed = False
    image_display.set_data(image)
    image_display.title = dataset_name
    if current_session.name == 'tdxrd':
        image_display.xlabel = "-Y"
        image_display.ylabel = "X"
    elif current_session.name == 'nscope':
        image_display.xlabel = "Y"
        image_display.ylabel = "X"

    image_display.yaxis_direction = "down"
    happy = "n"
    while happy not in "yY":  # :)
        print("Please click on the region of interest to be centered")
        coords = image_display.select_points(1)[0]
        tmp_image = np.copy(image)
        tmp_image[int(coords[1]) - 1 : int(coords[1]) + 1, :] = 0
        tmp_image[:, int(coords[0]) - 1 : int(coords[0]) + 1] = 0
        image_display.set_data(tmp_image)
        happy = input("Are you happy with the clicked pos? (y/N) ") or "n"

    print(f"Clicked position relative to center: X {coords[1]-image.shape[1]/2:.4f}, Y {coords[0]-image.shape[0]/2:.4f} pixels")
    return coords[1] - image.shape[1] / 2, coords[0] - image.shape[0] / 2  # X,Y

    
def _get_pixel_size():
    if current_session.name == "tdxrd":
        return tomo_tdxrd.detectors.active_detector.user_sample_pixel_size
    elif current_session.name == 'nscope':
        return tomo_nscope.detectors.active_detector.user_sample_pixel_size
    else:
        print(f"Session {current_session} not supported")


def _get_motor_position():
    if current_session.name == "tdxrd":
        x_pos = bliss_globals.samtx.position
        y_pos = bliss_globals.samty.position
        z_pos = bliss_globals.samtz.position
    elif current_session.name == 'nscope':
        x_pos = bliss_globals.ntx.position
        y_pos = bliss_globals.nty.position
        z_pos = bliss_globals.ntz.position
    else:
        print(f"Session {current_session} not supported")
    return x_pos, y_pos, z_pos


def _calculate_movement(x_coord, y_coord, pixel_size):
    if current_session.name == "tdxrd":
        x_move = x_coord * (pixel_size / 1000)
        y_move = -y_coord * (pixel_size / 1000)
    elif current_session.name == "nscope":
        #x_move = x_coord * (pixel_size / 1000)
        x_move = -x_coord * (pixel_size / 1000) # corrected on 14/09/2024
        y_move = y_coord * (pixel_size / 1000)
    else:
        print(f"Session {current_session} not supported")
    return x_move, y_move


def cor_move(path=None, auto=False, slice_number=None):
    """
    This function is moving x, y and z according to a reconstructed dataset from nabu
    This function does not correct for the tilt angles above the rotation
    Meaning: It can be off in case of big tilts, you may want to run it a second time
    """
    pixel_size = _get_pixel_size()
    x_pos, y_pos, z_pos = _get_motor_position()
    if slice_number is None:
        image_path, image = _get_tomo_slice_rec(path)
        if not os.path.exists(image_path):
            print("Reconstruction not ready!")
            return
        x_coord, y_coord = _get_coordinates_from_image(image,image_path)
        x_move, y_move = _calculate_movement(x_coord, y_coord, pixel_size)
        if current_session.name == "tdxrd":
            print(f"Calculated movement samtx: {x_pos:.4f} -> {x_pos+x_move:.4f} (moving by {x_move:.4f} mm)")
            print(f"Calculated movement samty: {y_pos:.4f} -> {y_pos+y_move:.4f} (moving by {y_move:.4f} mm)")
            print(f"To apply movement run: umv(samtx, {x_pos+x_move:.4f}, samty, {y_pos+y_move:.4f})")
            if auto:
                umv(samtx, x_pos + x_move, samty, y_pos + y_move)
        elif current_session.name == "nscope":
            print(f"Calculated movement ntx: {x_pos:.4f} -> {x_pos+x_move:.4f} (moving by {x_move:.4f} mm)")
            print(f"Calculated movement nty: {y_pos:.4f} -> {y_pos+y_move:.4f} (moving by {y_move:.4f} mm)")
            print(f"To apply movement run: umv(ntx, {x_pos+x_move:.4f}, nty, {y_pos+y_move:.4f})")
            if auto:
                umv(ntx, x_pos + x_move, nty, y_pos + y_move)
        else:
            print(f"Session {current_session} not supported")
    else:
        image_path, image, dim_z = _get_tomo_volume_rec(path, slice_number)
        if not os.path.exists(image_path):
            print("Reconstruction not ready!")
            return
        x_coord, y_coord = _get_coordinates_from_image(image,image_path)
        z_move = (slice_number - (dim_z / 2)) * (pixel_size / 1000)
        x_move, y_move = _calculate_movement(x_coord, y_coord, pixel_size)
        if current_session.name == "tdxrd":
            print(f"Calculated movement samtx: {x_pos:.4f} -> {x_pos+x_move:.4f} (moving by {x_move:.4f} mm)")
            print(f"Calculated movement samty: {y_pos:.4f} -> {y_pos+y_move:.4f} (moving by {y_move:.4f} mm)")
            print(f"Calculated movement samtz: {z_pos:.4f} -> {z_pos+z_move:.4f} (moving by {z_move:.4f} mm)")
            print(f"To apply movement run: umv(samtx, {x_pos+x_move:.4f}, samty, {y_pos+y_move:.4f}, samtz, {z_pos+z_move:.4f})")
            if auto:
                umv(samtx, x_pos + x_move, samty, y_pos + y_move, samtz, z_pos + z_move)
        elif current_session.name == "nscope":
            print(f"Calculated movement ntx: {x_pos:.4f} -> {x_pos+x_move:.4f} (moving by {x_move:.4f} mm)")
            print(f"Calculated movement nty: {y_pos:.4f} -> {y_pos+y_move:.4f} (moving by {y_move:.4f} mm)")
            print(f"Calculated movement ntz: {z_pos:.4f} -> {z_pos+z_move:.4f} (moving by {z_move:.4f} mm)")
            print(f"To apply movement run: umv(ntx, {x_pos+x_move:.4f}, nty, {y_pos+y_move:.4f}, ntz, {z_pos+z_move:.4f})")
            if auto:
                umv(ntx, x_pos + x_move, nty, y_pos + y_move, ntz, z_pos + z_move)
        else:
            print(f"Session {current_session} not supported")

                
