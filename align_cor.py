from __future__ import print_function, division

import numpy as np
import math
from scipy.optimize import curve_fit


def _dscan_fit(mot, rng, npts, ctim, ctr, sig_sign):
    mot.sync_hard()
    plotselect(ctr)
    lastscan = dscan(mot, -rng, rng, npts, ctim)
    sleep(1)
    mot.sync_hard()
    A = lastscan.get_data()
    y = sig_sign * A[ctr]
    x = A[mot.name]
    bkg = (y[0] + y[-1]) / 2
    cen = (x * (y - bkg)).sum() / (y - bkg).sum()
    print("bkg,cen", bkg, cen)
    return cen


def _fscan_fit(mot, rng, npts, ctim, ctr, sig_sign):
    """Uses fscan for continous scan of dty"""
    from bliss.setup_globals import fscan
    mot.sync_hard()
    plotselect(ctr)
    plotinit(ctr)
    mstart = mot.position
    try:
        fscan(
            mot, mstart - rng, rng * 2 / (npts - 1), npts, ctim
        )  # , scan_mode='CAMERA')
        sleep(1)
        mot.sync_hard()
    finally:
        mv(mot, mstart)
    A = fscan.get_data()
    # fluo signal - take log to "ignore" asymmetry:
    m = A[ctr] < 1e9
    # y = np.log(np.clip(sig_sign*A[ctr], 1, 1e99))
    y = A[ctr][m]
    x = A[mot.name][m]
    bkg = np.min(y)
    pk = np.clip(y - bkg, 0, 1e99)
    cen = (x * pk).sum() / pk.sum()
    print("bkg,cen", bkg, cen)

    return cen


def _align_cor(rng, npts, ctim, ctr="Cu_det0", sig_sign=1):
    """
    Mainly for ID11 nscope as motors hard wired
    Try to align the center of rotation by scanning
    py and px and 0,90,180,270 degrees and fitting a counter
       rng = scan range (um)
       npts = points per scan
       ctim = counting time
       ctr = thing to plot
       sig_sign = positive or negative peak?
    """
    cens = []
    umv(rot, 0)
    cens.append(_dscan_fit(py, rng, npts, ctim, ctr, sig_sign))
    py.sync_hard()
    px.sync_hard()
    umv(rot, 90)
    cens.append(_dscan_fit(px, rng, npts, ctim, ctr, sig_sign))
    py.sync_hard()
    px.sync_hard()
    umv(rot, 180)
    cens.append(_dscan_fit(py, rng, npts, ctim, ctr, sig_sign))
    py.sync_hard()
    px.sync_hard()
    umv(rot, 270)
    cens.append(_dscan_fit(px, rng, npts, ctim, ctr, sig_sign))
    py.sync_hard()
    px.sync_hard()
    pyideal = (cens[0] + cens[2]) / 2
    # umv(py, pyideal
    pxideal = (cens[1] + cens[3]) / 2
    # umv(px, pxideal)
    dtyerr = (cens[0] - cens[2] + cens[1] - cens[3]) / 4
    print("er1, er2", (cens[0] - cens[2]) / 2, (cens[1] - cens[3]) / 2)
    # umvr(dty, dtyerr)
    print(cens)
    print("pxideal, pyideal, dtyerr", pxideal, pyideal, dtyerr)
    return cens


def _align_cor_dty(rng, npts, ctim, ctr="eiger:roi_counters:roi1_avg", sig_sign=1):
    """
    Mainly for ID11 nscope as motors hard wired
    Try to align the center of rotation by scanning
    py and px and 0,90,180,270 degrees and fitting a counter,
       rng = scan range (um)
       npts = points per scan
       ctim = counting time
       ctr = thing to plot
       sig_sign = positive or negative peak?
    """
    cens = []
    # ACTIVE_MG.disable("frelon4")
    # ACTIVE_MG.enable("mca")
    # ct(0.1)
    start_dty = dty.position
    plotselect(ctr)
    try:
        umv(rot, 0)
        cens.append(_fscan_fit(dty, rng, npts, ctim, ctr, sig_sign))
        umv(rot, 90)
        cens.append(_fscan_fit(dty, rng, npts, ctim, ctr, sig_sign))
        umv(rot, 180)
        cens.append(_fscan_fit(dty, rng, npts, ctim, ctr, sig_sign))
        umv(rot, 270)
        cens.append(_fscan_fit(dty, rng, npts, ctim, ctr, sig_sign))
        # print(cens)
        dtyideal = np.mean(cens)
        pyerr = (cens[0] - cens[2]) / 2
        # umvr(py, pyerr)
        pxerr = (cens[1] - cens[3]) / 2
        # umvr(px, pxerr)
        # umv(dty, dtyideal)
        print(cens)
        print("umvr(py,", pyerr, ",px,", pxerr, ")")
        print("umv(dty,", dtyideal, ")")
    finally:
        print("Moving dty back to initial position", start_dty)
        umv(dty, start_dty)


def _align_cor_dty45(rng, npts, ctim, ctr="mca:Cu_det0", sig_sign=1):
    """
    Mainly for ID11 nscope as motors hard wired
    Try to align the center of rotation by scanning
    py and px and 0,90,180,270 degrees and fitting a counter
       rng = scan range (um)
       npts = points per scan
       ctim = counting time
       ctr = thing to plot
       sig_sign = positive or negative peak?
    """
    cens = []
    # ACTIVE_MG.disable("frelon4")
    # ACTIVE_MG.enable("mca")
    ct(0.1)
    plotselect(ctr)
    umv(rot, 45)
    cens.append(_fscan_fit(dty, rng, npts, ctim, ctr, sig_sign))
    umv(rot, 135)
    cens.append(_fscan_fit(dty, rng, npts, ctim, ctr, sig_sign))
    umv(rot, 225)
    cens.append(_fscan_fit(dty, rng, npts, ctim, ctr, sig_sign))
    umv(rot, 315)
    cens.append(_fscan_fit(dty, rng, npts, ctim, ctr, sig_sign))
    print(cens)
    dtyideal = np.mean(cens)
    pxpyerr = (cens[0] - cens[2]) / 2
    pxmyerr = (cens[1] - cens[3]) / 2
    umvr(px, (pxpyerr + pxmyerr) / np.sqrt(2))
    umvr(py, (pxpyerr - pxmyerr) / np.sqrt(2))
    umv(dty, dtyideal)
    print(cens)
    print("dtypos", dtyideal)


def _align_cor_dty_tmot(rng, npts, ctim, ctr="roi1_avg", sig_sign=1):
    """
    Mainly for ID11 nscope as motors hard wired
    Try to align the center of rotation by scanning
    shty and shtx and 0,90,180,270 degrees and fitting a counter,
       rng = scan range (um)
       npts = points per scan
       ctim = counting time
       ctr = thing to plot
       sig_sign = positive or negative peak?
    """
    cens = scan_cor_dty_tmot(rng, npts, ctim, ctr, sig_sign)
    dtyideal = np.mean(cens)
    # pyerr = (cens[0]-cens[2])/2
    tyerr = ((cens[0] - cens[2]) / 2) / 1000
    umvr(shty, tyerr)
    # pxerr = (cens[1]-cens[3])/2
    txerr = ((cens[1] - cens[3]) / 2) / 1000
    umvr(shtx, txerr)
    umv(dty, dtyideal)
    print(cens)
    print("tyerr, txerr, dtypos", tyerr, txerr, dtyideal)


def scan_cor_dty_tmot(rng, npts, ctim, ctr="roi1_avg", sig_sign=1):
    """
    Mainly for ID11 nscope as motors hard wired
    Try to align the center of rotation by scanning
    shty and shtx and 0,90,180,270 degrees and fitting a counter,
       rng = scan range (um)
       npts = points per scan
       ctim = counting time
       ctr = thing to plot
       sig_sign = positive or negative peak?
    """
    cens = []
    # ACTIVE_MG.disable("frelon4")
    # ACTIVE_MG.enable("mca")
    # ct(0.1)
    plotselect(ctr)
    umv(rot, 0)
    cens.append(_fscan_fit_eiger(dty, rng, npts, ctim, ctr, sig_sign))
    umv(rot, 90)
    cens.append(_fscan_fit_eiger(dty, rng, npts, ctim, ctr, sig_sign))
    umv(rot, 180)
    cens.append(_fscan_fit_eiger(dty, rng, npts, ctim, ctr, sig_sign))
    umv(rot, 270)
    cens.append(_fscan_fit_eiger(dty, rng, npts, ctim, ctr, sig_sign))
    print(cens)
    return cens


def make_cens(cens):
    dtyideal = np.mean(cens)
    # pyerr = (cens[0]-cens[2])/2
    tyerr = ((cens[0] - cens[2]) / 2) / 1000
    umvr(shty, -tyerr)
    # pxerr = (cens[1]-cens[3])/2
    txerr = ((cens[1] - cens[3]) / 2) / 1000
    umvr(shtx, -txerr)
    umv(dty, dtyideal)


def _fscan_fit_eiger(mot, rng, npts, ctim, ctr, sig_sign):
    """Uses fscan for continous scan of dty"""
    from bliss.setup_globals import fscan
    mot.sync_hard()
    plotselect(ctr)
    plotinit(ctr)
    mstart = mot.position
    fscan(mot, mstart - rng, rng * 2 / (npts - 1), npts, ctim)  # ,scan_mode='CAMERA')
    sleep(1)
    mot.sync_hard()
    A = fscan.get_data()
    # fluo signal - take log to "ignore" asymmetry:
    m = A[ctr] < 1e9
    # y = np.log(np.clip(sig_sign*A[ctr], 1, 1e99))
    y = A[ctr][m]
    x = A[mot.name][m]
    bkg = np.min(y)
    pk = np.clip(y - bkg, 0, 1e99)
    cen = (x * pk).sum() / pk.sum()
    print("bkg,cen", bkg, cen)
    mv(mot, mstart)
    return cen


def _align_cor_dty_eiger(
    rng, npts, ctim, ctr="eiger:roi_counters:roi1_sum", sig_sign=1, domove=True
):
    """
    Mainly for ID11 nscope as motors hard wired
    Try to align the center of rotation by scanning
    py and px and 0,90,180,270 degrees and fitting a counter,
       rng = scan range (um)
       npts = points per scan
       ctim = counting time
       ctr = thing to plot
       sig_sign = positive or negative peak?
    """
    cens = []
    plotselect(ctr)
    umv(rot, 0)
    cens.append(_fscan_fit_eiger(dty, rng, npts, ctim, ctr, sig_sign))
    umv(rot, 90)
    cens.append(_fscan_fit_eiger(dty, rng, npts, ctim, ctr, sig_sign))
    umv(rot, 180)
    cens.append(_fscan_fit_eiger(dty, rng, npts, ctim, ctr, sig_sign))
    umv(rot, 270)
    cens.append(_fscan_fit_eiger(dty, rng, npts, ctim, ctr, sig_sign))
    # print(cens)
    dtyideal = np.mean(cens)
    pyerr = (cens[0] - cens[2]) / 2
    if domove:
        umvr(py, pyerr)
    pxerr = (cens[1] - cens[3]) / 2
    if domove:
        umvr(px, pxerr)
        umv(dty, dtyideal)
    else:
        print("umvr(py,", pyerr, ",px,", pxerr, ")")
        print("umv(dty,", dtyideal, ")")
    print(cens)


def align_cor_interactive(
    rng,
    npts=None,
    ctim=None,
    ctr="roi1_avg",
    angles=[0, 90, 180],
    nifit=False,
    domove=True,
    alignto=None,
    piezooff=True,
    shtxy=False,
):
    """
    Mainly for ID11 nscope as motors hard wired.
    Try to align the center of rotation by scanning dty
    and using user input of the sample centres or edges
       rng = scan range (um)
       npts = points per scan
       ctim = counting time - if None will find fast speed
       ctr = thing to plot - can be one counter or a list of counters, one for each angle
       angles = rot positions (integers) at which to scan - minimum of 3
       nifit = non-interactive fit (takes COM)
       domove = move to calculated position at end
       alignto = edges (e) or centre (c)? will be prompted if not given
       piezooff = only move sh motors
       shtxy = use hexapod rather than ntx/nty
    """
    if npts is None:
        npts = rng
        if rng < 100:
            npts = 200
    if ctim is None:
        ctim = 2 * (rng / (dty.velocity * 0.4)) / npts
    xdata = {}
    ydata = {}
    dtystart = dty.position
    rotstart = rot.position
    f = flint()
    p = f.get_plot("curve", name="align COR interactive", unique_name="ACI")
    p.clear_data()
    if isinstance(ctr, str):
        ctr = [ctr for i in angles]
    else:
        assert len(angles) == len(ctr), "length of ctr and angles lists do not match"
    for j, k in enumerate(angles):
        umv(rot, k)
        if ctr[j][:3] == "roi":
            psc = "eiger:roi_counters:%s" % ctr[j]
            print("Calling plotselect ", psc)
            plotselect(psc)
        else:
            plotselect(ctr[j])
        if j % 2 == 0:
            xdata["%f degrees" % k], ydata["%f degrees" % k] = _fscan_interactive(
                dty, rng, npts, ctim, ctr[j]
            )
        else:
            xdata["%f degrees" % k], ydata["%f degrees" % k] = _fscan_interactive(
                dty, -rng, npts, ctim, ctr[j]
            )
        p.add_curve(
            xdata["%f degrees" % k], ydata["%f degrees" % k], legend="%f degrees" % k
        )
    if nifit:
        cens = ni_fit(xdata, ydata, angles)
    else:
        cens, edges = interactive_cor_fit(xdata, ydata, angles, alignto, p)
    print("centres =", cens, "\n", "edges =", edges)
    # fit centres
    fitpars, _ = curve_fit(sinfit, angles, cens, [45, 1, 0])
    pyerr = fitpars[1] * np.sin(np.radians(fitpars[0]))
    pxerr = fitpars[1] * np.cos(np.radians(fitpars[0]))
    dtyideal = fitpars[2]
    print("pyerr =", pyerr, ", pxerr =", pxerr, ", dtyideal =", dtyideal)
    if piezooff and domove:
        if shtxy:
            umvr(shty, (pyerr / 1000))
            umvr(shtx, (pxerr / 1000))
        else:
            umvr(nty, (pyerr / 1000))
            umvr(ntx, (pxerr / 1000))
        umv(dty, dtyideal, rot, rotstart)
        return
    if domove:
        if (py.position + pyerr) > 5 and (py.position + pyerr) < 95:
            umvr(py, pyerr)
        else:
            if shtxy:
                umvr(shty, (pyerr / 1000))
            else:
                umvr(nty, (pyerr / 1000))
        if (px.position + pxerr) > 5 and (px.position + pxerr) < 95:
            umvr(px, pxerr)
        else:
            if shtxy:
                umvr(shtx, (pxerr / 1000))
            else:
                umvr(ntx, (pxerr / 1000))
        umv(dty, dtyideal, rot, rotstart)
        return
    else:
        if (py.position + pyerr) > 5 and (py.position + pyerr) < 95:
            print("would do umvr(py, %f)" % pyerr)
        else:
            print("would do umvr(shty, %f)" % (pyerr / 1000))
        if (px.position + pxerr) > 5 and (px.position + pxerr) < 95:
            print("would do umvr(px, %f)" % pxerr)
        else:
            print("would do umvr(shtx, %f)" % (pxerr / 1000))
        print("would do umv(dty,", dtyideal, ")")
        umv(dty, dtystart, rot, rotstart)


def align_cor_interactive_hexapod(
    rng,
    npts=None,
    ctim=None,
    ctr="roi1_avg",
    angles=[0, 90, 180],
    nifit=False,
    domove=True,
    alignto=None,
    piezooff=False,
):
    """
    Mainly for ID11 nscope as motors hard wired.
    Try to align the center of rotation by scanning dty
    and using user input of the sample centres or edges
       rng = scan range (um)
       npts = points per scan
       ctim = counting time - if None will find fastest speed
       ctr = thing to plot
       angles = rot positions (integers) at which to scan - minimum of 3
       nifit = non-interactive fit (takes COM)
       domove = move to calculated position at end
       alignto = edges (e) or centre (c)? will be prompted if not given
       piezooff = only move sh motors
    """
    if npts is None:
        npts = rng
        if rng < 100:
            npts = 200
    if ctim is None:
        ctim = 2 * (rng / (dty.velocity * 0.4)) / npts
    xdata = {}
    ydata = {}
    dtystart = dty.position
    f = flint()
    p = f.get_plot("curve", name="align COR interactive", unique_name="ACI")
    p.clear_data()
    for j, k in enumerate(angles):
        umv(rot, k)
        if j % 2 == 0:
            xdata["%f degrees" % k], ydata["%f degrees" % k] = _fscan_interactive(
                dty, rng, npts, ctim, ctr
            )
        else:
            xdata["%f degrees" % k], ydata["%f degrees" % k] = _fscan_interactive(
                dty, -rng, npts, ctim, ctr
            )
        p.add_curve(
            xdata["%f degrees" % k], ydata["%f degrees" % k], legend="%f degrees" % k
        )
    if nifit:
        cens = ni_fit(xdata, ydata, angles)
    else:
        cens, edges = interactive_cor_fit(xdata, ydata, angles, alignto, p)
    print("centres =", cens)
    print("edges =", edges)
    # fit centres
    fitpars, _ = curve_fit(sinfit, angles, cens, [45, 1, 0])
    pyerr = fitpars[1] * np.sin(np.radians(fitpars[0]))
    pxerr = fitpars[1] * np.cos(np.radians(fitpars[0]))
    dtyideal = fitpars[2]
    print("pyerr =", pyerr, ", pxerr =", pxerr, ", dtyideal =", dtyideal)
    if piezooff and domove:
        umvr(shty, (pyerr / 1000))
        umvr(shtx, (pxerr / 1000))
        umv(dty, dtyideal, rot, 0)
        return
    if domove:
        if (py.position + pyerr) > 5 and (py.position + pyerr) < 95:
            umvr(py, pyerr)
        else:
            umvr(shty, (pyerr / 1000))
        if (px.position + pxerr) > 5 and (px.position + pxerr) < 95:
            umvr(px, pxerr)
        else:
            umvr(shtx, (pxerr / 1000))
        umv(dty, dtyideal, rot, 0)
    else:
        if (py.position + pyerr) > 5 and (py.position + pyerr) < 95:
            print("would do umvr(py, %f)" % pyerr)
        else:
            print("would do umvr(shty, %f)" % (pyerr / 1000))
        if (px.position + pxerr) > 5 and (px.position + pxerr) < 95:
            print("would do umvr(px, %f)" % pxerr)
        else:
            print("would do umvr(shtx, %f)" % (pxerr / 1000))
        print("would do umv(dty,", dtyideal, ")")
        umv(dty, dtystart, rot, 0)


def _fscan_interactive(mot, rng, npts, ctim, ctr, start=0):
    """Uses fscan and returns data"""
    from bliss.setup_globals import fscan
    mot.sync_hard()
    #    plotselect(ctr)
    fscan(mot, start - rng, rng * 2 / (npts - 1), npts, ctim)
    sleep(1)
    mot.sync_hard()
    A = fscan.get_data()
    if isinstance(ctr, str):
        m = A[ctr] < 1e9
        y = A[ctr][m]
    elif isinstance(ctr, list):
        m = A[ctr[0]] < 1e9
        y = A[ctr[0]][m]
    x = A[mot.name][m]
    return x, y


def dscan_interactive(mot, rng, npts, ctim, ctr):
    """Uses dscan and returns data"""
    mot.sync_hard()
    plotselect(ctr)
    plotinit(ctr)
    mstart = mot.position
    dscan(mot, mstart - rng, mstart + rng, npts, ctim)
    lastscan = SCANS[-1]
    sleep(1)
    mot.sync_hard()
    A = lastscan.get_data()
    m = A[ctr] < 1e9
    y = A[ctr][m]
    x = A[mot.name][m]
    sx = A["axis:samtx"][m]
    sy = A["axis:samty"][m]
    mv(mot, mstart)
    return x, sx, sy, y


def sinfit(x, a, b, c):
    """
    a is angle of max displacement
    b is magnitude of displacement
    c is centre of rotation
    """
    return b * np.sin(np.radians(x + a)) + c


def interactive_cor_fit(xdata, ydata, angles, alignto, p):
    # choose centre or edge alignment
    if alignto is None:
        alignto = input("Align using the centre (c) or edges (e)?")
    if alignto in ["c", "centre", "center", "C"]:
        nbp = 1
    elif alignto in ["e", "edge", "edges", "E"]:
        nbp = 2
    else:
        print("I didn't understand the alignto input, defaulting to centre")
        nbp = 1
    # input centres or edges
    cs = []
    c = [
        "b",
        "r",
        "g",
        "m",
        "y",
        "k",
        "b",
        "r",
        "g",
        "m",
        "y",
        "k",
        "b",
        "r",
        "g",
        "m",
        "y",
        "k",
    ]  # plot in same colour
    for i, r in enumerate(angles):
        p.clear_data()
        p.add_curve(
            xdata["%f degrees" % r],
            ydata["%f degrees" % r],
            legend="%f degrees" % r,
            color=c[i],
        )
        print("Select position at ", r, " degrees")
        try:
            cs.append(p.select_points(nbp))
        except:
            print("Try again")
            cs.append(p.select_points(nbp))
    p.clear_data()
    # replot the data
    try:
        for i, r in enumerate(angles):
            p.add_curve(
                xdata["%f degrees" % r],
                ydata["%f degrees" % r],
                legend="%f degrees" % r,
                color=c[i],
            )
    except:
        print("replotting did not work")
    # return dty values
    if nbp == 1:
        return [cs[k][0][0] for k in range(len(angles))], 0
    if nbp == 2:
        edges = []
        for k in range(len(angles)):
            if (cs[k][0][0] / np.abs(cs[k][0][0])) < 0:
                a1 = angles[k] + 180 if angles[k] < 180 else angles[k] - 180
                edges.append([a1, cs[k][0][0]])
            else:
                edges.append([angles[k], cs[k][0][0]])
            if (cs[k][1][0] / np.abs(cs[k][1][0])) < 0:
                a1 = angles[k] + 180 if angles[k] < 180 else angles[k] - 180
                edges.append([a1, cs[k][1][0]])
            else:
                edges.append([angles[k], cs[k][1][0]])

        return [np.mean([cs[k][1][0], cs[k][0][0]]) for k in range(len(angles))], edges


def ni_fit(xdata, ydata, angles):
    cens = []
    for k in angles:
        m = ydata["%f degrees" % k] < 1e9
        y = ydata["%f degrees" % k][m]
        x = xdata["%f degrees" % k][m]
        bkg = np.min(y)
        pk = np.clip(y - bkg, 0, 1e99)
        cens.append((x * pk).sum() / pk.sum())
    return cens


def samr_cor(mot, start, stop, npts, ctim, ctr):
    """displays summed images of samrx, samry scan to find COR"""
    lastscan = ascan(mot, start, stop, npts, ctim, ctr)
    sleep(1)
    A = lastscan.get_data()
    y = A["image"]
    summed = np.mean(y, axis=0)
    f = flint()
    dataplot1 = f.get_plot("image", name="align samr 1")
    dataplot1.side_histogram_displayed = False
    dataplot1.set_data(summed)
    summedm = summed - y[int(npts / 2)]
    dataplot2 = f.get_plot("image", name="align samr 2")
    dataplot2.side_histogram_displayed = False
    dataplot2.set_data(summedm)


def friedelrot(ypin, zpin, distance_px=149.788 / 0.075, cy=1111, cz=1050):
    """
    distance_px = distance in pixels
    cy, cz = beam center in y/z
    yp, zp = spot position in y/z
    returns the rot angle to the Fridel pair at (2cy-yp, 2cz-zp)
    """
    yp = np.asarray(ypin)
    zp = np.asarray(zpin)
    assert yp.shape == zp.shape
    shape = yp.shape
    vout = np.array(
        (np.full(len(yp.ravel()), distance_px), yp.ravel() - cy, zp.ravel() - cz)
    )
    n = vout / np.linalg.norm(vout, axis=0) - np.array((1, 0, 0))[:, np.newaxis]
    r = np.degrees(np.arctan2(-n[0], n[1]) * 2).reshape(shape)
    r = np.where(r > 180, r - 360, r)
    r = np.where(r < -180, r + 360, r)
    if shape != ():
        return r
    return float(r)


# todo : check sign of rotation angle vs +/- y

# todo : roi definitions. Given cy, cz and one roi, add the other 3 and compute the angles


def friedel_rois(roiname, xc=1054.53, yc=1111.29, d=152.06, detector="eiger", motor="rot"):
    """
    Give an roi counter that is on a diffraction spot

    xc = horizontal beam center
    yc = vertical beam center
    distance = sample to detector distance in mm

    """
    det = config.get(detector)
    ctr = det.roi_counters[roiname]
    px = xc - ctr.width // 2
    py = yc - ctr.height // 2
    distance_pixels = d * 1e-3 / np.mean(det.proxy.camera_pixelsize)
    # 180 degrees : x = x, y = 2*cy-py
    det.roi_counters.set("r180", (ctr.x, 2 * py - ctr.y, ctr.width, ctr.height))
    det.roi_counters.set("fr0", (2 * px - ctr.x, 2 * py - ctr.y, ctr.width, ctr.height))
    det.roi_counters.set("fr180", (2 * px - ctr.x, ctr.y, ctr.width, ctr.height))

    domega = friedelrot(
        ctr.x + ctr.width / 2, ctr.y + ctr.height / 2, distance_pixels, xc, yc
    )
    angles = [
        rot.position,
        rot.position + domega,
        rot.position + 180,
        rot.position + domega + 180,
    ]
    counters = [ctr.name, "fr0", "r180", "fr180"]
    cnames = ["%s:roi_counters:%s_sum" % (detector, name) for name in counters]
    print("Expected angles and rois:")
    args = {"angles": angles, "ctr": cnames}
    for i in range(4):
        a = angles[i]
        c = counters[i]
        print(f"rot = {a} ctr = {c} {det.roi_counters[c]}")
    return args


def friedel_rois_2angles(roiname, xc=1054.53, yc=1111.29, d=152.06, detector="eiger", motor="rot"):
    """
    Give an roi counter that is on a diffraction spot

    xc = horizontal beam center
    yc = vertical beam center
    distance = sample to detector distance in mm
    Only return 2 angles if the other 2 angles are not accessible

    """
    det = config.get(detector)
    ctr = det.roi_counters[roiname]
    px = xc - ctr.width // 2
    py = yc - ctr.height // 2
    distance_pixels = d * 1e-3 / np.mean(det.proxy.camera_pixelsize)
    # 180 degrees : x = x, y = 2*cy-py
    det.roi_counters.set("r180", (ctr.x, 2 * py - ctr.y, ctr.width, ctr.height))
    det.roi_counters.set("fr0", (2 * px - ctr.x, 2 * py - ctr.y, ctr.width, ctr.height))
    det.roi_counters.set("fr180", (2 * px - ctr.x, ctr.y, ctr.width, ctr.height))

    domega = friedelrot(
        ctr.x + ctr.width / 2, ctr.y + ctr.height / 2, distance_pixels, xc, yc
    )
    angles = [
        rot.position,
        rot.position + 180,
    ]
    counters = [ctr.name, "r180"]
    cnames = ["%s:roi_counters:%s_sum" % (detector, name) for name in counters]
    print("Expected angles and rois:")
    args = {"angles": angles, "ctr": cnames}
    for i in range(2):
        a = angles[i]
        c = counters[i]
        print(f"rot = {a} ctr = {c} {det.roi_counters[c]}")
    return args
    
# check the rot angle by scanning ? Not sure if it will be 'good enough'


def getmeshpos(scan, ptnum, motors=("py", "pz"), filename=None, scan_num=None):
    """
    scan = bliss scan object

    myscan = dmesh( py, -1, 1, 20, pz, -1, 1, 20, .1 )
    myscan == your scan object

    """
    import h5py

    ans = []
    cmd = "umv("
    if filename is None:
        filename = scan.scan_info["filename"]
    with h5py.File(filename, "r") as hin:
        for motor in motors:
            if scan_num is None:
                scan_num = scan.scan_info["scan_nb"]
            pos = hin[f"{scan_num}.1/measurement/{motor}"][()]
            ans.append(config.get(motor))
            ans.append(pos[ptnum])
            cmd += f"{motor},{pos[ ptnum ]},"
    cmd += ")"
    if input("Execute " + cmd + " (y/n) ?")[0] in "yY":
        print("ok, do move")
        umv(*ans)
    else:
        print(cmd)
    return cmd


def getmeshposauto(scan, ptnum, motors=("py", "pz"), filename=None, scan_num=None):
    """
    scan = bliss scan object

    myscan = dmesh( py, -1, 1, 20, pz, -1, 1, 20, .1 )
    myscan == your scan object

    """
    import h5py

    ans = []
    cmd = "umv("
    if filename is None:
        filename = scan.scan_info["filename"]
    with h5py.File(filename, "r") as hin:
        for motor in motors:
            if scan_num is None:
                scan_num = scan.scan_info["scan_nb"]
            pos = hin[f"{scan_num}.1/measurement/{motor}"][()]
            ans.append(config.get(motor))
            ans.append(pos[ptnum])
            cmd += f"{motor},{pos[ ptnum ]},"
    cmd += ")"
    umv(*ans)


def sxscannew(
    step_size=-0.5,
    count_time=1,
    start=0 + 35,
    end=0 - 35,
    xc=1052,
    yc=1102,
    d=150,
    wvln=0.2846,
):
    from bliss.setup_globals import fscan, nc
    from bliss.scanning import scan_meta
    if step_size >= 0:
        print("WARNING! CrysAlis cannot handle positive steps!! Changing to negative")
        step_size = -step_size
    if abs(start) > 180:
        print("start out of range")
        return
    nsteps = round((end - start) / step_size)
    end = nsteps * step_size + start
    if abs(end) > 180:
        print("end out of range high")
        return
    speed = step_size / count_time
    if speed > 25:  #: rot.velocity:
        print("speed is too fast")
        return

    if xc is None:
        xc, yc = estimate_centrepixels(det2y.position, detz.position)
    if d is None:
        d = frelx.position
    if wvln is None:
        wvln = (
            2
            * (5.34094 / math.sqrt(3))
            * math.sin(math.radians(config.get("llbragg1").position))
        )

    scan_meta_obj = scan_meta.get_user_scan_meta()
    scan_meta_obj.instrument.set(
        "crysalis",  # category
        {"crysalis": {"distance": d, "xcenter": xc, "ycenter": yc, "wavelength": wvln}},
    )

    # plotbeamcenter(xc, yc)
    fscan(rot, start, step_size, nsteps, count_time, scan_mode="CAMERA")
    nc.auto_makesperanto()
