
#spec
#MOTPAR:misc_par_1 = 0,0,75.,0,0,0
#MOTPAR:misc_par_2 = 0,0,0,0,0,0

#SHEXA_CMD["CFG#CS"]["cmd"] 	="Q80=%f Q81=%f Q82=%f Q83=%f Q84=%f Q85=%f Q86=%f Q87=%f Q88=%f Q89=%f Q90=%f Q91=%f Q20=21"
#SHEXA_CMD["CFG#CS"]["narg"]    =12

#SHEXA_CMD["CFG#CS?"]["cmd"] 	="Q20=31"
#SHEXA_CMD["CFG#CS?"]["cmd1"] 	="Q80,12,1"
#SHEXA_CMD["CFG#CS?"]["nrdg"] = 12

def sh_set_user_centre():
    cmd = "Q80=0. Q81=0. Q82=75. Q83=0. Q84=0. Q85=0. Q86=0. Q87=0. Q88=0. Q89=0. Q90=0. Q91=0. Q20=21"
    shtz.controller.protocol().pmac( cmd )
    sleep(1)
    # machine limits only: 4.4.14 in manual:
    shtz.controller.protocol().pmac( "Q80=1 Q20=24" )
    sleep(1)
    shtz

def sh_unset_user_centre():
    cmd = "Q80=0. Q81=0. Q82=0. Q83=0. Q84=0. Q85=0. Q86=0. Q87=0. Q88=0. Q89=0. Q90=0. Q91=0. Q20=21"
    shtz.controller.protocol().pmac( cmd )
    sleep(1)
    shtz.controller.protocol().pmac( "Q80=1 Q20=24" )
    sleep(1)
    shtz
    
def sh_closed_loop_on():
    shtz.controller.protocol().pmac( "Q20=3" )
    sleep(1)
    shtz

def sh_closed_loop_off():
    shtz.controller.protocol().pmac( "Q20=4" )
    sleep(1)
    shtz    
