
import numpy as np
import time

        


def rough():
    lastscan = dscan(llrz1, -0.01, 0.01, 40, 0.05, pico0)
    sleep(1)
    A = lastscan.get_data( )
    y = A[pico0]
    x = A[llrz1]
    peak = x[np.argmax(y)]
    print(peak)
    umv(llrz1,peak)
    return


def fine():
    lastscan = dscan(llrz1, -0.005, 0.005, 50, 0.05, pico0)
    sleep(1)
    A = lastscan.get_data( )
    y = A[pico0]
    x = A[llrz1]
    peak = x[np.argmax(y)]
    print(peak)
    umv(llrz1,peak)
    return peak
    

bendvals = np.linspace(1.4,4,28)

def bendscan():
    for bv in bendvals:
        print(bv)
        umv(llbend1, bv)
        rough()
        newcen = fine()
        print("llbend1, llrz cen")
        print(bv, newcen)
