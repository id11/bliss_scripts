
import time
import pprint
import numpy 
import bliss.comm.serial
from bliss.common.counter import SoftCounter


class cryo600:
    
    def __init__(self, ser_line):
        self.ser_line = ser_line
        # This sleeptime seems to make a big difference...
        self.sleeptime = 0.4

    def version(self):
        version = self.ask( "V$" )
        if version.startswith( b"V" ):
            items = version.split(b":")
            vs = items[1]   # 611
            baud = items[3] # 9600
        return version

    def status(self):
        status = self.ask( "S$" )
        # 'S:S:309.8:0.0:309.8:120.0:$\r'
        if status.startswith(b"S"):
            items = status.split( b":" )
            sd = {
                'slavemode': {b"S":'slave', b'L':'local' } [ items[1] ],
                'setpoint' : float(items[2]),
                't_error'  : float(items[3]),
                't_final'  : float(items[4]),
                'ramprate' : float(items[5]),
                'full_status': status,
                }
            sd['gastemp'] = sd['setpoint']-sd['t_error'] # who knew?
            # pprint.pprint(sd)
            return sd
        else:
            return status

    def rampto(self, setpoint, rate):
        if setpoint > 375 or setpoint  < 28:
            raise Exception("setpoint out of range")
        cmd = "C:R:%.1f:%.1f:$"%( setpoint, rate )
        print("Sending",cmd,"waiting for response")
        response = self.ask( cmd )
        if not response.startswith(b"A:1:$"):
            print("Communication Error")
        else:
            print("OK, command accepted")
        return self.status()
    
    def ask(self, command ):
        self.ser_line.flush()
        for character in command:
            b = character.encode( 'ascii' )
            self.ser_line.write( b )
            time.sleep( self.sleeptime )
        return self.ser_line.readline()

    def read(self ):
        self.ser_line.readline(timeout=0.5)

    @property
    def value(self):
        try:
            return self.status()['gastemp']
        except:
            return numpy.nan
        
    
cryostream = cryo600(
    bliss.comm.serial.Serial(
        'tango://id11/serial/lid112_13',
        parity=bliss.comm.serial.PARITY_NONE,
        baudrate=9600, bytesize=8, stopbits=1) )


cryostreamtemp = SoftCounter( cryostream, name='Cryostream600' )

__all__=["cryostream" , "cryostreamtemp" ]
