#Thin film alignment macros
#Developed by Ellie 2022 

import numpy as np

def center_sample_motors():
    umv(shtx,0,shty,0,shtz,0,shrx,0,shry,0,shrz,0)
    umv(px,50,py,50,pz,50,rot,0,dty,0)

def home_hexapod(cen_motors=False):
    shtz
    shtz.controller.virtual_home()
    sync()
    if cen_motors:
        center_sample_motors()
    print('Hexapod homing complete')
    
def moveto_basler(pinhole_switch=False):
    sheh3.close()
    umv(edoor,0)
    #umv(det2y,-0.007)
    #umv(det2y,-0.0075) # updated on 03/09/2024
    #umv(det2y,0.1050) # updated on 13/09/2024
    #umv(det2y,0.12875) # updated on 26/09/2024
    #umv(det2y,0.15062) # updated on 19/11/2024
    umv(det2y,0.13250) # updated on 24/11/2024
    if pinhole_switch:
        switch_pinhole()
    ACTIVE_MG.disable('eig*')
    ACTIVE_MG.enable('basler_eh32*')
    ACTIVE_MG.disable('*bpm*')
    
def moveto_eiger(scanbeamstop=True):
    sheh3.close()
    umv(edoor,0)
    umv(det2y,-223.94)
    umv(piny,0, pinz,0)
    ACTIVE_MG.enable('eig*')
    ACTIVE_MG.disable('basler_*')
    open_edoor(scanbeamstop=scanbeamstop)

def moveto_fluo():
    fluoin()
    ACTIVE_MG.enable("mca*")

def switch_pinhole():
    if abs(piny.position <= 0.1):
        umv(piny,17.4,pinz,0.425)
    else:
        umv(piny,0,pinz,0)


def close_edoor():
    umv(edoor,0)
    
def open_edoor(scanbeamstop=True):
    #check slits
    if (s9vg.position > 0.31) or (s9hg.position > 0.31):
        ok = input('S9 is large, do you have the right beamsize for eiger?')
        if ok[0] not in 'yY':
            return
    #check eiger beamstop position
    ebsest = estimate_ebspos(det2y.position,detz.position)
    print(ebsest)
    #print(ebsest, np.abs(ebsest[0]-ebsy.position),np.abs(ebsest[1]-ebsz.position))
    if (np.abs(ebsest[0]-ebsy.position) > 0.5) or (np.abs(ebsest[1]-ebsz.position) > 0.5):
        print('Beamstop does not seem to be in the right position')
    #check counts on pico6
    sheh3.open()
    s = ct(1,pico6).get_data('keithley:pico6')[0]
    if s > 4e7:
        print('Counts are high, caution before opening edoor')
    elif s < 1e5:
        print('Counts are very low, is there beam?')
    else:
        print('Counts seem reasonable, OK to open edoor') 
 
    #check estimated beam position
    cps = estimate_centrepixels(det2y.position,detz.position)
    if cps[0] < -5:
        print('Beam may be beyond detector edge, safe edoor opening will not work')
        return
    #scan beamstop
    if scanbeamstop:
        safetoopen = checkbeamstop()   
        if not safetoopen:
            print('Beam stop not in place, will not open edoor')
            return
    #move and check edoor with counts on eiger
    else:
        edoor_edge = estimate_edooredgepos(det2y.position)
        print('Partially opening edoor...')
        for buffer in [-10,-5,-3,-1,1,3,5,10]:
            esafe = moveandcheck_edoor(edoor_edge + buffer, cps)
            if esafe:
                continue
            else:
                return
    print('edoor is safe to open!')
    # umv(dioderx,75)
    diodeout()
    umv(edoor,185)

def moveandcheck_edoor(edoorpos, cps):
    """Moves edoor and checks roi around centre pixels (cps)"""
    cps = [int(cps[0]), int(cps[1])]
    umv(edoor, edoorpos)
    s = loopscan(1,0.1,eiger).get_data('eiger:image')[0][cps[0]-120:cps[0]+120,cps[1]-120:cps[1]+120]
    bsroi = np.mean(s[s<3e9])
    if bsroi > 10:
        print('High counts on eiger, closing edoor')
        umv(edoor,0)
        return False
    else:
        print('Mean counts around beamstop:',bsroi)
        return True

def estimate_centrepixels(det2y,detz):
    """Returns estimate of beam centre on eiger in pixels as [x,y]"""
    return [(13.30579294*det2y) + 4048.466699+12, 
            (13.29787234*detz) + 1084.80907582-13]
     
def estimate_ebspos(det2y,detz):
    """Returns estimate of ebs position as [ebsy,ebsz]"""
    return [(-1*det2y) + -223.94, (-1*detz) + 5.6800]
 
def estimate_edooredge_pixel_from_pos(edoor):
    """Returns edge of edoor in ypixel"""
    return (-13.65*edoor) + 2257.75

def estimate_edooredge_pos_from_pixel(ypixel):
    """Returns edoor pos for edge at ypixel"""
    return (-7.32600733e-02*ypixel) + 1.65402930e+02

def estimate_edooredgepos(det2y):
    """Returns edoor pos to have edoor edge of beam"""
    cp = estimate_centrepixels(det2y,0)
    return estimate_edooredge_pos_from_pixel(cp[0])
    
def plotbeamcenter(xc=None, yc=None):
    if xc is None:
        xc,yc = estimate_centrepixels(det2y.position,detz.position)
    p = flint().get_live_plot(image_detector="eiger")
    p.update_marker("mybc", (xc, yc), "Beam Center")

def checkbeamstop(auto_validate = False):
    # umv(edoor,0,dioderx,0)
    umv(edoor, 0)
    diodein()
    dscan(ebsy,-3.5,3.5,90,0.1,pico7)
    # Please do not goto_cen(). This can be noisy. Very dangerous.
    # goto_cen()
    measured_pos = cen()
    where()    
    if abs( measured_pos - ebsy.position ) > 0.25:
        print("WARNING: it looks like the beamstop is not aligned")
        auto_validate = False
    print(fwhm())
    if not auto_validate:
        ok = input('Can you see a 3mm beamstop there? (y/n)')
        if ok[0] not in 'yY':
            return
    if 3.3-fwhm() > 0.1:
        dscan(ebsz,-3,3,90,0.1,pico7)
        # Please do not goto_cen(). This can be noisy. Very dangerous.
        # goto_cen()
        measured_pos = cen()
        where()    
        if abs( measured_pos - ebsz.position ) > 0.25:
            print("WARNING: it looks like the beamstop is not aligned")
            auto_validate = False
        if abs(fwhm()-3.1) > 0.3:
            print('Poor ebs alignment')
            return False
        else: 
            return True
    elif 3.3-fwhm() > 0.1:
            print('Poor ebs alignment')
            return False
    else:
        return True

def plotqvals(energy, xc=None, yc=None, pixelsize=0.075):
    xl = [1,1/np.sqrt(2),0,-1/np.sqrt(2),-1,-1/np.sqrt(2),0,1/np.sqrt(2)]
    yl = [0,-1/np.sqrt(2),-1,-1/np.sqrt(2),0,1/np.sqrt(2),1,1/np.sqrt(2)]
    lam = 12.39847 / energy
    if xc == None:
        xc, yc = estimate_centrepixels(det2y.position,detz.position)
    plotbeamcenter(xc, yc)
    p = flint().get_live_plot(image_detector="eiger")
    for q in np.arange(1,31,1):
        tth  = 2 * np.arcsin( q * lam / (4 * np.pi) )
        rp = frelx.position * np.tan( tth ) / pixelsize
        for i in range(8):
            xp, yp = rp*xl[i]+xc, rp*yl[i]+yc
            if xp>0 and xp<2068 and yp>0 and yp<2162:  
                p.update_marker("eigerq%i_%i"%(q,i), (xp,yp), "q=%i"%q)

def removeqvals():
    print('Removing q points...')
    p = flint().get_live_plot(image_detector="eiger")
    for q in np.arange(1,31,1):
        for i in range(8):
            p.remove_marker("eigerq%i_%i"%(q,i))
            
def estimateqval(energy, xc=None, yc=None, pixelsize=0.075):
    lam = 12.39847 / energy
    if xc == None:
        xc, yc = estimate_centrepixels( det2y.position, detz.position )
    p = flint().get_live_plot( image_detector="eiger" )
    pix = p.select_points(1)[0]
    xp, yp = pix[0] - xc, pix[1] - yc
    rp = np.sqrt( xp**2 + yp**2 )
    tth = np.arctan( rp * pixelsize / frelx.position )
    q = np.sin( tth / 2 ) * 4 * np.pi / lam
    print('Estimated q value of (%.2f,%.2f) is %.2f AA^-1'%(xp,yp,q))

def set_qroi(energy, qval, qwidth, xc=None, yc=None, startangle=0, endangle=360, pixelsize=0.075):
    lam = 12.39847 / energy
    if xc == None:
        xc, yc = estimate_centrepixels(det2y.position,detz.position)
    tthn  = 2 * np.arcsin( (qval-(qwidth/2)) * lam / (4 * np.pi) ) 
    rpn = frelx.position * np.tan( tthn ) / pixelsize #min q in pixels
    tthx  = 2 * np.arcsin( (qval+(qwidth/2)) * lam / (4 * np.pi) )
    rpx = frelx.position * np.tan( tthx ) / pixelsize #max q in pixels
    eiger.roi_counters.set( 'q=%.2f'%qval, (xc, yc, rpn, rpx, startangle, endangle) )

def diodein():
    from bliss.setup_globals import mot_findlimit
    mot_findlimit(dioderx, -1)

def diodeout():
    from bliss.setup_globals import mot_findlimit
    mot_findlimit(dioderx, 1)

def fluoin():
    if fluoz.position>=150 and mty.position <= 180:
        umv(mty,-100)
        umv(mty,0,fluoz,20)
    else:
        print("[WARNING] Fluo is already in place")
        wm(mty,fluoz)

def fluoout():
    if abs(mty.position) <=1:
        umv(fluoz,100)
        umv(fluoz,180,mty,-200)
    else:
        print("[WARNING] Fluo is already out")
        wm(mty,fluoz)
    
def plotimagecenter(detector=basler_eh32):
    roi = detector.image.roi
    xc, yc = roi[2] / 2, roi[3] / 2
    p = flint().get_live_plot(image_detector=detector.fullname)
    p.remove_marker("%scenter"%detector.fullname)
    p.update_marker("%scenter"%detector.fullname, (xc, yc), text="Image Center", editable=False)

def restartflint():
   import os, time
   os.system("ps aux | grep flint | grep nscope | awk '{print $2}' | xargs kill -9")
   time.sleep(5)
   flint()
    
    
