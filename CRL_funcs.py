import numpy as np
import xraylib

def sumlenslist(ks,ds):
    assert len(ds)==(len(ks)-1), "len(ks) should be one more than len(ds)"
    newK = ks[0]
    newD = 0
    for i in range(len(ds)):
        newK, newD = addlenses(newK, ks[i+1], ds[i]+newD)  
    #print('Compound lenses have power',newK,'and focal distance', 1/newK, 'with effective position', newD, 'behind last lens')
    return newK, newD

def addlenses(k1,k2,d):
    K = k1 + k2 - (k1*k2*d)
    dt2 = (d*k1) / (k1 + k2 - (d*k1*k2))
    return K, dt2

def getcartridge(n,d,r,delta):
    k = r/2*delta
    K, D = sumlenslist([k for i in range(n)],[d for i in range(n-1)])
    deff = (d*(n-1)/2)-D  # Distance of effective position from centre
    return K, deff

dspaces = np.array([49,49,49,49,65,109])
lenspercart = np.array([2,4,8,16,32,64,128])

def calcCRL(material, energy, rho, n, printresult=True):
    assert n%2 == 0, "only even numbers for n allowed"
    delta = 1 - xraylib.Refractive_Index_Re(material, energy, rho)
    kvalues = np.array([getcartridge(nl, 1, 0.2, delta)[0] for nl in lenspercart])

    dshifts = np.array([getcartridge(nl, 1, 0.2, delta)[1] for nl in lenspercart])
    dspshfs = dspaces + [dshifts[i+1] - dshifts[i] for i in range(len(dshifts)-1)]
    
    bsl = np.zeros(7, dtype=int)
    bs = [int(k) for k in list("{0:b}".format(n//2))[::-1]]
    bsl[:len(bs)] = bs
    lastlens = np.argmax(bsl*lenspercart)
    
    if sum(bsl) == 0:
        print('No lenses!')
        return
    ds = findgaps(dspshfs,bsl)
    ks = kvalues[bsl == 1]
    if sum(bsl) == 1:
        k, d = ks[0], 0
    else:
        k, d = sumlenslist(ks,ds)
        
    dffc = sum(dspaces[:lastlens]) + dshifts[lastlens] - d
    srd = 95800 #distance from source to 3dxrd
    sol = np.roots([k, -k*srd, srd])
    focuspos = (1/(k-(1/sol[0]))) + dffc
    sld = sol[0] - dffc
    
    if printresult:
        print('With', n, material,'lenses: \nlens power =', k,
          '\nfocal length =', 1/k, 'mm',
          '\neffective distance from centre of first cartridge =', dffc, 'mm',
          'For focus on 3DXRD:',
          '\ndistance from source to centre of first cartridge =', sld, 'mm',
          '\ndistance from centre of first cartridge to focus =', focuspos, 'mm',
          '\nCheck: sum =', sld+focuspos,' total distance =', srd)
    return 1/k, sld, focuspos
    
def findgaps(ds, bs):
    """sum the gaps, ds, between used lenses, bs"""
    gaps = []
    rg = 0
    second = False
    for k in range(len(bs)):
        if bs[k] == 1:
            if second:
                gaps.append(rg)
            rg = 0
            second = True
        if k < len(ds):
            rg += ds[k]
    return gaps

def solvequad(a,b,c):
    d = b**2-4*a*c # discriminant

    if d < 0:
        print("This equation has no real solution")
    elif d == 0:
        x = (-b + np.sqrt(d)) / (2 * a)
        print("This equation has one solutions: ", x)
    else:
        x1 = (-b + np.sqrt(d)) / (2 * a)
        x2 = (-b - np.sqrt(d)) / (2 * a)
        print("This equation has two solutions: ", x1, " and", x2)
        
def CRLoptions(energy):
    cxlims = [1025,2050]
    ns = np.array(list(range(2,255,2)))
    nosol = True
    print('#lenses \t cx position (mm)(first cartridge to cor distance)')
    for i,n in enumerate(ns):
        fd, ds, fp = calcCRL('Al', energy, 270, n, printresult=False)
        if (fp < cxlims[1]) & (fp > cxlims[0]):
            nosol = False
            print(n,'\t\t', fp)
        if nosol:
            print('No solutions found!!!')
            second = True
        if i < len(ds):
            rg += ds[i]
    return gaps
