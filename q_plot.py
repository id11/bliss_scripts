import numpy as np


def plotbeamcenter(xc=None, yc=None):
    if xc is None:
        xc, yc = tdxrd_estimate_centrepixels(ffdty1.position, ffdtz1.position)
    p = flint().get_live_plot(image_detector="frelon3")
    p.update_marker("frelon3bc", (xc, yc), "Beam Center")


def tdxrd_estimate_centrepixels(dy, dz):
    """Returns estimate of beam centre on eiger in pixels as [x,y]"""
    return [995 + (20 * dy), 1007 - (20 * dz)]


def plotqvals(energy, xc=None, yc=None, pixelsize=0.05):
    xl = [1, 1 / np.sqrt(2), 0, -1 / np.sqrt(2), -1, -1 / np.sqrt(2), 0, 1 / np.sqrt(2)]
    yl = [0, -1 / np.sqrt(2), -1, -1 / np.sqrt(2), 0, 1 / np.sqrt(2), 1, 1 / np.sqrt(2)]
    lam = 12.39847 / energy
    if xc == None:
        xc, yc = tdxrd_estimate_centrepixels(ffdty1.position, ffdtz1.position)
    plotbeamcenter(xc, yc)
    p = flint().get_live_plot(image_detector="frelon3")
    for q in np.arange(5, 35, 5):
        tth = 2 * np.arcsin(q * lam / (4 * np.pi))
        rp = ffdtx1.position * np.tan(tth) / pixelsize
        for i in range(8):
            xp, yp = rp * xl[i] + xc, rp * yl[i] + yc
            if xp > 0 and xp < 2048 and yp > 0 and yp < 2048:
                p.update_marker("frelon3q%i_%i" % (q, i), (xp, yp), "q=%i" % q)


def removeqvals():
    print("This does not work, probably needs a bliss update...")
    print("Use the remove all markers option in flint instead")
    p = flint().get_live_plot(image_detector="frelon3")
    for q in np.arange(5, 35, 5):
        for i in range(8):
            p.remove_marker("frelon3q%i_%i" % (q, i))


def plotbeamcenter_eh1(xc, yc):
    p = flint().get_live_plot(image_detector="frelon6")
    p.update_marker("frelon6bc", (xc, yc), "Beam Center")


def estimateqval(energy, xc=None, yc=None, pixelsize=0.05):
    lam = 12.39847 / energy
    if xc == None:
        xc, yc = tdxrd_estimate_centrepixels(ffdty1.position, ffdtz1.position)
    p = flint().get_live_plot(image_detector="frelon3")
    pix = p.select_points(1)[0]
    xp, yp = pix[0] - xc, pix[1] - yc
    rp = np.sqrt(xp ** 2 + yp ** 2)
    tth = np.arctan(rp * pixelsize / ffdtx1.position)
    q = np.sin(tth / 2) * 4 * np.pi / lam
    print("Estimated q value of (%.2f,%.2f) is %.2f AA^-1" % (xp, yp, q))


def estimateqval_eh1(energy, distance, xc, yc, pixelsize=0.05):
    lam = 12.39847 / energy
    p = flint().get_live_plot(image_detector="frelon6")
    pix = p.select_points(1)[0]
    xp, yp = pix[0] - xc, pix[1] - yc
    rp = np.sqrt(xp ** 2 + yp ** 2)
    tth = np.arctan(rp * pixelsize / distance)
    q = np.sin(tth / 2) * 4 * np.pi / lam
    print("Estimated q value of (%.2f,%.2f) is %.2f AA^-1" % (xp, yp, q))
