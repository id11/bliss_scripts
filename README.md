# ID11 custom Bliss Scripts

This repository contains custom scripts for ESRF - ID11 beamline.

The scripts are written in Python and are intended to be used in Bliss.

The main scripts are located in the `bliss_scripts` folder and can be imported directly in Bliss from `user_script_load('xxx.py')`

Other older scripts are located in the `old` folder and are not intended to be used anymore.

Scripts for specific experiments are located in the `user_exp` folder.

## Main scripts sourced automatically by Bliss

| File Name       | Functions                                                                                                                                                                                                                                                                                                                                                                                                                    | Information |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|
| eh1macros.py    | ws<br/>plotbeamcenter<br/>plotqvals<br/>removeqvals<br/>estimateqvals                                                                                                                                                                                                                                                                                                                                                        |             |
| tdxrdmacros.py  | ws<br/>plotimagecenter<br/>centeredroi<br/>plotbeamcenter<br/>tdxrd_estimate_centrepixel<br/>plotqvals<br/>removeqvals<br/>estimateqvals<br/>align_cor_tdxrd<br/>align_cor_tdxrd_interactive<br/>fscan_interactive<br/>sinfit<br/>interactive_cor_fit<br/>samr_cor                                                                                                                                                           |             |
| nscopemacros.py | ws<br/>center_sample_motors<br/>home_hexapod<br/>moveto_basler<br/>moveto_eiger<br/>close_edoor<br/>open_edoor<br/>moveandcheck_edoor<br/>estimate_centrepixels<br/>estimate_ebspos<br/>estimate_edooredge_pixel_from_pos<br/> estimate_edooredge_pos_from_pixel<br/>estimate_edooredgepos<br/>plotbeamcenter<br/>checkbeamstop<br/> plotqvals<br/>  removeqvals<br/>estimateqval<br/>set_qroi<br/>diodein<br/>diodeout<br/> |             |


## Other scripts

