
import os
from math import sqrt, sin, radians
from bliss.scanning import scan_meta
import os
from pathlib import Path


def makeponi(calibrant = "CeO2"):
    """ Assume you just collected the data picture """
    s = SCANS[-1]

    cmd1 = "cd /data/id11/nanoscope/ponifiles"
    cmd2 = "pyFAI-calib2 %s::%d.1/instrument/eiger "%(
        s.scan_info['filename'], s.scan_info['scan_nb'] ) + \
          "-m /data/id11/nanoscope/Eiger/20200925.msk " + \
          "-c %s "%(calibrant)  + \
          "-D Eiger2_4M " + \
          "-w %f "%(wvln()) + \
          "-l %f "%(frelx.position)  + \
          "--poni1=%f "%( 1e-3*(detz.position + 75 )) + \
          "--poni2=%f "%( 1e-3*(311.5 + det2y.position ))
    # spatial ?
    uname = input("Enter your username for ssh")
    cmd = 'ssh -X %s@crunch "%s && %s"'%(uname, cmd1, cmd2)
    print(cmd)
    os.system("%s "%(cmd))
    loadponi( cmd = cmd  )


def loadponi(pname = None, cmd=""):
    pname = sorted(
        Path("/data/id11/nanoscope/ponifiles"
         ).iterdir(), key=os.path.getmtime)[-1]
    print(pname)
    if input("OK ?")[0] not in 'yY%':
        print("So tell me which one to use please")
        return
    poni = open(pname,"r").read()
    scan_meta_obj = scan_meta.get_user_scan_meta()
    scan_meta_obj.instrument.set("poni",  # category
                                 { "poni" :
                                   {   "poni": poni,
                                       "filename" : str(pname) ,
                                       "cmd": cmd } } )



import string 
import os
from math import sqrt, sin, radians
from bliss.scanning import scan_meta
import os
from pathlib import Path
#import pyFAI, fabio


def wvln():
    return 2*(5.34094/sqrt(3))*sin(radians(config.get('llbragg1').position))



def makesperanto_slurm(wvln, xbeam, ybeam, distance, run, now=True):
    
    s=SCANS[-1]  
    expath=s.scan_info['filename'] 
    r=expath.replace('.h5', '',) 
    datapath=(os.path.dirname(r)) 
    scnb=s.scan_info['scan_nb']
    step=s.scan_info['instrument']['fscan_parameters']['step_size'] 
    cmdpath='%s/scan%.4d' %(datapath, scnb) 
    savepathu=(os.path.dirname(r))
    savepathi='/data/id11/nanoscope/ihch1575/id11/analysis/'
    omegastart= s.scan_info['npoints']/2*step
    cmd0 = "cd %s" %(cmdpath)
    try:
        if SCAN_SAVING.proposal_type == 'visitor':
            exptype = 'user'
        else:
            exptype = 'inhouse'
    except:
        exptype = input("inhouse or user") 
    savepath=savepathu
    if exptype=="user": 
        savepath=savepathu
    print('saving data in %s' % (savepath))
    if exptype=="inhouse":
        #/data/id11/nanoscope/blcxxxx/id11/sample/sample_dataset/scan]
        savepath='%s%s' %(savepathi, datapath)
    print('saving data in %s' % (savepath))

    job_file = os.path.join(savepath,"conversion.job")
    username = input('Enter username: ')
    
    
    with open(job_file,"w") as fh:
        fh.writelines("#!/bin/bash\n")
        fh.writelines("#SBATCH --partition=nice\n")
        fh.writelines("#SBATCH --nodes=1\n")
        fh.writelines("#SBATCH --ntasks=1\n")
        fh.writelines("#SBATCH --cpus-per-task=20\n")
        fh.writelines("#SBATCH --time=01:00:00\n")
        fh.writelines("# job parameters\n")
        fh.writelines("#SBATCH --job-name=maskesperanto\n")
        fh.writelines('echo "[INFO] Job started at $(date) on $(hostname)"\n')
        fh.writelines("# Job steps\n")
        fh.writelines('echo "[INFO] Job step: converting esperanto file"\n')
        fh.writelines("srun -l eiger2crysalis eiger*h5 -w %f --beam %f %f --omega=-%d+index*%f --distance=%f -o %s/esperanto/frame_%d_{index}.esperanto\n" %(wvln, xbeam, ybeam, omegastart, step, distance, savepath,run))
        fh.writelines('echo "[INFO] Job ended at $(date)"\n')

    cmd2 = "sbatch %s"%job_file
    cmd = 'ssh -X "%s@rnice %s && %s"'%(username, cmd0, cmd2)
    if now:
        os.system(cmd)
    else:
        scriptname = os.path.join( datapath, 'make_esperanto_%04d.sh'%(scnb) )
        with open(scriptname, "w") as outf:
            outf.write("#!/bin/sh\n")
            outf.write(cmd + "\n")
        print("Run your conversion later:", scriptname)
        return scriptname


    
#omega from rot

#running in silent mode

# save a command script - can run automatically after a scan


#ai = pyFAI.load("/data/id11/nanoscope/ponifiles/20201105_16h57.poni")
#ai.getFit2D()['tilt'] 
#ai.getFit2D()['centerX'] 
#ai.getFit2D()['centerY'] 
#ai.getFit2D()['directDist']   

#for i in range(1,361,1): 
#    os.rename(r'frame_1.000000_%d.esperanto'%(i),r'frame_1_%.4d.esperanto' %(i))


def makecbf(wvln, xbeam, ybeam):
    
    s=SCANS[-1]  
    expath=s.scan_info['filename'] 
    r=expath.replace('.h5', '',) 
    datapath=(os.path.dirname(r)) 
    scnb=s.scan_info['scan_nb']
    step=s.scan_info['instrument']['fscan_parameters']['step_size'] 
    cmdpath='%s/scan%.4d' %(datapath, scnb) 
    savepathu=(os.path.dirname(r))
    savepathi='/nobackup/crunch3/inhouse_esperanto/'
    cmd0 = "cd %s" %(cmdpath)
    exptype = input("inhouse or user") 
    savepath=savepathu
    if exptype=="user": 
        savepath=savepathu
    print('saving data in %s' % (savepath))
    if exptype=="inhouse":
        #/data/id11/nanoscope/blcxxxx/id11/sample/sample_dataset/scan]
        savepath='%s%s' %(savepathi, datapath)
    print('saving data in %s' % (savepath))
    cmd2 = "eiger2cbf eiger*h5 -w %f --beam %f %f --omega=-180+index*%f -o %s/cbf/frame_{index:04d}.cbf" %(wvln, xbeam, ybeam, step, savepath) #changed %f in %d for run
    cmd = 'ssh -X opid11@crunch "%s && %s"'%(cmd0, cmd2)
    print(cmd)
    os.system("%s "%(cmd))


 #eiger2cbf scan0001/eiger_000*.h5 -o cbf/enstatite_{index:04d}.cbf -w 0.29339 --beam 1074 1100 --offset 1 --dummy -1 --omega='-180+index*0.5' --phi=0 --kappa=0 --debug



def makesperantoHP(wvln, xbeam, ybeam, distance, omegastart, step, sname,  run):


#omegastart, step, name, 
    
    s=SCANS[-1]  
    expath=s.scan_info['filename'] 
    r=expath.replace('.h5', '',) 
    datapath=(os.path.dirname(r)) 
    scnb=s.scan_info['scan_nb']
    #step=s.scan_info['instrument']['fscan_parameters']['step_size'] 
    cmdpath='%s/scan%.4d' %(datapath, scnb) 
    savepathu=(os.path.dirname(r))
    savepathi='/nobackup/crunch3/inhouse_esperanto/'
    #omegastart= s.scan_info['npoints']/2*step
    cmd0 = "cd %s" %(cmdpath)
    exptype = input("inhouse or user") 
    savepath=savepathu
    if exptype=="user": 
        savepath=savepathu
    print('saving data in %s' % (savepath))
    if exptype=="inhouse":
        #/data/id11/nanoscope/blcxxxx/id11/sample/sample_dataset/scan]
        savepath='%s%s' %(savepathi, datapath)
    print('saving data in %s' % (savepath))
    cmd2 = "eiger2crysalis eiger*h5 -w %f --beam %f %f --omega=%d+index*%f --distance=%d -o %s/esperanto/%s_%d_{index}.esperanto" %(wvln, xbeam, ybeam, omegastart, step, distance, savepath, sname, run) #changed %f in %d for run
    cmd = 'ssh -X opid11@crunch "%s && %s"'%(cmd0, cmd2)
    print(cmd)
    os.system("%s "%(cmd))




















