#id11 scripts
for s in ['lenscalc', 'makeponi', 'nc_bliss', 'fitrot', 'nscopemacros', 'easyscans', 'align_cor', 'makeesperanto']:
    user_script_load(s, export_global=True)

#global scripts
for s in ['clicktomove', 'sxmacro']:
    user_script_load(s, export_global=True)
