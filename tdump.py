


def tdump(logname=None):
    """ Have tmux save the screen buffer to a file 
    Uses the capture-pane option

    see also: pipe-pane 
              ... not sure how to avoid big files with that
              ... pipe through screen ?
    """
    import os, time
    from bliss import current_session

    if logname is None:
        logname = "~/logfiles/tmuxlog_" + \
            current_session.name + "_" + time.strftime("%Y%m%dT%Hh%M")

    uid = os.geteuid()
    sockfile = "/tmp/bliss_tmux_%s_%s.sock"%(
        current_session.name, uid )

    command = 'tmux -S %s capture-pane -pS - > %s' % (
              sockfile, logname )
    
    print('Executing:', command)
    os.system(command)
    
