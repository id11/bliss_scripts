
import numpy as np


def offline_fitrot( ):
    """ 
    To be called from the command line to get the data
    """
    import h5py, sys
    h = h5py.File( sys.argv[1], "r")
    camera = sys.argv[2]
    darkscan = sys.argv[3]
    flatscan = sys.argv[4]
    calibscan0 = sys.argv[5]
    calibscan1 = sys.argv[6]
    # surely wrong in general :
    d = h[darkscan]["measurement"][camera][:].mean(axis=0)
    f = h[flatscan]["measurement"][camera][:].mean(axis=0)
    d0 = h[calibscan0]["measurement"][camera][:]
    d180 = h[calibscan1]["measurement"][camera][:]
    # projections
    p0 = ( d0 - d ) / ( f - d )
    p180 = ( d180 - d ) / ( f - d )
    print(p0.shape, p180.shape)
    return fitrot(p0, p180)


def collect_fitrot( expo_time= 0.01,
                   rotmotname="diffrz",
                   ymotname = "samty",
                   ydist = -2.0,
                   shuttername = "sheh3",
                   angles = (0,180),
                   nframes = 10,
                   cameraname = "marana3",
                   Transpose=False,
                   doplot=False
                   ):
    """ To be called in bliss to measure the data """
    from bliss import current_session
    assert nframes > 0
    for x in rotmotname, ymotname, shuttername, cameraname:
        assert x in current_session.object_names, x
    rotmot = config.get(rotmotname) # idiom for this?
    ymot = config.get(ymotname)
    shutter = config.get(shuttername)
    camera = config.get(cameraname)
    from bliss.scanning.group import Sequence
    seq = Sequence(title="tomo_camera_rotation_and_offset", scan_info={"type":"fitrot"})
    with seq.sequence_context() as scan_seq:
        print("Closing shutter for the dark")
        shutter.close()
        drk = loopscan( nframes, expo_time, camera.image,
                        title="dark", run=False )
        scan_seq.add( drk )
        drk.run()
        shutter.open()
        umv( rotmot, angles[0] )
        y0 = ymot.position
        umv( ymot, y0+ydist )
        print("Taking the flat")
        flt = loopscan( nframes, expo_time, camera.image,
                        title="flat", run=False )
        scan_seq.add( flt )
        flt.run()
        umv( ymot, y0 )
        print("Taking the p0")
        p0 = loopscan( nframes, expo_time, camera.image,
                       title="p0", run=False )
        scan_seq.add( p0 )
        p0.run()
        umv( rotmot, angles[1] )
        print("Taking the p180")
        p180 = loopscan( nframes, expo_time, camera.image,
                         title="p180", run=False )
        scan_seq.add( p180 )
        p180.run()
        umv( rotmot, angles[0] )
    return process_fitrot_online( drk, flt, p0, p180, nframes, cameraname, doplot, Transpose)

def sum_scan_frames( scan, camera, nframes ):
    o = scan.get_data(camera)
    if hasattr(o, 'get_image'):
        mysum = o.get_image(0).astype(np.float32)
        for i in range( 1, nframes ):
            mysum += o.get_image( i ).astype(np.float32)
    else:
        mysum = o[0].astype(np.float32)
        for i in range( 1, nframes ):
            mysum += o[i].astype(np.float32)
    return mysum

def process_fitrot_online( drk, flt, p0, p180, nframes, cameraname, doplot=False, Transpose=False):
    """ args are bliss scan objects """
    # processing
    camera = cameraname + ":image"
    dark = sum_scan_frames( drk, cameraname, nframes )
    flat = sum_scan_frames( flt, cameraname, nframes )
    d0   = sum_scan_frames( p0, cameraname, nframes )
    d180 = sum_scan_frames( p180, cameraname, nframes )
    m = np.where(flat != dark , 1./(flat-dark), 1.)
    p0 = m*(d0 - dark)
    p180 = m*(d180 - dark)
    if doplot:
        # fixme - can we do this in flint?
        import pylab as pl
        pl.subplot(211)
        pl.imshow( p0 )
        pl.subplot(212)
        pl.imshow( p180 )
        pl.show()
    if Transpose:
        p0 = p0.T
        p180 = p180.T
    return fitrot( p0, p180 )

def robust_line( x, y, mec = 3):
    """ Fit a line and throw out some outliers """
    p = np.polyfit( x, y, 1)      # polynomial fit
    c = np.polyval( p, x )        # calc value
    e = abs( y - c )              # error
    m = e < np.median( e ) * mec  # median error cutoff
    m0 = len(y)
    k = 0
    while (m.sum() > len(m)//2) and (m.sum() != m0) and k < 5:
        m0 = m.sum()
        p = np.polyfit( x[m], y[m], 1)   # polynomial fit
        c = np.polyval( p, x )        # calc value
        e = abs( y - c )              # error
        m = e < np.median( e ) * mec     # median error cutoff
        #print(p,m0,m.sum(),np.median(e))
        k += 1 # give up at some point
    return p, m

def remove_bg( y, n = 5 ):
    # fixme - makes more sense to drop the low frequency terms...
    x = np.linspace(-1,1,len(y))
    p = np.polyfit( x, y, 5 )
    c = np.polyval( p, x )
    return y - c

def fitrot(p0, p180, npts=5, doplot=True):
    """
    Fit the axis rotation and center for tomo
    """
    # offset vs height
    xj = np.arange( p0.shape[1] ) - p0.shape[1]//2
    offsets = np.zeros( len(p0), dtype=float)
    corr = []
    for i in range(len(p0)):
        c = np.convolve( remove_bg(p0[i]), remove_bg(p180[i]),'same')
        corr.append( c )
        j = min( max( np.argmax(c), npts ), len(xj)-npts-1)
        # Take the highest point and 2 neighbors
        cw = c[j-npts:j+1+npts]
        offsets[i] = np.average(xj[j-npts:j+1+npts],
                                weights = cw - cw.min() )
    xi = np.arange(p0.shape[0]) - p0.shape[0]//2
#    offsets[0]+=100 # test robust
    p, m = robust_line( xi, offsets )
    #print(m)
    s = "Offset %f px, angle %f deg"%(-p[1]/2,np.degrees(p[0]/2))
    print(s)
    if doplot:
        import pylab as pl
        pl.subplot(221)
        pl.plot(xi,offsets, "o")
        pl.plot(xi[m],offsets[m], "o")
        pl.plot(xi, np.polyval(p, xi),"-")
        pl.title(s)
        pl.subplot(222)
        pl.imshow(corr, aspect='auto')
        pl.subplot(223)
        pl.imshow( p0 )
        pl.subplot(224)
        pl.imshow( p180 )
#        pl.figure()
#        pl.plot(corr[512])
        pl.show()

#def nabu_fit():umvr 
#    from nabu.preproc.alignment  import CenterOfRotation
#    cor_finder = CenterOfRotation( poly_deg=2 )
#    print("nabu says:",cor_finder.find_shift( p0, p180 ) )



if __name__=="__main__" and 'current_session' not in globals():
    # print("Warning ! Exec main!")
    offline_fitrot()
