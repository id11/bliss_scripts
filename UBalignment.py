import numpy as np
from scipy.optimize import fsolve
from datetime import datetime

def dfscan(motor,start,stop,npts,ctm,*args):
    c = motor.position
    fscan(motor,c+start,(stop-start)/npts,npts,ctm,*args)
    umv(motor,c)

def rotationmatrix(vector,a):
    """Makes rotation matrix for rotation of angle a (radians) about vector (magnitude 1)"""
    v1,v2,v3 = vector
    return np.array([[np.cos(a)+v1*v1*(1-np.cos(a)), v1*v2*(1-np.cos(a))-v3*np.sin(a), v1*v3*(1-np.cos(a))+v2*np.sin(a)],
                  [v2*v1*(1-np.cos(a))+v3*np.sin(a),    np.cos(a)+v2*v2*(1-np.cos(a)), v2*v3*(1-np.cos(a))-v1*np.sin(a)],
                  [v3*v1*(1-np.cos(a))-v2*np.sin(a), v3*v2*(1-np.cos(a))+v1*np.sin(a),    np.cos(a)+v3*v3*(1-np.cos(a))]])
    
def Sy(sy=samry.position): 
    """Rotation matrix for samry in degrees"""
    sy = np.radians(sy)
    return np.array([[np.cos(sy), 0, np.sin(sy)],[0, 1, 0],[-np.sin(sy), 0, np.cos(sy)]])

def Sx(sx=samrx.position): 
    """Rotation matrix for samrx in degrees"""
    sx = np.radians(sx)
    return np.array([[1, 0, 0],[0, np.cos(sx), -np.sin(sx)],[0, np.sin(sx), np.cos(sx)]])

def wv():
    return 12.39841662513396/energy

def rotatearoundbeam(angle):
    umvr(samry, angle*np.sin(np.radians(diffrz.position)), samrx, np.cos(np.radians(diffrz.position)) )

energy = 44.0

class dctsample():

    def __init__(self,a=1, b=1, c=1, alpha=90, beta=90, gamma=90):
        self.usingUBcombo = False
        self.refinementlist = []
        self.detector = 0
        self.setlattice(a, b, c, alpha, beta, gamma)
        
    def setlattice(self, a1, b1, c1, alpha1, beta1, gamma1):
        """Sets lattice and calculates relevant parameters"""
        self.alpha = np.radians(alpha1)
        self.beta = np.radians(beta1)
        self.gamma = np.radians(gamma1)    
        self.a = a1
        self.b = b1
        self.c = c1
    
        self.setU()
        self.setV()
        self.setLV()        
        self.setRL()
        self.setB()
        self.UB = []
    
    def setV(self):
        """Calculates lattice volume"""
        vv = np.sqrt(1-(np.cos(self.alpha)**2)-(np.cos(self.beta)**2)-(np.cos(self.gamma)**2)+(2*np.cos(self.alpha)*np.cos(self.beta)*np.cos(self.gamma)))
        self.V = self.a*self.b*self.c
    
    def setB(self):
        """Calculates B matrix"""
        self.B = np.array([[self.a_r, self.b_r*np.cos(self.gamma_r), self.c_r*np.cos(self.beta_r)],
                  [0, self.b_r*np.sin(self.gamma_r), -self.c_r*np.sin(self.beta_r)*np.cos(self.alpha)],
                  [0,0,1/self.c]])
    
    def setLV(self):
        """Calculates lattice basis vectors"""
        self.av = np.array([self.a,0,0])
        self.bv = np.array([self.b*np.cos(self.gamma),self.b*np.sin(self.gamma),0])
        self.cv = np.array([self.c*np.cos(self.beta), 
                           self.c*(np.cos(self.alpha)-(np.cos(self.beta))*np.cos(self.gamma))/np.sin(self.gamma), 
                           self.c*self.V/np.sin(self.gamma)])
    
    def setRL(self):
        """Calculates reciprocal lattice"""
        vv = np.sqrt(1-(np.cos(self.alpha)**2)-(np.cos(self.beta)**2)-(np.cos(self.gamma)**2)+(2*np.cos(self.alpha)*np.cos(self.beta)*np.cos(self.gamma)))
        self.a_r = (self.b * self.c * np.sin(self.alpha)) / self.V
        self.b_r = (self.c * self.a * np.sin(self.beta)) / self.V
        self.c_r = (self.a * self.b * np.sin(self.gamma)) / self.V
        self.a_rv = np.cross(self.bv, self.cv) / self.V
        self.b_rv = np.cross(self.cv, self.av) / self.V
        self.c_rv = np.cross(self.av, self.bv) / self.V
        self.alpha_r = np.arccos(((np.cos(self.beta) * np.cos(self.gamma)) - np.cos(self.alpha)) / (np.sin(self.beta) * np.sin(self.gamma)))
        self.beta_r = np.arccos(((np.cos(self.alpha) * np.cos(self.gamma)) - np.cos(self.beta)) / (np.sin(self.alpha) * np.sin(self.gamma)))
        self.gamma_r = np.arccos(((np.cos(self.beta) * np.cos(self.alpha)) - np.cos(self.gamma)) / (np.sin(self.beta) * np.sin(self.alpha)))
            
    def getdspace(self, h, k, l):
        """Returns d space of reflection"""
        return 1/np.linalg.norm(np.dot(self.B,np.array([h,k,l]))) #2*pi?
    
    def setU(self, U1=[[1,0,0],[0,1,0],[0,0,1]]):
        """Set U matrix"""
        self.U = np.array(U1)
        
    def getBhkl(self, h, k, l):
        """Returns Bhkl vector i.e. reflection in sample space"""
        if self.usingUBcombo:
            print('Using combined UB matrix, returning UBhkl')
            return self.getUBhkl(self, h, k, l)
        else:
            return np.dot( self.B, np.array([h,k,l]))
        
    def getUBhkl(self, h, k, l):
        """Returns UBhkl vector i.e. reflection in lab space"""
        if self.usingUBcombo:
            return np.linalg.multi_dot([self.UB, np.array([h,k,l])])
        else:
            return np.linalg.multi_dot([self.U, self.B, np.array([h,k,l])])
    
    def gethklvector(self, h, k, l):
        """Returns Q as a vector in lab frame"""
        return 2 * np.pi * np.linalg.multi_dot([Sy(), Sx(), self.U, self.B, np.array([h,k,l])])
        
    def gethklvector_2(self, h, k, l):
        """Returns Q as a vector in lab frame - 2nd method"""
        return ((h*self.a_rv) + (k*self.b_rv) + (l*self.c_rv)) * 2 * np.pi 

    def findxyz(self,xyz,h,k,l):
        """Inner function for position determination"""
        ubh, ubk, ubl = 2*np.pi*self.getUBhkl(h, k, l)
        x, y, z = xyz
        return [ubh*np.cos(y) + ubk*np.sin(x)*np.sin(y) + ubl*np.cos(x)*np.sin(y),
                ubk*np.cos(x) - ubl*np.sin(x),
                -ubh*np.sin(y) + ubk*np.sin(x)*np.cos(y) + ubl*np.cos(x)*np.cos(y) -(4/wv())*np.sin(z)] # z / 2 ???

    def findpos(self,h,k,l):
        """Function for finding [samrx, samry, diffry] of a reflection in degrees"""
        return np.degrees(fsolve(self.findxyz, np.array([0, 0, 0]), args=(h,k,l)))

    def findubhkl(ubhkl,xyz):
        """Inner function for hkl determination"""
        ubh, ubk, ubl = ubhkl
        x, y, z = xyz
        return [ubh*np.cos(y) + ubk*np.sin(x)*np.sin(y) + ubl*np.cos(x)*np.sin(y),
                ubk*np.cos(x) - ubl*np.sin(x),
                -ubh*np.sin(y) + ubk*np.sin(x)*np.cos(y) + ubl*np.cos(x)*np.cos(y) -(4*np.pi/wv())*np.sin(z)] 
    
    def findhkl_2(self,xyz=np.radians([samrx.position,samry.position,diffry.position])):
        """Function for determining current hkl position"""
        ubhkl = fsolve(findubhkl, [0, 0, 0], xyz)
        if self.usingUBcombo:
            return np.linalg.multi_dot([np.linalg.inv(self.UB), ubhkl]) 
        else:
            return np.linalg.multi_dot([np.linalg.inv(self.B), np.linalg.inv(self.U), ubhkl]) 
        
    def findhkl(self,xyz=[samrx.position,samry.position,diffry.position]):
        """Function for determining current hkl position"""
        if self.usingUBcombo:
            return np.linalg.multi_dot([np.linalg.inv(self.UB), 
                                        np.linalg.inv(Sx(xyz[0])),
                                        np.linalg.inv(Sy(xyz[1])),
                                        [0, 0, np.sin(xyz[2]/2)*4*np.pi*energy/12.398417]/(2*np.pi)]) 
        else:
            return np.linalg.multi_dot([np.linalg.inv(self.B), 
                                        np.linalg.inv(self.U),
                                        np.linalg.inv(Sx(xyz[0])),
                                        np.linalg.inv(Sy(xyz[1])),
                                        [0, 0, np.sin(np.radians(xyz[2])/2)*4*np.pi*energy/12.398417]/(2*np.pi)]) 
        
    def addtorefinementlist(self,h,k,l,sx=samrx.position, sy=samry.position, dry=diffry.position, dtz=None, dtx=nfdtx.position, en=energy):
        """Saves user input hkl and current position to list of 
        saved reflections & positions to use for refinement"""
        now = datetime.now()
        if dtz is None:
            dtz = self.detz()
        reftoadd = [h, k, l, sx, sy, dry, dtz, dtx, en, now.strftime("%d/%m/%Y %H:%M:%S")]
        self.refinementlist.append(reftoadd)
        print('Reflection added to refinement list')
        
    def detz(self):
        if self.detector == 1:
            return d1tz.position
        if self.detector == 2:
            return d2tz.position
        if self.detector == 3:
            return d3tz.position
        print('Could not find specified detector')
        return detz    
    
    def removerefinementlist(self,index):
        """Removes ref from refinement list"""
        print('Removed ',self.refinementlist.pop(index))
    
    def getrefinementlist(self):
        """Prints list of saved reflections & positions to use for refinement"""
        print('index \t h \t k \t l \t samrx \t samry \t theta \t detz \t detx \t energy \t time')
        for k,row in enumerate(self.refinementlist):
            print( '%i \t'%k,' \t '.join(map(str, row)))

    def findU(self,ref1,ref2):
        """refines U matrix using 2 reflections in refinementlist - give index of chosen refs"""
        self.usingUBcombo = False
        if len(self.refinementlist) < np.max([ref1,ref2]):
            print('Could not find reflections in refinement list')
            return
        s1, b1 = self.getUrefinementparameters(ref1)
        s2, b2 = self.getUrefinementparameters(ref2)
        tc = self.makeunitvectors(b1,b2)
        tl = self.makeunitvectors(s1,s2)
        self.U = np.dot(tl,np.transpose(tc))
        print("U matrix updated")
        
    def getUrefinementparameters(self,ref):
        """gets lab and sample vectors for U matrix refinement - give index of chosen ref"""
        r = self.refinementlist[ref]
        s = np.linalg.multi_dot([np.linalg.inv(Sx(r[3])),
                                  np.linalg.inv(Sy(r[4])),
                                 [0, 0, np.sin(r[5]/2)*4*np.pi*r[8]/12.39841662513396]])
        b = np.dot(self.B, np.array([r[0], r[1], r[2]]))
        return s, b

    def makeunitvectors(self,v1,v2):
        t1 = v1/np.linalg.norm(v2)
        t3 = np.cross(v1,v2)/np.linalg.norm(np.cross(v1,v2))
        t2 = np.cross(t3,t1)
        return np.transpose(np.array([t1,t2,t3]))
    
    def findUB(self,ref1,ref2,ref3):
        """Refines UB matrix using 3 reflections in refinementlist - give index of chosen refs"""
        if len(self.refinementlist) < np.max([ref1,ref2,ref3]):
            print('Could not find all reflections in refinement list')
            return
        s1, b1 = self.getUBrefinementparameters(ref1)
        s2, b2 = self.getUBrefinementparameters(ref2)
        s3, b3 = self.getUBrefinementparameters(ref3)
        C = np.transpose(np.array([b1,b2,b3]))
        L = np.transpose(np.array([s1,s2,s3]))
        self.usingUBcombo = True
        self.UB = np.dot(L,np.linalg.inv(C))
        print("UB matrix updated")
    
    def getUBrefinementparameters(self,ref):
        """Gets lab and sample vectors for U matrix refinement - give index of chosen ref"""
        r = self.refinementlist[ref]
        s = np.linalg.multi_dot([np.linalg.inv(Sx(r[3])),
                                  np.linalg.inv(Sy(r[4])),
                                 [0, 0, np.sin(r[5]/2)*4*np.pi*r[8]/12.39841662513396]])
        b = np.array([r[0], r[1], r[2]])
        return s, b
    
    def gotohkl(self,h,k,l):
        """Move to hkl reflection aligned on diffrz axis"""
        a, b, c = self.findpos(h,k,l)
        #d = (nfdtx + self.det_xoff)*np.tan(c) - self.det_zoff
        umv(samrx,a, samry,b, diffry,c)
        #self.movedet(d)
        
    def movedet(self,d):
        """Move the right detector"""
        if self.detector == 1:
            umv(d1tz,d)
            return 
        if self.detector == 2:
            umv(d2tz,d)
            return 
        if self.detector == 3:
            umv(d3tz,d)
            return 
        print('Could not find specified detector') 
    
    def calibratedetector(self,ref1,ref2):
        """Calibrates detector position using 2 reflections in refinementlist - give index of chosen refs"""
        th1, tz1, tx1 = self.refinementlist[ref1][5:8]
        th2, tz2, tx2 = self.refinementlist[ref2][5:8]
        d1, d2 = [th1,th2]*2
        x = (tx2*np.tan(np.radians(d2)) - tx1*np.tan(np.radians(d1)) - tz2 + tz1) / (np.tan(np.radians(d1)) - np.tan(np.radians(d2)))
        z = (tx1 + x)*np.tan(np.radians(d1)) - tz1
        self.det_xoff = x
        self.det_zoff = z
        print('Detector calibration completed')
        



"""
s=user.dctsample(5.43,5.34,5.43)                                                                                            
s.addtorefinementlist(0,0,4,12.6998291571754,-3.2180445860352997,-5.9738992389223515,0.2999999999999998, 40.00024, 43.44)   
s.addtorefinementlist(1,1,3,-12.3999145785877,0.2460984701653507,-4.910090115328945,0.299,43.44)                            
s.findU(0,1) 


rotate around any vector in lab frame
rotation around g to get second reflection 


"""
