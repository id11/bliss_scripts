import numpy as np
import datetime

f = flint()

class correctedimage(object):
    def __init__(self,detector,flatmotor,flatmovement,ctime=0.01):
        self.detector = detector
        self.detectorname = detector.fullname
        self.image_name = '%s:image'%detector.fullname
        self.flatmotor = flatmotor
        self.flatmovement = flatmovement
        self.nframes = 20
        self.ctime = ctime
        self.refim = {}
        
    def takereferenceimages(self,ctime):
        print('Taking reference images...')
        ctname = self.setctime(ctime)
        self.refim[ctname] = {}
        #self.refim[ctname]['time'] = datetime.now()
        self.refim[ctname]['dark'] = self.takedark()
        self.refim[ctname]['flat'] = self.takeflat()
        
    def takedark(self):
        print('Taking dark images...')
        sheh3.close()
        lastscan = loopscan(self.nframes,self.ctime,self.detector)
        dark = np.mean(lastscan.get_data(self.image_name), axis=0)
        print('Dark images aquired')
        sheh3.open()
        return dark
    
    def takeflat(self):
        print('Taking flat images...')
        umvr(self.flatmotor, self.flatmovement)
        try:
            lastscan = loopscan(self.nframes,self.ctime,self.detector)
            flat = np.mean(lastscan.get_data(self.image_name), axis=0)
            print('Flat images aquired')
        except:
            print('Issue taking flats...')
        umvr(self.flatmotor, -self.flatmovement)
        return flat
    
    def ct(self,ctime=None,**kwargs):
        ctname = self.setctime(ctime)       
        if ctname not in self.refim.keys(): 
            self.takereferenceimages(self.ctime)
        try:
            lastscan = sct(self.ctime,**kwargs) #would ct work????????
        except:
            lastscan = SCANS[-1]
        self.correctandplot(lastscan,ctname)
                  
    def umvct(self,motor,movement,ctime=None,**kwargs):
        umv(motor,movement,**kwargs)
        self.ct(ctime)
                  
    def umvrct(self,motor,movement,ctime=None,**kwargs):
        umvr(motor,movement,**kwargs)
        self.ct(ctime)
                  
    def fscan(self,motor,start,stepsize,npts,ctime=None,**kwargs):
        ctname = self.setctime(ctime)       
        if ctname not in self.refim.keys(): 
            self.takereferenceimages(ctime)
        lastscan = fscan(motor,start,stepsize,npts,self.ctime,**kwargs)
        self.correctandplot(lastscan,ctname)
        
    def dscan(self,motor,start,stop,npts,ctime=None,**kwargs):
        ctname = self.setctime(ctime)      
        if ctname not in self.refim.keys(): 
            self.takereferenceimages(ctime)                  
        lastscan = dscan(motor,start,stop,npts,self.ctime,**kwargs)
        self.correctandplot(lastscan,ctname)
                  
    def ascan(self,motor,start,stop,npts,ctime=None,**kwargs):
        ctname = self.setctime(ctime)      
        if ctname not in self.refim.keys():  
            self.takereferenceimages(ctime)
        lastscan = ascan(motor,start,stop,npts,self.ctime,**kwargs)
        self.correctandplot(lastscan,ctname)
        
    def correctandplot(self,lastscan,ctname):
        try:
            image = lastscan.get_data(self.image_name)
        except:
            print('Could not get image - is the detector in ACTIVE_MG?')
        assert image.shape == self.refim[ctname]['dark'].shape, 'Image and reference not same shape - has detector ROI changed?'
        cim = (image - self.refim[ctname]['dark']) / (self.refim[ctname]['flat'] - self.refim[ctname]['dark'])
        flintplot = f.get_plot("image", name='Corrected ct', unique_name='CCT', selected=True)
        flintplot.side_histogram_displayed = False
        flintplot.set_data( cim[0] ) 

    def correctandplotscan(self,lastscan,ctname):
        try:
            images = lastscan.get_data(self.image_name)
        except:
            print('Could not get images - is the detector in ACTIVE_MG?')
        assert images.shape[1:3] == self.refim[ctname]['dark'].shape, 'Image and reference not same shape - has detector ROI changed?'
        cims = (image - self.refim[ctname]['dark']) / (self.refim[ctname]['flat'] - self.refim[ctname]['dark'])
        flintscan = f.get_plot(plot_class="stackview", name="corrected scan", unique_name='Cscn', selected=True)
        flintscan.set_data(cims)
    
    def setctime(self,ctime):
        if ctime == None:
            if self.ctime == None:
                print('Need a count time!!!')
                return
            ctime = self.ctime
        else:
            self.ctime = ctime
        return '%.3f'%ctime     
