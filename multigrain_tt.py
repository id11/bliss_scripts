import json
import time
import os

class NumpyEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.defualt(self,obj)


def writedictionaryfile(dictname,filename):
    """to write dictionary to file"""
    datatowrite = json.dumps(dictname, cls=NumpyEncoder)
    with open('%s.json'%filename, 'w') as f:
        f.write(datatowrite)
    print("Dictionary written as %s.json"%filename)
    return

def appenddictionaryfile(dictname,filename):
    """to write dictionary to file"""
    newdict = {}
    newdict[str(time.time())] = dictname
    if os.path.exists('%s.json'%filename):
	with open('%s.json'%filename, 'r+') as f:
	    data = json.loads(f.read())
	    data.update(newdict)
	    appended_data = json.dumps(data, cls=NumpyEncoder)
            f.seek(0)
	    f.write(appended_data)
    else:
        datatowrite = json.dumps(newdict, cls=NumpyEncoder)
        with open('%s.json'%filename, 'w') as f:
            f.write(datatowrite)
    print("Dictionary appended to %s.json"%filename)
    return

def readdictionaryfile(filename):
    """to read dictionary from file, returns dictionary"""
    with open('%s.json'%filename, 'r') as f:
        readdata = json.loads(f.read())
    print("Dictionary read from %s.json"%filename)
    return readdata


def update_json_grain_info(sample,grain):
    writedictionaryfile(grain,'%s_g%s'%(sample,grain['gr_id']))
    appenddictionaryfile(grain,'%s_g%s_history'%(sample,grain['gr_id']))
    return

def get_json_grain_info(sample,grain_number):
    return readdictionaryfile('%s_g%s'%(sample,str(grain_number)))

gtest = {}
gtest['gr_id'] = 2
gtest['wfwe']=323.05474656464644
gtest['f3'] = [1,4,-2,4]
gtest['ydgrd'] = -55
update_json_grain_info('test',gtest)

