#Auto calibration
#This is design to calibrate and retrigger a workflow of a sample based on CeO2 dataset
#POA 07/2023

#Imports
from bliss.setup_globals import *
import numpy as np
import glob,os,time
import fabio

#Scan sequence (EH1)

def scan_sequence(dist=0, motor=None,ctime=0.1, calibrant='CeO2'):
    newsample(calibrant)
    if motor:
        umv(motor,dist)
        newdataset('%s%s'%(motor,dist))
    else:
        newdataset('calib')
    sheh1.open()
    calib_scan = loopscan(60, ctime)
    
def distance_calibration(det='/data/id11/inhouse1/ewoks/detectors/frelon6/frelon4m.spline', energy=None):
    """
    Calibration with starting parameters from last poni made.
    The ponies are stored in ewoks path
    """
    import pyFAI
    from pyFAI.calibrant import CALIBRANT_FACTORY
    from pyFAI.goniometer import SingleGeometry
    frames = lastscan.get_data()
    frame = np.average(frames['1.1/measurement/%s'%det],axis=0)
    detector = pyFAI.detectors.FReLoN(splineFile=det)
    ponies = sorted(glob.glob(os.path.dirname(det)+'/*.poni'))
    last_poni = pyFAI.load(ponies[-1])
    ceria = CALIBRANT_FACTORY("CeO2")
    ceria.wavelength = last_poni.wavelength
    if energy:
        ceria.wavelength = 12.398 / energy / 1e10
        last_poni.wavelength = 12.398 / energy / 1e10
    newdist, newponi1, newponi2 = compare_geometry(detector.pixel1,last_poni)
    updatepars = input("Do you want to update parameters for the new fit ? (y/n)")
    if 'yY' in updatespars:
        last_poni.dist = newdist
        last_poni.poni1 = newponi1
        last_poni.poni2 = newponi2
    sg = SingleGeometry("New", frame, calibrant=ceria, detector=detector, geometry=last_poni)
    sg.extract_cp(max_rings=20)
    sg.geometry_refinement.refine2(fix=["rot1", "rot2", "rot3", "wavelength"])
    sg.get_ai()
    sg.geometry_refinement.save(os.path.join(os.path.dirname(ponies[-1]),'CeO2_%S.poni'%time.strftime("%Y%m%d_%H%M")))

def compare_geometry(pixel_size,last_poni):
    """
    This compares the sample detector position and the poni from last calibration with current setup
    """
    #Last calibration 20230718 > detx + fx + hx = 97.06 + 137.20 + 0 (diffx=15.39) = 234.26 for calib dist 240.0448
    #                          > Poni1 = 0.051493, Poni2 = 0.049060 > 1029.86 on Z, 981.2 on Y for fz = 9.10, dety = 48
    XOFFSET = 240.0448 - 234.26
    detxpos = frames['1.1/instrument/positioners/detx'][:]
    fxpos = frames['1.1/instrument/positioners/fx'][:]
    hxpos = frames['1.1/instrument/positioners/hx'][:]
    xtotal = detxpos + fxpos + hxpos
    currentdistm = (xtotal+XOFFSET)/1000
    distdiff = currentdistm - last_poni.dist
    print('Sample-detector distance is approximately %s m'%())
    if distdiff <0:
        print('Sample-detector has decreased by %sm'%distdiff)
    else:
        print('Sample-detector has increased by %sm'%distdiff)
    YCOFFSET =  51.493 + 9.1
    XCOFFSET =  49.060 - 48
    yc = last_poni.poni1/pixel_size
    xc = last_poni.poni2/pixel_size
    detypos = frames['1.1/instrument/positioners/dety'][:]
    fzpos = frames['1.1/instrument/positioners/fz'][:]
    diffponi1 = (-fzpos + YCOFFSET)/1000
    diffponi2 = (detypos + XCOFFSET)/1000
    print('Based on last poni values, beam was approximately on pixel X = %4d, Y = %4d'%(xc,yc))
    print('With dety and fz postions, beam is now approximately on pixel X = %4d, Y = %4d'%(diffponi2/pixel_size,diffponi1/pixel_size))
    print('New poni are: poni1: %s, poni2: %s'%(diffponi1,diffponi2))
    return currentdistm, diffponi1, diffponi2
                                
def calib_auto_edge(energy):
    scan_sequence()
    distance_calibration(energy)
