from bliss.scanning import scan_meta
import math



def sxscan(step_size, count_time, start, end, xc=None, yc=None, d=None, wvln=None):
    from bliss.setup_globals import fscan, nc
    if step_size >= 0:
        print('WARNING! CrysAlis cannot handle positive steps!! Changing to negative')
        step_size = -step_size
    if abs(start) > 180:
        print('start out of range')
        return
    nsteps = round((end-start)/step_size)
    end = nsteps * step_size + start
    if abs(end) > 180:
        print('end out of range high')
        return        
    speed = step_size / count_time
    if speed > 25: #: rot.velocity:
        print('speed is too fast')
        return

    if xc is None:
        xc, yc = estimate_centrepixels(det2y.position,detz.position)
    if d is None:
        d = frelx.position
    if wvln is None:
        wvln= 2*(5.34094/math.sqrt(3))*math.sin(math.radians(config.get('llbragg1').position))
    
    scan_meta_obj = scan_meta.get_user_scan_meta()
    scan_meta_obj.instrument.set("crysalis",  # category
                                   {   "crysalis":
                                     {   "distance": d,
                                       "xcenter" : xc,
                                       "ycenter" : yc, 
                                       "wavelength" : wvln } } )
                                       
    #plotbeamcenter(xc, yc)                                       
    fscan(rot, start, step_size, nsteps, count_time)
    nc.auto_makesperanto()
    
            

