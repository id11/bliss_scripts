## Removed from global scripts

########################################
# pars had been added to user profile...        #
# http://lid11control.esrf.fr:9030/    #
#   find file sessions/tdxrd_setup.py  #
########################################

# -*- coding: utf-8 -*-
import os
import numpy as np

difftz_dct = -42.97 

def define_abs_pars():
    abs_pars={}
    abs_pars['start_pos'] = 0
    abs_pars['step_size'] = 1
    abs_pars['num_proj'] = 360 / abs_pars['step_size']  
    abs_pars['exp_time'] = 0.1
    abs_pars['refon'] = abs_pars['num_proj']  
    abs_pars['ref_step'] = 1
    abs_pars['scan_mode'] = 'TIME'
    abs_pars['nref'] = 21
    abs_pars['slit_hg'] = 1.2
    abs_pars['slit_vg'] = 2
    abs_pars['dist'] = 211.5
    abs_pars['ref_mot'] = samy
    abs_pars['zmot'] = difftz
    abs_pars['samtz_cen'] = difftz_dct
    abs_pars['shift_step_size'] = 0.35
    abs_pars['nof_shifts'] = 1
    abs_pars['scan_type'] = 'fscan'
    abs_pars['samrx_range'] = 3
    abs_pars['samry_range'] = 3
    abs_pars['num_tiltx'] = 3
    abs_pars['num_tilty'] = 3
    return abs_pars

abs_pars = define_abs_pars()

def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.05
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.2
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = 1
    dct_pars['scan_mode'] = 'TIME'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.9
    dct_pars['slit_vg'] = 0.38
    dct_pars['dist'] = 211.5
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = difftz
    dct_pars['samtz_cen'] = difftz_dct
    dct_pars['shift_step_size'] = 0.35
    dct_pars['nof_shifts'] = 5
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars

dct_pars = define_dct_pars()

def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = 0
    pct_pars['step_size'] = 0.2
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.07
    pct_pars['refon'] = pct_pars['num_proj']
#    pct_pars['ref_int'] = 10
    pct_pars['ref_step'] = -1
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 100
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = difftz
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = difftz
    pct_pars['samtz_cen'] = difftz_dct
    pct_pars['shift_step_size'] = 1
    pct_pars['nof_shifts'] = 2
    pct_pars['slit_hg'] = 1.3
    pct_pars['slit_vg'] = 1.3
    pct_pars['dist'] = 210
    pct_pars['scan_type'] = 'fscan'
    return pct_pars
    
pct_pars = define_pct_pars()

