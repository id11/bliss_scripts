

def forward_scan(stepsize=0.25, exptime = 0.1):
    N = int( 360 / stepsize )
    assert abs(N * stepsize - 360.) < 1e-4 
    pico6.auto_range=True
    ct(1)
    pico6.auto_range=False
    # put range into header
    finterlaced( rot, 0, stepsize, N, exptime, mode="FORWARD" )


def stepscan():
    stepsize=0.5
    start = 0
    end = 360.01
    while start < end:
        fscan(rot, start, stepsize, 1, 4)
        start += stepsize
