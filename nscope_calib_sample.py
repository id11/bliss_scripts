#POA 07/2022
#These are the positions of the CoR of key features on the calib sample 
#Si, AuZ 5um wire, Lumilux, CeO2
#At rot=0 on microscope, CeO2 is on the bottom left, Lumilux on the bottom right
#These values might change at some point, these are just helping to get faster

def move_to_vcross():
    home_hexapod(True)
    umv(shtx,0.8018,shty,-0.0088,shtz,0.2501) #v
    #umv(shtx,0.8018,shty,-0.0410,shtz,0.1502) #h
    #umv(shtx,0.71665,shty,-0.11569,shtz,0.30016)
    
def move_to_ceo2():
    home_hexapod(True)
    umv(shtx,0.88036,shty,-0.092,shtz,0.55018)
    #umv(shtx,0.88036,shty,-0.092,shtz,0.55018)

def move_to_lumilux():
    home_hexapod(True)
    umv(shtx,0.48144,shty,-0.10599,shtz,0.55017)
    
def move_to_si():
    home_hexapod(True)
    umv(shtx,0.66878,shty,-0.20289,shtz,-0.09978)
