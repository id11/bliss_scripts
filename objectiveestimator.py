import numpy as np
from scipy import ndimage
from scipy.signal import savgol_filter

def objectiveestimator(detector, ctim,y,z):
    """takes image of slits roughly centred on detector to estimate the lens objective"""
    #collect data
    lastscan = loopscan(2, ctim, detector)
    y = lastscan.get_data('%s:image'%detector.fullname)[0]
    osp = s8hg.position
    umv(s8hg, 0.2)
    lastscan = loopscan(2, ctim, detector)
    z = lastscan.get_data('%s:image'%detector.fullname)[0]
    umv(s8hg, osp)

    #analyse data
    w = _objcalc(y,z)
    objs = np.array([5,7.5,10,20])
    if detector == marana:
        px = 11
    if detector == frelon11:
        px = 11 ############### check
    ess = 200 / (px / objs)
    lens = np.argmin( abs( ess - w ) )
    print('Estimated lens magnification is %ix'%objs[lens])
    

def _objcalc(y,z):
    """analyses slit image"""
    x = np.mean(z-y, axis = 0)
    x = x - x.min()
    xc = int(ndimage.measurements.center_of_mass(x)[0])
    x = abs( savgol_filter(x, 51, 1) - (x[xc]/2))
    return (np.argmin(x[xc:]) + xc) - np.argmin(x[:xc])
