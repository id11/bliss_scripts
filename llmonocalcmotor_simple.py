
"""
 Compute the true Bragg angles from the various motor positions
 for a single crystal bender and rotation axis.

https://docs.google.com/presentation/d/1lYgPLldDJUbVW9DIwxCdsFKOheoN2SNToVjCANLmb_g/edit?usp=sharing

 Input motor positions are:
  Axis 1:  llrz1 / llbend1 / llty1
  Axis 2:  llrz2 / llbend2 / llty2

 Simple case

  Number being computed:
   - Bragg angle in degrees
"""


from bliss.controllers.motor import CalcController
from bliss.common.logtools import log_info

class LLAngle(CalcController):

    def initialize(self):
        CalcController.initialize(self)
        self.bzero  = self.config.get("bend_zero", float)
        self.yb  = self.config.get("bend_y", float)
        self.y0  = self.config.get("ty_zero", float)

    
    def calc_from_real( self, positions_dict ):
        log_info(self, "LLAngle calc_from_real()")
        log_info(self, "LLAngle real: %s"%(positions_dict))
        # Angle due to pusher not being a rotation
        # Effect of bending
        # Effect of translation
        ty = positions_dict['ty']
        bend = positions_dict['bend']
        rz = positions_dict['rz']
        # Constants:
        bend_offset = np.degrees( ( bend - self.bzero ) * ( ty - self.y0 ) / self.yb )
        # only for bent crystal and mono in beam
        valid = (bend > self.bzero) & (abs(ty-self.y0) < 50.)
        angle = np.where( valid, rz + bend_offset, rz )
        calc_dict = { "angle"    : angle , # computed
                      "truebend" : bend - self.bzero,     # pass through
                      "absty" : ty - self.y0,         # pass through
        }
        log_info(self, "LLangle %s"%(angle))
        return calc_dict
                      
    def calc_to_real(self, positions_dict ):
        log_info(self, "LLBragg calc_to_real()")
        log_info(self, "LLBragg calc: %s"%(positions_dict))
        #
        angle = positions_dict["angle"]
        # Effect of bending
        bend = positions_dict[ "truebend" ]
        # Effect of translation
        ty  = positions_dict["absty"]      # llty1 / llty2
        # Assume we go to the destination ty / bend.
        # Compute the effect for the angle only
        calc_dict = { "bend" : bend + self.bzero,
                      "ty"   : ty + self.y0 }
        bend_offset = np.degrees( ( bend - self.bzero ) * ( ty - self.y0 ) / self.yb )
        # only for bent crystal and mono in beam
        valid = (bend > self.bzero) & (abs(ty-self.y0) < 50.)
        # - versus + above:
        rz = np.where( valid, angle - bend_offset, angle )
        calc_dict['rz']     = rz
        log_info(self,"LLrz: %s"%(str(rz)))
        return calc_dict
        
"""
# ID11 LaueLaue Monochromator

LL_BEND1_FLAT = -0.90
LL_BEND1_BENT = 1.525 #
LL_BEND2_FLAT = 1.000
LL_BEND2_BENT = 3.000 
#                   bent    -   flat
LL_BEND1_ANGDIFF = 3.893346 - 3.8324  # at Sn edge
LL_BEND2_ANGDIFF = 2.58085  - 2.6370 # at Nd edge
"""
        
        
    
