# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 11. Feb 11:30:00 CET 2022
#
#----------------------------------------------------------------------
# Description: 
#       reproduce functionality of the twc functiuon in SPEC
#
#1803.SLITS> twc nix 0
#Cursor right/left  : move motor 1 positive / negative
#1/2                : half / double the step sizes
#l                  : toggle 'limit to single step size' flag  << not implemented
#+/-                : use the initial or its inverted direction
#s                  : return to start position
#Space              : stop moving motors   << not implemented.
#Esc / q            : quit twc
#
#
#
#
# TODO:
#      optimise output with stdout
#----------------------------------------------------------------------



import sys
import click


from bliss.shell.standard import umv, umvr, mv, mvr
from bliss.common.scans import ct
from bliss import current_session, setup_globals, global_map

#from id01.scripts.ID01_utils import ANSI

class ANSI():
    """
    background: allows background formatting. Accepts ANSI codes between 40 and 47, 100 and 107
    style_text: corresponds to formatting the style of the text. Accepts ANSI code between 0 and 8
    color_text:  Corresponds to the text of the color. Accepts ANSI code between 30 and 37, 90 and 97
    """
    def background(code,string):
        return "\33[{code}m{string}\33[0m".format(code=code,string=string)
  
    def style_text(code,string):
        return "\33[{code}m{string}\33[0m".format(code=code,string=string)
  
    def color_text(code,string):
        return "\33[{code}m{string}\33[0m".format(code=code,string=string)

    
def twc(mot0obj, step0, mot1obj = None, step1 = None, exposure = None, verbose = False):
    """
    User tweak function, up to two motors, a la SPEC twc
    """
    
    # workaround for mot1obj name being an exposure time
    try:
        eval("setup_globals."+mot1obj.name)
    except AttributeError:
        #print("mot1obj is not an axis")
        try:
            # workaround so transparent
            if exposure is None:
                exposure = mot1obj
            mot1obj = None
        except:
            pass
    
    
    ## TODO add exception for catching bad parameters i.e twc(mot,0.1,0.1) instead of twc(mot,0.1,exposure = 0.1)
    keyboard_dict = {"":"",
                    "cursor_left":'\x1b[D',
                    "cursor_right":'\x1b[C',
                    "cursor_up":'\x1b[A',
                    "cursor_down":'\x1b[B',
                    "1":"1",
                    "2":"2",
                    "+":"+",
                    "-":"-",
                    "l":"l",
                    "start":"s",
                    "space":" ",
                    "esc":'\x1b',
                    "q":"q"
                    }
                    
    keyboard_dict_inv = {v:k for k,v in keyboard_dict.items()}
                    
    doc_string_mot0 = "Cursor right/left  : move motor 0 positive / negative\n"
    doc_string_mot1 = "Cursor up/down     : move motor 1 positive / negative\n"
    doc_string_all =  "1/2                : half / double the step sizes\n"
    #doc_string_all += "l                  : toggle 'limit to single step size' flag\n"
    doc_string_all += "+/-                : invert step direction\n"
    doc_string_all += "s                  : return to start position\n"
    #doc_string_all += "Space              : stop moving motors\n"
    doc_string_all += "Esc / q            : quit twc\n"

    help_string_mot0 = doc_string_mot0 + doc_string_all
    help_string_mot1 = doc_string_mot0 + doc_string_mot1 + doc_string_all


    motion_start_mot0 = (mot0obj,mot0obj.position,)
    motion_step_mot0 = (mot0obj,step0,)
    help_start_pos = f"--Start position: {mot0obj.name} = {mot0obj.position:.5f}                 "
    
    if mot1obj is not None:
        motion_start_mot1 = (mot1obj,mot1obj.position,)
        motion_step_mot1 = (mot1obj,step1,)
        help_start_pos += f",  {mot1obj.name} = {mot1obj.position:.5f}"
        print(help_string_mot1)
    else:
        motion_start_mot1 = ()
        motion_step_mot1 = ()
        print(help_string_mot0)        

    def scale_step(scale_factor, atuple):
        """
        scale a tuple (motobj,motstep,),
        """
        return [item*scale_factor if (index and not (index+1)%2) else item for index,item in enumerate(atuple)]

    # user interaction
    motion_ref_mot0_start = motion_start_mot0
    motion_ref_mot1_start = motion_start_mot1
    toggle_ct = None
    
    ans = ""    
    while (keyboard_dict_inv[ans] != "q" and keyboard_dict_inv[ans] != "esc"):
        # 
        try:
            if keyboard_dict_inv[ans] == "cursor_left":
                tmp_motion = scale_step(-1,motion_step_mot0)
                if verbose:
                    umvr(*tmp_motion)
                else:
                    mvr(*tmp_motion)
                toggle_ct = True

            
            elif keyboard_dict_inv[ans] == "cursor_right":
                tmp_motion = scale_step(1,motion_step_mot0)
                if verbose:
                    umvr(*tmp_motion)
                else:
                    mvr(*tmp_motion)
                toggle_ct = True

            elif keyboard_dict_inv[ans] == "cursor_up":
                tmp_motion = scale_step(1,motion_step_mot1)
                if verbose:
                    umvr(*tmp_motion)
                else:
                    mvr(*tmp_motion)
                toggle_ct = True
                
            elif keyboard_dict_inv[ans] == "cursor_down":
                tmp_motion = scale_step(-1,motion_step_mot1)
                if verbose:
                    umvr(*tmp_motion)
                else:
                    mvr(*tmp_motion)
                toggle_ct = True
                
            elif keyboard_dict_inv[ans] == "1":
                motion_step_mot0 = scale_step(0.5,motion_step_mot0)
                motion_step_mot1 = scale_step(0.5,motion_step_mot1)
                toggle_ct = False
                    
            elif keyboard_dict_inv[ans] == "2":
                motion_step_mot0 = scale_step(2,motion_step_mot0)
                motion_step_mot1 = scale_step(2,motion_step_mot1) 
                toggle_ct = False
                
            elif keyboard_dict_inv[ans] == "+":
                motion_step_mot0 = scale_step(-1,motion_step_mot0)
                motion_step_mot1 = scale_step(-1,motion_step_mot1)
                toggle_ct = False

            elif keyboard_dict_inv[ans] == "-":
                motion_step_mot0 = scale_step(-1,motion_step_mot0)
                motion_step_mot1 = scale_step(-1,motion_step_mot1) 
                toggle_ct = False
                               
            elif keyboard_dict_inv[ans] == "start":
                if verbose:
                    umv(*(motion_ref_mot0_start+motion_ref_mot1_start))
                else:
                    mv(*(motion_ref_mot0_start+motion_ref_mot1_start))
                toggle_ct = True
                
            elif keyboard_dict_inv[ans] == "space":
                toggle_ct = False
                pass

            elif keyboard_dict_inv[ans] == "l":
                toggle_ct = False
                pass
                
        except:
            pass
            
        current_params = "Current position: "
        
        current_params+= f"{mot0obj.name} = {mot0obj.position:.5f} , step = {motion_step_mot0[1]:.5f}"
        
        if mot1obj is not None:
            current_params += f",  {mot1obj.name} = {mot1obj.position:.5f}, step = {motion_step_mot1[1]:.5f}"
        
        if (exposure is not None) and toggle_ct:
            ct(exposure)
        
        sys.stdout.write(  ANSI.color_text(31,help_start_pos+"\r\n") +
                                 ANSI.color_text(32,current_params+"\r\n"))
                                 
        # TODO clean with the curses module for reducing output in window
        ans = click.getchar(echo=False)
        
        while list(keyboard_dict_inv.keys()).count(ans) == 0:
            ans = click.getchar(echo=False)
            print("...please use another key - valid keys are:\n")

            if mot1obj is not None:
                print(help_string_mot1)
            else:
                print(help_string_mot0) 
                
        ## use for key debug
        #print(keyboard_dict_inv[ans])
        sys.stdout.flush()
    



def twcnmot(*motion, exposure = None, verbose = False):
    """
    User tweak function, up to two motors, a la SPEC twc
    """
    mot_step = dict() 
    for m, step in zip(global_map.get_axis_objects_iter(*motion[::2]), motion[1::2]): 
        mot_step[m] = step 
    #return mot_step    
    
    ## TODO add exception for catching bad parameters i.e twc(mot,0.1,0.1) instead of twc(mot,0.1,exposure = 0.1)
    keyboard_dict = {"":"",
                    "cursor_left":'\x1b[D',
                    "cursor_right":'\x1b[C',
                    "1":"1",
                    "2":"2",
                    "+":"+",
                    "-":"-",
                    "l":"l",
                    "start":"s",
                    "space":" ",
                    "esc":'\x1b',
                    "q":"q"
                    }
                    
    keyboard_dict_inv = {v:k for k,v in keyboard_dict.items()}
                    
    doc_string_mot = "Cursor right/left  : move motor 0 positive / negative\n"
    doc_string_all =  "1/2                : half / double the step sizes\n"
    #doc_string_all += "l                  : toggle 'limit to single step size' flag\n"
    doc_string_all += "+/-                : invert step direction\n"
    doc_string_all += "s                  : return to start position\n"
    #doc_string_all += "Space              : stop moving motors\n"
    doc_string_all += "Esc / q            : quit twc\n"

    help_string_mot = doc_string_mot + doc_string_all

    print(help_string_mot)
    
    # make the start position list
    help_start_pos = f"--Start position: "
    
    mot_start_pos = dict()
    
    for motObj in mot_step.keys():
        print(motObj.name,motObj.position)
        help_start_pos += f"  {motObj.name} = {motObj.position:.5f}"
        mot_start_pos[motObj.name] = motObj.position    
        
    motion_start = ()
    for item, val in mot_start_pos.items():
        motion_start += (current_session.env_dict[item],val,)

    def scale_step(scale_factor, atuple):
        """
        scale a tuple (motobj,motstep,),
        """
        return [item*scale_factor if (index and not (index+1)%2) else item for index,item in enumerate(atuple)]

    def scale_step_dict(scale_factor, adict):
        """
        scale dictionary off steps, motObj:step
        """
        tmp_dict = dict()
        for motObj in mot_step.keys():
            tmp_dict[motObj] = adict[motObj]*scale_factor
        motion = ()
        for item, val in tmp_dict.items():
            motion += (item,val,) 
        return motion, tmp_dict

    # user interaction
    toggle_ct = None
    
    ans = ""    
    while (keyboard_dict_inv[ans] != "q" and keyboard_dict_inv[ans] != "esc"):
        # 
        try:
            if keyboard_dict_inv[ans] == "cursor_left":
                tmp_motion, tmp_dict = scale_step_dict(-1,mot_step)
                if verbose:
                    umvr(*tmp_motion)
                else:
                    mvr(*tmp_motion)
                toggle_ct = True

            
            elif keyboard_dict_inv[ans] == "cursor_right":
                tmp_motion, mot_step = scale_step_dict(1,mot_step)
                if verbose:
                    umvr(*tmp_motion)
                else:
                    mvr(*tmp_motion)
                toggle_ct = True
                
                
            elif keyboard_dict_inv[ans] == "1":
                motion_step_mot0, mot_step = scale_step_dict(0.5,mot_step)
                toggle_ct = False
                    
            elif keyboard_dict_inv[ans] == "2":
                motion_step_mot0, mot_step = scale_step_dict(2,mot_step)
                toggle_ct = False
                
            elif keyboard_dict_inv[ans] == "+":
                motion_step_mot0, mot_step = scale_step_dict(-1,mot_step)
                toggle_ct = False

            elif keyboard_dict_inv[ans] == "-":
                motion_step_mot1, mot_step = scale_step_dict(-1,mot_step)
                toggle_ct = False
                               
            elif keyboard_dict_inv[ans] == "start":
                if verbose:
                    umv(*motion_start)
                else:
                    mv(*motion_start)
                toggle_ct = True
                
            elif keyboard_dict_inv[ans] == "space":
                toggle_ct = False
                pass

            elif keyboard_dict_inv[ans] == "l":
                toggle_ct = False
                pass               
        except:
            #print("...please use another key...\n")
            #print(help_string_mot)
            pass
            
        current_params = "Current position: "

        for motObj in mot_step.keys():
            current_params+= f"  {motObj.name} = {motObj.position:.5f} , step = {mot_step[motObj]:.5f}, "
        
        if (exposure is not None) and toggle_ct:
            ct(exposure)
        
        sys.stdout.write(  ANSI.color_text(31,help_start_pos+"\r\n") +
                                 ANSI.color_text(32,current_params+"\r\n"))
                                 
        # TODO clean with the curses module for reducing output in window

                
        ans = click.getchar(echo=False)
        while list(keyboard_dict_inv.keys()).count(ans) == 0:
            ans = click.getchar(echo=False)
            print("...please use another key - valid keys are:\n")
            print(help_string_mot)
        
        ## use for key debug
        #print(keyboard_dict_inv[ans])
        sys.stdout.flush()
    
