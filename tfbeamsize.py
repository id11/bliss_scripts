def vert_lenses():
	for ii,lenses in enumerate([0,4,8,12,18,24,32]):
		ct(0.1)
		print('set %i lenses'%lenses)
		try:
			tfoh1.set(lenses,"Be")		
		except RuntimeError:
			pass
		dscan(pz, -0.5, 0.5, 200, 0.2, title='%i Be lenses'%lenses)
	tfoh1.set(0,'Be')
	dscan(pz, -0.5, 0.5, 200, 0.2)

def horz_lenses():
	for ii,lenses in enumerate([0,4,8,12,18,24,32]):
		ct(0.1)
		print('set %i lenses'%lenses)
		try:
			tfoh1.set(lenses,"Be")		
		except RuntimeError:
			pass
		dscan(py, -0.5, 0.5, 200, 0.2, title='%i Be lenses'%lenses)
	tfoh1.set(0,'Be')
	dscan(py, -0.5, 0.5, 200, 0.2)
