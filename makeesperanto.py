
import os
from math import sqrt, sin, radians
from bliss.scanning import scan_meta
import os
from pathlib import Path


def makeponi(calibrant = "CeO2"):
    """ Assume you just collected the data picture """
    s = SCANS[-1]

    cmd1 = "cd /data/id11/nanoscope/ponifiles"
    cmd2 = "pyFAI-calib2 %s::%d.1/instrument/eiger "%(
        s.scan_info['filename'], s.scan_info['scan_nb'] ) + \
          "-m /data/id11/nanoscope/Eiger/20200925.msk " + \
          "-c %s "%(calibrant)  + \
          "-D Eiger2_4M " + \
          "-w %f "%(wvln()) + \
          "-l %f "%(frelx.position)  + \
          "--poni1=%f "%( 1e-3*(detz.position + 75 )) + \
          "--poni2=%f "%( 1e-3*(311.5 + det2y.position ))
    # spatial ?
    uname = input("Enter your username for ssh")
    cmd = 'ssh -X %s@crunch "%s && %s"'%(uname, cmd1, cmd2)
    print(cmd)
    os.system("%s "%(cmd))
    loadponi( cmd = cmd  )


def loadponi(pname = None, cmd=""):
    pname = sorted(
        Path("/data/id11/nanoscope/ponifiles"
         ).iterdir(), key=os.path.getmtime)[-1]
    print(pname)
    if input("OK ?")[0] not in 'yY%':
        print("So tell me which one to use please")
        return
    poni = open(pname,"r").read()
    scan_meta_obj = scan_meta.get_user_scan_meta()
    scan_meta_obj.instrument.set("poni",  # category
                                 { "poni" :
                                   {   "poni": poni,
                                       "filename" : str(pname) ,
                                       "cmd": cmd } } )



import string 
import os
from math import sqrt, sin, radians
from bliss.scanning import scan_meta
import os
from pathlib import Path
#import pyFAI, fabio


def wvln():
    return 2*(5.34094/sqrt(3))*sin(radians(config.get('llbragg1').position))



def makesperanto(wvln, xbeam, ybeam, distance, run, now=True):
    
    s=SCANS[-1]  
    expath=s.scan_info['filename'] 
    r=expath.replace('.h5', '',) 
    datapath=(os.path.dirname(r)) 
    scnb=s.scan_info['scan_nb']
    step=s.scan_info['instrument']['fscan_parameters']['step_size'] 
    cmdpath='%s/scan%.4d' %(datapath, scnb) 
    savepathu=(os.path.dirname(r))
    savepathi=(os.path.dirname(r))
    omegastart= s.scan_info['npoints']/2*step
    cmd0 = "cd %s" %(cmdpath)
    try:
        if SCAN_SAVING.proposal_type == 'visitor':
            exptype = 'user'
        else:
            exptype = 'inhouse'
    except:
        exptype = input("inhouse or user") 
    savepath=savepathu
    if exptype=="user": 
        savepath=savepathu
    print('saving data in %s' % (savepath))
    if exptype=="inhouse":
        savepath=savepathi
    print('saving data in %s' % (savepath))
    cmd2 = "eiger2crysalis eiger*h5 -w %f --beam %f %f --omega=-%d+index*%f --distance=%f -o %s/esperanto/frame_%d_{index}.esperanto" %(wvln, xbeam, ybeam, omegastart, step, distance, savepath,run) #changed %f in %d for run
    cmd = 'ssh -X opid11@crunch "%s && %s"'%(cmd0, cmd2)
    print(cmd)
    if now:
        os.system("%s "%(cmd))
    else:
        scriptname = os.path.join( datapath, 'make_esperanto_%04d.sh'%(scnb) )
        with open(scriptname, "w") as outf:
            outf.write("#!/bin/sh\n")
            outf.write(cmd + "\n")
        print("Run your conversion later:", scriptname)
        return scriptname

def auto_makesperanto():
    print("Centering the scan on zero")
    
    s=SCANS[-1]  
    scnb=s.scan_info['scan_nb']
    step=s.scan_info['instrument']['fscan_parameters']['step_size']
    npts=s.scan_info['instrument']['fscan_parameters']['npoints'] 
    wvln=s.scan_info['instrument']['crysalis']['wavelength']
    xbeam= s.scan_info['instrument']['crysalis']['xcenter']
    ybeam= s.scan_info['instrument']['crysalis']['ycenter']
    distance= s.scan_info['instrument']['crysalis']['distance']
    #     omegastart= s.scan_info['measurement']['rot'][0]
    omegastart = s.scan_info['instrument']['fscan_parameters']['start_pos']
    # The scan must be centered around zero
    center = step * npts / 2 + omegastart
    omegastart -= center
    
    run=1
       
#    basename = SCAN_SAVING.base_path 
#    proposalname = SCAN_SAVING.proposal_dirname 
#    samplename = SCAN_SAVING.collection_name 
#    datasetname = SCAN_SAVING.dataset_name 
#    savepath = os.path.join(basename,proposalname,'id11/20221104/AUTO',samplename,samplename+'_'+datasetname,samplename+ '_'+datasetname+'_scan'+str(scnb))
#    datapath = os.path.join(basename,proposalname,'id11/20221104',samplename,samplename+'_'+datasetname)

    
    datapath = os.path.split(SCAN_SAVING.filename)[0]
    cmdpath  = os.path.join(datapath,'scan%04d'%(scnb))
    savepath = cmdpath.replace("/RAW_DATA/","/PROCESSED_DATA/")
    
    cmd0 = "cd %s" %(cmdpath)
    cmd2 = "eiger2crysalis eiger*h5 -w %f --beam %f %f --omega=-%f-index*%f --distance=%f --flip-lr -o %s/frame_%d_{index}.esperanto > /dev/null " %(
            wvln, xbeam, ybeam, omegastart, step, distance, savepath, run) #changed %f in %d for run
    os.system('ls ' + os.path.split(datapath)[0]) 
    scriptname = os.path.join( datapath, 'make_esperanto_%04d.sh'%(scnb) )
    print("Writing",scriptname)
    with open(scriptname, 'w') as outf:
        outf.write("#!/bin/sh\n")
        outf.write("source /users/blissadm/conda/miniconda/etc/profile.d/conda.sh\n")
        outf.write("conda activate eiger2crysalis\n")
        outf.write(cmd0 + "\n")
        outf.write(cmd2 + "\n")
        outf.write("cp /data/id11/nanoscope/Eiger/frame_1_.set %s/frame.set\n"%(savepath)) # copy mask
    cmd = 'ssh -fn opid11@lid11eiger2lima "bash -l %s" &'%(scriptname)
    #print(cmd)
    os.system("%s "%(cmd))



#omega from rot

#running in silent mode

# save a command script - can run automatically after a scan


#ai = pyFAI.load("/data/id11/nanoscope/ponifiles/20201105_16h57.poni")
#ai.getFit2D()['tilt'] 
#ai.getFit2D()['centerX'] 
#ai.getFit2D()['centerY'] 
#ai.getFit2D()['directDist']   

#for i in range(1,361,1): 
#    os.rename(r'frame_1.000000_%d.esperanto'%(i),r'frame_1_%.4d.esperanto' %(i))


def makecbf(wvln, xbeam, ybeam):
    
    s=SCANS[-1]  
    expath=s.scan_info['filename'] 
    r=expath.replace('.h5', '',) 
    datapath=(os.path.dirname(r)) 
    scnb=s.scan_info['scan_nb']
    step=s.scan_info['instrument']['fscan_parameters']['step_size'] 
    cmdpath='%s/scan%.4d' %(datapath, scnb) 
    savepathu=(os.path.dirname(r))
    savepathi='/nobackup/crunch3/inhouse_esperanto/'
    cmd0 = "cd %s" %(cmdpath)
    exptype = input("inhouse or user") 
    savepath=savepathu
    if exptype=="user": 
        savepath=savepathu
    print('saving data in %s' % (savepath))
    if exptype=="inhouse":
        #/data/id11/nanoscope/blcxxxx/id11/sample/sample_dataset/scan]
        savepath='%s%s' %(savepathi, datapath)
    print('saving data in %s' % (savepath))
    cmd2 = "eiger2cbf eiger*h5 -w %f --beam %f %f --omega=-180+index*%f -o %s/cbf/frame_{index:04d}.cbf" %(wvln, xbeam, ybeam, step, savepath) #changed %f in %d for run
    cmd = 'ssh -X opid11@crunch "%s && %s"'%(cmd0, cmd2)
    print(cmd)
    os.system("%s "%(cmd))


 #eiger2cbf scan0001/eiger_000*.h5 -o cbf/enstatite_{index:04d}.cbf -w 0.29339 --beam 1074 1100 --offset 1 --dummy -1 --omega='-180+index*0.5' --phi=0 --kappa=0 --debug



def makesperantoHP(wvln, xbeam, ybeam, distance, omegastart, step, sname,  run):


#omegastart, step, name, 
    
    s=SCANS[-1]  
    expath=s.scan_info['filename'] 
    r=expath.replace('.h5', '',) 
    datapath=(os.path.dirname(r)) 
    scnb=s.scan_info['scan_nb']
    #step=s.scan_info['instrument']['fscan_parameters']['step_size'] 
    cmdpath='%s/scan%.4d' %(datapath, scnb) 
    savepathu=(os.path.dirname(r))
    savepathi='/nobackup/crunch3/inhouse_esperanto/'
    #omegastart= s.scan_info['npoints']/2*step
    cmd0 = "cd %s" %(cmdpath)
    exptype = input("inhouse or user") 
    savepath=savepathu
    if exptype=="user": 
        savepath=savepathu
    print('saving data in %s' % (savepath))
    if exptype=="inhouse":
        #/data/id11/nanoscope/blcxxxx/id11/sample/sample_dataset/scan]
        savepath='%s%s' %(savepathi, datapath)
    print('saving data in %s' % (savepath))
    cmd2 = "eiger2crysalis eiger*h5 -w %f --beam %f %f --omega=%d+index*%f --distance=%d -o %s/esperanto/%s_%d_{index}.esperanto" %(wvln, xbeam, ybeam, omegastart, step, distance, savepath, sname, run) #changed %f in %d for run
    cmd = 'ssh -X opid11@crunch "%s && %s"'%(cmd0, cmd2)
    print(cmd)
    os.system("%s "%(cmd))






def make_tif():
        
    s=SCANS[-1]  
    scnb=s.scan_info['scan_nb']
    
    datapath = os.path.split(SCAN_SAVING.filename)[0]
    cmdpath  = os.path.join(datapath,'scan%04d'%(scnb))
    savepath = cmdpath.replace("/RAW_DATA/","/PROCESSED_DATA/TIF/")
    
    cmd0 = "cd %s" %(cmdpath)
    cmd2 = f"OMP_NUM_THREADS=1 python ../h5_tif_conversion.py {SCAN_SAVING.filename} {savepath} {scnb} > /dev/null"

    os.system('ls ' + os.path.split(datapath)[0]) 
    os.system('cp ~/bliss_scripts/h5_tif_conversion.py ' + datapath+'/' )
    scriptname = os.path.join( datapath, 'make_tif_%04d.sh'%(scnb) )
    print("Writing",scriptname)
    with open(scriptname, 'w') as outf:
        outf.write("#!/bin/sh\n")
        outf.write("source /users/blissadm/conda/miniconda/etc/profile.d/conda.sh\n")
        outf.write("conda activate eiger2crysalis\n")
        outf.write(cmd0 + "\n")
        outf.write(cmd2 + "\n")
    cmd = 'ssh -fn opid11@lid11eiger2lima "bash -l %s" &'%(scriptname)
    #print(cmd)
    os.system("%s "%(cmd))















