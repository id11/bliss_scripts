import numpy as np


def plotbeamcenter(xc=None, yc=None):
    if xc is None:
        xc, yc = estimate_centrepixels(det2y.position, detz.position)
    p = flint().get_live_plot(image_detector="frelon6")
    p.update_marker("mybc", (xc, yc), "Beam Center")


def plotqvals(energy, xc=None, yc=None, dist=None, pixelsize=0.0475):
    xl = [1, 1 / np.sqrt(2), 0, -1 / np.sqrt(2), -1, -1 / np.sqrt(2), 0, 1 / np.sqrt(2)]
    yl = [0, -1 / np.sqrt(2), -1, -1 / np.sqrt(2), 0, 1 / np.sqrt(2), 1, 1 / np.sqrt(2)]
    lam = 12.39847 / energy
    if dist == None:
        dist = fx.position + detx.position
    if xc == None:
        xc, yc = estimate_centrepixels(dety.position, fz.position)
    plotbeamcenter(xc, yc)
    p = flint().get_live_plot(image_detector="frelon6")
    for q in np.arange(5, 35, 5):
        tth = 2 * np.arcsin(q * lam / (4 * np.pi))
        rp = dist * np.tan(tth) / pixelsize
        for i in range(8):
            xp, yp = rp * xl[i] + xc, rp * yl[i] + yc
            if xp > 0 and xp < 2048 and yp > 0 and yp < 2048:
                p.update_marker("frelon6%i_%i" % (q, i), (xp, yp), "q=%i" % q)


def removeqvals():
    print("Removing q points...")
    p = flint().get_live_plot(image_detector="frelon6")
    for q in np.arange(5, 35, 5):
        for i in range(8):
            p.remove_marker("frelon6%i_%i" % (q, i))


def estimateqval(energy, xc=None, yc=None, dist=None, pixelsize=0.0475):
    lam = 12.39847 / energy
    if dist == None:
        dist = fx.position + detx.position
    if xc == None:
        xc, yc = estimate_centrepixels(dety.position, fz.position)
    p = flint().get_live_plot(image_detector="frelon6")
    pix = p.select_points(1)[0]
    xp, yp = pix[0] - xc, pix[1] - yc
    rp = np.sqrt(xp ** 2 + yp ** 2)
    tth = np.arctan(rp * pixelsize / (dist))
    q = np.sin(tth / 2) * 4 * np.pi / lam
    print("Estimated q value of (%.2f,%.2f) is %.2f AA^-1" % (xp, yp, q))
