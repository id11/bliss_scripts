


def f3_goto_ftm(bin=(1,2)):
    frelon3.camera.image_mode = 'FRAME TRANSFER'
    frelon3.camera.roi_mode = 'SLOW'
    frelon3.camera.spb2_config = 'SPEED'
    frelon3.image.binning = bin
    frelon3.image.roi = [0,0,frelon3.image.width, frelon3.image.height]
    fsh_tdxrd.shutter_time = 0.015
    frelon3.shutter.close_time = 0.0
    fscan.pars.scan_mode='CAMERA'
    ct(0.1)
       

def f3_goto_kinetic( row = 0 ):
    frelon3.camera.image_mode = 'FULL FRAME'
    frelon3.camera.spb2_config = 'PRECISION'
    frelon3.image.binning = 1, 64
    frelon3.image.roi = [0,row,frelon3.image.width, 1]
    frelon3.camera.roi_mode = 'KINETIC'
    frelon3.shutter.close_time = 0.0
    fsh_tdxrd.shutter_time = 0.0
    fscan.pars.scan_mode='CAMERA'
    ct(0.1)

def f3_goto_normal( ):
    frelon3.camera.image_mode = 'FULL FRAME'
    frelon3.camera.roi_mode = 'SLOW'
    frelon3.camera.spb2_config = 'SPEED'
    frelon3.image.binning = 1, 1
    frelon3.image.roi = [0,0,frelon3.image.width, frelon3.image.height]
    frelon3.shutter.close_time = 0.015
    fsh_tdxrd.shutter_time = 0.015
    # finterlaced anyway
    ct(0.1)

from time import time

def f3_check():
    print(frelon3.camera.__info__())
    print(frelon3.image.__info__())
    t0 = time()
    limatake( 0.001, 1000 )
    t1 = time()
    print( 'approx time', (t1-t0-1)/1000 )
    print( frelon3.camera.readout_time, frelon3.camera.transfer_time)
    
