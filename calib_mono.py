

ll_foils_energies = {  # These values from xraylib
    'Ag': 25.514, 
    'Sn': 29.2001,
    'Nd': 43.4689,
    'Gd': 50.2391,
    'Hf': 65.3508,
    'W' : 69.525,
    'Pt': 78.3948,
    'Pb': 88.0045,
    }

from id11.undulator import id11_cpm18, id11_u22, undpeaks

def scan_all_edges(edges):
    g18_mm = id11_cpm18.ef_from_gap(10)
    g22_mm = id11_u22.ef_from_gap(10)
    for k,e in edges: # ll_foils_energies.items():  
        print(k,e,end=" " )
        n18 = int(e / g18_mm) # "harmonic" above 7 mm gap
        g18 = id11_cpm18.gap_from_ef( e / (n18+0.5) )
        n22 = int(e / g22_mm) # "harmonic" above 7 mm gap
        g22 = id11_u22.gap_from_ef( e / (n22+0.5) )
        print('cpm18 %.3f u22 %.3f'%(g18, g22))
        # undpeaks(e)
        umv( cpm18, g18 )
        umv( u22, g22 )
        ll_edge_scan( k )

        

        
        


if __name__=="__main__" and "session" not in globals():
    scan_all_edges()
            
