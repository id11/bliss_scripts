

def xrotprepare( ):
    # diodes in
    fsh.session='TDXRD'
    sheh3.open()
    pico4.insert()
    diodey = config.get('diodey')
    if diodey.position < 13:
        umv(diodey, 14)
    # Set full scale range
    pico4.auto_range=True
    pico3.auto_range=True
    ct(1, pico3, pico4 )
    pico4.auto_range=False
    pico3.auto_range=False
    ctrs = ACTIVE_MG.enabled
    ACTIVE_MG.disable('*')
    ACTIVE_MG.enable('p201_20*')
    return ctrs

def xrotcleanup( ctrs ):
    ACTIVE_MG.disable('*')
    for ctr in ctrs:
        ACTIVE_MG.enable( ctr )
    pico4.auto_range=True
    pico3.auto_range=True


def xrotfullrange( step=0.002, ctime=0.002, start=0, end=360 ):
    """
    Full range scan
    """
    if end < start:
        assert step < 0
    try:
        ctrs = xrotprepare( )
        fscan( xroty, start, step, (end - start)/step, ctime )
    except:
        raise
    finally:
        xrotcleanup( ctrs )

def xrotpeaks( peaklist, step=0.001, ctime=0.05 ):
    # step=0.002, ctime=0.002, start=0, end=360
    """
    Full range scan
    """
    assert step > 0
    try:
        ctrs = xrotprepare( )
        for cen, rng in peaklist:
            start = cen - rng / 2
            end   = cen + rng / 2 
            fscan( xroty, start, step, (end - start)/step, ctime )
            # do the fit?
    except:
        raise
    finally:
        xrotcleanup( ctrs )        


# TO DO:
#   Given the energy...
#     compute a list of expected peak positions
#     go and measure a this list of peaks
#     fit the positions of the measured peaks
#     report the energy

def testpks_Hf():
    cenlist = [ 357.332, 357.089, 356.584, 356.5,
                307.212, 303.745, 273.040, 267.381,
                273.066, 267.373, 236.635, 233.034,
                218.568, 212.403, 211.915, 200.561,
                183.481, 183.013, 177.459, 177.011,
                176.202, 174.381, 174.125, 168.374,
                161.344, 160.225, 155.081, 141.033,
                131.633, 128.109, 124.584, 118.710,
                105.394,  92.470,  87.379,  61.140,
                 56.725,  53.481,  38.762,  32.359,
                 20.217,   3.313,  3.112 ]
    pklist = [ (c, 0.1) for c in cenlist[::-1] ]
    xrotpeaks( pklist )
                
        
