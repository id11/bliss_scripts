
import numpy as np
import time
user_script_load("align_cor.py") 


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan(*a,**k):
    print(a,k)
    try:
        fscan(*a,**k)
    except: # pretty hopeless but why not ? 
        fscan(*a,**k)

def myumv(*a,**k):
    print(a,k)
    umv(*a,**k)
        

        
def layer_s1():
    y0 = 0.0  # centre of rotation
    width = 9.
    step  = 0.6
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.1
    anrange = 180.
    speed = 25.
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    eiger.camera.photon_energy=43468.9
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        # edited here Fri 8h for 
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, 180, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"
        
def layer_x3():
    y0 = 0.0  # centre of rotation
    width = 8.
    step  = 0.6
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.1
    anrange = 360.
    speed = 25.
    eiger.camera.photon_energy=43468.9
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        # edited here Fri 8h for 
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"
