
import time
import numpy as np
# 1.17 done
# sample top is at 1.07


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

TESTING = False


#def myfscan(*a,**k):
#    print(a,k)
#    if TESTING:
#        return
#    try:
#        fscan(*a,**k)
#    except: # pretty hopeless but why not ?
#        print("Something has gone wrong!!! Retry scans!!")
#        elog_print("Retry a scan !!!")
#        print("Sleep a minute or two first")
#        time.sleep(60)
#        pause_for_refill(120)
#        fscan(*a,**k)
        
def myumv(*a,**k):
    print(a,k)
    if TESTING:
        return
    umv(*a,**k)

def nscopedtylayerscan(y_start, y_step, ny, a_time, rstart, rstep, rend):
    eiger.camera.photon_energy = 65350.8
    for i,r in enumerate(np.arange(rstart,rend,rstep)):
        myumv(rot, r )
        pause_for_refill(ny*a_time+1)
        if i%2 == 0:
            myfscan(dty, y_start, y_step, ny, a_time, scan_mode='CAMERA')
        else:
            myfscan(dty, y_start+y_step*ny, -y_step, ny, a_time, scan_mode='CAMERA')



def myfscan( *args, **kwds):
    for i in range(3):
        try:
            fscan(*args, **kwds)
        except Exception as e:
            elog_print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break

import numpy as np        
        
print("Load this script at least twice please!")

def my2d2(y0, ye=260, ys=5):
    fscan.pars.latency_time=0
    for i, ypos in enumerate(np.arange(y0,ye,ys)):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            myfscan(rot, -90, 25*0.005, 181/(25*0.005), 0.005, scan_mode='CAMERA' ) #was 0.125, 2896
        else:
            myfscan(rot, 90, -25*0.005, 181/(25*0.005), 0.005, scan_mode='CAMERA' )#was -0.125, 2896




def my2d(y0, ye=5.01, ys=0.5):
    fscan.pars.latency_time=0
    for i, ypos in enumerate(np.arange(y0,ye,ys)):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            myfscan(rot, -1, 0.125, 2896, 0.03, scan_mode='CAMERA' ) 
        else:
            myfscan(rot, 361, -0.125, 2896, 0.03, scan_mode='CAMERA' )

def volquartz():
    for zpos in np.arange(50,85,1): # 43 scans
        if int(zpos) < 84:
            continue
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -30, 1, 60, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -30, 1, 60, 0.3, 180, -2, -0.1 )
           

def night_check():
	for i in range (200):
                dscan(llrz1, -0.01, 0.01, 40, 0.1, pico0)
                dscan(llrz2, -0.01, 0.01, 40, 0.1, pico0)
                (cpm18t, 0.06)
                ascan(cpm18,9.5,10.5,40, 0.1, pico0, sleep_time=0.15)   
                ascan(u22,8,9, 40, 0.1, pico0, sleep_time=0.1)
                



def pdf():
    fscan(rot, 0, 1, 360, 1)
    umv(dty, 2000)
    newsample('air_nscope')
    newdataset('pdf')
    fscan(rot, 0, 1, 360, 1)
                

def collect2():
    tfoh1.set(0, "Be" )
    newdataset("300K025d02s")
    fscan(rot, 0, 0.25, 360/0.25, 0.2,scan_mode='CAMERA')
    tfoh1.set(12, "Be" )
    newdataset('300K025d02s_12Be')
    fscan(rot, 0, 0.25, 360/0.25, 0.2,scan_mode='CAMERA')
    tfoh1.set(0, "Be" )
    umv(rot,0)
  

def night():
    umv(shtz,2.07)
    fscan2d(pz,1,1,98,dty,-150,1,300,0.1)
    umv(shtz,2.17)
    fscan2d(pz,1,1,98,dty,-150,1,300,0.1)
    umv(shtz,2.27)
    fscan2d(pz,1,1,98,dty,-150,1,300,0.1)
    umv(shtz,2.37)
    fscan2d(pz,1,1,98,dty,-150,1,300,0.1)
    umv(shtz,2.47)
    fscan2d(pz,1,1,98,dty,-150,1,300,0.1)
    umv(shtz,2.57)
    fscan2d(pz,1,1,98,dty,-150,1,300,0.1)
    umv(shtz,2.67)
    fscan2d(pz,1,1,98,dty,-150,1,300,0.1)
    umv(shtz,2.77)
    fscan2d(pz,1,1,98,dty,-150,1,300,0.1)
    umv(shtz,2.87)
    fscan2d(pz,1,1,98,dty,-150,1,300,0.1)





def collectmichela():
    #tfoh1.set(0, "Be" )
    #newdataset("S5")
    #fscan(rot, 0, 0.25, 360/0.25, 0.5,scan_mode='CAMERA')
    #tfoh1.set(12, "Be" )
    newdataset('S6')
    fscan(rot, 0, 0.25, 360/0.25, 0.5,scan_mode='CAMERA')
    #tfoh1.set(0, "Be" )
    newdataset("S7")
    fscan(rot, 0, 0.1, 360/0.1, 0.5,scan_mode='CAMERA')
   

def labradorite():
    newdataset('layer_scan')
    nscopedtylayerscan(-120,0.5,480,0.1,0,0.25,181)
    newdataset('scan_dty')
    fscan2d(dty,-120,0.5,480,rot,0,0.25,720,0.1) 
    newdataset('scan_rot')
    fscan2d(rot,0,0.25,720,dty,-120,0.5,480,0.1) 





def labradorite_layer():
    newsample("labradorite")
    newdataset("layer1")
    my2d( -130.0)
     
def labradorite_layer_fast():
    newsample("labradoritefast2")
    newdataset("layer_fast")
    my2d2( -130.0) 

def labradorite_night():
    labradorite_layer_fast()
    labradorite_layer()
    
def bassanite_night():
    #newsample("labradorite")
    newdataset("layer_med_pos2")
    my2d( -26.0)
    newdataset("sx_med_pos2")
    fscan(rot, 0, 0.25, 1440, 0.5)
    umv(shtx,-0.4592,shty,-0.4784,shtz,-2.2000)
    newdataset("layer_top_pos1")
    my2d( -26.0)
    newdataset("sx_top_pos1")
    fscan(rot, 0, 0.25, 1440, 0.5)




def bassanite_layer():
    newdataset("top_layer")
    my2d( -20.0)




def mygypsum(y0, ye=10, ys=0.15):
    #eiger_3dxrd()
    fscan.pars.latency_time=0
    for i, ypos in enumerate(np.arange(y0,ye,ys)):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            myfscan(rot, -90, 25*0.005, 181/(25*0.01), 0.01, scan_mode='CAMERA' ) #was 0.125, 2896
        else:
            myfscan(rot, 90, -25*0.005, 181/(25*0.01), 0.01, scan_mode='CAMERA' )#was -0.125, 2896


def mystart():
    umv(dty, 0)
    umv(px, 50, py, 50, pz, 50)
    umv(rot,0)
    print('Now homing...')
    shtx.home()

def eiger_3dxrd ():
    #tfoh1.set(0, "Be" )
    sheh3.close()
    umv(frelx, 350)
    sheh3.open()
    print('I moved far away...ready for 3DXRD')    
    
def eiger_SX ():
    sheh3.close()
    umv(frelx, 130)
    sheh3.open()
    print('I moved closer...ready for SX') 
    
def gypsum_SX ():
    umv(dty,0)
    tfoh1.set(0, "Be" )
    newdataset("SX_0Be_bottom")
    fscan(rot, 0, 0.25, 360/0.25, 0.2,scan_mode='CAMERA')
    tfoh1.set(16, "Be" )
    newdataset('SX_16Be_bottom')
    fscan(rot, 0, 0.25, 360/0.25, 0.2,scan_mode='CAMERA')
    
    
    
def Si_wafer ():
    newdataset('layer_2nd')
    loopscan(100, 5)
    umv(dty, -231)
    newdataset('air_2nd')
    loopscan(100, 5)
    sheh3.close()
    
    
    
    

