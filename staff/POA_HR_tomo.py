def HR_tomo_16092021():
    y_zero = [0, 1.4, 2.8, 4.2, 5.6, 7, 8.4, 9.8]
    for i in range(0,7):
        newdataset('good_1_diffty_%d'%(y_zero[i]))
        tomo.full_turn_scan()
        umvr(diffty,1.4)
        sct(0.02)

#Pixel size = 11/(7.5)/0.9
# = 1.6296

import numpy as np
#diffty = 14.63900
#36000
def HR_tomo_12102021():
    y_zero = [1,3.5,6]
    y0 = 14.63900
    newdataset('tiled_1')
    sheh3.close()
    fscan(diffrz,0,0.01,100,0.3) #dark
    sheh3.open()
    for y in y_zero:
        umv(diffty,y0+11)
        fscan(diffrz,0,0.01,100,0.3) #flat
        umv(diffty,y0+y)
        fscan(diffrz,0,0.01,36000,0.3) #proj
    umv(diffty,y0+11)
    fscan(diffrz,0,0.01,100,0.3) #flat
    sheh3.close()
    fscan(diffrz,0,0.01,100,0.3) #dark
    sheh3.open()
    
def HR_tomo_28102021():
    y_zero = [0,0.4,0.8,1.2,1.6,2.0,2.4,2.8,3.2,3.6,4.0,4.4,4.8]
    y0 = 14.139
    newdataset('CT_1')
    sheh3.close()
    fscan(diffrz,0,0.01,100,0.01) #dark
    sheh3.open()
    for y in y_zero:
        umvr(difftz,-8)
        fscan(diffrz,0,0.01,1000,0.01) #flat
        umvr(difftz,8)
        umv(diffty,y0+y)
        fscan(diffrz,0,360/22500,22500,0.01) #proj
    umvr(difftz,-8)
    fscan(diffrz,0,0.01,100,0.01) #flat
    sheh3.close()
    fscan(diffrz,0,0.01,100,0.01) #dark
    
def HR_tomo_17112021():
    y_zero = [0,1,2,3,4]
    y0 = 14.66
    newdataset('CT_6')
    sheh3.close()
    fscan(diffrz,0,0.01,100,0.02) #dark
    sheh3.open()
    for y in y_zero:
        umvr(samtz,-8)
        fscan(diffrz,0,0.01,1000,0.02) #flat
        umvr(samtz,8)
        umv(diffty,y0+y)
        fscan(diffrz,0,360/24300,24300,0.02) #proj
    umvr(samtz,-8)
    fscan(diffrz,0,0.01,100,0.02) #flat
    sheh3.close()
    fscan(diffrz,0,0.01,100,0.02) #dark
    
def HR_tomo_13092022(tz):
    y_zero = [0,1.1,2.2,3.3,4.4,5.5,6.6,7.7,8.8]
    y0 = 14.42
    newdataset('CT_%s'%(str(int(tz*100))))
    sheh3.close()
    fscan(diffrz,0,0.01,100,0.02) #dark
    sheh3.open()
    umv(samtz,1)
    fscan(diffrz,0,0.01,1000,0.02) #flat
    umv(samtz,tz)
    for y in y_zero:
        umv(diffty,y0+y)
        fscan(diffrz,0,360/24300,24300,0.02) #proj
    umv(samtz,1)
    fscan(diffrz,0,0.01,100,0.02) #flat
    sheh3.close()
    fscan(diffrz,0,0.01,100,0.02) #dark
    
def HR_samtz():
    tz_series = [8.27,7.17,6.07,4.97,3.87]
    for tz in tz_series:
        HR_tomo_13092022(tz)

