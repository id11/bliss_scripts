
import time, numpy as np

# user_script_load("es1190.py")
user_script_load('optics.py')


# def check_cpm18(pos=6.4158):
def check_cpm18(pos=10.073):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()

def half1(ymax, datasetname, ystep=5, ymin=-1801):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, -90,  0.05, 3620, 0.002 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot,  91, -0.05, 3620, 0.002 )

def full1(ymax, datasetname, ystep=10, ymin=-99901):
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    eiger.camera.auto_summation='OFF'
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.05, 7220, 0.002 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 361, -0.05, 7220, 0.002 )
    eiger.camera.auto_summation='ON'



def goto_basler():
    moveto_basler()                           # go to imaging camera
    umv(pinz, 0.3, piny, 17.32, bigy, 24)     # bigger beam
    umv(dty,0)
    umv(attrz, 0, atty, -10)                  # attenuator out

# tomo recipe
# user_script_load('tomo_by_ascan')
# user.tomo_by_ascan(0,360,3600,0.01,rot='rot', ymotor='ntz', ystep=-1)

    
    
def goto_focused():
    umv(pinz, -0.0152, piny, 0.03, bigy, 0.0048)
    umv(attrz, -10, atty, 0)


    
