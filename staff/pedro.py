import sys
import numpy as np
import time
import os
    
atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
bigy = config.get('bigy')
u22 = config.get('u22')


############## POSITIONS##########################

#OH2
atty_in = 0
atty_out = -10
attrz_in = -10
attrz_out = 0

#SLITS:FOR A GIVEN SCAN

#DCT
s7hg_dct = 0.6
s7vg_dct = 0.3
s8hg_dct = s7hg_dct + 0.1
s8vg_dct = s7vg_dct + 0.1
#FF
s7hg_ff = 1.0
s7vg_ff = 0.3
s8hg_ff = s7hg_ff + 0.05
s8vg_ff = s7vg_ff + 0.05
#PCT
s7hg_pct = 0.6
s7vg_pct = 0.3
s8hg_pct = s7hg_pct + 0.1
s8vg_pct = s7vg_pct + 0.1

#DIFFRACTOMETER
samrx_cen = 0
samry_cen = 0
diffry_cen = 0

samtz_cen = -3.35
difftz_cen = -11.81

#CAMERAS YZ
d3ty_in = 0
d3ty_out =180
d3tz_in = 0
d3tz_out = 50

ffdtx1_in = 200
ffdtx1_out = 400
ffdtz1_in = 0
ffdtz1_out = 300

#CAMERAS X: FOR A GIVEN SCAN TYPE

#DCT
nfdtx_dct_in = 10
nfdtx_dct_out = 50
#FF
ffdtx1_ff_in = 200
ffdtx1_ff_out = 400
#PCT
nfdtx_pct_in = 100
nfdtx_pct_out = 100

#BEAMSTOPS
d3bsty_in = 0
d3bsty_out = 2000
d3bstz_in = 0
d3bstz_out = 2000


def attyin():
    umv(atty, atty_in, attrz, attrz_in)

def attyout():
    umv(atty, atty_out, attrz, attrz_out)

def d3bsin():
	umv(d3_bsty, d3bsty_in)

def d3bsout(d3_bsty_out=-1000):
	umv(d3_bsty, d3bsty_out)


def frelon1in():
    umv(d3ty, d3_out)
    umv(ffdtx1, 500, nfdtx, 200, ffdtz1, 100)
    tfoh1.set(0,'Al')
    tfoh1.set(16,'Be')
    #for i in range (3):
    #    umv(tfz, -0.319, tfy, 14.55)
    attyout();
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon3*")
    ACTIVE_MG.enable("frelon1:image")
    umv(diffrz, 0)
    umv(d1ty, 0, d1tz, 0)
    print("ready to collect dct data")
    
def ffin(ff_pars):
    sheh3.close()
    tfoh1.set(18,'Be')
    tfoh1.set(0,'Al')
    for i in range (3):
        umv(tfz, -0.325, tfy, 14.71)
    umv(bigy, bigy_out)
    attyin();
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon16:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out, d2ty, d2_out)
    umvct(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umvct(s8hg, ff_pars['slit_vg'] + 0.2, s8hg, ff_pars['slit_hg'] + 0.2)
    umv(ffdtx1, ff_dist, nfdtx, dct_dist, ffdtz1, 0)
    print("ready to collect far-field data")
    sheh3.open()
 
def scanning_ff():
    if bigy.position > 20:
        umvr(bigy, -bigy_out-0.047)
    umv(atty, 0, attrz,-9)
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon16:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out)
    #umv(ffdtx1, ff_dist, nfdtx, dct_dist, ffdtz1, 0)
    print("ready to collect far-field data")

def marana_pct(pct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    umv(furnace_z, furnace_out)
    umv(nfdtx, pct_pars['dist'])
    centeredroi(marana,1024,1024)
    bsout()
    sct(pct_pars['exp_time'])
    umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'])
    umvct(s8vg, pct_pars['slit_vg'] + 0.05, s8hg, pct_pars['slit_hg'] + 0.05)

def maranain(dist):
    tfoh1.set(18,'Be')
    tfoh1.set(0,'Al')
    for i in range (3):
        umv(tfz, -0.325, tfy, 14.71)
    sheh3.close()
    attyout()
    umv(bigy, bigy_out)
    ACTIVE_MG.enable("marana:image")
    ACTIVE_MG.disable("frelon16*") 
    ACTIVE_MG.disable("frelon3*")
    #sam_dct_pos()
    umv(nfdtx, dist, ffdtz1, ff_z)
    umv(d2ty, d2_out, d3ty, 0, d3tz, d3tz_pos_dct)
    print("ready to collect marana data") 
    sheh3.open()
    ct(0.02)


def define_tt_pars():
    tt_pars={}
    tt_pars['start_pos'] = 6
    tt_pars['diffrz_step'] = 4
    tt_pars['num_proj'] = 360 / tt_pars['diffrz_step']
    tt_pars['diffry_step'] = 0.02
    tt_pars['exp_time'] = 0.05
    # tt_pars['search_range'] = 0.3 # 0, 44, 48
    # tt_pars['search_range'] = 0.4 # 52
    tt_pars['search_range'] = 0.45 # 56
    tt_pars['scan_mode'] = 'CAMERA'
    tt_pars['image_roi'] = [512, 512, 1024, 1024] 
    # tt_pars['counter_roi'] = [412, 412, 200, 200] # 0N
    # tt_pars['counter_roi'] = [412-10, 412-10, 200+20, 200+20] # 44N
    # tt_pars['counter_roi'] = [412-20, 412-20, 200+40, 200+40] # 48N
    tt_pars['counter_roi'] = [412-20, 412-20, 200+40, 200+40] # 48N
    tt_pars['slit_hg'] = 0.3
    tt_pars['slit_vg'] = 0.3
    tt_pars['dist'] = 8
    return tt_pars
    

def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.25
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -2.5
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.5
    dct_pars['slit_vg'] = 0.25
    dct_pars['dist'] = 8 #2.7S
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = -2.9088
    dct_pars['shift_step_size'] = 0.12
    dct_pars['nof_shifts'] = 5
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars


def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = -7
    pct_pars['step_size'] = 0.2
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.0
    pct_pars['refon'] = pct_pars['num_proj']
#    pct_pars['ref_int'] = 10
    pct_pars['ref_step'] = -1
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 100
    pct_pars['ref_mot'] = samy
 #   pct_pars['zmot'] = samtz
    pct_pars['slit_hg'] = 1.5
    pct_pars['slit_vg'] = 1.5
    pct_pars['dist'] = 100
    pct_pars['scan_type'] = 'fscan'
    return pct_pars
    
def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = -1
    ff_pars['step_size'] = 0.8
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.7
    ff_pars['slit_vg'] = 0.3
    ff_pars['mode'] = 'FORWARD'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = -0.622
    ff_pars['shift_step_size'] = 0.275
    ff_pars['nof_shifts'] = 3
    ff_pars['cpm18_detune'] = 0
    return ff_pars
    
def define_sff_pars():
    sff_pars={}
    sff_pars['start_pos'] = -0.4
    sff_pars['step_size'] = 0.8
    sff_pars['num_proj'] = 360 / sff_pars['step_size']
    sff_pars['exp_time'] = 0.08
    sff_pars['slit_hg'] = 0.01
    sff_pars['slit_vg'] = 0.01
    sff_pars['mode'] = 'FORWARD'
    sff_pars['zmot'] = samtz
    sff_pars['samtz_cen'] = 0.829 + 0.017+0+0+0
    sff_pars['shift_step_size'] = 0.05
    sff_pars['nof_shifts'] = 5
    sff_pars['cpm18_detune'] = 0
    return sff_pars


def load_ramp_by_ascan(start, stop, npoints, exp_time, loadstep, pct_pars):
    marana_large_beam(pct_pars)
    newdataset('loadramp_%d' %  loadstep)
    ascan(stress, start, stop, npoints, exp_time, stress_adc, marana)
    return


def load_ramp_by_target(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    write_to_file = to_file if to_file is not None else sys.stdout
    #marana_large_beam(pct_pars)
    current_load = stress_adc.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return


def define_pars(load_list):
    """Produces default input parameters for a load sequence (see next function)
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    for load in load_list:
        step_pars['dct_pars'] = define_dct_pars()
        step_pars['pct_pars'] = define_pct_pars()
        step_pars['tt_pars'] = define_tt_pars()
        step_pars['ff_pars'] = define_ff_pars()
        step_pars['sff_pars']= define_sff_pars()
        step_pars['target'] = load
        step_pars['load_step'] = step
        step = step + 1
        par_list.append(step_pars)        
    return par_list

def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])
    
def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.2, s8hg, pars['slit_hg'] + 0.2)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def tomo_by_finterlaced_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.2, s8hg, pars['slit_hg'] + 0.2)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        #fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)

def ref_scan_samy(exp_time, nref, ystep, omega, scanmode):
    umvr(samy, ystep)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umvr(samy, -ystep)
    
def ref_scan_difftz(exp_time, nref, zstep, omega, scanmode):
    difftz0 = difftz.position
    umv(difftz, difftz0 + zstep)
    print("fscan(diffrz, %6.2f, 0.1, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.1, nref, exp_time, scan_mode = scanmode)
    umv(difftz, difftz0)
    

def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')
        
def in_situ_loading(pars, sample_name, voltage_ramprate = 0.01, voltage_target = 120, nscans=1000, last_scan = 0):
    
    if pars['dist'] < 90:
        print('You are dangerously close')
        return
    ACTIVE_MG.enable('stress*')
    print('Activated stress saving')
    stress_regul.plot()
    initialize_stress_regul()
    print('Initialized stress regulation')
    print('Will move nfdtx and slits')
    umv(nfdtx, pars['dist'], s7hg, pars['slit_hg'], s8hg, pars['slit_hg']+0.2, s7vg, pars['slit_vg'], s8vg, pars['slit_vg']+0.2)
    newsample(f'{sample_name}_insitu')
    newdataset('sct')
    sct(pars['exp_time'])
    #take darks
    #print('Taking darks')
    #newdataset('darks')
    #sheh3.close()
    #ftimescan(pars['exp_time'], pars['nref']/10)
    #sheh3.open()
    #print('Took darks')
    #take flats
    #print('Will take flat fields. Moving difftz away')
    #newdataset('flats')
    #umvr(pars['ref_mot'], pars['ref_step'])
    #ftimescan(pars['exp_time'], pars['nref'])
    #print('Took flats. Moving samy back')
    #umvr(pars['ref_mot'], -1 * pars['ref_step'])
    #print('Sleeping for 30s.')
    #sleep(60)
    print('Activating ramprate')
    regul_off(voltage_ramprate, voltage_target)
    #init_load = stress_adc.get_value()
    print('Started voltage ramprate')
    ii = 0
    if last_scan != 0:
        ii = last_scan + 1
    for _ in range(nscans):
        
        newdataset(f'pct_{ii:04}')
        #take flats
        #if ii % 10 == 0:   
        #acquire projections
        print('Will acquire projections')
        if ii % 2 == 0:
            fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        else:
            fscan(diffrz, pars['start_pos'] + 360, -1 * pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        
        if stress_adc.get_value() <= 1:
            regul_off(0, stress.position)
            print('Your sample probably broke. Finishing.')
            break
        ii += 1        
    print('Finished')         
    


def tdxrd_boxscan(scanname, ff_pars):
    print("using slit_vg %f and slit_hg %f" % (ff_pars['slit_vg'], ff_pars['slit_hg']))
    umv(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.2, s8hg, ff_pars['slit_hg'] + 0.2)
    newdataset(scanname)
    finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])



def acquire_pca(dset_name, ndarks = 500, nflats = 1000, exp_time = 0.04):

    newdataset(dset_name)
    umvr(samy, -4)
    sheh3.open()
    #take flats
    ftimescan(exp_time, nflats)
    sheh3.close()
    #take darks
    ftimescan(exp_time, ndarks)
    umvr(samy, 4)
    sheh3.open()
    ct(exp_time)
    
def acquire_pca_tseries(dset_name, nacq = 10, sleep_time = 60, ndarks = 500, nflats = 1000, exp_time = 0.04):

    newdataset(dset_name)
    
    for _ in range(nacq):
		
	    sheh3.open()
	#take flats
	    ftimescan(exp_time, nflats)
	    sheh3.close()
	#take darks
	    ftimescan(exp_time, ndarks)
	    sleep(sleep_time)
    
    sheh3.open()
    ct(exp_time)
    

def anneal_cycle(pct_pars, dct_pars, time, step, Temp=465, tol=5):
    sheh3.open()
    nanodac3.setpoint = Temp
    while np.abs(nanodac3_temp.read() - Temp) > tol:
        print("waiting for furnace to reach setpoint")
        sleep(30)
    print("Switch to phase contrast configuration")
    marana_pct(pct_pars)
    print("moving down furnance...")
    umv(furnace_z, 0)
    print(f'annealing for {time} seconds')
    sleep(time)
    print('moving furnace out')
    umv(furnace_z, furnace_out)
    print('retracting furnace and waiting 5 min to stabilize')
    sleep(300)
    print('collecting phase contrast scan')
    fulltomo.full_turn_scan(f'pct_{Temp}C_{step:02}')
    #print('switching to dct configuration')
    #marana_dct(dct_pars)
    #print('acquiring dct scan...')
    #dct_marana_dict(f'dct_{Temp}C_{step:02}', dct_pars)
    print('annealing cycle finished...')
    

