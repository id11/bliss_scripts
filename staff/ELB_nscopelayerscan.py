import numpy as np
import time

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan(*a,**k):
    print(a,k)
    try:
        fscan(*a,**k)
    except: # pretty hopeless but why not ? 
        fscan(*a,**k)

def myumv(*a,**k):
    print(a,k)
    umv(*a,**k)

def nscopelayerscan_trial(y_start,y_end,y_step,a_range,a_step,a_speed,run_trial=True):
    """
    Estimates time to do a layer scan with dty as outer motor and rot as inner.
    Zigzag with rot
    y_step is actual y step, no factor of 2
    """
    ypos = np.arange(-y_start, y_end+(y_step/10), y_step*2)
    trialpos = np.arange(-2*y_step, 2*y_step+(y_step/10), y_step*2)
    a_time = a_step / a_speed
    a_rate = a_speed / a_step
    assert (a_step * a_rate)<26 , 'too fast'
    timest = a_range/a_step * a_time + 1
    print('y motion:\n from %0.4f to %0.4f with steps of %0.4f and a total of %d points'
          %(ypos[0],ypos[-1],y_step,len(ypos)))
    print('angular motion:\n from %0.4f to %0.4f with steps of %0.4f and a total of %d points'
          %(0,a_range,a_step,a_range/a_step))
    print(' count time of %0.4f s per point and angular speed of %0.4f (max is 25)'
          %(a_time,a_speed))
    print('estimated time:\n %0.4f s per y step and %0.4f s or %0.2f min total'
          %(timest,timest*2*len(ypos),timest*2*len(ypos)/60))
    if run_trial:
        starttime = time.perf_counter()
        ACTIVE_MG.disable("mca")
        if a_time < 0.006:
            eiger.camera.auto_summation="OFF"
        else:
            eiger.camera.auto_summation="ON"
        ACTIVE_MG.enable("eiger:image")
        for my_y in trialpos:
            myumv(dty, my_y)
            pause_for_refill(timest)
            myfscan(rot, 0, a_step, a_range/_astep, a_time, scan_mode='CAMERA')
            myumv(dty, my_y + a_step)
            pause_for_refill(timest)
            myfscan(rot, a_range, -a_step, a_range/a_step, a_time, scan_mode='CAMERA')
        eiger.camera.auto_summation="ON"
        endtime = time.perf_counter()
        print('measured time:\n %0.4f s per y step and %0.2f s or %0.2f min total'
              %((endtime-starttime)/(len(trialpos)*2),
                (endtime-starttime)*(len(ypos)/len(trialpos)),
                (endtime-starttime)*(len(ypos)/len(trialpos))/60))  
    return (endtime-starttime)/(len(trialpos)*2)



def nscopelayerscan(y_start,y_end,y_step,a_range,a_step,a_speed):
    """
    layer scan with dty as outer motor and rot as inner.
    Zigzag with rot
    y_step is actual y step, no factor of 2
    """
    ypos = np.arange(-y_start, y_end+(y_step/10), y_step*2)
    a_time = a_step / a_speed
    a_rate = a_speed / a_step
    assert (a_step * a_rate)<26 , 'too fast'
    timest = a_range/a_step * a_time + 1
    ACTIVE_MG.disable("mca")
    if a_time < 0.006:
        eiger.camera.auto_summation="OFF"
    else:
        eiger.camera.auto_summation="ON"
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, a_step, a_range/_astep, a_time, scan_mode='CAMERA')
        myumv(dty, my_y + a_step)
        pause_for_refill(timest)
        myfscan(rot, a_range, -a_step, a_range/a_step, a_time, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"





