from __future__ import print_function, division

from bliss.setup_globals import *

import sys, os, glob, io
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import urllib.request
import time
import math
#from nabu.estimation.cor import CenterOfRotation

#Input
CAM_ANGLE = 38.7 #Angle is anti clockwise from the beam
IMAGE_PATH = '/tmp_14_days/pautran/'

timeStr = time.strftime("%Y_%m_%d")

#Def's

def camera_align_cor_centerOfMass(matrix,axis=1):
    """Calculating center of Mass of a matrix along column axis"""
    matrix_flat = matrix
    x = np.linspace(0,np.size(matrix_flat,axis)-1,np.size(matrix_flat,axis))
    CoM = np.linspace(0,np.size(matrix_flat,abs(axis-1))-1,np.size(matrix_flat,abs(axis-1)))
    for i in range(0,np.size(matrix_flat,abs(axis-1))):
        CoM[i] = np.sum( (x*matrix_flat[i,:])/np.sum(matrix_flat[i,:]) )
    #CoM = CoM - np.size(matrix_flat,axis)/2
    return CoM
    
def camera_align_cor_takeImage(imageUrl,angle):
    imagePath = IMAGE_PATH + 'ID11_Camera_align_'+timeStr+'_'+str(angle)+'deg.jpg'
    urllib.request.urlretrieve(imageUrl, imagePath)
    with open(imagePath, 'r+') as file:
        image = np.asarray(Image.open(imagePath))
    return(image)

def camera_align_cor_filtering(image):
    image = np.sum(image,2)[:1000,:] #Crop bottom part to avoid date/time area
    image[image <= 320] = 1 #Upper binary
    image[image > 1] = 0 #Lower binary
    return image

def camera_align_cor_camtx(x,camrz): #Pseudo motor camtx moves like samx but with the camera direction instead of the beam
    camtxVals = [round(x*math.cos((camrz*math.pi)/180),4),round(x*math.sin((camrz*math.pi)/180),4)]
    return camtxVals 

def camera_align_cor_camty(y,camrz): #Pseudo motor camty moves like samy but with the camera direction instead of the beam
    camtyVals = [round(-y*math.sin((camrz*math.pi)/180),4),round(y*math.cos((camrz*math.pi)/180),4)]
    return camtyVals 

def camera_align_cor():
    ##CAM_CALIB
    pixelCalib = 0.00415 # mm/px

    ##Take 0deg image
    umv(diffrz,CAM_ANGLE)
    sleep(2)
    zeroDegImage = camera_align_cor_takeImage("http://160.103.31.8/picture/1/current/?_username=admin&_signature=902175615fc60ddb2641c75b3c03f487db82e0ce",0)

    ##Take 180deg image
    umv(diffrz,CAM_ANGLE+180)
    sleep(2)
    oneEightyDegImage = camera_align_cor_takeImage("http://160.103.31.8/picture/1/current/?_username=admin&_signature=902175615fc60ddb2641c75b3c03f487db82e0ce",180)

    ##Filtering
    zeroDegImage = camera_align_cor_filtering(zeroDegImage) 
    oneEightyDegImage = camera_align_cor_filtering(oneEightyDegImage) 

    ##CoR by CoM of the sample
    CoM_zeroDegImage = camera_align_cor_centerOfMass(zeroDegImage,1)
    vScale_zeroDegImage = np.linspace(0,np.shape(zeroDegImage)[0]-1,np.shape(zeroDegImage)[0])
    fitCoM_zeroDegImage = np.poly1d(np.polyfit(vScale_zeroDegImage[100:900], CoM_zeroDegImage[100:900], 1))

    CoM_oneEightyDegImage = camera_align_cor_centerOfMass(oneEightyDegImage,1)
    vScale_oneEightyDegImage = np.linspace(0,np.shape(oneEightyDegImage)[0]-1,np.shape(oneEightyDegImage)[0])
    fitCoM_oneEightyDegImage = np.poly1d(np.polyfit(vScale_oneEightyDegImage[100:900], CoM_oneEightyDegImage[100:900], 1))
    
    CoR_X = (fitCoM_zeroDegImage(589)-fitCoM_oneEightyDegImage(589))/2

    #Move on camera axis camtx
    #print('umvr(samtx,'+str(camera_align_cor_camtx(CoR_X*pixelCalib,CAM_ANGLE)[0])+',samty,'+str(camera_align_cor_camtx(CoR_X*pixelCalib,CAM_ANGLE)[1])+')')
    umvr(samtx,camera_align_cor_camtx(CoR_X*pixelCalib,CAM_ANGLE)[0],samty,camera_align_cor_camtx(CoR_X*pixelCalib,CAM_ANGLE)[1])

    ##Take 90deg image
    umv(diffrz,CAM_ANGLE+90)
    sleep(2)
    ninetyDegImage = camera_align_cor_takeImage("http://160.103.31.8/picture/1/current/?_username=admin&_signature=902175615fc60ddb2641c75b3c03f487db82e0ce",90)

    ##Filtering
    ninetyDegImage = camera_align_cor_filtering(ninetyDegImage)
    CoM_ninetyDegImage = camera_align_cor_centerOfMass(ninetyDegImage,1)
    vScale_ninetyDegImage = np.linspace(0,np.shape(ninetyDegImage)[0]-1,np.shape(ninetyDegImage)[0])
    fitCoM_ninetyDegImage = np.poly1d(np.polyfit(vScale_ninetyDegImage[100:900], CoM_ninetyDegImage[100:900], 1))

    CoR_Y = (max(fitCoM_oneEightyDegImage(589),fitCoM_zeroDegImage(589))-abs(CoR_X))-fitCoM_ninetyDegImage(589)

    #Move on camera axis camty
    umvr(samtx,camera_align_cor_camty(CoR_Y*pixelCalib,CAM_ANGLE)[0],samty,camera_align_cor_camty(CoR_Y*pixelCalib,CAM_ANGLE)[1])
    #print('umvr(samtx,'+str(camera_align_cor_camty(CoR_Y*pixelCalib,CAM_ANGLE)[0])+',samty,'+str(camera_align_cor_camty(CoR_Y*pixelCalib,CAM_ANGLE)[1])+')')

    ##Nabu CoR calc
    #CoR_X_calc = CenterOfRotation()
    #CoR_X = CoR_X_calc.find_shift(zeroDegImage,oneEightyDegImage)

    #CoR_Y_calc = CenterOfRotation()
    #CoR_Y = CoR_Y_calc.find_shift(ninetyDegImage,twoSeventyDegImage)

    ##Check rot
    umv(diffrz,0)

    ##Imshow
    #fig = plt.figure()
    #ax1 = fig.add_subplot(2, 2, 1)
    #ax1.imshow(zeroDegImage)
    #ax1.plot([fitCoM_zeroDegImage(100),fitCoM_zeroDegImage(900)],[0,998],color="yellow")
    #ax2 = fig.add_subplot(2, 2, 2)
    #ax2.imshow(ninetyDegImage)
    #ax2.plot([fitCoM_ninetyDegImage(100),fitCoM_ninetyDegImage(900)],[0,998],color="yellow")
    #ax3 = fig.add_subplot(2, 2, 3)
    #ax3.imshow(oneEightyDegImage)
    #ax3.plot([fitCoM_oneEightyDegImage(100),fitCoM_oneEightyDegImage(900)],[0,998],color="yellow")
    #ax4 = fig.add_subplot(2, 2, 4)
    #ax4.imshow(oneEightyDegImageCheck)
    #ax4.plot([fitCoM_oneEightyDegImageCheck(100),fitCoM_oneEightyDegImageCheck(900)],[0,998],color="yellow")
    #plt.show()

#camera_align_cor()
