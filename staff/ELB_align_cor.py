
from __future__ import print_function, division

from bliss.setup_globals import *
import numpy, time, sys
import numpy as np
from bliss.common import plot as bl_plot
from scipy.optimize import curve_fit


def ELB_align_cor_interactive(mot, rmot, rng, npts, ctim, ctr='eiger:roi_counters:roi1_sum', domove=True, alignto=None, angles=[0,90,180,270]):
    """
    Mainly for ID11
    Try to align the center of rotation by scanning mot
    and using user input of the sample centres or edges
       mot = translation motor
       rmot = rotation motor
       rng = scan range (um)
       npts = points per scan
       ctim = counting time
       ctr = thing to plot
       domove = move to calculated position at end
       alignto = edges (e) or centre (c)? will be prompted if not given
       angles = rot positions (integers) at which to scan - minimum of 3
    """
    #collect data
    datas = {}
    #plot data
    dataplot = bl_plot.plot(xlabel=mot.name, ylabel=ctr, name='align COR')
    for k in angles:
        umv(rmot, k)
        x, datas['%i degrees'%k] = fscan_interactive( mot, rng, npts, ctim, ctr)
        dataplot.plot({'x': x, '%i degrees'%k: datas['%i degrees'%k]}) 
    cens = interactive_cor_fit(alignto, angles, datas, x, ctr, mot, dataplot)
    #fit centres
    print('centres =',cens)
    fitpars, _ = curve_fit(sinfit, angles, cens, [45,1,0])
    f = bl_plot.plot_curve(xlabel=mot.name, ylabel=ctr, name='align COR fit')
    f.plot({'x': np.linspace(min(angles), min(angles)+360, 360), 
          'fit': sinfit(np.linspace(min(angles), min(angles)+360, 360), *fitpars)})
    f.plot({'x': [min(angles), min(angles)+360], 'COR': [fitpars[2], fitpars[2]]})
    f.plot({'x': angles, 'centres': cens}, symbol='o', linestyle='')
    pyerr = fitpars[1] * np.cos(np.radians( fitpars[0] ))
    pxerr = fitpars[1] * np.sin(np.radians( fitpars[0] ))
    dtyideal = fitpars[2]
    print('pyerr =',pyerr,', pxerr =',pxerr,', dtyideal =',dtyideal)
    if domove:
        umvr(py, pyerr, px, pxerr)
        umv(dty, dtyideal)
    else:
        print('umvr(py,',pyerr,', px,',pxerr,')')
        print('umv(dty,',dtyideal,')')


def fscan_interactive( mot, rng, npts, ctim, ctr):
    """ Uses fscan and returns data"""
    mot.sync_hard()
    plotselect(ctr)
    plotinit(ctr)
    mstart = mot.position
    ascan( mot, mstart-rng, rng*2/(npts-1), npts, ctim)
    lastscan = SCANS[-1]
    sleep(1)
    mot.sync_hard()
    A = lastscan.get_data( )
    m = A[ctr] < 1e9
    y = A[ctr][m]
    x = A[mot.name][m]
    mv(mot, mstart)
    return x, y

def sinfit(x,a,b,c):
    """
    a is angle of max displacement
    b is magnitude of displacement
    c is centre of rotation
    """
    return b*np.sin(np.radians(x+a)) + c

def interactive_cor_fit(alignto,angles,datas,x,ctr,mot,dataplot):
    #choose centre or edge alignment
    if alignto == None:
        alignto = input("Align using the centre (c) or edges (e)?")
    if alignto in ["c", "centre", "center", "C"]:
        nbp = 1
    elif alignto in ["e", "edge", "edges", "E"]:
        nbp = 2
    else:
        print("I didn't understand the alignto input, defaulting to centre")
        nbp = 1
    #input centres or edges
    cs = []
    for r in angles:
        dataplot.clear_data()
        dataplot.plot({'x': x, '%i degrees'%r: datas['%i degrees'%r]})
        print("Select position at ",r," degrees")
        cs.append(dataplot.select_points(nbp))
    dataplot.plot(data=datas, x=x)
    if nbp == 1:
        return [cs[k][0][0] for k in range(len(angles))]
    if nbp == 2:
        return [np.mean([cs[k][1][0],cs[k][0][0]]) for k in range(len(angles))]





