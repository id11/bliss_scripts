def flat_correct_acq(dtx):
    umv(ffdtx1,dtx)
    #Doing CeO2
    umv(ffdtz1,(0.085*ffdtx1.position))
    umv(samty,5.56)
    newdataset('ffdtx1_%s_z%d'%( dtx, ffdtz1.position ))
    loopscan(20,0.5)
    #Doing glass
    umv(samty,-4.34)
    loopscan(480,0.5)

    #Doing CeO2
    umv(ffdtz1,(0.085*ffdtx1.position)+25)
    umv(samty,5.56)
    newdataset('ffdtx1_%s_z%d'%( dtx, ffdtz1.position ))
    loopscan(20,0.5)
    #Doing glass
    umv(samty,-4.34)
    loopscan(480,0.5)

    #Doing CeO2
    umv(ffdtz1,(0.085*ffdtx1.position)+50)
    umv(samty,5.56)
    newdataset('ffdtx1_%s_z%d'%( dtx, ffdtz1.position ))
    loopscan(20,0.5)
    #Doing glass
    umv(samty,-4.34)
    loopscan(480,0.5)


def flat_auto():
    dtx_input = [300,400,500,600]
    newsample('flat_calib')
    for d in dtx_input:
        flat_correct_acq(d)

def spatial_dist_scan():
    sheh3.open()
    newsample('Cu_Spheres_2')
    umv(diffrz,0)
    newdataset('flat')
    loopscan(50,0.05)
    sheh3.close()
    newdataset('dark')
    loopscan(50,0.05)
    umv(diffrz,90)
    sheh3.open()
    newdataset('projections')
    dmesh(samtx,-1.1,1.1,4,samtz,-1.1,1.1,4,0.05,backnforth=True)
    umv(diffrz,0)
    newdataset('flat')
    loopscan(50,0.05)
    
    
