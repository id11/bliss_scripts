
import numpy as np, time



# def check_cpm18(pos=6.4158):
def check_cpm18(pos=6.605):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()

def measure_layer_withbeamcheck( yrange, ystep, rspeed=25., exptime=0.002 ):
    ####-90 to 90 scan range, via zero, to avoid worse glitches
    eiger.camera.photon_energy = 44000.
    ACTIVE_MG.disable("mca")
    ACTIVE_MG.enable("eiger")
    rstep = rspeed * exptime
    if exptime < 0.003:
        # 16 bit data from the camera
        eiger.camera.auto_summation='OFF'   
    else:
        # 32 bit data
        eiger.camera.auto_summation='ON'
    pause_for_refill(1)
    fscan2d( dty, -yrange, ystep, 2 * yrange / ystep + 1,
             rot, -90, rstep, np.ceil(180 / rstep) + 1, exptime,
             scan_mode="CAMERA")
    # and clean up:
    eiger.camera.auto_summation = 'ON'
    umv(dty, 0)

def fscanloop360( yrange, ystep, rstep=0.05, rrange=360, rstart=4 ):
    nr = int(np.round(rrange / rstep))    
    for i, y in enumerate( np.arange( -yrange, yrange+ystep/2, ystep ) ):
        umv(dty, y )
        time.sleep(3) # sure - I agree
        pause_for_refill( 30 )
        if i%2 == 0:
            fscan(rot, rstart,  rstep, nr, 0.002, scan_mode = 'CAMERA' )
        else:
            fscan(rot, rstart+rstep*nr, -rstep, nr, 0.002, scan_mode = 'CAMERA' )

    
def myscan():
    newdataset("dt50um")
    y0 = diffty.position    
    for my_y  in np.arange(-0.75,0.76,0.05):
        print(my_y)
        umv(diffty, y0 + my_y)
        umv(diffty, y0 + my_y)
        finterlaced( diffrz, 0, 1, 360, 0.2 )

    sheh3.close()


def top( cut=0.2, highlim=1e9 ):
    s = fscan.scan
    y = s.get_data('eiger:roi_counters:roi2_sum')
    x = s.get_data('axis:dty')
    ymin = np.nanmin( y )
    y = np.where( y > 1e9, ymin, y )
    ymax = np.nanmax( y )
    m = y >= ( (ymax - ymin)*cut + ymin )
    return np.average( x[m], weights=abs( y[m] - cut ) )

def align90():
    eiger.camera.auto_summation='ON'
    plotselect(eiger.counters.roi2_sum)
    umv(rot, 90)
    fscan( dty, -20, 0.1, 401, 0.01, scan_mode='CAMERA' )
    cp = top( )
    umv(rot, 0)
    fscan( dty, -20, 0.1, 401, 0.01, scan_mode='CAMERA' )
    c0 = top( )
    umv(rot, -90)
    fscan( dty, -20, 0.1, 401, 0.01, scan_mode='CAMERA' )
    cm = top( )
    print(cp,c0,cm)
    umv( dty, (cm + cp)*0.5 )
    umvr(px, (cp - cm)*0.5 )
    umvr(py, c0 - (cm + cp)*0.5 )

    
def goldball():
    
    start = time.time()
    # up to 12 hours

    # 27 steps was 5 mins, guess 24 minutes for this
    # 12 hours will give ~25 layers.
    # do a 1 micron spacing
    zpos = 91
    # aim at 7h00                 h    m    s
    while time.time() - start < (12. * 60 * 60 ):
        umv( pz , zpos )

        newdataset( "r2_Z%03d"%(zpos) )

        umv(dty, 0)
        pause_for_refill( 120 )
        align90()
        
        measure_layer_withbeamcheck( 13, 0.2 )
        zpos = zpos - 1
        
        
def full1(ymax, datasetname, ystep=5, ymin=1801):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.125, 2888, 0.005 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 361, -0.125, 2888, 0.005 )
        
                  
    
def half1(ymax, datasetname, ystep=5, ymin=-1801):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.05, 3620, 0.002 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 181, -0.05, 3620, 0.002 )

def half1mca(ymax, datasetname, ystep=5, ymin=-1801):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, -90,  0.125, 1448, 0.005 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 91, -0.125, 1448, 0.005 )

            
def MoAu():
    for zpos in np.arange(52.1,60,0.1):
        umv(pz, zpos)
        half1mca(9, 'Z%04d'%(zpos*10), ystep=0.15 )
            
def NbC():
    for zpos in (45,50,51,56):
        umv(pz, zpos)
        half1( 145, 'Z'+str(zpos), ystep=0.3 )
        
                          
def  ktype():
    for zpos in (10,20,30,40,50,60,70,80,90):
        umv(pz, zpos)
        half1( 300, 'bZ'+str(zpos), ystep=1)

def  ktype2():

    for zpos in (10,20,30,40,50,60,70,80,90):
        umv(pz, zpos)
        half1( 270, 'cZ'+str(zpos), ystep=1)

def  ktype3():

    for zpos in (10,20,30,40,50,60,70,80,90):
        umv(pz, zpos)
        half1( 280, 'cZ'+str(zpos), ystep=1)        
                
def check_tfoh1(rng=0.5, npt=100, ctime=0.1):
    tfoh1.set(0, 'Al')   
    for nbe in (16,12,8,4,0):
        tfoh1.set(nbe, 'Be')
        si = {'instrument': { 'CRL' : {'nBe':10, 'nAl': 0} } }
        dscan(py,-rng,rng,npt,ctime, save_images=False, scan_info=si)
        goto_cen()
        dscan(pz,-rng,rng,npt,ctime, save_images=False, scan_info=si)
        goto_cen()
        
              
              
def measure_beam(rng=1):
    for i in range(2):
        dscan(px, -rng, rng, 80, 0.1)
        goto_cen()
        dscan(pz, -rng, rng, 80, 0.1)
        goto_cen()

    
def choose_u22( e ):
    gold = 12
    for i in range(1,12,2):
        ef = e/i
        if ef < id11.undulator.id11_u22.Emax:
            g = id11.undulator.id11_u22.gap_from_ef( ef )
            if g < 7:
                return gold
            gold = g
    return gold

def tune_energy( e ):
    ll_goto_energy( e )
    lrock2(0.03)
    lrock2(0.01)
    umv(u22, choose_u22( e ) )
    dscan(u22, -0.1, 0.1, 40, 0.1 )
    goto_peak( pico0 )
    try:
        fsh_tdxrd.open()
        plotselect(pico3)
        dscan(llrx2, -0.03, 0.03, 40, 0.1 )
        goto_cen(pico3)
    except Exception as e:
        print(e)
    finally:
        fsh_tdxrd.close()
    measure_marana_eff( 0.2 )
    
def measure_marana_eff(t):
    try:
        fsh_tdxrd.open()
        sleep(0.1)
        loopscan(10,t,pico0, pico3, pico4, marana)
    except Exception as e:
        print(e)
    finally:
        fsh_tdxrd.close()
    



def tile1():
    
    for y in np.arange(-3,3.025,0.05):
        umv(diffy,y)
        finterlaced(hrz, 5., -0.1, 100, 0.2 )
    for y in np.arange(-3,3.025,0.05):
        umv(diffy,y)
        finterlaced(hrz, -175., -0.1, 100, 0.2 )
    umv(diffy,0)

        


def eigerFlipTest():
    
    for r in ["NONE", "90", "180", "270"]:
        eiger.image.rotation = r
        for f0 in (False,True):
            for f1 in (False,True):
                eiger.image.flip = [f0, f1]

                newdataset(f'r_{r}_{f0}_{f1}')
                loopscan(10,1)
                
                
            
        
    
