import os, sys
import numpy as np

import time
from bliss.common import cleanup


def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.05
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.2
    dct_pars['refon'] = dct_pars['num_proj']
    dct_pars['ref_step'] = -3
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.4
    dct_pars['slit_vg'] = 0.56
    dct_pars['dist'] = 4
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 4.32
    dct_pars['shift_step_size'] = 0.23
    dct_pars['nof_shifts'] = 1
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars

def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 0.25
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.085
    ff_pars['slit_hg'] = 1.45
    ff_pars['slit_vg'] = 0.12
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = 8.3010 #1.007
    ff_pars['shift_step_size'] = 0.12
    ff_pars['nof_shifts'] = 7
    return ff_pars
        
    
def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')

def goto_sample():
    umv(samtx, -2.27)
    umv(samty, -0.47)
    umv(samtz, 4.40)
    umv(difftz, -18.3311680)


def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    #umv(s8vg, pars['slit_vg'] + 0.05, s8hg, pars['slit_hg'] + 0.05)
    umv(s8vg, pars['slit_vg'] + 0.05)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)
    
def ff_zseries(ff_pars, datasetname = 'ff'):
    #user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    #umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    umv(diffrz, ff_pars['start_pos'], s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    #umv(s8vg, ff_pars['slit_vg'] + 0.05, s8hg, ff_pars['slit_hg'] + 0.05)
    umv(s8vg, ff_pars['slit_vg'] + 0.05)
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    #user.cpm18_goto(cpm18_tuned)
    #umv(u22, u22_tuned)
    print('ff_zseries succeed')
    return('Succeed')


def AA5182_nscope():
    # top 50 um position
    ntz0 = -2.5210
    pz0 = 50.0
    umv(ntz,ntz0)
    umv(pz,pz0)
    
    # parameters
    ystep=0.16
    ymax=55.5
    ymin=-45.5
    
    rstep=0.125
    astart=-90
    arange=181
    expotime=0.005
    
    datasetname = 'sliceZ_-50um'
    half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)

    print("Successfully done !!!")   
    sheh3.close()
    
    
def AA5182_nscope_series():
    # top 50 um position
    ntz0 = -2.5210
    pz0 = 50.0
    umv(ntz,ntz0)
    umv(pz,pz0)
    
    # parameters
    ystep=0.16
    ymax_series=[55.5 for _ in range(10)]
    ymin_series=[-45.5 for _ in range(10)]
    
    rstep=0.125
    astart=-90
    arange=181
    expotime=0.005
    
    z_vals = [-25.0, 25.0]
    dsetnames = ['sliceZ_25um', 'sliceZ_75um']
    sheh3.open()
    for i, z_val in enumerate(z_vals):
        umv(pz, pz0 + z_val)
        datasetname = dsetnames[i]
        print(datasetname)
        ymax = ymax_series[i]
        ymin = ymin_series[i]
        #print(ymax,ymin)
        half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
        
    print("Successfully done !!!")
    sheh3.close()

def AA5182_ref_pos():
    # top 50 um position
    umv(ntx,1.85)
    umv(nty,-0.1290)
    umv(ntz,-2.5210)
    umv(dty,0)
    
    umv(px, 30.4)
    umv(py, 38.7)
    umv(pz, 50.0)


    
def half1(ymax,
          datasetname,
          ystep=5,
          ymin=-550,
          rstep=0.25,
          astart = -90,
          arange = 181,
          expotime=0.01):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)

def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t / 3.)
        
class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        #if self.expotime < 0.005:
        #    ACTIVE_MG.disable('mca')
        #    print("Disabled mca: too fast scan")
        #    eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )



