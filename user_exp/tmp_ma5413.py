def define_pct_pars_sagbo():
    pct_pars={}
    pct_pars['start_pos'] = -1
    pct_pars['step_size'] = 0.36
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.04
    pct_pars['refon'] = pct_pars['num_proj']
    pct_pars['ref_int'] = 10
    pct_pars['refy_step'] = 0.8
    pct_pars['refz_step'] = -8
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 100
    pct_pars['ndarks'] = 40
    pct_pars['moty'] = samy
    pct_pars['motz'] = difftz
    pct_pars['slit_hg'] = 2
    pct_pars['slit_vg'] = 2
    pct_pars['dist'] = 100
    return pct_pars
    
def sagbo_scan_dict(scanname, pars, nacq=700, last_scan = None):
	
	last_scan = None
	#take darks
	newdataset('sagbo_darks')
	print(20*'-', '\n', 'Will take dark images, closing shutter', '\n', 20*'-')
	fsh.disable()
	ftimescan(pars['exp_time'], pars['ndarks'])
	fsh.enable()    
       
    
    #set the scan positions
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.2, s8hg, pars['slit_hg'] + 0.2)
    
    #how many scans
    for ii in range(nacq):
		
		if last_scan is None:
			last_scan = ii
			
		newdataset(scanname + f'_{last_scan}')
		#z flat fielding at every ref int scans
		if last_scan % pars['ref_int']:
			#TODO: how to keep the load
			
			print(20*'-', '\n', 'Performing z-flat field scan', '\n', 20*'-')
			#move difftz to take flat
			print('', '\n', 'Will move difftz and take flats', '\n', '')
			umvr(pars['motz'], pars['refz_step'])
			#flats
			ftimescan(pars['exp_time'], pars['nref'])
			
			#move difftz back
			print('', '\n', 'Moving back difftz', '\n', '')
			umvr(pars['motz'], -1*pars['refz_step'])
			
			#acquire projections
			print(20*'-', '\n', 'Will acquire images', '\n', 20*'-')
			fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode=pars['scan_mode'])
			umv(diffrz, 360)
			diffrz.position = 0
		else:
			#TODO: how to keep loading at constant rate
			
			print(20*'-', '\n', 'Performing y-flat field scan', '\n', 20*'-')
			#move difftz to take flat
			print('', '\n', 'Will move samy and take flats', '\n', '')
			umvr(pars['moty'], pars['refy_step'])
			#flats
			ftimescan(pars['exp_time'], pars['nref'])
			
			#move difftz back
			print('', '\n', 'Moving back samy', '\n', '')
			umvr(pars['moty'], -1*pars['refy_step'])
			
			#acquire projections
			print(20*'-', '\n', 'Will acquire images', '\n', 20*'-')
			fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode=pars['scan_mode'])
			umv(diffrz, 360)
			diffrz.position = 0

