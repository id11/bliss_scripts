"""
DCT acquisition class macro

James Ball
Haixing Fang
Charles Romain
03/09/2024
"""

# TODO
# scanmode for frelon - ref_scan
# scanmode for eiger


import os
import json

import pprint
import numpy as np


import bliss.setup_globals as bliss_globals


class TDXRDScan:
    """A base class for DCT and 3DXRD acquisition on the TDXRD station.
       Should not be used directly"""
       
    def __init__(self):
        pass

    def get_serialisable_pars(self):
        """Serialise all pars - mainly converts axes to strings"""
        
        motor_pars = ['ref_mot', 'zmot']
        
        serial_pars = self.pars.copy()
        for key, value in serial_pars.items():
            if key in motor_pars:
                serial_pars[key] = value.name
        
        return serial_pars
    
    def export_pars(self, filename):
       """Export the parameters used to a file on disk."""
       
       pars_to_dump = self.get_serialisable_pars()
       with open(filename, 'w') as fout:
           json.dump(pars_to_dump, fout, indent = 2)
    
    @staticmethod
    def ref_scan(ref_mot, exp_time, nref, ref_step, scanmode):
        """
        Move ref_mot by ref_step
        Take a number of reference images
        Move ref_mot back to start
        """
        refpos = ref_mot.position
        umv(ref_mot, refpos + ref_step)
        print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
        ftimescan(exp_time, nref, scan_mode=scanmode)
        umv(ref_mot, refpos)
    
    def set_slits(self):
        umv(s7vg, self.pars['slit_vg'], s7hg, self.pars['slit_hg'])
        if self.pars['use_slit8']:
            umv(s8vg, self.pars['slit_vg'] + 0.05, s8hg, self.pars['slit_hg'] + 0.05)


class DCTScan(TDXRDScan):
    """
    # attributes:
    self.pars - dictionary of acquisition parameters
    
    # methods:
    self.ref_scan(...) - take a number of reference projections without moving sample (e.g flats, darks)
    self.run_single(...) - run a single scan
    self.run_zseries(...) - run multiple scans over a z series
    
    Usage in Bliss:
    
    ```
    (align sample...)


    dct = DCTScan()
    # check parameters...
    dct.pars
    # maybe change a parameter
    dct.pars['slit_hg'] = 1.0
    # happy with pars?
    dct.run_single()
    ```

    """
    
    def __init__(self):
        super().__init__()
        self.pars = self.default_pars()
        print('Default pars loaded:')
        pprint(self.pars)
    
    @staticmethod
    def default_pars():
        """Return a typical dict of DCT parameters - should be normally fine for standard samples"""
        
        pars = {}
        pars['start_pos'] = 0
        pars['step_size'] = 0.1
        pars['num_proj'] = 360 / pars['step_size']  
        pars['exp_time'] = 0.1
        pars['refon'] = pars['num_proj']
        pars['ref_step'] = -3
        pars['scan_mode'] = 'CAMERA'
        pars['nref'] = 41
        pars['slit_hg'] = 0.8
        pars['slit_vg'] = 0.3
        pars['ref_mot'] = samy
        pars['zmot'] = samtz
        pars['samtz_cen'] = pars['zmot'].position
        pars['shift_step_size'] = 0.04
        pars['nof_shifts'] = 1
        pars['use_slit8'] = True
        
        return pars
    
    def run_single(self, scanname='dct'):
        """Collect single DCT scan"""
        newdataset(scanname)
        # set slits
        self.set_slits()
        # rotate to start angle
        umv(diffrz, self.pars['start_pos'])
        num_im_grp = self.pars['refon']
        # this is to take reference images every N chunks of images
        # this is calculated by num_proj/refon
        # normally refon == num_proj
        for ref_grp in np.arange(0, self.pars['num_proj']/self.pars['refon'], 1):
            # starting angle for this group of projections
            startpos_grp = ref_grp * self.pars['refon'] * self.pars['step_size'] + self.pars['start_pos']
            # Take flat for this group of projections
            self.ref_scan(self.pars['ref_mot'], self.pars['exp_time'], self.pars['nref'], self.pars['ref_step'], self.pars['scan_mode'])
            print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, self.pars['step_size'], num_im_grp, self.pars['exp_time'], self.pars['scan_mode']))
            # Take fscan - the actual rotation for this group of projections
            fscan(diffrz, startpos_grp, self.pars['step_size'], num_im_grp, self.pars['exp_time'], scan_mode = self.pars['scan_mode'])
        # Take flat at the end
        self.ref_scan(self.pars['ref_mot'], self.pars['exp_time'], self.pars['nref'], self.pars['ref_step'], self.pars['scan_mode'])
        # Disables fast shutter so it doesn't open while collecting darks
        fsh.disable()
        # Take dark
        self.ref_scan(self.pars['ref_mot'], self.pars['exp_time'], self.pars['nref'], 0, self.pars['scan_mode'])
        # Re-enable fast shutter at the end
        fsh.enable()
        # Move back to start omega angle
        umv(diffrz, self.pars['start_pos'])
        # Save pars to disk
        # get pars filename
        scan = SCANS[-1]
        par_file_url = scan.scan_saving.filename.replace('.h5', '_pars.json')
        self.export_pars(filename=par_file_url)
        print('Single DCT scan done')
    
    def run_zseries(self, scanname='dct'):
        """Collect zseries of DCT scans"""
        shift_step_size = abs(self.pars['shift_step_size'])
        if(self.pars['nof_shifts'] < 1):
            print('The number of shifts is incorrect')
            return('The number of shifts is incorrect')
        offset_samtz_pos = self.pars['samtz_cen'] - (self.pars['nof_shifts'] - 1) * shift_step_size / 2
        print('Central samtz:' + str(self.pars['samtz_cen']))
        if(offset_samtz_pos < self.pars['zmot'].low_limit or offset_samtz_pos + (self.pars['nof_shifts'] - 1) * shift_step_size > self.pars['zmot'].high_limit):
            print('Exceed the limits of samtz')
            return('Exceed the limits of samtz')
        for iter_i in range(int(self.pars['nof_shifts'])):
            umv(self.pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
            self.run_single(scanname=scanname + str(iter_i + 1))
        umv(self.pars['zmot'], self.pars['samtz_cen'])
        print('Zseries DCT scans done')

class FFScan(TDXRDScan):
    """
    # attributes:
    self.pars - dictionary of acquisition parameters
    
    # methods:
    self.run_single(...) - run a single scan
    self.run_zseries(...) - run multiple scans over a z series
    
    Usage in Bliss:
    
    ```
    (align sample...)


    ffs = FFScan()
    # check parameters...
    ffs.pars
    # maybe change a parameter
    ffs.pars['slit_hg'] = 1.0
    # happy with pars?
    ffs.run_single()
    ```

    """
    
    def __init__(self):
        super().__init__()
        self.pars = self.default_pars()
        print('Default pars loaded:')
        pprint(self.pars)
    
    @staticmethod
    def default_pars():
        """Return a typical dict of far-field parameters - should be normally fine for standard samples"""
        
        pars = {}
        pars['start_pos'] = 0
        pars['step_size'] = 0.25
        pars['num_proj'] = 360 / pars['step_size']  
        pars['exp_time'] = 0.1
        pars['refon'] = pars['num_proj']
        pars['ref_step'] = -3
        pars['nref'] = 41
        pars['slit_hg'] = 1.0
        pars['slit_vg'] = 0.2
        pars['ref_mot'] = samy
        pars['zmot'] = samtz
        pars['samtz_cen'] = pars['zmot'].position
        pars['shift_step_size'] = 0.04
        pars['nof_shifts'] = 1
        pars['use_slit8'] = True
        
        return pars
    
    def run_single(self, scanname='ff'):
        """Collect single far-field 3DXRD scan"""
        newdataset(scanname)
        # set slits
        self.set_slits()
        # rotate to start angle
        umv(diffrz, self.pars['start_pos'])
        
        # Take flats
        self.ref_scan(self.pars['ref_mot'], self.pars['exp_time'], self.pars['nref'], self.pars['ref_step'], self.pars['scan_mode'])
        # Do interlaced scan
        finterlaced(self.pars['ref_mot'], self.pars['start_pos'], self.pars['ref_step'], self.pars['num_proj'], self.pars['exp_time'], mode='ZIGZAG')
        # Take darks
        # Disables fast shutter so it doesn't open while collecting darks
        fsh.disable()
        self.ref_scan(self.pars['ref_mot'], self.pars['exp_time'], self.pars['nref'], 0, self.pars['scan_mode'])
        
        
        num_im_grp = self.pars['refon']
        # this is to take reference images every N chunks of images
        # this is calculated by num_proj/refon
        # normally refon == num_proj
        for ref_grp in np.arange(0, self.pars['num_proj']/self.pars['refon'], 1):
            # starting angle for this group of projections
            startpos_grp = ref_grp * self.pars['refon'] * self.pars['step_size'] + self.pars['start_pos']
            # Take flat for this group of projections
            self.ref_scan(self.pars['ref_mot'], self.pars['exp_time'], self.pars['nref'], self.pars['ref_step'], startpos_grp, self.pars['scan_mode'])
            print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, self.pars['step_size'], num_im_grp, self.pars['exp_time'], self.pars['scan_mode']))
            # Take fscan - the actual rotation for this group of projections
            fscan(diffrz, startpos_grp, self.pars['step_size'], num_im_grp, self.pars['exp_time'], scan_mode = self.pars['scan_mode'])
        # Take flat at the end
        self.ref_scan(self.pars['ref_mot'], self.pars['exp_time'], self.pars['nref'], self.pars['ref_step'], self.pars['start_pos'] + self.pars['num_proj']*self.pars['step_size'], self.pars['scan_mode'])
        # Disables fast shutter so it doesn't open while collecting darks
        fsh.disable()
        # Take dark
        self.ref_scan(self.pars['ref_mot'], self.pars['exp_time'], self.pars['nref'], 0, self.pars['start_pos'] + self.pars['num_proj']*self.pars['step_size'], self.pars['scan_mode'])
        # Re-enable fast shutter at the end
        fsh.enable()
        # Move back to start omega angle
        umv(diffrz, self.pars['start_pos'])
        # Save pars to disk
        # get pars filename
        scan = SCANS[-1]
        par_file_url = scan.scan_saving.filename.replace('.h5', '_pars.json')
        self.export_pars(filename=par_file_url)
        print('Single DCT scan done')
    
    def run_zseries(self, scanname='dct'):
        """Collect zseries of DCT scans"""
        shift_step_size = abs(self.pars['shift_step_size'])
        if(self.pars['nof_shifts'] < 1):
            print('The number of shifts is incorrect')
            return('The number of shifts is incorrect')
        offset_samtz_pos = self.pars['samtz_cen'] - (self.pars['nof_shifts'] - 1) * shift_step_size / 2
        print('Central samtz:' + str(self.pars['samtz_cen']))
        if(offset_samtz_pos < self.pars['zmot'].low_limit or offset_samtz_pos + (self.pars['nof_shifts'] - 1) * shift_step_size > self.pars['zmot'].high_limit):
            print('Exceed the limits of samtz')
            return('Exceed the limits of samtz')
        for iter_i in range(int(self.pars['nof_shifts'])):
            umv(self.pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
            self.run_single(scanname=scanname + str(iter_i + 1))
        umv(self.pars['zmot'], self.pars['samtz_cen'])
        print('Zseries DCT scans done')
