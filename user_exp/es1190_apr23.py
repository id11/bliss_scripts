
import time, numpy as np

# user_script_load("es1190.py")
user_script_load('optics.py')


# def check_cpm18(pos=6.4158):
def check_cpm18(pos=10.073):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()

def half1(ymax, datasetname, ystep=5, ymin=-1801):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.05, 3620, 0.002 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 181, -0.05, 3620, 0.002 )

def full1(ymax, datasetname, ystep=10, ymin=-99901):
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    eiger.camera.auto_summation='OFF'
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.05, 7220, 0.002 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 361, -0.05, 7220, 0.002 )
    eiger.camera.auto_summation='ON'

def loop_slice(pos_z,zo,ymax):
    for i in pos_z:
        umv(ntz,i+zo)
        full1(ymax,str(int(i*1E3))+'um')

def fri():
    ymax = 3100
    z0 = -4.075
    zp = (2,5,8)
    loop_slice( zp, z0, ymax )

def sat():
    ymax = 3100
    z0 = -4.5
    zp = (3,8)
    loop_slice(zp,z0,ymax)

def olivine_nscope():
    umv(pz,99)
    half1( 650, "DT1umZ99", ystep=1)
    umv(pz,79)
    half1( 650, "DT1umZ79", ystep=1)
    umv(pz,59)
    half1( 650, "DT1umZ59", ystep=1)
    
    umv( dty, 0)

def olivine_pristine_nscope():
    umv(pz,10)
    half1( 400, "DT1umZ10", ystep=1)
    umv(pz,90)
    half1( 680, "DT1umZ90", ystep=1)
    umv(pz,50)
    half1( 550, "DT1umZ50", ystep=1)
    umv( dty, 0)

def sat_night():
    z0 = 0.173
    ymax = 400
    for z in np.arange(20, 221, 10):
        umv(ntz, z0 + z/1e3)
        full1( ymax, f'z{z}', ystep=7.5 )

        
def sun_failed_sample_DM1735_4A_02():
    z0 = 3.4240
    ymax = 300
    for z in np.arange(0,19.73,3):# STOPPED DURING SCAN at 12.0
        umv(ntz, z0 + z/1e3)
        full1( ymax, f'z{z}', ystep=5)
        
def sun():
    z0 = 3.4240
    ymax = 300
    zz=(19.73,39.46)
    for z in zz:
        umv(ntz, z0 + z/1e3)
        full1( ymax, f'z{z}', ystep=5)
        
        
def sun_night():
    z0   = -4.1
    ymax = 1320
    zz=(1900,2100,2300)
    for z in zz:
        umv(ntz, z0 + z/1e3)
        full1( ymax, f'z{z}', ystep=5)

def mon():
    z0   = -4.1
    ymax = 1320
    zz=(1700,2500)
    for z in zz:
        umv(ntz, z0 + z/1e3)
        full1( ymax, f'z{z}', ystep=5)


def goto_basler():
    moveto_basler()                           # go to imaging camera
    umv(pinz, 0.3, piny, 17.32, bigy, 24)     # bigger beam
    umv(dty,0)
    umv(attrz, 0, atty, -10)                  # attenuator out

# tomo recipe
# user_script_load('tomo_by_ascan')
# user.tomo_by_ascan(0,360,3600,0.01,rot='rot', ymotor='ntz', ystep=-1)

    
    
def goto_focused():
    umv(pinz, -0.0152, piny, 0.03, bigy, 0.0048)
    umv(attrz, -10, atty, 0)


    
