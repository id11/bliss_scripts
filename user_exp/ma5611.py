import numpy as np
import time
from bliss.common import cleanup
user_script_load('tomo_by_ascan')

def goto_tomo():
    moveto_basler()
    umv(bigy,22)
    umv(s9vg,2,s9hg,2)
    umv(attrz,0,atty,-10)
    umv(rot,0)

#Align CoR based on rot, 0, 90, 180 (moving ntx, nty)
#umv(rot,0)
#ct(0.01)
#reference one edge of the grain with crossmarker
#umv(rot,-90)
#reference the same edge
#umv(nty, to the middle of the 2 cross)
#reference this middle of the 2 cross
#umv(rot,90)
#umv(ntx, to this middle of the 2 cross)
#umv(rot,0)

def start_tomo(dsetname):
    sheh3.open()
    umv(bigy,22)
    newdataset(dsetname)
    user.tomo_by_ascan(-90,90,1800,0.01,ystep=3,rot='rot',ymotor='nty')

def check_tdxrd():
    umv(s9hg,1.0,s9vg,0.4)
    ct(0.01)


def goto_tdxrd():
    umv(attrz,-7.5,atty,0)
    myhg = s9hg.position
    myvg = s9vg.position
    umv(s9hg,0.05,s9vg,0.05)
    moveto_eiger()
    umv(bigy,22)
    umv(s9hg,myhg,s9vg,myvg)
    
def start_tdxrd(dsetname):
    newdataset(dsetname)
    fscan(rot,-90,0.05,3620,0.002)
    
def goto_scanning():
    umv(bigy,0)
    umv(bigy,0)
    umv(s9hg,0.1,s9vg,0.1)
    umv(attrz,-5.25,atty,0)

#newdataset('align')
#Run align_cor_interactive(400)
#Run dscan(ntz,-0.5,0.5,100,0.01) to get ntz_top

def start_scanning(dsetname,ntz_top): #1.4 top of the sample, needs dscan(ntz,-0.5,0.5,100,0.01) to get this
    ntzstart = ntz.position
    ntzpos = np.linspace(ntz_top,ntz_top+0.015*24,25)   
    for i,z in enumerate(ntzpos):
        umv(ntz,z)
        half1(570,dsetname+'_%s'%i,ystep=15,ymin=-570)
    umv(ntz,ntzstart)

def circle_scan(dsetname,ntz_top,ntz_down):
    #This is an attempt at scanning a circle around the grain to save some time. 
    #It was made late at night and not tested but it is probably correct...
    #There is room for improvements in how to handle the units (microns for y and mm for z)
    # It will save about one hour.
    ntzstart = ntz.position
    r = 1000*np.abs((ntz_top-ntz_down)/2.)+15.
    ntzpos = np.linspace(1000*ntz_down,1000*ntz_top+15,34)
    zmid = 1000*(ntz_top+ntz_down)/2.   
    for i,z in enumerate(ntzpos):
        umv(ntz,z/1000.) #z motor is in mm
        ymax = np.sqrt(r**2-(z-zmid)**2) #in micron
        half1(ymax,dsetname+'_%s'%i,ystep=15,ymin=-ymax)
    umv(ntz,ntzstart)
    
        
class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )  
    
    
def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    
def half1(ymax,
          datasetname,
          ystep=0.25,
          ymin=-1801,
          rstep=0.05,
          astart = -90,
          arange = 181,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)
