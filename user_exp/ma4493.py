
user_script_load("tomo_by_ascan")
user_script_load("optics")


import numpy as np
import time


#SIMULATE = True
SIMULATE = False

def pause_for_refill(t):
    if SIMULATE:
        return
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)


        
my_nscan = 0
        
def myfscan(*a,**k):
    if SIMULATE:
        global my_nscan
        my_nscan += 1
        print(my_nscan)
        return
    try:
        fscan(*a,**k)
    except: # pretty hopeless but why not ?
        print("Got an arror, sleep and then try again" )
        time.sleep(180)
        fscan(*a,**k)

        

def myumv(*a,**k):
    if SIMULATE:
        print(a[0].name,a[1])
        return
    umv(*a,**k)
    

def layer_smaller_360_test():
    y0 = 0.0  # centre of rotation
    width = 3
    step  = 0.3
    ypos = np.arange(-step, width+step/10, step)
    astep = 0.1
    anrange = 360.
    speed = 7.5
    eiger.camera.photon_energy=43468.9
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="ON"
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"
    
def layer_360():
    y0 = 100.0  # centre of rotation
    width = 800.0
    step  = 40 # does 1/2 steps
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.05
    anrange = 360.
    speed = 25.
    eiger.camera.photon_energy=43468.9
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    ACTIVE_MG.disable("*bpm*")
    ACTIVE_MG.disable("*basler*")
    for my_y in ypos:
        myumv(dty, y0+my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, y0+my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="OFF"

def layer_360_test_contd():
    y0 = 100.0  # centre of rotation
    width = 800.0
    step  = 40 # does 1/2 steps
    ypos = np.arange(0, width+step/10, step)
    astep = 0.05
    anrange = 360.
    speed = 25.
    eiger.camera.photon_energy=43468.9
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    ACTIVE_MG.disable("*bpm*")
    ACTIVE_MG.disable("*basler*")
    for my_y in ypos:
        myumv(dty, y0+my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, y0+my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="OFF"


def boxScan():
    astep = 0.05
    anrange = 360.
    speed = 25.
    eiger.camera.photon_energy=43468.9
    
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    ACTIVE_MG.disable("*bpm*")
    ACTIVE_MG.disable("*basler*")
    #
    umv( s9hg , 0.02, s9vg, 1)
    y0 = 0.0  # centre of rotation
    width = 800.0
    step  = 40 # does 1/2 steps
    ypos = np.arange(y0-width, y0+width+step/10, step)
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')

    umv( dty, y0 )
    umv( s9hg, 1.5, s9vg, 0.02)
    z0 = shtz.position
    zstep = 0.04
    zpos = np.arange( z0 - 0.5, z0 + 0.501, zstep)
    for my_z in zpos:
        myumv(shtz, my_z)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_z + zstep/2) > width:
            break
        myumv(shtz, my_z)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    umv(shtz, z0)

    umv( s9hg, 0.02, s9vg, 0.02)

    

def volscan():
    pz0 = 34.52
    pz_rel = [1.27, 3.34, 5.31, 8.27, 10.55, 12.87, 16.45, 18.76, 21.82, 25.86, 30.9, 60]
    k = 0
    for zrel in pz_rel:
        if ( k % 3 ) == 0:
            pz0 = ref_sample( pz0, zrel )
        if k == 11:
            pz0 = ref_sample( pz0, zrel )
        k +=1          
        umv( pz, pz0 + zrel )
        newdataset( "360_Z%03d"%( k )  )
        layer_smaller_360_test()

        

def pct_layers():
    pz0 = 40
    pz_vals = np.arange(0,40,3)
    frames=1440
    extime=0.3
    for zrel in pz_vals:
        umv(pz, pz0 + zrel)
        newdataset('z%03d'%int(zrel))
        pause_for_refill(14*60)
        user.tomo_by_ascan(0,360, frames, extime, ystep=60, rot='rot', ymotor='dty', sleep_time=0.01, ndark=21, nflat=21)
    



    
def attin():
    lfyaw = config.get("lfyaw")
    umv(lfyaw,11)

def attout():
    lfyaw = config.get("lfyaw")
    umv(lfyaw,1)


def goto_PCT():
    sheh3.close()
    umv(det2y, 0.02125, s9hg, 2, s9vg, 1.5)
    ACTIVE_MG.disable("eiger*")
    ACTIVE_MG.enable("basler_eh32:image")
    attout()

def goto_DIF():
    sheh3.close()
    #attin()
    umv(det2y, -234.46, s9hg, 0.02, s9vg, 0.02)
    ACTIVE_MG.enable("eiger:image")
    ACTIVE_MG.enable("eiger:roi*")
    ACTIVE_MG.disable("basler_eh32:image")
    




###############################################


def collect_pct_quartz1():
    # offset rotation axis and do 360s at two heights
    
    umv(u22, 8.3159)
    user.cpm18_goto(6.38)
    # one on axis
    umv(shtz, -0.98)
    umv(dty, 0)
    ACTIVE_MG.disable("basler*")
    ACTIVE_MG.disable("eiger*")
    ACTIVE_MG.enable("basler_eh32:image")
    user.tomo_by_ascan( 0, 360, 1500, 0.02, ystep=2000,
                        rot='rot', ymotor='dty', 
                        ndark=21, nflat=21) 
    umv(dty, 500)
    user.tomo_by_ascan( 0, 360, 1500, 0.02, ystep=1500,
                        rot='rot', ymotor='dty', 
                        ndark=21, nflat=21) 
    umv(shtz, -1.38)
    umv(dty, 0)
    user.tomo_by_ascan( 0, 360, 1500, 0.02, ystep=2000,
                        rot='rot', ymotor='dty', 
                        ndark=21, nflat=21) 
    umv(dty, 500)
    user.tomo_by_ascan( 0, 360, 1500, 0.02, ystep=1500,
                        rot='rot', ymotor='dty', 
                        ndark=21, nflat=21) 

# 20 N z from -0.53 to -1.73
# 40 N z from -0.53 to -1.68

def quartz1_boxScan():
    astep = 0.05
    anrange = 360.
    speed = 25.
    eiger.camera.photon_energy=43468.9
    
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    ACTIVE_MG.disable("*bpm*")
    ACTIVE_MG.disable("*basler*")
    #
    umv(shtz, (-1.73-0.53)/2)
    umv( s9hg , 0.02, s9vg, 1.4)
    y0 = 0.0  # centre of rotation
    width = 740.0
    step  = 40 # does 1/2 steps
    ypos = np.arange(y0-width, y0+width+step/10, step)
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')

    umv( dty, y0 )
    umv( s9hg, 1.5, s9vg, 0.02)
    z0 = shtz.position
    zstep = 0.04
    #### HERE
    # 10 N zpos = np.arange( -1.93, -0.479, zstep)
    # 20 N
    zpos = np.arange( -1.73, -0.53, zstep)
    for my_z in zpos:
        myumv(shtz, my_z)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_z + zstep/2) > width:
            break
        myumv(shtz, my_z)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    umv(shtz, z0)

    umv( s9hg, 0.02, s9vg, 0.02)





def halflayer_360(skip=0, ystep=None):
    y0 = 0.0  # centre of rotation
    width = 760.0
    if ystep is None:
        step  = 40 # does 1/2 steps
    else:
        step = ystep
    ypos = np.arange(-step, width+step/10, step)
    astep = 0.05
    anrange = 360.
    speed = 25.
    eiger.camera.photon_energy=43468.9
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    myumv(u22, 8.3156)
    myumv(cpm18, 6.3783, cpm18t, 0.07)
    if not SIMULATE:
        ACTIVE_MG.disable("mca")
        eiger.camera.auto_summation="OFF"
        ACTIVE_MG.enable("eiger:image")
        ACTIVE_MG.disable("*bpm*")
        ACTIVE_MG.disable("*basler*")
    for k,my_y in enumerate(ypos):
        if k < skip:
            continue
        myumv(dty, y0+my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, y0+my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="OFF"



def volscan20N():
    umv( s9hg, 0.02, s9vg, 0.02)
    zstep = 0.02
    zpos = np.arange( -1.73, -0.53, zstep)
    for k in range(len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("DT0_z%03d"%(k))
        else:
            newdataset("DT0_z%03d"%(k))
        halflayer_360()
        
def volscan20N_cont():
    umv( s9hg, 0.02, s9vg, 0.02)
    zstep = 0.02
    zpos = np.arange( -1.73, -0.53, zstep)
    myumv(shtz, zpos[58])
    newdataset("DT0_z%03d_2"%(58))
    halflayer_360(skip=11)
    for k in range(59,len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("DT0_z%03d"%(k))
        else:
            newdataset("DT0_z%03d"%(k))
        halflayer_360()


# 40 N z from -0.53 to -1.68
def volscan40N():
    sheh3.open()
    umv( s9hg, 0.025, s9vg, 0.025)
    zstep = 0.025
    zpos = np.arange( -1.68, -0.53, zstep)
    for k in range(12,len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("40NDT1_z%03d"%(k))
        else:
            if k > 12:
                newdataset("40NDT1_z%03d"%(k))
        halflayer_360(ystep=50.)

# 40 N z from -0.53 to -1.68
def volscan40Nc():
    sheh3.open()
    umv( s9hg, 0.025, s9vg, 0.025)
    zstep = 0.025
    zpos = np.arange( -1.68, -0.53, zstep)
    myumv(shtz, zpos[44])
    halflayer_360(ystep=50.,skip=11)
    for k in range(45,len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("40NDT1_z%03d"%(k))
        else:
            if k > 12:
                newdataset("40NDT1_z%03d"%(k))
        halflayer_360(ystep=50.)
        

# 40 N z from -0.53 to -1.68
def volscan60N():
    sheh3.open()
    umv( s9hg, 0.025, s9vg, 0.025)
    zstep = 0.025
    zpos = np.arange( -1.65, -0.53, zstep)
    for k in range(0,len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("60NDT0_z%03d"%(k))
        else:
            newdataset("60NDT0_z%03d"%(k))
        halflayer_360(ystep=50.)

def fine_pct_for_60N():
#    user.goto_PCT()
    umv(u22, 8.3159)
    user.cpm18_goto(6.38)
    # one slightly off axis
#    umv(shtz, 0.5*(-1.65-.53))
    umv(dty, 100)
    ACTIVE_MG.disable("basler*")
    ACTIVE_MG.disable("eiger*")
    ACTIVE_MG.enable("basler_eh32:image")
    
    user.tomo_by_ascan( 0, 360, 14400, 0.01, ystep=2000,
                        rot='rot', ymotor='dty', 
                        ndark=21, nflat=21) 
    ### for extinctions ...
    
# 90 N z from -0.53 to -1.56
def volscan90N():
    sheh3.open()
    umv( s9hg, 0.025, s9vg, 0.025)
    zstep = 0.025
    zpos = np.arange( -1.56, -0.53, zstep)
    for k in range(0,len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("90NDT0_z%03d"%(k))
        else:
            newdataset("90NDT0_z%03d"%(k))
        halflayer_360(ystep=50.)
    
# 90 N z from -0.53 to -1.56
def volscan90Nc1():
    sheh3.open()
    umv( s9hg, 0.025, s9vg, 0.025)
    zstep = 0.025
    zpos = np.arange( -1.56, -0.53, zstep)
    for k in range(11,len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("90NDT0_z%03d"%(k))
        else:
            if k > 11:
                newdataset("90NDT0_z%03d"%(k))
        halflayer_360(ystep=50.)

def sand3_volscan3N():
    sheh3.open()
    umv( s9hg, 0.025, s9vg, 0.025)
    zstep = 0.025
    zpos = np.arange( -1.68, -0.579, zstep)
    for k in range(0, len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("3NDT0_z%03d"%(k))
        else:
            newdataset("3NDT0_z%03d"%(k))
        halflayer_360(ystep=50.)
        
def sand3_volscan3Ncontd():
    #01h40 skip to z 18 y 12 (24)
    sheh3.open()
    umv( s9hg, 0.025, s9vg, 0.025)
    zstep = 0.025
    zpos = np.arange( -1.68, -0.579, zstep)

    myumv(shtz, zpos[18])
    halflayer_360(ystep=50, skip=12)
    
    for k in range(19, len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("3NDT0_z%03d"%(k))
        else:
            newdataset("3NDT0_z%03d"%(k))
        halflayer_360(ystep=50.)


def fine3dstep():
    for a0 in np.arange(0,351,10):
        fscan(rot, a0, 0.0025, 10/0.0025, 0.0044, scan_mode='CAMERA')

eiger.camera.photon_energy = 43468.9

def sand3_volscan20N():
    sheh3.open()
    umv( s9hg, 0.025, s9vg, 0.025)
    zstep = 0.025
    zpos = np.arange( -1.68, -0.579, zstep)
    for k in range(0, len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("20NDT0_z%03d"%(k))
        else:
            newdataset("20NDT0_z%03d"%(k))
        halflayer_360(ystep=50.)


def sand3_volscan40N():
    sheh3.open()
    umv( s9hg, 0.025, s9vg, 0.025)
    zstep = 0.025
    zpos = np.arange( -1.68, -0.55, zstep)

    halflayer_360(ystep=50., skip=12)
    
    for k in range(35, len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("40NDT0_z%03d"%(k))
        else:
            newdataset("40NDT0_z%03d"%(k))
        halflayer_360(ystep=50.)


    umv(cpm18, 6.48)
    attin()
    time.sleep(60)
    umv(shtz, (-1.68-0.55)/2)
    umv(dty, 0)
    umv(s9vg, 1.3, s9hg, 1.4 )
    newdataset("40N_diffFine")
    fine3dstep()
    sheh3.close()
    
    goto_PCT()
    sheh3.open()
    newdataset("40N_PCTFine")
    umv(dty, 100)
    fine_pct_for_60N()
        
def sand3_volscan60N():
    sheh3.open()
    umv( s9hg, 0.025, s9vg, 0.025)
    zstep = 0.025
    zpos = np.arange( -1.65, -0.55, zstep)
    
    for k in range(0, len(zpos)):
        myumv(shtz, zpos[k])
        if SIMULATE:
            print("60NDT0_z%03d"%(k))
        else:
            newdataset("60NDT0_z%03d"%(k))
        halflayer_360(ystep=50.)

    umv(cpm18, 6.48)
    attin()
    time.sleep(60)
    umv(shtz, (-1.68-0.55)/2)
    umv(dty, 0)
    umv(s9vg, 1.3, s9hg, 1.4 )
    newdataset("60N_diffFine")
    fine3dstep()
    sheh3.close()
    
    goto_PCT()
    sheh3.open()
    newdataset("60N_PCTFine")
    umv(dty, 100)
    fine_pct_for_60N()
        
