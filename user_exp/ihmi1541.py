import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime

atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')

prefix_name = ''
print(prefix_name)

def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 6.5
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.05
    dct_pars['refon'] = dct_pars['num_proj']
    dct_pars['ref_step'] = -2
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.55
    dct_pars['slit_vg'] = 0.1
    dct_pars['dist'] = 4.5
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 0.44482
    dct_pars['shift_step_size'] = 0.08
    dct_pars['nof_shifts'] = 9
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars

def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = 6.5
    pct_pars['step_size'] = 0.2
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.05
    pct_pars['refon'] = pct_pars['num_proj']
    pct_pars['ref_step'] = -2
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 41
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = samtz
    pct_pars['samtz_cen'] = 0.44482
    pct_pars['slit_hg'] = 2
    pct_pars['slit_vg'] = 2
    pct_pars['nof_shifts'] = 1
    pct_pars['shift_step_size'] = 1
    pct_pars['dist'] = 100
    pct_pars['abs_dist'] = 10
    pct_pars['scan_type'] = 'fscan'
    return pct_pars
    
def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 6.5
    ff_pars['step_size'] = 0.25
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.55
    ff_pars['slit_vg'] = 0.1
    ff_pars['mode'] = 'ZIGZAG' # FORWARD
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = 0.44482
    ff_pars['shift_step_size'] = 0.08
    ff_pars['nof_shifts'] = 9
    ff_pars['cpm18_detune'] = 0
    ff_pars['ceo2_samtx'] = 0.1850
    ff_pars['ceo2_samty'] = -0.7652
    ff_pars['ceo2_samtz'] = 4.6154
    ff_pars['ceo2_difftz'] = -21.7648
    return ff_pars

    
def run_dct_ff(prefix_name = prefix_name):
    # assum you start with dct condition
    # start DCT measurements
    switch_to_dct()
    dct_pars = define_dct_pars()
    print(dct_pars)
    umv(atty,-10,attrz,0)
    dct_zseries(dct_pars, f'dct_{prefix_name}')
    
    # switch to 3DXRD measurements
    switch_to_ff()
    ff_pars = define_ff_pars()
    umv(atty,0,attrz,-8.5)
    ff_zseries(ff_pars, f'ff_{prefix_name}')
    
    # switch back to DCT camera for alignment (CoR = 1044 pixels)
    fsh.close()
    umv(atty,-10,attrz,0)
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    ff_out()
    umv(d3ty, 0, d3tz, 0)
    umv(nfdtx,100)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3:image')
    fsh.open()
    umvct(s7vg, 1.2, s7hg, 1.2, s8vg,1.3, s8hg, 1.3)
    sheh3.close()
    print('Done successfully')


def switch_to_dct():
    fsh.close()
    tfoh1.set(16,'Be')
    #tfoh1.set(64,'Al')
    marana_in()
    umv(atty,-10,attrz,0)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3:image')
    ACTIVE_MG.enable('marana3:roi*')
    fsh.open()
    umvct(s7vg, 0.1, s7hg, 0.55, s8vg,0.15, s8hg, 0.6)

def switch_to_pct():
    fsh.close()
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    ff_out()
    umv(atty,-10,attrz,0)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3:image')
    ACTIVE_MG.enable('marana3:roi*')
    fsh.open
    umvct(nfdtx,100)
    umvct(d3ty,0, d3tz, 0)
    umvct(s7vg, 2, s7hg, 2, s8vg,2.5, s8hg, 2.5)
    
    
def switch_to_ff():
    fsh.close()
    tfoh1.set(16,'Be')
    #tfoh1.set(64,'Al')
    ff_in()
    umv(atty,0,attrz,-10)
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.enable('frelon3*')
    fsh.open()
    umvct(s7vg, 0.1, s7hg, 0.55, s8vg,0.15, s8hg, 0.6)
   
def ff_out():
    umv(ffdtx1, 530)
    umv(ffdtz1, 400)
    
def marana_in():
    ff_out()
    umv(d3ty, 0, d3tz, 0)
    umv(nfdtx,4.5)

def marana_out():
    umv(nfdtx,100)
    umv(d3ty,130)
    
def ff_in():
    marana_out()
    umv(ffdtz1, 0)
    umv(ffdtx1, 130)

def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'], s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umv(s8vg, dct_pars['slit_vg'] + 0.05, s8hg, dct_pars['slit_hg'] + 0.05)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])
    
def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')

def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.05, s8hg, pars['slit_hg'] + 0.05)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)
    
def ff_zseries(ff_pars, datasetname = 'ff'):
    #user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    #umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    umv(diffrz, ff_pars['start_pos'], s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.05, s8hg, ff_pars['slit_hg'] + 0.05)
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    #user.cpm18_goto(cpm18_tuned)
    #umv(u22, u22_tuned)
    print('ff_zseries succeed')
    return('Succeed')

def ff_ceo2(ff_pars, prefix_name = prefix_name):
    umvct(s7vg, 0.1, s7hg, 0.1 )
    umvct(s8vg, 0.15, s8hg, 0.15 )
    
    samtx0 = samtx.position
    samty0 = samty.position
    samtz0 = samtz.position
    difftz0 = difftz.position
    
    fsh.close()
    umv(difftz, ff_pars['ceo2_difftz'])
    umv(samtx,ff_pars['ceo2_samtx'],samty,ff_pars['ceo2_samty'],samtz,ff_pars['ceo2_samtz'])
    umv(diffrz, 0)
    dset_name = 'ff_' + prefix_name + 'CeO2'
    newdataset(dset_name)
    fsh.open()
    loopscan( 50, ff_pars['exp_time']*12.5)
    umv(diffrz, 180)
    loopscan( 50, ff_pars['exp_time']*12.5)
    
    # move back
    umv(samtx,samtx0,samty,samty0,samtz,samtz0)
    umv(difftz,difftz0)   
    
def pct_by_fscan(scanname, pars, scan_back = False):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.05, s8hg, pars['slit_hg'] + 0.05)
    if scan_back == False:
        fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
    else:
        fscan(diffrz, pars['start_pos'] + 360, -1 * pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
    print('Succeed !')
    
def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

    
