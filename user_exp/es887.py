
import numpy as np
import time
user_script_load("align_cor.py") 


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan(*a,**k):
    print(a,k)
    try:
        fscan(*a,**k)
    except: # pretty hopeless but why not ? 
        fscan(*a,**k)

def myumv(*a,**k):
    print(a,k)
    umv(*a,**k)
        


def ref_sample(pz0, relheight):
    umv(dty, 0, pz, pz0, rot, 0)
   # ACTIVE_MG.enable("mca")
    ACTIVE_MG.enable("eiger:image")
    newedge = edge_fit()
    umvr(pz,relheight)
    user.align_cor_dty(5, 80, 0.2, ctr='roi1_sum')
    dty.position = 0
    #ACTIVE_MG.enable("eiger:image")
    #ACTIVE_MG.disable("eiger:image")
    plotinit("eiger:roi_counters:roi1_max")
    plotselect("eiger:roi_counters:roi1_max")
    return newedge


def edge_fit(ctr='roi1_sum'):
    plotinit("eiger:roi_counters:roi1_sum")
    plotselect("eiger:roi_counters:roi1_sum")
    lastscan = dscan(pz, -1.5, 1.5, 150, .1)
    sleep(1)
    A = lastscan.get_data( )
    y = A[ctr]
    x = A[pz]
    edge = x[np.argmin(np.abs(y-( ( np.mean(y[130:149]) + np.mean(y[1:20]) )/2) ))]
    print(edge)
    umv(pz,edge)
    return edge
    



def layer_smaller_360_test():
    y0 = 0.0  # centre of rotation
    width = 3
    step  = 0.3
    ypos = np.arange(-step, width+step/10, step)
    astep = 0.1
    anrange = 360.
    speed = 7.5
    eiger.camera.photon_energy=43468.9
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="ON"
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"
    
def layer_360():
    y0 = 0.0  # centre of rotation
    width = 3.0
    step  = 0.24
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.02
    anrange = 360.
    speed = 6
    eiger.camera.photon_energy=43468.9
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="ON"
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"
    

def volscan():
    pz0 = 34.52
    pz_rel = [1.27, 3.34, 5.31, 8.27, 10.55, 12.87, 16.45, 18.76, 21.82, 25.86, 30.9, 60]
    k = 0
    for zrel in pz_rel:
        if ( k % 3 ) == 0:
            pz0 = ref_sample( pz0, zrel )
        if k == 11:
            pz0 = ref_sample( pz0, zrel )
        k +=1          
        umv( pz, pz0 + zrel )
        newdataset( "360_Z%03d"%( k )  )
        layer_smaller_360_test()

        
def volscan_night():
    pz0 = 39.5
    pz_rel = [3.34, 5.31, 8.27, 10.55, 12.87, 16.45, 18.76, 21.82, 25.86, 30.9] #1.27 already done
    k = 1
    for zrel in pz_rel:
        k +=1
        newdataset( "360ref_Z%03d"%( k )  )
        pz0 = ref_sample( pz0, zrel )
        umv( pz, pz0 + zrel )
        newdataset( "360_Z%03d"%( k )  )
        layer_360()



def pct_layers():
    pz0 = 40
    pz_vals = np.arange(0,40,3)
    frames=1440
    extime=0.3
    for zrel in pz_vals:
        umv(pz, pz0 + zrel)
        newdataset('z%03d'%int(zrel))
        pause_for_refill(14*60)
        user.tomo_by_ascan(0,360, frames, extime, ystep=60, rot='rot', ymotor='dty', sleep_time=0.01, ndark=21, nflat=21)


def volscan_night_2():
    pz0 = 39.5
    pz_rel = [1.27, 3.34, 5.31, 8.27, 10.55, 12.87, 16.45, 18.76, 21.82, 25.86, 30.9] 
    k = 0
    for zrel in pz_rel:
        k +=1
        newdataset( "360ref_Z%03d"%( k )  )
        pz0 = ref_sample( pz0, zrel )
        umv( pz, pz0 + zrel )
        newdataset( "360_Z%03d"%( k )  )
        layer_360()
    newdataset('W_ref')
    umv( pz, 98)
    umv(dty,0)
    user.align_cor_dty(5, 80, 0.2, ctr='roi1_sum')
    fscan(rot, 0, 180,1, 180/25)
    fscan(rot, 180, -5, 180/5, 0.2)
    try:
        tfoh1.set(0, 'Be')
        fscan(rot, 0, 180,1, 180/25)
        fscan(rot, 180, -5, 180/5, 0.2)
    except:
        fscan(rot, 0, 180,1, 180/25)
        fscan(rot, 180, -5, 180/5, 0.2)
       
    


def returnto6v8h():
    umv(shnee,87.08, axmo,62.96)
    umv(vlry,0.03, vlrz,-0.21)
    umv(vly,-0.27825, vlz,0.5675)
    umv(hlry,-0.04, hlrz,-0.27825)
    umv(hly,0.482, hlz,0.13)
    umv(dty,0, rot,0, pz, 45)
