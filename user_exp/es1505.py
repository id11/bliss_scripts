import os, sys
import numpy as np


import time
from bliss.common import cleanup

tfoh1 = config.get('tfoh1')

def Si_makesperanto():
    print("Centering the scan on zero")
    
    step = -0.25
    npts = 1440
    wvln = 0.2847
    xbeam = 1052.9
    ybeam = 1113.1
    distance = 163.284
    omegastart = 0
    
    # The scan must be centered around zero
    center = step * npts / 2 + omegastart
    omegastart -= center

    datapath = '/data/visitor/es1505/id11/20240926/RAW_DATA/Si_cube/Si_cube_rot'
    run=1
    scnb = 1
    cmdpath  = os.path.join(datapath,'scan%04d'%(scnb))
    savepath = cmdpath.replace("/RAW_DATA/","/PROCESSED_DATA/")

    cmd0 = "cd %s" %(cmdpath)
    cmd2 = "eiger2crysalis eiger*h5 -w %f --beam %f %f --omega=-%f-index*%f --distance=%f --flip-lr -o %s/frame_%d_{index}.esperanto > /dev/null " %(
            wvln, xbeam, ybeam, omegastart, step, distance, savepath, run) #changed %f in %d for run
    #os.system('ls ' + os.path.split(datapath)[0])
    scriptname = os.path.join( datapath, 'make_esperanto_%04d.sh'%(scnb) )
    print("Writing",scriptname)
    with open(scriptname, 'w') as outf:
        outf.write("#!/bin/sh\n")
        outf.write("source /users/blissadm/conda/miniconda/etc/profile.d/conda.sh\n")
        outf.write("conda activate eiger2crysalis\n")
        outf.write(cmd0 + "\n")
        outf.write(cmd2 + "\n")
        outf.write("cp /data/id11/nanoscope/Eiger/frame_1_.set %s/frame.set\n"%(savepath)) # copy mask
    cmd = 'ssh -fn opid11@lid11eiger2lima "bash -l %s" &'%(scriptname)
    #print(cmd)
    os.system("%s "%(cmd))
    
def restartflint():
   import os, time
   os.system("ps aux | grep flint | grep nscope | awk '{print $2}' | xargs kill -9")
   time.sleep(5)
   flint()


def switch_to_pct():
    umv(attrz, 0, atty,-10)
    fsh.session='TDXRD'
    tfoh1.set(0,'Be')
    centeredroi(marana3,1200,1200)
    umv(nfdtx, 100)
    umv(d3ty,0)
    slit(2.5, 2.5)


def switch_to_eiger():
    umv(s9vg, 0.05, s9hg, 0.05)
    fsh.session='NSCOPE'
    moveto_eiger()
    #umv(aly,0)
    umv(vly, 0.002, hlz, -0.30700)
    if piny.position > 1.0:
        switch_pinhole()
    tfoh1.set(0, 'Be')
    umvct(atty,0,attrz,-20)
    newdataset('align')
    

def switch_to_basler():
    moveto_basler()
    fsh.session='NSCOPE'
    #umv(aly,-8)
    umv(vly, 0.002-1, hlz, -0.30700+1)
    if piny.position < 1.0:
        switch_pinhole()
    sheh3.open()
    umv(atty,-10,attrz,0)
    tfoh1.set(0, 'Be')
    umvct(s9vg,1.5, s9hg, 1.5)
    plotimagecenter()



def uda5_initial_pos():
    umv(ntx, -0.97521, nty, 0.30525, ntz, 12.16)
    umv(px, 59.948, py, 22.424, pz, 50.134)
    umv(dty,0)


def scan_inclusions():
    pos_list = [[-0.9060, 0.3589, 12.4781],
                [-1.0361, 0.3931, 12.0921],
                [-1.10113, 0.0373, 13.3279],
                [-1.2891, 0.5405, 13.2888],
                [-1.2498,0.5305,13.2544],
                [-1.4354,0.5635,13.2423],
                [-1.2397,0.6303,13.1019],
                [-1.4631,0.4998,13.0861],
                [-1.4512,0.5518,13.0628]]
    for i, pos in enumerate(pos_list):
        dset_name = f'sx_inclusion{i+1}'
        print(dset_name, pos)
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        if i > 0:
            newdataset(dset_name)
            sxscannew(step_size=-0.5,count_time=0.5, start = 90, end=-90, xc=1052.9, yc = 1113.1, d=163.284, wvln = 0.2847)
        
        
        #dset_name = f'sxewoks_inclusion{i+1}'
        #print(dset_name, pos)
        #newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        print(f'Done for inclusion no. {i+1}')




def uda5_rep_scan_inclusions():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-1.0134, 0.5168, 12.4771],
                [-1.1061, 0.4138, 12.3720],
                [-1.1432, 0.5708, 12.1016],
                [-1.1363, 0.5682, 12.0902],
                [-1.1545,0.2070,13.33],
                [-1.3690,0.7407,13.2947],
                [-1.3281,0.7242,13.2575],
                [-1.5096,0.7834,13.2491],
                [-1.2924,0.8212,13.1198],
                [-1.5448, 0.7259, 13.0947],
                [-1.5264, 0.7738, 13.0696]]

    for i, pos in enumerate(pos_list):
        #dset_name = f'sx_inclusion{i+1}'
        #print(dset_name, pos)
        #umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #newdataset(dset_name)
        #sxscannew(step_size=-0.5,count_time=0.5, start = 90, end=-90, xc=1052.9, yc = 1113.1, d=163.284, wvln = 0.2847)
        
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=60, end=-60)
        print(f'Done for inclusion no. {i+1}')





def lgm15_inclusions():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-1.1722, -0.0215, 13.1283],
                [-1.1926, -0.0042, 13.0270],
                [-1.1423, 0.0998, 12.9991],
                [-0.8288, 0.1223, 12.8263],
                [-0.9577, 0.1304, 12.6719],
                [-1.0045, 0.0419, 12.6524],
                [-1.2008, -0.0298, 12.6943],
                [-1.0465, 0.3200, 12.5808],
                [-0.9205, 0.3130, 12.3678]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=60, end=-60)
        print(f'Done for inclusion no. {i+1}')
    
    sheh3.close()
    


    

def lgm15_inclusions():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-1.1724, -0.0039, 13.1338],
                [-1.1813, -0.0137, 13.1338],
                [-1.1930, -0.0230, 13.1338],
                [-1.1846, -0.0308, 13.1422]]
                
                
    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=60, end=-60)
        print(f'Done for inclusion no. {i+1}')
    
    sheh3.close()
    
def lgm12_inclusions():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-1.1389, 0.1326, 12.7132],
                [-1.4729, -0.1531, 12.9440],
                [-1.8745, -0.0969, 12.9589],
                [-1.1622, -0.1357, 13.0919],
                [-1.2602, -0.1325, 13.1012],
                [-1.4491, -0.1264, 13.4723],
                [-1.2736, -0.1184, 13.4723]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=60, end=-60)
        print(f'Done for inclusion no. {i+1}')
    
    sheh3.close()
    
def P_dia_2_inclusions():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-0.5773, 0.2949, 12.2712],
                [-0.4966, 0.3598, 12.3363],
                [-0.5099, 0.4297, 12.4070],
                [-0.4339, 0.0952, 12.4349],
                [-0.5755, 0.1698, 12.4265],
                [-0.5016, 0.4563, 12.4600],
                [-0.5226, 0.1932, 12.4869],
                [-0.5166, 0.2460, 12.5120],
                [-0.5087, 0.3557, 12.5502],
                [-0.4718, 0.1351, 12.5623],
                [-0.6456, 0.1987, 12.7092]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=60, end=-60)
        print(f'Done for inclusion no. {i+1}')
    
    sheh3.close()
    
    
def olisc():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-1.5565, 0.47941, 12.6963]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=60, end=-60)
        print(f'Done for inclusion no. {i+1}')
    
    sheh3.close()
    
    
    
def nancy():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-1.38049, 0.47690, 12.7182995]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=180, end=-180)
        print(f'Done for inclusion no. {i+1}')
    
    sheh3.close()
    
    
    
def GRC2():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-1.3605, -0.3306, 13.0088],
                [-1.3952, -0.3410, 13.0135],
                [-1.2181, -0.3312, 13.0283],
                [-1.4069, -0.2412, 13.0507],
                [-1.2366, -0.1996, 13.0525],
                [-1.4479, -0.3087, 13.0832],
                [-1.2422, -0.1626, 13.1176],
                [-1.1461, -0.2005, 13.1409],
                [-1.3255, -0.1990, 13.1520],
                [-1.4793, -0.2696, 13.1706],
                [-1.3809, -0.1654, 13.1883],
                [-1.2309, -0.2362, 13.2683],
                [-1.4034, -0.1727, 13.3064]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=60, end=-60)
        print(f'Done for inclusion no. {i+1}')
    
    sheh3.close()
    
    
    
def GRC2():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-1.3605, -0.3306, 13.0088],
                [-1.3952, -0.3410, 13.0135],
                [-1.2181, -0.3312, 13.0283],
                [-1.4069, -0.2412, 13.0507],
                [-1.2366, -0.1996, 13.0525],
                [-1.4479, -0.3087, 13.0832],
                [-1.2422, -0.1626, 13.1176],
                [-1.1461, -0.2005, 13.1409],
                [-1.3255, -0.1990, 13.1520],
                [-1.4793, -0.2696, 13.1706],
                [-1.3809, -0.1654, 13.1883],
                [-1.2309, -0.2362, 13.2683],
                [-1.4034, -0.1727, 13.3064]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1.5, start=90, end=-90)
        print(f'Done for inclusion no. {i+1}')
    
    sheh3.close()
    
    
    
def nancy2():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-1.39219, 0.18741, 11.8879]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=180, end=-180)
        print(f'Done for inclusion no. {i+1}')
    
    sheh3.close()
    
def GRC4():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-0.9893, -0.1417, 12.5865],
                [-1.1910, -0.0258, 12.6116],
                [-1.1946,  0.0449, 12.6656],
                [-1.1316,  0.1292, 12.6758],
                [-1.0168,  0.0318, 12.6926],
                [-1.2552,  0.0985, 12.7418],
                [-1.1857,  0.0880, 12.8488],
                [-1.1854,  0.0250, 12.8944],
                [-1.0940,  0.0738, 12.4545],
                [-1.0861,  0.0177, 12.4508]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1.5, start=90, end=-90)
        print(f'Done for inclusion no. {i+1}')
    
    sheh3.close()
    
def GRC8():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-0.6778, -0.1511, 11.6125],
                [-0.8023, -0.1334, 11.8282],
                [-0.7503, -0.0705, 11.8682],
                [-0.7847, -0.1059, 11.8896]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_nb_inclusion{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1.5, start=60, end=-60)
        print(f'Done for inclusion no. {i+1}')
    
    sheh3.close()    
    
    


def FeAu():
    # this function cannot run from this macro, please copy, paste to run in the bliss directly
    pos_list = [[-1.3199, -0.3412, 11.5542],
                [-1.3268, -0.3583, 11.5542],
                [-1.4071, -0.3692, 11.5542],   # slice no.246
                [-1.4031, -0.3857, 11.8323],
                [-1.3956, -0.2847, 11.8323],   # slice no.545
                [-1.4162, -0.4366, 12.1029],
                [-1.4406, -0.4319, 12.1029],
                [-1.4138, -0.3973, 12.1029],
                [-1.3710, -0.3040, 12.1029]]   # slice no.836

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        if i==0:
            dset_name = f'sxewoks_redo_p{i+1}'
        else:
            dset_name = f'sxewoks_p{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=60, end=-60)
        print(f'Done for point no. {i+1}')
    
    
    umv(ntx, -1.43072, nty, -0.38998, ntz, 11.774599)
    #umv(px, 83.03, py, 6.758, pz, 50)
    pzs = [11.5542, 11.8323, 12.1029]
    print(pzs)
    for i, pos in enumerate(pzs):
        umv(ntz, pos)
        user.half1(ymax=220, datasetname=f'_slice_z{i+1}', ystep=0.5, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    print('Done')
    sheh3.close()
    umv(edoor,0)
    
    
    

    
    
    
    
    
    
        

    
    
