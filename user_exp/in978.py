import time
user_script_load("optics.py")

# def check_cpm18(pos=6.4158):
def check_cpm18(pos=6.426):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03:
        user.cpm18_goto(pos)



def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()


def myfscan( *args, **kwds):
    for i in range(3):
        try:
            fscan(*args, **kwds)
        except Exception as e:
            elog_print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break

import numpy as np        
        
print("Load this script at least twice please!")
def my2d(y0, ye=5.01, ys=0.15):
    fscan.pars.latency_time=0
    for i, ypos in enumerate(np.arange(y0,ye,ys)):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            myfscan(rot, -1, 0.125, 2896, 0.005, scan_mode='CAMERA' )
        else:
            myfscan(rot, 361, -0.125, 2896, 0.005, scan_mode='CAMERA' )
    

def sample1A():
    newsample("sample2A")
    newdataset("slice1")
    my2d( -24.9 )
    
def sample4A():
    newsample("sample4A")
    newdataset("slice1")
    my2d( -27. )
    umv(shtz, -0.0755)
    newdataset("slice2")
    my2d( -27. )

def sample6F():
    newsample("sample6F")
    newdataset("slice1")
    my2d( -35. )
    umv(shtz, -0.005)
    newdataset("slice2")
    my2d( -35. )

    
def atend():
    umv(fly, 100, rot, 0, dty, 0)

def polyp():
    umv(fly, 50)
    newdataset('slice1')
    my2d( -10.05, 10.051 )
    
def polycyc():
    umv(fly, 50)
    newdataset('slice1R2')
    my2d( -20.1, 20.101 )
    
def gradientp():
    umv(fly, 50)
    newdataset('slice1')
    my2d( -12.0, 12.01 )

def gradientp2():
    umv(fly, 50)
    newdataset('slice3')
    my2d( -20.1, 20.101 )

def gradientc():
    umv(fly, 50)
    newdataset('slice1')
    my2d( -20.1, 20.101 )
   
def singlep():
    umv(fly, 50)
    newdataset('slice2')
    my2d( -20.1, 20.101 )

def thisIsGradientC():
    umv(fly, 50)
    newdataset('slice1')
    my2d( -10.05, 10.051 )

def singlec():
    umv(fly, 50)
    newdataset('slice2')
    my2d( -15.0, 15.001 )

def bigGradientC():
    umv(fly, 50)
    newdataset('slice1')
    my2d( -50.625, 50.626 )

