import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime

atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')

def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = -2.8
    dct_pars['step_size'] = 0.05
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.05
    dct_pars['refon'] = dct_pars['num_proj']
    dct_pars['ref_step'] = -4
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.65
    dct_pars['slit_vg'] = 0.15
    dct_pars['dist'] = 8
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 1.0724
    dct_pars['shift_step_size'] = 0.1
    dct_pars['nof_shifts'] = 5
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars
    
def define_dct_pars_1N():
    dct_pars={}
    dct_pars['start_pos'] = -2.8
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.05
    dct_pars['refon'] = dct_pars['num_proj']
    dct_pars['ref_step'] = -4
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.65
    dct_pars['slit_vg'] = 0.4
    dct_pars['dist'] = 8
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 1.0724
    dct_pars['shift_step_size'] = 0.35
    dct_pars['nof_shifts'] = 3
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars
    
# ref_samtz = 6.52, -2.9 #8      => 60 N 
# ref_samtz = 6.64, -3.8 #1
# ref_samtz = 0.8294,-0.4 #7     topo-tomo
# ref_samtz = 0.8224,-0.5 # 3    => 58 N
# ref_samtz = 0.8474,-7 # 2    => until failre, but load limit to 70 N
# ref_samtz = 1.0724,-2.8 # 6    => planned pct until 69.2, but fail after acquisition with 55 N


# ffdtx1=200
def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = -2.8
    ff_pars['step_size'] = 0.8
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.65
    ff_pars['slit_vg'] = 0.15
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = 1.0724
    ff_pars['shift_step_size'] = 0.1
    ff_pars['nof_shifts'] = 5
    return ff_pars

def define_ff_pars_1N():
    ff_pars={}
    ff_pars['start_pos'] = -2.8
    ff_pars['step_size'] = 0.8
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.65
    ff_pars['slit_vg'] = 0.4
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = 1.0724
    ff_pars['shift_step_size'] = 0.35
    ff_pars['nof_shifts'] = 3
    return ff_pars
    
def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = -2.8
    pct_pars['step_size'] = 0.24
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.1
    pct_pars['refon'] = pct_pars['num_proj']
    pct_pars['ref_step'] = -4
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 100
    pct_pars['ref_mot'] = samy
 #   pct_pars['zmot'] = samtz
    pct_pars['slit_hg'] = 3.5
    pct_pars['slit_vg'] = 3.5
    pct_pars['dist'] = 30
    pct_pars['scan_type'] = 'fscan'
    return pct_pars
    
        
def ff_zseries(ff_pars, datasetname = 'ff'):
    #user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    #umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    umv(diffrz, ff_pars['start_pos'], s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.2, s8hg, ff_pars['slit_hg'] + 0.2)
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    #user.cpm18_goto(cpm18_tuned)
    #umv(u22, u22_tuned)
    print('ff_zseries succeed')
    return('Succeed')
    
def load_series_pct():
    # loads = [0.5,]
    loads = [40, 42, 44, 46, 48, 50, 52, 54, 55, 56, 57, 58, 59, 60, 60.6, 61.2, 61.8, 62.4, 63, 63.6, 64.2, 64.8, 65.4, 66, 66.4, 66.8, 67.2, 67.6, 68, 68.4, 68.8, 69.2] # can't go beyong 70 N
    #loads = [40, 44, 48, 52, 56, 58, 60, 62, 64, 66, 68, 69]
    switch_to_pct()
    pct_pars = define_pct_pars()
    for load in loads:
        load_constant(load)
        ## PCT
        scan_name = 'pct_%dN_' % load
        tomo_by_fscan_dict(scan_name, pct_pars)   
    print('Done successfully !!!')
        
def load_series_pct_dct_ff():
    loads = [0.5,]
    # loads = [44, 48, 52, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 81, 82, 83, 84] # can't go beyong 70 N
    # loads = [40, 44, 48, 52, 56, 58, 60, 62, 64, 66, 68, 69]
    for load in loads:
#        load_and_unload(load)
        load_constant(load)
        
        pct_pars = define_pct_pars()
        ## PCT
        switch_to_pct()       
        scan_name = 'pct_%dN_' % load
        tomo_by_fscan_dict(scan_name, pct_pars)       
        
        if load < 1 or load == 56 or load == 58: 
            # dct scan
            ds_name_dct = 'load_' + str(load) + 'N_dct'
            switch_to_dct()
            if load <= 2:
                dct_pars = define_dct_pars_1N()
            else:
                dct_pars = define_dct_pars()
            print(dct_pars)
            dct_zseries(dct_pars,ds_name_dct)
        
        # ff scan
        ds_name_ff = 'load_' + str(load) + 'N_ff'
        switch_to_ff()
        if load <= 1:
            ff_pars = define_ff_pars_1N()
        else:
            ff_pars = define_ff_pars()
        print(ff_pars)
        ff_zseries(ff_pars,ds_name_ff)
    switch_to_pct()
    print('Done successfully !!!')


def ff_test():
    load = [0,]
    ds_name_ff = 'load_' + str(load) + 'N_ff'
    switch_to_ff()
    if load[0] <= 1:
        ff_pars = define_ff_pars_1N()
    else:
        ff_pars = define_ff_pars()
    print(ff_pars)
    ff_zseries(ff_pars,ds_name_ff)
    switch_to_pct()
    print('Done successfully !!!')


def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def load_ramp_by_target(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    write_to_file = to_file if to_file is not None else sys.stdout
    #marana_large_beam(pct_pars)
    current_load = stress_adc.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return
    
    
def load_and_unload(f_target):
    # f0 = stress_adc.get_value()
    f0 = 0.8
    if f_target > 1:
        load_ramp_by_target(f_target, time_step=0.5,to_file=None)
        load_ramp_by_target(f0, time_step=0.5,to_file=None)
    
def load_constant(f_target):
    if f_target > 1:
        load_ramp_by_target(f_target, time_step=0.5,to_file=None)
        
def prepare_exp_load(f_target=0):
    load_and_unload(f_target)
    marana.image.roi = [0, 0, 2048, 2048]
    umvct(s7hg,0.65, s7vg, 1.8, s8hg, 0.8, s8vg, 2)
    umvct(diffrz, -0.4, d3tz, -0.045)
    umvct(nfdtx,8)
    umvct(d3_bsty,-2500)


def switch_to_ff():
    fsh.close()
    atty = config.get('atty')
    attrz = config.get('attrz')
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    umv(nfdtx,30)
    umv(d3ty,100)
    umv(atty,0)
    umv(attrz,-5)
    
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.enable('frelon3*')
    fsh.open()
    
def switch_to_dct():
    fsh.close()
    atty = config.get('atty')
    attrz = config.get('attrz')
    tfoh1.set(18,'Be')
    tfoh1.set(0,'Al')
    for i in range (3):
        umv(tfz, -0.325, tfy, 14.71)
    umv(d3ty,0)
    umv(nfdtx,8)
    umv(atty,-10)
    umv(attrz,0)
    umv(d3_bsty,100)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana*')
    marana.image.roi=[0,0,2048,2048]
    fsh.open()


def switch_to_pct():
    tfoh1.set(96,'Al')
    tfoh1.set(18,'Be')
    for i in range (3):
        umv(tfz, -0.325, tfy, 14.71)
    fsh.close()
    umv(d3_bsty,-2500)
    umv(d3ty,0)
    umv(atty,-10)
    umv(attrz,0) 
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana*')
    marana.image.roi=[512,0,1024,2048]
    print("ready to collect marana pct data") 
    fsh.open()
    sct(0.05)
    umvct(s7vg, 2.5, s7hg, 2.5, nfdtx, 30)
    umvct(s8vg, 3, s8hg, 3)

def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])   
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')
    
def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.2, s8hg, pars['slit_hg'] + 0.2)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)


'''
def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.2, s8hg, pars['slit_hg'] + 0.2)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])
'''





