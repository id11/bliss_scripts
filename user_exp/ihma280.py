
import numpy as np
import time
def check_cpm18(pos=6.3483):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03:
        user.cpm18_goto(pos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()

def myfscan2d(*a,**k):
    try:
        print(a,k)
        fscan2d(*a,**k)
    except:
        print("Scan failed, sleep then retry")
        time.sleep(60)
        fscan2d(*a,**k)        
        

def layer1():
    # quick overview (check scan range)
    fscan2d( dty, -64.5, 1.5, 87, rot, -90.5, 0.125, 1448, 0.005 )
    


def full1(ymax, datasetname):    
    newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ystep = 0.8
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = 0
    while i < len(ypos):
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, -180.5,  0.125, 1448, 0.005 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot,    0.5, -0.125, 1448, 0.005 )
        i += 1

def layers(ymax):
    zvals=np.arange(43,80,0.15)
    for z in zvals:
        umv(pz,z)
        full1( ymax, 'top_hr_150step_z'+str(z) )
    # do newsample before
    # call as user.layers( [ 54. 50, 60, 80],  38.0 )
    # 


def fmesh(zmot, zstart, zstop, znp, ymot, ystart, ystop, ynp, ctim, ctr='roi1_avg'): 
    ypos = np.linspace( ystart, ystop, ynp )
    ystep = ypos[1] - ypos[0]    
    zpos = np.linspace( zstart, zstop, znp )
    zstep = zpos[1] - zpos[0]
    
    p = flint().get_plot("image", name='fmesh')
    p.xlabel = ymot.name
    p.ylabel = zmot.name
    p.side_histogram_displayed = False
    
    mesh = np.zeros((znp,ynp))
    for i, zp in enumerate(zpos):
        umv(zmot, zp)
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan( ymot, ystart, ystep, ynp, ctim )
        else:
            fscan( ymot, ystop, -ystep, ynp, ctim )
        A = fscan.get_data()
        y = A[ctr]
        x = A[ymot.name]
        if (i % 2) == 0: 
            mesh[i,:] = y
        else: 
            mesh[i,:] = y[::-1]
        p.set_data(mesh, scale=(ystep, zstep)) 
        p.side_histogram_displayed = False



