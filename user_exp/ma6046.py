
import numpy as np
import time
from bliss.common import cleanup


def goto_tomo():
    moveto_basler(pinhole_switch=True)
    umv(attrz,0,atty,-10)
    umv(s9hg,2,s9vg,2,aly,-8)
    tfoh1.set(0,'Be')
    sheh3.open()

def basler_tomo_scan():
    fulltomo.half_turn_scan('nscope_pct')
    
def goto_eiger():
    umv(s9hg,0.1,s9vg,0.1,aly,0)
    tfoh1.set(16,'Be')
    umv(attrz,-8,atty,0)
    newdataset('align')
    moveto_eiger()
    
def z_scan(z_start, z_stop):
    newdataset('z_scan')
    for i,z in enumerate(np.arange(z_start,z_stop,0.003)):
        umv(ntz,z)
        if i % 2:
            fscan(rot,-90, 1.25, 181/1.25, 0.05)
        else:
            fscan(rot,90, -1.25, 181/1.25, 0.05)
    umv(ntz,(z_start+z_stop)/2)
    
def z_scan_pz(z_start, z_stop):
    newdataset('z_scan')
    for i,z in enumerate(np.arange(z_start,z_stop,1)):
        umv(pz,z)
        if i % 2:
            fscan(rot,-90, 1.25, 181/1.25, 0.05)
        else:
            fscan(rot,90, -1.25, 181/1.25, 0.05)
    umv(pz,50)

def night_scan_4():
    for i in range(0,200):
        goto_tomo()
        basler_tomo_scan()
        goto_eiger()
        z_scan_pz(25,75)
        
def lunch_scan():
    for i in range(0,200):
        fulltomo.half_turn_scan('pct')

def night_scan_3():
    for i in range(0,200):
        goto_tomo()
        basler_tomo_scan()
        goto_eiger()
        z_scan(-3.884, -3.664)

def night_scan_2():
#    umv(ntz,-3.57454)
#    half1(150, 'XRDCT')
#    umv(ntz,-3.49)
    for i in range(0,200):
        goto_tomo()
        basler_tomo_scan()
        goto_eiger()
        z_scan(-3.80, -3.628)

def night_scan():
    umv(ntz,-3.57454)
    half1(150, 'XRDCT')
    umv(ntz,-3.49)
    for i in range(0,100):
        goto_tomo()
        basler_tomo_scan()
        goto_eiger()
        z_scan(-3.58, -3.40)

def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)


    

class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        #if self.expotime < 0.005:
        #    ACTIVE_MG.disable('mca')
        #    print("Disabled mca: too fast scan")
        #    eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )


def half1(ymax,
          datasetname,
          ystep=1,
          ymin=-150,
          rstep=0.125,
          astart = -90,
          arange = 181,
          expotime=0.005):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)


def next_rstart( stepsize, speed, home_range=15. ):
    """ Computes the next start position to keep going """
    if abs(360 - (rot.dial % 360)) < home_range: # if you are within range, go home
        print("Homing because there is a home nearby")
        rot.home()
    elif rot.dial*rot.steps_per_unit > pow(2,30):
        print("Running out of steps so homing")
        rot.home()
    pnow = rot.position
    offset = speed * speed / ( 2 * rot.acceleration )
    next_start = ( int(np.floor( (pnow + np.sign(stepsize)*offset)/stepsize )) + 1 ) * stepsize
    return next_start
    
def z_series(dset = 'z_serie'):    
    z0 = pz.position
    zpos = z0 + np.array([0,-1,1,-2,2,-3,3,-4,4])
    for z in zpos:
        umv(pz,z)
        half1(250, f"{dset}_{z:.0f}", ystep=0.500,rstep=0.05,astart = -90,arange = 181,expotime=0.002)
