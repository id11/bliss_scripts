import numpy as np

def moveto_imaging():
    umv(ffdtx1,225,ffdtz1,100,bigy,22)
    umv(d3ty,0)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.disable('mca*')
    ACTIVE_MG.enable('marana*')
    ACTIVE_MG.disable('*bpm*')
    umv(s8vg,1.5,s8hg,1.5,s7vg,2,s7hg,1.5)
    
def moveto_diffraction():
    umv(d3ty,180,bigy,0)
    umv(ffdtx1,225,ffdtz1,0,nfdtx,80)
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.enable('frelon3*')
    ACTIVE_MG.enable('mca*')
    ACTIVE_MG.disable('*bpm*')
    umv(s8vg,0.5,s8hg,0.5,s7vg,1.2,s7hg,1)
    
def scanrotation():
    sc = dscan(samry,-0.4,0.4,80,0.02)
    ry = sc.get_data('axis:samry')
    ims = np.mean(np.array(sc.get_data('marana:image'))[:,700:1300,1000:1020], axis=2).T
    f=flint()
    rya = np.array([ry for i in range(600)])
    xa = np.array([[i for i in range(600)] for k in range(len(ry))]).T
    p = f.get_plot("scatter", name="Rotation scan", unique_name="RSP")
    p.clear_data()
    p.set_data(rya.flatten(), xa.flatten(), ims.flatten())
    p.focus()
    
    
def edgescan():
    ctim = 0.2
    rng = 0.1
    s = dscan(samtz,-0.05,0.08,50,ctim)
    x, y = s.get_data('axis:samtz'), s.get_data('frelon3:roi_counters:roi1_avg')
    ny = np.clip(y, y.min(), 1*y.max()) - (y.min()) 
    ny = ny - (ny.max()/4)
    edge = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    print(edge)
    umv(samtz, edge)
    where()
    return edge
    
    
def voltageramp(voltage, voltage_step, step_time, count=False):
    cv = hmc2.voltage_setpoint
    if cv > voltage: voltage_step = -abs(voltage_step)
    voltsteps = np.arange(cv, voltage, voltage_step)
    print('Ramping voltage to',voltage,'V in',voltage_step,'V steps')
    for vs in voltsteps:
        hmc2.voltage_setpoint = vs
        print(vs,'V')
        if count: ct(0.01,wcid11k)
        sleep(step_time)
    hmc2.voltage_setpoint = voltage
    print('Voltage ramped to',voltage)
    
    
def runtemperaturescans():
    temperatures = [22,40,60,80,100,120]
    for temp in temperatures:
        umv(nanodacpool_axis, temp)
        sleep(10)
        newdataset('align_%03iC'%temp)
        edgescan()
        newdataset('scan_%03iC'%temp)
        dscan(samtz,-0.015,0.075,180,2)
    umv(nanodacpool_axis, 22)
    sleep(10)
    newdataset('align_022C_after')
    edgescan()
    newdataset('scan_022C_after')
    dscan(samtz,-0.015,0.075,180,2)
        
    
    
def runvoltagescans():
    voltages = np.arange(0,1050,100)
    print(voltages)
    for volt in voltages:
        hmc2.voltage_setpoint = volt/1000
        hmc2.output = 'ON'
        sleep(8)
        newdataset('align_%04iV'%volt)
        edgescan()
        newdataset('scan_%04iV'%volt)
        dscan(samtz,-0.01,0.04,100,2)
    hmc2.output = 'OFF'
    hmc2.voltage_setpoint = 0
    sleep(8)
    newdataset('align_0000V_after')
    edgescan()
    newdataset('scan_0000V_after')
    dscan(samtz,-0.01,0.04,100,2)


def runvoltageuntilbreakdown_10stacks():
    voltages = [0,50,100,150,200,250,300]
    print(voltages)
    #hmc2.voltage_setpoint = 0
    for volt in voltages:
        hmc2.output = 'ON'
        #voltageramp(volt/1000, 40/1000, 1, count=False)
        hmc2.voltage_setpoint = volt/1000
        sleep(8)
        s = ct(0.1,wcid11k)
        adc = s.get_data('wcid11k:adcn1')
        if abs(adc-hmc2.voltage_setpoint) > 0.04: break
        print('Voltage seems good, taking data...')
        newdataset('align_%04iV'%volt)
        edgescan()
        newdataset('scan_%04iV'%volt)
        dscan(samtz,-0.015,0.075,180,2)
    hmc2.output = 'OFF'
    hmc2.voltage_setpoint = 0
    print('Voltage back to 0, taking data...')
    sleep(8)
    newdataset('align_0000V_after')
    edgescan()
    newdataset('scan_0000V_after')
    dscan(samtz,-0.015,0.075,180,2)

def runvoltageuntilbreakdown_1stack():
    voltages = [0,20,40,60]
    print(voltages)
    #hmc2.voltage_setpoint = 0
    for volt in voltages:
        hmc2.output = 'ON'
        #voltageramp(volt/1000, 40/1000, 1, count=False)
        hmc2.voltage_setpoint = volt/1000
        sleep(8)
        s = ct(0.1,wcid11k)
        adc = s.get_data('wcid11k:adcn1')
        if abs(adc-hmc2.voltage_setpoint) > 0.04: break
        print('Voltage seems good, taking data...')
        newdataset('align_%04iV'%volt)
        edgescan()
        newdataset('scan_%04iV'%volt)
        dscan(samtz,-0.01,0.04,100,2)
    hmc2.output = 'OFF'
    hmc2.voltage_setpoint = 0
    print('Voltage back to 0, taking data...')
    sleep(8)
    newdataset('align_0000V_after')
    edgescan()
    newdataset('scan_0000V_after')
    dscan(samtz,-0.01,0.04,100,2)


def run1000voltagescan():
    sheh3.open()
    voltage = 1000
    print('Sample connection should be in positive sense')
    print('Measuring at %i V'%voltage)
    hmc2.voltage_setpoint = voltage/1000
    hmc2.output = 'ON'
    sleep(8)
    newdataset('align_p%04iV'%voltage)
    edgescan()
    newdataset('scan_p%04iV'%voltage)
    dscan(samtz,-0.01,0.04,100,2)
    hmc2.output = 'OFF'
    hmc2.voltage_setpoint = 0
    sheh3.close()

def runnegativevoltagescan(voltage):
    sheh3.open()
    voltage = abs(voltage)
    print('Sample connection should be in negative sense')
    print('Measuring at -%i V'%voltage)
    hmc2.voltage_setpoint = voltage/1000
    hmc2.output = 'ON'
    sleep(8)
    newdataset('align_n%04iV'%voltage)
    edgescan()
    newdataset('scan_n%04iV'%voltage)
    dscan(samtz,-0.01,0.04,100,2)
    hmc2.output = 'OFF'
    hmc2.voltage_setpoint = 0
    sheh3.close()

    
def runhighvoltageramp():
    print('Sample connection should be in positive sense')
    print('Ramping to 1000 V')
    hmc2.voltage_setpoint = 1
    hmc2.output = 'ON'
    sleep(10)
    hmc2.output = 'OFF'
    hmc2.voltage_setpoint = 0
    





