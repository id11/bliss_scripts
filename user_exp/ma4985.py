

# In NSCOPE window:
#   user_script_load("ma4985.py")
#   user.grain_map_slices( 'datasetname' )

import numpy as np
import time


user_script_load("nscopemacros")
user_script_load("optics")

def pause_for_refill(t, cpos=10.106, upos=8.315):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    if abs(cpm18.position - cpos)>0.02 or abs(cpm18t.position - 0.075)>0.02:
        cpm18_goto(cpos)
    if abs(u22.position - upos)>0.02:
        umv(u22, upos)


def scanbox( ):
    zpoints = np.linspace( 5, 95, 10 )
    z0 = pz.position
    rotcen = 150
    rotrange = 2.5  # plus or minus ...
    rotstep = 0.05
    rotpoints = int(np.ceil(rotrange*2/rotstep)) + 1
    expotime = 0.5
    try:
        for zoff in zpoints:
            umv(pz, zoff)
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen - rotrange, rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
    finally:
        umv(pz, z0)

def domanyscans( datasetname ):
    newdataset( datasetname )
    while 1:
        scanbox( )
        
def scanboxncm( ):
    zpoints = ( 5, 25, 45, 65, 85 )
    z0 = pz.position
    rotcen = 210
    rotrange = 2.5  # plus or minus ...
    rotstep = 0.05
    rotpoints = int(np.ceil(rotrange*2/rotstep)) + 1
    expotime = 0.1
    try:
        for zoff in zpoints:
            umv(pz, zoff)
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen - rotrange, rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
    finally:
        umv(pz, z0)

def domanyncmscans( datasetname ):
    newdataset( datasetname )
    while 1:
        scanboxncm( )

def scanboxfast( ):
#    zpoints = np.linspace( 5, 25 , 2)
    zpoints = ( 5, 25 )
    z0 = pz.position
    rotcen = 150
    rotrange = 2.5  # plus or minus ...
    rotstep = 0.05
    rotpoints = int(np.ceil(rotrange*2/rotstep)) + 1
    expotime = 0.1
    try:
        for zoff in zpoints:
            umv(pz, zoff)
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen - rotrange, rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
    finally:
        umv(pz, z0)

def domanyfastscans( datasetname ):
    newdataset( datasetname )
    while 1:
        scanboxfast( )

def scanboxlfp( ):
    zpoints = np.linspace( 5, 15, 2)
    z0 = pz.position
    rotcen = 210
    rotrange = 2.5  # plus or minus ...
    rotstep = 0.05
    rotpoints = int(np.ceil(rotrange*2/rotstep)) + 1
    expotime = 1
    try:
        #for zoff in zpoints:
        for i in range(0, len(zpoints), 2):
            # foward scan:
            umv(pz, zpoints[i] )
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen - rotrange, rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
            # backward scan:
            umv(pz, zpoints[i+1] )
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen + rotrange, -rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
    finally:
        umv(pz, z0)

def domanylfpscans( datasetname ):
    newdataset( datasetname )
    while 1:
        scanboxlfp( )


def scanboxlfp_fscan2d( ):
    zpoints = np.linspace( 5, 35 , 4)
    z0 = pz.position
    rotcen = 150
    rotrange = 2.5  # plus or minus ...
    rotstep = 0.05
    rotpoints = int(np.ceil(rotrange*2/rotstep)) + 1
    expotime = 0.025
            # motor, start, step, npts
    fscan2d( pz, 5, 10, 4,
             rot, rotcen-rotrange, rotstep, rotpoints, 
             expotime, fast_motor_mode='ZIGZAG')

def domanylfpscans_fscan2d( datasetname ):
    newdataset( datasetname )
    while 1:
        scanboxlfp_fscan2d( )


def grain_map_slices( datasetname ):
    ACTIVE_MG.disable( "mca*" ) # turn off as unused
    newdataset( datasetname )         # 1,2, 3, 4, 5, 6, 7, 8, 9, 10 
    zpoints = ( 5, 25, 45, 65, 85 )   # 5,15,25,35,45,55,65,75,85,95
    rotcen = 210.0
    rotrange = 50.0  # plus or minus ... cell opens 120 in total
    rotstep = 0.05   # suggest to keep the same as before, matches spots
    rotpoints = int(np.ceil(rotrange*2/rotstep)) + 1
    expotime = 0.01   # from 0.005 up to whatever you want
    stepsizey = 1.   # micron
    # layer is 40 microns thick
    nysteps = 30      # at least 20 deep on +/- y (40 micron sample thickness)
    
    total_time_s = (len(zpoints)*(rotpoints)*(expotime)*(2*nysteps+1))
    print("estimate", total_time_s, "/s",total_time_s/60,"/minutes",total_time_s/3600,"/hours")
    
    try:
        for zoff in zpoints:
            umv(pz, zoff)
            #    motor, start             , steps
            pause_for_refill( expotime )
            #       slowmotor slowstartpos    slowstepsize  slownpoint
            fscan2d( dty, -nysteps*stepsizey, stepsizey, 2*nysteps+1,
                     rot, rotcen-rotrange, rotstep, rotpoints, expotime,
                     fast_motor_mode='ZIGZAG', scan_mode='CAMERA', )
            #      fastmotor  faststartpos   faststepsize  fastnpoints expotime                   
            
    finally:
        umv(pz, z0)
    
    
    
    
            
        
def scanboxyloop( datasetname ):
    newdataset( datasetname )
    zpoints = (15,20,30)
    z0 = pz.position
    rotcen = 150
    rotrange = 50.  # plus or minus ...
    rotstep = 0.05
    rotpoints = int(np.ceil(rotrange*2/rotstep)) + 1
    expotime = 0.01
    try:
        for zoff in zpoints:
            umv(pz, zoff)
            #                     start,end,step
            for yoff in np.arange(-30,30.01,1.):
                umv(dty, yoff)
                #    motor, start             , steps
                pause_for_refill( expotime )
                fscan(rot, rotcen - rotrange, rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
    finally:
        umv(pz, z0, dty, 0)
        
        
def BIGMAP_scancolumn():
    zpoints = np.linspace( 5, 95, 10 )
    z0 = pz.position
    rotcen = 570
    rotrange = 2.5  # plus or minus ...
    rotstep = 0.05
    rotpoints = int(np.ceil(rotrange*2/rotstep)) + 1
    expotime = 0.2
    try:
        #for zoff in zpoints:
        for i in range(0, len(zpoints), 2):
            # foward scan:
            umv(pz, zpoints[i] )
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen - rotrange, rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
            # backward scan:
            umv(pz, zpoints[i+1] )
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen + rotrange, -rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
    finally:
        umv(pz, z0)        

def BIGMAP_domanyscans( datasetname ):
    newdataset( datasetname )
    while 1:
        BIGMAP_scancolumn( )        
 
def BIGMAP_domanyhscans(datasetname):
    newdataset( datasetname )
    while 1:
        horizontal_scan( )   
            

def horizontal_scan():
    y0=-1500
    ypoints = (-1500, -1000, -500, 0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 3500, 3000, 2500, 2000, 1500, 1000, 500, 0, -500,-1000)
    rotcen = 210
    rotrange = 2.5  # plus or minus ...
    rotstep = 0.05
    rotpoints = int(np.ceil(rotrange*2/rotstep)) + 1
    expotime = 0.05
    try:
        #for y in ypoints:
        for i in range(0, len(ypoints), 2):
            # foward scan:
            umv(dty, ypoints[i] )
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen - rotrange, rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
            # backward scan:
            umv(dty, ypoints[i+1] )
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen + rotrange, -rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
    finally:
        umv(dty, y0)          
        
def zigzag():
    ypoints = np.linspace(0,3000,100)
    y0= dty.position
    rotcen = 210
    rotrange = 2.5  # plus or minus ...
    rotstep = 0.05
    rotpoints = int(np.ceil(rotrange*2/rotstep)) + 1
    expotime = 0.05
    try:
        #for zoff in ypoints:
        for i in range(0, len(ypoints), 2):
            # foward scan:
            umv(dty, ypoints[i] )
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen - rotrange, rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
            # backward scan:
            umv(dty, ypoints[i+1] )
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen + rotrange, -rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
    finally:
        umv(dty, y0)         
    
def BIGMAP_fastscancolumn():
    zpoints = np.linspace( 5, 95, 10 )
    z0 = pz.position
    rotcen = 210
    rotrange = 2.5  # plus or minus ...
    rotstep = 0.05
    rotpoints = int(np.ceil(rotrange*2/rotstep)) + 1
    expotime = 0.05
    try:
        #for zoff in zpoints:
        for i in range(0, len(zpoints), 2):
            # foward scan:
            umv(pz, zpoints[i] )
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen - rotrange, rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
            # backward scan:
            umv(pz, zpoints[i+1] )
            #    motor, start             , steps
            pause_for_refill( expotime )
            fscan(rot, rotcen + rotrange, -rotstep, rotpoints, expotime,
                  scan_mode='CAMERA' )
    finally:
        umv(pz, z0)             
        
def BIGMAP_fastdomanyscans( datasetname ):
    newdataset( datasetname )
    while 1:
        BIGMAP_fastscancolumn( )         
