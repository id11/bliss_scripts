import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime

atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')

save_path = '/data/visitor/ihma423/id11/20231205/RAW_DATA/S9'
f_target = 20

prefix_name = str(f_target)
prefix_name.replace('.','p')
# prefix_name = 'Al_test3_' + prefix_name + 'N_'
# prefix_name = 'FSH_steel_' + prefix_name + 'N_'
prefix_name = 'S9_' + prefix_name + 'N_'
print(prefix_name)


def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 7
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.1
    dct_pars['refon'] = dct_pars['num_proj']
    dct_pars['ref_step'] = -4
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.8 #0.58 #0.55 # 0.85 #0.58 # 0.25 #0.85
    dct_pars['slit_vg'] = 0.1 #0.15 # 0.12 # 0.15 # 0.2
    dct_pars['dist'] = 5.4 # 2.1 # 1.6 #2.8 #4.1   # 4.5
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = -2.07 #0.1379 # 0.5079 # 0.5229 # 1.1229 #1.9129 #2.7799 #2.8149 #1.5799 #1.8549 # 1.7184 # at 0N scan
    dct_pars['shift_step_size'] = 0.08
    dct_pars['nof_shifts'] = 8
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars

def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 7 # 3.9 # -7 # 18.9 at 0N scan
    ff_pars['step_size'] = 0.25
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.085 # 0.08 # 0.1 #0.08
    ff_pars['slit_hg'] = 0.75 # 0.53 # 0.37
    ff_pars['slit_vg'] = 0.1 # 0.1 #0.12 # 0.1 #0.2 # 0.3
    ff_pars['mode'] = 'ZIGZAG' # FORWARD
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = -2.07#4.53555#4.6504#4.6404 #44.6354 #4.6654#4.6854#4.6804#4.6754#4.6454#4.6154 # 4.4104 #4.5654 #4.5404#4.5104#4.5604#4.5554 #4.5654 #4.5554 #4.5454 #4.5304#4.5506#4.5756#4.5506 #4.6256#4.5956 #4.5456 #4.5556 #4.5856 # 4.7984 # 4.7634 # 4.6884 at 0N scan
    ff_pars['shift_step_size'] = 0.08 # 0.08 # 0.18 # 0.28
    ff_pars['nof_shifts'] = 8 # 6 #4 #9
    ff_pars['cpm18_detune'] = 0
    ff_pars['ceo2_samtx'] = 0.1850 # -0.1092 for TiWL # -0.5846 for T40
    ff_pars['ceo2_samty'] = -0.7652 # -1.4638 for TiWL # -0.7178 for T40
    ff_pars['ceo2_samtz'] = 4.6154 # 4.5654 for TiWL # 4.5556  for T40
    ff_pars['ceo2_difftz'] = -21.7648 # -21.7648 # for TiWL -21.7648 for T40
    return ff_pars

def run_dct_ff(prefix_name = prefix_name):
    # assum you start with dct condition
    # start DCT measurements
    switch_to_dct()
    dct_pars = define_dct_pars()
    print(dct_pars)
    umv(atty,-10,attrz,0)
    dct_zseries(dct_pars, f'dct_{prefix_name}')
    
    # switch to 3DXRD measurements
    switch_to_ff()
    ff_pars = define_ff_pars()
    umv(atty,0,attrz,-10)
    ff_zseries(ff_pars, f'ff_{prefix_name}')
    ff_ceo2(ff_pars)
    
    # switch back to DCT camera for alignment (CoR = 1111 pixels)
    fsh.close()
    umv(atty,-10,attrz,0)
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    ff_out()
    umv(d3ty, 0, d3tz, 0)
    umv(nfdtx,100)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana:image')
    fsh.open()
    umvct(s7vg, 1.2, s7hg, 1.2, s8vg,1.3, s8hg, 1.3)
    print('Done successfully')

def switch_to_dct():
    fsh.close()
    tfoh1.set(16,'Be')
    tfoh1.set(64,'Al')
    marana_in()
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3:image')
    fsh.open()
    # umvct(s7vg, 0.25, s7hg, 0.25, s8vg,0.4, s8hg, 0.4)    

def switch_to_pct():
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    ff_out()
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3:image')
    umvct(nfdtx,100)
    umvct(d3ty,0,d3tz,0)
    umvct(s7vg, 2.5, s7hg, 2.5, s8vg,2.8, s8hg, 2.8)
    
    
def switch_to_ff():
    fsh.close()
    #tfoh1.set(16,'Be')
    #tfoh1.set(64,'Al')
    ff_in()
    ACTIVE_MG.disable('marana3*')
    ACTIVE_MG.enable('frelon3*')
    fsh.open()
    umvct(s7vg, 0.05, s7hg, 0.05, s8vg,0.1, s8hg, 0.1)
   
def ff_out():
    umv(ffdtx1, 530)
    umv(ffdtz1, 350)
    
def marana_in():
    ff_out()
    umv(d3ty, 0, d3tz, 0)
    umv(nfdtx,100)

def marana_out():
    umv(nfdtx,100,d3ty,150)
    
def ff_in():
    marana_out()
    umv(ffdtz1, 0)
    umv(ffdtx1, 150)

def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'], s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umv(s8vg, dct_pars['slit_vg'] + 0.05, s8hg, dct_pars['slit_hg'] + 0.05)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])
    
def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')

def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.05, s8hg, pars['slit_hg'] + 0.05)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)
    
def ff_zseries(ff_pars, datasetname = 'ff'):
    #user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    #umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    umv(diffrz, ff_pars['start_pos'], s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.05, s8hg, ff_pars['slit_hg'] + 0.05)
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    #user.cpm18_goto(cpm18_tuned)
    #umv(u22, u22_tuned)
    print('ff_zseries succeed')
    return('Succeed')

def ff_ceo2(ff_pars, prefix_name = prefix_name):
    umvct(s7vg, 0.1, s7hg, 0.1 )
    umvct(s8vg, 0.15, s8hg, 0.15 )
    
    samtx0 = samtx.position
    samty0 = samty.position
    samtz0 = samtz.position
    difftz0 = difftz.position
    
    fsh.close()
    umv(difftz, ff_pars['ceo2_difftz'])
    umv(samtx,ff_pars['ceo2_samtx'],samty,ff_pars['ceo2_samty'],samtz,ff_pars['ceo2_samtz'])
    umv(diffrz, 0)
    dset_name = 'ff_' + prefix_name + 'CeO2'
    newdataset(dset_name)
    fsh.open()
    loopscan( 50, ff_pars['exp_time']*12.5)
    umv(diffrz, 180)
    loopscan( 50, ff_pars['exp_time']*12.5)
    
    # move back
    umv(samtx,samtx0,samty,samty0,samtz,samtz0)
    umv(difftz,difftz0)   
    
    
def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def loading(load, file_name, voltage_target = 120, voltage_ramprate = 0.1, tol = 0.1):
    initialize_stress_regul()
    print('Initialized stress regulation')
    print('Activating ramprate')
    regul_off(voltage_ramprate, voltage_target)
    print('Started voltage ramprate')
    with open(file_name, 'w') as f:
        # ramping up the load
        t0 = time.time()
        dt = 0
        exit_flag = False
        while exit_flag == False:
            dt = time.time() - t0
            #print('Waiting to reach the targe = {} N'.format(load))
            #print('Current load is {} N; Ramping time is {} s'.format(stress_adc.get_value(), dt))
            print(_get_human_time(),stress.position,stress_adc.get_value(),file=f)
            
            if stress.position >= 119.8:
                print('Reaching maximum !!!')
                regul_off(voltage_ramprate, 118)
                #initialize_stress_regul()
                exit_flag = True
                reach_max = True
            
            if np.abs(stress_adc.get_value() - load) <= tol:
                exit_flag = True

    initialize_stress_regul()

def unloading(load, file_name, voltage_target = 0, voltage_ramprate = 0.1, tol = 0.1):
    initialize_stress_regul()
    print('Initialized stress regulation')
    print('Activating ramprate')
    regul_off(voltage_ramprate, voltage_target)
    print('Started voltage ramprate')
    with open(file_name, 'w') as f:
        # ramping up the load
        t0 = time.time()
        dt = 0
        exit_flag = False
        while exit_flag == False:
            dt = time.time() - t0
            #print('Waiting to reach the targe = {} N'.format(load))
            #print('Current load is {} N; Ramping time is {} s'.format(stress_adc.get_value(), dt))
            print(_get_human_time(),stress.position,stress_adc.get_value(),file=f)
            
            if stress.position >= 119.8:
                print('Reaching maximum !!!')
                regul_off(voltage_ramprate, 118)
                #initialize_stress_regul()
                exit_flag = True
                reach_max = True
            
            if np.abs(stress_adc.get_value() - load) <= tol:
                exit_flag = True

    initialize_stress_regul()
    
def load_ramp_by_target(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    stress_regul.plot()
    if to_file is not None:
        with open(to_file, 'w') as f:
            # write_to_file = to_file if to_file is not None else sys.stdout
            current_load = stress_adc.get_value()
            if abs(target - current_load) < 1:
                #print('target position is to close to current position - aborting !',
                #      file=write_to_file)
                print(_get_human_time(),stress.position,current_load,file=f)
                return
            if target > current_load:
                stress_increment = 0.1
                while (current_load < target):
                    mvr(stress, stress_increment)
                    current_load = stress_adc.get_value()
                    sleep(time_step)
                    print(_get_human_time(),stress.position,current_load,file=f)
            else:
                stress_increment = -0.1
                while (current_load > target):
                    mvr(stress, stress_increment)
                    current_load = stress_adc.get_value()
                    sleep(time_step)
                    print(_get_human_time(),stress.position,current_load,file=f)
    return
    

def load_constant(f_target = f_target, prefix_name = prefix_name, save_path = save_path, time_step=0.5):
    current_load = stress_adc.get_value()
    if f_target > current_load:
        # file_name = 'load_to' + str(int(f_target)) + 'N.txt'
        file_name = prefix_name + '.txt'
        to_file = os.path.join(save_path, file_name)
        loading(f_target, to_file)
    else:
        # file_name = 'unload_to' + str(int(f_target)) + 'N.txt'
        file_name = prefix_name + '.txt'
        to_file = os.path.join(save_path, file_name)
        unloading(f_target, to_file)
    #if f_target > 1:
    #    load_ramp_by_target(f_target, time_step,to_file)
    #else:
    #    print('I do nothing because the load is smaller than 1 N')
    

# loading by changing set point
def loading_by_sp(load = f_target, prefix_name = prefix_name, ramp_rate = 0.5, tol = 0.25,save_path = save_path):
    file_name = os.path.join(save_path, prefix_name + '.txt')    
    initialize_stress_regul()
    print('Initialized stress regulation')
    print('Change force setpoint')
    stress_regul.ramprate = ramp_rate
    stress_regul.setpoint=load # [N]
    print('Started loading ...')
    with open(file_name, 'w') as f:
        # ramping up the load
        t0 = time.time()
        dt = 0
        exit_flag = False
        while exit_flag == False:
            dt = time.time() - t0
            #print('Waiting to reach the targe = {} N'.format(load))
            #print('Current load is {} N; Ramping time is {} s'.format(stress_adc.get_value(), dt))
            print(_get_human_time(),stress.position,stress_adc.get_value(),file=f)
            
            if stress.position >= 119.8:
                print('Reaching maximum !!!')
                regul_off(0.1, 118)
                #initialize_stress_regul()
                exit_flag = True
                reach_max = True
            
            if np.abs(stress_adc.get_value() - load) <= tol:
                exit_flag = True
    print('Reached the force targe {} N within a tolerance of {} N, exiting ...'.format(load, tol))
    initialize_stress_regul()


    
