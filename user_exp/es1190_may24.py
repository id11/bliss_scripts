
import time, numpy as np

# user_script_load("es1190.py")
user_script_load('optics.py')


# def check_cpm18(pos=6.4158):
def check_cpm18(pos=7.0383):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()



def full1(ymax, datasetname, ystep=0.05, ymin=-99901):

    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    
    
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        umv(diffy,ypos[i])
        pause_for_refill( 90 )
        
        #        if (i % 2) == 0: # forwards
        # finterlaced(hrz,-330,  0.8, 450, 0.08 )
        #else:
        finterlaced(hrz,  -62, 0.8, 450, 0.08 )
        
        
        
def full2(ymin, ymax, datasetname, ystep=0.05):

    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    
    ny = int( (ymax-ymin) // ystep ) +1

  #  ylim = ny * ystep
    
  #  if ylim < ymax:
  #      ny += 1
  #      ylim += ystep
    ypos = np.linspace( ymin, ymax, ny )

    for i,y in enumerate(ypos):
        umv(diffy,y)
        pause_for_refill( 90 )
        
        #        if (i % 2) == 0: # forwards
        # finterlaced(hrz,-330,  0.8, 450, 0.08 )
        #else:
        finterlaced(hrz,  -62, 0.8, 450, 0.08 )
        
        
        
def z_scan(datasetname, z_up=17.75, z_low = 8.78, halfrange = .4, step = 0.010):
    if datasetname is not None:
        newdataset(datasetname)
    
    assert all([z_low - halfrange > hz.limits[0], z_up + halfrange < hz.limits[1]]), 'range out of hz limits'
    umv(diffy,0)   # go to central y pos
    
    umv(hz, z_up)
    
    
    dscan(hz, -halfrange, halfrange, int(2*halfrange/step), 0.1)
    
    umv(hz, z_low)
    dscan(hz, -halfrange, halfrange, int(2*halfrange/step), 0.1)
    
    umv(hz, (z_up+z_low)/2)
  
  

     

    

    
        


