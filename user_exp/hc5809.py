
from bliss.common import cleanup

def transmission( step=0.002, exptime=0.005 ):
    close_ffdoor()
    sheh3.open()
    with cleanup.cleanup(ffdiode, ffbsz, attrz, atty, restore_list=(cleanup.axis.POS,)):
        umvr(ffbsz, 5)
        ffdiode_in()
        umv( atty, -10, attrz, 0 )
        pico4.auto_range=True
        pico5.auto_range=True
        pico4.insert()
        ACTIVE_MG.disable('frelon3*')
        ct(1, pico4, pico5)
        pico4.auto_range=False
        pico5.auto_range=False
        
        fscan( diffrz, 0, step, 360/step, exptime )
        umvr(ffbsz, -5)
        ACTIVE_MG.enable('frelon3*')
    pico4.auto_range=True
    pico5.auto_range=True
    
def ADMET_diffraction( fname ):
    sheh3.open()
    umv(diffrz,-180)
    if (ffdoor.position > -10):
        open_ffdoor()
    umv( atty, 0, attrz, -6 )
    ACTIVE_MG.enable('frelon3*')
    newdataset( fname )
    plotselect("frelon3:roi_counters:roi1_max")
    finterlaced( diffrz, -38.-90,-0.1, (147-38)/0.1, 0.1 )
    finterlaced( diffrz,  28.-90, 0.1,  (142-28)/0.1, 0.1 )
    

def ADMET_transmission( fname, step=0.002, exptime=0.005 ):
    # -90 - 50
    #    close_ffdoor()
    sheh3.open()
    newdataset( fname )
    with cleanup.cleanup(ffdiode, ffbsz, attrz, atty, restore_list=(cleanup.axis.POS,)):
        umvr(ffbsz, 5)
        ffdiode_in()
        umv( atty, -10, attrz, 0 )
        pico4.auto_range=True
        pico5.auto_range=True
        pico4.insert()
        umv(diffrz, -180)
        ACTIVE_MG.disable('frelon3*')
        ct(1, pico4, pico5)
        pico4.auto_range=False
        pico5.auto_range=False
        plotselect("p201_20:ct2_counters_controller:fpico5")
        fscan( diffrz, -147, step, abs(137-38)/step, exptime )
        fscan( diffrz,   28, step, abs(142-28)/step, exptime )
        umvr(ffbsz, -5)
        ACTIVE_MG.enable('frelon3*')
    pico4.auto_range=True
    pico5.auto_range=True
    
    
def ADMET_cryo( fname ):
    sheh3.open()
    if (ffdoor.position > -10):
        umv(diffrz,90)
        open_ffdoor()
    umv( atty, 0, attrz, -6 )
    ACTIVE_MG.enable('frelon3*')
    newdataset( fname )
    plotselect("frelon3:roi_counters:roi1_max")
    finterlaced( diffrz,  31., 0.1,  (100-31)/0.1, 0.1 )
    
    
def ADMET_cool( fname ):
    newdataset(fname)
    ox800.ramprate = 120
    ox800.setpoint = 80
    tnow = ox800.read() 
    while True:
        finterlaced( diffrz, 50, 1, 12, 1 )
        print('tnow:' ,tnow )
        tnow = ox800.read() 
        

def collect_2scans( fname ):
    newdataset(fname)
    umv(attrz, -6)
    finterlaced( diffrz, 0, 0.1, 3600, 0.1 )
    umv(attrz, -12)
    finterlaced( diffrz, 0, 0.1, 3600, 0.1 )

def collect_1scan( fname ):
    newdataset(fname)
    umv(attrz, -6)
    finterlaced( diffrz, 0, 0.1, 3600, 0.1 )
       
        
def T80_3spots( fname ):
    ox800.ramprate = 360
    ox800.setpoint = 80
    tnow = ox800.read()
    while abs( ox800_in.read() - 80) > 1:
        sleep(10)
        tnow = ox800_in.read()
        print('tnow', tnow)
    print("wait 10 minutes")
    sleep(30)
    umv( samtx, -1.15, samty, -4.239, samtz, 0.89288 )
    collect_2scans( f"{fname}_edge" )
    umv( samtx, -1.15, samty, -4.239, samtz, 0.10712 )
    collect_2scans( f"{fname}_corner" )
    umv( samtx, -1.44, samty, -3.889, samtz, 0.89288 )
    collect_2scans( f"{fname}_middle" )
    
    
def Tseries( tsleep=3):
    # for t in np.arange( 110, 130, 0.5):
    for t in (113, 112, 111, 110, 100, 80, 175, 200) :
        ox800.ramprate = 360
        ox800.setpoint = t
        tnow = ox800_in.read()
        while abs( ox800_in.read() - t) > 0.1:
            ox800.setpoint = t
            sleep(10)
            tnow = ox800_in.read()
            print('tnow', tnow)
        print(f"wait {tsleep} minutes")
        sleep(tsleep*60)
        
        collect_1scan( f'T{t}K_zf_10' )


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)



def collect_nscope():
    sheh3.open()
    for i in range(3):
        try:
            if rot.position > 180:
                fscan( rot, 360, -0.05, 7200, 0.008 )
            else:
                fscan( rot,   0,  0.05, 7200, 0.008 )
            break
        except KeyboardInterrupt:
            raise
        except:
            print("The scan failed, trying to continue anyway")

     
     
     
def nscope_to_130( fname ):
    # aiming for about 45 minutes
    ox800.ramprate = 360
    ox800.setpoint = 90
    newdataset(fname)
    tnow = ox800_in.read()
    while abs( tnow - 130 ) > 0.1:
        ox800.setpoint = 130
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)
    for i in range(10):    
        collect_nscope()


def nscope_130K_090K_ref( fname ):
    ox800.ramprate = 50
    ox800.setpoint = 130
    newdataset(fname)
    tnow = ox800_in.read()
    while abs( tnow - 90 ) > 0.1:
        ox800.ramprate = 50
        ox800.setpoint = 90
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)
        
        
def nscope_090K_130K_ref( fname ):
    ox800.ramprate = 50
    ox800.setpoint = 100
    newdataset(fname)
    tnow = ox800_in.read()
    while abs( 130 - tnow ) > 0.1:
        ox800.ramprate = 50
        ox800.setpoint = 130
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)        




def nscope_through_VT_cool( fname ):
    ox800.ramprate = 40
    ox800.setpoint = 150
    newdataset(fname)
    tnow = ox800_in.read()
    while abs( tnow - 130 ) > 0.1:
        ox800.ramprate = 40
        ox800.setpoint = 130
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)
    
    ox800.ramprate = 20
    ox800.setpoint = 130
    tnow = ox800_in.read()
    while abs( tnow - 125 ) > 0.1:
        ox800.ramprate = 20
        ox800.setpoint = 125
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)
    
    ox800.ramprate = 10
    ox800.setpoint = 125
    tnow = ox800_in.read()
    while abs( tnow - 115 ) > 0.1:
        ox800.ramprate = 10
        ox800.setpoint = 115
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)

    ox800.ramprate = 20
    ox800.setpoint = 115
    tnow = ox800_in.read()
    while abs( tnow - 110 ) > 0.1:
        ox800.ramprate = 20
        ox800.setpoint = 110
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)

    ox800.ramprate = 40
    ox800.setpoint = 110
    tnow = ox800_in.read()
    while abs( tnow - 90 ) > 0.1:
        ox800.ramprate = 40
        ox800.setpoint = 90
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)

def nscope_through_VT_cool_ref( fname ):
    ox800.ramprate = 40
    ox800.setpoint = 150
    newdataset(fname)
    tnow = ox800_in.read()
    while abs( tnow - 125 ) > 0.1:
        ox800.ramprate = 40
        ox800.setpoint = 125
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)
    
    ox800.ramprate = 10
    ox800.setpoint = 125
    tnow = ox800_in.read()
    while abs( tnow - 121 ) > 0.1:
        ox800.ramprate = 10
        ox800.setpoint = 121
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)
    
    ox800.ramprate = 5
    ox800.setpoint = 121
    tnow = ox800_in.read()
    while abs( tnow - 116 ) > 0.1:
        ox800.ramprate = 5
        ox800.setpoint = 116
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)

    ox800.ramprate = 10
    ox800.setpoint = 116
    tnow = ox800_in.read()
    while abs( tnow - 110) > 0.1:
        ox800.ramprate = 10
        ox800.setpoint = 110
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)

    ox800.ramprate = 40
    ox800.setpoint = 110
    tnow = ox800_in.read()
    while abs( tnow - 90 ) > 0.1:
        ox800.ramprate = 40
        ox800.setpoint = 90
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)


def nscope_through_VT_heat( fname ):
    ox800.ramprate = 40
    ox800.setpoint = 90
    newdataset(fname)
    tnow = ox800_in.read()
    while abs( 110 - tnow ) > 0.1:
        ox800.ramprate = 40
        ox800.setpoint = 110
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)
    
    ox800.ramprate = 10
    ox800.setpoint = 110
    tnow = ox800_in.read()
    while abs( 116 - tnow ) > 0.1:
        ox800.ramprate = 10
        ox800.setpoint = 116
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)
 
    ox800.ramprate = 5
    ox800.setpoint = 116
    tnow = ox800_in.read()
    while abs( 121 - tnow ) > 0.1:
        ox800.ramprate = 5
        ox800.setpoint = 121
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)   
  
    ox800.ramprate = 10
    ox800.setpoint = 121
    tnow = ox800_in.read()
    while abs( 125 - tnow ) > 0.1:
        ox800.ramprate = 10
        ox800.setpoint = 125
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)
        
    ox800.ramprate = 40
    ox800.setpoint = 125
    tnow = ox800_in.read()
    while abs( 150 - tnow ) > 0.1:
        ox800.ramprate = 40
        ox800.setpoint = 150
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)


def nscope_through_VT_heat_short( fname ):
 
    ox800.ramprate = 10
    ox800.setpoint = 114
    newdataset(fname)
    tnow = ox800_in.read()
    while abs( 121 - tnow ) > 0.1:
        ox800.ramprate = 10
        ox800.setpoint = 121
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)   
  
    ox800.ramprate = 20
    ox800.setpoint = 121
    tnow = ox800_in.read()
    while abs( 125 - tnow ) > 0.1:
        ox800.ramprate = 20
        ox800.setpoint = 125
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)
        
    ox800.ramprate = 40
    ox800.setpoint = 125
    tnow = ox800_in.read()
    while abs( 150 - tnow ) > 0.1:
        ox800.ramprate = 40
        ox800.setpoint = 150
        collect_nscope()
        tnow = ox800_in.read()
        print('tnow',tnow)
    
    
