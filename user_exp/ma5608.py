

import numpy as np
import time



def heating_testsample(tmin=900, tmax=1500, tstep=100, k=53):
    nanodacpool.ramprate=20
    for setpoint in np.arange( tmin, tmax+0.1, tstep):
        nanodacpool.setpoint = setpoint
        t = nanodacpool_temp.read()
        while abs( t - setpoint )>2:
            time.sleep( 1 )
            t = nanodacpool_temp.read()
            print("T=%f\r"%(t), end='     ')
        print()
        newdataset('ff_%dC_infurnace_%d'%( setpoint, k ) )
        k += 1
        finterlaced( diffrz, 0, 0.8, 360/0.8, 0.08 )
    nanodacpool.setpoint = 850
    

        
def map_initial():
    ACTIVE_MG.enable('mca')
    for zpos in np.arange( 80, 90.01, 0.5):
        umv( pz, zpos )
        user.half1( 50,
                    'Z%03d'%(zpos*10),
                    ystep = 0.3,
                    rstep = 0.125,
                    expotime = 0.005 )
        
            
    
def map_ti():
    z0 = 5.7
    ACTIVE_MG.disable('mca') # no mca 
    for zpos in (60,):
        umv(pz, z0 + zpos)
        user.full1( 360,
                    'Z%03d_3'%(zpos*10),
                    ystep = 0.3,
                    rstep = -0.05,
                    expotime = 0.002 )



def map_step1():
    ACTIVE_MG.enable('mca')
    for zpos in np.arange( 85.5, 96.01, 0.5):
        umv( pz, zpos )
        user.half1( 50,
                    'Z%03d'%(zpos*10),
                    ystep = 0.3,
                    rstep = 0.125,
                    expotime = 0.005 )

def map_step2():
    ACTIVE_MG.enable('mca')
#    for zpos in np.arange( 87.5, 100.01, 0.5):
    for zpos in np.arange( 92.5, 100.01, 0.5):        
        umv( pz, zpos )
        user.half1( 50,
                    'Z%03d'%(zpos*10),
                    ystep = 0.3,
                    rstep = 0.125,
                    expotime = 0.005 )
    for zpos in np.arange( 87., 80, -0.5):
        umv( pz, zpos )
        user.half1( 50,
                    'Z%03d'%(zpos*10),
                    ystep = 0.3,
                    rstep = 0.125,
                    expotime = 0.005 )                
        

def scan_at_voltage( volts ):
    vnow = stress.position
    newdataset('%06dmV'%(int(volts*1e3)))
    ascan( stress, vnow, volts, 60, .1, sleep_time=0.9 )
    loopscan( 60, 0.1, sleep_time=0.9 )    
    finterlaced(diffrz,0, 0.8, 360/0.8, 0.08)


def vmanydemo( ):
    for v in np.arange(-15, 120.1, 15):
        scan_at_voltage( v )
    ascan(stress, 120, -15, 1350, 0.1, sleep_time=0.2)
    
