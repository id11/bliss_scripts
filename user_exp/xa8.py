
def goto_full():
    frelon6.camera.image_mode='FULL FRAME'
    frelon6.image.bin = 1,1
    # frelon6.roi_counters.set('roi1',(0,512,2047,1024))
    frelon6.image.roi=[0,512,2048,1024]
    frelon6.roi_counters.set('roi1',(0,0,2047,1023))
    print(frelon6.camera.image_mode)
    
    
def collect_full(nframes, expotime, sleep_time):
    goto_full()
    loopscan(nframes, expotime, sleep_time=sleep_time)
    
def goto_ftm():
    frelon6.roi_counters.set('roi1',(0,0,2047,1023))
    frelon6.camera.image_mode='FRAME TRANSFER'
    frelon6.image.bin = 1,1
    print(frelon6.camera.image_mode)
    
def collect_ftm(nframes, expotime):
    goto_ftm()
    ftimescan(expotime, nframes)
    
def collect(nframes=10, expotime=0.0321, sleeptime=0):
    if sleeptime==0:
        collect_ftm( nframes, expotime )
    else:
        collect_full( nframes, expotime, sleeptime )
        
def modfrot():
    pos = frot.position
    frot.position = pos % 360
    frot.dial = pos % 360
      
def collect3dxrd(nturns=1, expotime=0.0321, rotstep=1):
    modfrot()
    fscan(frot, 18, rotstep, nturns*360/rotstep, expotime)
    
    
def align_3dxrd():
    sheh1.open()
    plotselect('frelon6:roi_counters:roi1_avg')
    p = frot.position
    # 0, 90, 180, 270
    for o in (0,90,180,270):
        umv(frot, o+p, diffy, -3)
        ascan(diffy, -3, 3, 60, .1)
    umv(diffy,0, frot, p)
    
def height_3d(zmax=20,zmin=10,zstep=2):
    z0 = hz2.position
    for z in np.arange(zmin,zmax+zstep/2,zstep):
        umv(hz2, z)
        collect3dxrd(nturns=1)
    umv(hz2, z0)
    


