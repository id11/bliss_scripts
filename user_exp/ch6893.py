
import time
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def capillary_scan(diffty0, nb_cap):
    diffty_positions = np.arange(0,2.5*nb_cap,2.5)+diffty0
    print(diffty_positions)
    sheh3.open()
    peaks = []
    diffty_airpad.on()
    for i in range(nb_cap):
        #pause_for_refill(60)
        umv(diffty,diffty_positions[i])
        fscan(diffty,diffty_positions[i]-0.5,0.05,20,0.1)
        peaks.append(cen())
        print('Capillary %s is done'%i)
    diffty_airpad.off()
    umv(diffty,diffty0)
    print(peaks)

def rack_scan():
    sheh3.open()
    CeO2_pos = -11.41979
    diffty_vals = [  -8.681216512171815, -6.08505, -3.6884717657851382, -1.08455, 1.31435, 3.7535071858347067, 6.300763930817053, 8.820588548092463]
    sample_names = [
    'ZrO_PIV_053',
    'NU1000_4PrPA_RM_046',
    'NU1000_8PrPA_RM_056',
    'NU1000_12PrPA_RM_057',
    'ZrOAc_4PhPA_H2O_solid2_049B',
    'NU1000_4PhPA_RM_036',
    'NU1000_8PhPA_RM_039',
    'NU1000_12PhPA_RM_040' 
    ]
    compositions = [
    'Zr6',
    'Zr6P4',
    'Zr6P8',
    'Zr6P12',
    'Zr6P4',
    'Zr6P4',
    'Zr6P8',
    'Zr6P12'
    ]
    umv(diffty,CeO2_pos)
    newdataset('CeO2')
    loopscan(60,0.1)
    for i, sample_name in enumerate(sample_names):
        newdataset(sample_name)
        pause_for_refill(60)
        xrpd_processor.pdf_options={'composition':compositions[i]}
        diffty.reset_closed_loop()
        umv(diffty,diffty_vals[i])
        loopscan(900,1)
        print('Capillary %s is done'%i)
    umv(diffty,CeO2_pos)
