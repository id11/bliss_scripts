import numpy as np
import time
from bliss.common import cleanup
#from bliss.setup_globals import *

def goto_tomo():
    moveto_basler()
    umv(s9vg,1.5,s9hg,1.5)
    umv(hlz,-0.3013+1,vly,1.0912-1)
    tfoh1.set(0,'Be')
    
def goto_xrd():
    umv(s9vg,0.05,s9hg,0.05)
    umv(hlz,-0.3013,vly,1.0912)
    tfoh1.set(16+8+4,'Be')
    moveto_eiger()

def goto_cross():
    umv(ntx,0.6811,nty,-0.6764,ntz,-2.7325)

def moveSamplePos(x0, y0, z0, dx, dy, dz, sign_x=1, sign_y=-1, sign_z=1):
    umv(ntx,x0)
    umv(nty,y0)
    umv(ntz,z0) 
    vol_size = [1296,1296,966]
    px_size = 1.05
    z_offset = 92
    x_trans = sign_x*((dx - (vol_size[0]/2.0))*px_size/1000.0)
    y_trans = sign_y*((dy - (vol_size[1]/2.0))*px_size/1000.0)
    z_trans = sign_z*((dz - (vol_size[2]/2.0) + z_offset)*px_size/1000.0) 
    # umv(ntx,x0,nty,y0,ntz,z0)
    umvr(nty, x_trans, ntx, y_trans, ntz, z_trans)

def s3dxrd(samplename,range_x=50, range_z=50,step_x=1,step_z=1):
    z0 = motdac1.position
    newsample(samplename)
    #for i, z_trans in enumerate(np.arange(14, 20)):
    for i,z_trans in enumerate(np.arange(-range_z/2,range_z/2,step_z)):
        print(z0+z_trans)
        umv(motdac1,z0+z_trans)
        #half1(range_x/2,'xrd_%s'%i,yls -step=step_x,rstep=180/((range_x/step_x)*2),expotime=0.0375)
        #half1(range_x/2,'xrd_%s'%(i+14),ystep=step_x,rstep=180/((range_x/step_x)*2),expotime=0.0375)
    umv(motdac1,z0)
    umv(dty, 0)
    
def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def half1(ymax,
          datasetname,
          ystep=0.25,
          ymin=-1801,
          rstep=0.05,
          astart = -90,
          arange = 181,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)

def full1(ymax,
          datasetname,
          ystep=0.25,
          ymin=-1801,
          rstep=0.05,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.full1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.full1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    NR = int(361/rstep)
    umv(dty, max(ypos[0], ymin) )
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=( cleanup.axis.POS, ) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                rstart = next_rstart( rstep, angular_velocity )
                fscan2d( dty, ypos[i], ystep, 1, rot, rstart, rstep, 361/abs(rstep), expotime )
                # 7 min 53s with 25 scans == 19 seconds per scan
                
    umv(dty, 0)

class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            #eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        # eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )
