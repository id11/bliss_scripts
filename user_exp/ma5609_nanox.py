#this is a suggestion for how to acquire the the "stress vs strain" curve with the nanox
#un-comment things in the setup file

# for Ni superalloy, E ~ 180 GPa, a voltage ramprate of 0.01 gives a strain rate around 2e-6 s^-1, calculated by DVC 
# I believe this way it will register the curve in a h5 file, but I would test before launching, never tried myself
# best, pedro

def initialize_stress():
    stress.limits = [-30, 120]
    stress_regul.kp = 1
    stress_regul.output.mode = 'absolute'
    stress_regul.output._limits =  [-30, 120]
    stress_regul.output.ramprate = 0.01
    stress_regul.plot()
    
    
def load_curve(ramprate = 0.05, target_volt = 120):
    ACTIVE_MG.enable('stress*')
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.disable('frelon*')
    newdataset('loading_curve')
    
    initialize_stress()
    
    stress_regul.output.ramprate = ramprate
    stress_regul.output.set_value(target_volt)
    loopscan(4800, 0.5)
    
# after it breaks or finishes:

#initialize_stress()
#umv(stress, 0)
    
