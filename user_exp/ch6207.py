from numpy import sin, cos, radians


def mystart():
    umv(dty, 0)
    umv(px, 50, py, 50, pz, 50)
    umv(rot,0)
    print('Now homing...')
    shtx.home()

def movesx(distance):
    theta = rot.position
    umvr(shtx,distance*cos(radians(theta)),shty,distance*sin(radians(theta)))

def movesy(distance):
    theta = rot.position
    umvr(shtx,distance*sin(radians(theta)),shty,distance*cos(radians(theta)))
    
def runsxscans(dataset_number):
    #tfoh1.set(0,'Be')
    #newdataset('dataset%i'%dataset_number)
    #sxscan(0.1,0.1,180,-180)
    tfoh1.set(12,'Be')
    newdataset('dataset%i_12Be_05sek'%dataset_number)
    sxscan(0.1,0.5,180,-180)
    tfoh1.set(12,'Be')
    newdataset('dataset%i_12Be_01sek'%dataset_number)
    sxscan(0.1,0.1,180,-180)
    #tfoh1.set(0,'Be')
    #sheh3.close()

def fastdataset():
    newdataset('fast_dataset')
    sxscan(0.8,0.8,180,-180)
    
def runlong12Be(dataset_number):
    newdataset('dataset%i'%dataset_number)
    tfoh1.set(12,'Be')
    newdataset('dataset%i_12Be'%dataset_number)
    sxscan(0.1,0.5,180,-180)
    tfoh1.set(0,'Be')


    
