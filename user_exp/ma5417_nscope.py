import time
import numpy as np


def overnight():
    for zpos in [28]:
        umv( pz, zpos )
        newdataset('z%i'%zpos)
        layerscan(-100, 100, 0.15, 0.1, 0.1/14)



def pause_for_refill(t, cpos=6.2908, upos=8.1962):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    if abs(cpm18.position - cpos)>0.02 or abs(cpm18t.position - 0.075)>0.02:
        user.cpm18_goto(cpos)
    if abs(u22.position - upos)>0.02:
        umv(u22, upos)
        
        

def layerscan(y_start, y_end, y_step, rstep, a_time):
    print(np.arange(y_start,y_end,y_step))
    for i,d in enumerate(np.arange(y_start,y_end,y_step)):
        umv(dty, d )
        pause_for_refill(30)
        if i%2 == 0:
            myfscan(rot, 0, rstep, 180/rstep, a_time )
        else:
            myfscan(rot, 180, -rstep, 180/rstep, a_time )        
        

       
def myfscan(*a,**k):
    try:
        fscan(*a,**k)
    except:
        print("Something has gone wrong!!! Retry scans!!")
        elog_print("Retry a scan !!!")
        print("Sleep a minute or two first")
        time.sleep(60)
        pause_for_refill(120)
        fscan(*a,**k) 
        
        
