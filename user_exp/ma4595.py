
#user_script_load("tomo_by_ascan")
#user_script_load("optics")


import numpy as np
import time


#SIMULATE = True
SIMULATE = False

def pause_for_refill(t):
    if SIMULATE:
        return
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)


 
def beforepx_scan(scans=15, count_time=5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_beforepx_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_beforepx_%03d"%( k )  )
        loopscan(images, count_time)

def ebs_beforepx_scan(scans=2, count_time=2.5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_beforepx_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_beforepx_%03d"%( k )  )
        loopscan(images, count_time)

def px_scan(scans=100, count_time=5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_px_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_px_%03d"%( k )  )
        loopscan(images, count_time)

def ebs_px_scan(scans=120, count_time=2.5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_px_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_px_%03d"%( k )  )
        loopscan(images, count_time)


def pxrx_scan(scans=200, count_time=5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_pxrx_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_pxrx_%03d"%( k )  )
        loopscan(images, count_time)

def ebs_pxrx_scan(scans=150, count_time=5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_pxrx_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_pxrx_%03d"%( k )  )
        loopscan(images, count_time)

def afterpxrx_scan(scans=20, count_time=5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_afterpxrx_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_afterpxrx_%03d"%( k )  )
        loopscan(images, count_time)

def afterpx_scan(scans=20, count_time=5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_afterpx_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_afterpx_%03d"%( k )  )
        loopscan(images, count_time)

def ebs_afterpx_scan(scans=10, count_time=2.5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_afterpx_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_afterpx_%03d"%( k )  )
        loopscan(images, count_time)

def relaxed_pxrx_scan(scans=10, count_time=5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_relaxed_pxrx_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_relaxed_pxrx_%03d"%( k )  )
        loopscan(images, count_time)

def relaxed_px_scan(scans=30, count_time=5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_relaxed_px_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_relaxed_px_%03d"%( k )  )
        loopscan(images, count_time)

def ebs_relaxed_px_scan(scans=2, count_time=2.5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_relaxed_px_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_relaxed_px_%03d"%( k )  )
        loopscan(images, count_time)

def ebs_relaxed_pxrx_scan(scans=10, count_time=2.5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_relaxed_pxrx_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_relaxed_pxrx_%03d"%( k )  )
        loopscan(images, count_time)

def test_scan(scans=10, count_time=0.5, images=2): 
    print('predicted time in %f'%(images*count_time*(scans+2)))
    print('taking air measurement')
    umvr(hz,1)
    newdataset('10k_test_air')
    loopscan(images, count_time)
    umvr(hz,-1)
    print('taking 10k measurents')
    for k in range(scans):
        newdataset( "10k_test_%03d"%( k )  )
        loopscan(images, count_time)





