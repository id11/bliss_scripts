#  ... this needs to be saved into ~opid11/bliss_scripts/in1045.py
# Initially setup this macro file by running: user_script_load('in1045.py')

import numpy as np

# Reload the macrofile.
def reload_macros():
    user_script_load('in1045')

diode_pos = 45

# Zero the hexapod stage to absolute positions of 0 in all axes.
def hexapod_zero():
    umv(px, 50, py, 50, pz, 50 )       # piezo state = midrange
    umv(shrx,0, shry, 0, shrz, 0)      # hexapod tilts
    # umv(shtx,0, shty, 0, shtz, 0)    # hexapod translations

# Print the positions of the six axes of the hexapod
def hexapod_where():
    wm(px, py, pz)
    wm(shtx, shty, shtz, shrx, shry, shrz)

# Print the positions of the four axes of the table
def table_where():
    wm(shtx, shty, shtz, rot)

# Acquire long darkfield series
def acquire_darkfield(nframes=50, exptime=2):
   # Acquires 50 dark field images for 2s each
   #  ... not generally needed for the eiger detector
   sheh3.close()
   loopscan( nframes, exptime )

# Automatic coarse alignment in ry
def auto_ry_coarse():
    # could be just a2scan( ... )
    AngleStart = -1.      #Degrees
    AngleEnd = 1.01       #Degrees
    AngleIncrement = 0.2; #Degrees
    for cAngle in np.arange( AngleStart, AngleEnd, AngleIncrement ):
        umv(shry, cAngle)
        displacementscan_coarse()

# Coarse z-axis scan using tz  :
def displacementscan_coarse():
    Increments = 20
    Start = 30
    Finish = 0
    CountTime = 2
    Step = (Finish - Start) * Increments
    ascan( shtz, Start, Finish, Step, CountTime)

# Automatic fine alignment in ry
def auto_ry_fine():
    # could be just a2scan( ... )
    AngleStart = -1 #Degrees
    AngleEnd = 1.01 #Degrees
    AngleIncrement = 0.2; #Degrees
    for cAngle in np.arange( AngleStart, AngleEnd, AngleIncrement ):
        umv(shry, cAngle)
        displacementscan_fine()

# Fine z-axis scan using pz
def displacementscan_fine():
    Increments = 5
    Start = 48 #microns
    Finish = 100 #microns
    CountTime = 2 #seconds
    Step = (Finish - Start) * Increments
    ascan(pz, Start, Finish, Step, CountTime)

# Acquire a scan series from 'Start' to 'Finish' in 'Steps' increments
# +1 is not needed on the Steps line, as it is done automatically
def acquiredscan(exptime=2):
    Start =48 #Microns
    Finish = 56 #Microns
    Steps = (Finish - Start) * 20
    print("Estimated time",Steps * exptime )
    for ypos in (-100,0,100,200,300):
        umv(dty, ypos)
        newdataset('zscan_%d'%(ypos) )
        ascan( pz, Start, Finish, Steps, exptime)
    umv(dty, 0)


# Acquire a scan series from 'Start' to 'Finish' in 'Steps' increments
# +1 is not needed on the Steps line, as it is done automatically
def acquiredscan180(exptime=2):
    Start =45 #Microns
    Finish = 96 #Microns
    Steps = (Finish - Start) * 20
    print("Estimated time",Steps * exptime )
    for rpos in (90,-90):
        umv(rot, rpos)
        newdataset('zscan_%d'%(rpos) )
        ascan( pz, Start, Finish, Steps, exptime)
    umv(rot, 90)


# Acquire a scan series from 'Start' to 'Finish' in 'Steps' increments
# +1 is not needed on the Steps line, as it is done automatically
def acquiredscanwithrot(exptime=1):
    Start =55 #Microns
    Finish = 90 #Microns
    stepsize = 0.2
    Steps = (Finish - Start) /stepsize
    print("Estimated time",Steps * exptime )
    for ypos in (100,150,200):
        umv(dty, ypos)
        newdataset('zscan_80_100_%d'%(ypos) )
        # ascan( pz, Start, Finish, Steps, exptime)
        fscan2d( pz, Start, stepsize, Steps,
                 rot, 80, 10, 2, exptime)
        #           starta, stepsize, nspteps, dt
        
    umv(dty, 0)
    


# As above, but with two independent ranges, e.g. varying step sizes
def acquiredscan_metalincrement(exptime=10):
   Start = 55
   Finish1 = 90
   Finish2 = 100
   Steps = (Finish - Start) * 20
   ascan( pz, Start, Finish, Steps, exptime)
   Steps2 = (Finish2 - Finish1) * 10
   ascan( pz, Finish1, Finish2, Steps2, exptime)

# % Home-made accumulation mode, displaces between counting frames
def SimpleAccumulation():
    for zscan in np.arange(20, 22.01, 0.5):
        umv(pz, zscan)
        CountManyOnes()

# % Home-made accumulation mode, counts a series of frames but doesn't sum.
def CountManyOnes(nframes=5, exptime=1):
    loopscan(nframes, exptime)



def measure_ry(zrange=15):
    plotselect('eiger:roi_counters:roi2_sum')
    pos = []
    nsteps = int(np.round(zrange*2/0.3))
    for ypos in (-300,0,300):
        umv( dty, ypos )
        dscan(pz, -zrange, zrange, nsteps,0.1)
        pos.append(cen())
    umv(dty,0)
    
    fit = np.polyfit([-300,0,300], pos, 1 )
    print('shry shift',np.degrees(fit[0])*np.sign(np.sin(np.radians(rot.position))))
    
        
def measure_rx(rxrange=0.2, rxstep=0.05, zrange=10):
    rx0 = shrx.position
    z0 = shtz.position
    try:
        pos = []
        xpos =  np.arange( -rxrange, +rxrange+0.001, rxstep )
        nsteps = int(np.round(zrange*2/0.1))
        for rx in xpos:
            umv( shrx, rx0 + rx, shtz, z0 + np.radians( rx )*shty.position )
            dscan(pz, -zrange, zrange, nsteps, .1)
            pos.append(cen())

    finally:
        umv( shrx, rx0, shtz, z0 )



