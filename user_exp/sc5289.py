import time
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)



def rack1_scan():
    dset_names = ['BAY030',
                  'BAY032',
                  'ODE',
                  'Empty'#,
#                  '5',
#                  '6',
#                  '7',
#                  '8',
#                  '9',
#                  '10',
#                  '11',
#                  '12',
#                  '13',
#                  '14',
#                  '15',
#                  '16',
#                  '17',
#                  '18',
#                  '19',
#                  '20'
                 ]
    diffy_pos = [1.92,4.00,6.69,9.07]
    #diffy_start = 2
    #step_size = 2.5
    newsample('rack1')
    sheh1.open()
    for i,dset in enumerate(dset_names):
        print('Measuring %s'%dset)
        newdataset(dset)
        umv(diffy,diffy_pos[i])
        pause_for_refill(60)
        loopscan(300,2)
        
def rack2_scan():
    dset_names = ['AH01',
                  'BW121',
                  'AH02',
                  'GL175_5',
                  'toluene',
                  'hexane',
                  'BW124_1',
                  'AH04',
                  'sol_GL116',
                  'sol_GL171_e',
                  'sol_GL175_178',
                  'GL116',
                  'GL171_e',
                  'BW126',
                  'LG250',
                  'LG251',
                  'LG252',
                  'LG248',
                  'LG249_1',
                  'LG246_6'
                 ]
    diffy_pos = [1.64,
                 4.18,
                 6.60,
                 9.03,
                 11.61,
                 13.81,
                 16.56,
                 18.97,
                 21.51,
                 24.18,
                 26.64,
                 28.88,
                 31.56,
                 33.80,
                 36.63,
                 38.91,
                 41.36,
                 43.9,
                 46.52,
                 49.01
                 ]
    sheh1.open()
    for i,dset in enumerate(dset_names):
        print('Measuring %s'%dset)
        newdataset(dset)
        umv(diffy,diffy_pos[i])
        pause_for_refill(60)
        loopscan(300,2)
        
        
def rack3_scan():
    dset_names = [
                  'BW177',
                  'BW177_10',
                  'BW177_15',
                  'BW177_20',
                  'BW177_25',
                  'BW172_5',
                  'BW172_10',
                  'BW172_15',
                  'BW172_20',
                  'BW172_25'
                 ]
    diffy_pos = [1.88,
                 6.81,
                 11.83,
                 16.63,
                 21.66,
                 26.67,
                 31.63,
                 36.55,
                 41.57,
                 46.65
                 ]
    sheh1.open()
    for i,dset in enumerate(dset_names):
        print('Measuring %s'%dset)
        newdataset(dset)
        umv(diffy,diffy_pos[i])
        pause_for_refill(60)
        loopscan(300,2)
    sheh1.close()
    
def rack4_scan(): #Seems like GL178_1 got stuck, I restarted the macro at GL178_2. POA. ETA 23:00
    dset_names = [#'BW167_549',
                  #'BW126',
                  #'AH05',
                  #'AH06',
                  #'AH07',
                  #'AH03',
                  #'AH21',
                  #'LG249',
                  #'LG249_2',
                  #'GL178_1',
                  'GL178_2',
                  'GL178_3',
                  'GL178_4',
                  'GL178_5',
                  'GL178_6',
                  'LG230_conc',
                  'LG252_QBconc',
                  'LG251_conc',
                  'AH_08',
                  'AH09'
                 ]
    diffy_pos = [#2.08,
                 #4.65,
                 #7.06,
                 #9.63,
                 #12.04,
                 #14.44,
                 #16.91,
                 #19.50,
                 #22.12,
                 #24.62,
                 27.00,
                 29.49,
                 31.87,
                 34.44,
                 36.96,
                 39.69,
                 42.04,
                 44.55,
                 47.07,
                 49.56
                 ]
    sheh1.open()
    for i,dset in enumerate(dset_names):
        print('Measuring %s'%dset)
        newdataset(dset)
        umv(diffy,diffy_pos[i])
        pause_for_refill(60)
        loopscan(300,2)
    sheh1.close()
    
def linkam_scan1():
    sheh1.open()
    newdataset('loopscan')
    
def rack5_scan(): 
    dset_names = ['AH10',
                  'AH11',
                  'AH16',
                  'AH08bis',
                  'AH09bis',
                  'AH13',
                  'AH14',
                  'AH15',
                  'LG246_1_C4',
                  'BAY031',
                  'BAY033',
                  'BAY034',
                  'AH18',
                  'AH12',
                  'CeO2'
                 ]
    diffy_pos = [1.82,
                 4.37,
                 6.87,
                 9.32,
                 11.64,
                 14.23,
                 16.62,
                 19.17,
                 24.26,
                 26.71,
                 31.73,
                 36.83,
                 41.77,
                 46.8,
                 49.23
                 ]
    sheh1.open()
    for i,dset in enumerate(dset_names):
        print('Measuring %s'%dset)
        newdataset(dset)
        umv(diffy,diffy_pos[i])
        pause_for_refill(60)
        loopscan(300,2)
    sheh1.close()
    
