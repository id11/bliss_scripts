import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime
from bliss.common import cleanup


def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)



tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
attrz = config.get('attrz')
atty = config.get('atty')

# sample dct alignment position, to be updated for each new sample

#def define_sam_dct_pos():
#    dct_pos = {}
#    # dct_pos['samtx_dct'] = -0.281
#    # dct_pos['samty_dct'] = 2.192
#    # dct_pos['samrx_dct'] = 4.6
#    # dct_pos['samry_dct'] = 1.7
#    dct_pos['samtx'] = -0.47386
#   dct_pos['samty'] = 3.30416
#   dct_pos['samrx'] = 6.1998
#   dct_pos['samry'] = 1.9998
#   return dct_pos


# sample positions during annealing (should be constant for all samples...)
anneal_pos = {}
anneal_pos['samtx'] = 0.968
anneal_pos['samty'] = -1.103
anneal_pos['samrx'] = 0
anneal_pos['samry'] = 0

# Value for 3 mm from top of specimen
#     for a1: -44.34
#     for a2: -42.97 (from cycle 0 to cycle 8)
#     for a2: -43.495 (from cycle 9 onwards)
#     for olaf: -40.895
difftz_dct = -41.235
# 
# # from sample a2
# orig_anneal_pos = -42.97

furnace_out = 107


####  Positioning  ####

def update_sam_pos():
    sam_pos = {}
    sam_pos['samtx'] = samtx.position
    sam_pos['samty'] = samty.position
    sam_pos['samrx'] = samrx.position
    sam_pos['samry'] = samry.position
    return sam_pos

def read_positions():
    g = {}   
    g['samrx']=samrx.position
    g['samry']=samry.position
    g['samtx']=samtx.position
    g['samty']=samty.position
    g['samtz']=samtz.position
    g['d2tz']=d2tz.position
    return g

def goto_sam_pos(pos):
    umv(samrx, pos['samrx'], samry, pos['samry'])
    umv(samtx, pos['samtx'], samty, pos['samty'])

def switch_to_exchange(anneal_pos):
    umv(furnace_z, furnace_out)
    assert furnace_z.position > 100
    umv(nfdtx, 158)
    goto_sam_pos(anneal_pos)

def switch_to_furnace(time, dct_pos, anneal_pos, cooltime = 300):
    umv(nfdtx, 158)
    goto_sam_pos(anneal_pos)
    umv(furnace_z, 0)
    print(f'annealing now for {time} seconds ...')
    sleep(time)
    umv(furnace_z, furnace_out)
    goto_sam_pos(dct_pos)
    print(f'waiting {cooltime} seconds for sample to cool down...')
    sleep(cooltime)
    print('ready to continue...')

def put_furnace(anneal_pos):
    umv(nfdtx, 158)
    goto_sam_pos(anneal_pos)
    # umv(furnace_z, 0) window wasnt lined up
    umv(furnace_z, 0)
    print('sample is inside furnace  and can be observed with high-res detector...')

def switch_to_ff(ff_pars, dct_pos):
    umv(furnace_z, furnace_out)
    assert furnace_z.position > 100
    goto_sam_pos(dct_pos)
    umv(d1ty, -100, d3ty, 180)
    umv(ffdtx1, 180, ffdtz1, 0)
    umv(atty, 0, attrz, -7)
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable('frelon16*')
    ACTIVE_MG.enable("frelon3*") 
    umvct(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umvct(s8vg, ff_pars['slit_vg'] + 0.05, s8hg, ff_pars['slit_hg'] + 0.05)
    sct(ff_pars['exp_time'])

def switch_to_dct_old(dct_pars, dct_pos):
    umv(furnace_z, furnace_out)
    assert furnace_z.position > 100
    goto_sam_pos(dct_pos)
    umv(d1ty, 0)
    bsin()
    tfoh1.set(0,'Al')
    tfoh1.set(8,'Be')
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.enable("frelon16:image") 
    umvct(s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umvct(s8vg, dct_pars['slit_vg'] + 0.05, s8hg, dct_pars['slit_hg'] + 0.05)
    sct(dct_pars['exp_time'])

def switch_to_dct(dct_pars, dct_pos):
    centeredroi(marana, 2048, 2048)
    umv(furnace_z, furnace_out)
    assert furnace_z.position > 100
    goto_sam_pos(dct_pos)
    umv(d3_bsty, 0)
    umv(nfdtx, 8)
    tfoh1.set(0,'Al')
    tfoh1.set(8,'Be')
    umv(s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umv(s8vg, dct_pars['slit_vg'] + 0.05, s8hg, dct_pars['slit_hg'] + 0.05)
    sct(dct_pars['exp_time'])

def switch_to_abs_old(abs_pars, dct_pos):
    umv(furnace_z, furnace_out)
    assert furnace_z.position > 100
    goto_sam_pos(dct_pos)
    umv(d1ty, 0)
    bsout()
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.enable("frelon16:image") 
    umv(s7vg, abs_pars['slit_vg'], s7hg, abs_pars['slit_hg'])
    umv(s8vg, abs_pars['slit_vg'] + 0.05, s8hg, abs_pars['slit_hg'] + 0.05)
    sct(abs_pars['exp_time'])
    
def switch_to_abs(abs_pars, dct_pos):
    centeredroi(marana, 2048, 2048)
    umv(furnace_z, furnace_out)
    goto_sam_pos(dct_pos)
    umvct(d3_bsty, 2000)
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    umv(s7vg, abs_pars['slit_vg'], s7hg, abs_pars['slit_hg'])
    umv(s8vg, abs_pars['slit_vg'] + 0.05, s8hg, abs_pars['slit_hg'] + 0.05)
    sct(abs_pars['exp_time'])

def switch_to_pct_old(pct_pars, dct_pos):
    umv(furnace_z, furnace_out)
    umv(d1ty, -100, d3ty, 0, d3tz, 0)
    assert furnace_z.position > 100
    goto_sam_pos(dct_pos)
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    ACTIVE_MG.enable("marana:image")
    ACTIVE_MG.disable("frelon16*") 
    umv(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'])
    umv(s8vg, pct_pars['slit_vg'] + 0.05, s8hg, pct_pars['slit_hg'] + 0.05)
    print("ready to collect pct data") 
    sheh3.open()
    sct(pct_pars['exp_time'])

def switch_to_pct(pct_pars, dct_pos):
    centeredroi(marana, 800, 800)
    umv(furnace_z, furnace_out)
    umv(nfdtx, 158)
    umv(d3_bsty, 2000)
    goto_sam_pos(dct_pos)
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    umv(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'])
    umv(s8vg, pct_pars['slit_vg'] + 0.05, s8hg, pct_pars['slit_hg'] + 0.05)
    print("ready to collect pct data") 
    sheh3.open()
    sct(pct_pars['exp_time'])


####  Beam stuff  ####

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e4):
    c = ct(1,p201_20).get_data()['fpico3']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_20).get_data()['fpico3']

def frelon16setCenteredRoi(roi_width=1920, roi_height=1920):
    frelon16.image.roi = [961 - int((roi_width+1)/2), 961 - int((roi_height+1)/2), roi_width, roi_height]
    print(frelon16.image.roi)



# -*- coding: utf-8 -*-
import os
import numpy as np

#def update_all_pars():
#    pct_pars = define_pct_pars()
#    dct_pars = define_dct_pars()
#    abs_pars = define_abs_pars()

def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = 0
    pct_pars['step_size'] = 0.2
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.03
    pct_pars['refon'] = pct_pars['num_proj']
#    pct_pars['ref_int'] = 10
    pct_pars['ref_step'] = -1
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 100
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = difftz
    pct_pars['samtz_cen'] = difftz_dct
    pct_pars['shift_step_size'] = 1
    pct_pars['nof_shifts'] = 1
    pct_pars['slit_hg'] = 1.6
    pct_pars['slit_vg'] = 2
    pct_pars['dist'] = 158
    pct_pars['scan_type'] = 'fscan'
    return pct_pars

def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.05
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.05
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = 1
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.9
    # dct_pars['slit_vg'] = 0.38
    dct_pars['slit_vg'] = 0.15
    dct_pars['dist'] = 8
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = difftz
    dct_pars['samtz_cen'] = difftz_dct
    # dct_pars['shift_step_size'] = 0.35
    dct_pars['shift_step_size'] = 0.12
    # dct_pars['nof_shifts'] = 5 # for 5 layers of dct
    dct_pars['nof_shifts'] = 6
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars

def define_abs_pars():
    abs_pars={}
    abs_pars['start_pos'] = 0
    abs_pars['step_size'] = 0.4
    abs_pars['num_proj'] = 360 / abs_pars['step_size']  
    abs_pars['exp_time'] = 0.025
    abs_pars['refon'] = abs_pars['num_proj']  
    abs_pars['ref_step'] = 1
    abs_pars['scan_mode'] = 'CAMERA'
    abs_pars['nref'] = 41
    abs_pars['slit_hg'] = 1.2
    abs_pars['slit_vg'] = 2
    abs_pars['dist'] = 8
    abs_pars['ref_mot'] = samy
    abs_pars['zmot'] = difftz
    abs_pars['samtz_cen'] = difftz_dct
    abs_pars['shift_step_size'] = 0.35
    abs_pars['nof_shifts'] = 1
    abs_pars['scan_type'] = 'fscan'
    abs_pars['samrx_range'] = 3
    abs_pars['samry_range'] = 3
    abs_pars['num_tiltx'] = 3
    abs_pars['num_tilty'] = 3
    return abs_pars


def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")


def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])
    
def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.2, s8hg, pars['slit_hg'] + 0.2)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    ct(exp_time)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    #ftimescan(exp_time, nref, scan_mode=scanmode)
    fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)

def ref_scan_samy(exp_time, nref, ystep, omega, scanmode):
    umvr(samy, ystep)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umvr(samy, -ystep)
    
def ref_scan_difftz(exp_time, nref, zstep, omega, scanmode):
    difftz0 = difftz.position
    umv(difftz, difftz0 + zstep)
    print("fscan(diffrz, %6.2f, 0.1, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.1, nref, exp_time, scan_mode = scanmode)
    umv(difftz, difftz0)
    
def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')
 
def ftomo_zseries(pct_pars, datasetname = 'pct'):
    shift_step_size = abs(pct_pars['shift_step_size'])
    if(pct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = pct_pars['samtz_cen'] - (pct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(pct_pars['samtz_cen']))
    if(offset_samtz_pos < pct_pars['zmot'].low_limit or offset_samtz_pos + (pct_pars['nof_shifts'] - 1) * shift_step_size > pct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(pct_pars['nof_shifts'])):
        umv(pct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        fulltomo.full_turn_scan(datasetname)
    umv(pct_pars['zmot'], pct_pars['samtz_cen'])
    print('PCT Succeed')
    return('Succeed')   

def tomo_series_cst_load(start, num_scans, target, sleep_time=0, pct_pars=None):
    for i in np.arange(start, start+num_scans):
        tomo.full_turn_scan(str(i))
        load_ramp_by_target(target, 0.05, 1)
	#dct_marana_dict(str(i), pct_pars)
        sleep(sleep_time)
    return

def ftomo_series(scanname, start, num_scans, sleep_time=0, pars=None):
    for i in np.arange(start, start+num_scans):
        newdataset(scanname + str(i))
        umv(diffrz, pars['start_pos']);
        fsh.disable()
        fsh.close()
        print("taking dark images")
        ftimescan(pars['exp_time'], pars['nref'],0)
        fsh.enable()
        if not(i%pars['ref_int']):
            print("taking flat images")
            ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'], pars['scan_mode'])
        else:
            print("skipping flat images - stay where we are")
            ftimescan(pars['exp_time'], 10,0)
        print("taking projections...")
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        print("resetting diffrz to 0")
        umv(shehe, pars['start_pos']+360);
        diffrz.position=pars['start_pos'];
        diffrz.dial = diffrz.position
        sleep(sleep_time)
    return
    
 
def update_flat():
    image_corr.take_dark()
    image_corr.take_flat()
    image_corr.dark_on()
    image_corr.flat_on()

def flat_on():
    image_corr.dark_on()
    image_corr.flat_on()

def flat_off():
    image_corr.dark_off()
    image_corr.flat_off()
    
##############DEFINED BY PEDRO IN 02/09/2022###################

def acquire_pca(dset_name, ndarks = 500, nflats = 1000, exp_time = 0.04):

    newdataset(dset_name)
    umvr(samy, -4)
    sheh3.open()
    #take flats
    ftimescan(exp_time, nflats)
    sheh3.close()
    #take darks
    ftimescan(exp_time, ndarks)
    umvr(samy, 4)
    sheh3.open()
    ct(exp_time)
    
def acquire_pca_tseries(dset_name, nacq = 10, sleep_time = 60, ndarks = 500, nflats = 1000, exp_time = 0.04):

    newdataset(dset_name)
    
    for _ in range(nacq):
		
	    sheh3.open()
	#take flats
	    ftimescan(exp_time, nflats)
	    sheh3.close()
	#take darks
	    ftimescan(exp_time, ndarks)
	    sleep(sleep_time)
    
    sheh3.open()
    ct(exp_time)
    

def clicked_pos(x,y):
     c2mplot = flint().get_live_plot('default-curve')
     print("Select position in flint")
     pos = c2mplot.select_points(1)
     return pos[0][0]



# # # SCANNING MACROS # # #

def meas_cycle(pct_pars, dct_pars, abs_pars, dct_pos, step):
    sheh3.open()
    # NO ANNEALING
    print('skipping annealing ...')
    print('switch to phase contrast configuration')
    switch_to_pct(pct_pars, dct_pos)
    print('collecting phase contrast scan')
    ftomo_zseries(pct_pars, f'pct_step{step:02}_')
    print('switching to dct configuration')
    switch_to_dct(dct_pars, dct_pos)
    print('acquiring dct scans...')
    dct_zseries(dct_pars, f'dct_step{step:02}_')
    print('acquiring absorption scan...')
    switch_to_abs(abs_pars, dct_pos)
    dct_zseries(abs_pars, f'abs_step{step:02}_')
    print('measurement cycle without annealing finished...')


def anneal_cycle(pct_pars, dct_pars, abs_pars, dct_pos, anneal_pos, time, step, Temp = 530, tol=5, cooltime = 300):
    sheh3.open()
    nanodac3.setpoint = Temp
    while np.abs(nanodac3_temp.read() - Temp) > tol:
        print("waiting for furnace to reach setpoint")
        sleep(5*60)
    print(f"Anneal for {time} seconds")
    # umv(difftz, orig_anneal_pos)
    switch_to_furnace(time, dct_pos, anneal_pos, cooltime)
    print('switch to phase contrast configuration')
    switch_to_pct(pct_pars, dct_pos)
    print('collecting phase contrast scan')
    ftomo_zseries(pct_pars, f'pct_step{step:02}_')
    print('switching to dct configuration')
    switch_to_dct(dct_pars, dct_pos)
    print('acquiring dct scans...')
    dct_zseries(dct_pars, f'dct_step{step:02}_')
    print('acquiring absorption scan...')
    switch_to_abs(abs_pars, dct_pos)
    dct_zseries(abs_pars, f'abs_step{step:02}_')
    print('annealing cycle finished...')


def do_cycles(pct_pars, dct_pars, abs_pars, dct_pos, anneal_pos, num_cycles, start_cycle, time = 10*60):
    sheh3.open()
    for ii in np.arange(start_cycle, start_cycle + num_cycles):
        print(f'\nNow runing cycle {ii} ...')
        anneal_cycle(pct_pars, dct_pars, abs_pars, dct_pos, anneal_pos, time, ii)
    sheh3.close()


def tomo_series(step, start, num_scans, sleep_time=0):
    for i in np.arange(start, start + num_scans):
        fulltomo.full_turn_scan(f'pct_step{step:02}_insitu{i}') 
        sleep(sleep_time)
    return        
            
            
    


# # # OLD METHODS # # #

def rex_cycle(pct_pars, dct_pars, abs_pars, dct_pos, anneal_pos, time, step, Temp = 400, tol=5, cooltime = 300):
    sheh3.open()
    nanodac3.setpoint = Temp
    while np.abs(nanodac3_temp.read() - Temp) > tol:
        print("waiting for furnace to reach setpoint")
        sleep(5*60)
    print(f"Recrystallize for {time} seconds")
    switch_to_pct(pct_pars, anneal_pos)
    put_furnace(anneal_pos)
    tomo_series(step, 1, 5)
    print('removing furnace and cooling...')
    umv(furnace_z,furnace_out)
    sleep(60*5)
    print('switch to phase contrast configuration')
    switch_to_pct(pct_pars, dct_pos)
    print('collecting phase contrast scan')
    ftomo_zseries(pct_pars, f'pct_step{step:02}_')
    print('switching to dct configuration')
    switch_to_dct(dct_pars, dct_pos)
    print('acquiring dct scans...')
    dct_zseries(dct_pars, f'dct_step{step:02}_')
    print('acquiring absorption scan...')
    switch_to_abs(abs_pars, dct_pos)
    dct_zseries(abs_pars, f'abs_step{step:02}_')
    print('annealing cycle finished...')



