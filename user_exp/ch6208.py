
import numpy as np
import time
def check_cpm18(pos=10.1208):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03:
        user.cpm18_goto(pos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()

def myfscan2d(*a,**k):
    try:
        print(a,k)
        fscan2d(*a,**k)
    except:
        print("Scan failed, sleep then retry")
        time.sleep(60)
        fscan2d(*a,**k)        
        

def layer1():
    # quick overview (check scan range)
    fscan2d( dty, -64.5, 1.5, 87, rot, -90.5, 0.125, 1448, 0.005 )
    
def full1(ymax, datasetname):    
    newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ystep = 0.15
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = 0
    while i < len(ypos):
        umv(dty, ypos[i])
        pause_for_refill( 13 )
        if (i % 2) == 0: # forwards
            fscan( rot, -180.5,  0.125, 1448, 0.005 )
        else:
            fscan( rot,    0.5, -0.125, 1448, 0.005 )
        i += 1

        
#    fscan2d( dty, -63.9, 0.15, 855, rot, -90.5, 0.125, 1448, 0.005 )


def full1(ymax, datasetname):    
    newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ystep = 0.15
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = 0
    while i < len(ypos):
        pause_for_refill( 13 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, -180.5,  0.125, 1448, 0.005 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot,    0.5, -0.125, 1448, 0.005 )
        i += 1

def layers(zvals, ymax):
    for z in zvals:
        umv(pz, z)
        full1( ymax, 'z'+str(z) )
    # do newsample before
    # call as user.layers( [ 54. 50, 60, 80],  38.0 )
    # 





