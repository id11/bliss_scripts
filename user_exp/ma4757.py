import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime

def test_loop_grains(grain_list):
    """Loops through a list of Topotomo grains  and performs refinement + TT scan_acquisition
    :param dict grain_list:  list of grain structures containing Topotomo alignment values
    :param dict tt_pars: Topotomo scan parameters
    :param int step: running number of load step in load-sequence  
    """
    for i in range(len(grain_list)):
        grain_list[i]['toto']=0
    return(grain_list)
    



def read_tt_infos(gid):
    data_dir = '/data/visitor/ma4757/id11/DCT_Analysis/A_dct_3N__0002/topotomo'
    data_path = os.path.join(data_dir, 'grain_%04d.mat' % gid)
    data = loadmat(data_path)
    t = data['out'][0][0][0][0]
    values = t[0][0][0][0][0]
    d = {}
    d['gr_id'] = values[0][0][0]
    # double check that the grain id is right
    if d['gr_id'] != gid:
        raise(ValueError('grain id mismatch, check your matlab output in file %s' % data_path))
    d['nfdtx'] = values[1][0][0]
    d['d3tz'] = values[2][0][0]
    d['diffry'] = values[3][0][0]
    d['samrx'] = values[4][0][0]
    d['samry'] = values[5][0][0]
    d['samtx'] = values[6][0][0]
    d['samty'] = values[7][0][0]
    d['samtz'] = values[8][0][0]
    d['samrx_offset'] = values[9][0][0]
    d['samry_offset'] = values[10][0][0]
    d['samtx_offset'] = values[11][0][0]
    d['samty_offset'] = values[12][0][0]
    d['samtz_offset'] = values[13][0][0]
    d['diffrz_offset'] = values[14][0][0]
    d['int_factor'] = values[15][0][0]
    print(d)
    return d
    
ly = config.get('ly')
lfyaw = config.get('lfyaw')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
dct_dist = 6
tt_dist = 20
abs_dist = 70
ff_offset = 525
tt_offset = -36.15
d3_out = 200
d2_out = -80
samrx_offset = 0  
samry_offset = 1.6
samtz_offset = 0  

def sam_dct_pos():
    umv(samrx, samrx_offset, samry, samry_offset, diffry, 0)
    umv(samtx, 0, samty, 0, samtz, 0)

def lyin():
    umv(ly,-10,lfyaw,10)

def lyout():
    umv(ly, 0, lfyaw, 0.1)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']

def maranain(dist):
    ff_offset = 525;
    lyout();
    ACTIVE_MG.enable("marana:image") 
    ACTIVE_MG.disable("frelon16*")
    sam_dct_pos()
    umv(ffdtx1, dist + ff_offset, nfdtx, dist)
    umv(d2ty, d2_out, d3ty, 0)
    print("ready to collect marana data") 

def marana_abs():
    tfoh1.set(64,'Al')
    tfoh1.set(0,'Be')
    for i in range (3):
        umv(tfz, -0.46, tfy, 14.488)
    marana.image.roi=[512,0,1024,2048]
    sct(0.05)
    umvct(s8vg, 3.5, s8hg, 2, nfdtx, abs_dist, ffdtx1, abs_dist + ff_offset, d3_bstz, 2000)
    umvct(samtz, 0)

def marana_dct(dct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    for i in range (3):
        umv(tfz, -0.32, tfy, 14.445)
    marana.image.roi=[0,0,2048,2048]
    sct(dct_pars['exp_time'])
    umvct(s8vg, dct_pars['slit_vg'], s8hg, dct_pars['slit_hg'], d3_bstz, 0)
    umvct(nfdtx, dct_pars['dist'], ffdtx1, dct_pars['dist'] + ff_offset)
    umv(samtz, samtz_offset)

def frelon16in(theta):
    tfoh1.set(0,'Al')
    tfoh1.set(18,'Be')
    for i in range (3):
        umv(tfz, -0.39, tfy, 14.51)
    lyout();r
    ACTIVE_MG.disable("marana:image") 
    ACTIVE_MG.enable("frelon16:image")
    d2tzpos = np.tan(2*np.deg2rad(theta))*tt_dist
    print("moving d2tz to target position %g"%d2tzpos)
    umvct(d2tz, d2tzpos)
    umv(diffrz, 0)
    umv(d2ty, 0, d3ty, d3_out)
    umv(ffdtx1, tt_dist + tt_offset + ff_offset, nfdtx, tt_dist + tt_offset)
    print("ready to collect topotomo data")


def read_positions():
    g = {}   
    g['samrx']=samrx.position
    g['samry']=samry.position
    g['samtx']=samtx.position
    g['samty']=samty.position
    g['samtz']=samtz.position
    g['d2tz']=d2tz.position
    return g

def topotomo_tilt_grain(gr_id):
    """drives diffractometer to grain alignment position for
    """
    topotomo_params = read_tt_infos(gr_id)
    print(topotomo_params)
    if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
        return('Error')
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d2tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')

def topotomo_tilt_grain_json(gr_id):
    """drives diffractometer to grain alignment position for
    """
    topotomo_params = get_json_grain_info(gr_id)
    print(topotomo_params)
    if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
        return('Error')
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d2tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')
    
def topotomo_tilt_grain_dict(topotomo_params):
    """drives diffractometer to current grain alignment position
    """
    print(topotomo_params)
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d2tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')

# -*- coding: utf-8 -*-
import os
import numpy as np



def define_tt_pars():
    tt_pars={}
    tt_pars['diffrz_step'] = 5
    tt_pars['num_proj'] = 360 / tt_pars['diffrz_step']
    tt_pars['diffry_step'] = 0.04
    tt_pars['exp_time'] = 0.05
    tt_pars['search_range'] = 0.4
    tt_pars['scan_mode'] = 'TIME'
    tt_pars['image_roi'] = [510, 510, 900, 900]
    tt_pars['counter_roi'] = [300, 300, 300, 300]
    tt_pars['slit_hg'] = 0.3
    tt_pars['slit_vg'] = 0.3
    return tt_pars
    
def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.05
    dct_pars['num_proj'] = 360 / dct_pars['step_size']
    dct_pars['exp_time'] = 0.1
    dct_pars['refon'] = dct_pars['num_proj']
    dct_pars['ref_step'] = -1
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.68
    dct_pars['slit_vg'] = 0.3
    dct_pars['dist'] = 6
    dct_pars['ref_mot'] = samy
    return dct_pars

def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = 0
    pct_pars['step_size'] = 0.3
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.05
    pct_pars['refon'] = pct_pars['num_proj']
    pct_pars['ref_step'] = -0.6
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 41
    pct_pars['ref_mot'] = samy
    return pct_pars

def load_grains_from_matlab(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = read_tt_infos(gr_id)
        grain_list.append(grain)
    return grain_list
    
def load_grains_from_json(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = get_json_grain_info(gr_id)
        grain_list.append(grain)
    return grain_list

def load_ramp_by_ascan(start, stop, npoints, exp_time, loadstep):
    marana_abs()
    newdataset('loadramp_%d' %  loadstep)
    ascan(stress, start, stop, npoints, exp_time, stress_cnt, marana)
    return

def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def load_ramp_by_target(target, exp_time, loadstep, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    write_to_file = to_file if to_file is not None else sys.stdout
    #marana_abs()
    current_load = stress_cnt.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return


def define_pars(load_list):
    """Produces default input parameters for a load sequence (see next function)
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    for load in load_list:
        step_pars['dct_pars'] = define_dct_pars()
        step_pars['pct_pars'] = define_pct_pars()
        step_pars['tt_pars'] = define_tt_pars()
        step_pars['target'] = load
        step_pars['load_step'] = step
        step = step + 1
        par_list=[par_list, step_pars]
    return par_list


def load_sequence(par_list, grain_list):
    """Load Experiment Master Sequence.
    
    This function will perform scan sequences (pct, dct, tt) for a list of target load values defined in "par_list" 
    Note: grain_list is currently produced by a matlab script and can be re-created / imported via:  grain_list = user.read_tt_info([list_of_grain_ids])
    par_list can be created via: par_list = user.define_pars()  and sub-parameters for dct, pct, tt can be adapted to cope with increasing mosaicity

    """
    for step_pars in par_list:
        dct_pars = step_pars['dct_pars']
        pct_pars = step_pars['pct_pars']
        tt_pars = step_pars['tt_pars']
        target = step_pars['target']
        load_step = step_pars['load_step']
        grain_list = load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, target, load_step)
    return


def load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, target, loadstep):
    """Performs a loadramp to the new target value and launches PCT, DCT and a series of TT scans at this new target load
    """
    maranain(abs_dist)
    marana_abs()
    load_ramp_by_target(target, tt_pars['exp_time'], loadstep)
    scan_name = 'pct_%dN_' % target
    dct_marana_dict(scan_name, pct_pars)
    marana_dct()
    scan_name = 'dct_%dN_' % target
    dct_marana_dict(scan_name, dct_pars)
    frelon16in(4)
    grain_list = loop_grains(grain_list, tt_pars, loadstep)
    return grain_list


def loop_grains(grain_list, tt_pars, step):
    """Loops through a list of Topotomo grains  and performs refinement + TT scan_acquisition
    :param dict grain_list:  list of grain structures containing Topotomo alignment values
    :param dict tt_pars: Topotomo scan parameters
    :param int step: running number of load step in load-sequence  
    """
    umv(s8hg, tt_pars['slit_hg'], s8vg, tt_pars['slit_vg'])
    frelon16.image.roi = tt_pars['image_roi'] # [510, 510, 900, 900]
    frelon16.roi_counters.set('roi1', tt_pars['counter_roi'])  #[300, 300, 300, 300])
    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    for i in range(len(grain_list)):
        newdataset('grain_%04d_step_%d' % (grain_list[i]['gr_id'], step))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time)
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'])
        sleep(1)
        #update_grain_info(grain)
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    return grain_list
        
def refine_tt_alignment(tt_grain, ang_step=0.05, search_range=1.0, exp_time=0.1):
    """Refine the topotomo samrx, samry alignment.
    
    This function runs 4 base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use (in degrees).
    :param float search_range: the base tilt angular search range in degrees.
    """
     # half base tilt range in degree
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/visitor/ma4757/id11/'
    sample_name = 'A'
    
    # open a script to gather all acquisition commands
    print(tt_grain)
    gid = tt_grain['gr_id']
    print('find range for grain %d\n' % gid)
    # check that diffry is negative (Bragg alignment)
    if tt_grain['diffry'] > 0:
        raise(ValueError('diffry value should be negative, got %.3f, please check your data' % tt_grain['diffry']))
        
    # align our grain
    topotomo_tilt_grain_dict(tt_grain)

    # define the ROI automatically as [710, 710, 500, 500]
    #frelon16.image.roi = [0, 0, 1920, 1920]
    #ct(0.1)
    #frelon16.roi_counters.set('roi1', [710, 710, 500, 500])
    #ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    #exp_time = exp_time / tt_grain['int_factor']
    # first scan at 0 deg
    umvct(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode='TIME')
    start_angle_000, end_angle_000, cen_angle_000 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # second scan at 180 deg
    umv(diffrz, 180)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode='TIME')
    start_angle_180, end_angle_180, cen_angle_180 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samry by half the difference
    samry_offset = 0.5 * (cen_angle_000 - cen_angle_180)
    print("moving samry by %.3f, final position: %.3f" % (samry_offset, tt_grain['samry'] + samry_offset))
    umvr(samry, samry_offset)

    # third scan at 270 deg
    umv(diffrz, 270)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, 0.1, scan_mode='TIME')
    start_angle_270, end_angle_270, cen_angle_270 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # fourth scan at 90 deg
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode='TIME')
    start_angle_090, end_angle_090, cen_angle_090 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samrx by half the difference
    samrx_offset = 0.5 * (cen_angle_090 - cen_angle_270)
    print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    #samrx_offset =  tt_grain['diffry'] - cen_angle_090
    #print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    umvr(samrx, samrx_offset)

    # update values in tt_grain
    tt_grain['samrx'] += samrx_offset
    tt_grain['samry'] += samry_offset
    # update_grain_info(tt_grain)  broken :-(
    print('values updated in tt_grain record')
    return tt_grain

def find_range(tt_grain, ang_step=0.05, search_range=0.3, exp_time=0.1):
    """Find the topotomo angular range.
    
    This function runs two base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition. Note that this function 
    assumes that the grain has been aligned already.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use.
    :param float search_range: the base tilt angular search range in degrees.
    """
    step = 1  # load step
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/visitor/ma4757/id11/'
    sample_name = 'A'
    
    # open a script to gather all acquisition commands
    gid = tt_grain['gr_id']
    cmd_path = os.path.join(data_dir, 'tt_acq_grain_%d_step_%d.py' % (gid, step))
    f = open(cmd_path, 'w')
    f.write("def tt_acq():\n\n")
    f.write("    # activate ROI\n")
    f.write("    frelon16.image.roi = [710, 710, 500, 500]\n")
    f.write("    frelon16.roi_counters.set('roi1', [100, 100, 300, 300])\n")
    f.write("    ct(0.1)\n")
    f.write("    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')\n\n")
    
    print('find range for grain %d\n' % gid)
    
    # see if we need to create a new dataset or not
    #dataset = 'grain_%04d_checkrange_step_%d' % (gid, step)
    #newdataset(dataset)

    # define the ROI automatically as [710, 710, 500, 500]
    #frelon16.roi_counters.set('roi1', [710, 710, 500, 500])
    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')

    # first scan at 90 deg
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode='TIME')
    start_angle_90, end_angle_90 = get_limits(fscan.get_data())
    diffry_range_90 = max(tt_grain['diffry'] - start_angle_90, end_angle_90 - tt_grain['diffry'])

    # second scan at 0 deg
    umv(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode='TIME')
    start_angle_00, end_angle_00 = get_limits(fscan.get_data())
    diffry_range_00 = max(tt_grain['diffry'] - start_angle_00, end_angle_00 - tt_grain['diffry'])

    # use the upper range value to define topotomo bounds
    print('diffry_range_00=%.3f - diffry_range_90=%.3f' % (diffry_range_00, diffry_range_90))
    diffry_range = max(diffry_range_00, diffry_range_90)

    # now find out the largest range to cover the grain
    diffry_start = tt_grain['diffry'] - diffry_range
    diffry_end = tt_grain['diffry'] + diffry_range
    n_acq_images = (diffry_end - diffry_start) / ang_step

    # write out the topotomo command for bliss
    f.write("    # create a new data set\n")
    f.write("    newdataset('grain_%04d_step_%d')\n\n" % (gid, step))
    f.write("    # acquisition for grain %d\n" % gid)
    f.write("    user.topotomo_tilt_grain(%d)\n" % gid)
    f.write("    umv(samrx, %.3f)\n" % tt_grain['samrx'])
    f.write("    umv(samry, %.3f)\n" % tt_grain['samry'])
    f.write("    fscan2d(diffrz, 0, 10, 36, diffry, %.3f, %.3f, %d, 0.1, scan_mode='TIME')\n\n" % (diffry_start, ang_step, n_acq_images))
    f.close()
    return diffry_start, n_acq_images

def get_limits(scan_data, thres=0.05, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry = scan_data['diffry']
    # work out the angular step
    ang_step = diffry[1] - diffry[0]
    intensity = scan_data['frelon16:roi_counters:roi1_avg']
    # background correction
    bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) -1
    start_angle = diffry[start_pos] - padding * ang_step
    end_angle = diffry[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, number of images={}'.format(start_angle, end_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle
    
def get_limits_and_weighted_cen_angle(scan_data, thres=0.05, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry = scan_data['diffry']
    # work out the angular step
    ang_step = diffry[1] - diffry[0]
    intensity = scan_data['frelon16:roi_counters:roi1_avg']
    # background correction
    #bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    bg = np.min(intensity)
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) - 1
    weighted_cen_pos = 0;
    for i_pos in range(start_pos, end_pos, 1):
        weighted_cen_pos += i_pos * intensity[i_pos]
    weighted_cen_pos = weighted_cen_pos / np.sum(intensity[start_pos:end_pos])
    # compute the angle using linear interpolation
    weighted_cen_angle = (diffry[start_pos] * (end_pos - weighted_cen_pos) + diffry[end_pos] * (weighted_cen_pos - start_pos)) / (end_pos - start_pos)
    start_angle = diffry[start_pos] - padding * ang_step
    end_angle = diffry[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, weighted_cen_angle={}, number of images={}'.format(start_angle, end_angle, weighted_cen_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle, weighted_cen_angle




import json
import time
import os

class NumpyEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self,obj)


def writedictionaryfile(dictname,filename):
    """to write dictionary to file"""
    datatowrite = json.dumps(dictname)
    with open('%s.json'%filename, 'w') as f:
        f.write(datatowrite)
    print("Dictionary written as %s.json"%filename)
    return

def appenddictionaryfile(dictname,filename):
    """to write dictionary to file"""
    if os.path.exists('%s.json'%filename):
        with open('%s.json'%filename, 'r+') as f:
            data = json.loads(f.read())
            data.update(newdict)
            appended_data = json.dumps(data)
            f.seek(0)
            f.write(appended_data)
    else:
        newdict = {}
        newdict[str(time.time())] = dictname
        datatowrite = json.dumps(newdict)
        with open('%s.json'%filename, 'w') as f:
            f.write(datatowrite)
    print("Dictionary appended to %s.json"%filename)
    return

def readdictionaryfile(filename):
    """to read dictionary from file, returns dictionary"""
    with open('%s.json'%filename, 'r') as f:
        readdata = json.loads(f.read())
    print("Dictionary read from %s.json"%filename)
    return readdata


def update_grain_info(grain):
    writedictionaryfile(grain,'%s_g%s'%(SCAN_SAVING.collection_name,grain['gr_id']))
    #appenddictionaryfile(grain,'%s_g%s_history'%(SCAN_SAVING.collection_name,grain['gr_id']))
    return

def get_json_grain_info(grain_number):
    return readdictionaryfile('%s_g%s'%(SCAN_SAVING.collection_name,str(grain_number)))

def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)
    
def ref_scan_samy(exp_time, nref, ystep, omega, scanmode):
    umvr(samy, ystep)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umvr(samy, -ystep)
    
def ref_scan_difftz(exp_time, nref, zstep, omega, scanmode):
    difftz0 = difftz.position
    umv(difftz, difftz0 + zstep)
    print("fscan(diffrz, %6.2f, 0.1, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.1, nref, exp_time, scan_mode = scanmode)
    umv(difftz, difftz0)
    
class LoadLoop:
    def __init__(self):
        self._task = None
        self._stop = False
        self._sleep_time = 1
        self._filepath = None
        
    def start(self,target,loadstep,filepath,sleep_time=1.,time_step=0.5):
        self._filepath = filepath
        if self._task:
            self._stop = True
            self._task.get()

        self._sleep_time = sleep_time
        self._stop = False
        self._task = gevent.spawn(self._run,target,loadstep,time_step)

    def stop(self):
        self._stop = True
        if self._task: 
           self._task.get()

    def _run(self,target,loadstep,time_step):
        newdataset('loadramp_%d' %  loadstep)
        with open(self._filepath,'a') as f:
            while not self._stop:
                load_ramp_by_target(target,0,loadstep,time_step,to_file=f)
                gevent.sleep(self._sleep_time)


def tomo_series(start, num_scans, target, sleep_time=0, pct_pars=None):
    for i in np.arange(start, start+num_scans):
        tomo.full_turn_scan(str(i))
        load_ramp_by_target(target, 0.05, 1)
	#dct_marana_dict(str(i), pct_pars)
        sleep(sleep_time)
    return
