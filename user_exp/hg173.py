import numpy as np
import time

#fsh.session = tdxrd

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico6']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico6']

#def tomoin():
#    ACTIVE_MG.enable("frelon1:image") 
#    ACTIVE_MG.disable("frelon3:image")
#    sheh3.close()
#    #umv(ffdtx1, 500)
#    umv(ffdtz1, 100, d1ty, 0)
#    sheh3.open()
#    print("Ready to collect tomo data with frelon1") 

def maranain():
    ACTIVE_MG.enable("marana:image") 
    ACTIVE_MG.disable("frelon3:image")
    ACTIVE_MG.disable("frelon3:roi*")
    sheh3.close()
    umv(ffdtz1, 100)
    umv(d3ty, 0)
    sheh3.open()
    print("Ready to collect tomo data with marana") 

def frelonin():
    ACTIVE_MG.disable("marana:image") 
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    sheh3.close()
    umv(d3ty, 100)
    umv(ffdtz1, 0)
    sheh3.open()
    print("Ready to collect diffraction data with frelon3") 

def crlin():
    bigy=config.get('bigy')
    tfoh1 = config.get("tfoh1")
    umv(bigy, 0)
    tfoh1.set(18,"Be") 

def crlout():
    bigy=config.get('bigy')
    tfoh1 = config.get("tfoh1")
    umv(bigy, 80)
    tfoh1.set(0,"Be")
    print("done")

def powder():
    frelonin()
    crlout()
    ascan(diffrz, 0, 180, 90, 0.1)

def difftyfrelon():
    # newproposal("hg173")
    # fpico4
    pico4.auto_range = False
    pico4.range=2.1e-5

    ACTIVE_MG.disable_all()
    ACTIVE_MG.enable("frelon3:roi*")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("*fpico4")

    y0 = 14.623
    width = 0.3
    step  = 0.004
    #24 minutes

    for my_y  in np.arange( y0-width, y0+width+step/10., step):
        umv(diffty, my_y)
        #pause_for_refill(30)
        finterlaced(diffrz,   0,  1, 180, 0.5, mode="ZIGZAG") 
        #diffty.reset_closed_loop()

def hydrascan():
    for hs,my_s in enumerate(np.arange(0,30,1)):
        newdataset("cerbonio_hydroscan_night_%03d"%(hs))
        frelonin()       
        crlin()
        difftyfrelon()
        crlout()
        powder()
        tomoin()
        umv(diffty, 14.665)
        tomo.half_turn_scan() 

def startscan():
    diffty.reset_closed_loop()
    newdataset("Bassanite2_start")
    frelonin()
    crlout()
    powder()
    crlin()
    difftyfrelon()
    crlout()
    tomoin()
    umv(diffty, 14.70)
    tomo.half_turn_scan() 

def hg173_myfscan( *args, **kwds):
    for i in range(3):
        try:
            fscan(*args, **kwds)
        except Exception as e:
            elog_print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break

def hg173_2d(y0=-1000, ye=1000, ys=20):
    fscan.pars.latency_time=0
    for i, ypos in enumerate(np.arange(y0,ye,ys)):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            hg173_myfscan(rot, -1, 0.5, 182/0.5, 0.05, scan_mode='CAMERA' ) 
        else:
            hg173_myfscan(rot, 181, -0.5, 182/0.5, 0.05, scan_mode='CAMERA' )
            
def volscan_29102021():
    shtz_zero = 0.383
    umv(shtz,shtz_zero)
    shtz_rel = [0.022, 0.041, 0.100 ,0.150, 0.165, 0.220, 0.304]
    for zrel in shtz_rel:
        new_shtz = shtz_zero + zrel
        umv( shtz, new_shtz )
        print('moved shtz to %s'%str(new_shtz) )
        newdataset( "180_%s"%str(  int(zrel*1000)  ))
        fscan2d(dty,-1000,5,401,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')
        
def GK532_7_301021():
    shtz_zero = 0.21
    umv(shtz,shtz_zero)
    shtz_rel = [0, 0.0365 ,0.074]
    for zrel in shtz_rel:
        new_shtz = shtz_zero + zrel
        umv( shtz, new_shtz )
        print('moved shtz to %s'%str(new_shtz) )
        newdataset( "180_2_%s"%str(  int(zrel*1000)  ))
        fscan2d(dty,-600,5,241,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')
        
def GK243_8_301021():
    shtz_zero = -0.56
    umv(shtz,shtz_zero)
    shtz_rel = [0.01, 0.028 ,0.035,0.054]
    for zrel in shtz_rel:
        new_shtz = shtz_zero + zrel
        umv( shtz, new_shtz )
        print('moved shtz to %s'%str(new_shtz) )
        newdataset( "180_%s"%str(  int(zrel*1000)  ))
        fscan2d(dty,-400,5,161,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')
        
def GK84b_15_301021():
    shtz_zero = -1.8
    umv(shtz,shtz_zero)
    shtz_rel = [0.01, 0.033 ,0.055,0.067]
    for zrel in shtz_rel:
        new_shtz = shtz_zero + zrel
        umv( shtz, new_shtz )
        print('moved shtz to %s'%str(new_shtz) )
        newdataset( "180_2_%s"%str(  int(zrel*1000)  ))
        fscan2d(dty,-200,5,81,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')
        
def GK84a_38_301021():
    shtz_zero = 0.15
    umv(shtz,shtz_zero)
    shtz_rel = [0.01, 0.028 ,0.035,0.044]
    for zrel in shtz_rel:
        new_shtz = shtz_zero + zrel
        umv( shtz, new_shtz )
        print('moved shtz to %s'%str(new_shtz) )
        newdataset( "180_%s"%str(  int(zrel*1000)  ))
        fscan2d(dty,-250,5,101,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')
        
def GK533_8_301021():
    shtz_zero = -1.35
    umv(shtz,shtz_zero)
    shtz_rel = [0.018, 0.032 ,0.045,0.060,0.074,0.083,0.1,0.12,0.135,0.153]
    for zrel in shtz_rel:
        new_shtz = shtz_zero + zrel
        umv( shtz, new_shtz )
        print('moved shtz to %s'%str(new_shtz) )
        newdataset( "180_%s"%str(  int(zrel*1000)  ))
        fscan2d(dty,-700,5,281,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')
        
def GK243_6_311021():
    shtz_zero = 0.64
    umv(shtz,shtz_zero)
    shtz_rel = [0.01, 0.047 ,0.065]
    for zrel in shtz_rel:
        new_shtz = shtz_zero + zrel
        umv( shtz, new_shtz )
        print('moved shtz to %s'%str(new_shtz) )
        newdataset( "180_%s"%str(  int(zrel*1000)  ))
        fscan2d(dty,-400,5,161,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')
        
def GK532_5_311021():
    shtz_zero = -1.48
    umv(shtz,shtz_zero)
    shtz_rel = [0.015, 0.031 ,0.059, 0.09, 0.11]
    for zrel in shtz_rel:
        new_shtz = shtz_zero + zrel
        umv( shtz, new_shtz )
        print('moved shtz to %s'%str(new_shtz) )
        newdataset( "180_%s"%str(  int(zrel*1000)  ))
        fscan2d(dty,-450,5,181,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')
        
def GK533_11_311021():
    shtz_zero = -2.4
    umv(shtz,shtz_zero)
    shtz_rel = [0.011, 0.035 ,0.060, 0.070]
    for zrel in shtz_rel:
        new_shtz = shtz_zero + zrel
        umv( shtz, new_shtz )
        print('moved shtz to %s'%str(new_shtz) )
        newdataset( "180_%s"%str(  int(zrel*1000)  ))
        fscan2d(dty,-450,5,181,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')
        
def GK84b_8_011121():
    shtz_zero = -0.95
    umv(shtz,shtz_zero)
    shtz_rel = [0.01, 0.053 ,0.072,0.090,0.130,0.148,0.163,0.200]
    for zrel in shtz_rel:
        new_shtz = shtz_zero + zrel
        umv( shtz, new_shtz )
        print('moved shtz to %s'%str(new_shtz) )
        newdataset( "180__2_%s"%str(  int(zrel*1000)  ))
        fscan2d(dty,-800,5,321,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')
        
def IT3441_3_011121():
    shtz_zero = -0.3
    umv(shtz,shtz_zero)
    shtz_rel = [0.005, 0.013 ,0.019,0.025, 0.030]
    for zrel in shtz_rel:
        new_shtz = shtz_zero + zrel
        umv( shtz, new_shtz )
        print('moved shtz to %s'%str(new_shtz) )
        newdataset( "180_%s"%str(  int(zrel*1000)  ))
        fscan2d(dty,-350,5,141,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')

def EP_Layer1():
    pz_rel = [0,0.3, 0.6 ,0.9,1.2, 1.5]
    for zrel in pz_rel:
        umv( pz, zrel )
        print('moved pz to %s'%str(zrel) )
        newdataset( "180_%s"%str(  zrel  ))
        fscan2d(dty,-240,0.3,1600,rot,-90,25*0.005,181/(25*0.005)+1,0.005,scan_mode='CAMERA')
