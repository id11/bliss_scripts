

### To load in nscope session:
### user_script_load('ch6365')



import numpy as np, time
user_script_load('optics')


def check_cpm18(pos=6.371):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()



# 


mca.load_configuration('SDD_maxEnergy81.9keV_higerResolution_24-07-2020.ini')

def demo():
    newdataset('align')
    align_cor_interactive(30,600,0.05,ctr='roi2_avg',angles=[45,90,135,270])
    umv(rot, 90)
    newdataset('experiment')
    pause_for_refill(20)
    loopscan(1,30)
    pause_for_refill(185)
    fscan(rot,45,0.5,180,1)
    umv(rot,0)
    
  

def align_scan():
    newdataset('align')
#    align_cor_interactive(30,600,0.05,ctr='roi2_avg',angles=[45,90,135,270])
#    plotselect(mca.counters.Sn_det0)
#    align_cor_interactive(30,600,0.05,ctr='mca:Sn_det0',angles=[135,90,135+180,270])
    umv(rot,0)
    
    
def collect():
    umv(rot,90)
    newdataset("experiment")
    pause_for_refill(20)
    loopscan(1,30)
    pause_for_refill(185)
    fscan(rot,45,0.5,180,1)
    umv(rot,0)
    
 
 
def center1():
    umv(shtx,0.1211)
    umv(shty,0.4705)
    umv(shtz,0.4030)

def center2():
    umv(shtx,0.3211)
    umv(shty,0.4205)
    umv(shtz,-0.1969)
    
    
    
    
    
