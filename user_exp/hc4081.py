
import time

def lunch():

    newdataset("1deg_per_s")
    loopscan(10,1)
    fscan(rot, 0, 1, 365, 1, scan_mode='CAMERA' )

    newdataset("p05deg_per_s")
    loopscan(10,1)
    fscan(rot, 0, .05, 365/.05, 1, scan_mode='CAMERA' )

def oxsetpoint( tset, ramp=360. ):
    ox700._controller.hw_controller.ramp( ramp, tset )
    time.sleep(0.1)
    return ox700._controller.hw_controller.read_target_temperature()

def gototemp(tset):
    while abs(oxsetpoint(tset) - tset) > 1:
        print("retry!!")
        time.sleep(1)
    # 360 per hour, so 6 per minute, so expect about a minute
    umv(rot, 0)
    loopscan( 90, 1, ox700 )

    
def night():

    for tset in range(125,281,5):
        for i in range(5):
            try:
                gototemp(tset)
                break
            except:
                print("I am not impressed")
                time.sleep(5)
        newdataset( "T%dK"%(tset) )
        fscan(rot, 0, 1, 365, 1, scan_mode='CAMERA' )
        
    newdataset("T%dK_01d1s"%(tset))
    fscan(rot, 0, .1, 3650, 1, scan_mode='CAMERA' )
    
