import time
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def pdf_capillary_scan_1():
    newsample('rack6')

    #Sample names WITHOUT CeO2 and Kapton
    sample_names = ['VB1','VB2','VB3','VB4','VB5','VB6','VB7','VB8','VB9','VB10']
    diffy_positions = [6.31,9.84,13.73,17.84,21.71,25.9,30.5,33.75,37.93,41.83]
    sheh1.open()

    #CeO2
    #newdataset('CeO2')
    #umv(diffy,-1.6) #To Modify
    #pause_for_refill(60)
    #loopscan(30,0.1)

	#Kapton
    #newdataset('kapton')
    #umv(diffy,2.14) #To Modify
    #pause_for_refill(60)
    #loopscan(20*60,1)

    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(10*60,1)

def pdf_capillary_scan():
	sheh1.open()
	#Calib scans sample Door
	newdataset('CeO2')
	umv(diffy,-8.06,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(30,0.1)
	newdataset('kapton1')
	umv(diffy,-4.08,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(15*60,1)
	newdataset('CuAl_LDH')
	umv(diffy,0,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuCo_LDH')
	umv(diffy,4,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuCe_LDH')
	umv(diffy,8.63,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuCr_LDH')
	umv(diffy,12,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuFe_LDH')
	umv(diffy,16,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuGa_LDH')
	umv(diffy,20,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuIn_LDH')
	umv(diffy,24.3,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuLa_LDH')
	umv(diffy,28,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuMn_LDH')
	umv(diffy,32,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuNd_LDH')
	umv(diffy,36,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuV_LDH')
	umv(diffy,40,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuCo_UT')
	umv(diffy,44.7,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('NiAl_LDH')
	umv(diffy,48,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('NiCo_LDH')
	umv(diffy,52,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('NiCe_LDH')
	umv(diffy,56.1,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	sheh1.close()
