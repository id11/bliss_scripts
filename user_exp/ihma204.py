import numpy as np
from scipy.optimize import curve_fit

def goto_1umbeam():
    umv(vly,0.81650,vlz,-0.34100,hly,0.52300,hlz,-0.68425,shnee,143.60000,axmo,58.00000)
    umv(s9hg,0.05,s9vg,0.05,pinx,26.70300)
    
def goto_smallbeam():
    umv(vly, 0.7925, vlz, -0.239, hly, 0.7085, hlz, -0.69125, piny, -0.002)
    umv(s9hg,0.05,s9vg,0.05)    
    umv(shnee,89.18,axmo,63.52)
    
def goto_horzbeam():
    umv(vly, 0.7925, vlz, -0.239, hly, 0.7085, hlz, -0.69125+1, piny, -0.002)
    umv(s9hg,0.05,s9vg,0.05)    
    umv(shnee,89.18,axmo,63.52)
    
def findside(x, y):
    y = y-y.min()
    cm = (x*y).sum()/y.sum()
    ny = np.clip(y, 0.2*y.max(), 1*y.max()) - (0.2*y.max()) 
    ny = ny - (ny.max()/2)
    e1 = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    e2 = np.interp(0, -ny[np.argmax(y):], x[np.argmax(y):])
    return e1, e2

def linefit(x, a, b):
    return  np.abs((a*x) + b)   
    
def align_r(raxis,rrange,rsteps,pzrange,pzsteps,ctim,ctr=mca):
    rcen = raxis.position
    if raxis == shrx:
        if np.abs((rot.position+90)%180)>1:
            umv(rot, 90)
    elif raxis == shry:
        if np.abs((rot.position)%180)>1:
            umv(rot, 0)
    ws = []
    rvals = np.linspace(-rrange,rrange,rsteps)
    for r in rvals:
        umv(raxis, rcen+r)
        s = dscan(pz,-pzrange,pzrange,pzsteps,ctim,ctr)
        fe = findside( s.get_data('pz'), s.get_data('mca:all_det0') )
        ws.append(fe[1]-fe[0])
    p, _ = curve_fit(linefit, rvals[:2], ws[:2], (0,0))
    print(p)
    p, _ = curve_fit(linefit, rvals, ws, (p[0],-10))
    print(p)
    rideal = -p[1]/p[0]
    if np.abs(rideal) < rrange:
        umv(raxis,rcen + rideal)
    return rcen + rideal
    
def scanandcen(axis,rng,steps,ctim,ctr=mca):
    s = dscan(axis,-rng,rng,steps,ctim,ctr)
    x, y = s.get_data(axis.name), s.get_data('mca:all_det0')
    y = y-y.min()
    ny = np.clip(y, 0.3*y.max(), 1*y.max()) - (0.3*y.max()) 
    return (x*ny).sum()/ny.sum()

def pzedge(ctim,ctr=mca):
    s = dscan(pz,-3,3,50,ctim,ctr)
    x, y = s.get_data('pz'), s.get_data('mca:all_det0')
    umv(pz, x[np.argmax(y)])
    s = dscan(pz,-1.5,1,150,ctim,ctr)
    x, y = s.get_data('pz'), s.get_data('mca:all_det0')
    edge = x[np.argmin(np.abs(y-( ( np.mean(y[110:149]) + np.mean(y[1:30]) )/2) ))]
    umv(pz, edge)
    return edge
    
def film_cor(rng, npts, ctim, ctr='eiger:roi_counters:roi1_sum'):
    cens = []
    plotselect(ctr)
    for j,k in enumerate([0,90,180,270]):
        umv(rot, k)
        if j%2 == 0:
            s = fscan(dty, -rng, rng*2/(npts-1), npts, ctim)
        else:
            s = fscan(dty, rng, -rng*2/(npts-1), npts, ctim)
        A = fscan.get_data()
        x, y = A['dty'], A[ctr]
        pk = np.clip(y-np.min(y), 0, 1e12)
        cens.append( (x*pk).sum()/pk.sum() )
        
    pyerr = (cens[0]-cens[2])/2
    pxerr = (cens[1]-cens[3])/2
    dtyideal = np.mean(cens)
    if (py.position+pyerr)>10 and (py.position+pyerr)<90:
        umvr(py, pyerr)
    else:
        umvr(shty, (pyerr/1000))
    if (px.position+pxerr)>10 and (px.position+pxerr)<90:
        umvr(px, pxerr)
    else:
        umvr(shtx, (pxerr/1000))
    umv(dty, dtyideal, rot, 0)
    
def align_film():
    umv(pz, 90)
    film_cor(2500, 500, 0.2)
    umv(pz,50)
    pzedge(0.1,ctr=mca)
    plotselect('mca:all_det0')
    ideals = []
    for r in [0,90,180,270]:
        umv(rot, r)
        pzcen = scanandcen(pz, 45, 90, 0.05)
        umvr(shtz, (pzcen-50)/1e3)
        if r%180 == 0:
            ideals.append(align_r(shry, 0.6, 8, 40, 80, 0.05))
        else:
            ideals.append(align_r(shrx, 0.6, 8, 40, 80, 0.05))
    print(ideals)
    umv(shry, np.mean([ideals[0],ideals[2]]))
    umv(shrx, np.mean([ideals[1],ideals[3]]))
    pzedge(0.1,ctr=mca)
    

    
