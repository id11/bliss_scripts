import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime

atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')

# dataset_prefix = 'test_10x10_posY'
# dataset_prefix = 'test_10x10_SiO2_posY'
# dataset_prefix = 'test3_grid'
# dataset_prefix = 'grid_operando_li1'
# dataset_prefix = 'test_grid'
# dataset_prefix = 'testY_10*100_beam'
#dataset_prefix = 'operando_Y_1st_lith'
#dataset_prefix = 'operando_Y_1st_lith_cont'
#dataset_prefix = 'operando_Y_1st_lith_2cont'
#dataset_prefix = 'operando_Y_jp3_1st_li'
#dataset_prefix = 'operando_Y_freestanding_aSi'
#dataset_prefix = 'operando_Y'
#dataset_prefix = 'empty_cell'
dataset_prefix = 'operando_Y_cont'



def define_pdf_pars():
    pdf_pars={}
    pdf_pars['samy_cen'] = 0.8723 #pdfct cell! #1.5 #1.57 #1.4086 #1.4532    # 1.5786
    pdf_pars['samtz_cen'] = 4.85 #4.65 #pdfct cell #4.9 #4.789 #5.424 #5.284 #5.154 #5.049 #5.4390   # 5.3390   # 5.029
    pdf_pars['nof_shifts'] = 5
    pdf_pars['shift_step_size'] = 0.1
    pdf_pars['num_projs'] = 3
    pdf_pars['exp_time'] = 10    # 20s detector saturates when hitting the Cu
    pdf_pars['slit_hg'] = 0.1
    pdf_pars['slit_vg'] = 0.01
    pdf_pars['zmot'] = samtz
    pdf_pars['ymot'] = samy
    pdf_pars['cpm18_detune'] = 0
    pdf_pars['pause_flag'] = True
#    pdf_pars['ceo2_samtx'] = -1.1274
#    pdf_pars['ceo2_samty'] = 0.3521
#    pdf_pars['ceo2_samtz'] = -3.1900
#    pdf_pars['ceo2_difftz'] = -35.2660
    return pdf_pars

def run_pdf():
    # assum you start with pct condition
    # start DCT measurements
    # switch_to_marana()
    # umv(atty,-10,attrz,0)
    
    # switch to PDF measurements
    switch_to_pdf()
    pdf_pars = define_pdf_pars()
    umv(atty,-10,attrz,0)
    umv(samy,pdf_pars['samy_cen'],samtz,pdf_pars['samtz_cen'])
    umv(s7vg, pdf_pars['slit_vg'], s7hg, pdf_pars['slit_hg'])
    umv(s8vg, pdf_pars['slit_vg'] + 0.08, s8hg, pdf_pars['slit_hg'] + 0.05)
    while(1):
        measure_pdf_yseries(pdf_pars)
    #measure_pdf_zseries(pdf_pars)
    
    
   # switch to grid PDF measurements
    #switch_to_pdf()
    #pdf_pars = define_pdf_pars()
    #umv(atty,-10,attrz,0)
    #umv(samy,pdf_pars['samy_cen'],samtz,pdf_pars['samtz_cen'])
    #umv(s7vg, pdf_pars['slit_vg'], s7hg, pdf_pars['slit_hg'])
    #umv(s8vg, pdf_pars['slit_vg'] + 0.08, s8hg, pdf_pars['slit_hg'] + 0.05)
    #measure_pdf_grid(pdf_pars)
    #while(1):
    #    measure_pdf_grid(pdf_pars)
    
    # switch back to marana camera for alignment
    #switch_to_marana()
    #umv(atty,-10,attrz,0)
    #umvct(s7vg, 1.2, s7hg, 1.2, s8vg,1.3, s8hg, 1.3)
    print('Done successfully')

   
def switch_to_marana():
    fsh.close()
    tfoh1.set(0,'Be') # 16
    tfoh1.set(0,'Al') # 64
    marana_in()
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana:image')
    ACTIVE_MG.enable('marana:roi_counters*')
    pico4.remove()
    fsh.open()
    umv(s7vg, 1.2, s7hg, 1.2, s8vg,1.3, s8hg, 1.3)
    ct(0.05)

def switch_to_pdf():
    fsh.close()
    tfoh1.set(16,'Be')
    tfoh1.set(64,'Al')
    ff_in()
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.enable('frelon3*')
    ACTIVE_MG.enable('fpico4')
    pico4.insert()
    fsh.open()

def measure_pdf_grid(pdf_pars, datasetname = dataset_prefix):
    shift_step_size = abs(pdf_pars['shift_step_size'])
    if(pdf_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = pdf_pars['samtz_cen'] - (pdf_pars['nof_shifts'] - 1) * shift_step_size / 2
    offset_samy_pos = pdf_pars['samy_cen'] - (pdf_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samy:' + str(pdf_pars['samy_cen']))
    print('Central samtz:' + str(pdf_pars['samtz_cen']))
    if(offset_samtz_pos < pdf_pars['zmot'].low_limit or offset_samtz_pos + (pdf_pars['nof_shifts'] - 1) * shift_step_size > pdf_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    if(offset_samy_pos < pdf_pars['ymot'].low_limit or offset_samy_pos + (pdf_pars['nof_shifts'] - 1) * shift_step_size > pdf_pars['ymot'].high_limit):
        print('Exceed the limits of samy')
        return('Exceed the limits of samy')
    newdataset(datasetname)
    for iter_i in range(int(pdf_pars['nof_shifts'])):
        umv(pdf_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        for iter_j in range(int(pdf_pars['nof_shifts'])):
            print('Measuring point Z{}_Y{}'.format(iter_i+1, iter_j+1))
            umv(pdf_pars['ymot'], iter_j * shift_step_size + offset_samy_pos)
            loopscan(pdf_pars['num_projs'], pdf_pars['exp_time'])
        umv(pdf_pars['ymot'], pdf_pars['samy_cen'])
    umv(pdf_pars['zmot'], pdf_pars['samtz_cen'])
    print('PDF Succeed')
    return('Succeed')
    
def measure_pdf_zseries(pdf_pars, datasetname = dataset_prefix):
    #switch_to_pdf()
    #pdf_pars = define_pdf_pars()
    #umv(s7vg, pdf_pars['slit_vg'], s7hg, pdf_pars['slit_hg'])
    #umv(s8vg, pdf_pars['slit_vg'] + 0.08, s8hg, pdf_pars['slit_hg'] + 0.05)
    shift_step_size = abs(pdf_pars['shift_step_size'])
    if(pdf_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = pdf_pars['samtz_cen'] - (pdf_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(pdf_pars['samtz_cen']))
    if(offset_samtz_pos < pdf_pars['zmot'].low_limit or offset_samtz_pos + (pdf_pars['nof_shifts'] - 1) * shift_step_size > pdf_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(pdf_pars['nof_shifts'])):
        umv(pdf_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        loopscan(pdf_pars['num_projs'], pdf_pars['exp_time'])
    umv(pdf_pars['zmot'], pdf_pars['samtz_cen'])
    newdataset('test')
    print('PDF Succeed')
    return('Succeed')

def pause_scan(t = 30):
    fsh.close()
    print('Fast Shutter closed ...')
    t0 = time.time()
    dt = 0
    exit_flag = False
    while exit_flag == False:
        dt = time.time() - t0
        if dt >= t:
            exit_flag = True
    print('Fast shutter reopened after {} s'.format(dt))
    fsh.open()

def measure_pdf_yseries(pdf_pars, datasetname = dataset_prefix):
    #switch_to_pdf()
    #pdf_pars = define_pdf_pars()
    #umv(s7vg, pdf_pars['slit_vg'], s7hg, pdf_pars['slit_hg'])
    #umv(s8vg, pdf_pars['slit_vg'] + 0.08, s8hg, pdf_pars['slit_hg'] + 0.05)
    shift_step_size = abs(pdf_pars['shift_step_size'])
    if(pdf_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samy_pos = pdf_pars['samy_cen'] - (pdf_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samy:' + str(pdf_pars['samy_cen']))
    if(offset_samy_pos < pdf_pars['ymot'].low_limit or offset_samy_pos + (pdf_pars['nof_shifts'] - 1) * shift_step_size > pdf_pars['ymot'].high_limit):
        print('Exceed the limits of samy')
        return('Exceed the limits of samy')
    for iter_i in range(int(pdf_pars['nof_shifts'])):
        umv(pdf_pars['ymot'], iter_i * shift_step_size + offset_samy_pos)
        newdataset(datasetname + str(iter_i + 1))
        loopscan(pdf_pars['num_projs'], pdf_pars['exp_time'])
        if pdf_pars['pause_flag'] == True:
            pause_scan()
    umv(pdf_pars['ymot'], pdf_pars['samy_cen'])
    print('PDF Succeed')
    return('Succeed')
    
def ff_out():
    umv(ffdtx1, 500)
    umv(ffdtz1, 320)
    
def marana_in():
    ff_out()
    umv(d3ty, 0, d3tz, 0)
    umv(nfdtx,100)

def marana_out():
    umv(nfdtx,100,d3ty,180)
    
def ff_in():
    marana_out()
    umv(ffdty1,0,ffdty2,0)  # ffdty1 = ffdty2 = 37.1881
    umv(ffdtz1, 0)     # ffdtz1 = 36.0595 for 3DXRD
    umv(ffdtx1, 100)
    umv(s7vg,0.01,s7hg,0.1,s8vg,0.09,s8hg,0.15)

def ff_ceo2(pdf_pars):
    umvct(s7vg, 0.05, s7hg, 0.05 )
    umvct(s8vg, 0.13, s8hg, 0.13 )
    
    samtx0 = samtx.position
    samty0 = samty.position
    samtz0 = samtz.position
    difftz0 = difftz.position
    
    fsh.close()
    umv(difftz, pdf_pars['ceo2_difftz'])
    umv(samtx,pdf_pars['ceo2_samtx'],samty,pdf_pars['ceo2_samty'],samtz,pdf_pars['ceo2_samtz'])
    umv(diffrz, 0)
    dset_name = 'pdf_' + 'CeO2'
    newdataset(dset_name)
    fsh.open()
    loopscan( 50, pdf_pars['exp_time'])
    umv(diffrz, 180)
    loopscan( 50, pdf_pars['exp_time'])
    
    # move back
    umv(samtx,samtx0,samty,samty0,samtz,samtz0)
    umv(difftz,difftz0)   
    
    
def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")


   
