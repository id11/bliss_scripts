import numpy as np

def acquire_multiple_distances():
	for dist in [50, 75, 100, 200]:
		print(f'will move d3x to {dist} mm')
		umvct(d3x, dist)
		print('will start acquisition')
		fulltomo.full_turn_scan(dataset_name = f'pct_{dist:03}_mm', start_pos = 4) 
