import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime


def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 1
    ff_pars['num_proj'] = 10 / ff_pars['step_size']
    ff_pars['exp_time'] = 1
    ff_pars['slit_hg'] = 0.1
    ff_pars['slit_vg'] = 0.1
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    # ff_pars['samtz_cen'] = -2.29
    # ff_pars['shift_step_size'] = 0.03
    # ff_pars['nof_shifts'] = 30
    return ff_pars

def ff_insitu_isothermal_Oct(ff_pars, t_msr=60, Temp=500, tol=5, datasetname = 'ff'):
    newdataset(datasetname + '_' + str(Temp)+'C')
    umv(s7hg,ff_pars['slit_hg'],s7vg,ff_pars['slit_vg'],s8hg,ff_pars['slit_hg']+0.15,s8vg,ff_pars['slit_vg']+0.15)
    sheh3.open()
    nanodac3.setpoint = Temp
    while np.abs(nanodac3_temp.read() - Temp) > tol:
        print("waiting for furnace to reach setpoint")
        sleep(30)
        
    print("moving down furnance...")
    umv(furnace_z, 0)
    t0 = time.time()
    dt = 0
    while dt <= t_msr:
        dt = time.time() - t0
        dt = dt/60;
        print('Delta time is', dt)
        #finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
        ftimescan(ff_pars['exp_time'],100)
        
    print('moving furnace out')
    umv(furnace_z, 105)
    nanodac3.setpoint = 25
    sheh3.close()
    fsh.close()
    print('Done successfully !')
        
def ff_insitu_isothermal(ff_pars, t_msr, Temp=500, tol=5, datasetname = 'ff'):
    newdataset(datasetname + '_' + str(Temp)+'C')
    umv(s7hg,ff_pars['slit_hg'],s7vg,ff_pars['slit_vg'],s8hg,ff_pars['slit_hg']+0.15,s8vg,ff_pars['slit_vg']+0.15)
    sheh3.open()
    nanodac3.setpoint = Temp
    while np.abs(nanodac3_temp.read() - Temp) > tol:
        print("waiting for furnace to reach setpoint")
        sleep(30)
        
    print("moving down furnance...")
    umv(furnace_z, 0)
    t0 = time.time()
    dt = 0
    while dt <= t_msr:
        dt = time.time() - t0
        dt = dt/60;
        print('Delta time is', dt)
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
        
    print('moving furnace out')
    umv(furnace_z, 145)
    print('Done !')

def thermal_dct_ramp(pct_pars, dct_pars, step, Temp_final, tol=1):
    sheh3.open()
    print("Switch to phase contrast configuration")
    marana_pct(pct_pars)
    print("moving down furnance...")
    umv(furnace_z, 0)
    nanodac3.ramp_speed = 2
    nanodac3.setpoint = Temp_final
    flag = 0
    while flag == 0:
        if np.abs(nanodac3_temp.read() - Temp_final) < tol:
                umv(furnace_z, furnace_out)
                flag = 1
                print('retracting furnace and waiting 5 min to stabilize')
                sleep(300)
                print('collecting phase contrast scan')
                fulltomo.full_turn_scan(f'pct_{Temp}C_{step:02}')
                print('switching to dct configuration')
                marana_dct(dct_pars)
                print('acquiring dct scan...')
                dct_marana_dict(f'dct_{Temp}C_{step:02}', dct_pars)
                print('annealing cycle finished...')
            
    
def T_step_cooling(Temp=[300, 250, 100, 25], Rate = [10, 10, 10, 10], t = [1, 1, 1, 1], tol=2):
    
    # time is in minutes
    assert len(Temp) == len(Rate) and len(Temp) == len(t)
    for i in range(len(Temp)):
        nanodac3.setpoint = Rate[i]
        nanodac3.setpoint = Temp[i]
        
        # cooling stage
        newdataset('cooling_to_' + str(Temp[i])+'C')
        exit_loopscan = False
        while exit_loopscan == False:
            if np.abs(nanodac3_temp.read() - Temp[i]) <= tol:
                exit_loopscan = True
            print("waiting for furnace to reach setpoint = {} C, currently at {} C".format(Temp[i], nanodac3_temp.read()))
            loopscan(20, 0.1, sleep_time = 0.2)

        # isothermal stage
        newdataset('isothermal_at_' + str(Temp[i])+'C')
        exit_loopscan = False 
        t0 = time.time()
        dt = 0
        while exit_loopscan == False:
            dt = time.time() - t0
            dt = dt/60
            print('Isothermal time is {} min'.format(dt))
            if dt > t[i]:
                exit_loopscan = True
            loopscan(20, 0.1, sleep_time = 0.2)
    print('Done !')

  
            
        
