def launch_insitu():
    for i in range(263,1000):
        #1st point
        umv(diffy,26.6,hz2,16.98)
        newsample('insitu_1000C_LNTO')
        newdataset('%04d'%i)
        dscan(hy,-1,1,20,0.1)
        dscan(hz,-0.9,1.1,20,0.1)
        #2nd point
        umv(diffy,26.6,hz2,40.38)
        newsample('insitu_NFO')
        newdataset('%04d'%i)
        dscan(hy,-1,1,20,0.1)
        dscan(hz,-0.9,1.1,20,0.1)
        #3rd point
        umv(diffy,25.6,hz2,64.18)
        newsample('insitu_C5NFO')
        newdataset('%04d'%i)
        dscan(hy,-1,1,20,0.1)
        dscan(hz,-0.9,1.1,20,0.1)
        #4th point
        umv(diffy,5.6,hz2,52.18)
        newsample('insitu_NLTO')
        newdataset('%04d'%i)
        dscan(hy,-1,1,20,0.1)
        dscan(hz,-0.9,1.1,20,0.1)
        #5th point
        umv(diffy,5.6,hz2,28.28)
        newsample('insitu_NYTO')
        newdataset('%04d'%i)
        dscan(hy,-1,1,20,0.1)
        dscan(hz,-0.9,1.1,20,0.1)        
        
        
def launch_pos1():
    for i in range(0,1000):
        #1st point
        umv(diffy,26.6,hz2,16.98)
        newsample('insitu_850C_LNTO_small_plateau')
        newdataset('%04d'%i)
        dscan(hy,-1,1,20,0.1)
        dscan(hz,-0.9,1.1,20,0.1)
