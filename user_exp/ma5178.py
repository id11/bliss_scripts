
import numpy as np

def m1_0N():
    zcen = 5.6
    zpos = zcen + np.linspace(-5,5,3)
    newdataset( 'M1_0N' )
    for z in zpos:
        umv( samtz, z )
        finterlaced( diffrz, -160, 0.1, 3200, 0.085 )
        


def m1_scan():
    finterlaced( diffrz, -15, -0.1, 1400, 0.1 )
    finterlaced( diffrz,  15,  0.1, 1400, 0.1 )
    
def m1_1000um():
    zcen = 4.5
    zpos = zcen + np.linspace(-3,3,7)
    newdataset( 'M1_1000um_z' )
    for z in zpos:
        umv( samtz, z )
        m1_scan()
        # finterlaced( diffrz, -160, 0.1, 3200, 0.085 )     
        
def m4_scan():
    newdataset('single_crystal')
    finterlaced( diffrz, 0, 0.1, 1800, 0.1 )
    
    
def m3_scan():
    newdataset('single_crystal_OK')
    finterlaced( diffrz, 0, 0.1, 3600, 0.1 )
    
       

        
