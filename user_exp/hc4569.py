


user_script_load("nscopemacros")
user_script_load("optics")




import time
import numpy as np
# 1.17 done
# sample top is at 1.07

def volscan():
    umv( shtz, 1.085 )

    for zpos in np.arange(1.085, 2.085, 0.04):
        umv( shtz, zpos )
        fscan2d(rot, 3060, -17, 181, dty,-800,5,321,.05, mode='ZIGZAG')
        umv( shtz, zpos + 0.02 )
        fscan2d(rot,    0,  17, 181, dty,-800,5,321,.05, mode='ZIGZAG')


#def pause_for_refill(t, cpos=10.138, upos=8.289):
def pause_for_refill(t, cpos=6.353, upos=8.287):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    if abs(cpm18.position - cpos)>0.02 or abs(cpm18t.position - 0.075)>0.02:
        user.cpm18_goto(cpos)
    if abs(u22.position - upos)>0.02:
        umv(u22, upos)


TESTING = False

def myfscan(*a,**k):
    print(a,k)
    if TESTING:
        return
    try:
        fscan(*a,**k)
    except KeyBoardInterrupt: # pretty hopeless but why not ?
        raise
    except:
        print("Something has gone wrong!!! Retry scans!!")
        elog_print("Retry a scan !!!")
        print("Sleep a minute or two first")
        time.sleep(60)
        pause_for_refill(120)
        fscan(*a,**k)
        
def myumv(*a,**k):
    print(a,k)
    if TESTING:
        return
    umv(*a,**k)

def nscopedtylayerscan(y_start, y_step, ny, a_time, rstart, rstep, rend):
    eiger.camera.photon_energy = 43569
    #    eiger.camera.photon_energy = 65350.8
    
    for i,r in enumerate(np.arange(rstart,rend,rstep)):
        myumv(rot, r )
        pause_for_refill(ny*a_time+1)
        if i%2 == 0:
            myfscan(dty, y_start, y_step, ny, a_time, scan_mode='CAMERA')
        else:
            myfscan(dty, y_start+y_step*ny, -y_step, ny, a_time, scan_mode='CAMERA')

def layer_hpt1(zpos):
    umv(pz, zpos)
    if not TESTING:
        newdataset('r1pz%03d'%(zpos))
    nscopedtylayerscan( -25.125, 0.25, 201, 0.1, -90, 1, 90.1)
    
def layers_hpt():
    for z in (49,51,53,55,57,59):
        layer_hpt1(z)
    
def layer_hpt2(zpos):
    umv(pz, zpos)
    if not TESTING:
        newdataset('r2pz%03d'%(zpos))
    nscopedtylayerscan( -25.125, 0.25, 201, 0.2, -90, 1, 90.1)   

# pz 45 px 59.9 py 80.95
# pz 52 px 57.8 py 82.80
# pz 60 px 61.1 py 85.28

def layers_relaxed():
    for z in np.arange(52,39,-1):
        umv(pz, z)
        if not TESTING:
            newdataset('r0pz%03d'%(z))
        nscopedtylayerscan( -26.125, 0.25, 209, 0.2, -90, 1, 90.1)   
        



def map2d(n2steps):
    tz = 0
    for zpos in np.arange(-nsteps,nsteps,1) :
         tz += 1
         umvr( pz, 0.5 )
         if tz%2 == 1:
            fscan(dty,-nsteps,0.5,2*nsteps,0.1,scan_mode='CAMERA')
         else:
            fscan(dty,nsteps,-0.5,2*nsteps,0.1,scan_mode='CAMERA')






def map2d50um():
    newdataset('map2d50um')
    for i,zpos in enumerate( np.arange(7, 0.29, -0.05) ):
        umv(samtz, zpos)
        # 2.09  ... 2.78
        pause_for_refill(100*0.134*2)
        if i%2 == 1:
           fscan(samty, 2.35,               0.05, 100, 0.134388 )
        else:
           fscan(samty, 2.35+99.5633*0.05, -0.05, 100,  0.134388 )

def map2d50um_rot():
    newdataset('map2d50um_rot')
    for i,zpos in enumerate( np.arange(7, 0.29, -0.05) ):
        umv(samtz, zpos)
        # 2.09  ... 2.78
        pause_for_refill(100*0.134*2)
        if i%2 == 1:
           fscan(samty, 0.95,               0.05, 100, 0.134388 )
        else:
           fscan(samty, 0.95+99.5633*0.05, -0.05, 100,  0.134388 )

def map2d50um_rot45():
    newdataset('map2d50um_rot45')
    for i,zpos in enumerate( np.arange(7, 0.29, -0.05) ):
        umv(samtz, zpos)
        # 2.09  ... 2.78
        pause_for_refill(100*0.134*2)
        if i%2 == 1:
           fscan(samty, 3.4,               0.05, 114, 0.134388 )
        else:
           fscan(samty, 3.4+113.5633*0.05, -0.05, 114,  0.134388 )  
           
           
           
def map2d_ribbon_0N():
 #    newdataset('2cuts_0N_340mm')
    for i,zpos in enumerate( np.arange(-5, 5, 0.1) ):
        umv(samtz, zpos)
        # 2.09  ... 2.78
        pause_for_refill(100*0.134*2)
        if i%2 == 1:
           fscan(samty, -4.6,                 0.05, 200, 0.134388 )
        else:
           fscan(samty, -4.6+199.5633*0.05,  -0.05, 200, 0.134388 )                                

