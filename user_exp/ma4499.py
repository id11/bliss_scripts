
import numpy as np, time

s11pos = {'diffty':-67.900000,
          'difftz':21.562000,
          'cz':0.300180,
          'ylo':-9.687000,
          'ylen':20, "done":True}
s12pos = { 'diffty':-47.6119,
           'difftz':21.562,
           'cz': 0.324,
           'ylo': -9.548,
           'ylen':20, "done":True,
           }
s13pos =  {'diffty':-27.211900,
           'difftz':21.562000,
           'cz':0.022140,
           'ylo':-9.372,
           'ylen':20 , "done":True,}
s14pos ={'diffty':-6.711900,
         'difftz':21.562000,
         'cz':0.106020,
         'ylo':-9.322000,
         'ylen':20 ,"done":True,}
s21pos ={'diffty':15.038100,
         'difftz':21.562000,
         'cz':-0.244140,
         'ylo':-9.506000,
         'ylen':20  ,"done":True,}
s22pos = {'diffty':35.538100,
          'difftz':21.562000,
          'cz':-0.167700,
          'ylo':-9.500000,
          'ylen':20 ,"done":True,}
# s23 and s24 do not exist



# 
s31pos = {'diffty':-67.900000,
          'difftz':-0.438000,
          'cz':0.453000,
          'ylo':-9.590000,
          'ylen':20,"done":True,}
s32pos = {'diffty':-47.400000,
          'difftz':-0.438000,
          'cz':0.756000,
          'ylo':-9.909000,
          'ylen':20,"done":True,}
s33pos = {'diffty':-27.300000,
          'difftz':-0.438000,
          'cz':0.761540,
          'ylo':-9.882040,
          'ylen':20,"done":True,}
s34pos = {'diffty':-7.100000,
          'difftz':-0.438000,
          'cz':0.931320,
          'ylo':-10.211920,
          'ylen':19.45,"done":True,}
# s41
s41pos = {'diffty':15.900000,
          'difftz':-0.438000,
          'cz':0.872300,
          'ylo':-10.914920,
          'ylen':19.45}
s42pos = {'diffty':35.600000,
          'difftz':-0.438000,
          'cz':0.732580,
          'ylo':-10.548140,
          'ylen':19.45}
s43pos = {'diffty':55.100000,
          'difftz':-0.438000,
          'cz':1.067200,
          'ylo':-10.521100,
          'ylen':20}
s44pos = {'diffty':75.100000,
          'difftz':-0.438000,
          'cz':1.141000,
          'ylo':-10.365840,
          'ylen':20}

s51pos = {'diffty':-68.561000,
          'difftz':21.895574,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':19.45,"done":True,}
s52pos = {'diffty':-49.060000, # this one looks damaged
          'difftz':21.689574,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':20, "done":True } # skip it 
s53pos = {'diffty':-28.737600,
          'difftz':21.765000,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':20,"done":True,}
s54pos = {'diffty':-8.201000,
          'difftz':21.718574,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':19.45,"done":True,}
s61pos = {'diffty':14.594900,
          'difftz':21.557719,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':20}
s62pos = {'diffty':34.751000,
          'difftz':21.730570,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':20}
s63pos = {'diffty':54.875200,
          'difftz':21.529715,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':20}
s64pos = {'diffty':74.319100,
          'difftz':21.492227,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':20}
s71pos = {'diffty':-69.033700,
          'difftz':1.278469,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':19.45}
s72pos = {'diffty':-49.582000,
          'difftz':1.376527,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':20}
s73pos = {'diffty':-29.369500,
          'difftz':1.312000,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':20}
s74pos = {'diffty':-9.061700,
          'difftz':1.394426,
          'cz':0.000000,
          'ylo':-10.000000,
          'ylen':20}



def saturday():
    for i in range(5,8):
        for j in range(1,5):
            s = "s%d%dpos"%(i,j)
            if s in globals():
                spos = globals()[s]
                print(s, spos)
                collect_data( s, spos )



def friday():
    for i in range(1,5):
        for j in range(1,5):
            s = "s%d%dpos"%(i,j)
            if s in globals():
                spos = globals()[s]
                print(s, spos)
                collect_data( s, spos )

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def refpos():
    print("{'diffty':%f,\n'difftz':%f,\n 'cz':%f,\n'ylo':%f,\n'ylen':20}\n"%( 
          diffty.position, difftz.position,samtz.position,samty.position))


def collect_data( name, spos ):
    if "done" in spos:
        print("done, goto next")
        return
    ylo = spos['ylo']
    ylen = spos['ylen']
    cz = spos['cz']
    newsample(name)
    plotselect("frelon3:roi_counters:roi1_sum")
    newdataset("0010")
    umv(diffty, spos['diffty'] )
    umv(difftz, spos['difftz'] )
    for yfrac in (0.2, 0.4, 0.6, 0.8):
        # find the surface height
        umv(samty, yfrac * ylen + ylo)
        umv(samtz, cz)
        # Coarse alignment scan
        pause_for_refill( 100*.1 + 10 ) 
        dfscan(samtz,-.2,.2,100,.1)
        cz = peak()
        umv(samtz, cz)
        for ypos in np.linspace(-0.05, 0.05, 10):
            umv(samty, yfrac * ylen + ylo + ypos)
            zcen = cz
            pause_for_refill( 200*.1 + 10 ) 
            fscan(samtz, zcen-0.05, 0.0005, 200, 0.1)  # 38 seconds

def dfscan( mot, lo, hi, npt, tim):
    b4 = mot.position
    st = (hi - lo)/npt
    spd = st/tim
    if spd > mot.velocity:
        print("Too fast")
        return
    try:
        fscan(mot, b4 + lo, st, npt, tim)
    except Exception as e:
        print("scan failed",str(e))
    finally:
        umv(mot, b4)
        

def afscan( mot, lo, hi, npt, tim):
    st = (hi - lo)/npt
    spd = st/tim
    if spd > mot.velocity:
        print("Too fast")
        return
    try:
        fscan(mot, lo, st, npt, tim)
    except Exception as e:
        print("scan failed",str(e))

        
        
    
          
