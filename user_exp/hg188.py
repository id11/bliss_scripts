

### To load in nscope session:
### user_script_load('ch6365')



import numpy as np, time
user_script_load('optics')

# def check_cpm18(pos=6.4158):
def check_cpm18(pos=6.3733):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()


def half1(ymax, datasetname, ystep=0.25, ymin=-85, expos=0.04):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  181/int(((ymax-ymin)/ystep)*1.5), int(((ymax-ymin)/ystep)*1.5), expos )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 181,  -181/int(((ymax-ymin)/ystep)*1.5), int(((ymax-ymin)/ystep)*1.5), expos )
    
    
def night_scan():
    umv(shtz,0.78)
    half1(50, '3DXRD_top_layer_1', ystep=0.25, ymin=-50, expos=0.05)
    umv(shtz,0.85)
    half1(100, '3DXRD_bottom_layer_1', ystep=0.25, ymin=-100, expos=0.02)
    sheh3.close()
    
def backto_nscope():
    umv(d3ty,180)
    fsh.session='NSCOPE'
