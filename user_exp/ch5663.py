

def collect(expo=0.1):
    fscan(rot, 22, 0.1, (157-22)/0.1, expo)
    fscan(rot, 209, 0.1, (335-209)/0.1, expo )



def collect35(expo=0.1):
    fscan(rot, 35, 0.1, (144.5-35)/0.1, expo)
    fscan(rot, 213, 0.1, (324.5-213)/0.1, expo )



def collect20(expo=0.1):
    fscan(rot, 20, 0.1, (153.-20)/0.1, expo)
    fscan(rot, 203, 0.1, (330.-203)/0.1, expo )


def collect18(expo=0.1):
    fscan(rot, 18, 0.1, (147.-18)/0.1, expo)
    fscan(rot, 198, 0.1, (327.-198)/0.1, expo )

def collectbye(expo=0.1):
    fscan(rot, 24, 0.1, (151.-24)/0.1, expo)
    fscan(rot, 204, 0.1, (331.-204)/0.1, expo )


def collect3():
    tfoh1.set(0, "Be" )
    newdataset("01d01s")
    fscan(rot, 0, 0.1, 360/0.1, 0.1,scan_mode='CAMERA')
    tfoh1.set(8, "Be" )
    newdataset("01d01s_8Be")
    fscan(rot, 0, 0.1, 360/0.1, 0.1,scan_mode='CAMERA')
    tfoh1.set(12, "Be" )
    newdataset("01d01s_12Be")
    fscan(rot, 0, 0.1, 360/0.1, 0.1,scan_mode='CAMERA')
    tfoh1.set(0, "Be" )
    umv(rot,0)
    
    

def collect4():
    tfoh1.set(0, "Be" )
    newdataset("01d01s")
    fscan(rot, 0, 0.1, 360/0.1, 0.1,scan_mode='CAMERA')
    newdataset("01d1s")
    fscan(rot, 0, 0.1, 360/0.1, 1,scan_mode='CAMERA')
    tfoh1.set(8, "Be" )
    newdataset("01d1s_8Be")
    fscan(rot, 0, 0.1, 360/0.1, 1,scan_mode='CAMERA')
    tfoh1.set(12, "Be" )
    newdataset("01d1s_12Be")
    fscan(rot, 0, 0.1, 360/0.1, 1,scan_mode='CAMERA')
    tfoh1.set(0, "Be" )
    umv(rot,0)
    sheh3.close()


def collect5():
    tfoh1.set(12, "Be" )
    newdataset("01d01s_12Be")
    fscan(rot, 0, 0.1, 360/0.1, 0.1,scan_mode='CAMERA')
    newdataset("01d05s_12Be")
    fscan(rot, 0, 0.1, 360/0.1, 0.5,scan_mode='CAMERA')
    tfoh1.set(0, "Be" )
    umv(rot,0)
    
def collect6():
    tfoh1.set(0, "Be" )
    newdataset("01d01s")
    fscan(rot, 0, 0.1, 360/0.1, 0.1,scan_mode='CAMERA')
    tfoh1.set(8, "Be" )
    newdataset("01d01s_8Be")
    fscan(rot, 0, 0.1, 360/0.1, 0.1,scan_mode='CAMERA')
    tfoh1.set(0, "Be" )
    umv(rot,0)

def collect7():
    tfoh1.set(12, "Be" )
    newdataset("01d01s_12Be")
    fscan(rot, 0, 0.1, 360/0.1, 0.1,scan_mode='CAMERA')
    newdataset("01d05s_12Be")
    fscan(rot, 0, 0.1, 360/0.1, 0.7,scan_mode='CAMERA')
    tfoh1.set(0, "Be" )
    umv(rot,0)
    

def collect2esp():
    newdataset('025s01s16Be')
    fscan(rot,0,0.25,1440,0.1,scan_mode='CAMERA') 
    user.makesperanto(0.29339,1041,1126,137.69,1) 
    newdataset('05s05s16Be')
    fscan(rot,0,0.5,720,0.5,scan_mode='CAMERA') 
    sheh3.close()
    user.makesperanto(0.29339,1041,1126,137.69,1)

