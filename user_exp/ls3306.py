import numpy as np
import time
from bliss.common import cleanup


def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
        


def mesh_scan(py_range = [25, 75], pz_range = [25, 75], step_size = 0.2, exp_time = 0.1, sample_inclined_ang = 135, tol = 2.5):
    py0 = py_range[0]
    pz0 = pz_range[0]
    pys = np.arange(py_range[0], py_range[1]+step_size, step_size)
    pzs = np.arange(pz_range[0], pz_range[1]+step_size, step_size)
    
    for i, pz_pos in enumerate(pzs):
        umv(pz, pz_pos)
        py_scan_start = pys[-1] + (pz_pos - pz0)*np.tan(np.deg2rad(sample_inclined_ang)) - tol
        py_scan_end = py_scan_start + tol*3;
        print(f'Line {i}: pz_pos = {pz_pos: .2f}, scan py from {py_scan_start: .2f} to {py_scan_end: .2f} with a step size of {step_size}')
        npts = round((py_scan_end - py_scan_start) / step_size)
        ascan(py, py_scan_start, py_scan_end, npts, exp_time)
    print('Done successfully !')


def run_dmesh_fun():
    # roi1
    dmesh_fun('dmesh_roi1',py_range = [-30, 30], pz_range = [-30, 30])
    
    # roi2
    #dmesh_fun('dmesh_roi2',py_range = [-30, 30], pz_range = [-30, 30])
    
    # roi3
    #dmesh_fun('dmesh_roi3',py_range = [-30, 30], pz_range = [-30, 30])
    
    sheh3.close()
    print('Done successfully !')



def dmesh_fun(dsetname, py_range = [-30, 30], pz_range = [-30, 30],exp_time = 2, step_size = 0.2):
    npts_y = round((py_range[1] - py_range[0])/step_size)
    npts_z = round((pz_range[1] - pz_range[0])/step_size)
    umv(rot, 0)
    newdataset(dsetname)
    dmesh(py, py_range[0], py_range[1], npts_y, pz, pz_range[0], pz_range[1], npts_z, exp_time)
    
    
def run_xrd():
    tfoh1.set(0, 'Be')
    newdataset('73o_9_XRD_120s')
    fscan(rot, -10, 0.25, 20/0.25, 120)
    tfoh1.set(18,'Be')
    newdataset('73o_9_XRD_120s_18lenses')
    fscan(rot, -10, 0.25, 20/0.25, 120)
    
    tfoh1.set(0, 'Be')
    umv(rot, 0)
    newdataset('mesh')
    amesh(py, 20, 80, 300, pz, 20, 80, 300, 0.25)
    print('Done')
    sheh3.close()
    
     
def run_align_and_mesh(angle = 10, prefix = 'ROI3', dty_min = -500, dty_max = 500, dty_step = 0.5, exp_time = 0.1):
    angle = round(angle)
    npts = round((dty_max-dty_min)/dty_step)
    # align
    umvct(rot, angle)
    newdataset('align')
    fscan(dty, dty_min, dty_step, npts, exp_time)
    goto_peak()
    # mesh mapping
    dset_name = f'{prefix}_mesh_{angle}deg'
    newdataset(dset_name)
    dmesh(py,-10,10,100,pz,-10,10,100,0.15)
    ## collect xrd data
    #dset_name = f'{prefix}_xrd_{angle}deg'
    #newdataset(dset_name)
    #ftimescan(90, 10)

    # to collect data for the fiber, please run fiber_xrd


#pos_list = [[80.051, 56.024], [84.981, 57.667], [85.391, 64.768], [87.534, 58.612]]
#pos_list = [[49.652, 49.764], [54.171, 51.231], [55.521, 58.147], [57.545, 52.069]]
pos_list = [[53.141, 61.841], [57.840, 64.065], [69.270, 72.483], [80.828, 78.514], [81.717, 73.753], [69.651, 70.259], [19.993, 27.535], [20.247, 23.087], [44.124, 56.918]]
def run_fiber_xrd(pos_list = pos_list, prefix='ROI3_large_night_fiber'):
    angle = round(rot.position)
    py0 = py.position
    pz0 = pz.position    
    for i, pos in enumerate(pos_list):
        py_value = pos[0]
        pz_value = pos[1]   
        print('Go to the target position ...')
        umv(py, py_value, pz, pz_value)
        dset_name = f'{prefix}{i}_xrd_{angle}deg'
        print(dset_name)
        newdataset(dset_name)
        ftimescan(90, 10)
    # in the end, you want to come back to the initial position
    print('Come back to the initial position ...')
    umv(py, py0, pz, pz0)
    print('Done')










