import time
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

#IMPORTANT: Run ftimescan instead of loopscans

def xrd_capillary_scan_1():
    newsample('rack1')

    #Sample names WITHOUT CeO2 and Kapton
    sample_names = ['bkg_small', 'S5_AuPure_MOF', 'S4_AuNH2_MOF','Wupey_S4', 'Wupey_S3','Wupey_S2','S3_8Rh_MOF','S2_4Rh_MOF','S1_2Rh_MOF','Dy_LBT_Qt_1000C','Dy_Qt_WT','Dy_LBT_Qt_600C','Dy_LBT_gPFD_LBT','Dy_LBT_gPFD','Dy_gPFD_Wt','Dy_LBT_Qt','Dy_NO3','Dy2O3','bkg','CeO2_cal']
    diffy_positions = [1.83,4.26,6.83,9.32,11.7,14.2,16.63,19.32,21.8,24.26,26.76,29.26,31.56,34.2,36.80,39.2,41.75,44.13,46.7,49.18]
    sheh1.open()

    #CeO2
    #newdataset('CeO2')
    #umv(diffy,-1.6) #To Modify
    #pause_for_refill(60)
    #loopscan(30,0.1)

	#Kapton
    #newdataset('kapton')
    #umv(diffy,2.14) #To Modify
    #pause_for_refill(60)
    #loopscan(20*60,1)

    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(120,0.5)

def pdf_capillary_scan_1():
    newsample('rack1_pdf')

    #CeO2
    umv(s4hg,0.1,s4vg,0.1)
    newdataset('CeO2')
    umv(diffy,49.05) #To Modify
    pause_for_refill(60)
    loopscan(60,0.1)

    #Sample names WITHOUT CeO2
    sample_names = ['bkg_small', 'Wupey_S4', 'Wupey_S3','Wupey_S2','S3_8Rh_MOF','S2_4Rh_MOF','S1_2Rh_MOF','Dy_LBT_Qt_1000C','Dy_Qt_WT','Dy_LBT_Qt_600C','Dy_LBT_gPFD_LBT','Dy_LBT_gPFD','Dy_gPFD_Wt','Dy_LBT_Qt','Dy2O3','bkg']
    diffy_positions = [1.76,9.08,11.72,14.07,16.69,19.18,21.67,24.16,26.72,29.00,31.63,34.05,36.67,39.29,44.07,46.69]
    sheh1.open()

    umv(s4hg,0.3,s4vg,0.3)
    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(2*5*60,0.5)



def xrd_capillary_scan_2():
    newsample('rack2')

    #Sample names WITHOUT CeO2 and Kapton
    sample_names = ['CeO2_cal','ClickTi_1500','ClickTi_1100', 'ClickTi_800','ClickHf_1500','ClickHf_1100','ClickHf_800','ClickZr_1500','ClickZr_1100','ClickZr_800','ClickMix_1500','ClickMix1100','ClickMix_800','VB_B2_R70C','VB_B3_R12B','VB_B3_R12E','VB_B3_R13_1000C','VB_B3_R13_1200C','small_bkg2']
    diffy_positions = [1.29,4.05,6.54,8.82,11.45,13.8,16.42,19.05,21.47,23.89,26.51,28.93,31.49,33.84,36.40,39.02,41.38,43.93,46.7]
    sheh1.open()

    #CeO2
    #newdataset('CeO2')
    #umv(diffy,-1.6) #To Modify
    #pause_for_refill(60)
    #loopscan(30,0.1)

	#Kapton
    #newdataset('kapton')
    #umv(diffy,2.14) #To Modify
    #pause_for_refill(60)
    #loopscan(20*60,1)

    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(120,0.5)
  
  
def pdf_capillary_scan_2():
    newsample('rack2_pdf')

    #CeO2
    #umv(s4hg,0.1,s4vg,0.1)
    #newdataset('CeO2')
    #umv(diffy,1.29) #To Modify
    #pause_for_refill(60)
    #loopscan(60,0.1)

    #Sample names WITHOUT CeO2
    sample_names = ['ClickTi_1100', 'ClickTi_800','ClickHf_1500','ClickHf_1100','ClickHf_800','ClickZr_1500','ClickZr_1100','ClickZr_800','ClickMix_1500','ClickMix1100','ClickMix_800','VB_B2_R70C','VB_B3_R12B','VB_B3_R12E','VB_B3_R13_1000C','VB_B3_R13_1200C','small_bkg2']
    diffy_positions = [6.54,8.82,11.45,13.8,16.32,19.05,21.47,23.89,26.51,28.93,31.49,33.98,36.40,39.02,41.38,43.87,46.42]
    sheh1.open()

    umv(s4hg,0.3,s4vg,0.3)
    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(2*5*30,0.5)
  
        
def xrd_capillary_scan_3():
    newsample('rack3')

    #Sample names WITHOUT CeO2 and Kapton
    sample_names = ['CeO2_cal','PZ20_Zr_1700C','PZ20_Zr_1500C','PZ20_Zr_1000C','PZ20_Ti_1700C','PZ20_Ti_1500C','PZ20_Ti_1000C','PZ20_Hf_1700C','PZ20_Hf_1500C','PZ20_Hf_1000C','PZ20_TiZrHf_1700C_N2','PZ20_TiZrHf_1700C_Ar','PZ20_TiZrHf_1000C','SiC_std','OPCo30','SiC1200','SiC800','ZrVMoHf']
    diffy_positions = [1.56,4.18,6.74,9.23,11.58,14.07,16.69,19.05,21.67,24.09,26.72,29.14,31.69,34.11,36.64,39.16,41.78,44.14]
    sheh1.open()

    #CeO2
    #newdataset('CeO2')
    #umv(diffy,-1.6) #To Modify
    #pause_for_refill(60)
    #loopscan(30,0.1)

	#Kapton
    #newdataset('kapton')
    #umv(diffy,2.14) #To Modify
    #pause_for_refill(60)
    #loopscan(20*60,1)

    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(120,0.5)
 
def pdf_capillary_scan_3():
    newsample('rack3_pdf')

    #CeO2
    umv(s4hg,0.1,s4vg,0.1)
    newdataset('CeO2')
    umv(diffy,2.03) #To Modify
    pause_for_refill(60)
    loopscan(60,0.1)

    #Sample names WITHOUT CeO2
    sample_names = ['PZ20_Zr_1700C','PZ20_Zr_1500C','PZ20_Zr_1000C','PZ20_Ti_1700C','PZ20_Ti_1500C','PZ20_Ti_1000C','PZ20_Hf_1700C','PZ20_Hf_1500C','PZ20_Hf_1000C','PZ20_TiZrHf_1700C_N2','PZ20_TiZrHf_1700C_Ar','PZ20_TiZrHf_1000C','OPCo30','SiC1200','SiC800','ZrVMoHf','bkg','bkg_small']
    diffy_positions = [4.25,6.74,9.23,11.72,14.27,16.90,19.18,21.67,24.29,26.92,29.34,31.69,36.81,39.23,41.78,44.27,46.62,49.31]
    sheh1.open()

    umv(s4hg,0.3,s4vg,0.3)
    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(2*5*30,0.5) 
 
def xrd_capillary_scan_4():
    newsample('rack4')

    #Sample names WITHOUT CeO2 and Kapton
    #sample_names = ['CeO2_cal','PZ20_1700C','PZ20_1500C','PZ20_1000C','HfC_std','TiC_std','ZrC_std','Si3N4_std','SiC_std','ZrO2_std','NiNO3_std','Cu5Si_std','SiOC900_0p5Ni','SiOC600_0p5Ni','TiZrHfNbTaMo_0p1M','VNbMoZr_0p1M','CrVNbZrMo_0p1M','USYD_Q4']
    #diffy_positions = [1.83,4.25,6.81,9.16,11.78,14.34,16.69,19.32,21.81,24.23,26.72,29.20,31.89,34.25,36.80,39.29,41.85,44.2]
    sample_names = ['CrVNbZrMo_0p1M','USYD_Q4']
    diffy_positions = [41.85,44.2]
    sheh1.open()

    #CeO2
    #newdataset('CeO2')
    #umv(diffy,-1.6) #To Modify
    #pause_for_refill(60)
    #loopscan(30,0.1)

	#Kapton
    #newdataset('kapton')
    #umv(diffy,2.14) #To Modify
    #pause_for_refill(60)
    #loopscan(20*60,1)

    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(120,0.5)       
   
def pdf_capillary_scan_4():
    newsample('rack4_pdf')

    #CeO2
    umv(s4hg,0.1,s4vg,0.1)
    newdataset('CeO2')
    umv(diffy,2.10) #To Modify
    pause_for_refill(60)
    loopscan(60,0.1)

    #Sample names WITHOUT CeO2
    sample_names = ['PZ20_1700C','PZ20_1500C','PZ20_1000C','SiOC900_0p5Ni','SiOC600_0p5Ni','TiZrHfNbTaMo_0p1M','VNbMoZr_0p1M','CrVNbZrMo_0p1M','USYD_Q4']
    diffy_positions = [4.45,6.87,9.36,31.89,34.38,36.80,39.29,41.85,44.27]
    sheh1.open()

    umv(s4hg,0.3,s4vg,0.3)
    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(2*5*30,0.5)    
      
def xrd_capillary_scan_5():
    newsample('rack5')

    #Sample names WITHOUT CeO2 and Kapton
    sample_names = ['CeO2_cal','HEO_1_14','BaHEO','BaHEOY_defi','BCZFY_4411','HEO11','HEO10','HEO7','HEO4','HEO1','HEO_1_15_4x2','ZrWHfMo','TiZrHfNbTa_1M','TiZrHfNbTa_0p1M_pyr','TiZrHfNbTa_0p1M','OPCo30_2','TiZrHfNbTaMoW_0p1M','ZrVMoHf_pyr','USYD_Q2','USYD_Q3']
    diffy_positions = [1.96,4.52,6.94,9.43,11.85,14.47,16.96,19.38,21.87,24.43,26.92,29.54,31.83,34.32,36.99,39.56,41.98,44.54,46.89,49.31]
    sheh1.open()

    #CeO2
    #newdataset('CeO2')
    #umv(diffy,-1.6) #To Modify
    #pause_for_refill(60)
    #loopscan(30,0.1)

	#Kapton
    #newdataset('kapton')
    #umv(diffy,2.14) #To Modify
    #pause_for_refill(60)
    #loopscan(20*60,1)

    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(120,0.5)
 
def pdf_capillary_scan_5():
    newsample('rack5_pdf')

    #CeO2
    #umv(s4hg,0.1,s4vg,0.1)
    #newdataset('CeO2')
    #umv(diffy,1.43) #To Modify
    #pause_for_refill(60)
    #loopscan(60,0.1)

    #Sample names WITHOUT CeO2
    sample_names = ['BCZFY_4411','HEO11','HEO10','HEO7','HEO4','HEO1','HEO_1_15_4x2','ZrWHfMo','TiZrHfNbTa_1M','TiZrHfNbTa_0p1M_pyr','TiZrHfNbTa_0p1M','OPCo30_2','TiZrHfNbTaMoW_0p1M','ZrVMoHf_pyr','USYD_Q2','USYD_Q3']
    diffy_positions = [11.85,14.34,16.9,19.38,21.87,24.36,26.85,29.47,31.76,34.25,36.94,39.43,41.92,44.40,46.76,49.31]
    sheh1.open()

    umv(s4hg,0.3,s4vg,0.3)
    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(2*5*30,0.5) 
          
def xrd_capillary_scan_6():
    newsample('rack6')

    #Sample names WITHOUT CeO2 and Kapton
    sample_names = ['CeO2_cal','bkg_MOF','MOF_808','MOF_808_RDS','nMOF_808_RDS','RDS','nMOF_808','nMOF_808_gly','MOF_808_gly']
    diffy_positions = [1.02,3.85,6.34,8.82,11.31,13.67,16.29,18.71,21.2]
    sheh1.open()

    #CeO2
    #newdataset('CeO2')
    #umv(diffy,-1.6) #To Modify
    #pause_for_refill(60)
    #loopscan(30,0.1)

	#Kapton
    #newdataset('kapton')
    #umv(diffy,2.14) #To Modify
    #pause_for_refill(60)
    #loopscan(20*60,1)

    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(120,0.5)          

def pdf_capillary_scan_6():
    newsample('rack6_pdf')

    #CeO2
    #umv(s4hg,0.1,s4vg,0.1)
    #newdataset('CeO2')
    #umv(diffy,1.22) #To Modify
    #pause_for_refill(60)
    #loopscan(60,0.1)

    #Sample names WITHOUT CeO2
    sample_names = ['nMOF_808','nMOF_808_gly','MOF_808_gly']
    diffy_positions = [16.42,18.98,21.4]
    sheh1.open()

    umv(s4hg,0.3,s4vg,0.3)
    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(2*60*30,0.5) 

def pdf_capillary_scan():
	sheh1.open()
	#Calib scans sample Door
	newdataset('CeO2')
	umv(diffy,-8.06,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(30,0.1)
	newdataset('kapton1')
	umv(diffy,-4.08,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(15*60,1)
	newdataset('CuAl_LDH')
	umv(diffy,0,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuCo_LDH')
	umv(diffy,4,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuCe_LDH')
	umv(diffy,8.63,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuCr_LDH')
	umv(diffy,12,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuFe_LDH')
	umv(diffy,16,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuGa_LDH')
	umv(diffy,20,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuIn_LDH')
	umv(diffy,24.3,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuLa_LDH')
	umv(diffy,28,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuMn_LDH')
	umv(diffy,32,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuNd_LDH')
	umv(diffy,36,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuV_LDH')
	umv(diffy,40,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('CuCo_UT')
	umv(diffy,44.7,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('NiAl_LDH')
	umv(diffy,48,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('NiCo_LDH')
	umv(diffy,52,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	newdataset('NiCe_LDH')
	umv(diffy,56.1,hy,0,hz,10,hz2,53.1493)
	pause_for_refill(60)
	loopscan(7.5*60,1)
	sheh1.close()
