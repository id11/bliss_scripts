def test_macro():
    nc.half1(1, 'test_macro', ystep=1)

def overnight():
    # work out positions
    # for loop on positions
    for pos in [3.3, 3.8, 4.3, 4.8]:
        # move ntz to that position
        umv(ntz, pos)
        pos_str = str(pos).replace('.','p')
        nc.half1(ymax=650,
                 datasetname=f'layer_{pos_str}',
                 ystep=1.0)
        umv(dty, 0, rot, 0)
