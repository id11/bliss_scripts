
import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime

atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')


def scan_ffdtx1():
    newdataset('scan_ffdtx1_in')
    dists = np.arange(720, 240, -10)
    for dist in dists:
        umv(ffdtx1, dist)
        sleep(1)
        ftimescan(3, 10)
    
    newdataset('scan_ffdtx1_out')
    dists = np.arange(250, 730, 10)
    for dist in dists:
        umv(ffdtx1, dist)
        sleep(1)
        ftimescan(3, 10)
    
    sheh3.close()
    print('Done !')
    
    
def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.1
    dct_pars['refon'] = dct_pars['num_proj']
    dct_pars['ref_step'] = -2
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.8
    dct_pars['slit_vg'] = 0.5
    dct_pars['dist'] = 6.2
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 4.5878
    dct_pars['shift_step_size'] = 0.45
    dct_pars['nof_shifts'] = 1
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars

def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 0.1
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.085
    ff_pars['slit_hg'] = 0.9
    ff_pars['slit_vg'] = 0.1
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = 3.22454
    ff_pars['shift_step_size'] = 0.08
    ff_pars['nof_shifts'] = 5
    ff_pars['cpm18_detune'] = 0
    ff_pars['ceo2_samtx'] = 0.1850
    ff_pars['ceo2_samty'] = -0.7652
    ff_pars['ceo2_samtz'] = 4.6154
    ff_pars['ceo2_difftz'] = -21.7648
    return ff_pars
    

def switch_to_marana1_dct():
    marana1_in()
    ACTIVE_MG.disable('marana3*')
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana1*')
    #tfoh1.set(16, 'Be')
    umvct(s7vg, 1.5, s7hg, 1.5, s8vg, 1.6)
    return 'Done'

def switch_to_marana3_pct():
    marana3_in()
    ACTIVE_MG.disable('marana1*')
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    tfoh1.set(0, 'Be')
    umvct(s7vg, 1.5, s7hg, 1.5, s8vg, 1.6)
    return 'Done'

def switch_to_ff():
    fsh.close()
    tfoh1.set(0,'Be')
    ff_in()
    umv(atty,0,attrz,-10)
    ACTIVE_MG.disable('marana3*')
    ACTIVE_MG.disable('marana1*')
    ACTIVE_MG.enable('frelon3*')
    fsh.open()
    umvct(s7vg, 0.9, s7hg, 0.9, s8vg, 1.0)


def marana3_in():
    ff_out()
    marana1_out()
    assert d1tz.position > 90
    assert d2tz.position > 100
    assert d1ty.position < -240
    assert d2ty.position < -40
    umv(nfdtx, 100)
    umv(d3ty, 0)
    umv(d3tz, 0)
    umvct(atty,-10,attrz,0)
    return 'Done'
    
def marana1_in():
    ff_out()
    marana3_out()
    assert ffdtx1.position > 390
    assert ffdtz1.position > 300
    assert d3ty.position > 130
    assert nfdtx.position < 203 and nfdtx.position > 190
    umv(d1tz, 0, d2tz, 27.47)
    umv(d2ty, 203.625)
    umv(d1ty, 0)
    umvct(atty,-10,attrz,0)
    #umv(d1x, 5) # if the sample has not been centered, this will hit the sample

def ff_in():
    marana3_out()
    marana1_out()
    assert d3ty.position > 110
    assert d2ty.position < -40
    umv(ffdtz1, 0)
    umv(ffdtx1, 180)
    
def marana1_out():
    umv(nfdtx, 200)
    umv(d1ty, -245)
    umv(d2ty, -47)
    umv(d1tz, 100, d2tz, 126)
    
    
def marana3_out():
    umv(nfdtx,200)
    umv(d3ty,150)

def ff_out():
    umv(ffdtx1, 600)
    umv(ffdtz1, 400)   
    
def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'], s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    #umv(s8vg, dct_pars['slit_vg'] + 0.05, s8hg, dct_pars['slit_hg'] + 0.05)
    #umv(s8vg, dct_pars['slit_vg'] + 0.05)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])
    
def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')
    
    
def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
#    umv(s8vg, pars['slit_vg'] + 0.05, s8hg, pars['slit_hg'] + 0.05)
    #umv(s8vg, pars['slit_vg'] + 0.05)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])


def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)

def ff_zseries(ff_pars, datasetname = 'ff'):
    #user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    #umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    umv(diffrz, ff_pars['start_pos'], s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
#    umv(s8vg, ff_pars['slit_vg'] + 0.05, s8hg, ff_pars['slit_hg'] + 0.05)
    #umv(s8vg, ff_pars['slit_vg'] + 0.05)
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    #user.cpm18_goto(cpm18_tuned)
    #umv(u22, u22_tuned)
    print('ff_zseries succeed')
    return('Succeed')


