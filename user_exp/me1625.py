import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime


def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 0.1
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.12
    ff_pars['slit_hg'] = 1
    ff_pars['slit_vg'] = 0.05
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = -2.29
    ff_pars['shift_step_size'] = 0.03
    ff_pars['nof_shifts'] = 30
    return ff_pars


def ff_zseries(ff_pars, datasetname = 'ff'):
    #user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    #umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    #user.cpm18_goto(cpm18_tuned)
    #umv(u22, u22_tuned)
    print('ff_zseries succeed')
    return('Succeed')



def define_ff_load_pars():
    ff_pars={}
    # the front side
    ff_pars['start_pos1'] = 33
    ff_pars['stop_pos1'] = 140
    # the back side
    ff_pars['start_pos2'] = -145
    ff_pars['stop_pos2'] = -38
    ff_pars['step_size'] = 0.2
    ff_pars['num_proj1'] = (ff_pars['stop_pos1'] - ff_pars['start_pos1']) / ff_pars['step_size']
    ff_pars['num_proj2'] = (ff_pars['stop_pos2'] - ff_pars['start_pos2']) / ff_pars['step_size']
    ff_pars['exp_time'] = 0.12
    ff_pars['slit_hg'] = 1.4
    ff_pars['slit_vg'] = 0.05
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = -3.22
    ff_pars['shift_step_size'] = 0.03
    ff_pars['nof_shifts'] = 12
    return ff_pars

def ff_zseries_load(ff_pars, datasetname = 'ff_load'):
    #user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    #umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        print('Start measurement for the front side')
        finterlaced(diffrz, ff_pars['start_pos1'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj1'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
        print('Go to the back side ...')
        umv(diffrz,ff_pars['start_pos2'])
        print('Start measurement for the back side')
        finterlaced(diffrz, ff_pars['start_pos2'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj2'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    umv(diffrz, ff_pars['start_pos1'])
    #user.cpm18_goto(cpm18_tuned)
    #umv(u22, u22_tuned)
    print('ff_zseries_load succeed')
    return('Succeed')



def tomo_load_390N(datasetname = 'pct_150mm_390N'):
    tomo_pars={}
    tomo_pars['start_pos1'] = 33
    tomo_pars['stop_pos1'] = 140
    # the back side
    tomo_pars['start_pos2'] = -145
    tomo_pars['stop_pos2'] = -38
    tomo_pars['step_size'] = 0.05
    tomo_pars['exp_time'] = 0.05
    newdataset(datasetname)
    print('Start measurement for the front side')
    fscan(diffrz, tomo_pars['start_pos1'],0.05,(tomo_pars['stop_pos1']-tomo_pars['start_pos1'])/tomo_pars['step_size'],tomo_pars['exp_time'])
    umv(diffrz,ff_pars['start_pos2'])
    print('Start measurement for the back side')
    fscan(diffrz, tomo_pars['start_pos2'],0.05,(tomo_pars['stop_pos2']-tomo_pars['start_pos2'])/tomo_pars['step_size'],tomo_pars['exp_time'])
    # move to ref position
    umv(diffrz, 90)
    umvr(samy,2)
    ftimescan(tomo_pars['exp_time'],41)    
    sheh3.close()                   
    ftimescan(tomo_pars['exp_time'],41)
    umvr(samy,-2)
    return('Succeed')









