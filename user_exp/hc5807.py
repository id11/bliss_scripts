
import time

def collect_still(still_rot_angs):
    """ Collect stills at the listed angles"""
    
    temp=gasblower.setpoint
    
    newdataset(f'stills_{temp:.0f}')
    
    for ang in list(still_rot_angs):
   
        umv(rot, ang)
    
        sct(0.1)
        sct(1)
        sct(10)
    
    umvr(ntz, -0.05)
    
    for ang in still_rot_angs:
        
        umv(rot, ang)
    
        newdataset(f'stills_{temp:.0f}_bkg')
    
        sct(0.1)
        sct(1)
        sct(10)
    
    umvr(ntz, 0.05)

def measure_copper(offset = 0.08):
    """ Measure copper rod diffraction """

    umvr(ntz, offset)
    temp = gasblower.setpoint
    
    newdataset(f'copper_{temp:.0f}')
    
    sxscan(5,0.2,180,-180,1053,1114,frelx.position, wvln=12.3984/78.395)
    
    umvr(ntz, -offset)
    
def do_measure():
    """ Do sxscan, measure stills and measure copper at current T. """
    
    temp = gasblower.setpoint
    
    newdataset(f'{temp:.0f}C_attr15')
    
    sxscan(0.5, 0.5, 180, -180, 1053, 1114, frelx.position, wvln=12.3984/78.395)
    
    collect_still([35.821, -144.819])
    
    measure_copper()
    
def measure_two_flux():
    """ Measure with and without the attenuator """
    
    temp = gasblower.setpoint
    
    umv(attrz,0, atty, -10)
    
    newdataset(f'{temp:.0f}C_attr0')
    
    sxscan(0.5, 0.5, 180, -180, 1053, 1114, frelx.position, wvln=12.3984/78.395)
    
    umv(attrz,-7, atty, 0)
    
    newdataset(f'{temp:.0f}C_attr7')
    
    sxscan(0.5, 0.5, 180, -180, 1053, 1114, frelx.position, wvln=12.3984/78.395)
    
    umv(attrz,0, atty, -10)



def night():

    for tset in [500, 600, 700, 800, 900]:
        tcurrent = gasblower.setpoint
    
        gasblower.setpoint = tset
        
        # Calculate sleep time based on 8.5 K/min ramping + 300 seconds
        sleep_delay = (tset - tcurrent) / 0.14166667 + 300
        print(f"Sleeping for {sleep_delay/60:.2f} minutes")
        time.sleep( sleep_delay )
            
        # Takes ~13 minutes
        measure_two_flux()
        
    # cool down
    gasblower.setpoint = 25    
        
        
