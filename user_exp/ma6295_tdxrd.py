import sys
import numpy as np
import time
import os

atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')



def switch_to_marana3_pct():
    fsh.session = 'TDXRD'
    fsh.close()
    marana3_in()
    fsh.open()
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    #umvct(d3_bstz, 2000)
    tfoh1.set(0, 'Be')
    plotimagecenter(marana3)
    slit(2.5, 2.5)
    return 'Done'


def switch_to_ff(scan_bs=True):
    fsh.session = 'TDXRD'
    fsh.close()
    #tfoh1.set(18,'Be')
    ff_in(scan_bs)
    umv(atty,0,attrz,-8)
    ACTIVE_MG.disable('marana3*')
    ACTIVE_MG.enable('frelon3*')
    fsh.open()
    slit(0.1, 1)
    ct(0.185)


def marana3_in():
    ff_out()
    assert d1tz.position > 90
    #assert d2tz.position > 100
    assert d1ty.position < -200
    assert d2ty.position < -387
    assert ffdtx1.position > 440
    umv(nfdtx, 200)
    assert nfdtx.position > 95
    umv(d3ty, 0)
    umv(d3tz, 0)
    umvct(atty,-10,attrz,0)
    return 'Done'
    
    
def ff_in(scan_bs=False):
    marana3_out()
    slit(0.1, 1)
    assert d3ty.position > 169.0
    assert nfdtx.position < 55.0
    assert d2ty.position < -387
    umv(ffdtz1, 0)
    umv(ffdtx1, 300)
    if scan_bs:
        open_ffdoor(detector_name = 'frelon3')
    else:
        umv(ffdoor,-170)
        
    
def marana3_out():
    if furnace_z.position < 70:
        umv(d3ty,170)
        assert d3ty.position > 169.0
        umv(nfdtx,50)
    else:
        umv(d3ty,170)
        umv(nfdtx,50)
    

def ff_out():
    umv(ffdoor, 0)
    umv(ffdtx1, 450)
    umv(ffdtz1, 400)

def remove_furnace():
    umvct(furnace_z, 115)
    
def insert_furnace():
    assert (nfdtx.position > 80.0 or d3ty.position > 169.0)
    umv(furnace_z, 0)

def fast_pct():
    marana3.image.binning=[2,2]
    marana3.image.roi = [0, 150, 1024, 750]

def slow_pct():
    marana3.image.binning=[1, 1]
    marana3.image.roi = [0, 300, 2048, 1500]    

def ref_pct():
    marana3.image.binning=[1, 1]
    marana3.image.roi = [0, 0, 2048, 2048]    

def collect_flat_and_dark():
    sheh3.open()
    umvr(samy, -3)
    fast_pct()
    ct(exp_time_fast)
    newdataset('flat_fast')
    ftimescan(exp_time_fast, 1000)
    slow_pct()
    ct(exp_time_slow)
    newdataset('flat_slow')
    ftimescan(exp_time_slow, 1000)
    mvr(samy, 3)
    sheh3.close()
    newdataset('dark_slow')
    ftimescan(exp_time_slow, 100)
    fast_pct()
    ct(exp_time_fast)
    newdataset('dark_fast')
    ftimescan(exp_time_fast, 100)
    sheh3.open()

def collect_flat_and_dark_furnace():
    samtzpos = samtz.position
    umv(samtz, -0.5)
    sheh3.open()
    fast_pct()
    ct(exp_time_fast)
    newdataset('flat_fast')
    ftimescan(exp_time_fast, 1000)
    slow_pct()
    ct(exp_time_slow)
    newdataset('flat_slow')
    ftimescan(exp_time_slow, 1000)
    sheh3.close()
    newdataset('dark_slow')
    ftimescan(exp_time_slow, 100)
    fast_pct()
    ct(exp_time_fast)
    newdataset('dark_fast')
    ftimescan(exp_time_fast, 100)
    sheh3.open()
    umv(samtz, samtzpos)

nproj_fast = 1200
exp_time_fast = 0.01
nproj_slow = 2500
exp_time_slow = 0.035

def pct_final():
    slow_pct()
    newdataset('pct_final')
    fscan(diffrz, 0, 360.0/nproj_slow, nproj_slow, exp_time_slow)
    
def pct_initial():
    slow_pct()
    newdataset('pct_initial')
    fscan(diffrz, 0, 360.0/nproj_slow, nproj_slow, exp_time_slow)

def run_insitu_pct(nbr_slow = 15):
    slow_pct()
    newdataset('pct_initial')
    fulltomo.full_turn_scan('pct_initial')
    
    umv(diffrz,0)
    umv(furnace_z,0)
    t0 = time.time()
    offset_time = 0
    dt = 0
    
    fast_pct()
    
    nbr_fast = 15
    for i in range(nbr_fast):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'pct_fast_' + str(i).zfill(2)
        newdataset(scanname)
        # take pct
        if i%2==0:            
            fscan(diffrz, 0, 360.0/nproj_fast, nproj_fast, exp_time_fast)
        else:
            fscan(diffrz, 360, -360.0/nproj_fast, nproj_fast, exp_time_fast)
        
    slow_pct()
    
    
    for i in range(nbr_slow):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'pct_slow_' + str(i).zfill(2)
        newdataset(scanname)
        # take pct
        if (nbr_fast+i)%2==0:            
            fscan(diffrz, 0, 360.0/nproj_slow, nproj_slow, exp_time_slow)
        else:
            fscan(diffrz, 360, -360.0/nproj_slow, nproj_slow, exp_time_slow)   
    
    collect_flat_and_dark_furnace()
    umv(furnace_z,100)

    print('Waiting 5 min to solidify the sample.')
    time.sleep(300)
    
    slow_pct()
    newdataset('pct_final')
    fulltomo.full_turn_scan('pct_final')


exp_time = 0.185
start_pos = 0
step = 2
num_im = 20

def run_insitu_ff(nbr_ff=40):
    #assert( input('est-on bien avec le detecteur tomo pct sorti?(y/n)) = 'y')
    slow_pct()
    newdataset('pct_initial')
    fulltomo.full_turn_scan('pct_initial')
    
    switch_to_ff()
    ff_initial()
    
    umv(diffrz,0)
    umv(furnace_z,0)
    t0 = time.time()
    offset_time = 0
    dt = 0
    slit(0.1, 1)
    exp_time = 0.185
    start_pos = 0
    step = 2
    num_im = 20
    #nbr_ff = 200
    for i in range(nbr_ff):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
    umv(furnace_z, 100)
    
    print('Waiting 5 min to solidify the sample.')
    time.sleep(300)
    
    ff_final()
    
    switch_to_marana3_pct()
    slow_pct()
    newdataset('pct_final')
    fulltomo.full_turn_scan('pct_final')
    
    
def restart_insitu_ff(nbr_ff=100):

    t0 = time.time()
    offset_time = 0
    dt = 0
    slit(0.1, 1)
    exp_time = 0.185
    start_pos = 0
    step = 2
    num_im = 20
    #nbr_ff = 200
    for i in range(43,nbr_ff):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
    umv(furnace_z, 100)
    
    print('Waiting 5 min to solidify the sample.')
    time.sleep(300)
    
    ff_final()
    
    switch_to_marana3_pct()
    slow_pct()
    newdataset('pct_final')
    fulltomo.full_turn_scan('pct_final')



def ramp_fe():

    ff_initial()
    t0 = time.time()
    umv(diffrz,0)
    umv(furnace_z,0)
    offset_time = 0
    dt = 0
    slit(0.1, 1)
    exp_time = 0.185
    start_pos = 0
    step = 2
    num_im = 20
    for i in range(20):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_plateau_bas_initial' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
        
    nanodac3.setpoint=900
    
    for i in range(60):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_rampe_ascendante' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
        
    for i in range(12):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_plateau_haut' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
     
    nanodac3.setpoint=600
    
    for i in range(60):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_ramp_descendante' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
    
    for i in range(12):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_plateau_bas_final' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)

    umv(furnace_z, 100)
    
    
    print('Waiting 5 min to solidify the sample.')
    time.sleep(300)
    
    ff_final()
        
        
def ramp_al():

    ff_initial()
    t0 = time.time()
    umv(diffrz,0)
    umv(furnace_z,0)
    offset_time = 0
    dt = 0
    slit(0.1, 1)
    exp_time = 0.185
    start_pos = 0
    step = 2
    num_im = 20
    for i in range(20):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_plateau_bas_initial' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
        
    nanodac3.setpoint=700
    
    for i in range(60):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_rampe_ascendante' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
        
    for i in range(12):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_plateau_haut' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
     
    nanodac3.setpoint=400
    
    for i in range(60):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_ramp_descendante' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
    
    for i in range(12):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_plateau_bas_final' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)

    umv(furnace_z, 100)
    
    
    print('Waiting 5 min to solidify the sample.')
    time.sleep(300)
    
    ff_final()
    
    
def rampe_melange():

    #slow_pct()
    #newdataset('pct_initial')
    #fulltomo.full_turn_scan('pct_initial')
    switch_to_ff()
    
    ff_initial()
    t0 = time.time()
    umv(diffrz,0)
    umv(furnace_z,0)
    offset_time = 0
    dt = 0
    slit(0.1, 1)
    exp_time = 0.185
    start_pos = 0
    step = 2
    num_im = 20
    
    for i in range(20):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_plateau_bas_initial' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
        
    nanodac3.setpoint=900
    
    for i in range(100):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_rampe_ascendante' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
        

    nanodac3.setpoint=400
    
    for i in range(100):
        dt = time.time() - t0
        print('Current temperature is {} C; Annealing time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))

        dt = time.time()
        scanname = 'ff_ramp_descendante' + str(i).zfill(2)
        newdataset(scanname)
        # take ff    
        finterlaced(diffrz, 0, step, num_im, exp_time)
    

    umv(furnace_z, 100)
    
    
    print('Waiting 5 min to solidify the sample.')
    time.sleep(300)
    
    ff_final()
    
    switch_to_marana3_pct()
    slow_pct()
    newdataset('pct_final')
    fulltomo.full_turn_scan('pct_final')
    
    
def ff_final():
    newdataset('ff_final')
    finterlaced(diffrz, 0, step, num_im, exp_time)
    finterlaced(diffrz, 0, 1, 360, 0.185)

def ff_initial():
    newdataset('ff_initial')
    finterlaced(diffrz, 0, step, num_im, exp_time)
    finterlaced(diffrz, 0, 1, 360, 0.185)

"""
def run_insitu_pct_ff(offset_time = 773):
    t0 = time.time()
    dt = 0
    exit_flag = False
    i=0
    ff_start = 0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        print('Current temperature is {} C; Corrosion time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))
        
        # take pct
        switch_to_marana3_pct()
        goto_sample_bot()
        dt = time.time() - t0
        scanname = 'pct_bot_' + str(int(dt/60.0 + offset_time)) + 'min'
        take_pct(scanname=scanname)
        
        goto_sample_top()
        #t = time.time() - t0
        scanname = 'pct_top_' + str(int(dt/60.0 + offset_time)) + 'min'
        take_pct(scanname=scanname)
        
        
        # take ff
        switch_to_ff(scan_bs=False)
        if i==1:
            ff_start = dt
        goto_sample_top()
        dt = time.time() - t0
        scanname = 'ff_top_' + str(int(dt/60.0 + offset_time)) + 'min'
        take_ff_3DXRD(datasetname = scanname)            
            
        goto_sample_bot()
        dt = time.time() - t0
        scanname = 'ff_bot_' + str(int(dt/60.0 + offset_time)) + 'min'
        take_ff_3DXRD(datasetname = scanname)   
            
    print('Done successfully')


def take_dct_2regions():
    goto_sample_top()
    take_dct('top_dct')
    
    umvrct(d3x, 2)
    goto_sample_bot()
    umvrct(d3x, -2)
    take_dct('bot_dct')
    
    print('done')


def take_pct_2regions():
    tfoh1.set(0, 'Be')
    umvct(atty, -10, attrz, 0)
    slit(1.5, 1.5)

    goto_sample_top()
    fulltomo.full_turn_scan('pct_top')
    
    goto_sample_bot()
    fulltomo.full_turn_scan('pct_bot')
    
    print('done')


def take_ff_2regions():
    goto_sample_bot()
    take_ff_3DXRD('bot_ff')
    
    goto_sample_top()
    take_ff_3DXRD('top_ff')
    
    print('done')


def goto_sample_top(pos = pos_top):
    umv(samtx, pos[0])
    umv(samty, pos[1])
    umv(samtz, pos[2])
    umv(difftz, pos[3])


def goto_sample_bot(pos = pos_bot):
    umv(samtx, pos[0])
    umv(samty, pos[1])
    umv(samtz, pos[2])
    umv(difftz, pos[3])
    
def take_pct(scanname='pct'):
    tfoh1.set(0, 'Be')
    umvct(atty, -10, attrz, 0)
    slit(1.5, 1.5)
    newdataset(scanname)
    #fscan(diffrz, 0, 0.1, 3600, 0.05, scan_mode = 'CAMERA')
    #fscan(diffrz, 0, 0.1, 1900, 0.05, scan_mode = 'CAMERA')
    fscan(diffrz, 0, 0.05, 3800, 0.05, scan_mode = 'CAMERA')
    print('Done')
    
    
    
def take_dct(scanname='dct'):
    tfoh1.set(18, 'Be')
    #umvct(d3_bstz, 0)
    #umvct(d3x,6.8) # for OC4 and IN625 top
    #umvct(d3x,7.5)  # for IN625 bottom
    #umvct(d3x,8.0)  # for IN625 bottom
    umvct(d3x,7.0)  # for OC4_ESRFcor_postmortem 
    slit(0.95, 0.18) #for OC_4
    #slit(0.85, 0.08) # for IN625
    dct=DCTScan()
    dct.pars['nof_shifts'] = 6
    dct.pars['exp_time'] = 0.1
    dct.pars['ref_step'] = -2
    dct.pars['shift_step_size'] = s7vg.position - 0.03 # for OC_4
    #dct.pars['shift_step_size'] = s7vg.position - 0.01 # for IN625
    print('Updated dct parameters:')
    print(dct.pars)
    dct.run_zseries(scanname)



def take_ff_3DXRD(datasetname = 'ff'):
    slit(0.95, 0.18) #for OC_4, OC4_ESRFcor_postmortem
    #slit(0.85, 0.08) # for IN625
    ff_pars={}
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 0.25
    #ff_pars['num_proj'] = 180 / ff_pars['step_size']   # in the furnace
    ff_pars['num_proj'] = 360 / ff_pars['step_size']   # in the furnace
    ff_pars['exp_time'] = 0.185
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = samtz.position
    ff_pars['shift_step_size'] = s7vg.position - 0.03 #for OC_4
    #ff_pars['shift_step_size'] = s7vg.position - 0.01 #for IN625
    ff_pars['nof_shifts'] = 6
    
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    print('ff_zseries succeed')
    return('Succeed')

"""
