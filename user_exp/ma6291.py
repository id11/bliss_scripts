import os, sys
import numpy as np


import time
from bliss.common import cleanup

tfoh1 = config.get('tfoh1')

def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

dct_dist = 6.5
pct_dist = 50
samtz_cen = 0.7525
start_pos = -1

def define_dct_pars():
    dct_pars={}
    dct_pars['active'] = False
    dct_pars['start_pos'] = start_pos
    dct_pars['step_size'] = 0.05
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.1
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -4
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.74
    dct_pars['slit_vg'] = 0.15
    dct_pars['dist'] = 6
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = samtz_cen
    dct_pars['shift_step_size'] = 0.125
    dct_pars['nof_shifts'] = 5
    dct_pars['scan_type'] = 'fscan'
    return dct_pars

def define_pct_pars():
    pct_pars={}
    pct_pars['active'] = False
    pct_pars['start_pos'] = start_pos
    pct_pars['step_size'] = 0.5
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.02
    pct_pars['refon'] = pct_pars['num_proj']
    pct_pars['ref_int'] = 10
    pct_pars['ref_step'] = -3.5
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 41
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = samtz
    pct_pars['samtz_lo'] = 0.6
    pct_pars['samtz_hi'] = -0.6
    pct_pars['slit_hg'] = 1.7
    pct_pars['slit_vg'] = 1.7
    pct_pars['dist'] = 100
    pct_pars['scan_type'] = 'fscan'
    return pct_pars


# Al6010 pars
'''
def define_ff_pars():
    ff_pars={}
    ff_pars['active'] = True
    ff_pars['start_pos'] = start_pos
    ff_pars['step_size'] = 0.5
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.74
    ff_pars['slit_vg'] = 0.05
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = samtz_cen
    ff_pars['shift_step_size'] = 0.025
    ff_pars['nof_shifts'] = 16
    ff_pars['cpm18_detune'] = 0
    ff_pars['ceo2_height'] = -7.55
    return ff_pars
'''

#Iron pars

def define_ff_pars():
    ff_pars={}
    ff_pars['active'] = True
    ff_pars['start_pos'] = start_pos
    ff_pars['step_size'] = 0.5
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.75
    ff_pars['slit_vg'] = 0.2
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = samtz_cen
    ff_pars['shift_step_size'] = 0.1
    ff_pars['nof_shifts'] = 9
    ff_pars['cpm18_detune'] = 0
    ff_pars['ceo2_height'] = -7.55
    return ff_pars





def switch_to_dct(dct_pars):
    ACTIVE_MG.enable('marana3:*')
    ACTIVE_MG.disable('frelon3:*')
    umv(ffdtz1, 600)
    umv(attrz, 0, atty,-10)
    umv(d3_bsty, 0, d3_bstz, 0)
    tfoh1.set(12,'Be')
    centeredroi(marana3,2048,2048)
    umv(d3x, 50)
    umv(d3ty, 0)
    umv(d3x,dct_pars['dist'])
    slit(dct_pars['slit_hg'], dct_pars['slit_vg'])

def switch_to_pct(pct_pars):
    ACTIVE_MG.enable('marana3:*')
    ACTIVE_MG.disable('frelon3:*')
    umv(ffdtz1, 600)
    umv(attrz, 0, atty,-10)
    umv(d3_bstz, 1500)
    tfoh1.set(0,'Be')
    centeredroi(marana3,1024,1250)
    umv(d3x, pct_pars['dist'])
    umv(d3ty,0)
    slit(pct_pars['slit_hg'], pct_pars['slit_vg'])

def switch_to_ff(ff_pars):
    ACTIVE_MG.disable('marana3:*')
    ACTIVE_MG.enable('frelon3:*')
    umv(d3ty, 150)
    umv(attrz, 0, atty,-10)
    tfoh1.set(0,'Be')
    umv(d3x, 50)
    umv(ffdtz1, 0)
    slit(ff_pars['slit_hg'], ff_pars['slit_vg'])
    


def ff_zseries(ff_pars, datasetname = 'ff'):
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    print('ff_zseries succeed')
    return('Succeed')

def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.05, s8hg, pars['slit_hg'] + 0.05)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])
    
def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)
    
def dct_zseries(dct_pars, datasetname = 'dct'):
    imagecorr.flat_off()
    imagecorr.dark_off()
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')

filename = '/data/visitor/ma6291/id11/20241002/RAW_DATA/loadramp.txt'

def load_step_sequence(dct_pars, pct_pars, ff_pars, target):
    """Performs a loadramp to the new target value and launches PCT, DCT and a series of TT scans at this new target load
    """
    ## Change setpoint of loadrig
    #initialize_stress_regul()
    #regul_on(target)
    #while stress_adc.get_value() < target :
        #print('waiting 1 second...')
        #sleep(1)
    #regul_off(10,stress.position)
    imagecorr.flat_off()
    imagecorr.dark_off()
    ## PCT
    if pct_pars['active']:
        switch_to_pct(pct_pars)
        scan_name = 'pct_%dN_' % target
        fulltomo.full_turn_scan(scan_name, start_pos = pct_pars['start_pos'])

    ## DCT
    if dct_pars['active']:
        switch_to_dct(dct_pars)
        scan_name = 'dct_%dN_' % target
        dct_zseries(dct_pars, datasetname = scan_name)
    ## 3DXRD
    if ff_pars['active']:
        switch_to_ff(ff_pars)
        scan_name = 'ff_%dN_' % target    
        ff_zseries(ff_pars, scan_name)
    return


def load_ramp_by_target(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    write_to_file = open(to_file, 'a') if to_file is not None else sys.stdout
    # marana_large_beam(pct_pars)
    current_load = stress_adc.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return

