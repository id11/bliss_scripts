import os, sys
import numpy as np


import time
from bliss.common import cleanup

tfoh1 = config.get('tfoh1')

def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)




class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )



def half1(ymax,
          datasetname,
          ystep=2.0,
          ymin=-3000,
          rstep=0.05,
          astart = -180,
          arange = 361,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        plotselect("eiger:roi_counters:roi1_max")
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)

def switch_to_dct():
    print('Dont forget to put the beamstop')
    fsh.session = 'TDXRD'
    umv(attrz, 0, atty,-10)
    tfoh1.set(20,'Be')
    centeredroi(marana3,2048,2048)
    umv(d3x,3.2)
    slit(0.35, 0.2)

def switch_to_pct():
    print('Dont forget to remove the beamstop')
    umv(attrz, 0, atty,-10)
    fsh.session='TDXRD'
    tfoh1.set(0,'Be')
    centeredroi(marana3,1024,1024)
    umv(d3x, 50)
    umv(d3ty,0)
    slit(1.7, 1.7)

def switch_to_eiger2():
    #umv(d3ty, 50)
    mv(s9vg, 0.05, s9hg, 0.05)
    fsh.session='NSCOPE'
    moveto_eiger()
    umv(hlz,-0.313, vly,-0.007)
    if piny.position > 1.0:
        switch_pinhole()
    tfoh1.set(0, 'Be')
    umv(atty,0,attrz,-40)
    newdataset('align')
    


def switch_to_basler2():
    #umv(d3ty, 50)
    moveto_basler()
    fsh.session='NSCOPE'
    umv(hlz,-0.313+1, vly,-0.007-1)
    if piny.position < 1.0:
        switch_pinhole()
    sheh3.open()
    umv(atty,-10,attrz,0)
    tfoh1.set(0, 'Be')
    mv(s9vg,1.5, s9hg, 1.5)




def scanning_1st():
    z0 = 55.881
    nz = 8
    pixel_size = 0.4
    pzs = np.arange(z0-nz*0.5*pixel_size, z0+nz*0.5*pixel_size-pixel_size/2,pixel_size)
    print(pzs)
    for i, pos in enumerate(pzs):
        umv(pz, pos)
        half1(ymax=132, datasetname=f'_first_z{i}', ystep=0.4, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    sheh3.close()    
    print('Done')    



def scanning_grain12_1st():
    z0 = 55.881
    nz = 15
    zpixel_size = 1.0
    pzs = np.arange(z0-nz*0.5*zpixel_size, z0+nz*0.5*zpixel_size-zpixel_size/2,zpixel_size)
    print(pzs)
    for i, pos in enumerate(pzs):
        umv(pz, pos)
        half1(ymax=30, datasetname=f'_grain12_z{i}', ystep=0.2, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    sheh3.close()
    print('Done') 

def after15min_triple_roi():
    z0 = 62.381
    umv(pz, z0)
    half1(ymax=30, datasetname=f'_grain12', ystep=0.2, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    sheh3.close()
    print('Done')
    
    
# grain12 changed to grain14 in the new DCT reconstruction from AlCu_s2_init
# change from triple_roi (the first grain 12 wrong position) to grain14 centered position
# do: umvr(ntx, -0.0137, nty, -0.0286)
def scanning_grain14_after15min():
    z0 = 62.381
    nz = 6
    zpixel_size = 1.0
    pzs = np.arange(z0-nz*0.5*zpixel_size, z0+nz*0.5*zpixel_size-zpixel_size/2,zpixel_size)
    print(pzs)
    for i, pos in enumerate(pzs):
        umv(pz, pos)
        half1(ymax=30, datasetname=f'_grain14_z{i}', ystep=0.2, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    sheh3.close()
    print('Done') 
    
        
def scanning_grain14_after15min_bigger():
    z0 = 62.381
    nz = 6
    zpixel_size = 1.0
    pzs = np.arange(z0-nz*0.5*zpixel_size, z0+nz*0.5*zpixel_size-zpixel_size/2,zpixel_size)
    print(pzs)
    for i, pos in enumerate(pzs):
        if i > 1:
            umv(pz, pos)
            half1(ymax=32, datasetname=f'_grain14_bigger_z{i}', ystep=0.2, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    umvct(pz, z0)
    sheh3.close()
    print('Done')
    

# after annealing 30 min (including the previous 15 min)
def after30min_triple_roi():
    z0 = 62.381
    umv(pz, z0)
    half1(ymax=30, datasetname=f'_grain12', ystep=0.2, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    print('Done')

    
def scanning_grain14_after30min_bigger():
    z0 = 62.381
    nz = 8
    zpixel_size = 1.0
    pzs = np.arange(z0-nz*0.5*zpixel_size, z0+nz*0.5*zpixel_size-zpixel_size/2,zpixel_size)
    print(pzs)
    for i, pos in enumerate(pzs):
        umv(pz, pos)
        half1(ymax=32, datasetname=f'_grain14_bigger_z{i}', ystep=0.2, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    umvct(pz, z0)
    sheh3.close()
    print('Done') 



def scanning_FeAu():
    z0 = 62.381
    nz = 8
    zpixel_size = 1.0
    pzs = np.arange(z0-nz*0.5*zpixel_size, z0+nz*0.5*zpixel_size-zpixel_size/2,zpixel_size)
    print(pzs)
    for i, pos in enumerate(pzs):
        umv(pz, pos)
        half1(ymax=32, datasetname=f'_grain14_bigger_z{i}', ystep=0.2, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    umvct(pz, z0)
    sheh3.close()
    print('Done') 







