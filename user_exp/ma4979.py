import numpy as np
import time


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    #pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']
       

def tomoin():
    ACTIVE_MG.enable("*marana:image") 
    ACTIVE_MG.disable("frelon3*")
    sheh3.close()
    umv(ffdtx1, 500)
    umv(ffdtz1, 100, d3ty, 0.22)
    sheh3.open()
    print("ready to collect tomo data") 

def frelonin():
    ACTIVE_MG.disable("*marana*") 
    ACTIVE_MG.enable("frelon3:image") 
    ACTIVE_MG.enable("frelon3:roi*")
    sheh3.close()
    umv(d3ty,180, ffdtz1, 1)
    umv(ffdtx1, 240)
    sheh3.open()
    print("ready to collect diffraction data") 

def crlin():
    bigy=config.get('bigy')
    tfoh1 = config.get("tfoh1")
    umv(bigy, 0.108)
    tfoh1.set(18,"Be") 
    
def crlout():
    bigy=config.get('bigy')
    tfoh1 = config.get("tfoh1")
    umv(bigy, 22)
    tfoh1.set(0,"Be")
    print("done")


def powder():
    frelonin()
    crlout()
    ascan(diffrz, 0, 180, 90, 0.1)
       
    
def difftyfrelon():
    # newproposal("ma4498")
    # fpico4
    #diffty_airpad.on()
    pico4.auto_range = False
    pico4.range=2.1e-5
    
    ACTIVE_MG.disable_all()
    ACTIVE_MG.enable("frelon3:roi*")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("*fpico4")
    
    y0 = 14.117
    width = 0.36
    step  = 0.005
    #2h

    for my_y  in np.arange( y0-width, y0+width+step/10., step):
        umv(diffty, my_y)
        pause_for_refill(60)
        finterlaced(diffrz,   0,  1, 180, 0.08, mode="ZIGZAG") 
        #diffty.reset_closed_loop()
    umv(diffty, y0)
    
    #diffty_airpad.off()


def hydrascan():
    for hs,my_s in enumerate(np.arange(0,30,1)):
        newdataset("cerbonio_hydroscan_night_%03d"%(hs))
        frelonin()       
        crlin()
        difftyfrelon()
        crlout()
        powder()
        tomoin()
        umv(diffty, 14.665)
        tomo.half_turn_scan() 

def startscan():
    diffty.reset_closed_loop()
    newdataset("Bassanite2_start")
    frelonin()
    crlout()
    powder()
    crlin()
    difftyfrelon()
    crlout()
    tomoin()
    umv(diffty, 14.70)
    tomo.half_turn_scan() 


 
def hydratomo():
    for hs,my_s in enumerate(np.arange(1,300,1)):
        print("this is scan_%03d"%(hs)) 
        newdataset("hydrotomo_shake_%03d"%(hs))
        tomo.half_turn_scan() 
        print("now sleeping zzzZzzzzzz....")  
        sleep(300)
    
 

def diffraction():
    frelonin()
    crlin()


def tomography():
    tomoin()
    crlout()
    
    
def capillary ():
    diffty_airpad.on()
    umv(diffrz, 0)
    dscan(diffty, -0.5, 0.5, 50, 0.08)
    umv(diffrz, 90)
    dscan(diffty, -0.5, 0.5, 50, 0.08)
    umv(diffrz, 180)
    dscan(diffty, -0.5, 0.5, 50, 0.08)
    umv(diffrz, 270)
    dscan(diffty, -0.5, 0.5, 50, 0.08)
    diffty_airpad.off()
    
    
def hydranight():
    tomography()
    for hs,my_s in enumerate(np.arange(1,43,1)):
        print("this is scan_%03d"%(hs)) 
        newdataset("hydrotomo_%03d"%(hs))
        tomo.half_turn_scan() 
    diffraction()
    newdataset('tdxrd_hydrotomo_halfnight')
    difftyfrelon()
    umv(diffty, 14.117)
    tomography()
    for hs,my_s in enumerate(np.arange(1,43,1)):
        print("this is scan_%03d"%(hs)) 
        newdataset("hydrotomo_round2_%03d"%(hs))
        tomo.half_turn_scan() 
    diffraction()
    newdataset('tdxrd_hydrotomo_morning')
    difftyfrelon() 
    umv(diffty, 14.117)
    tomography()
    for hs,my_s in enumerate(np.arange(1,43,1)):
        print("this is scan_%03d"%(hs)) 
        newdataset("hydrotomo_round3_%03d"%(hs))
        tomo.half_turn_scan() 
      
     
     
     
def full():
    newdataset('tomo_volume')
    tomography()
    tomo.half_turn_scan() 
    diffraction()
    newdataset('tdxrd')
    difftyfrelon()
    print("tomo and diffraction done!") 
    tomography()
    
    
def single_hydranight():
    #tomography()
    for hs,my_s in enumerate(np.arange(1,750,1)):
        print("this is scan_%03d"%(hs)) 
        newdataset("hydrotomo_single_%03d"%(hs))
        tomo.half_turn_scan() 
    
        
    


    
    
  
