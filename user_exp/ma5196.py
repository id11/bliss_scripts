import time

def ramptotemp(temp):
    print('Ramping to',temp)
    nanodacpool.setpoint = temp
    while nanodacpool.is_ramping:
        print('Temperature is',nanodacpool_axis.position)
        sleep(10)

        

def tempcalib():
    sample_hz = 13
    ceria_hz = 14
    temps = [140,150,160,170,180,400,450,460,470,480,490]
    for t in temps:
        umv(nanodacpool_axis, t)
        sleep(30)
        umv(hz,sample_hz)
        loopscan(20,0.4)
        umv(hz,ceria_hz)
        loopscan(20,0.4)


def temp_rampto490():
    sheh1.open()
    temp = 505
    print('Ramping to',temp)
    nanodacpool.setpoint = temp
    sleep(1)
    while abs(nanodacpool_axis.position - temp) > 1:
        print('Temperature is',nanodacpool_axis.position)
        newdataset('AN490x1_t%03d'%nanodacpool_axis.position)
        loopscan(20,1)
        sleep(1)

    start_time = time.time()
    i = 0
    while (time.time() - start_time) < (60*60*1):
        i = i + 1
        newdataset('t%03d_%03d'%(temp,i))
        print('Time since reaching 505C is %04d mins'%((time.time()-start_time)/60))
        loopscan(40,.5)
        sleep(1)
    nanodacpool.setpoint = 25

def temp_rampto525():
    sheh1.open()
    temp = 540
    print('Ramping to',temp)
    nanodacpool.setpoint = temp
    sleep(1)
    while abs(nanodacpool_axis.position - temp) > 1:
        print('Temperature is',nanodacpool_axis.position)
        newdataset('AB525x1_t%03d'%nanodacpool_axis.position)
        loopscan(20,1)
        sleep(1)

    start_time = time.time()
    i = 0
    while (time.time() - start_time) < (60*60*1):
        i = i + 1
        newdataset('AB525x1_t%03d_%03d'%(temp,i))
        print('Time since reaching 525C is %04d mins'%((time.time()-start_time)/60))
        loopscan(40,.5)
        sleep(1)
    nanodacpool.setpoint = 25

def temp_rampto525_cont():
    sheh1.open()
    temp = 540
    #print('Ramping to',temp)
    #nanodacpool.setpoint = temp
    #sleep(1)
    #while abs(nanodacpool_axis.position - temp) > 1:
    #    print('Temperature is',nanodacpool_axis.position)
    #    newdataset('AB525x1_t%03d'%nanodacpool_axis.position)
    #    loopscan(20,1)
    #    sleep(1)

    start_time = time.time()
    i = 0
    while (time.time() - start_time) < (60*60*1):
        i = i + 1
        newdataset('AB525x1_t%03d_%03d'%(temp,i))
        print('Time since reaching 525C is %04d mins'%((time.time()-start_time)/60))
        loopscan(40,.5)
        sleep(1)
    nanodacpool.setpoint = 25
    
    
def temp_rampto180():
    sheh1.open()
    temp = 198
    print('Ramping to',temp)
    nanodacpool.setpoint = temp
    sleep(1)
    while abs(nanodacpool_axis.position - temp) > 1:
        print('Temperature is',nanodacpool_axis.position)
        newdataset('AN180x5_b_t%03d'%nanodacpool_axis.position)
        loopscan(40,1)
        sleep(1)

    start_time = time.time()
    i = 0
    while (time.time() - start_time) < (60*60*5):
        i = i + 1
        newdataset('AN180x5_t%03d_%03d'%(temp,i))
        print('Time since reaching 180C is %04d mins'%((time.time()-start_time)/60))
        loopscan(60,.5)
        sleep(1)
    
    nanodacpool.setpoint = 25

def temp_rampto160():
    sheh1.open()
    temp = 178
    print('Ramping to',temp)
    nanodacpool.setpoint = temp
    sleep(1)
    while abs(nanodacpool_axis.position - temp) > 1:
        print('Temperature is',nanodacpool_axis.position)
        newdataset('AB160x4h30_t%03d'%nanodacpool_axis.position)
        loopscan(20,1)
        sleep(1)

    start_time = time.time()
    i = 0
    while (time.time() - start_time) < (60*60*4.5):
        i = i + 1
        newdataset('AB160x4h30_t%03d_%03d'%(temp,i))
        print('Time since reaching 160C is %04d mins'%((time.time()-start_time)/60))
        loopscan(40,.5)
        sleep(1)
nan    
    nanodacpool.setpoint = 25  

def temp_rampto490_cont():
    sheh1.open()
    temp = 505

    start_time = time.time()
    i = 36
    while (time.time() - start_time) < (60*60*3):
        i = i + 1
        newdataset('t%03d_%03d'%(temp,i))
        print('Time since reaching 505C is %04d mins'%((time.time()-start_time)/60))
        loopscan(60,0.5)
        sleep(1)
        

        
      
def temp_rampto160_cont():
    sheh1.open()
    temp = 178
    print('Ramping to',temp)
    nanodacpool.setpoint = temp
    sleep(1)
    while abs(nanodacpool_axis.position - temp) > 1:
        print('Temperature is',nanodacpool_axis.position)
        newdataset('AN160x3_t%03d'%nanodacpool_axis.position)
        loopscan(20,1)
        sleep(1)

    start_time = time.time()
    i = 156
    while (time.time() - start_time) < (60*85):
        i = i + 1
        newdataset('AN160x3_t%03d_%03d'%(temp,i))
        print('Time since reaching 160C is %04d mins'%((time.time()-start_time)/60))
        loopscan(40,.5)
        sleep(1)
    
    nanodacpool.setpoint = 25  
