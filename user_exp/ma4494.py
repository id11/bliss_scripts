
import numpy as np
import time
user_script_load("align_cor.py") 


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan(*a,**k):
    print(a,k)
    try:
        fscan(*a,**k)
    except: # pretty hopeless but why not ? 
        fscan(*a,**k)

def myumv(*a,**k):
    print(a,k)
    umv(*a,**k)
        
def testscan1():
    y0 = 0.0  # centre of rotation
    width = 17
    step  = 0.4
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.1
    anrange = 180.
    speed = 25.
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
#    print(timest)
    eiger.camera.auto_summation="OFF"
    eiger.camera.photon_energy=43468.9
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot,-180, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot,   0, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"


def align_s1():
    # assumes we are about 10 um above the Pt marker
    # put sample on center of rotation using Ni fiuo
    ACTIVE_MG.enable("mca")
    user.align_cor_dty( 30 , 60 , 0.2, "Ni_det0")
    # Here dty will be in the wrong position due to asymmetry
    umv( dty, 0 )
    umv( rot , 90 )
    plotselect( "mca:Pt_det0" )
    # check height
    dscan( pz, -15, -5, 50, .1 )
    plotselect( "mca:Pt_det0" )
    print(cen())
    
# To check after deforming : tfoh1 = 16 for more flux
#     ... and reset pico6 range 


        
def layer_s1():
    y0 = 0.0  # centre of rotation
    width = 12.
    step  = 0.6
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.1
    anrange = 180.
    speed = 25.
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    #    print(timest)
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    eiger.camera.photon_energy=43468.9
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot,-180, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        # edited here Fri 8h for 
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot,   0, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"



def volscan_s1():
    for iz, my_z in enumerate( np.arange( 55, 70, 0.3 )) :
        newdataset( "z%03d"%(iz))
        umv( pz, my_z )
        layer_s1()

def volscan_400MPa():
    for iz, my_z in enumerate( np.arange( 28, 43, 0.3 )) :
        newdataset( "z%03d"%(iz))
        umv( pz, my_z )
        layer_s1()
        
def volscan_endofplateau():
    for iz, my_z in enumerate( np.arange( 36, 51, 0.3 )) :
        newdataset( "z%03d"%(iz))
        umv( pz, my_z )
        layer_s1()
        
def volscan_11pc():
    for iz, my_z in enumerate( np.arange( 23.5, 38.5, 0.3 )) :
        newdataset( "z%03d"%(iz))
        umv( pz, my_z )
        layer_s1()
        
def volscan_15pc():
    refz = 49.4
    for iz, my_z in enumerate( np.arange( refz+5, refz+15, 0.3 )) :
        newdataset( "z%03d"%(iz))
        umv( pz, my_z )
        layer_s1()
        
def volscan_15pc_cont():
    refz = 49.4
    for iz, my_z in enumerate( np.arange( refz+15, refz+20, 0.3 )) :
        newdataset( "z%03d"%(iz))
        umv( pz, my_z )
        layer_s1()
        
def volscan_unloaded():
    for iz, my_z in enumerate( np.arange( 21.4, 36.4, 0.3 )) :
        newdataset( "z%03d"%(iz))
        umv( pz, my_z )
        layer_s1()

        
def volscan_heated():
    # 22.6
    for iz, my_z in enumerate( np.arange( 22.6+5, 22.6+20, 0.3 )) :
        newdataset( "z%03d"%(iz))
        umv( pz, my_z )
        layer_s1()
