
import time

def Ni_calib_scan(count_time=0.05, images_per_scan=12, scans=3000): 
    for k in range(int(scans/100)):
        newdataset('%05d'%k)
        for j in range(100):
            loopscan(images_per_scan, count_time)
    print('done...')

def Ni_calib_scan_test(count_time=0.05, images_per_scan=12, scans=3000): 
    print('predicted time in %f'%(scans*images_per_scan*count_time))
    start_time = time.time()   
    loopscan(images_per_scan, count_time)
    end_time = time.time()
    print('Scan took %f'%(end_time-start_time))

    print('Predicted total scan time is:' )
    print('%f seconds'%(scans*(end_time-start_time)))
    print('%f mins'%(scans*(end_time-start_time)/60))
    print('%f hours'%(scans*(end_time-start_time)/3600))

def D19_scan(count_time=0.05, images_per_scan=12, scans=1500): 
    for k in range(int(scans/100)):
        newdataset('%05d'%k)
        for j in range(100):
            loopscan(images_per_scan, count_time)
    print('done...')


def D19_scan_test(count_time=0.1, images_per_scan=10, scans=1500): 
    print('predicted time in %f'%(scans*images_per_scan*count_time))
    start_time = time.time()   
    loopscan(images_per_scan, count_time)
    end_time = time.time()
    print('Scan took %f'%(end_time-start_time))

    print('Predicted total scan time is:' )
    print('%f seconds'%(scans*(end_time-start_time)))
    print('%f mins'%(scans*(end_time-start_time)/60))
    print('%f hours'%(scans*(end_time-start_time)/3600))


def D19_scan_low_flux(count_time=0.1, images_per_scan=10, scans=1500): 
    for k in range(int(scans/100)):
        newdataset('%05d'%k)
        for j in range(100):
            loopscan(images_per_scan, count_time)
    print('done...')

def Ni_calib_scan_short(count_time=0.1, images_per_scan=10, scans=900): 
    for k in range(int(scans/100)):
        newdataset('%05d'%k)
        for j in range(100):
            loopscan(images_per_scan, count_time)
    print('done...')

def Ni_calib_scan_short_test(count_time=0.1, images_per_scan=10, scans=900): 
    print('predicted time in %f'%(scans*images_per_scan*count_time))
    start_time = time.time()   
    loopscan(images_per_scan, count_time)
    end_time = time.time()
    print('Scan took %f'%(end_time-start_time))

    print('Predicted total scan time is:' )
    print('%f seconds'%(scans*(end_time-start_time)))
    print('%f mins'%(scans*(end_time-start_time)/60))
    print('%f hours'%(scans*(end_time-start_time)/3600))

def DSS_scan_low_flux(count_time=0.1, images_per_scan=10, scans=1500): 
    for k in range(int(scans/100)):
        newdataset('%05d'%k)
        for j in range(100):
            loopscan(images_per_scan, count_time)
    print('done...')

def DSS_scan_low_flux_test(count_time=0.1, images_per_scan=10, scans=1500): 
    print('predicted time in %f'%(scans*images_per_scan*count_time))
    start_time = time.time()   
    loopscan(images_per_scan, count_time)
    end_time = time.time()
    print('Scan took %f'%(end_time-start_time))

    print('Predicted total scan time is:' )
    print('%f seconds'%(scans*(end_time-start_time)))
    print('%f mins'%(scans*(end_time-start_time)/60))
    print('%f hours'%(scans*(end_time-start_time)/3600))

def DSS_scan_low_flux_low_count(count_time=0.05, images_per_scan=12, scans=1750): 
    for k in range(int(scans/100)):
        newdataset('%05d'%k)
        for j in range(100):
            loopscan(images_per_scan, count_time)
    print('done...')

def DSS_scan_low_flux_low_count_test(count_time=0.05, images_per_scan=12, scans=1750): 
    print('predicted time in %f'%(scans*images_per_scan*count_time))
    start_time = time.time()   
    loopscan(images_per_scan, count_time)
    end_time = time.time()
    print('Scan took %f'%(end_time-start_time))

    print('Predicted total scan time is:' )
    print('%f seconds'%(scans*(end_time-start_time)))
    print('%f mins'%(scans*(end_time-start_time)/60))
    print('%f hours'%(scans*(end_time-start_time)/3600))

def DSS_scan_short (count_time=0.05, images_per_scan=12, scans=700): 
    for k in range(int(scans/100)):
        newdataset('%05d'%k)
        for j in range(100):
            loopscan(images_per_scan, count_time)
    print('done...')

def DSS_scan_short_test(count_time=0.05, images_per_scan=12, scans=700): 
    print('predicted time in %f'%(scans*images_per_scan*count_time))
    start_time = time.time()   
    loopscan(images_per_scan, count_time)
    end_time = time.time()
    print('Scan took %f'%(end_time-start_time))

    print('Predicted total scan time is:' )
    print('%f seconds'%(scans*(end_time-start_time)))
    print('%f mins'%(scans*(end_time-start_time)/60))
    print('%f hours'%(scans*(end_time-start_time)/3600))

