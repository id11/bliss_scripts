import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime
from bliss.common import cleanup
   
#data_dir = '/data/visitor/ma6048/id11/20240611/PROCESSED_DATA/NiTi_s1_in_nanox2/NiTi_s1_in_nanox2_dct2'

data_dir = '/data/visitor/ma6048/id11/20240611/PROCESSED_DATA/NiTi_s1_in_nanox2/NiTi_s1_in_nanox2_dct_120N_1'



def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def read_tt_infos(gid):
    data_path = os.path.join(data_dir, 'grain_%04d.mat' % gid)
    data = loadmat(data_path)
    t = data['out'][0][0][0][0]
    values = t[0][0][0][0][0]
    d = {}
    d['gr_id'] = int(values[0][0][0])
    # double check that the grain id is right
    if d['gr_id'] != gid:
        raise(ValueError('grain id mismatch, check your matlab output in file %s' % data_path))
    d['nfdtx'] = float(values[1][0][0])
    d['d3tz'] = float(values[2][0][0])
    d['diffry'] = float(values[3][0][0])
    d['samrx'] = float(values[4][0][0])
    d['samry'] = float(values[5][0][0])
    d['samtx'] = float(values[6][0][0])
    d['samty'] = float(values[7][0][0])
    d['samtz'] = float(values[8][0][0])
    d['samrx_offset'] = float(values[9][0][0])
    d['samry_offset'] = float(values[10][0][0])
    d['samtx_offset'] = float(values[11][0][0])
    d['samty_offset'] = float(values[12][0][0])
    d['samtz_offset'] = float(values[13][0][0])
    d['diffrz_offset'] = float(values[14][0][0])
    d['int_factor'] = float(values[15][0][0])
    print(d)
    return d
    
atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
bigy = config.get('bigy')
u22 = config.get('u22')

dct_dist = 11
pct_dist = 50
tt_dist = 0
ff_dist = 150

samtx_offset = 0
samty_offset = 0
samtz_offset = 0
samrx_offset = 0
samry_offset = 0
diffty_offset = 13.9565

ff_tz_out = 600
ff_tx_out = 600

d1_out = -205
d2_out = -388
d3_out = 180

bigy_in = -21.7038
bigy_out = 0


def sam_dct_pos():
    umv(samrx, samrx_offset, samry, samry_offset, diffry, 0)
    umv(samtx, samtx_offset, samty, samty_offset, samtz, samtz_offset)

def attin():
    umv(atty, 0)
    umv(attrz, -15) #was -10

def attin_sff():
    # umv(atty, 0)
    # umv(attrz, -20) # was -15
    attout()

def attout():
    umv(atty, -10)
    umv(attrz, 0)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    # pause_for_flux(t)

"""
def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']
"""
    
def ffin(ff_pars):
    sheh3.close()
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    bigy_goto(bigy_out)
    sam_dct_pos()
    attin()
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon16:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out, d2ty, d2_out)
    umv(ffdtx1, ff_dist, nfdtx, dct_dist, ffdtz1, 0)
    umv(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'], s8vg, ff_pars['slit_vg'] + 0.05, s8hg, ff_pars['slit_hg'] + 0.05)
    print("ready to collect far-field data")
    sheh3.open()

def scanning_ff(sff_pars):
    sheh3.close()
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    bigy_goto(bigy_in)
    sam_dct_pos()
    attin_sff()
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon16:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out, d2ty, d2_out)
    umv(ffdtx1, ff_dist, nfdtx, dct_dist, ffdtz1, 0)
    umv(s7vg, sff_pars['slit_vg'], s7hg, sff_pars['slit_hg'], s8vg, sff_pars['slit_vg'] + 0.025, s8hg, sff_pars['slit_hg'] + 0.025)

    print("ready to collect scanning far-field data")
    sheh3.open()

def marana_pct(pct_pars):
    tfoh1.set(96,'Al')
    tfoh1.set(0,'Be')
    marana3.image.roi=[0,0,2048,2048]
    # sct(pct_pars['exp_time'])
    #umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'])
    s(pct_pars['slit_hg'],pct_pars['slit_vg'])
    maranain(pct_pars['dist'])
   
def s(hor, ver, delta=0.1):
    umv(s7hg, hor, s8hg, hor+delta, s7vg, ver, s8vg, ver+delta)
 

def maranain(dist):
    sheh3.close()
    attout()
    bigy_goto(bigy_out)
    ACTIVE_MG.enable("marana3:image")
    ACTIVE_MG.disable("frelon16*") 
    ACTIVE_MG.disable("frelon3*")
    sam_dct_pos()
    umv(ffdtx1, ff_tx_out, nfdtx, dist, ffdtz1, ff_tz_out)
    umv(d2ty, d2_out, d3ty, 0, d3tz, 0)
    print("ready to collect marana data") 
    sheh3.open()
    ct(0.02)

"""
def marana_large_beam(pct_pars):
    tfoh1.set(64,'Al')
    tfoh1.set(0,'Be')
    #for i in range (3):
    #    umv(tfz, -0.46, tfy, 14.488)
    marana3.image.roi=[512,0,1024,2048]
    sct(0.05)
    umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'], nfdtx, pct_pars['dist'], ffdtx1, ff_tx_out)
    umvct(s8vg, pct_pars['slit_vg'] + 0.1, s8hg, pct_pars['slit_hg'] + 0.1)
    umvct(samtz, samtz_offset)
    ct(pct_pars['exp_time'])
"""

def marana_dct(dct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(12,'Be')
    #for i in range (3):
    #    umv(tfz, -0.32, tfy, 14.445)
    marana3.image.roi=[0,0,2048,2048]
    # sct(dct_pars['exp_time'])
    umv(s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umv(s8vg, dct_pars['slit_vg'] + 0.05, s8hg, dct_pars['slit_hg'] + 0.05)
    maranain(dct_pars['dist'])


def frelon16in(theta, tt_pars):
    # tfoh1.set(0,'Al')
    # tfoh1.set(16,'Be')
    #for i in range (3):
    #    umv(tfz, -0.39, tfy, 14.51)
    attout()
    ACTIVE_MG.disable("marana3*")
    ACTIVE_MG.disable("frelon3*") 
    ACTIVE_MG.enable("frelon16*")
    frelon16.image.flip=[False, True]
    d2tzpos = np.tan(2*np.deg2rad(theta))*tt_dist
    print("moving nfdtx, ffdtx1 and ffdtz1 to save positions")
    umvct(nfdtx, tt_pars['dist'], ffdtx1, ff_tx_out, ffdtz1, ff_tz_out)
    print("moving d2tz to target position %g"%d2tzpos)
    umvct(d2tz, d2tzpos)
    umv(d2ty, 0, d3ty, d3_out)
    # ct(0.1)
    print("ready to collect topotomo data")


def read_positions():
    g = {}   
    g['samrx']=samrx.position
    g['samry']=samry.position
    g['samtx']=samtx.position
    g['samty']=samty.position
    g['samtz']=samtz.position
    g['d2tz']=d2tz.position
    return g

def topotomo_tilt_grain(gr_id):
    """drives diffractometer to grain alignment position for
    """
    topotomo_params = read_tt_infos(gr_id)
    print(topotomo_params)
    if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
        return('Error')
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d3tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')

def topotomo_tilt_grain_json(gr_id):
    """drives diffractometer to grain alignment position for
    """
    topotomo_params = get_json_grain_info(gr_id)
    print(topotomo_params)
    if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
        return('Error')
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d2tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')
    
def topotomo_tilt_grain_dict(topotomo_params):
    """drives diffractometer to current grain alignment position
    """
    print(topotomo_params)
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d2tz, topotomo_params['d3tz'])
    umv(diffrz, 90) #save position to change tilts for the diffractometer with Nanox during ma5178...
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')

# -*- coding: utf-8 -*-
import os
import numpy as np



def remove_grains_from_list(grain_list_in, grain_ids_to_remove):
    # remove a list of grain IDs from the grain list
    # e.g if you want to remove grain 2 and 3:
    # new_grain_list = user.remove_grains_from_list(grain_list, [2, 3])
    new_grain_list = [g for g in grain_list_in if g['gr_id'] not in grain_ids_to_remove]
    return new_grain_list


def define_tt_pars():
    tt_pars={}
    tt_pars['diffrz_step'] = 5
    tt_pars['num_proj'] = 360 / tt_pars['diffrz_step']
    tt_pars['diffry_step'] = 0.025
    tt_pars['exp_time'] = 0.05
    tt_pars['search_range'] = 0.4
    tt_pars['scan_mode'] = 'TIME'
    tt_pars['image_roi'] = [0, 0, 1920, 1920] #[480, 480, 960, 960]
    tt_pars['counter_roi'] = [760, 760, 400, 400] #[812, 1531, 466, 500]
    tt_pars['slit_hg'] = 0.4
    tt_pars['slit_vg'] = 0.4
    tt_pars['dist'] = 11
    tt_pars['active'] = 0
    return tt_pars
    
def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.05
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -4
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.8
    dct_pars['slit_vg'] = 0.36
    dct_pars['dist'] = 8
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 0
    dct_pars['shift_step_size'] = 0.35
    dct_pars['nof_shifts'] = 3
    dct_pars['active'] = 1
    return dct_pars

def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = 0
    pct_pars['step_size'] = 0.4
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.04
    pct_pars['refon'] = pct_pars['num_proj']
    pct_pars['ref_step'] = -4
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 41
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = samtz
    pct_pars['slit_hg'] = 1.7
    pct_pars['slit_vg'] = 3.5
    pct_pars['dist'] = 50
    pct_pars['active'] = 1
    return pct_pars
    
def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 0.25
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.8
    ff_pars['slit_vg'] = 0.05
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = 0
    ff_pars['shift_step_size'] = 0
    ff_pars['nof_shifts'] = 1
    ff_pars['active'] = 1
    return ff_pars
    
def define_sff_pars():
    sff_pars={}
    sff_pars['start_pos'] = 0
    sff_pars['step_size'] = 0.8
    sff_pars['num_proj'] = 180 / sff_pars['step_size']
    sff_pars['exp_time'] = 0.08
    sff_pars['slit_hg'] = 0.05
    sff_pars['slit_vg'] = 0.05
    sff_pars['ystep_wholesample'] = 0.002
    sff_pars['ystep_onegrain'] = 0.002
    sff_pars['ymax'] = 0.4
    sff_pars['mode'] = 'ZIGZAG'
    sff_pars['active'] = 1
    return sff_pars

def load_grains_from_matlab(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = read_tt_infos(gr_id)
        grain_list.append(grain)
    return grain_list
    
def load_grains_from_json(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = get_json_grain_info(gr_id)
        grain_list.append(grain)
    return grain_list

def load_ramp_by_ascan(start, stop, npoints, exp_time, loadstep, pct_pars):
    # marana_large_beam(pct_pars)
    newdataset('loadramp_%d' %  loadstep)
    ascan(stress, start, stop, npoints, exp_time, stress_adc, marana3)  #, frelon16)
    return


def safe_move_diffty(pos):
    for attempt in range(5):
        try:
            umv(diffty, pos)
            break
        except KeyboardInterrupt:
            raise
        except Exception as e:
            print(e)
            # elog_add()  # stupid stupid idea!
            
            
        diffty_airpad.on(60*24*5)
        time.sleep(1)
        diffty.reset_closed_loop()


# copied from ~/bliss_scripts/difftomomacro.py
def tdxrd_forward180(
    ymax, datasetname, ystep=0.01, ycen=14.501, ymin=0., rstep=0.8, expotime=0.08
):

    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ycen = central position 
    ymin = low y values to skip, for restarting from a crash
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.tdxrd_forward(0.5, 'toto', ystep=0.1, ycen=14.65)

    """

    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 11:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is", angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int(ymax // ystep)
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace(-ylim, ylim, ny * 2 + 1) + ycen
    print('ypos:', ypos)
    i = -1
    #
    num_proj = 180 / rstep
    diffty_airpad.on(60 * 24 * 5)
    with cleanup.cleanup(diffrz, restore_list=(cleanup.axis.POS,)):
        while i < len(ypos) - 1:
            i += 1
            if ypos[i] < ymin:  # skip positions already done
                continue
            pause_for_refill(25)  # time for one scan
            safe_move_diffty(ypos[i])
            finterlaced(diffrz, 0, rstep, num_proj, expotime, mode="ZIGZAG")

    safe_move_diffty(ycen)
    diffty_airpad.off()



def sff_scan(scanname, sff_pars):
    "Call tdxrd_forward180"
    
    tdxrd_forward180(ymax=sff_pars['ymax'],
                     datasetname=scanname,
                     ystep=sff_pars['ystep_wholesample'],
                     ycen=diffty_offset,
                     rstep=sff_pars['step_size'],
                     expotime=sff_pars['exp_time'])
                     
    

    umv(diffrz,0)      
    print("All done!")


"""
def sff_one_grain(scanname, sff_pars, grain):
    # TODO: update
    # better macros
    lyin()
    umv(s7vg, 0.5, s7hg, 0.5, s8vg, 0.05, s8hg, 0.05) 
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, bigy_in)
    umv(samtx, grain['samtx'], samty, grain['samty'], samtz, grain['samtz'])
    stage_diffty_zero = diffty_offset

    scanstarty= -0.1  # in mm
    scanendy=0.101
    scanstepy=0.01
    
    scanstartz= -0.15  # in mm
    scanendz=0.1501 
    scanstepz=0.01    

    umvr(samtz, scanstartz)
    z_scans_number = (scanendz - scanstartz) / scanstepz
    newdataset(scanname)
    for i in range(int(z_scans_number)):
        umvr(samtz, scanstepz)
        for ty in np.arange(scanstarty,scanendy,scanstepy):
            umv(diffty, stage_diffty_zero+ty)
            print(f"Scan pos: y: {ty} couche {i}")
            finterlaced( diffrz, sff_pars['start_pos'], acq_size = sff_pars['step_size'], npoints = sff_pars['num_proj'], acq_time = sff_pars['exp_time'], mode = sff_pars['mode'])

    umv(diffrz,0)      
    umv(diffty, stage_diffty_zero)
    umv(bigy, bigy_out)
    diffty_airpad.off()
    user.cpm18_goto(cpm18_tuned)
    
    umv(samtz,0)
    print("All done!")
"""






def load_ramp_by_target(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    write_to_file = to_file if to_file is not None else sys.stdout
    # marana_large_beam(pct_pars)
    current_load = stress_adc.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return


def define_pars_one_load():
    """Produces default input parameters for one load
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    step_pars['dct_pars'] = define_dct_pars()
    step_pars['pct_pars'] = define_pct_pars()
    step_pars['tt_pars'] = define_tt_pars()
    step_pars['ff_pars'] = define_ff_pars()
    step_pars['sff_pars']= define_sff_pars()
    step_pars['target'] = load
    step_pars['load_step'] = step
    par_list.append(step_pars)
    return par_list


#def load_sequence(par_list, grain_list):
#    """Load Experiment Master Sequence.
#    
#    This function will perform scan sequences (pct, dct, tt) for a list of target load values defined in "par_list" 
#    Note: grain_list is currently produced by a matlab script and can be re-created / imported via:  grain_list = user.read_tt_info([list_of_grain_ids])
#    par_list can be created via: par_list = user.define_pars()  and sub-parameters for dct, pct, tt can be adapted to cope with increasing mosaicity
#
#   """
#   for step_pars in par_list:
#       dct_pars = step_pars['dct_pars']
#       pct_pars = step_pars['pct_pars']
#      tt_pars = step_pars['tt_pars']
#       ff_pars = step_pars['ff_pars']
#      sff_pars = step_pars['sff_pars']
#      target = step_pars['target']
#      load_step = step_pars['load_step']
#      grain_list = load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, ff_pars, sff_pars, target, load_step)
#  return grain_list

def load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, ff_pars, sff_pars, target):
    """Performs a loadramp to the new target value and launches PCT, DCT and a series of TT scans at this new target load
    """
    ## PCT
    if pct_pars['active']:
        maranain(pct_pars['dist'])
        marana_pct(pct_pars)
        # marana_large_beam(pct_pars)
        # load_ramp_by_target(target)
        scan_name = 'pct_%dN_' % target
        
        umv(diffrz, 0)
        # tomo_by_fscan_dict(scan_name, pct_pars)
        fulltomo.full_turn_scan(scan_name)

    ## DCT
    if dct_pars['active']:
        marana_dct(dct_pars)
        scan_name = 'dct_%dN_' % target
        dct_zseries(dct_pars, datasetname = scan_name)

    ## TT
    if tt_pars['active']:
        scan_name = 'tt_%dN_' % target
        #marana_tt(grain_list, tt_pars, target)
        frelon16in(4, tt_pars)
        grain_list = loop_grains(grain_list, tt_pars, target)

    ## 3DXRD
    if ff_pars['active']:
        ffin(ff_pars)
        scan_name = 'ff_%dN_' % target    
        ff_zseries(ff_pars, scan_name)

    ## s-3DXRD
    if sff_pars['active']:
        #scan_name = 'sff_%dN_g%d' % (target, grain_list[0]['gr_id'])
        #tdxrd_pointscan(scan_name, sff_pars, grain)
        scanning_ff(sff_pars)
        scanname = 'sff_%dN' % target
        sff_scan(scanname, sff_pars)
        
        # scan_name = 'sff_2d_%dN_g%d' % (target, grain_list[0]['gr_id'])
        # sff_one_grain(scan_name, sff_pars, grain_list[0])

    return grain_list


def loop_grains(grain_list, tt_pars, target):
    """Loops through a list of Topotomo grains  and performs refinement + TT scan_acquisition
    :param dict grain_list:  list of grain structures containing Topotomo alignment values
    :param dict tt_pars: Topotomo scan parameters
    :param int step: running number of load step in load-sequence  
    """
    tfoh1.set(20,'Be')
    tfoh1.set(0,'Al')
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    #marana3.image.roi = tt_pars['image_roi'] # [510, 510, 900, 900]
    #marana3.roi_counters.set('roi1', tt_pars['counter_roi'])  #[300, 300, 300, 300])
    #ACTIVE_MG.enable('marana:roi_counters:roi1_avg')
    for i in range(len(grain_list)):
        newdataset('grain_%04d_%dN_' % (grain_list[i]['gr_id'], target))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time)
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'])
        sleep(1)
        #update_grain_info(grain_list[i])
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    sam_dct_pos()
    return grain_list

def marana_tt(grain_list, tt_pars, target):
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    umv(d3tz, d3tz_pos_tt, nfdtx, tt_pars['dist'])
    marana3.image.roi = tt_pars['image_roi']
    marana3.roi_counters.set('roi1', tt_pars['counter_roi'])
    ACTIVE_MG.enable('marana:roi_counters:roi1_avg')
    for i in range(len(grain_list)):
        newdataset('grain_%04d_%dN_' % (grain_list[i]['gr_id'], target))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time, tt_pars['scan_mode'])
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'], tt_pars['scan_mode'])
        sleep(1)
        update_grain_info(grain_list[i])
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    sam_dct_pos()
    return grain_list

        
def refine_tt_alignment(tt_grain, ang_step=0.05, search_range=0.5, exp_time=0.1, scan_mode = 'TIME'):
    """Refine the topotomo samrx, samry alignment.
    
    This function runs 4 base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use (in degrees).
    :param float search_range: the base tilt angular search range in degrees.
    """
     # half base tilt range in degree
    n_search_images = 2 * search_range / ang_step
    
    # open a script to gather all acquisition commands
    print(tt_grain)
    gid = tt_grain['gr_id']
    print('find range for grain %d\n' % gid)
    # check that diffry is negative (Bragg alignment)
    #if tt_grain['diffry'] > 0:
    #    raise(ValueError('diffry value should be negative, got %.3f, please check your data' % tt_grain['diffry']))
        
    # align our grain
    topotomo_tilt_grain_dict(tt_grain)

    # define the ROI automatically as [710, 710, 500, 500]
    frelon16.image.roi = [0, 0, 1920, 1920]
    ct(0.1)
    frelon16.roi_counters.set('roi1', [810, 810, 300, 300])
    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    #exp_time = exp_time / tt_grain['int_factor']
    # first scan at 0 deg
    umvct(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_000, end_angle_000, cen_angle_000 = get_limits_and_weighted_cen_angle(fscan.get_data())
    # second scan at 180 deg
    umv(diffrz, 180)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_180, end_angle_180, cen_angle_180 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samry by half the difference
    samry_offset = 0.5 * (cen_angle_000 - cen_angle_180)
    print("moving samry by %.3f, final position: %.3f" % (samry_offset, tt_grain['samry'] + samry_offset))
    umvr(samry, samry_offset)

    # third scan at 270 deg
    umv(diffrz, 270)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, 0.1, scan_mode = scan_mode)
    start_angle_270, end_angle_270, cen_angle_270 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # fourth scan at 90 deg
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_090, end_angle_090, cen_angle_090 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samrx by half the difference
    samrx_offset = 0.5 * (cen_angle_090 - cen_angle_270)
    print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    #samrx_offset =  tt_grain['diffry'] - cen_angle_090
    #print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    umvr(samrx, samrx_offset)

    # update values in tt_grain
    tt_grain['samrx'] += samrx_offset
    tt_grain['samry'] += samry_offset
    #update_grain_info(tt_grain)
    print('values updated in tt_grain record')
    return tt_grain

def find_range(tt_grain, ang_step=0.05, search_range=0.3, exp_time=0.1, scan_mode = 'TIME'):
    """Find the topotomo angular range.
    
    This function runs two base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition. Note that this function 
    assumes that the grain has been aligned already.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use.
    :param float search_range: the base tilt angular search range in degrees.
    """
    step = 1  # load step
    n_search_images = 2 * search_range / ang_step
    
    # open a script to gather all acquisition commands
    gid = tt_grain['gr_id']

    
    print('find range for grain %d\n' % gid)
    
    # see if we need to create a new dataset or not
    #dataset = 'grain_%04d_checkrange_step_%d' % (gid, step)
    #newdataset(dataset)

    # define the ROI automatically as [710, 710, 500, 500]
    frelon16.roi_counters.set('roi1', [810, 810, 300, 300])
    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    #ACTIVE_MG.enable('marana:roi_counters:roi1_avg')
    # first scan at 90 deg
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_90, end_angle_90 = get_limits(fscan.get_data())
    diffry_range_90 = max(tt_grain['diffry'] - start_angle_90, end_angle_90 - tt_grain['diffry'])

    # second scan at 0 deg
    umv(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode= scan_mode)
    start_angle_00, end_angle_00 = get_limits(fscan.get_data())
    diffry_range_00 = max(tt_grain['diffry'] - start_angle_00, end_angle_00 - tt_grain['diffry'])

    # use the upper range value to define topotomo bounds
    print('diffry_range_00=%.3f - diffry_range_90=%.3f' % (diffry_range_00, diffry_range_90))
    diffry_range = max(diffry_range_00, diffry_range_90)

    # now find out the largest range to cover the grain
    diffry_start = tt_grain['diffry'] - diffry_range
    diffry_end = tt_grain['diffry'] + diffry_range
    n_acq_images = (diffry_end - diffry_start) / ang_step

    
    
    return diffry_start, n_acq_images

def get_limits(scan_data, thres=0.05, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry = scan_data['diffry']
    # work out the angular step
    ang_step = diffry[1] - diffry[0]
    intensity = scan_data['frelon16:roi_counters:roi1_avg']
    # background correction
    bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) -1
    start_angle = diffry[start_pos] - padding * ang_step
    end_angle = diffry[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, number of images={}'.format(start_angle, end_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle
    
def get_limits_and_weighted_cen_angle(scan_data, thres=0.05, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry = scan_data['diffry']
    # work out the angular step
    ang_step = diffry[1] - diffry[0]
    intensity = scan_data['frelon16:roi_counters:roi1_avg']
    # background correction
    #bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    bg = np.min(intensity)
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres)  - 1  # removed this because we kept getting start_pos = end_pos which leads to nan
    if start_pos == end_pos:
        # peak only showed up on one frame
        if end_pos != len(intensity) - 1:
            # end_pos is not the final frame
            # bump it by one
            end_pos += 1
        else:
            # end_pos is the final frame
            # decrease start_pos instead
            start_pos -= 1
        
    weighted_cen_pos = 0;
    for i_pos in range(start_pos, end_pos, 1):
        weighted_cen_pos += i_pos * intensity[i_pos]
    weighted_cen_pos = weighted_cen_pos / np.sum(intensity[start_pos:end_pos])
    # compute the angle using linear interpolation
    print(start_pos, end_pos)
    weighted_cen_angle = (diffry[start_pos] * (end_pos - weighted_cen_pos) + diffry[end_pos] * (weighted_cen_pos - start_pos)) / (end_pos - start_pos)
    start_angle = diffry[start_pos] - padding * ang_step
    end_angle = diffry[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, weighted_cen_angle={}, number of images={}'.format(start_angle, end_angle, weighted_cen_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle, weighted_cen_angle




import json
import time
import os

class NumpyEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self,obj)


def writedictionaryfile(dictname,filename):
    """to write dictionary to file"""
    datatowrite = json.dumps(dictname)
    with open('%s'%filename, 'w') as f:
        f.write(datatowrite)
    print("Dictionary written as %s"%filename)
    return

def appenddictionaryfile(dictname,filename):
    """to write dictionary to file"""
    if os.path.exists('%s'%filename):
        with open('%s'%filename, 'r+') as f:
            data_to_write = [json.load(f)]
            dictname['time'] = str(time.ctime())
            data_to_write.append(dictname)
            f.seek(0)
            json.dump(data_to_write, f)
    else:
        newdict = dictname
        newdict['time'] = str(time.ctime())
        with open('%s'%filename, 'w') as f:
            json.dump(newdict, f)
            #f.write(datatowrite)
    print("Dictionary appended to %s"%filename)
    return

def readdictionaryfile(filename):
    """to read dictionary from file, returns dictionary"""
    with open('%s'%filename, 'r') as f:
        readdata = [json.loads(f.read())]
    print("Dictionary read from %s"%filename)
    return readdata[-1]


def update_grain_info(grain):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    writedictionaryfile(grain,'%s/%s_g%s'%(basedir,samplename,grain['gr_id']))
    #appenddictionaryfile(grain,'%s_g%s_history'%(SCAN_SAVING.collection_name,grain['gr_id']))
    return

def get_json_grain_info(grain_number):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    return readdictionaryfile('%s/%s_g%s'%(basedir,samplename,str(grain_number)))

def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])


def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        #finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])


def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)

def ref_scan_samy(exp_time, nref, ystep, omega, scanmode):
    umvr(samy, ystep)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umvr(samy, -ystep)
    
def ref_scan_difftz(exp_time, nref, zstep, omega, scanmode):
    difftz0 = difftz.position
    umv(difftz, difftz0 + zstep)
    print("fscan(diffrz, %6.2f, 0.1, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.1, nref, exp_time, scan_mode = scanmode)
    umv(difftz, difftz0)
    
class LoadLoop:
    def __init__(self):
        self._task = None
        self._stop = False
        self._sleep_time = 1
        self._filepath = None
        
    def start(self,target,loadstep,filepath,sleep_time=1.,time_step=0.5):
        self._filepath = filepath
        if self._task:
            self._stop = True
            self._task.get()

        self._sleep_time = sleep_time
        self._stop = False
        self._task = gevent.spawn(self._run,target,loadstep,time_step)

    def stop(self):
        self._stop = True
        if self._task: 
           self._task.get()

    def _run(self,target,loadstep,time_step):
        newdataset('loadramp_%d' %  loadstep)
        with open(self._filepath,'a') as f:
            while not self._stop:
                load_ramp_by_target(target,0,loadstep,time_step,to_file=f)
                gevent.sleep(self._sleep_time)

def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')
    
def ff_zseries(ff_pars, datasetname = 'ff'):
    # att_in()
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    # att_out()
    print('ff_zseries succeed')
    return('Succeed')

def tomo_series_cst_load(start, num_scans, target, sleep_time=0, pct_pars=None):
    for i in np.arange(start, start+num_scans):
        tomo.full_turn_scan(str(i))
        load_ramp_by_target(target, 0.05, 1)
	#dct_marana_dict(str(i), pct_pars)
        sleep(sleep_time)
    return

def ftomo_series(scanname, start, num_scans, sleep_time=0, pars=None):
    for i in np.arange(start, start+num_scans):
        newdataset(scanname + str(i))
        umv(diffrz, pars['start_pos']);
        fsh.disable()
        fsh.close()
        print("taking dark images")
        ftimescan(pars['exp_time'], pars['nref'],0)
        fsh.enable()
        print("taking flat images")
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'], pars['scan_mode'])
        print("taking projections...")
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        print("resetting diffrz to 0")
        umv(diffrz, pars['start_pos']+360);
        diffrz.position=pars['start_pos'];
        sleep(sleep_time)
    return

def tdxrd_boxscan(scanname, ff_pars):
    print("using slit_vg %f and slit_hg %f" % (ff_pars['slit_vg'], ff_pars['slit_hg']))
    umv(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.5, s8hg, ff_pars['slit_hg'] + 0.5)
    newdataset(scanname)
    finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
 
def update_flat():
    image_corr.take_dark()
    image_corr.take_flat()
    image_corr.dark_on()
    image_corr.flat_on()

def flat_on():
    image_corr.dark_on()
    image_corr.flat_on()

def flat_off():
    image_corr.dark_off()
    image_corr.flat_off()

