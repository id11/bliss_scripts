from dataclasses import dataclass
from typing import Union, Any
import numpy as np

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection



def flatten(container):
    flat_values = []
    for i in container.values():
        flat_values.append(i)
        if len(i.dependents) > 0:
            flat_values.extend(flatten(i.dependents))
            
    return flat_values


class AntiCollisionScene():
    """Class to detect beamline hardware collisions"""
    
    def __init__(self, offset):
        self.position_lab = offset
        self.dependents = dict()
        
        # placeholder so dependency calculations work
        self.value_vector = np.array([0., 0., 0.])
    
    @property
    def dependents_flat_list(self):
        return flatten(self.dependents)
    
    def test_move_relative(self, axis_name, rel_value, resolution=101):
        print(f'Simulating relative move of {axis_name} by {rel_value}...')
        for hw in self.dependents_flat_list:
            if hw.name == axis_name:
                axis = hw
        abs_positions = np.linspace(axis.value, axis.value + rel_value, resolution)
        initial_position = axis.value
        for pos in abs_positions:
            axis.value = pos
            # print(pos)
            if self.check_collisions():
                print(f'Simulated collision detected at {axis_name} = {pos}!')
                break
        # axis.value = initial_position
        
        
    def check_collisions(self):
        # get all the dependents in the scene as a flat list
        # for each dependent, check against each other dependent
        # skip if they depend on each other
        all_hardware = self.dependents_flat_list
        for inc1, hw1 in enumerate(all_hardware):
            for inc2, hw2 in enumerate(all_hardware[inc1+1:]):
                if hw1 is not hw2.depends_on:
                    if hw2 is not hw1.depends_on:
                        # print(f'Checking {hw1.name} with {hw2.name}!')
                        if hw1.collides_with(hw2):
                            print(f'{hw1.name} collides with {hw2.name}!')
                            return True
    
    def plot(self):
        fig = plt.figure(figsize=(15,15))
        ax = fig.add_subplot(111, projection='3d')
        for dep in self.dependents_flat_list:
            dep.add_to_axis(ax)
        
        ax.set_xlim(-3000, 3000)
        ax.set_ylim(-3000, 3000)
        ax.set_zlim(-3000, 3000)
        
        ax.set(xlabel='x', ylabel='y', zlabel='z')
        ax.view_init(elev=45, azim=60)
        fig.tight_layout()
        plt.show()
    

def box_collision_check(box1, box2):
    if np.abs(box1.position_lab[0] - box2.position_lab[0]) > (box1.x_length / 2 + box2.x_length/2):
        return False
        
    if np.abs(box1.position_lab[1] - box2.position_lab[1]) > (box1.y_length / 2 + box2.y_length/2):
        return False
        
    if np.abs(box1.position_lab[2] - box2.position_lab[2]) > (box1.z_length / 2 + box2.z_length/2):
        return False
        
    return True

def box_cylinder_collision_check(box, cyl):
    if cyl.orientation == 'x':
        if np.abs(box.position_lab[0] - cyl.position_lab[0]) > (box.x_length / 2 + cyl.length / 2):
            return False

        closest_z = max(box.position_lab[2] - box.z_length / 2, min(cyl.position_lab[2], box.position_lab[2] + box.z_length / 2))
        closest_y = max(box.position_lab[1] - box.y_length / 2, min(cyl.position_lab[1], box.position_lab[1] + box.y_length / 2))
        dist_z = closest_z - cyl.position_lab[2]
        dist_y = closest_y - cyl.position_lab[1]
        
        if dist_z**2 + dist_y**2 > (cyl.diameter/2)**2:
            return False
    
    elif cyl.orientation == 'y':
        if np.abs(box.position_lab[1] - cyl.position_lab[1]) > (box.y_length / 2 + cyl.length / 2):
            return False

        closest_x = max(box.position_lab[0] - box.x_length / 2, min(cyl.position_lab[0], box.position_lab[0] + box.x_length / 2))
        closest_z = max(box.position_lab[2] - box.z_length / 2, min(cyl.position_lab[2], box.position_lab[2] + box.z_length / 2))
        dist_x = closest_x - cyl.position_lab[0]
        dist_z = closest_z - cyl.position_lab[2]
        
        if dist_x**2 + dist_z**2 > (cyl.diameter/2)**2:
            return False
    
    elif cyl.orientation == 'z':
        if np.abs(box.position_lab[2] - cyl.position_lab[2]) > (box.z_length / 2 + cyl.length / 2):
            return False

        closest_x = max(box.position_lab[0] - box.x_length / 2, min(cyl.position_lab[0], box.position_lab[0] + box.x_length / 2))
        closest_y = max(box.position_lab[1] - box.y_length / 2, min(cyl.position_lab[1], box.position_lab[1] + box.y_length / 2))
        dist_x = closest_x - cyl.position_lab[0]
        dist_y = closest_y - cyl.position_lab[1]
        
        if dist_x**2 + dist_y**2 > (cyl.diameter/2)**2:
            return False
    
    return True

        

def cylinder_collision_check(cyl1, cyl2):
    """Check if two cylinders collide.
    This assumes each cylinder is oriented along x, y or z
    This works by projecting the shape of the cylinders into the YZ, XZ and XY planes
    We assume there's a collision if the projected shapes intersect in all three planes
    """
    if ((cyl1.proj_x.collides_with(cyl2.proj_x)) and
       (cyl1.proj_y.collides_with(cyl2.proj_y)) and
       (cyl1.proj_z.collides_with(cyl2.proj_z))):
           return True
    else:
        return False


def rects_collision_check(rect1, rect2):
    """Simple collision check for two rectangles"""
    dist_i = np.abs(rect1.pos[0] - rect2.pos[0])
    dist_j = np.abs(rect1.pos[1] - rect2.pos[1])
    
    shape_i = rect1.size[0]/2 + rect2.size[0]/2
    shape_j = rect1.size[1]/2 + rect2.size[1]/2
    
    if dist_i <= shape_i:
        if dist_j <= shape_j:
            return True
    
    return False


def circles_collision_check(cir1, cir2):
    sep_i = np.abs(cir1.pos[0] - cir2.pos[0])
    sep_j = np.abs(cir1.pos[1] - cir2.pos[1])
    sep = np.sqrt(sep_i**2 + sep_j**2)
    if sep <= (cir1.radius + cir2.radius):
        return True
    return False


def rect_circle_collision_check(rect, cir):
    closest_i = max(rect.pos[0] - rect.shape[0] / 2, min(cir.pos[0], rect.pos[0] + rect.shape[0] / 2))
    closest_j = max(rect.pos[1] - rect.shape[1] / 2, min(cir.pos[1], rect.pos[1] + rect.shape[1] / 2))
    
    dist_i = cir.pos[0] - closest_i
    dist_j = cir.pos[1] - closest_j
    
    dist_squared = dist_i ** 2 + dist_j ** 2
    
    return dist_squared <= (cir.radius**2)


@dataclass
class Rectangle:
    """2D rectangle with centre-of-mass position (pos) and size""" 
    size: np.ndarray
    pos: np.ndarray
    
    @property
    def shape(self):
        return self.size
    
    def collides_with(self, other):
        if isinstance(other, Rectangle):
            return rects_collision_check(self, other)
        elif isinstance(other, Circle):
            return rect_circle_collision_check(rect=self, cir=other)


@dataclass
class Circle:
   """2D circle with centre-of-mass position (pos) and diameter"""
   diameter: float
   pos: np.ndarray
   
   @property
   def radius(self):
       return self.diameter / 2
   
   def collides_with(self, other):
        if isinstance(other, Rectangle):
            return rect_circle_collision_check(rect=other, cir=self)
        elif isinstance(other, Circle):
            return circles_collision_check(self, other)


@dataclass
class HardwareRegion:
    """Class to represent a contiguous volume in a 3D space, like a detector or an axis.
    All values are in mm"""
    name: str
    depends_on: Any
    # the offset between centre-of-mass positions of the region and the thing it depends on
    # which could be another HardwareRegion or the Scene
    pos_offset: np.ndarray  
    
    def __post_init__(self):
        self.dependents = dict()
        self.depends_on.dependents[self.name] = self
        self.plot_colour = 'red'
    
    @property
    def position_lab(self):
        # What is the centre-of-mass position of this region in the lab frame?
        # Take the centre-of-mass position of the thing it depends on
        # Add the value vector of the thing it depends on (the actual axis USER position)
        # Add my offset
        return self.depends_on.position_lab + self.depends_on.value_vector + self.pos_offset
    
    def collides_with(self, other):
        if isinstance(self, Box):
            if isinstance(other, Box):
                return box_collision_check(self, other)
            elif isinstance(other, Cylinder):
                return box_cylinder_collision_check(box=self, cyl=other)
        elif isinstance(self, Cylinder):
            if isinstance(other, Box):
                return box_collision_check(box=other, cyl=self)
            elif isinstance(other, Cylinder):
                return cylinder_collision_check(self, other)
        else:
            return True


@dataclass
class Box(HardwareRegion):
    """Dataclass for 3D rectangular box"""
    
    size: np.ndarray
    
    # Determine the bounds of the box in its reference frame
    
    def __post_init__(self):
        super().__post_init__()
        self.plot_colour = 'red'
    
    @property
    def x_length(self):
        return self.size[0]
    
    @property
    def y_length(self):
        return self.size[1]
    
    @property
    def z_length(self):
        return self.size[2]
    
    @property
    def x_min_rel(self):
        return -self.x_length / 2
    
    @property
    def x_max_rel(self):
        return self.x_length / 2
    
    @property
    def y_min_rel(self):
        return -self.y_length / 2
    
    @property
    def y_max_rel(self):
        return self.y_length / 2
        
    @property
    def z_min_rel(self):
        return -self.z_length / 2
    
    @property
    def z_max_rel(self):
        return self.z_length / 2
    
    @property
    def x_min_lab(self):
        return self.x_min_rel + self.position_lab[0]
    
    @property
    def x_max_lab(self):
        return self.x_max_rel + self.position_lab[0]
    
    @property
    def y_min_lab(self):
        return self.y_min_rel + self.position_lab[1]
    
    @property
    def y_max_lab(self):
        return self.y_max_rel + self.position_lab[1]
        
    @property
    def z_min_lab(self):
        return self.z_min_rel + self.position_lab[2]
    
    @property
    def z_max_lab(self):
        return self.z_max_rel + self.position_lab[2]
        
    @property
    def vertices(self):
        # 3 x 8 numpy array
        # first axis is X Y Z order
        return np.array([
        [self.x_min_lab, self.y_min_lab, self.z_min_lab],
        [self.x_min_lab, self.y_min_lab, self.z_max_lab],
        [self.x_min_lab, self.y_max_lab, self.z_min_lab],
        [self.x_min_lab, self.y_max_lab, self.z_max_lab],
        [self.x_max_lab, self.y_min_lab, self.z_min_lab],
        [self.x_max_lab, self.y_min_lab, self.z_max_lab],
        [self.x_max_lab, self.y_max_lab, self.z_min_lab],
        [self.x_max_lab, self.y_max_lab, self.z_max_lab],
        ])
    
    @property
    def faces(self):
        return np.array([
            [self.vertices[0], self.vertices[2], self.vertices[6], self.vertices[4]],
            [self.vertices[1], self.vertices[3], self.vertices[7], self.vertices[5]],
            [self.vertices[0], self.vertices[1], self.vertices[3], self.vertices[2]],
            [self.vertices[4], self.vertices[5], self.vertices[7], self.vertices[6]],
            [self.vertices[2], self.vertices[3], self.vertices[7], self.vertices[6]],
            [self.vertices[0], self.vertices[1], self.vertices[5], self.vertices[4]]
            ])
    
    @property
    def plot_repr(self):
        return Poly3DCollection(self.faces, facecolors=self.plot_colour, linewidths=1, edgecolors='k', alpha=0.5)
    
    def add_to_axis(self, ax):
        ax.add_collection3d(self.plot_repr)
    
    def point_is_inside(self, point):
        """Determine if a point is inside or outside the box"""
        if self.x_min_lab <= point[0] <= self.x_max_lab:
            if self.y_min_lab <= point[1] <= self.y_max_lab:
                if self.z_min_lab <= point[2] <= self.z_max_lab:
                    return True
        return False
    

@dataclass
class MotorAxis(Box):
    # the current USER value of the motor 
    value: float
    
    # the offset needed to move value=0 away from the centre-of-mass of the axis
    value_offset: float
    orientation: str
    
    def __post_init__(self):
        super().__post_init__()
        self.plot_colour = 'black'
        self.orientation = self.orientation.lower()
        if self.orientation not in ['x', 'y', 'z']:
            raise ValueError('Orientation must be one of [x, y, z, X, Y, Z]')
    
    @property
    def value_vector(self):
        if self.orientation == 'x': 
            return np.array([self.value + self.value_offset, 0, 0])
        elif self.orientation == 'y':
            return np.array([0, self.value + self.value_offset, 0])
        elif self.orientation == 'z':
            return np.array([0, 0, self.value + self.value_offset])
    
    @property
    def position_lab(self):
        return self.depends_on.position_lab + self.pos_offset + self.depends_on.value_vector
        


@dataclass
class Cylinder(HardwareRegion):
    """3D cylinder"""
    
    length: float
    diameter: float
    orientation: str
    
    def __post_init__(self):
        """Ensure axial orientation"""
        super().__post_init__()
        self.plot_colour = 'green'
        self.orientation = self.orientation.lower()
        if self.orientation not in ['x', 'y', 'z']:
            raise ValueError('Orientation must be one of [x, y, z, X, Y, Z]')
    
    @property
    def x_min_rel(self):
        if self.orientation == 'x':
            return -self.length / 2
        else:
            return -self.diameter / 2
    
    @property
    def x_max_rel(self):
        if self.orientation == 'x':
            return self.length / 2
        else:
            return self.diameter / 2
    
    @property
    def y_min_rel(self):
        if self.orientation == 'y':
            return -self.length / 2
        else:
            return -self.diameter / 2
    
    @property
    def y_max_rel(self):
        if self.orientation == 'y':
            return self.length / 2
        else:
            return self.diameter / 2
    
    @property
    def z_min_rel(self):
        if self.orientation == 'z':
            return -self.length / 2
        else:
            return -self.diameter / 2
    
    @property
    def z_max_rel(self):
        if self.orientation == 'z':
            return self.length / 2
        else:
            return self.diameter / 2
    
    @property
    def x_min_lab(self):
        return self.x_min_rel + self.position_lab[0]
    
    @property
    def x_max_lab(self):
        return self.x_max_rel + self.position_lab[0]
    
    @property
    def y_min_lab(self):
        return self.y_min_rel + self.position_lab[1]
    
    @property
    def y_max_lab(self):
        return self.y_max_rel + self.position_lab[1]
        
    @property
    def z_min_lab(self):
        return self.z_min_rel + self.position_lab[2]
    
    @property
    def z_max_lab(self):
        return self.z_max_rel + self.position_lab[2]
    
    @property
    def proj_x(self):
        # position in YZ plane
        pos = np.array([self.position_lab[1], self.position_lab[2]])
        if self.orientation == 'x':
            # YZ, so it's a circle
            return Circle(diameter=self.diameter, pos=pos)
        elif self.orientation == 'y':
            # length along Y, diameter along Z
            return Rectangle(pos=pos, size=np.array([self.length, self.diameter]))
        elif self.orientation == 'z':
            # diameter along Y, length along Z
            return Rectangle(pos=pos, size=np.array([self.diameter, self.length]))
    
    @property
    def proj_y(self):
        # position in XZ plane
        pos = np.array([self.position_lab[0], self.position_lab[2]])
        if self.orientation == 'x':
            # length along X, diameter along Z
            return Rectangle(pos=pos, size=np.array([self.length, self.diameter]))
        elif self.orientation == 'y':
            # XZ, so it's a circle
            return Circle(diameter=self.diameter, pos=pos)
        elif self.orientation == 'z':
            # diameter along X, length along Z
            return Rectangle(pos=pos, size=np.array([self.diameter, self.length]))
    
    @property
    def proj_z(self):
        # position in XY plane
        pos = np.array([self.position_lab[0], self.position_lab[1]])
        if self.orientation == 'x':
            # length along X, diameter along Y
            return Rectangle(pos=pos, size=np.array([self.length, self.diameter]))
        elif self.orientation == 'y':
            # diameter along X, length along Y
            return Rectangle(pos=pos, size=np.array([self.diameter, self.length]))
        elif self.orientation == 'z':
            # XY, so it's a circle
            return Circle(diameter=self.diameter, pos=pos)
    
    def point_is_inside(self, point):
        if self.orientation == 'x':
            if self.x_min_lab <= point[0] <= self.x_max_lab:
                if np.sqrt(point[1]**2 + point[2]**2) <= self.diameter/2:
                    return True
            return False
        elif self.orientation == 'y':
            if self.y_min_lab <= point[1] <= self.y_max_lab:
                if np.sqrt(point[0]**2 + point[1]**2) <= self.diameter/2:
                    return True
            return False
        elif self.orientation == 'z':
            if self.z_min_lab <= point[2] <= self.z_max_lab:
                if np.sqrt(point[0]**2 + point[2]**2) <= self.diameter/2:
                    return True
            return False
        return False
    
    def add_to_axis(self, ax):
       length_linspace = np.linspace(-self.length/2, self.length/2, 50)
       theta = np.linspace(0, 2*np.pi, 50)
       theta_grid, length_grid = np.meshgrid(theta, length_linspace)
       if self.orientation == 'x':
           length_grid += self.position_lab[0]
           y_grid = self.diameter/2 * np.cos(theta_grid) + self.position_lab[1]
           z_grid = self.diameter/2 * np.sin(theta_grid) + self.position_lab[2]
           x_plot, y_plot, z_plot = length_grid, y_grid, z_grid
       elif self.orientation == 'y':
           length_grid += self.position_lab[1]
           x_grid = self.diameter/2 * np.cos(theta_grid) + self.position_lab[0]
           z_grid = self.diameter/2 * np.sin(theta_grid) + self.position_lab[2]
           x_plot, y_plot, z_plot = x_grid, length_grid, z_grid
       elif self.orientation == 'z':
           length_grid += self.position_lab[2]
           x_grid = self.diameter/2 * np.cos(theta_grid) + self.position_lab[0]
           y_grid = self.diameter/2 * np.sin(theta_grid) + self.position_lab[1]
           x_plot, y_plot, z_plot = x_grid, y_grid, length_grid
           
           
       ax.plot_surface(x_plot, y_plot, z_plot, facecolor=self.plot_colour, alpha=0.5)


def main():
    scene = AntiCollisionScene(offset=np.array([0, 0, 0]))
    
    """
    cyl1 = Cylinder(name='cyl1',
                          depends_on=scene,
                          length=10,
                          diameter=4,
                          orientation='z',
                          pos_offset=np.array([0., 0., 0]))
    
    cyl2 = Cylinder(name='cyl2',
                    depends_on=scene,
                    length=5,
                    diameter=4,
                    orientation='z',
                    pos_offset=np.array([3, 0, 0]))
    """

    ffdty1 = MotorAxis(name='ffdty1',
                       depends_on=scene,
                       orientation='y',
                       size=np.array([400., 1000, 250.]),
                       pos_offset=np.array([850., 0., -1000.]),  # stage is downstream and below the origin
                       value=0,
                       value_offset=0) # 0 is in the middle of the axis roughly
    
    ffdtx1 = MotorAxis(name='ffdtx1',
                       depends_on=ffdty1,
                       orientation='x',
                       size=np.array([2500., 300, 300.]),
                       pos_offset=np.array([870., 0., 300.]),
                       value=400,
                       value_offset=-2500/2)  # half the length to get 0 in one corner
    
    ffdtz1 = MotorAxis(name='ffdtz1',
                       depends_on=ffdtx1, 
                       orientation='z',
                       size=np.array([300., 300, 2000.]),
                       pos_offset=np.array([0., -300., 850.]),
                       value=0,
                       value_offset=0)
    
    frelon3 = Box(name='frelon3',
                  depends_on=ffdtz1,
                  size=np.array([800., 300, 300.]),
                  pos_offset=np.array([-250., 300., 0.]))
    
    nfdtx = MotorAxis(name='MotorAxis',
                      depends_on=scene,
                      orientation='x',
                      size=np.array([1070., 240, 240.]),
                      pos_offset=np.array([170., 900., 600.]),
                      value=0,
                      value_offset=-170-100)
    
    d3ty = MotorAxis(name='MotorAxis',
                     depends_on=nfdtx,
                     orientation='y',
                     size=np.array([200., 2400, 200.]),
                     pos_offset=np.array([0., -1100., 200.]),
                     value=0,
                     value_offset=0)
    
    diffractometer = Cylinder(name='diff',
                              depends_on=scene,
                              length=850,
                              diameter=700,
                              orientation='z',
                              pos_offset=np.array([0., 0., -500]))
    
    # diffractometer = Box(name='diff',
    #                      depends_on=scene,
    #                      size=np.array([700., 700, 850.]),
    #                      pos_offset=np.array([0., 0., -500]))
    
    scene.test_move_relative('ffdtz1', -500)
    
    scene.plot()


if __name__ == '__main__':
    main()
