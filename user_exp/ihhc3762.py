import numpy as np
from scipy.optimize import curve_fit

def vitot(v,i):
    """V, I(mA) to T(C)>50"""
    x = v/i
    d, e, f = 7.45710482e-03,  1.70161372e+00, -4.75545136e+01
    return  (d*x*x) + (e*x) + f
    
def ohmtot(ohm):
    """ohm to T(C)>50"""
    x = ohm
    d, e, f = 7.45710482e-03,  1.70161372e+00, -4.75545136e+01
    return  (d*x*x) + (e*x) + f
    
def ttov(x):
    a, b, c = -2.06986907e-05,  6.51505877e-02, -3.66898981e-01
    return (a*x*x) + (b*x) + c
    
def gotoT(temp):
    vset = new_ttov(temp)
    print('Going to %f V for %f deg C'%(vset,temp))
    hmc.voltage_setpoint = vset
    hmc.output = 'ON'
    print('Heating...')
    sleep(3)
    
def new_ttov(t):
    return 0.03575133*t + 3.17850699  
    
       
def findside(x, y):
    y = y-y.min()
    #cm = (x*y).sum()/y.sum()
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    e1 = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    e2 = np.interp(0, -ny[np.argmax(y):], x[np.argmax(y):])
    return e1, e2

def linefit(x, a, b):
    return  np.abs((a*x) + b)   
    
def align_r(raxis,rrange,rsteps,pzrange,pzsteps,ctim,ctr=mca):
    rcen = raxis.position
    if raxis == shrx:
        if np.abs((rot.position+90)%180)>1:
            umv(rot, 90)
    elif raxis == shry:
        if np.abs((rot.position)%180)>1:
            umv(rot, 0)
    ws = []
    rvals = np.linspace(-rrange,rrange,rsteps)
    nc.piezo_scanmode_on()
    for r in rvals:
        umv(raxis, rcen+r)
        s = dscan(pz,-pzrange,pzrange,pzsteps,ctim,ctr)
        fe = findside( s.get_data('pz'), s.get_data('roi1_avg') )
        ws.append(fe[1]-fe[0])
    p, _ = curve_fit(linefit, rvals[:2], ws[:2], (0,0))
    p, _ = curve_fit(linefit, rvals, ws, (p[0],-10))
    print(ws)
    nc.piezo_scanmode_off()
    rideal = -p[1]/p[0]
    umv(raxis,rcen)
    if np.abs(rideal) < rrange:
        umv(raxis,rcen + rideal)
    return rcen + rideal
    
def scanandcen(axis,rng,steps,ctim,ctr=mca):
    s = dscan(axis,-rng,rng,steps,ctim,ctr)
    x, y = s.get_data(axis.name), s.get_data('roi1_avg')
    y = y-y.min()
    ny = np.clip(y, 0.1*y.max(), 0.9*y.max()) - (0.1*y.max()) 
    return (x*ny).sum()/ny.sum()

def roughzedge(ctim,rng,ctr=mca):
    s = dscan(shtz,-rng,+rng,20,ctim,ctr)
    x, y = s.get_data('shtz'), s.get_data('roi1_avg')
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    edge = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    #edge = x[np.argmin(np.abs(y-( ( np.mean(y[25:20]) + np.mean(y[0:5]) )/2) ))]
    umv(shtz, edge)
    return edge

def pzedge(ctim,rng,ctr=mca):
    s = ascan(pz,50-rng,50+rng,150,ctim)
    x, y = s.get_data('pz'), s.get_data('roi1_avg')
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    edge = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    #edge = x[np.argmin(np.abs(y-( ( np.mean(y[25:20]) + np.mean(y[0:5]) )/2) ))]
    #umv(pz, edge)
    return edge
    
def pzpeak(ctim,rng):
    s = ascan(pz,50-rng,50+rng,150,ctim)
    x, y = s.get_data('pz'), s.get_data('roi1_avg')
    maxz = x[np.argmax(y)]
    umv(pz, maxz)
    return maxz
    
def pzcom(ctim,rng):
    s = ascan(pz,50-rng,50+rng,150,ctim)
    comz = com()
    umv(pz, 50)
    return comz
    
def film_cor(rng, npts, ctim, ctr='eiger:roi_counters:roi1_sum'):
    cens = []
    plotselect(ctr)
    for j,k in enumerate([0,90,180,270]):
        umv(rot, k)
        if j%2 == 0:
            s = fscan(dty, -rng, rng*2/(npts-1), npts, ctim)
        else:
            s = fscan(dty, rng, -rng*2/(npts-1), npts, ctim)
        A = fscan.get_data()
        x, y = A['dty'], A[ctr]
        pk = np.clip(y-np.min(y), 0, 1e9)
        cens.append( (x*pk).sum()/pk.sum() )
        
    pyerr = (cens[0]-cens[2])/2
    pxerr = (cens[1]-cens[3])/2
    dtyideal = np.mean(cens)
    if (py.position+pyerr)>10 and (py.position+pyerr)<90:
        umvr(py, pyerr)
    else:
        umvr(shty, (pyerr/1000))
    if (px.position+pxerr)>10 and (px.position+pxerr)<90:
        umvr(px, pxerr)
    else:
        umvr(shtx, (pxerr/1000))
    umv(dty, dtyideal, rot, 0)
  
   
def new_align_film(ctim=0.1,dran=100):
    plotselect('eiger:roi_counters:roi1_avg')
    umv(rot, 0, shrx, 0, shry, 0)
    #roughzedge(0.05,0.15)
    shiftpzto50(ctim,48)

    umv(dty,-dran)
    #ascan(pz,2,98,150,ctim)
    #c1 = com()
    #umv(dty,dran)
    #ascan(pz,98,2,150,ctim)
    #c2 = com()
    c1 = pzedge(ctim,40)
    umv(dty,dran)
    c2 = pzedge(ctim,40)
    srxoff = np.degrees(np.arctan((c1-c2)/(2*dran))) 
    print(c1,c2,srxoff)   
    umv(dty,0)  
    umvr(shrx,srxoff)   

    umv(rot, 90)
    #roughzedge(0.05,0.15)
    shiftpzto50(ctim,40)
    
    umv(dty,-dran)
    c1 = pzedge(ctim,40)
    umv(dty,dran)
    c2 = pzedge(ctim,40)
    sryoff = np.degrees(np.arctan((c1-c2)/(2*dran)))
    print(c1,c2,sryoff)   
    umv(dty,0)
    umvr(shry,-sryoff) 
    
    umv(rot,0)
    shiftpzto50(ctim,40)
    ed = pzedge(ctim,15)
    umv(pz,ed)
    return srxoff, -sryoff
   
def shiftpzto50(ctim, rng):
    pzcen = pzedge(ctim, rng)
    umv(pz,50)
    umvr(shtz, (pzcen-50)/1e3)
    
def checkandscanZ():
    umv(rot,0,dty,0)
    roughzedge(0.05,0.1)
    pzcen = pzedge(0.05,20)
    umv(pz,50)
    umvr(shtz, (pzcen-50)/1e3)
    pzcen1 = pzedge(0.05,20)
    umv(pz, pzcen1)
    #umv(rot,90,dty,0)
    #roughzedge(0.05,0.1)
    #pzcen = pzedge(0.05,40)
    #umv(pz,50)
    #umvr(shtz, (pzcen-50)/1e3)
    #pzcen2 = pzedge(0.05,20)
    #umv(pz, np.max([pzcen1,pzcen2]))
    
def trackZ_at_temp(temp):
    gotoT(temp)
    umv(rot,0,dty,0)
    for i,ctim in enumerate([0.1,0.1,0.1,0.1,0.6]):
        print('Esitmated T = %i'%vitot(hmc.voltage,hmc.current))
        newdataset('align_%02d_%02d'%(temp,i))
        roughzedge(0.05,0.1)
        pzcen = pzedge(0.05,30)
        umv(pz,50)
        umvr(shtz, (pzcen-50)/1e3)
        pzedge(0.05,15)
        print('fscan_%02d_%02d'%(temp,i))
        print(ctim)
        newdataset('fscan_%02d_%02d'%(temp,i))
        fscan(rot,0,0.1,3600,ctim)

def trackZ_at_temp_continued(temp): 
    for i in range(5,15):
        print('Esitmated T = %i'%vitot(hmc.voltage,hmc.current))
        newdataset('align_%02d_%02d'%(temp,i))
        roughzedge(0.05,0.1)
        pzcen = pzedge(0.05,30)
        umv(pz,50)
        umvr(shtz, (pzcen-50)/1e3)
        pzedge(0.05,15)
        print('fscan_%02d_%02d'%(temp,i))
        newdataset('fscan_%02d_%02d'%(temp,i))
        fscan(rot,0,0.1,3600,0.1)
    
              
def trackZ_at_temp_overnight():
    temps = [350]
    try:
        for temp in temps:
            gotoT(temp)
            umv(rot,0,dty,0)
            for i in range(5):
                print(temp)
                print('Esitmated T = %i'%vitot(hmc.voltage,hmc.current))
                newdataset('align_%02d_%02d'%(temp,i))
                align_film()
                print('fscan_%02d_%02d'%(temp,i))
                newdataset('fscan_%02d_%02d'%(temp,i))
                fscan(rot,0,0.1,3600,0.1)
        hmc.output='OFF'
    except:
        hmc.output='OFF'
        
                
def tempramp():
    pzcen = pzedge(0.05,30)
    umv(pz,50)
    umvr(shtz, (pzcen-50)/1e3)
    pzpeak(0.05,15)
    newdataset('fscan_120_03')
    fscan(rot,0,0.1,3600,0.5)

    temps = np.arange(130,355,10)
    print(temps)
    try:
        for temp in temps:
            gotoT(temp)
            umv(rot,0,dty,0)
            for i,ctim in enumerate([0.1,0.5]):
                print(temp)
                newdataset('align_%02d_%02d'%(temp,i))
                if i == 0:
                    align_film()
                else:
                    pzcen = pzedge(0.05,30)
                    umv(pz,50)
                    umvr(shtz, (pzcen-50)/1e3)
                    pzpeak(0.05,15)
                print('fscan_%02d_%02d'%(temp,i))
                newdataset('fscan_%02d_%02d'%(temp,i))
                fscan(rot,0,0.1,3600,ctim)
        hmc.output='OFF'
    except:
        hmc.output='OFF'
        
def tempramp2():
    temps = np.arange(360,505,30)
    print(temps)
    try:
        for temp in temps:
            gotoT(temp)
            umv(rot,0,dty,0)
            for i,ctim in enumerate([0.1,0.2]):
                print(temp)
                newdataset('align_%02d_%02d'%(temp,i))
                if i == 0:
                    align_film()
                print('fscan_%02d_%02d'%(temp,i))
                newdataset('fscan_%02d_%02d'%(temp,i))
                fscan(rot,0,0.1,3600,ctim)
        hmc.output='OFF'
    except:
        hmc.output='OFF'  
        
            

def tempramp2_continued():
    temp=390
    gotoT(temp)
    umv(rot,0,dty,0)
    for i in range(2,20):
        print('fscan_%02d_%02d'%(temp,i))
        newdataset('fscan_%02d_%02d'%(temp,i))
        fscan(rot,0,0.1,3600,0.2)       
                
                
def tempramp3():
    temps = np.arange(360,505,30)
    print(temps)
    try:
        for temp in temps:
            gotoT(temp)
            umv(rot,0,dty,0)
            for i,ctim in enumerate([0.1,0.2]):
                print(temp)
                if i == 0:
                    newdataset('align_%02d_%02d'%(temp,i))
                    new_align_film()
                print('fscan_%02d_%02d'%(temp,i))
                newdataset('fscan_%02d_%02d'%(temp,i))
                if i == 1:
                    umvr(shrx,0.2)
                fscan(rot,0,0.1,3600,ctim)
        hmc.output='OFF'
    except:
        hmc.output='OFF'  


def tiltedscan(srxoff):
    umv(shrx,srxoff-0.2)
    umv(rot,90)
    pzcom(0.02,15)
    fscan(rot,0,1,180,15/180)
 


def uptoconstanttemp():
    temps = np.arange(100,260,20)
    print(temps)
    for temp in temps:
        print(temp)
        gotoT(temp)
        print('align_%02d_00'%temp)
        newdataset('align_%02d_00'%temp)
        srxoff, sryoff = new_align_film()
        
        umv(shrx,srxoff+0.1)
        umv(rot,90)
        pzedge(0.02,20)
        
        print('fscan_%02d_00'%temp)
        newdataset('fscan_%02d_00'%temp)
        tfoh1.set(12, 'Be')
        fscan(rot,0,0.1,3600,0.3)
        tfoh1.set(4, 'Be')

    temp = 280
    for i in range(1,50):      
        print('align_%02d_%02d'%(temp,i))
        #umv(shrx,srxoff+0.1)
        umv(rot,90)
        pzedge(0.02,20)
        
        print('fscan_%02d_%02d'%(temp,i))
        newdataset('fscan_%02d_%02d'%(temp,i))
        tfoh1.set(12, 'Be')
        fscan(rot,0,0.1,3600,0.3)
        tfoh1.set(4, 'Be')
        
        
def uptoconstanttemp2():
    pristinescan()
    temps = np.arange(200,300,10)
    print(temps)
    for temp in temps:
        print(temp)
        gotoT(temp)
        print('align_%02d_00'%temp)
        newdataset('align_%02d_00'%temp)
        srxoff, sryoff = new_align_film()
        print('fscan_%02d_00'%temp)
        tfoh1.set(12, 'Be')
        newdataset('fscan_%02d_00'%temp)
        fscan(rot,0,0.1,3600,0.02)
        newdataset('fscan_%02d_01'%temp)
        fscan(rot,3600,-0.1,3600,0.3)
        tfoh1.set(4, 'Be')

    temp = 300
    for i in range(2,50,2):      
        print('align_%02d_%02d'%(temp,i)) 
        umv(rot,0)
        ed = pzedge(0.02,15)
        umv(pz,ed)    
        print('fscan_%02d_00'%temp)
        tfoh1.set(12, 'Be')    
        newdataset('fscan_%02d_%02d'%(temp,i))
        fscan(rot,0,0.1,3600,0.02)
        newdataset('fscan_%02d_%02d'%(temp,i+1))
        fscan(rot,3600,-0.1,3600,0.3)
        tfoh1.set(4, 'Be')
                     
def pristinescan():
    tfoh1.set(12, 'Be')
    newdataset('fscan_p3_00')
    fscan(rot,0,0.1,3600,0.02)
    newdataset('fscan_p3_01')
    fscan(rot,360,-0.1,3600,0.3)
    tfoh1.set(4, 'Be')


def temp_calib():        
    temps = np.arange(140,361,20)
    for temp in temps:
        gotoT(temp)
        umv(rot,0,dty,0)
        sleep(60)
        newdataset('align_%02d'%(temp))
        print('align_%02d'%(temp))
        pzp = pzpeak(0.05,30)
        umv(pz,50)
        umvr(shtz, (pzp-50)/1e3)
        pzpeak(0.05,15)
        newdataset('fscan_%02d'%(temp))
        fscan(rot,0,180,2,20)
        
        

def temp_calib_RT():        
    umv(rot,0,dty,0)
    newdataset('align_RT')
    pzp = pzpeak(0.05,30)
    umv(pz,50)
    umvr(shtz, (pzp-50)/1e3)
    pzpeak(0.05,15)
    newdataset('fscan_RT')
    fscan(rot,0,180,2,20)
