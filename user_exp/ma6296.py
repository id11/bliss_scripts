# -*- coding: utf-8 -*-
"""
Created on Sat Nov  2 15:26:44 2024

@author: louis
"""


import time 
# from matplotlib import pyplot
from pipython import GCSDevice, pitools, datarectools
# import numpy as np
# from pypylon import pylon
# from PIL import Image
# import os


CONTROLLERNAME = 'E-754' 
STAGES = None  
REFMODES = None

SCALEOUTPUT = 0.1

CYCLELENGTH = 3125
SINUSLENGTH = 3125
SINUSSTART = 0
SINUSCENTER = 1562

#output_folder = os.path.join(os.path.expanduser("~"), "Documents", "Images_Camera")
#folder = os.path.join(os.path.expanduser("~"), "Documents", "data_fatigue_2")


def AutoZero(pidevice) :
    '''Set Automatic Zero Point Calibration.
    
    Internally, the method calls the ATZ command : ATZ [{<AxisID> <LowValue>}]
    '''
    print('AutoZero initialisation...')
    pidevice.ATZ(1, 0)
    pitools.waitonautozero(pidevice, 1)
    print('AutoZero done')
    
    time.sleep(1)


def fatigue(pidevice):
    '''This function set and start the fatigue test.'''
    
    Vmax = float(input('Enter max value for amplitude (Newton) : '))
    Vmin = float(input('Enter min value for amplitude (Newton) : '))
    Ncycles = int(input('Enter number of cycles for the fatigue test : '))
    
    amplitude = Vmax - Vmin
    
    sinus(amplitude, Vmin, Ncycles, pidevice)


def sinus(amp, amp_offset, nb, pidevice):
    """Function to create a sine wave signal, start the sine wave signal and grab images

    This function creates a sine wave with the specified parameters with the WAVE_SIN_P command
    The WSL command set the connection of Wave Table to Wave Generator
    The WGC command set the number of cycles
    The command is started with WGO

    @param float amp: the amplitude in Newton
    @param float amp_offset : the offset of the amplitude in Newton
    @param int nb : number of cycles to do
    """
    wavegenerator = 1
    wavetable = 1
    
    print(f'Set sine waveforms  for wave table {wavetable}')
    pidevice.WAV_SIN_P(table=wavetable, firstpoint=SINUSSTART,
                        numpoints=CYCLELENGTH, append='X', center=SINUSCENTER,
                        amplitude=amp, offset=amp_offset, seglength=SINUSLENGTH)
    if pidevice.HasWSL():
        print(f'Connect wave generator {wavegenerator} to wave table {wavetable}')
        pidevice.WSL(wavegenerator, wavetable)
    if pidevice.HasWGC():
        print(f'Set wave generator {wavegenerator} to run for {nb} cycles')
        pidevice.WGC(wavegenerator, nb)
    
    pidevice.SVO(1, True)
    
    start = input('ready to go ? [yes/no] ')
    if start == "yes":
        pidevice.WGO(wavegenerator, 1)
        while pidevice.IsGeneratorRunning(wavegenerator)[1] is True:
            time.sleep(1.0)
        print("Test Done")
    else:
        print('No test done')


def main():
    with GCSDevice() as pidevice:
        pidevice.ConnectTCPIP(ipaddress='172.29.14.170')
        #pidevice.ConnectTCPIP(ipaddress='192.168.90.17')
        #pidevice.ConnectUSB(serialnum='123014723')
        #pidevice.ConnectRS232(comport=1, baudrate=115200)

        print('connected: {}'.format(pidevice.qIDN().strip()))

        if pidevice.HasqVER():
            print('version info: {}'.format(pidevice.qVER().strip()))

        print('initialize connected stages...')
        pitools.startup(pidevice, stages=STAGES, refmodes=REFMODES)
        
        """CCL 1 Advanced permit to enter parameters modification"""
        pidevice.CCL(1, "Advanced")
        
        """Then we use SPA command to configure the analog output to watch the piezo's signal on another device (oscilloscope, Labview, ...)
        0x0A000003 and 0x0A000004 select the output type and index
        Finally 0x07001005 scales the output value regarding how many V may handle the other device."""
        pidevice.SPA(2, 0x0A000003, 2)
        pidevice.SPA(2, 0x0A000004, 1)
        pidevice.SPA(1, 0x07001005, SCALEOUTPUT)
        
        """SPA command is eventually used to configure the linearization of the piezo's analog signal to a digital signal for the controller.
        0x02000200 for the offset and 0x02000300 for the gain"""
        pidevice.SPA(3, 0x02000200, 0)
        pidevice.SPA(3, 0x02000300, 4)
        
        do_zero = input('Do Auto-Zero ? [yes/no] ')
        if do_zero == 'yes':
            AutoZero(pidevice)
            fatigue(pidevice)
            
        else:
            fatigue(pidevice)            


main() 
