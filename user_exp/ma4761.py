import numpy as np
import time
import os
from scipy.io import loadmat

def read_tt_infos(gid):
    data_dir = '/data/visitor/ma4500/id11/DCT_Analysis/Ni_s11/Ni_s11_dct1/topotomo'
    data_path = os.path.join(data_dir, 'grain_%04d_step_1.mat' % gid)
    data = loadmat(data_path)
    t = data['out'][0][0][0][0]
    values = t[0][0][0][0][0]
    d = {}
    d['gr_id'] = values[0][0][0]
    # double check that the grain id is right
    if d['gr_id'] != gid:
        raise(ValueError('grain id mismatch, check your matlab output in file %s' % data_path))
    d['nfdtx'] = values[1][0][0]
    d['d3tz'] = values[2][0][0]
    d['diffry'] = values[3][0][0]
    d['samrx'] = values[4][0][0]
    d['samry'] = values[5][0][0]
    d['samtx'] = values[6][0][0]
    d['samty'] = values[7][0][0]
    d['samtz'] = values[8][0][0]
    d['samrx_offset'] = values[9][0][0]
    d['samry_offset'] = values[10][0][0]
    d['samtx_offset'] = values[11][0][0]
    d['samty_offset'] = values[12][0][0]
    d['samtz_offset'] = values[13][0][0]
    d['diffrz_offset'] = values[14][0][0]
    print(d)
    return d
    
ly = config.get('ly')
lfyaw = config.get('lfyaw')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
dct_dist = 5.2
tt_dist = 20
abs_dist = 70
ff_offset = 525
tt_offset = -36.15
d3_out = 200
d2_out = -80
samrx_offset = 2.5  #1.4
samry_offset = 0.9
samtz_offset = -0.64  #-0.7

def sam_dct_pos():
    umv(samrx, samrx_offset, samry, samry_offset, diffry, 0)
    umv(samtx, 0, samty, 0, samtz, 0)

def lyin():
    umv(ly,-10,lfyaw,10)

def lyout():
    umv(ly, 0, lfyaw, 0.1)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']

def maranain():
    ff_offset = 525;
    lyout();
    ACTIVE_MG.enable("marana:image") 
    ACTIVE_MG.disable("frelon16*")
    sam_dct_pos()
    umv(ffdtx1, dct_dist + ff_offset, nfdtx, dct_dist)
    umv(d2ty, d2_out, d3ty, 0)
    print("ready to collect marana data") 

def marana_abs():
    tfoh1.set(64,'Al')
    tfoh1.set(0,'Be')
    for i in range (3):
        umv(tfz, -0.46, tfy, 14.488)
    marana.image.roi=[512,0,1024,2048]
    sct(0.05)
    umvct(s8vg, 3.5, s8hg, 2, nfdtx, abs_dist, ffdtx1, abs_dist + ff_offset, d3_bstz, 2000)
    umvct(samtz, 0)

def marana_dct():
    tfoh1.set(0,'Al')
    tfoh1.set(8,'Be')
    for i in range (3):
        umv(tfz, -0.72, tfy, 14.445)
    marana.image.roi=[0,0,2048,2048]
    sct(0.05)
    umvct(s8vg, 0.4, s8hg, 0.66, d3_bstz, 0)
    umvct(nfdtx, dct_dist, ffdtx1, dct_dist + ff_offset)
    umv(samtz, samtz_offset)

def frelon16in(theta):
    tfoh1.set(0,'Al')
    tfoh1.set(16,'Be')
    for i in range (3):
        umv(tfz, -0.64, tfy, 14.488)
    lyout();
    ACTIVE_MG.disable("marana:image") 
    ACTIVE_MG.enable("frelon16:image")
    d2tzpos = np.tan(2*np.deg2rad(theta))*tt_dist
    print("moving d2tz to target position %g"%d2tzpos)
    umvct(d2tz, d2tzpos)
    umv(d2ty, d2_out, d3ty, d3_out)
    umv(ffdtx1, tt_dist + tt_offset + ff_offset, nfdtx, tt_dist + tt_offset)
    umv(d2ty, 0)
    print("ready to collect topotomo data")

def grain_alignments():
    g8 = {}
    g8['samrx'] = -17
    g8['samry'] = 2.9
    g8['samtx'] = -0.124
    g8['samty'] = 0.159
    g8['samtz'] = 0.015


def read_positions():
    g = {}   
    g['samrx']=samrx.position
    g['samry']=samry.position
    g['samtx']=samtx.position
    g['samty']=samty.position
    g['samtz']=samtz.position
    g['d2tz']=d2tz.position
    return g

def topotomo_tilt_grain(gr_id):
    topotomo_params = read_tt_infos(gr_id)
    print(topotomo_params)
    if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
        return('Error')
    #umv(d2ty, d2_out)
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d2tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    #umv(d2ty, 0)
    return('Success')
