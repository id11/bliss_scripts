#MA5837

attrz = config.get('attrz')
atty = config.get('atty')
tfoh1 = config.get('tfoh1')

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def tomoin():
    ACTIVE_MG.enable("marana3:image") 
    ACTIVE_MG.disable("frelon*")
    sheh3.close()
    umv(ffdtz1, 150, d3ty, 0)
    umv(attrz,0,atty,-10)
    umv(s8hg,2,s8vg,2,s7hg,1.5,s7vg,1.5)
    tfoh1.set(0, 'Be')
    sheh3.open()
    print("Ready to collect tomo data") 

def frelonin():
    ACTIVE_MG.disable("marana3*") 
    ACTIVE_MG.disable("frelon16*") 
    ACTIVE_MG.enable("frelon3:image") 
    ACTIVE_MG.enable("frelon3:roi*")
    sheh3.close()
    umv(d3ty,130, ffdtz1, 0)
    umv(attrz,-18,atty,0)
    tfoh1.set(32, 'Be')
    print("Ready to collect diffraction data")
    
def pct_zseries(dset):
    zzero = samtz.position
    zpos = [-1.0, 0.0, 1.0]
    for i, zp in enumerate(zpos):
        umv(samtz, zzero + zp)
        umv(diffrz, 0)
        fulltomo.full_turn_scan(dataset_name='%s_%s'%(dset, i), start_pos=0)
    umv(samtz, zzero)
    
def bb3DXRD_seq(dset):
    zzero = samtz.position
    zpos = [-0.050, -0.025, 0.0, 0.025, 0.050]
    for i, zp in enumerate(zpos):
        newdataset('%s_%s'%(dset,i))
        umv(samtz, zzero+zp)
        finterlaced(diffrz,0,0.25,360/0.25,0.08,mode='ZIGZAG')
    umv(samtz, zzero)



def load_ramp_by_target(target, time_step=0.05):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached
    """
    stress_regul.plot()
    current_load = stress_adc.get_value()
    if abs(target - current_load) < 1:
        print('target position is to close to current position - aborting !')
        print(_get_human_time(),stress.position,current_load)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load)
    return


def load_ramp_by_target_measure_ff(target, time_step=0.05, exp_time=0.2):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached and measures Frelon data
    """
    stress_regul.plot()
    current_load = stress_adc.get_value()
    if abs(target - current_load) < 1:
        print('target position is to close to current position - aborting !')
        print(_get_human_time(),stress.position,current_load)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            sct(exp_time)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            sct(exp_time)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load)
    return

