
import numpy as np, time



# def check_cpm18(pos=6.4158):
def check_cpm18(pos=6.605):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()

def half1mca(ymax, datasetname, ystep=5, ymin=-1801):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, -90,  0.125, 1448, 0.005 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 91, -0.125, 1448, 0.005 )




  
def height1mca(datasetname):
    umv(dty,0)
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    for i, zpos in enumerate(  np.arange(0,100.01,0.25) ):
        umv(pz, zpos)
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, 0, 0.01, 1, rot, -90,  0.125, 1448, 0.005 )
        else:
            fscan2d( dty, 0, 0.01, 1, rot, 91, -0.125, 1448, 0.005 )


def slicescontd():
    z = 90
    width = 30 + (z-50)*10/50
    half1mca(width,
             None,#     f'Z{z:003d}',
             ystep=0.25,
             ymin=22.2 )
    for z in (80,70,60):
        print(z, width)
        umv(pz,z)
        half1mca(width,
                 f'Z{z:003d}',
                 ystep=0.25)

        

def slicesnight():
    # top pz = 0:     px = 58.68, py = 58.81, width = 24
    # bottom pz = 60: px = 62.  , py = 69.5   widthx = 37 wy = 34
    for z in np.arange(0,61,5):
        width = 24 + (37-24)*z/60
#        print(z, width, dx, dy)
        umv(pz,z)
        half1mca(width,
                 f'Z{z:003d}',
                 ystep=0.25)

        

  

def slicesnightctd():
    # top pz = 0:     px = 58.68, py = 58.81, width = 24
    # bottom pz = 60: px = 62.  , py = 69.5   widthx = 37 wy = 34
    for z in np.arange(45,61,5):
        width=40
#        print(z, width, dx, dy)
        umv(pz,z)
        half1mca(width,
                 f'Z{z:003d}',
                 ystep=0.25)

        

  
