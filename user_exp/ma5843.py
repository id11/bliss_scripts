import time
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def vertical_scan_multiramp(ctime,start_hz2,end_hz2,target_temp=700,ramprate=20,time_palier=60,target_temp2=850,ramprate2=10,time_palier2=1800):
    nanodacpool_induction.ramprate = ramprate
    nanodacpool_induction.setpoint = target_temp
    newdataset('vertical_scan_hz_%s_%s'%(start_hz2,end_hz2))
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    print('NOW RAMPING UP')
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -') 
    while nanodacpool_induction.is_ramping():
        start_time = time.time()
        ascan(hz2,start_hz2,end_hz2,3,ctime)
        umv(hz2,start_hz2)
    time_after_ramp = time.time()
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    print('NOW DWELLING')
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    while (time.time() - time_after_ramp) < time_palier:
        start_time = time.time()
        ascan(hz2,start_hz2,end_hz2,3,ctime)
        umv(hz2,start_hz2)
    nanodacpool_induction.ramprate = ramprate2
    nanodacpool_induction.setpoint = target_temp2
    while nanodacpool_induction.is_ramping():
        start_time = time.time()
        ascan(hz2,start_hz2,end_hz2,3,ctime)
        umv(hz2,start_hz2)
    time_after_ramp = time.time()
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    print('NOW DWELLING 2')
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    while (time.time() - time_after_ramp) < time_palier2:
        start_time = time.time()
        ascan(hz2,start_hz2,end_hz2,3,ctime)
        umv(hz2,start_hz2)        
    nanodacpool_induction.ramprate = 25
    nanodacpool_induction.setpoint = 20
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    print('NOW RAMPING DOWN')
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    while nanodacpool_induction.is_ramping():
        start_time = time.time()
        ascan(hz2,start_hz2,end_hz2,3,ctime)
        umv(hz2,start_hz2)

def vertical_scan(ctime,start_hz2,end_hz2,target_temp,ramprate,time_palier):
    nanodacpool_induction.ramprate = ramprate
    nanodacpool_induction.setpoint = target_temp
    newdataset('vertical_scan_hz_%s_%s'%(start_hz2,end_hz2))
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    print('NOW RAMPING UP')
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -') 
    while nanodacpool_induction.is_ramping():
        start_time = time.time()
        ascan(hz2,start_hz2,end_hz2,3,ctime)
        umv(hz2,start_hz2)
    time_after_ramp = time.time()
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    print('NOW DWELLING')
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    while (time.time() - time_after_ramp) < time_palier:
        start_time = time.time()
        ascan(hz2,start_hz2,end_hz2,3,ctime)
        umv(hz2,start_hz2)
    nanodacpool_induction.ramprate = 25
    nanodacpool_induction.setpoint = 20
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    print('NOW RAMPING DOWN')
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    while nanodacpool_induction.is_ramping():
        start_time = time.time()
        ascan(hz2,start_hz2,end_hz2,3,ctime)
        umv(hz2,start_hz2)


def scan_pdf_fixed(ctime,target_temp,ramprate = 10, time_palier = 1800):
    sheh1.open()
    nanodacpool_induction.ramprate = ramprate
    nanodacpool_induction.setpoint = target_temp
    total_time = (target_temp / ramprate) * 3600 + time_palier
    newdataset('loopscan_temp_%s'%target_temp)
    for i in range(int(total_time/60)+240):
#    for i in range(120):
        ftimescan(ctime,60)

def scan_pdf_ramp(ctime,target_temp,ramprate_up = 10, ramprate_down = 25,time_palier = 1800):
    nanodacpool_induction.ramprate = ramprate_up
    nanodacpool_induction.setpoint = target_temp
    newdataset('loopscan_temp_%s'%target_temp)
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    print('NOW RAMPING UP')
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')    
    while nanodacpool_induction.is_ramping():
        ftimescan(ctime,60)
    time_after_ramp = time.time()
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    print('NOW DWELLING')
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    while (time.time() - time_after_ramp) < time_palier:
        ftimescan(ctime,60)
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    print('NOW RAMPING DOWN')
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    nanodacpool_induction.ramprate = ramprate_down
    nanodacpool_induction.setpoint = 20
    while nanodacpool_induction.is_ramping():
        ftimescan(ctime,60)


def vertical_scan_pdf(ctime,start_hz,end_hz,target_temp,ramprate = 10,time_palier = 1800):
    nanodacpool_induction.ramprate = ramprate
    nanodacpool_induction.setpoint = target_temp
    newdataset('vertical_scan_hz_%s_%s'%(start_hz,end_hz))
    while nanodacpool_induction.is_ramping():
        start_time = time.time()
        ascan(hz,start_hz,end_hz,4,ctime)
        umv(hz,start_hz)
    time_after_ramp = time.time()
    while (time.time() - time_after_ramp) < time_palier:
        start_time = time.time()
        ascan(hz,start_hz,end_hz,4,ctime)
        umv(hz,start_hz)
    nanodacpool_induction.ramprate = 25
    nanodacpool_induction.setpoint = 25
    while nanodacpool_induction.is_ramping():
        start_time = time.time()
        ascan(hz,start_hz,end_hz,4,ctime)
        umv(hz,start_hz)
  
def capillary_scan():
    newsample('rack2')
    sample_names = ['LaB6','AG188','Ti3AlC2','AS04','CI1038_1','CI1021_1','CI1038_5','CI1021_5']
    diffy_positions = [50.95,45.93,41.41,36.89,31.68,26.72,21.63,16.80] 
    sheh1.open()
    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(1*60,1)
        
def pdf_capillary_scan():
    newsample('rack3')
    sample_names = ['AG_187','CI1021_5','CI1038_5','CI1038_1','AS_04','AG_188','LaB6']
    diffy_positions = [6.25,16.61,21.7,31.68,36.95,46.05,51.07] 
    sheh1.open()
    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(20*60,1)
