


def polefigure_deformed(samplename):
    newsample(samplename)
    newdataset('polefigure')
    sheh3.open()
    tfoh1 = config.get('tfoh1')
    tfoh1.set(32,'Be')
    tfoh1.set(32,'Al') 
    finterlaced( diffrz, 45, -1, 90, 0.1)


def polefigure_pristine(samplename):
    newsample(samplename)
    newdataset('polefigure')
    sheh3.open()
    tfoh1 = config.get('tfoh1')
    tfoh1.set(32,'Be')
    tfoh1.set(32,'Al') 
    finterlaced( diffrz, 45, -1, 90, 0.1)
    tfoh1 = config.get('tfoh1')
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al') 
    finterlaced( diffrz, 45, -1, 90, 0.1)


def movie(samplename, minutes):
    umv(diffrz, 0)
    newsample(samplename)
    newdataset('timeseries')
    sheh3.open()
    tfoh1 = config.get('tfoh1')
    tfoh1.set(32,'Be')
    tfoh1.set(32,'Al')
    npictures = minutes * 60 / 0.180164
    ftimescan( 0.1, npictures, scan_mode='TIME' )
    
    
    
    

    
