import numpy as np
import time

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan(*a,**k):
    print(a,k)
    try:
        fscan(*a,**k)
    except: # pretty hopeless but why not ?
        print("Something has gone wrong!!! Retry scans!!")
        elog_print("Retry a scan !!!")
        print("Sleep a minute or two first")
        time.sleep(60)
        pause_for_refill(120)
        fscan(*a,**k)

def myumv(*a,**k):
    print(a,k)
    umv(*a,**k)

def nscopelayerscan_trial(y_start,y_end,y_step,a_range,a_step,a_speed,run_trial=True):
    """
    Estimates time to do a layer scan with dty as outer motor and rot as inner.
    Zigzag with rot
    y_step is actual y step, no factor of 2
    """
    ypos = np.arange( y_start, y_end+(y_step/10), y_step)
    trialpos = np.arange(-2*y_step, 2*y_step+(y_step/10), y_step)
    a_time = a_step / a_speed
    a_rate = a_speed / a_step
    assert (a_step * a_rate)<26 , 'too fast'
    timest = a_range/a_step * a_time + 1
    print('y motion:\n from %0.4f to %0.4f with steps of %0.4f and a total of %d points'
          %(ypos[0],ypos[-1],y_step,len(ypos)))
    print('angular motion:\n from %0.4f to %0.4f with steps of %0.4f and a total of %d points'
          %(0,a_range,a_step,a_range/a_step))
    print(' count time of %0.4f s per point and angular speed of %0.4f (max is 25)'
          %(a_time,a_speed))
    print('estimated time:\n %0.4f s per y step and %0.4f s or %0.2f min total'
          %(timest,timest*len(ypos),timest*len(ypos)/60))
    if run_trial:
        starttime = time.perf_counter()
        ACTIVE_MG.disable("mca")
        if a_time < 0.006:
            eiger.camera.auto_summation="OFF"
        else:
            eiger.camera.auto_summation="ON"
        ACTIVE_MG.enable("eiger:image")
        for my_y in trialpos:
            myumv(dty, my_y)
            pause_for_refill(timest)
            myfscan(rot, 0, a_step, a_range/a_step, a_time, scan_mode='CAMERA')
            myumv(dty, my_y + a_step)
            pause_for_refill(timest)
            myfscan(rot, a_range, -a_step, a_range/a_step, a_time, scan_mode='CAMERA')
        eiger.camera.auto_summation="ON"
        endtime = time.perf_counter()
        print('measured time:\n %0.4f s per y step and %0.2f s or %0.2f min total'
              %((endtime-starttime)/(len(trialpos)*2),
                (endtime-starttime)*(len(ypos)/len(trialpos)),
                (endtime-starttime)*(len(ypos)/len(trialpos))/60))
        return (endtime-starttime)/(len(trialpos)*2)
    



def nscopelayerscan(y_start, y_end, y_step, a_range, a_step=0.05, a_speed=25.):
    """
    layer scan with dty as outer motor and rot as inner.
    Zigzag with rot
    y_step is actual y step
    """
    ypos = np.arange( y_start, y_end+(y_step/10), y_step)
    a_time = a_step / a_speed
    a_rate = a_speed / a_step
    assert (a_step * a_rate)<26 , 'too fast'
    timest = a_range/a_step * a_time + 1
    ACTIVE_MG.disable("mca")
    if a_time < 0.006:
        eiger.camera.auto_summation="OFF"
    else:
        eiger.camera.auto_summation="ON"
    ACTIVE_MG.enable("eiger:image")
    eiger.camera.photon_energy = 65350.8
    for i, my_y in enumerate(ypos):
        myumv(dty, my_y)
        pause_for_refill(timest)
        if i%2 == 0:
            myfscan(rot, 0, a_step, a_range/a_step, a_time, scan_mode='CAMERA')
        else:
            myfscan(rot, a_range, -a_step, a_range/a_step, a_time, scan_mode='CAMERA')
#    eiger.camera.auto_summation="ON"




def s2_scan1():
    newdataset( 'testlayer')
    umv( pz, 62.5 )
    nscopelayerscan( -25, 25, .2, 360 )
    

def s2_BoxVert():
    fscan2d( pz, 5, 0.5, 160, rot, 0, 1, 180, 0.04, mode='ZIGZAG' )
    
def s2_scan2():
    #newdataset( 'layer40')
    umv( pz, 40 )
    nscopelayerscan( -25, 25, .2, 360 )

def s2_scan3():
    newdataset( 'layer30')
    umv( pz, 30 )
    nscopelayerscan( -25, 25, .2, 360 )
    
def s2_vol():
    # pz = top/pt = 10 um
    # hydride pz = 25 um up to 70 um
    zpos = list( np.arange(10,85,1.5) )
    for iz,z in enumerate( zpos ):
        newdataset("vol%03d"%(iz))
        umv(pz, z)
        nscopelayerscan( -.8 , 26, .2, 360 )

    
def s2_vol_restart( num ):
    # pz = top/pt = 10 um
    # hydride pz = 25 um up to 70 um
    zpos = list( np.arange(10,85,1.5) )
    for iz,z in enumerate( zpos ):
        if iz < num:
            continue
        newdataset("vol%03d"%(iz))
        umv(pz, z)
        nscopelayerscan( -.8 , 26, .2, 360 )


# user_script_load("ma4484.py")
# s2_vol_restart( 10 ) # whichever number

# 16h30 Thursday - restart for 26. Layer 25 got 135 scans before the timeout thing


def s2_heatup( ):
    volts = np.arange(0,14.01,0.25)
    for i, v in enumerate(volts):
        hmc.voltage_setpoint = v
        if i%2 == 0:
            myfscan( rot, 0, 0.05, 7200, 0.002, scan_mode='CAMERA')
        else:
            myfscan( rot, 360, -0.05, 7200, 0.002, scan_mode='CAMERA')


def s2_heatup2( ):
    volts = np.linspace(12.25,13.33,15)
    for i, v in enumerate(volts):
        hmc.voltage_setpoint = v
        print("VOLTS SET TO",v)
        if i%2 == 0:
            myfscan( rot, 0, 0.05, 7200, 0.002, scan_mode='CAMERA')
        else:
            myfscan( rot, 360, -0.05, 7200, 0.002, scan_mode='CAMERA')            
    s2_stab()

def s2_heatup3( ):
    volts = np.linspace(13.33, 14.6,15)
    for i, v in enumerate(volts):
        hmc.voltage_setpoint = v
        print("VOLTS SET TO",v)
        if i%2 == 0:
            myfscan( rot, 0, 0.05, 7200, 0.002, scan_mode='CAMERA')
        else:
            myfscan( rot, 360, -0.05, 7200, 0.002, scan_mode='CAMERA')            
    s2_stab()
    
def s2_stab( ):
    i = 0 
    while 1:
        if i%2 == 0:
            myfscan( rot, 0, 0.05, 7200, 0.002, scan_mode='CAMERA')
        else:
            myfscan( rot, 360, -0.05, 7200, 0.002, scan_mode='CAMERA')
        i+=1


def s2_map_Friday(skip=1):
    # pz = top/pt = 10 um
    # hydride pz = 25 um up to 70 um
    zpos = list( np.linspace(15,75,16) )
    for iz,z in enumerate( zpos ):
        if iz < skip:
            continue
        newdataset("vol%03d"%(iz))
        umv(pz, z)
        nscopelayerscan( -.8 , 26, .2, 360 )

def s2_heatup3( ):
    volts = np.linspace( 14.6, 18.8, 25)
    for i, v in enumerate(volts):
        hmc.voltage_setpoint = v
        print("VOLTS SET TO",v)
        if i%2 == 0:
            myfscan( rot, 0, 0.05, 7200, 0.002, scan_mode='CAMERA')
        else:
            myfscan( rot, 360, -0.05, 7200, 0.002, scan_mode='CAMERA')            
    s2_stab()
    

def s2_heatup4( ):
    volts = np.linspace( 18.8, 18.8+2.43, 10)
    for i, v in enumerate(volts):
        hmc.voltage_setpoint = v
        print("VOLTS SET TO",v)
        if i%2 == 0:
            myfscan( rot, 0, 0.05, 7200, 0.002, scan_mode='CAMERA')
        else:
            myfscan( rot, 360, -0.05, 7200, 0.002, scan_mode='CAMERA')            
    s2_stab()

def s2_map_T428(skip=-1):
    # pz = top/pt = 10 um
    # hydride pz = 25 um up to 70 um
    zpos = list( np.linspace(15,75,4) )
    for iz,z in enumerate( zpos ):
        if iz < skip:
            continue
        newdataset("layerT428C_%03d"%(iz))
        umv(pz, z)
        nscopelayerscan( -.8 , 26, .2, 360 )


        
def s2_map_Saturday(skip=-1):
    # pz = top/pt = 12 um
    # hydride pz = 25 um up to 70 um
    zpos = list( np.linspace(17,77,16) )
    umv(pz, 42)
    nscopelayerscan(-26,26,0.2,360)
    newdataset("full")
    for iz,z in enumerate( zpos ):
        if iz < skip:
            continue
        newdataset("layer%03d"%(iz))
        umv(pz, z)
        nscopelayerscan( -.8 , 26, .2, 360 )


def s2_map_Saturday_bis(skip=-1):
    # pz = top/pt = 12 um
    # hydride pz = 25 um up to 70 um
    zpos = list( np.linspace(17,77,16) )
    zpos = 9,13,81,85
#    umv(pz, 42)
 #   nscopelayerscan(-26,26,0.2,360)
  #  newdataset("full")
    for iz,z in enumerate( zpos ):
        if iz < skip:
            continue
        newdataset("layer%03d"%(iz+16))
        umv(pz, z)
        nscopelayerscan( -1 , 25, .2, 360 )        

def s1_map_Sunday(skip=-1):
    # pz = top/pt = 10 um
    zpos = list( range(25, 66) ) 
    for iz,z in enumerate( zpos ):
        if iz < skip:
            continue
        newdataset("pz%03d"%(iz))
        umv(pz, z)
        nscopelayerscan( -1 , 20, .2, 360 ) 

def s1_map_Monday_am(skip=59):
    # pz = top/pt = 10 um
    iz0 = 41
    # first 5 micron top/bottom, then another 5 on each end
    zpos = list( range(66,71) ) + list( range(24,19,-1) )
    zpos +=list( range(71,76) ) + list( range(19,14,-1) )
    zpos +=list( range(76,81) ) + list( range(14,9,-1) )
    # 16 hours ...
    for iz,z in enumerate( zpos ):
        if (iz+iz0) < 60:
            continue
        newdataset("pz%03d"%(iz+iz0))
        umv(pz, z)
        nscopelayerscan( -1 , 20, .2, 360 ) 
        
        
        
def copper_tscan():
    for v in np.arange( 0, 25.01, .25):
        hmc.voltage_setpoint = v
        fscan(rot, -30-15,15,2,5)   
        fscan(rot, -30-15,15,2,5)   
    for v in np.arange( 25, -0.01, .25):
        hmc.voltage_setpoint = v
        fscan(rot, -30-15,15,2,5)   
        fscan(rot, -30-15,15,2,5)   
        
        
import time
def copper_tscan2():
    v = 1
    hmc.voltage_setpoint = v
    r = hmc.counters.hmc_ohm.raw_read
    while v < 20 and r < 151:
        print("v, r =",v,r)
        newdataset("%dmV"%(v*1000))
        fscan(rot,140-45, 1,90,0.1)
        fscan(rot,140+45,-1,90,0.1)
        print("v, r =",v,r)
        v = v + 0.25
        hmc.voltage_setpoint = v
        time.sleep(1)
        r = hmc.counters.hmc_ohm.raw_read
        
def copper_tscan3():
    v = 20.1
    hmc.voltage_setpoint = v
    r = hmc.counters.hmc_ohm.raw_read
    while v > 0:
        print("v, r =",v,r)
        newdataset("%dmV"%(v*1000))
        fscan(rot,140-45, 1,90,0.1)
        fscan(rot,140+45,-1,90,0.1)
        print("v, r =",v,r)
        v = v - 0.25
        hmc.voltage_setpoint = v
        time.sleep(1)
        r = hmc.counters.hmc_ohm.raw_read


import time
def sample_ramp():
    v = 1
    hmc.voltage_setpoint = v
    r = hmc.counters.hmc_ohm.raw_read
    while v < 20 and r < 151:
        print("v, r =",v,r)
        newdataset("%dmV"%(v*1000))
        loopscan(40,0.1,hmc,sleep_time=0.1) 
        print("v, r =",v,r)
        v = v + 1
        hmc.voltage_setpoint = v
        time.sleep(1)
        r = hmc.counters.hmc_ohm.raw_read
        
            
