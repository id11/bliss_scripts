#MA5836

attrz = config.get('attrz')
atty = config.get('atty')

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def tomoin():
    ACTIVE_MG.enable("marana:image") 
    ACTIVE_MG.disable("frelon*")
    sheh3.close()
    umv(d1ty,-200)
    umv(ffdtx1,400)
    umv(d3ty,150)
    umv(nfdtx,100)
    umv(ffdtz1, 150, d3ty, -0.2, d3tz, 0)
    umv(attrz,0,atty,-10)
    umv(s8hg,2,s8vg,2,s7hg,1.5,s7vg,1.5)
    sheh3.open()
    print("Ready to collect tomo data") 

def frelonin():
    ACTIVE_MG.disable("marana*") 
    ACTIVE_MG.disable("frelon16*") 
    ACTIVE_MG.enable("frelon3:image") 
    ACTIVE_MG.enable("frelon3:roi*")
    sheh3.close()
    umv(d1ty,-200)
    umv(nfdtx,100)
    umv(d3ty,150, ffdtz1, 0)
    umv(ffdtx1,265)
    umv(attrz,-3.5,atty,0)
    umv(s8hg,1.5,s8vg,1.5,s7hg,1.2,s7vg,1.2)
    sheh3.open()
    print("Ready to collect diffraction data") 
    
def dctin():
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon3*") 
    ACTIVE_MG.enable("frelon16:image") 
    ACTIVE_MG.enable("frelon16:roi*")
    sheh3.close()
    nanodac3.setpoint = 25
    umv(furnace_z,140)
    umv(ffdtx1,400,d3ty,150, ffdtz1, 150)
    umv(nfdtx,215)
    umv(d1ty,-0.2)
    umv(d3ty,-0.55,d3tz,9.50)
    umv(attrz,0,atty,-10)
    umv(s8hg,1.5,s8vg,1.5,s7hg,1.2,s7vg,1.2)
    sheh3.open()
    print("Ready to collect dct data")     
    #bs d3ty -1.45 d3tz 9.40 diffty 15.08

def launch_pct(dset='RTfinal_tomo',start=0):
    tomoin()
    fulltomo.full_turn_scan(dataset_name=dset,start_pos=start)
    
def launch_bb3DXRD(dset='RTfinal_3DXRD'):
    newdataset(dset)
    frelonin()
    #fscan(diffrz,0,0.1,3600,0.08)
    finterlaced(diffrz,0,0.1,3600,0.08,mode='ZIGZAG')

def launch_dct(dset='RTfinal_DCT'):
    dctin()
    user.dct_marana_dict(dset, dct_pars=dct_pars)

def bb3DXRD_seq(dset):
    zzero = samtz.position
    zpos = [0,0.1,0.2,0.3,0.4]
    for i,zp in enumerate(zpos):
        newdataset('%s_%s'%(dset,i))
        umv(samtz,zzero+zp)
        finterlaced(diffrz,0,0.1,3600,0.08,mode='ZIGZAG')
       
def lunchtime():
    launch_pct(dset='RTfinal_tomo')
    launch_bb3DXRD(dset='RTfinal_tomo')
    launch_dct(dset='RTfinal_tomo')

def afternoon():
    launch_pct()
    launch_bb3DXRD()
 
 
def DCTend():
    ftimescan(0.060000, 41, scan_mode = 'TIME') 
    #ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    umvr(samy,-2)
    print("taking dark images")
    ftimescan(0.060000, 41, scan_mode = 'TIME') 
    #ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()

def lastnight():
    for i in [7,8,9,10,11,12]:
        launch_pct(dset=f'560C_tomo_{i}')
        launch_bb3DXRD(dset=f'560C_3DXRD_{i}')
        sleep(1200)
    nanodac3.setpoint = 25
    umv(furnace_z,140)
    launch_pct(dset='after560C_tomo')
    launch_bb3DXRD(dset='after560C_3DXRD')
    launch_dct(dset='after560C_DCT')
    # this will crash after DCT scan.
    # restart device server
    # run user.DCTend()
    # run user.tomoin()
   
   
def overnight():
    i=1
    while 1:
        launch_pct(dset=f'530C_tomo_{i}')
        launch_bb3DXRD(dset=f'530C_3DXRD_{i}')
        i += 1
        sleep(1200)
  
      
        
        
    

