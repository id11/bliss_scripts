import numpy as np
import time


def volscan_night():
    pz0 = 27.12
    pz_rel = [40.3, 33.5, 22.2, 11.7,28] 
    k = 1
    for zrel in pz_rel:
        k +=1
        newdataset( "layerref_Z%03d"%( k )  )
        pz0 = ref_sample( pz0, zrel )
        umv( pz, pz0 + zrel )
        newdataset( "layer_Z%03d"%( k )  )
        recf2scan_constant_r_step()
        
def volscan_2():
    pz0 = 18.38
    pz_rel = [7.9,8.1,8.3,25.9,26.1,26.3] 
    k = 0
    for zrel in pz_rel:
        k +=1
        newdataset( "layerref_Z%03d"%( k )  )
        pz0 = ref_sample( pz0, zrel )
        umv( pz, pz0 + zrel )
        newdataset( "layer_Z%03d"%( k )  )
        recf2scan_constant_r_step()

def volscan_3():
    pz0 = 18.38
    pz_rel = [8.1,8.3,25.9,26.1,26.3]
    k = 2
    for zrel in pz_rel:
        k +=1
        newdataset( "layerref_Z%03d"%( k )  )
        pz0 = ref_sample( pz0, zrel )
        umv( pz, pz0 + zrel )
        newdataset( "layer_Z%03d"%( k )  )
        recf2scan_constant_r_step()

def volscan_4():
    pz0 = 7.98
    pz_rel = [26.1,18,9.9]
    k = 1
    for zrel in pz_rel:
        k +=1
        newdataset( "layerref_Z%03d"%( k )  )
        pz0 = ref_sample( pz0, zrel )
        umv( pz, pz0 + zrel )
        newdataset( "layer_Z%03d"%( k )  )
        recf2scan_constant_r_step()

def volscan_4restart():
    w = 15   #short dimension of sample
    l = 46  #long dimension of sample
    buf = 3  #buffer around edge of sample
    theta = -22  #angle to put long axis along y 
    ystep = 0.1  #step size of dty
    rstep = 0.2  #step size of rot
    ctim = 0.05  #count time

    #shouldn't need to edit below here
    d = np.sqrt(w**2 + l**2)
    dtys = np.arange( -1.992, (d/2)+buf+(ystep/2), ystep)
    bs = getbetas(dtys, w, l, buf)

    #do the scans
    for i, ypos in enumerate(dtys):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            myfscan(rot, theta-bs[i], rstep, (bs[i]*2)/rstep+1, ctim, scan_mode='CAMERA' ) 
        else:
            myfscan(rot, theta+bs[i], -rstep, (bs[i]*2)/rstep+1, ctim, scan_mode='CAMERA') 
 
def volscan_5():
    pz0 = 27.4
    pz_rel = [8.1,26.1]
    k = 0
    for zrel in pz_rel:
        k +=1
        newdataset( "layerref_Z%03d"%( k )  )
        pz0 = ref_sample( pz0, zrel )
        umv( pz, pz0 + zrel )
        newdataset( "layer_Z%03d"%( k )  )
        recf2scan_constant_r_step() 
           
def ref_sample(pz0, relheight):
    umv(dty, 0, pz, pz0, rot, 63)
    newedge = edge_fit()
    umvr(pz,relheight)
    return newedge

def edge_fit(ctr='mca:Zr_det0'):
    plotinit(ctr)
    plotselect(ctr)
    lastscan = dscan(pz, -1.5, 1.5, 150, .1)
    sleep(1)
    y = lastscan.get_data(ctr)
    x = lastscan.get_data(pz)
    edge = x[np.argmin(np.abs(y-( ( np.mean(y[130:149]) + np.mean(y[1:20]) )/2) ))]
    print(edge)
    umv(pz,edge)
    return edge    
        
def findbeta(dty,w,h,buf):
    dmax = np.sqrt((h/2+buf)**2 + (w/2+buf)**2)
    dr = (np.abs(dty)-(w/2+buf)) / (dmax)
    return np.abs(np.degrees(np.arccos(dr)))
    
def getbetas(dtys,w,h,buf):
    b = np.ones(len(dtys))*90
    for i,dty in enumerate(dtys):
        if np.abs(dty)>((w/2)+buf):
            b[i] = findbeta(dty,w,h,buf)
    return np.nan_to_num(b, nan=90)


def recf2scan_constant_r_step():
    w = 12   #short dimension of sample
    l = 42  #long dimension of sample
    buf = 3  #buffer around edge of sample
    theta = 63-90  #angle to put long axis along y 
    ystep = 0.1  #step size of dty
    rstep = 0.2  #step size of rot
    ctim = 0.05  #count time

    #shouldn't need to edit below here
    d = np.sqrt(w**2 + l**2)
    dtys = np.arange( -((d/2)+buf), (d/2)+buf+(ystep/2), ystep)
    bs = getbetas(dtys, w, l, buf)

    #do the scans
    for i, ypos in enumerate(dtys):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            myfscan(rot, theta-bs[i], rstep, (bs[i]*2)/rstep+1, ctim, scan_mode='CAMERA' ) 
        else:
            myfscan(rot, theta+bs[i], -rstep, (bs[i]*2)/rstep+1, ctim, scan_mode='CAMERA') 


def recf2scan_same_r_npts():
    w = 3   #short dimension of sample
    l = 14  #long dimension of sample
    buf = 0.3  #buffer around edge of sample
    theta = 8  #rot angle at which long axis is along y 
    ystep = 0.2  #step size of dty
    rnpts = 180  #step size of rot
    ctim = 0.1  #count time

    #shouldn't need to edit below here
    d = np.sqrt(w**2 + l**2)
    dtys = np.arange( -((d/2)+buf), (d/2)+buf+(ystep/2), ystep)
    bs = getbetas(dtys, w, buf)

    #do the scans
    for i, ypos in enumerate(dty):
        rstep = bs[i]*2/rnpts
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            myfscan(rot, theta-bs[i], rstep, rnpts, ctim, scan_mode='CAMERA' ) 
        else:
            myfscan(rot, theta+bs[i], -rstep, rnpts, ctim, scan_mode='CAMERA' )

                          
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    #pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico6']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico6']

def myfscan( *args, **kwds):
    for i in range(3):
        try:
            fscan(*args, **kwds)
        except Exception as e:
            elog_print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break

    
def my2d(y0=-1000, ye=1000, ys=20):
    fscan.pars.latency_time=0
    for i, ypos in enumerate(np.arange(y0,ye,ys)):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            myfscan(rot, -1, 0.5, 182/0.5, 0.05, scan_mode='CAMERA' ) 
        else:
            myfscan(rot, 181, -0.5, 182/0.5, 0.05, scan_mode='CAMERA' )

