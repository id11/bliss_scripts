

### To load in nscope session:
### user_script_load('ch6365')



import numpy as np, time
user_script_load('optics')

# def check_cpm18(pos=6.4158):
def check_cpm18(pos=10.09):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()

def myfscan2d(*a,**k):
    try:
        fscan2d(*a,**k)
    except:
        print("Something has gone wrong!!! Retry scans!!")
        elog_print("Retry a scan !!!")
        print("Sleep a minute or two first")
        time.sleep(60)
        pause_for_refill(120)
        fscan2d(*a,**k) 

def z_scan():
    for z in [50,48,20,18,16]:
        umv(pz,z)
        half1('Z_%d'%(z))

def half1(datasetname, ymax=231 , ystep=0.5, ymin=-231, expos=0.002):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            myfscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.05, 181/0.05, expos )
        else:
            myfscan2d( dty, ypos[i], ystep, 1, rot, 181,  -0.05, 181/0.05, expos )
    
  
