import time
#user_script_load("optics.py")

# def check_cpm18(pos=6.4158):
def check_cpm18(pos=6.439):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03:
        user.cpm18_goto(pos)

def check_cpm18(pos=None):
    print("FIXME!!! - cpm18")
    pass


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()

def myfscan( *args, **kwds):
    for i in range(3):
        try:
            fscan(*args, **kwds)
        except Exception as e:
            print("ERROR!!! Retrying a scan !!! "+str(e))
            elog_print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break

def myfscan2d( *args, **kwds):
    for i in range(3):
        try:
            fscan2d(*args, **kwds)
        except Exception as e:
            print("ERROR!!! Retrying a scan !!! "+str(e))
            elog_print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break

import numpy as np        
        
print("Load this script at least twice please!") # wtf?

def my2d(yrange, ys=0.15):
    # do -91 -> 91 degrees because of glitch
    # use fscan2d to see the glitch (or not)
    # does a symmetric range around zero with 180 degrees
    # this is for an mca, so 5 ms 25deg/s
    fscan.pars.latency_time=0
    speed = 25.0
    exptime = 0.005
    astep = exptime * speed
    nframe = np.round(182 / astep).astype( int )
    ny = np.ceil( abs( yrange / ys ) ).astype( int )
    ypositions = np.linspace( -ny*ys, ny*ys, 2*ny+1 )
    print(ypositions)
    for i, ypos in enumerate(ypositions):
        pause_for_refill(60)
        # Does 1 step but uses f2scan2d : shows glitching
        if i%2 == 0:
            myfscan2d(dty,  ypos, ys, 1,
                rot, -91, astep, nframe, exptime, scan_mode='CAMERA' )
        else:
            myfscan2d( dty,  ypos, ys, 1,
                rot, 91, -astep, nframe, exptime, scan_mode='CAMERA' )
    



def bigGradientC():
    umv(fly, 50)
    newdataset('slice1')
    my2d( -50.625, 50.626 )

