
import numpy as np, os, time

def explog( s ):
    with open( "/data/visitor/hc4568/id11/S129/drift.log","a") as f:
        ans = "%s %s\n"%(time.ctime(),s)
        print(ans)
        f.write(ans)
        f.flush()



def check_drift():
    f = open("hc4568_drift.dat","w")
    gty.motion_hooks[0].set('pre_move')
    try:
        while 1:
            umv( rot, 90 )
            dscan( px, -15,-5,100,.05)
            f.write("x %f rot %f\n"%(cen(eiger.counters.roi1_sum), rot.position))
            dscan( pz, -15,-5,100,.05)
            f.write("z %f rot %f\n"%(cen(eiger.counters.roi1_sum), rot.position))
            umv( rot, -90 )
            dscan( px, -15,-5,100,.05)
            f.write("x %f rot %f\n"%(cen(eiger.counters.roi1_sum), rot.position))
            dscan( pz, -15,-5,100,.05)
            f.write("z %f rot %f\n"%(cen(eiger.counters.roi1_sum), rot.position))
    finally:
        f.flush()
        f.close()
        



# def check_cpm18(pos=6.4158):
def check_cpm18(pos=6.425):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03:
        user.cpm18_goto(pos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()
        
def measure_layer( yrange, ystep, rspeed=25., exptime=0.002 ):     ####-90 to 90 scan range, via zero, to avoid worse glitches
    eiger.camera.photon_energy = 44000.
    ACTIVE_MG.disable("mca")
    ACTIVE_MG.enable("eiger")
    rstep = rspeed * exptime
    if exptime < 0.003:
        # 16 bit data from the camera
        eiger.camera.auto_summation='OFF'   
    else:
        # 32 bit data
        eiger.camera.auto_summation='ON'
    fscan2d( dty, -yrange, ystep, 2 * yrange / ystep + 1,
             rot, -90, rstep, np.ceil(180 / rstep) + 1, exptime,
             scan_mode="CAMERA")
    
def measure_layer_withbeamcheck( yrange, ystep, rspeed=25., exptime=0.002 ):   ####-90 to 90 scan range, via zero, to avoid worse glitches
    eiger.camera.photon_energy = 44000.
    ACTIVE_MG.disable("mca")
    ACTIVE_MG.enable("eiger")
    rstep = rspeed * exptime
    if exptime < 0.003:
        # 16 bit data from the camera
        eiger.camera.auto_summation='OFF'   
    else:
        # 32 bit data
        eiger.camera.auto_summation='ON'
    pause_for_refill(1)
    fscan2d( dty, -yrange, ystep, 2 * yrange / ystep + 1,
             rot, -90, rstep, np.ceil(180 / rstep) + 1, exptime,
             scan_mode="CAMERA")             
             
def pymca():
    os.system("pymca %s &"%(SCAN_SAVING.filename))

def air_on():
    gty.motion_hooks[0].set('pre_move')

def air_off():
    gty.motion_hooks[0].set('post_move')    
        
def rotfix():
    """ sort out the rotation zero position """
    if abs(rot.position)>360:
        diff = rot.dial % 360
        if diff > 180:
            umvr(rot, diff-360-2)
        else:
            umvr(rot, diff-2)
        rot.home()

def ref_pz_height( pzref ):
    """ Try to find the top of the sample """
    ACTIVE_MG.enable("mca")
    umv(rot, 0, dty, 0)
    umv( pz, pzref )
    pause_for_refill(40)
    sc = dscan( pz, -1, 1, 40, .1 )
    pzref = process_ref(sc)
    return pzref 

def process_ref(sc):
    """ fit a reference scan to find the top """
    z = sc.get_data('pz')
    y2= sc.get_data('mca:Pt_det0')
    y1= sc.get_data('mca:Mo_det0')
    angle = np.arctan2( y1 / y1.max() , y2 / y2.max() )
    ideal = np.arctan2(1, 1)
    i = abs(angle-ideal).argmin()
    print(z[i])
    zref = np.interp( ideal, angle[i-2:i+2], z[i-2:i+2] )
    print(zref, ideal)
    f = flint()
    p = f.get_plot("Plot1D", "hc4568", "hc4568_plot")
    p.clear_items()
    p.add_curve( z, angle, legend='tan', color='blue')
    p.add_curve([zref, zref], [ideal - 1, ideal + 1], legend='CEN', color='black')
    p.add_curve(z, y1/y1.max(), legend='Mo', color='green')
    p.add_curve(z, y2/y2.max(), legend='Pt', color='red')
    #    p.export_to_logbook()
    ans ="zref %f"%(zref)
    elog_print(ans)
    explog( ans )
    return zref

def ref_pz_height_indtens( pzref,alignangle ):
    """ Try to find the top of the sample """
    umv(rot,alignangle+90,dty,0)
    umv( pz, pzref )
    ACTIVE_MG.enable("mca")
    pause_for_refill(40)
    sc = dscan( pz, -5, 5, 80, .1 )
    pzref = process_ref_indtens(sc)
    return pzref 

def process_ref_indtens(sc):
    """ fit a reference scan to find the top """
    y1= sc.get_data('mca:Pt_det0')
    z = sc.get_data('pz')
    zref  = cen(mca.counters.Pt_det0, pz, sc)
    print(zref)
    #f = flint()
    #p = f.get_plot("Plot1D", "hc4568", "hc4568_plot")
    #p.clear_items()
    #p.add_curve([cenz, cenz], [0.1*y1.max(), 1.1*y1.max()], legend='CEN', color='black')
    #p.add_curve(z, y1, legend='Pt', color='red')
    #    p.export_to_logbook()
    ans ="zref %f"%(zref)
    elog_print(ans)
    explog( ans )
    return zref


def ref_pz_height_indtens_alt( pzref,alignangle ): ####uses process_ref_indtens above, but at laternative 90 deg angle to 'ref_pz_height_indtens' above: think where grippers align!
    """ Try to find the top of the sample """
    umv(rot,alignangle,dty,0)
    umv( pz, pzref )
    ACTIVE_MG.enable("mca")
    pause_for_refill(40)
    sc = dscan( pz, -5, 5, 80, .1 )
    pzref = process_ref_indtens(sc)
    return pzref 


def ref_pz_height_knife( pzref ): 
    """ Try to find the top of the sample """
    rotfix() #added only for Mgpoly
    umv(rot,180,dty,0)
    umv( pz, pzref )
    ACTIVE_MG.enable("mca")
    pause_for_refill(40)
    sc = dscan( pz, -1, 1, 40, .2 )
    pzref = process_ref_knife(sc)
    return pzref

def process_ref_knife(sc):    #####this was previously used to align magnesium ex sity Tbars, using Pt blob on top, so consider the high/low direction of the scan data!!!!
    """ fit a reference scan to find the top """
    z = sc.get_data('pz')
    y = sc.get_data('mca:Pt_det0')
    
    f = flint()
    p = f.get_plot("plot1d", "hc4082", "hc4082_plot")
    
    p.plot( x=z, data=y)
    imax = y.argmax()
    ycut = y[imax:]   # assumes scan always goes the right way
    zcut = z[imax:]
    ideal = (ycut.max() + ycut.min())/2  # halfway up
    
    i = abs(ycut-ideal).argmin()
    print(zcut[i])
    lo = max( i-2, 0 )
    zref = np.interp( ideal, ycut[lo:i+2][::-1], zcut[lo:i+2][::-1] )
    print(zref, ideal)
    p.plot({'x': [zref, zref], 'COR': [ideal/2, ideal*3/2]}, linestyle='--')
    p.plot({'x': zcut, 'ycut': ycut })    
    return zref

def ref_pz_height_knife_insitu_Ti( pzref,alignangle ):
    """ Try to find the top of the sample """
    umv(rot,alignangle,dty,0)
    umv( pz, pzref )
    ACTIVE_MG.enable("mca")
    pause_for_refill(40)
    sc = dscan( pz, -1, 1, 40, .2 )
    pzref = process_ref_knife_insitu_Ti(sc)
    return pzref


def process_ref_knife_insitu_Ti(sc): 
    """ fit a reference scan to find the top """
    z = sc.get_data('pz')
    y = sc.get_data('mca:Ti_det0')
        
    imax = y.argmax()
    imin = y.argmin()
    ycut = y[imin:]   # assumes scan always goes the right way
    zcut = z[imin:]
    ideal = (ycut.max() + ycut.min())/2  # halfway up
    
    i = abs(ycut-ideal).argmin()
    print(zcut[i])
    lo = max( i-2, 0 )
    zref = np.interp( ideal, ycut[lo:i+2], zcut[lo:i+2] )
    print(zref, ideal)
    return zref

def ref_pz_height_knife_insitu_Zr( pzref,alignangle ):
    """ Try to find the top of the sample """
    umv(rot,alignangle,dty,0)
    umv( pz, pzref )
    ACTIVE_MG.enable("mca")
    pause_for_refill(40)
    sc = dscan( pz, -1, 1, 100, .2 )
    pzref = process_ref_knife_insitu_Zr(sc)
    return pzref


def process_ref_knife_insitu_Zr(sc):   ##30% thresholding ##this is like the 10% thresholding done previous; it's not very nice as it's dependent on the spacing of dscan points in the function that calls it
    """ fit a reference scan to find the top """
    z1 = sc.get_data('pz')
    y1 = sc.get_data('mca:Zr_det0')
    y1calc=y1
    y1calc=[0 if x<0.5*np.nanmax(y1calc) else x for x in y1calc]
    y1calc=np.divide(y1calc,y1calc)   ###forced division by zero: messy!
    z1filtered=np.multiply(y1calc,z1)

    zref = np.nanmin(z1filtered)
    ans ="zref %f"%(zref)
    elog_print(ans)
    explog( ans )
    print(zref)
    return zref


#### new dty using Mo flurescence    at positions -90,0,90 only     for an alternative choice of 3 angles, using 'rotflex' or 'rotflex_cen' code below
def ref_dty_rot90( ):
    pause_for_refill(120)
    ACTIVE_MG.enable('mca')
    umv( rot, -90 )
    fscan( dty, -10, .05, 400, .05, scan_mode='TIME')
    s1 = fscan.scan
    # px*sin(100)+py*cos(100)
    umv( rot, 0 )
    fscan( dty, -10, .05, 400, .05, scan_mode='TIME')
    s2 = fscan.scan
    # px*sin(190)+py*cos(190)
    umv( rot, 90 )
    fscan( dty, -10, .05, 400, .05, scan_mode='TIME')
    s3 = fscan.scan
#    return s1,s2,s3
    fit_dty_rot90( s1, s2, s3, domove=True)
    print("Resetting dty to zero!!!")
    ans = "ref_dty reset dty from %f to %f"%(dty.position,0)
    elog_print(ans)
    explog(ans)
    dty.position=0


def fit_dty_rot90( scan1, scan2, scan3, domove=False):
    cen1  = cen( mca.counters.Mo_det0, dty, scan1)
    cen2  = cen( mca.counters.Mo_det0, dty, scan2)
    cen3  = cen( mca.counters.Mo_det0, dty, scan3)
    dtycen = (cen1 + cen3)/2
    err1 = (cen3 - cen1)/2
    err2 = cen2 - dtycen
    print(cen1,cen2,cen3,dtycen,err1,err2)
    if domove:
        umv( dty, dtycen )
        umvr( px, err1 )
        umvr( py, err2 )
    ans = "fit_dty_rot90: dtycen %f px %f py %f"%( dtycen, px.position, py.position )
    elog_print(ans)
    explog(ans)


#### new position dty with flexible choice of angles; but check processing function is flexible to all choices of angles ! currently only 0,90,180 related       using Pt fluoresence
def ref_dty_rotflex(angle1,angle2,angle3 ):
    pause_for_refill(120)
    ACTIVE_MG.enable('mca')
    umv( rot, angle1 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s1 = fscan.scan
    # px*sin(100)+py*cos(100)
    umv( rot, angle2 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s2 = fscan.scan
    # px*sin(190)+py*cos(190)
    umv( rot, angle3 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s3 = fscan.scan
#    return s1,s2,s3
    fit_dty_rotflex( s1, s2, s3, angle1,angle2,angle3,domove=True)
    print("Resetting dty to zero!!!")
    ans = "ref_dty reset dty from %f to %f"%(dty.position,0)
    elog_print(ans)
    explog(ans)
    dty.position=0


def fit_dty_rotflex( scan1, scan2, scan3,angle1,angle2,angle3, domove=False):  ###only works if angles are 0,90,180deg related!
    ###uses 10% of max intensity rule for choice of the two dty positions to centre on
    
    z1 = scan1.get_data('dty')
    y1 = scan1.get_data('mca:Pt_det0')
    y1calc=y1
    y1calc=[0 if x<0.3*np.nanmax(y1calc) else x for x in y1calc]
    y1calc=np.divide(y1calc,y1calc)   ###forced division by zero: messy!
    z1filtered=np.multiply(y1calc,z1)
    #imax=z.argmax()
    #imin=z.argmin()

    z2 = scan2.get_data('dty')
    y2 = scan2.get_data('mca:Pt_det0')
    y2calc=y2
    y2calc=[0 if x<0.*np.nanmax(y2calc) else x for x in y2calc]
    y2calc=np.divide(y2calc,y2calc)
    z2filtered=np.multiply(y2calc,z2)

    z3 = scan3.get_data('dty')
    y3 = scan3.get_data('mca:Pt_det0')
    y3calc=y3
    y3calc=[0 if x<0.1*np.nanmax(y3calc) else x for x in y3calc]
    y3calc=np.divide(y3calc,y3calc)
    z3filtered=np.multiply(y3calc,z3)
    
    cen1= (np.nanmax(z1filtered)+np.nanmin(z1filtered))/2
    cen2= (np.nanmax(z2filtered)+np.nanmin(z2filtered))/2
    cen3= (np.nanmax(z3filtered)+np.nanmin(z3filtered))/2
    

    #cen1  = cen( mca.counters.Mo_det0, dty, scan1)
    #cen2  = cen( mca.counters.Mo_det0, dty, scan2)    #cen3  = cen( mca.counters.Mo_det0, dty, scan3)
    dtycen = (cen1 + cen3)/2
    err1 = (cen1 - cen3)/2
    err2 = cen2 - dtycen
    print(cen1,cen2,cen3,dtycen,err1,err2)
    if domove:
        umv( dty, dtycen )
        umvr( px, err1*np.sin(np.radians(angle1))+ err2*np.sin(np.radians(angle2)))
        umvr( py, err1*np.cos(np.radians(angle1))+ err2*np.cos(np.radians(angle2)))
    ans = "fit_dty_rot90: dtycen %f px %f py %f"%( dtycen, px.position, py.position )
    elog_print(ans)
    explog(ans)

def ref_dty_rotflex_Zr(angle1,angle2,angle3 ):
    pause_for_refill(120)
    ACTIVE_MG.enable('mca')
    umv( rot, angle1 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s1 = fscan.scan
    # px*sin(100)+py*cos(100)
    umv( rot, angle2 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s2 = fscan.scan
    # px*sin(190)+py*cos(190)
    umv( rot, angle3 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s3 = fscan.scan
#    return s1,s2,s3
    fit_dty_rotflex_Zr( s1, s2, s3, angle1,angle2,angle3,domove=True)
    print("Resetting dty to zero!!!")
    ans = "ref_dty reset dty from %f to %f"%(dty.position,0)
    elog_print(ans)
    explog(ans)
    dty.position=0


def fit_dty_rotflex_Zr( scan1, scan2, scan3,angle1,angle2,angle3, domove=False):  ###only works if angles are 0,90,180deg related!
    ###uses 20% of max intensity rule for choice of the two dty positions to centre on
    
    z1 = scan1.get_data('dty')
    y1 = scan1.get_data('mca:Zr_det0')
    y1calc=y1
    y1calc=[0 if x<0.2*np.nanmax(y1calc) else x for x in y1calc]
    y1calc=np.divide(y1calc,y1calc)   ###forced division by zero: messy!
    z1filtered=np.multiply(y1calc,z1)
    #imax=z.argmax()
    #imin=z.argmin()

    z2 = scan2.get_data('dty')
    y2 = scan2.get_data('mca:Zr_det0')
    y2calc=y2
    y2calc=[0 if x<0.2*np.nanmax(y2calc) else x for x in y2calc]
    y2calc=np.divide(y2calc,y2calc)
    z2filtered=np.multiply(y2calc,z2)

    z3 = scan3.get_data('dty')
    y3 = scan3.get_data('mca:Zr_det0')
    y3calc=y3
    y3calc=[0 if x<0.2*np.nanmax(y3calc) else x for x in y3calc]
    y3calc=np.divide(y3calc,y3calc)
    z3filtered=np.multiply(y3calc,z3)
    
    cen1= (np.nanmax(z1filtered)+np.nanmin(z1filtered))/2
    cen2= (np.nanmax(z2filtered)+np.nanmin(z2filtered))/2
    cen3= (np.nanmax(z3filtered)+np.nanmin(z3filtered))/2
    

    #cen1  = cen( mca.counters.Mo_det0, dty, scan1)
    #cen2  = cen( mca.counters.Mo_det0, dty, scan2)    #cen3  = cen( mca.counters.Mo_det0, dty, scan3)
    dtycen = (cen1 + cen3)/2
    err1 = (cen1 - cen3)/2
    err2 = cen2 - dtycen
    print(cen1,cen2,cen3,dtycen,err1,err2)
    if domove:
        umv( dty, dtycen )
        umvr( px, err1*np.sin(np.radians(angle1))+ err2*np.sin(np.radians(angle2)))
        umvr( py, err1*np.cos(np.radians(angle1))+ err2*np.cos(np.radians(angle2)))
    ans = "fit_dty_rot90: dtycen %f px %f py %f"%( dtycen, px.position, py.position )
    elog_print(ans)
    explog(ans)



    
###alternative tensile with classic centering with Pt fluorescence
def ref_dty_rotflex_cen(angle1,angle2,angle3 ):
    pause_for_refill(120)
    ACTIVE_MG.enable('mca')
    umv( rot, angle1 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s1 = fscan.scan
    # px*sin(100)+py*cos(100)
    umv( rot, angle2 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s2 = fscan.scan
    # px*sin(190)+py*cos(190)
    umv( rot, angle3 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s3 = fscan.scan
#    return s1,s2,s3
    fit_dty_rotflex_cen( s1, s2, s3, angle1,angle2,angle3,domove=True)
    print("Resetting dty to zero!!!")
    ans = "ref_dty reset dty from %f to %f"%(dty.position,0)
    elog_print(ans)
    explog(ans)
    dty.position=0


def fit_dty_rotflex_cen( scan1, scan2, scan3,angle1,angle2,angle3, domove=False):

    cen1  = cen( mca.counters.Pt_det0, dty, scan1)
    cen2  = cen( mca.counters.Pt_det0, dty, scan2)
    cen3  = cen( mca.counters.Pt_det0, dty, scan3)

    dtycen = (cen1 + cen3)/2
    err1 = (cen1 - cen3)/2
    err2 = cen2 - dtycen
    print(cen1,cen2,cen3,dtycen,err1,err2)
    if domove:
        umv( dty, dtycen )
        umvr( px, err1*np.sin(np.radians(angle1))+ err2*np.sin(np.radians(angle2)))
        umvr( py, err1*np.cos(np.radians(angle1))+ err2*np.cos(np.radians(angle2)))
    ans = "fit_dty_rot90: dtycen %f px %f py %f"%( dtycen, px.position, py.position )
    elog_print(ans)
    explog(ans)

###alternative tensile with classic centering with Ti fluorescence
def ref_dty_rotflex_cen_Ti(angle1,angle2,angle3 ):
    pause_for_refill(120)
    ACTIVE_MG.enable('mca')
    umv( rot, angle1 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s1 = fscan.scan
    # px*sin(100)+py*cos(100)
    umv( rot, angle2 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s2 = fscan.scan
    # px*sin(190)+py*cos(190)
    umv( rot, angle3 )
    fscan( dty, -15, .075, 400, .05, scan_mode='TIME')
    s3 = fscan.scan
#    return s1,s2,s3
    fit_dty_rotflex_cen_Ti( s1, s2, s3, angle1,angle2,angle3,domove=True)
    print("Resetting dty to zero!!!")
    ans = "ref_dty reset dty from %f to %f"%(dty.position,0)
    elog_print(ans)
    explog(ans)
    dty.position=0


def fit_dty_rotflex_cen_Ti( scan1, scan2, scan3,angle1,angle2,angle3, domove=False):

    cen1  = cen( mca.counters.Ti_det0, dty, scan1)
    cen2  = cen( mca.counters.Ti_det0, dty, scan2)
    cen3  = cen( mca.counters.Ti_det0, dty, scan3)

    dtycen = (cen1 + cen3)/2
    err1 = (cen1 - cen3)/2
    err2 = cen2 - dtycen
    print(cen1,cen2,cen3,dtycen,err1,err2)
    if domove:
        umv( dty, dtycen )
        umvr( px, err1*np.sin(np.radians(angle1))+ err2*np.sin(np.radians(angle2)))
        umvr( py, err1*np.cos(np.radians(angle1))+ err2*np.cos(np.radians(angle2)))
    ans = "fit_dty_rot90: dtycen %f px %f py %f"%( dtycen, px.position, py.position )
    elog_print(ans)
    explog(ans)  

###alternative tensile with classic centering with Zr fluorescence
def ref_dty_rotflex_cen_Al(angle1,angle2,angle3 ):
    pause_for_refill(120)
    ACTIVE_MG.enable('mca')
    umv( rot, angle1 )
    fscan( dty, -15, .075, 400, .2, scan_mode='TIME')
    s1 = fscan.scan
    # px*sin(100)+py*cos(100)
    umv( rot, angle2 )
    fscan( dty, -15, .075, 400, .2, scan_mode='TIME')
    s2 = fscan.scan
    # px*sin(190)+py*cos(190)
    umv( rot, angle3 )
    fscan( dty, -15, .075, 400, .2, scan_mode='TIME')
    s3 = fscan.scan
#    return s1,s2,s3
    fit_dty_rotflex_cen_Al( s1, s2, s3, angle1,angle2,angle3,domove=True)
    print("Resetting dty to zero!!!")
    ans = "ref_dty reset dty from %f to %f"%(dty.position,0)
    elog_print(ans)
    explog(ans)
    dty.position=0


def fit_dty_rotflex_cen_Al( scan1, scan2, scan3,angle1,angle2,angle3, domove=False):

    cen1  = cen( mca.counters.Al_det0, dty, scan1)
    cen2  = cen( mca.counters.Al_det0, dty, scan2)
    cen3  = cen( mca.counters.Al_det0, dty, scan3)

    dtycen = (cen1 + cen3)/2
    err1 = (cen1 - cen3)/2
    err2 = cen2 - dtycen
    print(cen1,cen2,cen3,dtycen,err1,err2)
    if domove:
        umv( dty, dtycen )
        umvr( px, err1*np.sin(np.radians(angle1))+ err2*np.sin(np.radians(angle2)))
        umvr( py, err1*np.cos(np.radians(angle1))+ err2*np.cos(np.radians(angle2)))
    ans = "fit_dty_rot90: dtycen %f px %f py %f"%( dtycen, px.position, py.position )
    elog_print(ans)
    explog(ans)  

##################ex situ compressed SS bixstal pillar from March 2021 session, measured in Nov 2021###############################################################

def volscan_S129(klast=0):
    pz0 = 31.5005

    for k, zpos in enumerate( np.arange(0,12,0.15) ):
        
        if k < klast: # skip already done points
            continue
        
        if ( k % 2 ) == 0 or k == klast:
            # timing about 51 s
            newdataset("r0_zref")
            pz0 = ref_pz_height( pz0 )
            
        if ( k % 10 ) == 0 or k == klast:
            umv( pz , pz0+1 )
            # timing about 82
            newdataset("r0_yref")
            ref_dty_rot90( )
        
        umv( pz, pz0 + zpos )
        newdataset( "r0_Z%03d"%( k )  )
        explog("LAYER r0_Z%03d %f %f %f %f %f"%(k, pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer( 3.9, 0.15 )
        
    
def volscan_S129_cont1(klast):
    pz0 = 31.2500

    for k, zpos in enumerate( np.arange(0,12,0.15) ):
        
        if k < klast: # skip already done points
            continue
        
        if ( k % 2 ) == 0 or k == klast:
            # timing about 51 s
            newdataset("r1_zref")
            pz0 = ref_pz_height( pz0 )
            
        if ( k % 10 ) == 0 or k == klast:
            umv( pz , pz0+1 )
            # timing about 82
            newdataset("r1_yref")
            ref_dty_rot90( )
        
        umv( pz, pz0 + zpos )
        newdataset( "r1_Z%03d"%( k )  )
        explog("LAYER r1_Z%03d %f %f %f %f %f"%(k, pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer( 3.9, 0.15 )


#########In situ indenter_Mg samples#################################################

def volscan_MgP04_1pc(klast=1):
    pz0 = 45.9836   ###initial guess of middle of Pt band or dot in z dir
    alignangle=30   ###modify for exact position where grippers align
    thicknessPt=3.06   ###full thickness (in z dir) of Pt band on pillars, from SEM image; make =0 for tensile
    offsetPty=0   ###make =0 for pillars...for tensile, small number, from SEM image
    offsetPtx=0   ###make =0 for pillars... for tensile, big number, from SEM image
    offsetPtz=0   ###make =0 for pillars... for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=1   ###for when you don't want to start at bottom of gauge
    for k, zpos in enumerate( np.arange(0,8.11,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
                 
        umv( pz, pz0 - offsetPtz - extraoffsetz - thicknessPt/2 - zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.6, 0.15 )

def MgP04_undef_quicklayers(klast=1):
    pz0 = 45.804
    alignangle=30   ###modify for exact position where grippers align
    thicknessPt=3.06   ###full thickness (in z dir) of Pt band on pillars, from SEM image; make =0 for tensile
    extraoffsetz=5   ###for when you don't want to start at bottom of gauge
    
    for k, zpos in enumerate( np.arange(0,0.7,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens( pz0,alignangle )
            
         
        umv( pz, pz0 - extraoffsetz - thicknessPt/2 - zpos )
        newdataset( "Z%03d"%( (k+1) )  )
        explog("LAYER r0_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.75, 0.15 )

def MgP03_undef_quicklayers(klast=1):
    pz0 = 54.803
    alignangle=30   ###modify for exact position where grippers align
    thicknessPt=2.02   ###full thickness (in z dir) of Pt band on pillars, from SEM image; make =0 for tensile
    extraoffsetz=5   ###for when you don't want to start at bottom of gauge
    
    for k, zpos in enumerate( np.arange(0,0.7,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens( pz0,alignangle )
            
         
        umv( pz, pz0 - extraoffsetz - thicknessPt/2 - zpos )
        newdataset( "Z%03d"%( (k+1) )  )
        explog("LAYER r0_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.45, 0.15 )



def volscan_MgP03_1pc(klast=1):
    pz0 = 44.11   ###initial guess of middle of Pt band or dot in z dir
    alignangle=30   ###modify for exact position where grippers align
    thicknessPt=2.02   ###full thickness (in z dir) of Pt band on pillars, from SEM image; make =0 for tensile
    offsetPty=0   ###make =0 for pillars...for tensile, small number, from SEM image
    offsetPtx=0   ###make =0 for pillars... for tensile, big number, from SEM image
    offsetPtz=0   ###make =0 for pillars... for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=1   ###for when you don't want to start at bottom of gauge
    for k, zpos in enumerate( np.arange(0,8.11,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
                 
        umv( pz, pz0 - offsetPtz - extraoffsetz - thicknessPt/2 - zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.6, 0.15) 




def volscan_MgP03_5pc(klast=1):    
    pz0 = 46.345   ###initial guess of middle of Pt band or dot in z dir
    alignangle=30   ###modify for exact position where grippers align
    thicknessPt=2.02   ###full thickness (in z dir) of Pt band on pillars, from SEM image; make =0 for tensile
    offsetPty=0   ###make =0 for pillars...for tensile, small number, from SEM image
    offsetPtx=0   ###make =0 for pillars... for tensile, big number, from SEM image
    offsetPtz=0   ###make =0 for pillars... for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=1   ###for when you don't want to start at bottom of gauge
    for k, zpos in enumerate( np.arange(0,8.11,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
                 
        umv( pz, pz0 - offsetPtz - extraoffsetz - thicknessPt/2 - zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.9, 0.15) 



def volscan_Tb02_0pc(klast=1):    ####Magnesium
    pz0 = 54.40  ###initial guess of middle of Pt band or dot in z dir
    alignangle=27.5   ###modify for exact position where grippers align
    thicknessPt=0   ###full thickness (in z dir) of Pt band on pillars, from SEM image; make =0 for tensile
    offsetPtx=0.31   ###make =0 for pillars...for tensile, small number, from SEM image
    offsetPty=9.09   ###make =0 for pillars... for tensile, big number, from SEM image
    offsetPtz=16.97   ###make =0 for pillars... for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=3.5   ###for when you don't want to start at bottom of gauge
    for k, zpos in enumerate( np.arange(0,0.1,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
                 
        umv( pz, pz0 - offsetPtz - 1 - thicknessPt/2 - zpos )
        newdataset( "bottom_r2_Z%03d"%((k+1))  )
        explog("LAYER bottom_r2_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.75, 0.15 )

    for k, zpos in enumerate( np.arange(0,0.7,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
        
                 
        umv( pz, pz0 - offsetPtz - extraoffsetz - thicknessPt/2 - zpos )
        newdataset( "middle_r2_Z%03d"%((k+1))  )
        explog("LAYER middle_r2_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.75, 0.15 )

    for k, zpos in enumerate( np.arange(0,0.1,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
        
        umv( pz, pz0 - offsetPtz - 7 - thicknessPt/2 - zpos )
        newdataset( "top_r2_Z%03d"%((k+1))  )
        explog("LAYER top_r2_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.75, 0.15 )



def volscan_Tb01_0pc(klast=1): ####Magnesium
    pz0 = 53.70  ###initial guess of middle of Pt band or dot in z dir
    alignangle=27.5   ###modify for exact position where grippers align
    thicknessPt=0   ###full thickness (in z dir) of Pt band on pillars, from SEM image; make =0 for tensile
    offsetPtx=0.28   ###make =0 for pillars...for tensile, small number, from SEM image
    offsetPty=7.35   ###make =0 for pillars... for tensile, big number, from SEM image
    offsetPtz=16.55   ###make =0 for pillars... for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=3.5   ###for when you don't want to start at bottom of gauge
    for k, zpos in enumerate( np.arange(0,0.1,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
                 
        umv( pz, pz0 - offsetPtz - 1 - thicknessPt/2 - zpos )
        newdataset( "bottom_r0_Z%03d"%((k+1))  )
        explog("LAYER bottom_r0_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.3, 0.15 )

    for k, zpos in enumerate( np.arange(0,0.7,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
        
                 
        umv( pz, pz0 - offsetPtz - extraoffsetz - thicknessPt/2 - zpos )
        newdataset( "middle_r0_Z%03d"%((k+1))  )
        explog("LAYER middle_r0_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.3, 0.15 )

    for k, zpos in enumerate( np.arange(0,0.1,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
        
        umv( pz, pz0 - offsetPtz - 7 - thicknessPt/2 - zpos )
        newdataset( "top_r0_Z%03d"%((k+1))  )
        explog("LAYER top_r0_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.3, 0.15 )



def volscan_Tb01_1pc(klast=1):        ####Magnesium
    pz0 = 53.167  ###initial guess of middle of Pt band or dot in z dir
    alignangle=27.5   ###modify for exact position where grippers align
    thicknessPt=0   ###full thickness (in z dir) of Pt band on pillars, from SEM image; make =0 for tensile
    offsetPtx=0.28   ###make =0 for pillars...for tensile, small number, from SEM image
    offsetPty=7.35   ###make =0 for pillars... for tensile, big number, from SEM image
    offsetPtz=16.55   ###make =0 for pillars... for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=0   ###for when you don't want to start at bottom of gauge
    for k, zpos in enumerate( np.arange(0,7.96,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
                 
        umv( pz, pz0 - offsetPtz - 1 - thicknessPt/2 - zpos )######!!!!!!   1um fixed offset should not have been here!!!!!
        newdataset( "bottom_r0_Z%03d"%((k+1))  )  #####################This 'bottom_r0_' prefeix should not have been here
        explog("LAYER bottom_r0_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))  #####################This 'bottom_r0_' prefeix should not have been here
        measure_layer_withbeamcheck( 3.45, 0.15 )

        
def volscan_Tb01_5pc(klast=1):       ####Magnesium
    pz0 = 51.775  ###initial guess of middle of Pt band or dot in z dir
    alignangle=27.5   ###modify for exact position where grippers align
    thicknessPt=0   ###full thickness (in z dir) of Pt band on pillars, from SEM image; make =0 for tensile
    offsetPtx=0.28   ###make =0 for pillars...for tensile, small number, from SEM image
    offsetPty=7.35   ###make =0 for pillars... for tensile, big number, from SEM image
    offsetPtz=16.55   ###make =0 for pillars... for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=0   ###for when you don't want to start at bottom of gauge
    for k, zpos in enumerate( np.arange(0,7.96,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
                 
        umv( pz, pz0 - offsetPtz - 1 - thicknessPt/2 - zpos )  ######!!!!!!   1um fixed offset should not have been here!!!!!
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.6, 0.15 )
      

###collect missing layers: Z029 failed to save data?!?!   plus extra 5 layers due to tensile stretch
def volscan_Tb01_5pc_extra1(klast):       ####Magnesium
    pz0 = 51.775  ###initial guess of middle of Pt band or dot in z dir
    alignangle=27.5   ###modify for exact position where grippers align
    thicknessPt=0   ###full thickness (in z dir) of Pt band on pillars, from SEM image; make =0 for tensile
    offsetPtx=0.28   ###make =0 for pillars...for tensile, small number, from SEM image
    offsetPty=7.35   ###make =0 for pillars... for tensile, big number, from SEM image
    offsetPtz=16.55   ###make =0 for pillars... for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=9.1   ###for when you don't want to start at bottom of gauge
    for k, zpos in enumerate( np.arange(0,4.21,0.15) ): ####orZ029
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
                 
        umv( pz, pz0 - offsetPtz - 1 - thicknessPt/2 - zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.6, 0.15 )

    for k, zpos in enumerate( np.arange(0,1.06,0.15) ): ####for effectively the negative Z layers: 1um height region below Z001
        
        if ( (k+1) % 2 ) == 0 or (k+1) == 1:
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == 1:
            umv( pz , pz0 )
            newdataset("yref")
            ref_dty_rotflex_cen(alignangle-90,alignangle,alignangle+90)
            umvr(px,-offsetPty*np.cos(np.radians(alignangle)))
            umvr(py,offsetPty*np.sin(np.radians(alignangle)))
            umvr(px,offsetPtx*np.sin(np.radians(alignangle)))
            umvr(py,offsetPtx*np.cos(np.radians(alignangle)))
                 
        umv( pz, pz0 - offsetPtz - extraoffsetz - thicknessPt/2 - zpos )
        newdataset( "extralayersabove_Z%03d"%((k+1))  )
        explog("LAYER extralayersabove_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.6, 0.15 )

#########In situ indenter_Ti tensile samples###################abandoned as alignment failed#####################



#code to run first to incorporate Ti mca signal:
##    mca.rois.set('Ti',350,550) ###in 10s eV
##100-200
def volscan_TiTb01_0pc(klast=1):    #####few unstrained layers: 1um frop bottom and top of gauge, plus 5 consecutive layers in the middle
    pz0 = 45.9836   ###initial guess of middle of Pt band or dot in z dir
    alignangle=27.5   ###modify for exact position where grippers align
    offsetPtz=20.25   ###make =0 for pillars... for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=3.5   ###for when you don't want to start at bottom of gauge
    for k, zpos in enumerate( np.arange(0,0.1,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 - 4 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 - offsetPtz -  1 - zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.45, 0.15 )

    for k, zpos in enumerate( np.arange(0,0.7,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
    
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 - 4 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 - offsetPtz -  extraoffsetz - zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.45, 0.15 )

    for k, zpos in enumerate( np.arange(0,0.1,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 - 4 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 - offsetPtz -  7 - zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.45, 0.15 )





def volscan_TiTb01_1pc(klast=1):
    pz0 = 45.9836   ###initial guess of middle of Pt band or dot in z dir
    alignangle=27.5   ###modify for exact position where grippers align
    offsetPtz=20.25   ###make =0 for pillars... for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=0   ###for when you don't want to start at bottom of gauge
    for k, zpos in enumerate( np.arange(0,8.26,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 - 4 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 - offsetPtz -  extraoffsetz - zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.45, 0.15 )


def volscan_TiTb01_5pc(klast=1):
    pz0 = 45.9836   ###initial guess of middle of Pt band or dot in z dir
    alignangle=27.5   ###modify for exact position where grippers align
    offsetPtz=20.25   ###make =0 for pillars... for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=0   ###for when you don't want to start at bottom of gauge
    for k, zpos in enumerate( np.arange(0,8.26,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_indtens_alt( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 - 4 )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 - offsetPtz -  extraoffsetz - zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.45, 0.15 )


#########In situ indenter_Ti pillar samples########################################

def volscan_TiP01_0pc(klast=1):
    pz0 = 36.47   ###initial guess of middle of Pt band or dot in z dir
    alignangle=30   ###modify for exact position where grippers align
    offsetPtz=3.02  ###make =0 for pillars..., except if tapered top; for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=4.5   ###for when you don't want to start at bottom/top of gauge
    for k, zpos in enumerate( np.arange(0,0.7,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_knife_insitu_Ti( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 + 1 + offsetPtz )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 + offsetPtz +  extraoffsetz + zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.45, 0.15 )



def volscan_TiP01_1pc(klast):
    pz0 = 35.92   ###initial guess of middle of Pt band or dot in z dir
    alignangle=30   ###modify for exact position where grippers align
    offsetPtz=3.02  ###make =0 for pillars..., except if tapered top; for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=0   ###for when you don't want to start at bottom/top of gauge
    for k, zpos in enumerate( np.arange(0,9.91,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_knife_insitu_Ti( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 + 1 + offsetPtz )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 + offsetPtz +  extraoffsetz + zpos )
        newdataset( "r1_Z%03d"%((k+1))  )
        explog("LAYER r1_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.6, 0.15 )

        
def volscan_TiP01_5pc(klast=1):
    pz0 = 35.72   ###initial guess of middle of Pt band or dot in z dir
    alignangle=30   ###modify for exact position where grippers align
    offsetPtz=3.02  ###make =0 for pillars..., except if tapered top; for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=0   ###for when you don't want to start at bottom/top of gauge
    for k, zpos in enumerate( np.arange(0,9.31,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_knife_insitu_Ti( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 + 1 + offsetPtz )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 + offsetPtz +  extraoffsetz + zpos )
        newdataset( "r0_Z%03d"%((k+1))  )
        explog("LAYER r0_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.45, 0.15 )

def volscan_TiP01_5pc_dty_alignatlocation(klast):
    pz0 = 36.39   ###initial guess of middle of Pt band or dot in z dir
    alignangle=30   ###modify for exact position where grippers align
    offsetPtz=3.02  ###make =0 for pillars..., except if tapered top; for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=0   ###for when you don't want to start at bottom/top of gauge
    for k, zpos in enumerate( np.arange(0,9.31,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_knife_insitu_Ti( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 + 0.75 + offsetPtz + zpos )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 + offsetPtz +  extraoffsetz + zpos )
        newdataset( "r0_Z%03d"%((k+1))  )
        explog("LAYER r0_Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.45, 0.15 )
    
####### second Ti pillar

def volscan_TiP02_0pc(klast=1):
    pz0 = 26.14   ###initial guess of middle of Pt band or dot in z dir
    alignangle=30   ###modify for exact position where grippers align
    offsetPtz=2.95  ###make =0 for pillars..., except if tapered top; for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=4.5   ###for when you don't want to start at bottom/top of gauge
    for k, zpos in enumerate( np.arange(0,1.06,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_knife_insitu_Ti( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 + 5 + offsetPtz )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 + offsetPtz +  extraoffsetz + zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.45, 0.15 )


def volscan_TiP02_1pc(klast=1):
    pz0 = 26.64   ###initial guess of middle of Pt band or dot in z dir
    alignangle=30   ###modify for exact position where grippers align
    offsetPtz=2.95  ###make =0 for pillars..., except if tapered top; for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=4.5   ###for when you don't want to start at bottom/top of gauge
    for k, zpos in enumerate( np.arange(0,1.06,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_knife_insitu_Ti( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 + 5 + offsetPtz )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 + offsetPtz +  extraoffsetz + zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.45, 0.15 )
        
def volscan_TiP02_unload(klast=1):
    pz0 = 27.39   ###initial guess of middle of Pt band or dot in z dir
    alignangle=30   ###modify for exact position where grippers align
    offsetPtz=2.95  ###make =0 for pillars..., except if tapered top; for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=4.0   ###for when you don't want to start at bottom/top of gauge
    for k, zpos in enumerate( np.arange(0,1.06,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_knife_insitu_Ti( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 + 4.5 + offsetPtz )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_cen_Ti(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 + offsetPtz +  extraoffsetz + zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.75, 0.15 )

        
#########Al Marvin samples#################################################

def volscan_AlP01_0pc(klast=1):  ####used for AlHT01    full heat treated sample, no deformation steps here
    pz0 = 46.6   ###initial guess of middle of Pt band or dot in z dir
    alignangle=0   ###modify for exact position where grippers align
    offsetPtz=0.91  ###make =0 for pillars..., except if tapered top; for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=4   ###for when you don't want to start at bottom/top of gauge
    for k, zpos in enumerate( np.arange(0,8.11,0.15) ):
        
        if (k+1) < klast: # skip already done points
            continue
        
        if ( (k+1) % 2 ) == 0 or (k+1) == klast:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_knife_insitu_Zr( pz0,alignangle )
            
        if ((k+1) % 10 ) == 0 or (k+1) == klast:
            umv( pz , pz0 + 6 + offsetPtz )
            # timing about 82
            newdataset("yref")
            ref_dty_rotflex_Zr(alignangle-90,alignangle,alignangle+90)

        umv( pz, pz0 + offsetPtz +  extraoffsetz + zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck( 3.6, 0.15 )



####### quick scan of undeformed, electropolished SS316L needle ############

def volscan_SSEPN3_SSundef_5centidegrees():  ####used for AlHT01    full heat treated sample, no deformation steps here
    pz0 = 70.6   ###initial guess of middle of Pt band or dot in z dir
    alignangle=0   ###modify for exact position where grippers align
    offsetPtz=0  ###make =0 for pillars..., except if tapered top; for tensile, centre Pt dot to bottom of gauge, from SEM image
    extraoffsetz=0   ###for when you don't want to start at bottom/top of gauge
    for k, zpos in enumerate( np.arange(0,20.01,10) ):   ####did 2um and 10um steps here
        
        umv( pz, pz0 + offsetPtz +  extraoffsetz + zpos )
        newdataset( "Z%03d"%((k+1))  )
        explog("LAYER Z%03d %f %f %f %f %f"%((k+1), pz0, px.position, py.position,
                                             dty.dial, pz.position))
        measure_layer_withbeamcheck(10 , 10 )      ####did 2um and 10um steps here

