import sys
import numpy as np
import time
import os
from bliss.common import cleanup
    
atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
bigy = config.get('bigy')
u22 = config.get('u22')



def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = -1
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.1
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -4
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.8
    dct_pars['slit_vg'] = 0.2
    dct_pars['dist'] = 9
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = difftz
    dct_pars['samtz_cen'] = -34.391
    dct_pars['shift_step_size'] = 0.18
    dct_pars['nof_shifts'] = 5
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars

def define_dct_hr_pars():
    dct_pars={}
    dct_pars['start_pos'] = -5
    dct_pars['step_size'] = 0.05
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.1
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -4
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.6
    dct_pars['slit_vg'] = 0.1
    dct_pars['dist'] = 5
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = difftz
    dct_pars['samtz_cen'] = -34.59
    dct_pars['shift_step_size'] = 0.08
    dct_pars['nof_shifts'] = 7
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars


def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = 0
    pct_pars['step_size'] = 0.24
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.1
    pct_pars['refon'] = pct_pars['num_proj']
#    pct_pars['ref_int'] = 10
    pct_pars['ref_step'] = -3
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 100
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = samtz
    pct_pars['samtz_cen'] = -2.36
    pct_pars['slit_hg'] = 1.3
    pct_pars['slit_vg'] = 1.3
    pct_pars['nof_shifts'] = 1
    pct_pars['shift_step_size'] = 1
    pct_pars['dist'] = 100
    pct_pars['abs_dist'] = 10
    pct_pars['scan_type'] = 'fscan'
    return pct_pars
    
def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = -1
    ff_pars['step_size'] = 0.8
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.8
    ff_pars['slit_vg'] = 0.2
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = difftz
    ff_pars['samtz_cen'] = -34.391
    ff_pars['shift_step_size'] = 0.18
    ff_pars['nof_shifts'] = 5
    ff_pars['cpm18_detune'] = 0
    return ff_pars
    
def tdxrd_boxscan(scanname, ff_pars):
    print("using slit_vg %f and slit_hg %f" % (ff_pars['slit_vg'], ff_pars['slit_hg']))
    umv(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.05, s8hg, ff_pars['slit_hg'] + 0.05)
    newdataset(scanname)
    finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    
def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return
    
def pct_zseries(pct_pars, datasetname = 'pct'):
    shift_step_size = abs(pct_pars['shift_step_size'])
    if(pct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = pct_pars['samtz_cen'] - (pct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(pct_pars['samtz_cen']))
    if(offset_samtz_pos < pct_pars['zmot'].low_limit or offset_samtz_pos + (pct_pars['nof_shifts'] - 1) * shift_step_size > pct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(pct_pars['nof_shifts'])):
        umv(pct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), pct_pars)
    umv(pct_pars['zmot'], pct_pars['samtz_cen'])
    print('PCT Z-series Succeed')
    return
    
def ff_zseries(ff_pars, datasetname = 'ff'):
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tdxrd_boxscan(f'{datasetname}{iter_i+1}', ff_pars)
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    print('FF Z-series Succeed')
    return('Succeed')
    
def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    ct(pars['exp_time'])
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.05, s8hg, pars['slit_hg'] + 0.05)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
            ct(pars['exp_time'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])
    
def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    #loopscan(nref, exp_time)
    umv(ref_mot, refpos)
    
def pct_abs_series(pars):
    
    umv(nfdtx, pars['dist'])
    pct_exp_time = pars['exp_time']
    pct_zseries(pars)
    
    umv(nfdtx, pars['abs_dist'])
    pars['exp_time'] = 0.06
    pct_zseries(pars, datasetname='abs')
    pars['exp_time'] = pct_exp_time
    umv(nfdtx, pars['dist'])
    
    
def att_out():
    fsh.close()
    try:
        assert ffdtz1.position > 400
        print('Safe to remove attenuators')
    except AssertionError:
        print('Check if the FF detector is out of the beam')
        return
    umv(atty, -10, attrz, 0)
    
def att_in():
    umv(atty, 0, attrz, -3.5)

def marana_out():
    umv(d3ty, 180)

def ff_in(pars):
    sheh3.close()
    att_in()
    ff_slits(pars)
    try:
        assert atty.position == 0 and d3ty.position > 179
    except AssertionError:
        print('Check attenuator or nfdtx!')
        return
    umv(ffdtz1, 0)
    ACTIVE_MG.disable('frelon*')
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.enable('frelon3*')
    
def ff_calibrant(pars):
    att_in()
    ff_slits(pars)
    umv(attrz, -10)
    newdataset('CeO2')
    finterlaced(diffrz, 0, 0.8, 360/0.8, 0.08)
    umv(attrz, -4)
    
    
def ff_slits(pars):
    umv(s7hg, pars['slit_hg'], s7vg, pars['slit_vg'], s8hg, pars['slit_hg'] + 0.05, s8vg, pars['slit_vg'] + 0.05) 
    
def tomo_slits(pars):
    umv(s7hg, pars['slit_hg'], s7vg, pars['slit_vg'], s8hg, pars['slit_hg'] + 0.1, s8vg, pars['slit_vg'] + 0.1) 
    
    
def marana_in():
    
    try:
        assert ffdtz1.position > 450
    except AssertionError:
        print("Won't move the marana, risk of colision ! Check inside !")
    umv(s7hg, 1.4, s7vg, 1.4, s8hg, 1.5, s8vg, 1.5)
    umv(d3ty, 0)
    ACTIVE_MG.disable('frelon*')
    ACTIVE_MG.enable('marana:image')
    ct(0.01)
    plotimagecenter()
    
def ff_out():
    sheh3.close()
    try:
        assert d3ty.position > 179
    except AssertionError:
        print('Check d3 position ! Might be dangerous to move !')
        return
    umv(ffdtz1, 500)
    att_out()

def F3P_1(dct_pars, ff_pars):
    dct_zseries(dct_pars)
    sheh3.close()
    tfoh1.set(0, 'Be')
    umv(nfdtx, 20)
    umv(d3ty, 180)
    umv(ffdtz1, 0)
    umv(atty, 0, attrz, -3.5)
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.enable('frelon3*')
    sheh3.open()
    ff_zseries(ff_pars)
    sheh3.close()
    umv(ffdtz1, 500)     

###############################################################
def tdxrd_forward360(datasetname, 
          ymax = 0.53,
          ystep = 0.0025,
          ycen = 15.0089,
          ymin = 0.,
          rstep=0.8,
          expotime=0.1,
          offset = 1
          ):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ycen = central position 
    ymin = low y values to skip, for restarting from a crash
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.tdxrd_forward(0.5, 'toto', ystep=0.1, ycen=14.65)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 11:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 ) + ycen
    i = -1
    #
    num_proj = 360 / rstep
    diffty_airpad.on( 60*24*5 )
    with cleanup.cleanup( diffrz, restore_list=(cleanup.axis.POS,) ):
        while i < len(ypos)-1:
            i += 1        
            if ypos[i] < ymin: # skip positions already done
                continue
            #pause_for_refill( 25 )  # time for one scan
            umv( diffty, ypos[i] )
            if (i % 2) == 0: # forwards
                 finterlaced( diffrz,   0 + offset,  rstep, num_proj, expotime, mode='FORWARD' )
            else:
                 finterlaced( diffrz, 720 + offset, -rstep, num_proj, expotime, mode='FORWARD' )
    umv(diffty, ycen)
    diffty_airpad.off()

def overnight(dset_name):
    heigths = [-0.052, 0, 0.06]
    for ii, height in enumerate(heigths):
        umv(samtz, height)
        tdxrd_forward360(f'{dset_name}{ii}')
    sheh3.close()
    umv(ffdtz1, 500)
	     
def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
	
def in_situ_loading(pars, sample_name, voltage_ramprate = 0.01, voltage_target = 120, nscans=1000, last_scan = 0):
    
    if pars['dist'] < 90:
        print('You are dangerously close')
        return
    ACTIVE_MG.enable('stress*')
    ACTIVE_MG.enable('nanodac3*')
    print('Activated stress saving')
    stress_regul.plot()
    initialize_stress_regul()
    print('Initialized stress regulation')
    print('Will move nfdtx and slits')
    umv(nfdtx, pars['dist'], s7hg, pars['slit_hg'], s8hg, pars['slit_hg']+0.2, s7vg, pars['slit_vg'], s8vg, pars['slit_vg']+0.2)
    newsample(f'{sample_name}_insitu')
    newdataset('sct')
    sct(pars['exp_time'])
    print('Activating ramprate')
    regul_off(voltage_ramprate, voltage_target)
    #init_load = stress_adc.get_value()
    print('Started voltage ramprate')
    ii = 0
    if last_scan != 0:
        ii = last_scan + 1
    for _ in range(nscans):
        
        newdataset(f'pct_{ii:04}')
        #take flats
        #if ii % 10 == 0:   
        #acquire projections
        print('Will acquire projections')
        if ii % 2 == 0:
            fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        else:
            fscan(diffrz, pars['start_pos'] + 360, -1 * pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        
        if stress_adc.get_value() <= 1:
            regul_off(0, stress.position)
            print('Your sample probably broke. Finishing.')
            break
        ii += 1        
    print('Finished') 
    
def flats_and_darks(pars, sample_name):
    if furnace_z.position < 80:
        print('Remove the furnace first !')
        return
    newdataset('darks')
    sheh3.close()
    ftimescan(pars['exp_time'], 50)
    sheh3.open()
    newdataset('flats')
    umvr(pars['ref_mot'], pars['ref_step'])
    ftimescan(pars['exp_time'], 1000)
    umvr(pars['ref_mot'], pars['ref_step'])
    print('Finished darks and flats')
            

def interrupt_scans():
    regul_off(10, -30)
    mvr(difftz, -8)
    newdataset('flats')
    ftimescan(pars['exp_time'], 1000)

def switch_to_pct(pct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    sheh3.close()
    ACTIVE_MG.enable('marana:image')
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.disable('frelon16*')
    umv(atty, -10, attrz, 0)
    umv(ffdtx1, 190)
    #umv(d1ty, -120, d2ty, -120)
    umv(d1ty, -120, d2ty, -120, d1tz, 100, d2tz, 100)
    if ffdtz1.position < 199:   #avoid moving nfdtx between pct scans
        umv(nfdtx, 50)
        umv(ffdtz1, 200)    
    #what if we were at DCT conditions first? To debug
    umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'])
    umvct(s8vg, pct_pars['slit_vg'] + 0.05, s8hg, pct_pars['slit_hg'] + 0.05)
    assert d1ty.position < -119
    assert ffdtz1.position > 199
    umvct(d3ty, 0, nfdtx, pct_pars['dist'])
    sheh3.open()
    sct(pct_pars['exp_time'])

def switch_to_dct(dct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(36,'Be')
    sheh3.close()
    ACTIVE_MG.enable('frelon16:image')
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.disable('frelon3*')
    umv(atty, -10, attrz, 0)
    umv(ffdtx1, 190)
    umv(d3ty, 180, ffdtz1, 700)
    umvct(s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umvct(s8vg, dct_pars['slit_vg'] + 0.05, s8hg, dct_pars['slit_hg'] + 0.05)
    assert d3ty.position > 179
    assert furnace_z.position > 129
    umvct(nfdtx, 215)  # corresponds to about 7 mm
    umv(d1tz, 0, d2tz, 0)
    umv(d1ty, 0, d2ty, 0, d1tz, 0, d2tz, 0)
    sheh3.open()
    sct(dct_pars['exp_time'])

def switch_to_ff(ff_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    sheh3.close()
    ACTIVE_MG.enable('frelon3:image')
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.disable('frelon16*')
    umv(atty, 0, attrz, -5)
    umv(d3ty, 180, d1ty, -120, d2ty, -120)
    umv(d1tz, 100, d2tz, 100)
    umvct(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umvct(s8vg, ff_pars['slit_vg'] + 0.05, s8hg, ff_pars['slit_hg'] + 0.05)
    assert d3ty.position > 179
    assert d1ty.position < -119
    assert d2ty.position < -119
    #assert furnace_z.position > 129
    umvct(nfdtx, 50)  
    umv(ffdtx1, 190)
    umv(ffdtz1, 0)
    sheh3.open()
    sct(ff_pars['exp_time'])
    
def interrupted_loading(pct_pars, ff_pars,sample_name = 'g9_s9', voltage_ramprate = 0.01, voltage_target = 120, delta = 4, max_load = 130, last_scan = 0, nscans = 1000):

    if pct_pars['dist'] < 90:
        print('You are dangerously close')
        return
    ACTIVE_MG.enable('stress*')
    ACTIVE_MG.enable('nanodac3*')
    print('Activated stress saving')
    stress_regul.plot()
    initialize_stress_regul()
    current_load = stress_adc.get_value()
    regul_on(current_load)
    print('Initialized stress regulation')
    print('Will move nfdtx and slits')
    umv(nfdtx, pct_pars['dist'], s7hg, pct_pars['slit_hg'], s8hg, pct_pars['slit_hg']+0.2, s7vg, pct_pars['slit_vg'], s8vg, pct_pars['slit_vg']+0.2)
    #newsample(f'{sample_name}_interrupted')
    #newdataset('sct')
    #sct(pct_pars['exp_time'])
   
    
    ii = 0
    if last_scan != 0:
        ii = last_scan + 1
    
    #init_load = round(stress_adc.get_value())
    
    #loads = [jj for jj in range(init_load, max_load, load_increment)]
    
    jj = 0
    
    for _ in range(nscans):
        is_ff_pos = False
    
        if (jj % delta == 0 and jj > 0):
            load_at_ff = stress_adc.get_value()
            initialize_stress_regul()
            current_output = stress_regul.output.read()
            regul_off(0, current_output)
            load_ramp_by_target(75)
            initialize_stress_regul()
            sleep(1)
            regul_on(75)
            sheh3.close()
            switch_to_ff(ff_pars)
            sheh3.open()
            ff_zseries(ff_pars, f'ff_{ii:04}_')
            #_ = loads.pop(0)
            is_ff_pos = True
	    
        if is_ff_pos:
            sheh3.close()
            switch_to_pct(pct_pars)
            sheh3.open()
            initialize_stress_regul()
            current_output = stress_regul.output.read()
            regul_off(0, current_output)
            load_ramp_by_target(load_at_ff)
            initialize_stress_regul()

        print('Activating ramprate')
        regul_off(voltage_ramprate, voltage_target)
        print('Started voltage ramprate')    
      
     
        newdataset(f'pct_{ii:04}')
        
        print('Will acquire projections')
        if ii % 2 == 0:
            fscan(diffrz, pct_pars['start_pos'], pct_pars['step_size'], pct_pars['num_proj'], pct_pars['exp_time'], scan_mode = pct_pars['scan_mode'])
            
        else:
            fscan(diffrz, pct_pars['start_pos'] + 360, -1 * pct_pars['step_size'], pct_pars['num_proj'], pct_pars['exp_time'], scan_mode = pct_pars['scan_mode'])
        current_load = stress_adc.get_value()    
        if current_load > max_load:
            print('Finished your scan. Go inside to unload')
            initialize_stress_regul()
            return
        #checks flag for far-field positions, if True, will switch to PCT - avoids excessive printing and shutter open/close - scts

        if stress_adc.get_value() <= 1:
            regul_off(0, stress.position)
            print('Your sample probably broke. Finishing.')
            break
        ii += 1
        jj += 1        
    print('Finished') 
    
    
def load_ramp_by_target(target, time_step=0.1):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
   
    #marana_large_beam(pct_pars)
    current_load = stress_adc.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print('Stress (V)', stress.position, 'Load:', current_load)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print('Stress (V)', stress.position, 'Load:', current_load)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print('Stress (V)', stress.position, 'Load:', current_load)
    return
    
    
def creep_sagbo(pars, sample_name, load_setpoint = 120, nscans=1000, last_scan = 0):
    
    if pars['dist'] < 90:
        print('You are dangerously close')
        return
    ACTIVE_MG.enable('stress*')
    ACTIVE_MG.enable('nanodac3*')
    print('Activated stress saving')
    stress_regul.plot()
    #initialize_stress_regul()
    print('Initialized stress regulation')
    print('Will move nfdtx and slits')
    umv(nfdtx, pars['dist'], s7hg, pars['slit_hg'], s8hg, pars['slit_hg']+0.2, s7vg, pars['slit_vg'], s8vg, pars['slit_vg']+0.2)
    newsample(f'{sample_name}_insitu')
    #newdataset('sct')
    #sct(pars['exp_time'])
    print('Activating ramprate')
    #regul_on(load_setpoint)
    #init_load = stress_adc.get_value()
    print(f'Started regulation at {load_setpoint} N.')
    ii = 0
    if last_scan != 0:
        ii = last_scan + 1
    for _ in range(nscans):
        
        newdataset(f'pct_{ii:04}')

        print('Will acquire projections')
        if ii % 2 == 0:
            fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        else:
            fscan(diffrz, pars['start_pos'] + 360, -1 * pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        
        if stress_adc.get_value() <= 1:
            regul_off(0, stress.position)
            print('Your sample probably broke. Finishing.')
            break
        ii += 1        
    print('Finished') 
    


