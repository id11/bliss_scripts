
import numpy as np
import time
from bliss.common import cleanup





def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)


def tdxrd_forward360(ymax,
          datasetname,
          ystep = 0.01,
          ycen = 14.501,
          ymin = 0.,
          rstep=0.8,
          expotime=0.08
          ):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ycen = central position 
    ymin = low y values to skip, for restarting from a crash
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.tdxrd_forward(0.5, 'toto', ystep=0.1, ycen=14.65)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 11:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 ) + ycen
    i = -1
    #
    num_proj = 360 / rstep
    diffty_airpad.on( 60*24*5 )
    with cleanup.cleanup( diffrz, restore_list=(cleanup.axis.POS,) ):
        while i < len(ypos)-1:
            i += 1        
            if ypos[i] < ymin: # skip positions already done
                continue
            pause_for_refill( 25 )  # time for one scan
            umv( diffty, ypos[i] )
            if (i % 2) == 0: # forwards
                 finterlaced( diffrz,   0,  rstep, num_proj, expotime, mode='FORWARD' )
            else:
                 finterlaced( diffrz, 720, -rstep, num_proj, expotime, mode='FORWARD' )
    umv(diffty, ycen)
    diffty_airpad.off()
    

    

class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )

        
            
        


def half1(ymax,
          datasetname,
          ystep=0.25,
          ymin=-1801,
          rstep=0.05,
          astart = -90,
          arange = 181,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        plotselect("eiger:roi_counters:roi1_max")
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)




def next_rstart( stepsize, speed, home_range=15. ):
    """ Computes the next start position to keep going """
    if abs(360 - (rot.dial % 360)) < home_range: # if you are within range, go home
        print("Homing because there is a home nearby")
        rot.home()
    elif rot.dial*rot.steps_per_unit > pow(2,30):
        print("Running out of steps so homing")
        rot.home()
    pnow = rot.position
    offset = speed * speed / ( 2 * rot.acceleration )
    next_start = ( int(np.floor( (pnow + np.sign(stepsize)*offset)/stepsize )) + 1 ) * stepsize
    return next_start
            
def full1(ymax,
          datasetname,
          ystep=0.25,
          ymin=-1801,
          rstep=0.05,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.full1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.full1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    NR = int(361/rstep)
    umv(dty, max(ypos[0], ymin) )
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=( cleanup.axis.POS, ) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                rstart = next_rstart( rstep, angular_velocity )
                fscan2d( dty, ypos[i], ystep, 1, rot, rstart, rstep, 361/abs(rstep), expotime )
                # 7 min 53s with 25 scans == 19 seconds per scan
                
    umv(dty, 0)


def sun_pm():
    ymax = 300
    for zpos in np.arange(5,100,15):
        umv(pz, zpos)
        half1(ymax, f'Z{int(zpos)}', ystep=1 )
        


# following is made on Monday
def furnace_in():
    umv(mty, -148.6)
    umv(fluoz, 126.35)
    
def furnace_out():
    umv(fluoz, 126.35 + 80)
    
def nscope_basler_view():
    umv(s9hg, 1.5, s9vg, 1.5)
    umv(piny, 17.4, pinz, 0.45)
    umv(aly, -7.98)
    #umv(fluoz, 126.35)   # the right position for the furnace
       
def nscope_box_beam(beamsize_y = 0.5, beamsize_z = 0.45, scanbeamstop=True):
    sheh3.close()
    umv(edoor,0)
    umv(det2y,-223.94)
    umv(s9hg, beamsize_y, s9vg, beamsize_z)
    ACTIVE_MG.enable('eig*')
    ACTIVE_MG.disable('basler_*')
    open_edoor(scanbeamstop=scanbeamstop)
    umv(piny, 17.4, pinz, 0.41)


def nscope_focus_beam(scanbeamstop=True):
    sheh3.close()
    umv(s9hg, 0.15, s9vg, 0.15)
    umv(piny, 0, pinz, 0)
    umv(aly, 0.02)
    umv(edoor,0)
    umv(det2y,-223.94)
    ACTIVE_MG.enable('eig*')
    ACTIVE_MG.disable('basler_*')
    open_edoor(scanbeamstop=scanbeamstop)
    


def in_situ_box_beam():
    umvct(rot, 0)
    while True:
        fscan(rot,-2, 0.1, 41, 0.005)



def monday_night():
    ymax = 195
    for zpos in np.arange(8,100,15):
        umv(pz, zpos)
        half1(ymax, f'e20um_Z{int(zpos)}', ystep=1 )


def monday_night():
    ymax = 195
    for zpos in np.arange(8,100,15):
        umv(pz, zpos)
        half1(ymax, f'e20um_Z{int(zpos)}', ystep=1 )


def wolfgang_sample():
    ymax = 345
    ntz0 = -0.6051667
    
    # middle layer
    umv(ntz, ntz0)
    half1(ymax, 'slice_middle', ystep=0.6)
    
    
    # upper layer
    umvr(ntz, ntz0-0.3)
    half1(ymax, 'slice_up_300um', ystep=0.6)
    
    
    # bottom layer
    umvr(ntz, ntz0+0.3)
    half1(ymax, 'slice_down_300um', ystep=0.6)
    
    sheh3.close()
    print('Done')

    
def gisele_sample():
    ntz0 = 5.490000
    pz0 = 50.0
    
    umv(ntz, ntz0, pz, pz0)
    # middle layer
  #  ymax = 182
   # umv(ntz, ntz0)
   # half1(ymax, 'slice_middle', ystep=0.6)
    
    
    # upper layer
   # ymax = 192
  #  umvr(pz, pz0-25)
  #  half1(ymax, 'slice_up_25um', ystep=0.6)
    
    
    # bottom layer
    ymax = 192
    umv(pz, pz0+25)
    half1(ymax, 'slice_down_25um', ystep=0.6)
    
    # region of interest 34*34*35 um^3
    ymax = 17
    for zpos in np.arange(pz0 - 17, pz0+17, 1):
        umv(pz, zpos)
        half1(ymax, f'ROI_Z{int(zpos)}', ystep = 0.6 )
    
    
    sheh3.close()
    print('Done')       


def gisele_sample_corr():
    ntz0 = 5.490000
    pz0 = 50.0
    
    umv(ntz, ntz0, pz, pz0)
    # middle layer
  #  ymax = 182
   # umv(ntz, ntz0)
   # half1(ymax, 'slice_middle', ystep=0.6)
    
    
    # upper layer
   # ymax = 192
  #  umvr(pz, pz0-25)
  #  half1(ymax, 'slice_up_25um', ystep=0.6)
    
    
    # upper lay
    ymax = 192
    umv(pz, pz0-25)
    half1(ymax, 'slice_rear_up_redo_25um', ystep=0.6)
    
    # region of interest 34*34*35 um^3
    ymax = 17
    for zpos in np.arange(pz0 - 17, pz0+17, 1):
        umv(pz, zpos)
        half1(ymax, f'ROI_Z{int(zpos)}', ystep = 0.6 )
    
    
    sheh3.close()
    print('Done')  




def yunhui_TiAl_sample():
    # corresponding to slice 375 in PCT
    ntz0 = -0.97341
    pz0 = 50.0
    
    umv(ntz, ntz0, pz, pz0)
    
    pzs = [35.0, 50.0, 65.0, 80.0]
    ymax = 200
    for zpos in pzs:
        umv(pz, zpos)
        if zpos != pzs[-1]:
            half1(ymax, f'slice_Z{int(zpos)}', ystep = 0.4, rstep = 0.125, astart = -90, arange = 181, expotime = 0.005 )
        else:
            half1(135, f'slice_Z{int(zpos)}', ystep = 0.4, rstep = 0.125, astart = -90, arange = 181, expotime = 0.005 )
    
    sheh3.close()
    umv(edoor, 0)
    print('Done')
        
