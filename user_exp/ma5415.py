import numpy as np, time
from bliss.common.cleanup import cleanup, axis as cleanup_axis

user_script_load('optics')
#diffty_airpad.on(alert_timeout=24*60*60*4)

# def check_cpm18(pos=6.4158):
def check_cpm18(pos=10.1208):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)


def pause_for_refill(t, pos=10.1208):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18(pos=pos)

def tomoand3dxrd(index,force):
    '''This does the tomo and 3dxrd measurements. Run after correcting positions'''
    gotopct()
    dsetname=f'{index}_{force}N_PCT'
    sheh3.open()
    fulltomo.full_turn_scan(dsetname,start_pos=-180)
    gotodiff()
    dsetname=f'{index+1}_{force}N_3DXRD'
    ff_layers(dsetname)
    gotopct()
    

def gotopct( ):
    sheh3.close()
    umv(ffdtx1, 450)
    umv(ffdtz1, 400)
    umv(nfdtx, 30)
    umv(d3ty, -0.15)
    umv(s7vg, 1.3, s8vg, 2., s7hg, 1.3, s8hg, 2. )
    ACTIVE_MG.enable('marana')
    ACTIVE_MG.disable('frelon3')
    fscan.pars.scan_mode='CAMERA' 
    user.cpm18_goto( 6.356 )
    
def gotodiff( ):
    sheh3.close()
    umv(nfdtx, 50)
    umv(d3ty, 180)
    umv(ffdtz1, -1)
    umv(ffdtx1, 150)
    umv(s7vg,0.03) #30 micron heigh beam
    ACTIVE_MG.enable('frelon3')
    ACTIVE_MG.disable('marana')
    fscan.pars.scan_mode='TIME' 

def takedic( dsetname ):
    newdataset( dsetname )
    umv(diffrz,0)
    sheh3.close()
    loopscan(3,0.1) #dark
    sheh3.open()
    posn = samy.position
    umvr(samy,-2)
    loopscan(3,0.1) #flat
    umv(samy, posn)
    loopscan(3,0.1) #proj at 0
    umv(diffrz,90)
    loopscan(3,0.1) #proj at 90
    umv(diffrz,180)
    loopscan(3,0.1)#proj at 180
    
def ff_layers( dsetname ):
    newdataset(dsetname)
    zpos=samtz.position
    dz=0.03
    zs=np.linspace(zpos-dz*5,zpos+dz*5,11)
    sheh3.open()
    for z in zs:
        umv(samtz,z)
        pause_for_refill( 120, pos=6.356 )
        finterlaced( diffrz, -90, 0.25, 180/0.25, 0.08, mode='ZIGZAG' )
    umv(samtz,zpos)
    

def hr3dxrd(dsetname):
    z0=samtz.position
    zs=[z0-0.15,z0-0.1,z0-0.05,z0,z0+0.05,z0+0.1,z0+0.15]
    sheh3.open()
    for z in zs:
        newdataset(f'DCT_z_{z:.4f}')
        umv(samtz,z)
       # pause_for_refill(pos=6.3483)
        fscan(diffrz,0,0.1,3600,2)
        
        

    
def s3dxrd_volume(datasetname):
    '''This should do a 1mm*0.1mm volume with s3dxrd'''
    #gotodiff()
    user.cpm18_goto(10.12) #detune the undulator
    umv(bigy,-0.08) #Move in the lenses
    z0=samtz.position
    zs=np.linspace(z0-0.05,z0+0.05,11)
    sheh3.open()
    for z in zs:
        dataset=f'{datasetname}_z_{z:.4f}'
        print(f"I'm at z={z:.4f}")
        #if z<1.855:
        #    continue
        umv(samtz,z)
        pointscan(0.51,dataset)
    print('Moving back to z0')
    umv(samtz,z0)
    sheh3.close()
    umv(bigy,22) #move out the lenses
    user.cpm18_goto(6.356) #optimize flux
    
    
    

def pointscan(ymax, datasetname, ystep=0.01, ymin=-1801):
    ''' ymax is the range : 0.15 goes from -0.15 to 0.15
    datasetname = None means append in the old file
    ymin = Number means restart from this point
    '''
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    y0 = diffty.position
    y0 = 14.5468 # ...
    with cleanup( diffty, restore_list=(cleanup_axis.POS,)): 
    
        ny = int( ymax // ystep )
        ylim = ny * ystep
        if ylim < ymax:
            ny += 1
            ylim += ystep
        ypos = np.linspace( -ylim, ylim, ny*2+1 )
        i = -1
        while i < len(ypos)-1:
            i += 1        
            if (ypos[i]+y0) < ymin:
                continue
            pause_for_refill( 45, pos=10.12 )
            umv( diffty, y0 + ypos[i] )
            finterlaced( diffrz, -90, 0.8, 180/0.8, 0.08, mode='ZIGZAG' )
    print("Diffty should be back to",y0)
    wm(diffty)

    

## Difftomo macro for nscope session

def half1(ymax, datasetname, ystep=0.75, ymin=-1801):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25,pos=10.13 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.05, 3620, 0.002 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 181, -0.05, 3620, 0.002 )
        
def wed_night():
    for z in (1,50,99):
        newsample(f'NSCOPE_SiMo1000_6')
        umv(pz, z)
        half1( 485, f'DT600nm_Z{z}' )

def sat_night():
    for z in (1,50,99):
        umv(pz,z)
        half1(485,f'DT750nm_Z{z}')
        
