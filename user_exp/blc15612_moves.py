"""
Macros to handle detector moves using IN, OUT and PARK positions .
Tries to move detectors safely by checking that other detectors are out of the way.

Philosophies:

- We should only move anything if certain conditions are met
    - i.e if the beamline is in a condition we have tested for
- Check the positions of other detectors before moving that detector
- Moving a detector should only move that detector
- Detectors can only move between IN, OUT and PARK positions

The philosophy is that moving one detector will never move another detector
THIS IS NOT GUARENTEED TO BE SAFE YET! STILL UNDER HEAVY DEVELOPMENT
Started by James Ball 30/08/2024
"""


"""
Range of sensibility for ffdty to allow any movement
We cannot guarentee safety if ffdty position is out of these ranges
"""
ffdty_safe_bounds = (-0.5, 0.5)


"""
Range of sensibility for nfdtx to allow any movement
We cannot guarentee safety if nfdtx position is out of these ranges
"""
nfdtx_safe_bounds = (10, 250)

"""
Range of sensibility for nfdtx to allow d3 movement
We cannot guarentee we can move d3 safely if nfdtx position is out of these ranges
"""
nfdtx_safe_bounds_d3 = (5, 100)



def check_ffdty_bounds(): 
    if ffdty_safe_bounds[0] <= ffdty.position <= ffdty_safe_bounds[1]:
        return True
    return False
    
def check_nfdtx_bounds():
    if nfdtx_safe_bounds[0] <= nfdtx.position <= nfdtx_safe_bounds[1]:
        return True
    return False


def safe_move_d4(pos: str):
    """Safely move detector d4 to either IN, OUT or PARK.
       Checks that d1, d2, d3 are all in OUT or PARK"""
    
    if pos not in ['IN', 'OUT', 'PARK']:
        raise ValueError("Invalid position supplied! Valid positions are one of ['IN', 'OUT', 'PARK']")
    
    if not check_ffdty_bounds():
        raise ValueError('ffdty not at a safe position!')
    if not check_nfdtx_bounds():
        raise ValueError('nfdtx not at a safe position!')
    
    if d4.position not in ['IN', 'OUT', 'PARK']:
        raise ValueError("d4 not in ['IN', 'OUT', 'PARK']. Cannot move")
    
    # we're in safe bounds to move, now check the other detectors
    for other_detector in [d1, d2, d3]:
        if other_detector.position not in ['OUT', 'PARK']:
            raise ValueError(f'{other_detector.name} not in OUT or PARK! Cannot move')
    
    # d4 should be safe to move
    print(f'Calling d4.move({pos})')
    d4.move(pos)


def safe_move_d3(pos: str):
    """Safely move detector d3 to either IN, OUT or PARK.
       Checks that d1, d2, d4 are all in OUT or PARK"""
    
    if pos not in ['IN', 'OUT', 'PARK']:
        raise ValueError("Invalid position supplied! Valid positions are one of ['IN', 'OUT', 'PARK']")
    
    if not check_ffdty_bounds():
        raise ValueError('ffdty not at a safe position!')
    if not check_nfdtx_bounds():
        raise ValueError('nfdtx not at a safe position!')
    
    if d3.position not in ['IN', 'OUT', 'PARK']:
        raise ValueError("d3 not in ['IN', 'OUT', 'PARK']. Cannot move")
    
    # we're in safe bounds to move, now check the other detectors
    for other_detector in [d1, d2, d4]:
        if other_detector.position not in ['OUT', 'PARK']:
            raise ValueError(f'{other_detector.name} not in OUT or PARK! Cannot move')
    
    # Now for d3 - check that NFDTX is in a sensible range to move

    if not (nfdtx_safe_bounds_d3[0] <= nfdtx.position <= nfdtx_safe_bounds_d3[1]):
        raise ValueError('nfdtx not at a safe position to move d3')
    
    # d3 should be safe to move
    print(f'Calling d3.move({pos})')
    d3.move(pos)
