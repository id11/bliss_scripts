import time
import numpy as np
def define_T_para():
    T_para={}
    T_para['heat_rate'] = 40
    T_para['cool_rate'] = 40
    T_para['time_iso'] = 0.5*60
    T_para['Temp'] = 410
    T_para['final_temp'] = 400
    T_step = 10
    T_para['Temp_step'] = np.arange(T_para['Temp']-T_step, T_para['final_temp'], -T_step)
    return T_para


def pct_heat_isothermal_step_cooling(T_para, tol = 3):
    heat_rate = T_para['heat_rate']
    cool_rate = T_para['cool_rate']
    time_iso = T_para['time_iso']
    Temp = T_para['Temp']
    final_temp = T_para['final_temp']
    Temp_step = T_para['Temp_step']   
    
    umv(furnace_z, 100)
    umv(nfdtx, 100)
    sheh3.open()
    fulltomo.full_turn_scan('pct_no_furnace')
    sheh3.close()
    
    #umv(nfdtx,250)
    print("moving down furnance...")
    umv(furnace_z, 0)
    
    if heat_rate > 50:
        print('heating rate is too high; set to 50 C per min')
        heat_rate = 50
    nanodac3.ramprate = heat_rate
    nanodac3.setpoint = Temp
    
    # heating
    t0 = time.time()
    dt = 0
    exit_flag = False
    i=1
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        print('Current temperature is {} C; Heating time is {} s'.format(nanodac3_temp.read(), dt))
        print('Heating time is {} s'.format(dt))
        if np.abs(nanodac3_temp.read() - Temp) <= tol:
            exit_flag = True
        sleep(10)

    # isothermal
    t0 = time.time()
    dt = 0
    exit_flag = False
    i=0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        print('Isothermal time is {} s'.format(dt))
        if dt > time_iso:
            exit_flag = True
        sleep(10)
    
    # cooling
    print("start cooling...")
    nanodac3.ramprate = cool_rate
    
    for T in Temp_step:
        nanodac3.setpoint = T
        while np.abs(nanodac3_temp.read() - T) > tol:
            print('Waiting to cool down to {} C'.format(T))
            sleep(10)
        
        umv(nfdtx, 100)
        sheh3.open()
        t0 = time.time()
        newdataset(f'pct_{T}C')
        fscan(diffrz, 0, 0.2, 1800, 0.1)
        dt = time.time() - t0
        print('Total acquisition time is {} s'.format(dt))
        umv(nfdtx, 250)
    
    print('Scan finished! start sleeping ...')
    sheh3.close()
    sleep(300)
    umv(furnace_z, 100)
    umv(nfdtx, 100)
    print('Heating, isothermal and cooling have finished...')


def ff_heat_isothermal_cool(T_para, tol = 5):
    heat_rate = T_para['heat_rate']
    cool_rate = T_para['cool_rate']
    time_iso = T_para['time_iso']
    Temp = T_para['Temp']
    final_temp = T_para['final_temp']
    
    print("moving down furnance...")
    umv(furnace_z, 0)
    
    sheh3.open()
    if heat_rate > 50:
        print('heating rate is too high; set to 50 C per min')
        heat_rate = 50
    nanodac3.ramprate = heat_rate
    nanodac3.setpoint = Temp
    
    # heating
    t0 = time.time()
    dt = 0
    exit_flag = False
    i=1
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        print('Current temperature is {} C; Heating time is {} s'.format(nanodac3_temp.read(), dt))
        print('Heating time is {} s'.format(dt))
        if np.abs(nanodac3_temp.read() - Temp) <= tol:
            exit_flag = True
        newdataset('heating_' +str(i))
        ftimescan(2, 100)
        #loopscan(50, 0.1, sleep_time = 0.1)

    # isothermal
    t0 = time.time()
    dt = 0
    exit_flag = False
    i=0
    while exit_flag == False:
        i=i+1
        newdataset('iso_' + str(Temp)+'C_'+str(i))
        ftimescan(2, 100)
        dt = time.time() - t0
        print('Isothermal time is {} s'.format(dt))
        if dt > time_iso:
            exit_flag = True 
        #loopscan(50, 0.1, sleep_time = 0.1)
    
    # cooling
    print("start cooling...")
    nanodac3.ramprate = cool_rate
    nanodac3.setpoint = final_temp
    t0 = time.time()
    exit_flag = False
    i=0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        print('Current temperature is {} C; Cooling time is {} s'.format(nanodac3_temp.read(), dt))       
        if nanodac3_temp.read() <= final_temp:
            exit_flag = True
        # newdataset('cooling_' + str(cool_rate)+'C_min_'+str(i))
        # ftimescan(2, 100)
        # loopscan(50, 0.1, sleep_time = 0.1)
    print('Scan finished! start sleeping ...')
    sheh3.close()
    sleep(300)
    umv(furnace_z, 100)
    print('Heating, isothermal and cooling have finished...')
    

def ff_out():
    umv(ffdtx1, 600, ffdtz1, 300)
    
def marana_in():
    ff_out
    umv(d3ty, 0, d3tz, 0)

def marana_out():
    umv(d3ty, 180, d3tz, 150)
    
def ff_in():
    marana_out
    umv(d3tz, 0, ffdtx1, 300)
    
def pdf_heat_isothermal_cool(T_para, tol = 5):
    heat_rate = T_para['heat_rate']
    cool_rate = T_para['cool_rate']
    time_iso = T_para['time_iso']
    Temp = T_para['Temp']
    final_temp = T_para['final_temp']
    
    print("moving down furnance...")
    umv(furnace_z, 0)
    
    sheh3.open()
    if heat_rate > 50:
        print('heating rate is too high; set to 50 C per min')
        heat_rate = 50
    nanodac3.ramprate = heat_rate
    nanodac3.setpoint = Temp
    
    # heating
    t0 = time.time()
    dt = 0
    exit_flag = False
    i=0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        print('Current temperature is {} C; Heating time is {} s'.format(nanodac3_temp.read(), dt))
        print('Heating time is {} s'.format(dt))
        if np.abs(nanodac3_temp.read() - Temp) <= tol:
            exit_flag = True
        newdataset('heating_' +str(i))
        loopscan(20, 0.1, sleep_time = 0.2)
        finterlaced(diffrz,0,1.8,25,0.4,mode='ZIGZAG')

    # isothermal
    t0 = time.time()
    dt = 0
    exit_flag = False
    i=0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        print('Isothermal time is {} s'.format(dt))
        if dt > time_iso:
            exit_flag = True        
        newdataset('iso_' + str(Temp)+'C_'+str(i))
        finterlaced(diffrz,0,1.8,25,0.4,mode='ZIGZAG')
    
    # cooling
    print("start cooling...")
    nanodac3.ramprate = cool_rate
    nanodac3.setpoint = final_temp
    t0 = time.time()
    exit_flag = False
    i=0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        print('Current temperature is {} C; Cooling time is {} s'.format(nanodac3_temp.read(), dt))       
        if nanodac3_temp.read() <= final_temp:
            exit_flag = True
        newdataset('cooling_' + str(cool_rate)+'C_min_'+str(i))
        finterlaced(diffrz,0,1.8,25,0.4,mode='ZIGZAG')
    print('Scan finished! start sleeping ...')
    sheh3.close()
    sleep(300)
    umv(furnace_z, 100)
    print('Heating, isothermal and cooling have finished...')



def heat_isothermal_cool(T_para, tol = 5):
    heat_rate = T_para['heat_rate']
    cool_rate = T_para['cool_rate']
    time_iso = T_para['time_iso']
    Temp = T_para['Temp']
    final_temp = T_para['final_temp']
    
    sheh3.open()
    if heat_rate > 50:
        print('heating rate is too high; set to 50 C per min')
        heat_rate = 50
    nanodac3.ramprate = heat_rate
    nanodac3.setpoint = Temp

    while np.abs(nanodac3_temp.read() - Temp) > tol:
        print("waiting for furnace to reach setpoint")
        sleep(30)
    
    print("moving down furnance...")
    umv(furnace_z, 0)
    t0 = time.time()
    dt = 0
    exit_flag = False
    i=0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        dt = dt
        print('Isothermal time is {} s'.format(dt))
        if dt > time_iso:
            exit_flag = True
        newdataset('iso_' + str(Temp)+'C_'+str(i)+'_T')
        sct(0.185)
        
        newdataset('iso_' + str(Temp)+'C_'+str(i))
        finterlaced(diffrz,0,1.8,50,0.185,mode='ZIGZAG')
    
    print("start cooling...")
    nanodac3.ramprate = cool_rate
    nanodac3.setpoint = final_temp
    t0 = time.time()
    exit_flag = False
    i=0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        dt = dt
        print('Current temperature is {} C; Cooling time is {} s'.format(nanodac3_temp.read(), dt))       
        if nanodac3_temp.read() <= final_temp:
            exit_flag = True
        # newdataset('cooling_' + str(cool_rate)+'C_min_'+str(i)+'_T')
        # sct(0.185)
        newdataset('cooling_' + str(cool_rate)+'C_min_'+str(i))
        finterlaced(diffrz,0,1.8,50,0.185,mode='ZIGZAG')
    print('Scan finished! start sleeping ...')
    sleep(300)
    umv(furnace_z, 100)
    print('Heating, isothermal and cooling have finished...')

def T_calibration(T_para, tol=5):
    heat_rate = T_para['heat_rate']
    cool_rate = T_para['cool_rate']
    time_iso = T_para['time_iso']
    Temp = T_para['Temp']
    final_temp = T_para['final_temp']
    
    print("moving down furnance...")
    umv(furnace_z, 0)
    sheh3.open()
    if heat_rate > 50:
        print('heating rate is too high; set to 50 C per min')
        heat_rate = 50
    nanodac3.ramprate = heat_rate
    nanodac3.setpoint = Temp
    
    exit_flag = False
    i=0
    while exit_flag == False:
        i=i+1
        if np.abs(nanodac3_temp.read() - Temp) <= tol:
            exit_flag = True
        newdataset('heating_' +str(i))
        loopscan(50, 0.185, sleep_time = 0.2)

    
    print("start isothermal...")
    t0 = time.time()
    dt = 0
    exit_flag = False
    i=0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        dt = dt
        print('Isothermal time is {} s'.format(dt))
        if dt > time_iso:
            exit_flag = True
        newdataset('iso_' + str(Temp)+'C_'+str(i))
        loopscan(50, 0.185, sleep_time = 0.2)
    
    print("start cooling...")
    nanodac3.ramprate = cool_rate
    nanodac3.setpoint = final_temp
    t0 = time.time()
    exit_flag = False
    i=0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        dt = dt
        print('Current temperature is {} C; Cooling time is {} s'.format(nanodac3_temp.read(), dt))       
        if nanodac3_temp.read() <= final_temp:
            exit_flag = True
        newdataset('cooling_' + str(cool_rate)+'C_min_'+str(i))
        loopscan(50, 0.185, sleep_time = 0.2)

    print('Scan finished! start sleeping ...')
    sleep(300)
    umv(furnace_z, 100)
    print('Heating, isothermal and cooling have finished...')
    print('Done !')




