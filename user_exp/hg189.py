import numpy as np
import time
#diffty = 14.6207

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    #pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']
       

def tomoin():
    ACTIVE_MG.enable("marana:image") 
    ACTIVE_MG.disable("frelon3*")
    sheh3.close()
    umv(ffdtz1, 150, d3ty, 0)
    sheh3.open()
    print("Ready to collect tomo data") 

def frelonin():
    ACTIVE_MG.disable("marana*") 
    ACTIVE_MG.enable("frelon3:image") 
    ACTIVE_MG.enable("frelon3:roi*")
    sheh3.close()
    umv(d3ty,150, ffdtz1, 0)
    sheh3.open()
    print("Ready to collect diffraction data") 

def crlin():
    bigy=config.get('bigy')
    #tfoh1 = config.get("tfoh1")
    umv(bigy, 0.)
    #tfoh1.set(18,"Be") 
    
def crlout():
    bigy=config.get('bigy')
    #tfoh1 = config.get("tfoh1")
    umv(bigy, 22)
    #tfoh1.set(0,"Be")
    #print("done")


def powder():
    frelonin()
    crlout()
    ascan(diffrz, 0, 180, 90, 0.1)
       
    
def difftyfrelon():
    # newproposal("ma4498")
    # fpico4
    #diffty_airpad.on()
    pico4.auto_range = False
    pico4.range=2.1e-5
    
    ACTIVE_MG.disable_all()
    ACTIVE_MG.enable("frelon3:roi*")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("*fpico4")
    
    y0 = 14.117
    width = 0.36
    step  = 0.005
    #2h

    for my_y  in np.arange( y0-width, y0+width+step/10., step):
        umv(diffty, my_y)
        pause_for_refill(60)
        finterlaced(diffrz,   0,  1, 180, 0.08, mode="ZIGZAG") 
        #diffty.reset_closed_loop()
    umv(diffty, y0)
    
    
    #diffty_airpad.off()


def hydrascan():
    for hs,my_s in enumerate(np.arange(0,30,1)):
        newdataset("cerbonio_hydroscan_night_%03d"%(hs))
        frelonin()       
        crlin()
        difftyfrelon()
        crlout()
        powder()
        tomoin()
        umv(diffty, 14.665)
        tomo.half_turn_scan() 

def startscan():
    diffty.reset_closed_loop()
    newdataset("Bassanite2_start")
    frelonin()
    crlout()
    powder()
    crlin()
    difftyfrelon()
    crlout()
    tomoin()
    umv(diffty, 14.70)
    tomo.half_turn_scan() 



def diffraction():
    frelonin()
    crlin()

def goto_tomo():
    newdataset('align')
    tomoin()
    crlout()
    umv(s7hg,2,s7vg,2,s8vg,2,s8hg,2)

def launch_tomography(start=0):
    newdataset('tomo')
    tomoin()
    crlout()
    umv(s7hg,2,s7vg,2,s8vg,2,s8hg,2)
    fulltomo.half_turn_scan(start_pos=start)
    
def xrd_zscan():
    z_zero=samtz.position
    for i in range(0,1000):
        if i % 2 == 0:
            ascan(samtz,z_zero,z_zero+7.5,75,0.1)
        else:
            ascan(samtz,z_zero+7.5,z_zero,75,0.1)
        
def launch_xrd_vscan(dset_name='xrd_vscan'):
    newdataset(dset_name)
    frelonin()
    crlout()
    umv(s8hg,1,s8vg,1,s7hg,1,s7vg,0.1)
    xrd_zscan()
    
def launch_xrdct(start_diffty,dset_name='xrdct_scan'):
    newdataset(dset_name)
    frelonin()
    crlin()
    umv(s8hg,1,s8vg,1,s7hg,1,s7vg,1)
    y0 = start_diffty
    width = 0.25
    step  = 0.006
    print(np.arange( y0-width, y0+width+step/10., step).shape)
    for my_y  in np.arange( y0-width, y0+width+step/10., step):
        diffty.reset_closed_loop()
        umv(diffty, my_y)
        pause_for_refill(60)
        finterlaced(diffrz,   0,  1, 180, 0.1, mode="ZIGZAG")
    umv(diffty, start_diffty)
    
def run_ct_xrdct_scan(start_diffty):
    for i in range(0,100):
        launch_tomography()
        launch_xrdct(start_diffty)
        
def calibration_scan(dset_name='finterlaced_diffrz'):
    newdataset(dset_name)
    frelonin()
    crlout()
    umv(s8hg,1,s8vg,1,s7hg,1,s7vg,0.1)
    finterlaced(diffrz,   -90,  0.1, 1800, 0.1, mode="ZIGZAG")
   
def capillary_scan():
    newsample('capillaries_2nd_set')
    names = ['MSN2','MSN3','MSN4_black','MSN5','MSN6','Noto10_DAP','Noto10_DAP_AmOx','Noto8_AmOx_DAP','Noto9_DAP_AmOx','CeO2']
    samty_vals = [16.75,12.85,8.7,5.05,1,-3.35,-7,-11.15,-15.45,-19.1]
    for i in range(0,10):
        newdataset(names[i])
        umv(samty,samty_vals[i])
        fscan(diffrz,-10,0.1,200,0.1)
    
    
    
    
    

    
    
    
