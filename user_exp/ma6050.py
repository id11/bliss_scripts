import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime


def run_exp(prefix):
    print(prefix)
    # med beam - med rotation
    bs_hg = 0.3
    bs_vg = 0.3
    change_beamsize(bs_hg, bs_vg)
    newdataset(prefix)
    diff_med_rotation()

    # big beamsh
#    bs_hg = 1.3
#    bs_vg = 0.5
#    change_beamsize(bs_hg, bs_vg)
#    newdataset(prefix + '_bigbeam')
#    diff_small_rotation()

    
    # median beam
#    bs_hg = 0.3
#    bs_vg = 0.3
#    change_beamsize(bs_hg, bs_vg)
#    newdataset(prefix + '_medianbeam')
#    diff_med_rotation()
    
    
    # small beam
#    bs_hg = 0.1
#    bs_vg = 0.1
#    change_beamsize(bs_hg, bs_vg)
#    newdataset(prefix + '_smallbeam')
#    diff_big_rotation()
    
    
    # change to median beam
#    bs_hg = 0.3
#    bs_vg = 0.3
#    change_beamsize(bs_hg, bs_vg)


def change_beamsize(bs_hg, bs_vg):
    umv(s7hg, bs_hg, s7vg, bs_vg, s8hg, bs_hg+0.05, s8vg, bs_vg+0.05)
    

def diff_small_rotation():
    finterlaced(diffrz, -10, 0.1, 200, 0.09)

def diff_med_rotation():
    finterlaced(diffrz, -40, 1, 80, 0.09)    
   
def diff_big_rotation():
    umv(diffrz,-40)
    finterlaced(diffrz, -40, 1, 80, 0.09)
    finterlaced(diffrz, -220, 1, 80, 0.09)


# during the loading
# newdataset('loading_to_160N')
# loopscan(1000,0.09)

def ff_zseries(datasetname = 'ff', nof_shifts=3, shift_step_size = 0.2, samtz_cen = 3.58):
    offset_samtz_pos = samtz_cen - (nof_shifts - 1) * shift_step_size / 2
    print('Central samtz:' + str('samtz_cen'))
    for iter_i in range(int(nof_shifts)):
        umv(samtz, iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, -40, 0.125, 640, 0.09)
        finterlaced(diffrz, -220, 0.125, 640, 0.09)
    
    umv(samtz, samtz_cen)
    umv(diffrz,0)
    print('ff_zseries succeed')
    return('Succeed')
    
    
    
def ff_zseries_big_step(datasetname = 'ff', nof_shifts=3, shift_step_size = 0.2, samtz_cen = 3.58):
    offset_samtz_pos = samtz_cen - (nof_shifts - 1) * shift_step_size / 2
    print('Central samtz:' + str('samtz_cen'))
    for iter_i in range(int(nof_shifts)):
        umv(samtz, iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, -40, 1, 80, 0.09)
        finterlaced(diffrz, -220, 1, 80, 0.09)
    
    umv(samtz, samtz_cen)
    umv(diffrz,0)
    print('ff_zseries succeed')
    return('Succeed')
    
    
def collect_data_during_cooling(dsetname='cooling', tol = 0.25, T_target = 120):
    newdataset(dsetname)
    stop_flag = False
    while stop_flag is not True:
        loopscan(100, 0.09)
        T = ox800.read()
        print(f'Current temperature is: {T} K')
        if abs(T - T_target)<=tol:
            stop_flag = True
        
        











    


