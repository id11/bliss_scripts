import os, sys
import numpy as np


import time
from bliss.common import cleanup

tfoh1 = config.get('tfoh1')

def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )

def half1(ymax,
          datasetname,
          ystep=2.0,
          ymin=-3000,
          rstep=0.05,
          astart = -180,
          arange = 361,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        plotselect("eiger:roi_counters:roi1_max")
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)

def switch_to_eiger():
    umv(s9vg, 0.05, s9hg, 0.05)
    fsh.session='NSCOPE'
    newdataset('align')
    moveto_eiger()
    #umv(aly,0)
    umv(vly, 0.002, hlz, -0.30700)
    if piny.position > 1.0:
        switch_pinhole()
    tfoh1.set(0, 'Be')
    umvct(atty,0,attrz,-20)
    

def switch_to_basler():
    moveto_basler()
    fsh.session='NSCOPE'
    #umv(aly,-8)
    umv(vly, 0.002-1, hlz, -0.30700+1)
    if piny.position < 1.0:
        switch_pinhole()
    sheh3.open()
    umv(atty,-10,attrz,0)
    tfoh1.set(0, 'Be')
    umvct(s9vg,1.5, s9hg, 1.5)
    plotimagecenter()
    
    
def s1_scan():
    pos_list = [[-0.9422, 0.2011, 10.4599],
                [-0.9709, 0.1547, 10.4804],
                [-0.9367, 0.2079, 10.4804],
                [-0.9852, 0.1346, 10.5036],
                [-0.9292, 0.2097, 10.5036]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_p{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=60, end=-60)
        print(f'Done for inclusion no. {i+1}')
        
def scanning_p1():
    half1(ymax=15, datasetname=f'_s3DXRD_p1', ystep=0.5, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    #sheh3.close()
    print('Done') 

def scanning_p5():
    pos =  [-0.9572 ,  0.17215, 10.5036 ]
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    half1(ymax=80, datasetname=f'_s3DXRD_p5', ystep=0.5, rstep=0.05, astart=-90, arange=181, expotime=0.002)
            
def s2_scan():
    pos_list = []

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_p{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=60, end=-60)
        print(f'Done for inclusion no. {i+1}')
               
def A2050_sxscan():
    pos_list = [[-0.8441, -0.7485, 14.5645],
                [-0.8318, -0.6278, 14.4148],
                [-0.8449, -0.6512, 14.4148],
                [-0.8578, -0.6111, 14.3432],
                [-0.8335, -0.6300, 14.3869],
                [-0.9206, -0.5786, 14.5933],
                [-0.9229, -0.5782, 14.6017]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_p{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1, start=40, end=-40)
        print(f'Done for inclusion no. {i+1}')     
        
        
def A2050_ROI_3DXRD_p1():
    pos =  [-0.8441, -0.7485, 14.5645]
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 0.027, ntz0 + 0.027, 0.0025)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=50, datasetname=f'ROI_p1_z{i}', ystep=0.5, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    umvct(rot,0)
    print('Done')
    sheh3.close() 


def A2050_full_3DXRD_p1():
    ntz0 = 14.5645
    umv(ntz, ntz0)
    zs = [14.555, 14.565, 14.575]
    nos = [7, 11, 15]
    for i, z_pos in zip(nos, zs):
        umvct(ntz, z_pos)
        half1(ymax=205, datasetname=f'full_p1_z{i}', ystep=0.5, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    umvct(ntz, ntz0)
    umvct(rot,0)
    print('Done')
    sheh3.close()



 
def A2050_ROI_3DXRD_p1_continue():
    pos =  [-0.8441, -0.7485, 14.5645]
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 0.027, ntz0 + 0.027, 0.0025)
    for i, z_pos in enumerate(zs):
        if i == 19:
            umvct(ntz, z_pos)
            half1(ymax=50, datasetname=f'ROI_p1_z{i}_rep2', ystep=0.5, rstep=0.05, astart=-90, arange=181, expotime=0.002)
        elif i > 19:
            umvct(ntz, z_pos)
            half1(ymax=50, datasetname=f'ROI_p1_z{i}', ystep=0.5, rstep=0.05, astart=-90, arange=181, expotime=0.002)           
    umvct(rot,0)
    print('Done')
    sheh3.close()
    

def large_tomo_fov():
    delta = 0.4
    pos_list = [delta, delta+0.95, delta+0.95*2]
    
    diffty0 = 14.0115
    samtz0 = 2.13264
    
    umv(samtz,samtz0)
    for i, pos in enumerate(pos_list):
        umv(diffty, diffty0 + pos)
        fulltomo.full_turn_scan(f'pct_bot_{i}')
    
    
    umv(samtz,samtz0 - 0.95)
    for i, pos in enumerate(pos_list):
        umv(diffty, diffty0 + pos)
        fulltomo.full_turn_scan(f'pct_top_{i}')
    
    umvct(diffty, diffty0)
    print('Done')
    
    
    
    
    
    
    
    
    
    
    




    
            
