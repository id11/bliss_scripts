import os, sys
import numpy as np


import time
from bliss.common import cleanup

tfoh1 = config.get('tfoh1')

def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )

def half1(ymax,
          datasetname,
          ystep=2.0,
          ymin=-3000,
          rstep=0.05,
          astart = -180,
          arange = 361,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        plotselect("eiger:roi_counters:roi1_max")
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)



def switch_to_eiger():
    umv(s9vg, 0.05, s9hg, 0.05)
    fsh.session='NSCOPE'
    newdataset('align')
    moveto_eiger()
    #umv(aly,0)
    umv(vly, -0.015, hlz, -0.293)
    if piny.position > 1.0:
        switch_pinhole()
    #tfoh1.set(32, 'Be')
    tfoh1.set(0, 'Be')
    umvct(atty,0,attrz,-20)
    

def switch_to_basler():
    moveto_basler()
    fsh.session='NSCOPE'
    #umv(aly,-8)
    umv(vly, -0.015-1, hlz, -0.293+1)
    if piny.position < 1.0:
        switch_pinhole()
    sheh3.open()
    umv(atty,-10,attrz,0)
    tfoh1.set(0, 'Be')
    umvct(s9vg,1.5, s9hg, 1.5)
    plotimagecenter()
    

def goto_top_ROI_s3DXRD():
    umv(ntx,-0.5917, nty, -0.1427, ntz, 1.8353)

def goto_bottom_ROI_s3DXRD():
    umv(ntx, -0.7763, nty, -0.1788, ntz, 13.1344)
    

def take_3DXRD():
    ##### top ROI
    goto_top_ROI_s3DXRD()
    ntz0 = ntz.position
    zs = [ntz0-0.02, ntz0-0.01, ntz0, ntz0+0.01]
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=115, datasetname=f'ROI_top_z{i}', ystep=0.5, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    umvct(rot,0)
    
    ##### bottom ROI
    goto_bottom_ROI_s3DXRD()
    ntz0 = ntz.position
    zs = [ntz0-0.02, ntz0-0.01, ntz0, ntz0+0.01, ntz0+0.02]
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=115, datasetname=f'ROI_bot_z{i}', ystep=0.5, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    umvct(rot, 0)
    
    print('Done')
    sheh3.close()
    umv(edoor, 0)
    
    
    









    
