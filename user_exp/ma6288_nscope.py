import os, sys
import numpy as np


import time
from bliss.common import cleanup

tfoh1 = config.get('tfoh1')

def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )

def half1(ymax,
          datasetname,
          ystep=2.0,
          ymin=-3000,
          rstep=0.05,
          astart = -180,
          arange = 361,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        plotselect("eiger:roi_counters:roi1_max")
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                    
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)


def switch_to_eiger_with_Si_lens():
    umv(s9vg, 0.05, s9hg, 0.05)
    if edoor.position < 20:
        fsh.session='NSCOPE'
        newdataset('align')
        moveto_eiger()
    umv(aly,-8)  # move Al lens out
#    umv(vly, 0.034, hlz, -0.30800)
    umv(vly, 0.038, hlz, -0.31400)  # update on Sunday 24/11
    if piny.position > 1.0:
        switch_pinhole()
    tfoh1.set(0, 'Be')
    #umvct(atty,0,attrz,-20)


def switch_to_eiger_with_Al_lens():
    if edoor.position < 20:
        umv(s9vg, 0.05, s9hg, 0.05)
        fsh.session='NSCOPE'
        newdataset('align')
        moveto_eiger()
#    umv(vly, 0.034-1, hlz, -0.30800+1) # move Si lens out
    umv(vly, 0.038-1, hlz, -0.31400+1) # move Si lens out, update on Sunday 24/11
    umv(aly,0)    
    if piny.position > 1.0:
        switch_pinhole()
    tfoh1.set(8, 'Be')
    #umvct(atty,0,attrz,-20)
      
def switch_to_eiger_with_big_beam():
    if edoor.position < 20:
        umv(s9vg, 0.05, s9hg, 0.05)
        fsh.session='NSCOPE'
        newdataset('align')
        moveto_eiger()
#    umv(vly, 0.034-1, hlz, -0.30800+1) # move Si lens out
    umv(vly, 0.038-1, hlz, -0.31400+1) # move Si lens out, update on Sunday 24/11
    umv(aly,-8)    # move Al lens out
    if piny.position < 1.0:
        switch_pinhole()
    tfoh1.set(0, 'Be')
    umvct(s9hg,0.6,s9vg,0.45)


def switch_to_basler():
    moveto_basler()
    fsh.session='NSCOPE'
    umv(aly,-8)
    #umv(vly, 0.034-1, hlz, -0.30800+1)
    umv(vly, 0.038-1, hlz, -0.3140+1)  # update on Sunday 24/11
    if piny.position < 1.0:
        switch_pinhole()
    sheh3.open()
    umv(atty,-10,attrz,0)
    tfoh1.set(0, 'Be')
    umvct(s9vg,1.5, s9hg, 1.5)
    plotimagecenter()
    
    
def furnace_in():
    assert furnaceZ > 60
    umv(mty, -148.6)
    umv(furnaceZ, 5.87)
    
def furnace_out():
    umv(furnaceZ, 80)
    umv(mty,0)

         
def A2050_sxscan_intermetallic_phase():
    # this has to run manually
    pos_list = [[0.2662, -0.5774, 8.1288],
                [0.1304, -0.5631, 8.1483],
                [0.2284, -0.3913, 8.1548],
                [0.3044, -0.4836, 8.2571]]

    for i, pos in enumerate(pos_list):
        umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
        #dset_name = f'sxewoks_sb_inclusion{i+1}'
        dset_name = f'sxewoks_p{i+1}'
        print(dset_name, pos)
        newdataset(dset_name)
        #user.sxscanewoks(step_size=-0.5, count_time=0.5, start=0 + 90, end=0 - 90)
        user.sxscanewoks(step_size=-0.5, count_time=1.5, start=36, end=-36)
        print(f'Done for inclusion no. {i+1}')     


def A2050_s3DXRD_0N():
    # 14 hours in total
    roi_big_center = [0.2113, -0.4657, 8.1585]    # 200*200 um box center
    roi_small_center = [0.2113, -0.4657, 8.1511]  # 50*25 um box center
    full_slice_center = roi_small_center          # selected full cross section of the sample
    
    switch_to_eiger_with_Si_lens()
    # full slice scan, 1 layer, 1.2 h
    pos = full_slice_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    half1(ymax=180, datasetname=f'full_slice', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
       
    # roi_small scan
    pos = roi_small_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 12.5*0.001, ntz0 + 12.5*0.001, 0.001)  # 25 layers, 4.2 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=25, datasetname=f'ROI_small_z{i}', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    
    switch_to_eiger_with_Al_lens()
    # roi_bg scan
    pos = roi_big_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 25*0.004, ntz0 + 25*0.004, 0.004)   # 50 layers, 8.4 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=100, datasetname=f'ROI_big_z{i}', ystep=4.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    umvct(rot,0)
    print('Done')
    sheh3.close() 


def A2050_s3DXRD_0N_continue():
    # interrupted at ROI_big_z14
    roi_big_center = [0.2113, -0.4657, 8.1585]    # 200*200 um box center
    roi_small_center = [0.2113, -0.4657, 8.1511]  # 50*25 um box center
    full_slice_center = roi_small_center          # selected full cross section of the sample
       
    
    switch_to_eiger_with_Al_lens()
    # roi_bg scan
    pos = roi_big_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 25*0.004, ntz0 + 25*0.004, 0.004)   # 50 layers, 8.4 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        if i<14:
            continue
        else:
            umvct(ntz, z_pos)
            half1(ymax=100, datasetname=f'ROI_big_z{i}', ystep=4.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    umvct(rot,0)
    print('Done')
    sheh3.close()
    
    
def A2050_s3DXRD_4N():
    full_slice_center = [0.2955, -0.4764, 8.1498]         # update this position
    
    switch_to_eiger_with_Si_lens()
    # full slice scan, 1 layer, 1.3 h
    pos = full_slice_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    half1(ymax=190, datasetname=f'full_slice', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    umvct(rot,0)
    print('Done')
    
    
def A2050_s3DXRD_8N():
    # 14 hours in total, please update the positions
    roi_big_center = [0.2930, -0.4761, 8.1582]    # 200*200 um box center
    roi_small_center = [0.2930, -0.4761, 8.1517]  # 50*25 um box center 
    full_slice_center = roi_small_center          # selected full cross section of the sample
    
    switch_to_eiger_with_Si_lens()
    # full slice scan, 1 layer, 1.3 h
    pos = full_slice_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    half1(ymax=190, datasetname=f'full_slice', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
       
    # roi_small scan
    pos = roi_small_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 12.5*0.001, ntz0 + 12.5*0.001, 0.001)  # 25 layers, 4.2 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=25, datasetname=f'ROI_small_z{i}', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    
    switch_to_eiger_with_Al_lens()
    # roi_bg scan
    pos = roi_big_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 25*0.004, ntz0 + 25*0.004, 0.004)   # 50 layers, 8.4 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=100, datasetname=f'ROI_big_z{i}', ystep=4.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    umvct(rot,0)
    print('Done')
    sheh3.close()
    
    
def A2050_s3DXRD_after_1st_strain():
    roi_big_center = [0.2218, -0.4400, 8.1963]    # 200*200 um box center
    roi_small_center = [0.2129, -0.4471, 8.1898]  # 50*25 um box center 
    full_slice_center = roi_small_center          # selected full cross section of the sample
    
    switch_to_eiger_with_Si_lens()
    # full slice scan, 5 layer, 1.3*5 h
    pos = full_slice_center
    
    ntz0 = pos[2]
    zs = [ntz0 - 0.05, ntz0, ntz0 + 0.05, ntz0 + 0.1, ntz0 + 0.2]  # 5 layers, 1.3*5 = 6.5 h
    for i, z_pos in enumerate(zs):
        umv(ntx, pos[0], nty, pos[1], ntz, z_pos)
        if i < 3:
            half1(ymax=190, datasetname=f'full_slice_z{i}', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
        else:
            half1(ymax=210, datasetname=f'full_slice_z{i}', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    
    # roi_small scan
    pos = roi_small_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 12.5*0.001, ntz0 + 12.5*0.001, 0.001)  # 25 layers, 4.2 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=25, datasetname=f'ROI_small_z{i}', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    
    switch_to_eiger_with_Al_lens()
    # roi_bg scan
    pos = roi_big_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 25*0.004, ntz0 + 25*0.004, 0.004)   # 50 layers, 8.4 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=100, datasetname=f'ROI_big_z{i}', ystep=4.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    umvct(rot,0)
    print('Done')
    sheh3.close()
    








### Below is for A2050_DN_W340
def DN_W340_s3DXRD_0N():
    roi_big_center = [0.14008, -0.85076, 7.87309]    # 200*200 um box center
    roi_small_center = roi_big_center  # 50*25 um box center
    full_slice_center = roi_small_center          # selected full cross section of the sample
    
    switch_to_eiger_with_Si_lens()
    # full slice scan, 1 layer, 1.3 h
    pos = full_slice_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    half1(ymax=182, datasetname=f'full_slice', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    umvct(rot,0)
    print('Done')
    sheh3.close()

    
    
def DN_W340_s3DXRD_after_5pct_strain():
    # 14 hours in total, please update the positions
    roi_big_center = [0.1933, -0.7610, 7.9224]    # 200*200 um box center
    roi_small_center = [0.1659, -0.6982, 7.9224]  # 50*25 um box center
    full_slice_center = roi_big_center          # selected full cross section of the sample
    
    switch_to_eiger_with_Si_lens()
    # full slice scan, 1 layer, 1.3 h
    pos = full_slice_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    half1(ymax=182, datasetname=f'full_slice', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    # roi_small scan
    pos = roi_small_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 12.5*0.001, ntz0 + 12.5*0.001, 0.001)  # 25 layers, 4.2 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=25, datasetname=f'ROI_small_z{i}', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    
    switch_to_eiger_with_Al_lens()
    # roi_bg scan
    pos = roi_big_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 25*0.004, ntz0 + 25*0.004, 0.004)   # 50 layers, 8.4 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=100, datasetname=f'ROI_big_z{i}', ystep=4.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    umvct(rot,0)
    print('Done')
    sheh3.close()


def DN_W340_s3DXRD_after_12pct_strain():
    # 15 hours in total, please update the positions
    roi_big_center = [0.1855, -0.6595, 7.9373]    # 200*200 um box center
    roi_small_center = [0.1602, -0.5873, 7.9373]  # 50*25 um box center
    full_slice_center = roi_big_center          # selected full cross section of the sample
    
    switch_to_eiger_with_Si_lens()
    # full slice scan, 1 layer, 1.3 h
    pos = full_slice_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    half1(ymax=182, datasetname=f'full_slice', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    # one more layer with sample moving 5 um down
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2]-0.005)
    half1(ymax=182, datasetname=f'full_slice_delta_5um', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    
    # roi_small scan
    pos = roi_small_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 12.5*0.001, ntz0 + 12.5*0.001, 0.001)  # 25 layers, 4.3 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=25, datasetname=f'ROI_small_z{i}', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    
    switch_to_eiger_with_Al_lens()
    # roi_bg scan
    pos = roi_big_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 25*0.004, ntz0 + 25*0.004, 0.004)   # 50 layers, 8.6 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=100, datasetname=f'ROI_big_z{i}', ystep=4.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    umvct(rot,0)
    print('Done')
    sheh3.close()




def DN_W340_s3DXRD_after_18pct_strain():
    roi_big_center = [0.3276, -0.4758, 7.9134]    # 200*200 um box center
    #roi_small_center = [0.1602, -0.5873, 7.9373]  # 50*25 um box center
    full_slice_center = roi_big_center          # selected full cross section of the sample
    
    switch_to_eiger_with_Si_lens()
    # full slice scan, 1 layer, 1.3 h
    pos = full_slice_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    half1(ymax=182, datasetname=f'full_slice', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    umvct(rot,0)
    print('Done')
    sheh3.close()


def DN_W340_s3DXRD_after_18pct_strain_final():
    # 17 hours in total, please update the positions
    roi_big_center = [0.3276, -0.4758, 7.9134]
    roi_small_center = [0.3023, -0.4036, 7.9134]
    
    full_slice_center = roi_big_center          # selected full cross section of the sample
    
    switch_to_eiger_with_Si_lens()
    # full slice scan, 1 layer, 1.3 h
    # pos = full_slice_center
    #umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    #half1(ymax=182, datasetname=f'full_slice', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    # one more layer with sample moving 5 um down
    # umv(ntx, pos[0], nty, pos[1], ntz, pos[2]-0.005)
    # half1(ymax=182, datasetname=f'full_slice_delta_5um', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    
    # roi_small scan
    pos = roi_small_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 12.5*0.001, ntz0 + 12.5*0.001, 0.001)  # 25 layers, 4.3 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=25, datasetname=f'ROI_small_z{i}', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    
    switch_to_eiger_with_Al_lens()
    # roi_bg scan
    pos = roi_big_center
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    ntz0 = pos[2]
    zs = np.arange(ntz0 - 25*0.004, ntz0 + 25*0.004, 0.004)   # 50 layers, 8.6 h
    print(zs, zs.shape)
    for i, z_pos in enumerate(zs):
        umvct(ntz, z_pos)
        half1(ymax=100, datasetname=f'ROI_big_z{i}', ystep=4.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    
    switch_to_eiger_with_Si_lens()
    # full slice scan, 1 layer, 1.3 h
    pos = full_slice_center
    #umv(ntx, pos[0], nty, pos[1], ntz, pos[2])
    #half1(ymax=182, datasetname=f'full_slice', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)
    
    # one more layer with sample moving 5 um down
    umv(ntx, pos[0], nty, pos[1], ntz, pos[2]-0.005)
    half1(ymax=182, datasetname=f'full_slice_delta_5um', ystep=1.0, rstep=0.05, astart=-90, arange=181, expotime=0.002)


    umvct(rot,0)
    print('Done')
    sheh3.close()


    
            
