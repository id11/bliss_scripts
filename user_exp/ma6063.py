import numpy as np
import time
from bliss.common import cleanup

def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t / 3.)


def map2d(y_start, y_final, y_step, z_start, z_final, z_step, exp_time, radius=None):

    """
    Function to perform a 2D map: The function performs a series of ascans i.e. one linescan per z position

    y_start: starting y position in mm
    y_final: final y position in mm
    y_step: y step in mm
    z_start: starting z position in mm
    z_final: final z position in mm
    z_step: z step in mm
    exp_time: exposure time in sec
    """

    z_positions = np.arange(z_start, z_final + z_step, z_step)
    y_positions = np.arange(y_start, y_final + y_step, y_step)
    ny = len(y_positions)
   
    y_middle = samty.position
    
    for i, z_pos in enumerate(z_positions):
 
        umv(samtz, z_pos)

        if radius is not None:
            L = 2 * np.sqrt(radius ** 2 - z_pos ** 2) + 1  ## scan extra 1 mm for safety
            y_positions = np.arange(-L / 2, L / 2 + y_step, y_step)
            ny = len(y_positions)
            if i % 2:
                fscan(samty, y_middle -(L / 2), y_step, ny, exp_time)
            else:
                fscan(samty, y_middle +(L / 2), -y_step, ny, exp_time)
        else:
            if i % 2:
                fscan(samty,y_middle + y_start, y_step, ny, exp_time)
            else:
                fscan(samty,y_middle - y_start, -y_step, ny, exp_time)
    umv(samty,y_middle)

# samtz bottom = 5.3
tfoh1 = config.get("tfoh1")
edoor = config.get("edoor")

def goto_marana_t(): 
    ACTIVE_MG.enable("marana3") 
    ACTIVE_MG.disable("frelon3*")
    sheh3.close()
    umv(edoor,0)
    umv(d3ty, 0,ffdtz1,200)
    umv(s7hg,1.5,s7vg,1.5)
    tfoh1.set(0,'Be')
    sync(bigy)
    umv(bigy,22)
    sheh3.open()
    print("ready to collect tomo data") 

def goto_frelon_t():
    ACTIVE_MG.disable("*marana3*") 
    ACTIVE_MG.enable("frelon3")
    sheh3.close()
    umv(edoor,0)
    umv(ffdtz1, 0, d3ty, 150)
    umv(s7hg,0.3,s7vg,0.3)
    tfoh1.set(32+16+8,'Be')
    sheh3.open()
    print("ready to collect diffraction data") 

def marana_tomo_scan():
    fulltomo.half_turn_scan('tdxrd_pct')

def goto_nanoscope():
    sheh3.close()
    umv(d3ty,150,ffdtz1,400)
    fsh.session='NSCOPE'
    
def goto_tdxrd():
    sheh3.close()
    umv(edoor,0)
    fsh.session='TDXRD'

def z_scan():
    # Start is bottom of the cell with white plastic
    fscan(samtz, samtz.position, -0.05, 200, 0.2)


def y_scan(samtx_0, nb_scan = 240):
    umv(samtx,samtx_0 - 2.55)
    for scan in range(0,nb_scan):
        print(scan)
        if scan % 2 == 0:
            ascan(samtx, samtx_0 - 2.55, samtx_0 + 2.55, 16, 0.08)
        else:
            ascan(samtx, samtx_0 + 2.55, samtx_0 - 2.55, 16, 0.08)

def goto_basler_n():
    moveto_basler(pinhole_switch=False)
    umv(bigy,22,piny,17.4,pinz,0.425)
    umv(hlz,0.847,vly,-1.004)
    umv(attrz,0,atty,-10)
    umv(s9hg,2,s9vg,2,aly,-8)
    tfoh1.set(0,'Be')
    sheh3.open()

def basler_tomo_scan():
    fulltomo.half_turn_scan('nscope_pct')

#def optimize_flux()
    # umv(atty,0) is attenuator in
    # umv(atty,-10) is attenuator out
    # umv(attrz,0) is full attenuation
    # gradually lower the angle of attrz to reduce attenuation
    # like: umv(attrz,-5), umv(attrz,-6), -90 is the lowest limit
    # to check the flux you do a fscan(rot,-90,0.25,181/0.25,0.01) avoid having more than a few spots at 65000 counts on the plot roi1_max vs rot
    

def goto_eiger_5um_n():
    sheh3.close()
    umv(s9hg,0.1,s9vg,0.1,aly,-8)
    umv(bigy,0,piny,0,pinz,0)
    umv(hlz,0.847,vly,-1.004)
    tfoh1.set(16,'Be')
    umv(attrz,0,atty,-10)
    newdataset('align')
    if abs(det2y.position)<0.1:
        moveto_eiger(scanbeamstop=True)
    sheh3.open()
        
def goto_eiger_1um_n():
    sheh3.close()
    umv(s9hg,0.1,s9vg,0.1,aly,0)
    umv(bigy,22,piny,0,pinz,0)
    umv(hlz,0.847,vly,-1.004)
    tfoh1.set(16,'Be')
    umv(attrz,0,atty,-10)
    newdataset('align')
    if abs(det2y.position)<0.1:
        moveto_eiger(scanbeamstop=True)
    sheh3.open()

def goto_eiger_05um_n():
    sheh3.close()
    umv(s9hg,0.05,s9vg,0.05,aly,-8)
    umv(bigy,22,piny,0,pinz,0)
    umv(hlz,0.847-1,vly,-1.004+1)
    tfoh1.set(16,'Be')
    umv(attrz,0,atty,-10)
    newdataset('align')
    #if abs(det2y.position)<0.1:
    #    moveto_eiger(scanbeamstop=True)
    sheh3.open()

class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        #if self.expotime < 0.005:
        #    ACTIVE_MG.disable('mca')
        #    print("Disabled mca: too fast scan")
        #    eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )


def half1(ymax,
          datasetname,
          ystep=5,
          ymin=-550,
          rstep=0.25,
          astart = -90,
          arange = 181,
          expotime=0.01):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)
    
    
def full1(ymax,
          datasetname,
          ystep=0.25,
          ymin=-1801,
          rstep=0.25,
          expotime=0.01):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.full1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.full1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    NR = int(361/rstep)
    umv(dty, max(ypos[0], ymin) )
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=( cleanup.axis.POS, ) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                rstart = next_rstart( rstep, angular_velocity )
                fscan2d( dty, ypos[i], ystep, 1, rot, rstart, rstep, 361/abs(rstep), expotime )
                # 7 min 53s with 25 scans == 19 seconds per scan
                
    umv(dty, 0)
    
    
def radios_horscan(samty_0, sample_size=22/2, horstep=0.5):

    umv(samty, samty_0 - sample_size)
    sct(0.1)

    for ii in range(int(2*sample_size/horstep)):
    
        umvr(samty, horstep)
        sct(0.1)

    
def radios_horscanx(samtx_0, sample_size=22/2, horstep=0.5):

    umv(samtx, samtx_0 - sample_size)
    sct(0.1)

    for ii in range(int(2*sample_size/horstep)):
    
        umvr(samtx, horstep)
        sct(0.1)




