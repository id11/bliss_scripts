
import time, numpy as np

# user_script_load("es1190.py")
user_script_load('optics.py')


# def check_cpm18(pos=6.4158):
def check_cpm18(pos=10.08):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()

def half1(ymax, datasetname, ystep=5, ymin=-1801, rstep=0.05):
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    ACTIVE_MG.disable("mca")
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, -90,  rstep, 181/rstep, 0.002 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot,  91, -rstep, 181/rstep, 0.002 )
    umv(dty, 0)

def full1(ymax, datasetname, ystep=10, ymin=-99901):
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    eiger.camera.auto_summation='OFF'
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.05, 7220, 0.002 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 361, -0.05, 7220, 0.002 )
    eiger.camera.auto_summation='ON'



def goto_basler():
    moveto_basler()                           # go to imaging camera
    umv(pinz, 0.3, piny, 17.32, bigy, 24)     # bigger beam
    umv(dty,0)
    umv(attrz, 0, atty, -10)                  # attenuator out

# tomo recipe
# user_script_load('tomo_by_ascan')
# user.tomo_by_ascan(0,360,3600,0.01,rot='rot', ymotor='ntz', ystep=-1)

    
    
def goto_focused():
    umv(pinz, -0.0152, piny, 0.03, bigy, 0.0048)
    umv(attrz, -10, atty, 0)


    
def thu_pre_dinner():
    #z0   = 52
    ymax = 10
    zz=(47,48)
    for z in zz:
        umv(pz, z)
        half1( ymax, f'z{z}_RT5', ystep=.2)

def thu_night():
    #z0   = 52
    ymax = 10
    for z in range(460,560,5):
        umv(pz, z/10.0)
        half1( ymax, f'z{z}_RT5', ystep=.2)


import time

def goto_temperature(t,r = 360, tol=0.2):
    print('send ramprate',r)
    ox700.ramprate = r
    sleep(2)
    print('send setpoint',t)
    ox700.setpoint = t
    while 1:
        time.sleep(2)
        tnow = ox700.input.read()
        terr = t - tnow
        print('tnow',tnow,'terr %.4f'%(terr))
        if ( abs( terr ) < tol ) and ( ox700.setpoint == t ):
            print('arrived at temperature')
            break
        if ox700.setpoint != t :
            ox700.setpoint = t
        
def fri_pre_dinner():
    #z0   = 52
    ymax = 10
    zz=(47,48)
    for z in zz:
        umv(pz, z)
        half1( ymax, f'z{z}_RT5', ystep=.2)

def half2(ymax, datasetname, ystep=5, ymin=-1801, rstep=0.05):
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    ACTIVE_MG.disable("mca")
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, -90,  rstep, 181/rstep, 0.004 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot,  91, -rstep, 181/rstep, 0.004 )
    umv(dty, 0)
    
def sat_early_morning():
    #z0   = 52
    ymax = 10
    zz=(68,65,66,67,69,70,71,72)
    for z in zz:
        umv(pz, z)
        half1( ymax, f'z{z}_160K', ystep=.2)

def mon_early_morning():
    #z0   = 52
    ymax = 25
    zz=range(40,60,4)
    for z in zz:
        umv(pz, z)
        half1( ymax, f'RT_z{z}_half', ystep=.1)




def run_f2():
    ACTIVE_MG.disable('mca')
    # from -15 to +1
    ystart = 15
    ystep = 0.1
    # goes from -15 to +1, then -1 to +18
    NY = (ystart + 1)/ystep
    rstep = 0.05 * 1.0000086500046697
    
    f2scan( rot,     0, 0.05,
            dty,   -ystart, ystep/7200,
            7200 * NY, 0.002,
            scan_mode='CAMERA')

    f2scan( rot,     0, 0.05,
            dty,    -1, ystep/7200,
            7200 * NY, 0.002,
            scan_mode='CAMERA') 




#ACTIVE_MG.add(ox700.counters.ox700_in)
#ACTIVE_MG.enable(ox700.counters.ox700_in.name)

def mon_evening():
    ymax = 22
    zz=[34, 36, 38]
    for z in zz:
        umv(pz, z)
        half1( ymax, f'120K_z{z}_half', ystep=.1)

def mon_night():
    ymax = 22
    zz=[29, 31, 33, 35]
    for z in zz:
        umv(pz, z)
        half1( ymax, f'140K_z{z}_half', ystep=.1)
