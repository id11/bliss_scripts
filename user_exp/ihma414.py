import sys
import numpy as np
import time
import os
    
atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
bigy = config.get('bigy')
u22 = config.get('u22')



def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = -5
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.3
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = 2.5
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 1.1
    dct_pars['slit_vg'] = 0.2
    dct_pars['dist'] = 215
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = -2.38
    dct_pars['shift_step_size'] = 0.18
    dct_pars['nof_shifts'] = 7
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars


def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = 1
    pct_pars['step_size'] = 0.1
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.08
    pct_pars['refon'] = pct_pars['num_proj']
#    pct_pars['ref_int'] = 10
    pct_pars['ref_step'] = -3
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 100
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = samtz
    pct_pars['samtz_cen'] = -2.36
    pct_pars['slit_hg'] = 1.5
    pct_pars['slit_vg'] = 1.5
    pct_pars['nof_shifts'] = 3
    pct_pars['shift_step_size'] = 1
    pct_pars['dist'] = 100
    pct_pars['abs_dist'] = 10
    pct_pars['scan_type'] = 'fscan'
    return pct_pars
    
def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 1
    ff_pars['step_size'] = 0.4
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 1.0
    ff_pars['slit_vg'] = 0.2
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = -2.36
    ff_pars['shift_step_size'] = 0.18
    ff_pars['nof_shifts'] = 5
    ff_pars['cpm18_detune'] = 0
    return ff_pars
    
def tdxrd_boxscan(scanname, ff_pars):
    print("using slit_vg %f and slit_hg %f" % (ff_pars['slit_vg'], ff_pars['slit_hg']))
    umv(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.05, s8hg, ff_pars['slit_hg'] + 0.05)
    newdataset(scanname)
    finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    
def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return
    
def pct_zseries(pct_pars, datasetname = 'pct'):
    shift_step_size = abs(pct_pars['shift_step_size'])
    if(pct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = pct_pars['samtz_cen'] - (pct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(pct_pars['samtz_cen']))
    if(offset_samtz_pos < pct_pars['zmot'].low_limit or offset_samtz_pos + (pct_pars['nof_shifts'] - 1) * shift_step_size > pct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(pct_pars['nof_shifts'])):
        umv(pct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), pct_pars)
    umv(pct_pars['zmot'], pct_pars['samtz_cen'])
    print('PCT Z-series Succeed')
    return
    
def ff_zseries(ff_pars, datasetname = 'ff'):
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tdxrd_boxscan(f'{datasetname}{iter_i+1}', ff_pars)
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    print('FF Z-series Succeed')
    return('Succeed')
    
def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    ct(pars['exp_time'])
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.2, s8hg, pars['slit_hg'] + 0.2)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
            ct(pars['exp_time'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])
    
def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)
    
def pct_abs_series(pars):
    
    umv(nfdtx, pars['dist'])
    pct_exp_time = pars['exp_time']
    pct_zseries(pars)
    
    umv(nfdtx, pars['abs_dist'])
    pars['exp_time'] = 0.06
    pct_zseries(pars, datasetname='abs')
    pars['exp_time'] = pct_exp_time
    umv(nfdtx, pars['dist'])
    
    
def att_out():
    fsh.close()
    try:
        assert ffdtz1.position > 400
        print('Safe to remove attenuators')
    except AssertionError:
        print('Check if the FF detector is out of the beam')
        return
    umv(atty, -10, attrz, 0)
    
def att_in():
    umv(atty, 0, attrz, -4)

def marana_out():
    umv(d3ty, 180)

def ff_in(pars):
    sheh3.close()
    att_in()
    ff_slits(pars)
    try:
        assert atty.position == 0 and d3ty.position > 179
    except AssertionError:
        print('Check attenuator or nfdtx!')
        return
    umv(ffdtz1, 0)
    ACTIVE_MG.disable('frelon*')
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.enable('frelon3*')
    
def ff_calibrant(pars):
    att_in()
    ff_slits(pars)
    umv(attrz, -10)
    newdataset('CeO2')
    finterlaced(diffrz, 0, 0.8, 360/0.8, 0.08)
    umv(attrz, -4)
    
    
def ff_slits(pars):
    umv(s7hg, pars['slit_hg'], s7vg, pars['slit_vg'], s8hg, pars['slit_hg'] + 0.05, s8vg, pars['slit_vg'] + 0.05) 
    
def tomo_slits(pars):
    umv(s7hg, pars['slit_hg'], s7vg, pars['slit_vg'], s8hg, pars['slit_hg'] + 0.1, s8vg, pars['slit_vg'] + 0.1) 
    
    
def marana_in():
    try:
        assert ffdtz1.position > 450
    except AssertionError:
        print("Won't move the marana, risk of colision ! Check inside !")
    umv(s7hg, 1.4, s7vg, 1.4, s8hg, 1.5, s8vg, 1.5)
    umv(d3ty, 0)
    ACTIVE_MG.disable('frelon*')
    ACTIVE_MG.enable('marana:image')
    ct(0.01)
    plotimagecenter()
    
def ff_out():
    sheh3.close()
    try:
        assert d3ty.position > 179
    except AssertionError:
        print('Check d3 position ! Might be dangerous to move !')
        return
    umv(ffdtz1, 500)
    att_out()
     

