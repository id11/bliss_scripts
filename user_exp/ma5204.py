import numpy as np, time
from scipy.optimize import curve_fit




    
def findside(x, y):
    y = y-y.min()
    cm = (x*y).sum()/y.sum()
    ny = np.clip(y, 0.2*y.max(), 1*y.max()) - (0.2*y.max()) 
    ny = ny - (ny.max()/2)
    e1 = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    e2 = np.interp(0, -ny[np.argmax(y):], x[np.argmax(y):])
    return e1, e2

def linefit(x, a, b):
    return  np.abs((a*x) + b)   
    
def align_r(rrange,rsteps,samtzrange,samtzsteps,ctim, ctr='frelon3:roi_counters:roi3_avg'):
    raxis = samrx
    rcen = raxis.position
    ws = []
    rvals = np.linspace(-rrange,rrange,rsteps)
    for r in rvals:
        umv(raxis, rcen+r)
        s = dscan(samtz,-samtzrange,samtzrange,samtzsteps,ctim)
        fe = findside( s.get_data('samtz'), s.get_data(ctr) )
        ws.append(fe[1]-fe[0])
    p, _ = curve_fit(linefit, rvals[:2], ws[:2], (0,0))
    print(p)
    p, _ = curve_fit(linefit, rvals, ws, (p[0],-10))
    print(p)
    rideal = -p[1]/p[0]
    if np.abs(rideal) < rrange:
        umv(raxis,rcen + rideal)
    return rcen + rideal

def align_ry(rrange,rsteps,samtzrange,samtzsteps,ctim, ctr='frelon3:roi_counters:roi3_avg'):
    raxis = samry
    rcen = raxis.position
    ws = []
    rvals = np.linspace(-rrange,rrange,rsteps)
    for r in rvals:
        umv(raxis, rcen+r)
        s = dscan(samtz,-samtzrange,samtzrange,samtzsteps,ctim)
        fe = findside( s.get_data('samtz'), s.get_data(ctr) )
        ws.append(fe[1]-fe[0])
    p, _ = curve_fit(linefit, rvals[:2], ws[:2], (0,0))
    print(p)
    p, _ = curve_fit(linefit, rvals, ws, (p[0],-10))
    print(p)
    rideal = -p[1]/p[0]
    if np.abs(rideal) < rrange:
        umv(raxis,rcen + rideal)
    return rcen + rideal


def pdf_scan(temperature):
        newdataset('insitu_after_realign_%d_deg'%(temperature))
        for i in range (0, 10, 1):
            loopscan(100,0.2)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def rt_scan_back():
    nanodacpool.setpoint=25
    while nanodacpool.is_ramping():
        print('Temperature is',nanodacpool_temp.read(),'C')
        sleep(5)
    newdataset('zscans_T25_back_no_kapton_2')
    plotselect('frelon3:roi_counters:roi3_avg')
    dscan(samtz,-0.05,0.05,40,0.1)
    goto_peak()
    dscan(samtz,-0.005,0.005,40,0.1)
    goto_cen()
    umvr(samtz, -0.001)
    newdataset('insitu_25_deg_back_no_kapton_2')
    loopscan(1000,0.2)
    


         
            
def measure_Si(temp):
    ly = config.get('ly')
    #moves attenuator in
    umv(ly, -12)
    print('attenuator is in no worries low intensities')
    newdataset('Silicon_calib2_%d_deg'%(temp))
    finterlaced(diffrz, -90, 1 ,180,0.5)
    umv(ly, 0)
    print('attenuator is out ready to collect data')
    
def Si_temp_scan():
    temps=np.arange(25, 726, 50)
    for temp in temps:
        nanodacpool.setpoint=temp
        while nanodacpool.is_ramping():
            print('Temperature is',nanodacpool_temp.read(),'C')
            sleep(5)
        # high temperature scan for thermal expansion
        measure_Si(temp)
    nanodacpool.setpoint=25
    sheh3.close()
    print('shutter is closed you can go in the hutch to open the air')
    

def temperature_scan():
    z0 = -2.07400# da cambiare ogni volta
    diffrz0 = 88.18
    nscan = 45
    '''
    nanodacpool.setpoint =25
    newdataset('zscans_T25')
    plotselect('frelon3:roi_counters:roi3_avg')
    dscan(samtz,-0.05,0.05,40,0.1) # scan spessi
    #dscan(samtz,-0.05,0.05,40,0.1)  # scan sottili
    goto_peak()
    dscan(samtz,-0.005,0.005,40,0.1)
    #dscan(samtz,-0.005,0.005,80,0.1)
    goto_cen()
    newdataset('insitu_25_deg')
    for i in range (0, 10, 1):
            loopscan(100,0.2)'''
    
   
    temps=np.arange(670, 671, 1)
    for temp in temps:
        #umv(samtz,z0)
        newdataset('zscans_T%d'%(temp))
        nanodacpool.setpoint=temp
        while nanodacpool.is_ramping():
            print('Temperature is',nanodacpool_temp.read(),'C')
            sleep(5)
        # high temperature scan for thermal expansion
        plotselect('frelon3:roi_counters:roi3_avg')
        dscan(samtz,-0.1,0.06,150,0.1)
        #dscan(samtz,-0.1,0.05,200,0.1)
        goto_peak()
        dscan(samtz,-0.005,0.005,60,0.1)
        #dscan(samtz,-0.005,0.005,100,0.1)
        goto_cen()
        for i in range (0, nscan, 1):
            newdataset('zscans_T%d'%(temp))
            dscan(samtz,-0.005,0.005,60,0.1)
            #dscan(samtz,-0.005,0.005,120,0.1)
            goto_cen()
            pdf_scan( temp )
        umvr(samtz,0.1)
        measure_Si(temp)
        umv(diffrz, diffrz0)
        nanodacpool.setpoint= 25
        while nanodacpool.is_ramping():
            print('Temperature is',nanodacpool_temp.read(),'C')
            sleep(5)
        newdataset('insitu_25back')
        dscan(samtz,-0.1,0.06,150,0.1)
        #dscan(samtz,-0.1,0.05,200,0.1)
        goto_peak()
        dscan(samtz,-0.005,0.005,60,0.1)
        #dscan(samtz,-0.005,0.005,100,0.1)
        goto_cen()
        for i in range (0, 10, 1):
            loopscan(100,0.2)
        print("Non leggerai mai questo messaggio perche se leggi questo messaggio la misura e andata a buon fine")

    
    
    


    

    
