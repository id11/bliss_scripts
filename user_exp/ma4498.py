import numpy as np
import time


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']
       

def tomoin():
    ACTIVE_MG.enable("frelon1:image") 
    ACTIVE_MG.disable("frelon3:image")
    sheh3.close()
    #umv(ffdtx1, 500)
    umv(ffdtz1, 100, d1ty, 0)
    sheh3.open()
    print("ready to collect tomo data") 

def frelonin():
    ACTIVE_MG.disable("frelon1:image") 
    ACTIVE_MG.enable("frelon3:image") 
    sheh3.close()
    umv(d1ty,-80, ffdtz1, 0)
    #umv(ffdtx1, 240)
    sheh3.open()
    print("ready to collect diffraction data") 

def crlin():
    bigy=config.get('bigy')
    tfoh1 = config.get("tfoh1")
    umv(bigy, 0)
    tfoh1.set(18,"Be") 
    
def crlout():
    bigy=config.get('bigy')
    tfoh1 = config.get("tfoh1")
    umv(bigy, 80)
    tfoh1.set(0,"Be")
    print("done")


def powder():
    frelonin()
    crlout()
    ascan(diffrz, 0, 180, 90, 0.1)
       
    
def difftyfrelon():
    # newproposal("ma4498")
    # fpico4
    diffty_airpad.on()
    pico4.auto_range = False
    pico4.range=2.1e-5
    
    ACTIVE_MG.disable_all()
    ACTIVE_MG.enable("frelon3:roi*")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("*fpico4")
    
    y0 = 14.625
    width = 0.45
    step  = 0.005
    #24 minutes

    for my_y  in np.arange( y0-width, y0+width+step/10., step):
        umv(diffty, my_y)
        #pause_for_refill(30)
        finterlaced(diffrz,   0,  1, 180, 0.08, mode="ZIGZAG") 
        #diffty.reset_closed_loop()
    
    diffty_airpad.off()


def hydrascan():
    for hs,my_s in enumerate(np.arange(0,30,1)):
        newdataset("cerbonio_hydroscan_night_%03d"%(hs))
        frelonin()       
        crlin()
        difftyfrelon()
        crlout()
        powder()
        tomoin()
        umv(diffty, 14.665)
        tomo.half_turn_scan() 

def startscan():
    diffty.reset_closed_loop()
    newdataset("Bassanite2_start")
    frelonin()
    crlout()
    powder()
    crlin()
    difftyfrelon()
    crlout()
    tomoin()
    umv(diffty, 14.70)
    tomo.half_turn_scan() 

def checkpowder ():
    newdataset("cerbonio_powder1")
    ascan(diffrz, 0, 180, 90, 0.1)
    newdataset("cerbonio_powder2")
    ascan(diffrz, 180, 0, 90, 0.1)
    newdataset("cerbonio_powder3")
    ascan(diffrz, 0, 180, 90, 0.1)
    newdataset("cerbonio_powder4")
    ascan(diffrz, 180, 0, 90, 0.1)
    newdataset("cerbonio_powder5")
    ascan(diffrz, 0, 180, 90, 0.1)
    newdataset("cerbonio_powder6")
    ascan(diffrz, 180, 0, 90, 0.1)
    newdataset("cerbonio_powder7")
    ascan(diffrz, 0, 180, 90, 0.1)
    newdataset("cerbonio_powder8")
    ascan(diffrz, 180, 0, 90, 0.1)
    newdataset("cerbonio_powder9")
    ascan(diffrz, 0, 180, 90, 0.1)
    newdataset("cerbonio_powder10")
    ascan(diffrz, 180, 0, 90, 0.1)
    newdataset("cerbonio_powder11")
    ascan(diffrz, 0, 180, 90, 0.1)
    newdataset("cerbonio_powder12")
    ascan(diffrz, 180, 0, 90, 0.1)
    newdataset("cerbonio_powder13")
    ascan(diffrz, 0, 180, 90, 0.1)
    newdataset("cerbonio_powder14")
    ascan(diffrz, 180, 0, 90, 0.1)
    newdataset("cerbonio_powder15")
    ascan(diffrz, 0, 180, 90, 0.1)
    newdataset("cerbonio_powder16")
    ascan(diffrz, 180, 0, 90, 0.1)
  

def morningscan():
    newdataset("cerbonio_hydroscan_morning_tomo")
    crlout()
    tomoin()
    umv(diffty, 14.665)
    tomo.half_turn_scan()  
    newdataset("cerbonio_hydroscan_morning_powder")    
    frelonin() 
    powder()     
    crlin()
    newdataset("cerbonio_hydroscan_morning_difftomo")
    difftyfrelon()
    


def C3Sscan():
    newsample("CEM_0p3pct_6min_difftomo") 
    #newdataset("single")
    #sct(0.25)
    #pico6.auto_range = False
    #pico6.range=2e-6
    width = 15
    step  = 1
    y0 = 0.
    ypos = np.arange(y0-width, y0+width+step/10, step)
    ACTIVE_MG.enable("eiger*")
    eiger.camera.photon_energy=73900.
    newdataset("difftomo")
    plotselect("eiger:roi1_avg")
    for my_y in ypos:
            umv(dty, my_y)
            fscan(rot, 0, 1, 360, 1.2)
            umv(dty, my_y+step/2)
            fscan(rot, 360, -1, 360, 1.2)



def hydratomo():
    for hs,my_s in enumerate(np.arange(1,300,1)):
        print("this is scan_%03d"%(hs)) 
        newdataset("hydrotomo_shake_%03d"%(hs))
        tomo.half_turn_scan() 
        print("now sleeping zzzZzzzzzz....")  
        sleep(300)
    
   
        

def collect():
    diffty_airpad.on()
    difftyfrelon()
    diffty_airpad.off()
    sheh3.close()

 



def cathyfrelon():
    # newproposal("ma4498")
    # fpico4
    
    pico4.auto_range = False
    pico4.range=2.1e-5
    
    ACTIVE_MG.disable_all()
    ACTIVE_MG.enable("frelon3:roi*")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("*fpico4")
    
    y0 = 14.63
    width = 0.9
    step  = 0.005
    #24 minutes

    for my_y  in np.arange( y0-width, y0+width+step/10., step):
        umv(diffty, my_y)
        #pause_for_refill(30)
        finterlaced(diffrz,   0,  1, 180, 0.3, mode="ZIGZAG") 
        #diffty.reset_closed_loop()




def Ni18_zscan():
    # newproposal("ma4498")
    # fpico4
 
    pico4.auto_range = False
    pico4.range=2.1e-5
    
    ACTIVE_MG.disable_all()
    ACTIVE_MG.enable("frelon3:roi*")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("*fpico4")
    
    z0 = -0.66
    width = 3
    step  = 0.05
    

    for my_z  in np.arange( z0, z0+width+step/10., step):
        umv(samtz, my_z)
        #pause_for_refill(30)
        finterlaced(diffrz,   0,  1, 360, 0.08, mode="ZIGZAG") 
        #diffty.reset_closed_loop()
    
    diffty_airpad.off()
















    
    
  
