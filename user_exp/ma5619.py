import time
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def vertical_scan(ctime,start_hz,end_hz,target_temp,ramprate = 10,time_palier = 1800):
    nanodacpool_induction.ramprate = ramprate
    nanodacpool_induction.setpoint = target_temp
    newdataset('vertical_scan_hz_%s_%s'%(start_hz,end_hz))
    while nanodacpool_induction.is_ramping():
        start_time = time.time()
        ascan(hz,start_hz,end_hz,4,ctime)
        umv(hz,start_hz)
        #sleep(-time.time()+start_time+30)
    time_after_ramp = time.time()
    while (time.time() - time_after_ramp) < time_palier:
        start_time = time.time()
        ascan(hz,start_hz,end_hz,4,ctime)
        umv(hz,start_hz)
        #sleep(-time.time()+start_time+30)
    nanodacpool_induction.ramprate = 25
    nanodacpool_induction.setpoint = 25
    #sleep(30)
    while nanodacpool_induction.is_ramping():
        start_time = time.time()
        ascan(hz,start_hz,end_hz,4,ctime)
        umv(hz,start_hz)
        #sleep(-time.time()+start_time+30)


def vertical_scan_pdf_fixed(ctime,target_temp,ramprate = 10):
    sheh1.open()
    nanodacpool_induction.ramprate = ramprate
    nanodacpool_induction.setpoint = target_temp
    newdataset('loopscan')
    loopscan(5*60*60,ctime)


def vertical_scan_pdf(ctime,start_hz,end_hz,target_temp,ramprate = 10,time_palier = 1800):
    nanodacpool_induction.ramprate = ramprate
    nanodacpool_induction.setpoint = target_temp
    newdataset('vertical_scan_hz_%s_%s'%(start_hz,end_hz))
    while nanodacpool_induction.is_ramping():
        start_time = time.time()
        ascan(hz,start_hz,end_hz,4,ctime)
        umv(hz,start_hz)
    time_after_ramp = time.time()
    while (time.time() - time_after_ramp) < time_palier:
        start_time = time.time()
        ascan(hz,start_hz,end_hz,4,ctime)
        umv(hz,start_hz)
    nanodacpool_induction.ramprate = 25
    nanodacpool_induction.setpoint = 25
    while nanodacpool_induction.is_ramping():
        start_time = time.time()
        ascan(hz,start_hz,end_hz,4,ctime)
        umv(hz,start_hz)
  
def capillary_scan():
    newsample('rack2')
    sample_names = ['LaB6','AG188','Ti3AlC2','AS04','CI1038_1','CI1021_1','CI1038_5','CI1021_5']
    diffy_positions = [50.95,45.93,41.41,36.89,31.68,26.72,21.63,16.80] 
    sheh1.open()
    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(1*60,1)
        
def pdf_capillary_scan():
    newsample('rack3')
    sample_names = ['AG_187','CI1021_5','CI1038_5','CI1038_1','AS_04','AG_188','LaB6']
    diffy_positions = [6.25,16.61,21.7,31.68,36.95,46.05,51.07] 
    sheh1.open()
    for y,sample in enumerate(sample_names):
        pause_for_refill(60)
        newdataset(sample)
        umv(diffy,diffy_positions[y])
        loopscan(20*60,1)
