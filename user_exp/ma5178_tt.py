import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime


def test_loop_grains(grain_list):
    """Loops through a list of Topotomo grains  and performs refinement + TT scan_acquisition
    :param dict grain_list:  list of grain structures containing Topotomo alignment values
    :param dict tt_pars: Topotomo scan parameters
    :param int step: running number of load step in load-sequence  
    """
    for i in range(len(grain_list)):
        grain_list[i]['toto']=0
    return(grain_list)
    



def read_tt_infos(gid):
    data_dir = '/data/visitor/ma5178/id11/DCT_Analysis/M4b'
    data_path = os.path.join(data_dir, 'grain_%04d.mat' % gid)
    data = loadmat(data_path)
    t = data['out'][0][0][0][0]
    values = t[0][0][0][0][0]
    d = {}
    d['gr_id'] = int(values[0][0][0])
    # double check that the grain id is right
    if d['gr_id'] != gid:
        raise(ValueError('grain id mismatch, check your matlab output in file %s' % data_path))
    d['nfdtx'] = float(values[1][0][0])
    d['d3tz'] = float(values[2][0][0])
    d['diffry'] = float(values[3][0][0])
    d['samrx'] = float(values[4][0][0])
    d['samry'] = float(values[5][0][0])
    d['samtx'] = float(values[6][0][0])
    d['samty'] = float(values[7][0][0])
    d['samtz'] = float(values[8][0][0])
    d['samrx_offset'] = float(values[9][0][0])
    d['samry_offset'] = float(values[10][0][0])
    d['samtx_offset'] = float(values[11][0][0])
    d['samty_offset'] = float(values[12][0][0])
    d['samtz_offset'] = float(values[13][0][0])
    d['diffrz_offset'] = float(values[14][0][0])
    d['int_factor'] = float(values[15][0][0])
    print(d)
    return d
    
ly = config.get('ly')
lfyaw = config.get('lfyaw')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
bigy = config.get('bigy')
u22 = config.get('u22')
cpm18_tuned = 10.09
u22_tuned = 8.306
dct_dist = 7
pct_dist = 50
samtx_offset = 0
samty_offset = 0
samtz_offset = 0
samrx_offset = -1.4
samry_offset = -0.13
diffty_offset = 13.7517
ff_offset = 350
tt_offset = 0
tt_dist = 11
ff_z = 100
ff_dist = 150
d1_out = -176
d2_out = -380
d2_in = 0
d3_out = 180
d3tz_pos_dct = 0
d3tz_pos_tt = -1.5
bigy_in = -0.233
bigy_out = 22


def sam_dct_pos():
    umv(samrx, samrx_offset, samry, samry_offset, diffry, 0)
    umv(samtx, samtx_offset, samty, samty_offset, samtz, samtz_offset)

def attin():
    umv(atty,-4)


def lyout():
    umv(ly, 0)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']

def frelon1in():
    umv(d3ty, d3_out)
    umv(ffdtx1, dct_dist + ff_offset, nfdtx, dct_dist, ffdtz1, ff_z)
    tfoh1.set(0,'Al')
    tfoh1.set(16,'Be')
    for i in range (3):
        umv(tfz, -0.319, tfy, 14.55)
    lyout();
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon3*")
    ACTIVE_MG.enable("frelon1:image")
    umv(diffrz, 0)
    umv(d1ty, 0, d1tz, 0, d2ty, d2_in)
    print("ready to collect dct data")
    
def ffin():
    sheh3.close()
    tfoh1.set(0,'Be')
    umv(bigy, bigy_out)
    sam_dct_pos()
    lyin();
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon16:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out, d2ty, d2_out)
    umv(ffdtx1, ff_dist, nfdtx, tt_dist, ffdtz1, 0)
    print("ready to collect far-field data")
    sheh3.open()
 
def scanning_ff():
    umv(bigy, bigy_in)
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon16:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out, d2ty, d2_out, d1ty, d1_out)
    umv(ffdtx1, ff_dist, nfdtx, dct_dist, ffdtz1, 0)
    print("ready to collect far-field data")

def marana_pct(pct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    marana.image.roi=[0,0,2048,2048]
    sct(pct_pars['exp_time'])
    umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'])


def maranain(dist):
    sheh3.close()
    lyout();
    umv(bigy, bigy_out)
    ACTIVE_MG.enable("marana:image")
    ACTIVE_MG.disable("frelon16*") 
    ACTIVE_MG.disable("frelon3*")
    sam_dct_pos()
    umv(ffdtx1, dist + ff_offset, nfdtx, dist)
    umv(d2ty, d2_out, d3ty, 0, ffdtz1, 100, d3tz, d3tz_pos_dct)
    print("ready to collect marana data") 
    sheh3.open()
    ct(0.02)

def marana_large_beam(pct_pars):
    tfoh1.set(64,'Al')
    tfoh1.set(0,'Be')
    #for i in range (3):
    #    umv(tfz, -0.46, tfy, 14.488)
    marana.image.roi=[512,0,1024,2048]
    sct(0.05)
    umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'], nfdtx, pct_pars['dist'], ffdtx1, pct_pars['dist'] + ff_offset)
    umvct(s8vg, pct_pars['slit_vg'] + 0.5, s8hg, pct_pars['slit_hg'] + 0.5)
    umvct(samtz, samtz_offset)
    ct(pct_pars['exp_time'])

def marana_dct(dct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(10,'Be')
    #for i in range (3):
    #    umv(tfz, -0.32, tfy, 14.445)
    marana.image.roi=[0,0,2048,2048]
    sct(dct_pars['exp_time'])
    umvct(s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umvct(s8hg, dct_pars['slit_vg'] + 0.5, s8hg, dct_pars['slit_hg'] + 0.5)
    umvct(nfdtx, dct_pars['dist'], ffdtx1, dct_pars['dist'] + ff_offset, d3tz, d3tz_pos_dct)


def frelon16in(theta, tt_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    #for i in range (3):
    #    umv(tfz, -0.39, tfy, 14.51)
    lyout();
    ACTIVE_MG.disable("marana:image")
    ACTIVE_MG.disable("frelon3*") 
    ACTIVE_MG.enable("frelon16:image")
    frelon16.image.flip=[False, True]
    d2tzpos = np.tan(2*np.deg2rad(theta))*tt_dist
    print("moving nfdtx, ffdtx1 and ffdtz1 to save positions")
    umvct(nfdtx, tt_pars['dist'], ffdtx1, tt_pars['dist'] + ff_offset, ffdtz1, 200)
    print("moving d2tz to target position %g"%d2tzpos)
    umvct(d2tz, d2tzpos)
    umv(d2ty, 0, d3ty, d3_out)
    ct(0.1)
    print("ready to collect topotomo data")


def read_positions():
    g = {}   
    g['samrx']=samrx.position
    g['samry']=samry.position
    g['samtx']=samtx.position
    g['samty']=samty.position
    g['samtz']=samtz.position
    g['d2tz']=d2tz.position
    return g

def topotomo_tilt_grain(gr_id):
    """drives diffractometer to grain alignment position for
    """
    topotomo_params = read_tt_infos(gr_id)
    print(topotomo_params)
    if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
        return('Error')
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d3tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')

def topotomo_tilt_grain_json(gr_id):
    """drives diffractometer to grain alignment position for
    """
    topotomo_params = get_json_grain_info(gr_id)
    print(topotomo_params)
    if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
        return('Error')
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d2tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')
    
def topotomo_tilt_grain_dict(topotomo_params):
    """drives diffractometer to current grain alignment position
    """
    print(topotomo_params)
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d2tz, topotomo_params['d3tz'])
    umv(diffrz, 90) #save position to change tilts for the diffractometer with Nanox during ma5178...
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')

# -*- coding: utf-8 -*-
import os
import numpy as np



def define_tt_pars():
    tt_pars={}
    tt_pars['diffrz_step'] = 5
    tt_pars['num_proj'] = 360 / tt_pars['diffrz_step']
    tt_pars['diffry_step'] = 0.05
    tt_pars['exp_time'] = 0.05
    tt_pars['search_range'] = 0.4
    tt_pars['scan_mode'] = 'CAMERA'
    tt_pars['image_roi'] = [0, 0, 2048, 2048] #[480, 480, 960, 960]
    tt_pars['counter_roi'] = [812, 1231, 466, 800] #[812, 1531, 466, 500]
    tt_pars['slit_hg'] = 0.7
    tt_pars['slit_vg'] = 1.4
    tt_pars['dist'] = 11
    return tt_pars
    
def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.05
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.05
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -1.5
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.7
    dct_pars['slit_vg'] = 0.6
    dct_pars['dist'] = 6
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 0
    dct_pars['shift_step_size'] = 0
    dct_pars['nof_shifts'] = 1
    return dct_pars

def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = 0
    pct_pars['step_size'] = 0.4
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.2
    pct_pars['refon'] = pct_pars['num_proj']
    pct_pars['ref_step'] = -3.5
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 41
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = samtz
    pct_pars['slit_hg'] = 1.7
    pct_pars['slit_vg'] = 3.5
    pct_pars['dist'] = 50
    return pct_pars
    
def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 1
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.8
    ff_pars['slit_vg'] = 0.6
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = 0
    ff_pars['shift_step_size'] = 0
    ff_pars['nof_shifts'] = 1
    ff_pars['cpm18_detune'] = 0.15
    return ff_pars
    
def define_sff_pars():
    sff_pars={}
    sff_pars['start_pos'] = 0
    sff_pars['step_size'] = 1
    sff_pars['num_proj'] = 180 / sff_pars['step_size']
    sff_pars['exp_time'] = 0.08
    sff_pars['slit_hg'] = 0.1
    sff_pars['slit_vg'] = 0.1
    sff_pars['mode'] = 'ZIGZAG'
    sff_pars['cpm18_detune'] = 0.1
    return sff_pars

def load_grains_from_matlab(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = read_tt_infos(gr_id)
        grain_list.append(grain)
    return grain_list
    
def load_grains_from_json(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = get_json_grain_info(gr_id)
        grain_list.append(grain)
    return grain_list

def load_ramp_by_ascan(start, stop, npoints, exp_time, loadstep, pct_pars):
    marana_large_beam(pct_pars)
    newdataset('loadramp_%d' %  loadstep)
    ascan(stress, start, stop, npoints, exp_time, stress_cnt, marana)
    return

def sff_one_grain(scanname, sff_pars, grain):
    user.cpm18_goto(cpm18_tuned + sff_pars['cpm18_detune'])
    umv(s7vg, 0.5, s7hg, 0.5, s8vg, 0.05, s8hg, 0.05) 
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, bigy_in)
    umv(samtx, grain['samtx'], samty, grain['samty'], samtz, grain['samtz'])
    stage_diffty_zero = diffty_offset

    scanstarty= -0.1  # in mm
    scanendy=0.101
    scanstepy=0.01
    
    scanstartz= -0.15  # in mm
    scanendz=0.1501 
    scanstepz=0.01    

    umvr(samtz, scanstartz)
    z_scans_number = (scanendz - scanstartz) / scanstepz
    newdataset(scanname)
    for i in range(int(z_scans_number)):
        umvr(samtz, scanstepz)
        for ty in np.arange(scanstarty,scanendy,scanstepy):
            umv(diffty, stage_diffty_zero+ty)
            print(f"Scan pos: y: {ty} couche {i}")
            finterlaced( diffrz, sff_pars['start_pos'], acq_size = sff_pars['step_size'], npoints = sff_pars['num_proj'], acq_time = sff_pars['exp_time'], mode = sff_pars['mode'])

    umv(diffrz,0)      
    umv(diffty, stage_diffty_zero)
    umv(bigy, bigy_out)
    diffty_airpad.off()
    user.cpm18_goto(cpm18_tuned)
    
    umv(samtz,0)
    print("All done!")
    

def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def load_ramp_by_target(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    write_to_file = to_file if to_file is not None else sys.stdout
    marana_large_beam(pct_pars)
    current_load = stress_cnt.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return


def define_pars(load_list):
    """Produces default input parameters for a load sequence (see next function)
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    for load in load_list:
        step_pars['dct_pars'] = define_dct_pars()
        step_pars['pct_pars'] = define_pct_pars()
        step_pars['tt_pars'] = define_tt_pars()
        step_pars['ff_pars'] = define_ff_pars()
        step_pars['sff_pars']= define_sff_pars()
        step_pars['target'] = load
        step_pars['load_step'] = step
        step = step + 1
        par_list.append(step_pars)        
    return par_list

def define_pars_one_load():
    """Produces default input parameters for one load
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    step_pars['dct_pars'] = define_dct_pars()
    step_pars['pct_pars'] = define_pct_pars()
    step_pars['tt_pars'] = define_tt_pars()
    step_pars['ff_pars'] = define_ff_pars()
    step_pars['sff_pars']= define_sff_pars()
    step_pars['target'] = load
    step_pars['load_step'] = step
    par_list.append(step_pars)
    return par_list


def load_sequence(par_list, grain_list):
    """Load Experiment Master Sequence.
    
    This function will perform scan sequences (pct, dct, tt) for a list of target load values defined in "par_list" 
    Note: grain_list is currently produced by a matlab script and can be re-created / imported via:  grain_list = user.read_tt_info([list_of_grain_ids])
    par_list can be created via: par_list = user.define_pars()  and sub-parameters for dct, pct, tt can be adapted to cope with increasing mosaicity

    """
    for step_pars in par_list:
        dct_pars = step_pars['dct_pars']
        pct_pars = step_pars['pct_pars']
        tt_pars = step_pars['tt_pars']
        ff_pars = step_pars['ff_pars']
        sff_pars = step_pars['sff_pars']
        target = step_pars['target']
        load_step = step_pars['load_step']
        grain_list = load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, ff_pars, sff_pars, target, load_step)
    return grain_list

def load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, ff_pars, sff_pars, target, loadstep):
    """Performs a loadramp to the new target value and launches PCT, DCT and a series of TT scans at this new target load
    """
    ## PCT
    maranain(pct_pars['dist'])
    marana_large_beam(pct_pars)
    #load_ramp_by_target(target)
    scan_name = 'pct_%dN_' % target
    tomo_by_fscan_dict(scan_name, pct_pars)
    ## DCT
    marana_dct(dct_pars)
    scan_name = 'dct_%dN_' % target
    dct_zseries(dct_pars, datasetname = scan_name)
    ## TT
    scan_name = 'tt_%dN_' % target
    marana_tt(grain_list, tt_pars, target)
    ##frelon16in(4, tt_pars)
    ##grain_list = loop_grains(grain_list, tt_pars, target)
    ## 3DXRD
    ffin()
    scan_name = 'ff_%dN_' % target    
    ff_zseries(ff_pars, scan_name)
    ## s-3DXRD
    #scan_name = 'sff_%dN_g%d' % (target, grain_list[0]['gr_id'])
    #tdxrd_pointscan(scan_name, sff_pars, grain)
    scan_name = 'sff_2d_%dN_g%d' % (target, grain_list[0]['gr_id'])
    sff_one_grain(scan_name, sff_pars, grain_list[0])
    return grain_list


def loop_grains(grain_list, tt_pars, target):
    """Loops through a list of Topotomo grains  and performs refinement + TT scan_acquisition
    :param dict grain_list:  list of grain structures containing Topotomo alignment values
    :param dict tt_pars: Topotomo scan parameters
    :param int step: running number of load step in load-sequence  
    """
    tfoh1.set(16,'Be')
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    marana.image.roi = tt_pars['image_roi'] # [510, 510, 900, 900]
    marana.roi_counters.set('roi1', tt_pars['counter_roi'])  #[300, 300, 300, 300])
    ACTIVE_MG.enable('marana:roi_counters:roi1_avg')
    for i in range(len(grain_list)):
        newdataset('grain_%04d_%dN_' % (grain_list[i]['gr_id'], target))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time)
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'])
        sleep(1)
        #update_grain_info(grain_list[i])
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    sam_dct_pos()
    return grain_list

def marana_tt(grain_list, tt_pars, target):
    tfoh1.set(0,'Be')
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    umv(d3tz, d3tz_pos_tt, nfdtx, tt_pars['dist'])
    marana.image.roi = tt_pars['image_roi']
    marana.roi_counters.set('roi1', tt_pars['counter_roi'])
    ACTIVE_MG.enable('marana:roi_counters:roi1_avg')
    for i in range(len(grain_list)):
        newdataset('grain_%04d_%dN_' % (grain_list[i]['gr_id'], target))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time, tt_pars['scan_mode'])
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'], tt_pars['scan_mode'])
        sleep(1)
        update_grain_info(grain_list[i])
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    sam_dct_pos()
    return grain_list

        
def refine_tt_alignment(tt_grain, ang_step=0.05, search_range=0.5, exp_time=0.1, scan_mode = 'CAMERA'):
    """Refine the topotomo samrx, samry alignment.
    
    This function runs 4 base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use (in degrees).
    :param float search_range: the base tilt angular search range in degrees.
    """
     # half base tilt range in degree
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/visitor/ma5178/id11/'
    sample_name = 'M4b'
    
    # open a script to gather all acquisition commands
    print(tt_grain)
    gid = tt_grain['gr_id']
    print('find range for grain %d\n' % gid)
    # check that diffry is negative (Bragg alignment)
    #if tt_grain['diffry'] > 0:
    #    raise(ValueError('diffry value should be negative, got %.3f, please check your data' % tt_grain['diffry']))
        
    # align our grain
    topotomo_tilt_grain_dict(tt_grain)

    # define the ROI automatically as [710, 710, 500, 500]
    #frelon16.image.roi = [0, 0, 1920, 1920]
    #ct(0.1)
    #frelon16.roi_counters.set('roi1', [710, 710, 500, 500])
    #ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    #exp_time = exp_time / tt_grain['int_factor']
    # first scan at 0 deg
    umvct(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_000, end_angle_000, cen_angle_000 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # second scan at 180 deg
    umv(diffrz, 180)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_180, end_angle_180, cen_angle_180 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samry by half the difference
    samry_offset = 0.5 * (cen_angle_000 - cen_angle_180)
    print("moving samry by %.3f, final position: %.3f" % (samry_offset, tt_grain['samry'] + samry_offset))
    umvr(samry, samry_offset)

    # third scan at 270 deg
    umv(diffrz, 270)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, 0.1, scan_mode = scan_mode)
    start_angle_270, end_angle_270, cen_angle_270 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # fourth scan at 90 deg
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_090, end_angle_090, cen_angle_090 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samrx by half the difference
    samrx_offset = 0.5 * (cen_angle_090 - cen_angle_270)
    print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    #samrx_offset =  tt_grain['diffry'] - cen_angle_090
    #print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    umvr(samrx, samrx_offset)

    # update values in tt_grain
    tt_grain['samrx'] += samrx_offset
    tt_grain['samry'] += samry_offset
    update_grain_info(tt_grain)
    print('values updated in tt_grain record')
    return tt_grain

def find_range(tt_grain, ang_step=0.05, search_range=0.3, exp_time=0.1, scan_mode = 'CAMERA'):
    """Find the topotomo angular range.
    
    This function runs two base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition. Note that this function 
    assumes that the grain has been aligned already.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use.
    :param float search_range: the base tilt angular search range in degrees.
    """
    step = 1  # load step
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/visitor/ma5178/id11/'
    sample_name = 'M4b'
    
    # open a script to gather all acquisition commands
    gid = tt_grain['gr_id']
    cmd_path = os.path.join(data_dir, 'tt_acq_grain_%d_step_%d.py' % (gid, step))
    f = open(cmd_path, 'w')
    f.write("def tt_acq():\n\n")
    f.write("    # activate ROI\n")
    f.write("    frelon16.image.roi = [710, 710, 500, 500]\n")
    f.write("    frelon16.roi_counters.set('roi1', [100, 100, 300, 300])\n")
    f.write("    ct(0.1)\n")
    f.write("    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')\n\n")
    
    print('find range for grain %d\n' % gid)
    
    # see if we need to create a new dataset or not
    #dataset = 'grain_%04d_checkrange_step_%d' % (gid, step)
    #newdataset(dataset)

    # define the ROI automatically as [710, 710, 500, 500]
    #frelon16.roi_counters.set('roi1', [710, 710, 500, 500])
    #ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    ACTIVE_MG.enable('marana:roi_counters:roi1_avg')
    # first scan at 90 deg
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_90, end_angle_90 = get_limits(fscan.get_data())
    diffry_range_90 = max(tt_grain['diffry'] - start_angle_90, end_angle_90 - tt_grain['diffry'])

    # second scan at 0 deg
    umv(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode= scan_mode)
    start_angle_00, end_angle_00 = get_limits(fscan.get_data())
    diffry_range_00 = max(tt_grain['diffry'] - start_angle_00, end_angle_00 - tt_grain['diffry'])

    # use the upper range value to define topotomo bounds
    print('diffry_range_00=%.3f - diffry_range_90=%.3f' % (diffry_range_00, diffry_range_90))
    diffry_range = max(diffry_range_00, diffry_range_90)

    # now find out the largest range to cover the grain
    diffry_start = tt_grain['diffry'] - diffry_range
    diffry_end = tt_grain['diffry'] + diffry_range
    n_acq_images = (diffry_end - diffry_start) / ang_step

    # write out the topotomo command for bliss
    f.write("    # create a new data set\n")
    f.write("    newdataset('grain_%04d_step_%d')\n\n" % (gid, step))
    f.write("    # acquisition for grain %d\n" % gid)
    f.write("    user.topotomo_tilt_grain(%d)\n" % gid)
    f.write("    umv(samrx, %.3f)\n" % tt_grain['samrx'])
    f.write("    umv(samry, %.3f)\n" % tt_grain['samry'])
    f.write("    fscan2d(diffrz, 0, 10, 36, diffry, %.3f, %.3f, %d, 0.1, scan_mode=tt_pars['scan_mode'])\n\n" % (diffry_start, ang_step, n_acq_images))
    f.close()
    return diffry_start, n_acq_images

def get_limits(scan_data, thres=0.05, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry = scan_data['diffry']
    # work out the angular step
    ang_step = diffry[1] - diffry[0]
    intensity = scan_data['marana:roi_counters:roi1_avg']
    # background correction
    bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) -1
    start_angle = diffry[start_pos] - padding * ang_step
    end_angle = diffry[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, number of images={}'.format(start_angle, end_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle
    
def get_limits_and_weighted_cen_angle(scan_data, thres=0.05, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry = scan_data['diffry']
    # work out the angular step
    ang_step = diffry[1] - diffry[0]
    intensity = scan_data['marana:roi_counters:roi1_avg']
    # background correction
    #bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    bg = np.min(intensity)
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) - 1
    weighted_cen_pos = 0;
    for i_pos in range(start_pos, end_pos, 1):
        weighted_cen_pos += i_pos * intensity[i_pos]
    weighted_cen_pos = weighted_cen_pos / np.sum(intensity[start_pos:end_pos])
    # compute the angle using linear interpolation
    weighted_cen_angle = (diffry[start_pos] * (end_pos - weighted_cen_pos) + diffry[end_pos] * (weighted_cen_pos - start_pos)) / (end_pos - start_pos)
    start_angle = diffry[start_pos] - padding * ang_step
    end_angle = diffry[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, weighted_cen_angle={}, number of images={}'.format(start_angle, end_angle, weighted_cen_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle, weighted_cen_angle




import json
import time
import os

class NumpyEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self,obj)


def writedictionaryfile(dictname,filename):
    """to write dictionary to file"""
    datatowrite = json.dumps(dictname)
    with open('%s'%filename, 'w') as f:
        f.write(datatowrite)
    print("Dictionary written as %s"%filename)
    return

def appenddictionaryfile(dictname,filename):
    """to write dictionary to file"""
    if os.path.exists('%s'%filename):
        with open('%s'%filename, 'r+') as f:
            data_to_write = [json.load(f)]
            dictname['time'] = str(time.ctime())
            data_to_write.append(dictname)
            f.seek(0)
            json.dump(data_to_write, f)
    else:
        newdict = dictname
        newdict['time'] = str(time.ctime())
        with open('%s'%filename, 'w') as f:
            json.dump(newdict, f)
            #f.write(datatowrite)
    print("Dictionary appended to %s"%filename)
    return

def readdictionaryfile(filename):
    """to read dictionary from file, returns dictionary"""
    with open('%s'%filename, 'r') as f:
        readdata = [json.loads(f.read())]
    print("Dictionary read from %s"%filename)
    return readdata[-1]


def update_grain_info(grain):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    writedictionaryfile(grain,'%s/%s_g%s'%(basedir,samplename,grain['gr_id']))
    #appenddictionaryfile(grain,'%s_g%s_history'%(SCAN_SAVING.collection_name,grain['gr_id']))
    return

def get_json_grain_info(grain_number):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    return readdictionaryfile('%s/%s_g%s'%(basedir,samplename,str(grain_number)))

def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])
    
def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        #finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)

def ref_scan_samy(exp_time, nref, ystep, omega, scanmode):
    umvr(samy, ystep)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umvr(samy, -ystep)
    
def ref_scan_difftz(exp_time, nref, zstep, omega, scanmode):
    difftz0 = difftz.position
    umv(difftz, difftz0 + zstep)
    print("fscan(diffrz, %6.2f, 0.1, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.1, nref, exp_time, scan_mode = scanmode)
    umv(difftz, difftz0)
    
class LoadLoop:
    def __init__(self):
        self._task = None
        self._stop = False
        self._sleep_time = 1
        self._filepath = None
        
    def start(self,target,loadstep,filepath,sleep_time=1.,time_step=0.5):
        self._filepath = filepath
        if self._task:
            self._stop = True
            self._task.get()

        self._sleep_time = sleep_time
        self._stop = False
        self._task = gevent.spawn(self._run,target,loadstep,time_step)

    def stop(self):
        self._stop = True
        if self._task: 
           self._task.get()

    def _run(self,target,loadstep,time_step):
        newdataset('loadramp_%d' %  loadstep)
        with open(self._filepath,'a') as f:
            while not self._stop:
                load_ramp_by_target(target,0,loadstep,time_step,to_file=f)
                gevent.sleep(self._sleep_time)

def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')
    
def ff_zseries(ff_pars, datasetname = 'ff'):
    user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    user.cpm18_goto(cpm18_tuned)
    umv(u22, u22_tuned)
    print('ff_zseries succeed')
    return('Succeed')

def tomo_series_cst_load(start, num_scans, target, sleep_time=0, pct_pars=None):
    for i in np.arange(start, start+num_scans):
        tomo.full_turn_scan(str(i))
        load_ramp_by_target(target, 0.05, 1)
	#dct_marana_dict(str(i), pct_pars)
        sleep(sleep_time)
    return

def ftomo_series(scanname, start, num_scans, sleep_time=0, pars=None):
    for i in np.arange(start, start+num_scans):
        newdataset(scanname + str(i))
        umv(diffrz, pars['start_pos']);
        fsh.disable()
        fsh.close()
        print("taking dark images")
        ftimescan(pars['exp_time'], pars['nref'],0)
        fsh.enable()
        print("taking flat images")
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'], pars['scan_mode'])
        print("taking projections...")
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        print("resetting diffrz to 0")
        umv(diffrz, pars['start_pos']+360);
        diffrz.position=pars['start_pos'];
        sleep(sleep_time)
    return

def tdxrd_boxscan(scanname, ff_pars):
    print("using slit_vg %f and slit_hg %f" % (ff_pars['slit_vg'], ff_pars['slit_hg']))
    umv(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.5, s8hg, ff_pars['slit_hg'] + 0.5)
    newdataset(scanname)
    finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])

def tdxrd_pointscan(scanname, sff_pars, grain):
    user.cpm18_goto(cpm18_tuned + sff_pars['cpm18_detune'])
    umv(s7vg, 0.5, s7hg, 0.5, s8vg, 0.05, s8hg, 0.05) 
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, bigy_in)
    #umv(samtx, grain['samtx'], samty, grain['samty'], samtz, grain['samtz'])
    stage_diffty_zero = diffty_offset
    scanstarty= -0.2  # in mm
    scanendy=0.201 
    scanstep=0.01
    
    newdataset(scanname)

    for ty in np.arange(scanstarty,scanendy,scanstep):
        umv(diffty, stage_diffty_zero+ty)
        print(f"Scan pos: y: {ty} ")
        finterlaced( diffrz, sff_pars['start_pos'], acq_size = sff_pars['step_size'], npoints = sff_pars['num_proj'], acq_time = sff_pars['exp_time'], mode = sff_pars['mode'])

    umv(diffrz,0)      
    umv(diffty, stage_diffty_zero)
    umv(bigy, bigy_out)
    diffty_airpad.off()
    user.cpm18_goto(cpm18_tuned)
    print("All done!")
 
def update_flat():
    image_corr.take_dark()
    image_corr.take_flat()
    image_corr.dark_on()
    image_corr.flat_on()

def flat_on():
    image_corr.dark_on()
    image_corr.flat_on()

def flat_off():
    image_corr.dark_off()
    image_corr.flat_off()

