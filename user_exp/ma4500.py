import numpy as np
import time
config.get('tfoh1')
ly = config.get('ly')
lfyaw = config.get('lfyaw')

def lyin():
    ly = config.get('ly')
    lfyaw = config.get('lfyaw')
    umv(ly,-10,lfyaw,10)
    
def lyout():
    ly = config.get('ly')
    lfyaw = config.get('lfyaw')
    umv(ly, 0, lfyaw, 0)
    
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']
       

def maranain():
    ff_offset = 340;
    lyout();
    dist = 5.2;
    ACTIVE_MG.enable("marana:image") 
    ACTIVE_MG.disable("frelon3:image")
    #ACTIVE_MG.disable("frelon2:image")
    #sheh3.close()
    umv(ffdtx1, dist + ff_offset, nfdtx, dist)
    umv(d2ty, -300, d3ty, 0)
    #sheh3.open()
    print("ready to collect marana data") 

def marana_abs():
    config.get('tfoh1')
    tfoh1.set(64,'Al')
    tfoh1.set(0,'Be')
    marana.image.roi=[512,0,1024,2048]
    sct(0.05)
    umvct(s8vg, 3.5, s8hg, 2)

def marana_dct():
    tfoh1.set(0,'Al')
    tfoh1.set(8,'Be')
    marana.image.roi=[0,0,2048,2048]
    sct(0.05)
    umvct(s8vg, 0.4, s8hg, 0.66)

def farfieldin():
    #ACTIVE_MG.disable("frelon2:image") 
    ACTIVE_MG.disable("marana*") 
    ACTIVE_MG.enable("frelon3:image")
    lyin()
    #sheh3.close()
    tolerance=0.01
    targetdet=140
    if(ffdtx1.position - targetdet)>tolerance:
        umv(nfdtx, 6)
        umv(d3ty, 180)
        umv(d2ty, -300)
        umv(ffdtx1,targetdet)
    else:
        print("already there")
    #sheh3.open()
    print("ready to collect diffraction data") 


def frelon16in():
    ff_offset = 640
    dist = 40
    ACTIVE_MG.enable("frelon16:image") 
    ACTIVE_MG.disable("marana:image") 
    ACTIVE_MG.disable("frelon3:image")
    umv(ffdtx1, dist + ff_offset, nfdtx, dist)
    umv(d3ty, 100, d2ty, 0)
    #frelon2.image.roi=(824,824,400,400) 
    #sheh3.open()
    print("ready to collect frelon16 data") 
    
def ET10_7_zseries():
    
    start_position = 5
    stepsize = 0.05
    nproj = 7200
    exp_time = 0.05
    ystep = -2
    refon = 7200
    nref = 21
    scanmode = 'CAMERA'

    samtz_start = -0.7
    umv(samtz, samtz_start)
    user.dct_marana('1', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)
    
    umv(samtz, samtz_start + 0.35)
    user.dct_marana('2', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)

    umv(samtz, samtz_start + 0.7)
    user.dct_marana('3', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)   
    
    umv(samtz, samtz_start + 1.05)
    user.dct_marana('4', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)
    
    umv(samtz, samtz_start + 1.4)
    user.dct_marana('5', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)

def Ni_s1_0N_zseries():
    
    start_position = 0
    stepsize = 0.05
    nproj = 7200
    exp_time = 0.05
    ystep = -1
    refon = 7200
    nref = 21
    scanmode = 'CAMERA'

    samtz_start = 0.11 #0.405 #-0.195 #0.71 #0
    umv(samtz, samtz_start - 0.35)
    user.dct_marana('dct1', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)
    
    umv(samtz, samtz_start)
    user.dct_marana('dct2', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)

    umv(samtz, samtz_start + 0.35)
    user.dct_marana('dct3', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)   
    
    
def Ni_s6_zseries_ff():
    user.maranain()
    umv(diffrz, 0)
    umv(nfdtx, 5.2)
    start_position = 0
    stepsize = 0.05
    nproj = 7200
    exp_time = 0.05
    ystep = -1
    refon = 7200
    nref = 21
    scanmode = 'CAMERA'

    samtz_start = 0.11
    umv(samtz, samtz_start - 0.3)
    user.dct_marana('final_dct1', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)

    umv(samtz, samtz_start - 0.15)
    user.dct_marana('final_dct2', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)

    umv(samtz, samtz_start)
    user.dct_marana('final_dct3', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)

    umv(samtz, samtz_start + 0.15)
    user.dct_marana('final_dct4', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode) 

    umv(samtz, samtz_start + 0.3)
    user.dct_marana('final_dct5', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode) 

    umv(nfdtx,80)
    user.farfieldin()

    newdataset('Ni_s6_final_ff1')
    umv(samtz, samtz_start - 0.3)
    finterlaced(diffrz,0,0.5,360,0.15)

    newdataset('Ni_s6_final_ff2')
    umv(samtz, samtz_start - 0.15)
    finterlaced(diffrz,0,0.5,360,0.15)

    newdataset('Ni_s6_final_ff3')
    umv(samtz, samtz_start)
    finterlaced(diffrz,0,0.5,360,0.15)

    newdataset('Ni_s6_final_ff4')
    umv(samtz, samtz_start + 0.15)
    finterlaced(diffrz,0,0.5,360,0.15)

    newdataset('Ni_s6_final_ff5')
    umv(samtz, samtz_start + 0.3)
    finterlaced(diffrz,0,0.5,360,0.15)

def Ni_s1_0N_ff():
    newdataset('Ni_s1_ff_0N')
    samtz_start = 0
    umv(samtz, samtz_start - 0.35)
    finterlaced(diffrz,0,0.5,360,0.15)
    
    umv(samtz, samtz_start)
    finterlaced(diffrz,0,0.5,360,0.15)

    umv(samtz, samtz_start + 0.35)
    finterlaced(diffrz,0,0.5,360,0.15)

def Ni_s3_0N_ff():
    newdataset('Ni_s3_ff1')
    samtz_start = 0.71 #0
    umv(samtz, samtz_start - 0.35)
    finterlaced(diffrz,0,0.5,360,0.15)
    newdataset('Ni_s3_ff2')
    umv(samtz, samtz_start)
    finterlaced(diffrz,0,0.5,360,0.15)
    newdataset('Ni_s3_ff3')
    umv(samtz, samtz_start + 0.35)
    finterlaced(diffrz,0,0.5,360,0.15)

def Ni_s6_0N_ff():
    newdataset('Ni_s6_ff1')
    samtz_start = 0.11 #0.405 #-0.195 #0.71 #0
    umv(samtz, samtz_start - 0.35)
    finterlaced(diffrz,0,0.5,360,0.15)
    newdataset('Ni_s6_ff2')
    umv(samtz, samtz_start)
    finterlaced(diffrz,0,0.5,360,0.15)
    newdataset('Ni_s6_ff3')
    umv(samtz, samtz_start + 0.35)
    finterlaced(diffrz,0,0.5,360,0.15)

# This function is used for Ni_s1_0N_topotomo()
def Ni_s1_0N_topotomo_singlegrainscan(diffry_value , samrx_value , samry_value , d3tz_value , dataset_name, amplitude=0.7, exp_time=0.1):
    umv(diffry , diffry_value , samrx , samrx_value , samry , samry_value , d3tz , d3tz_value)
    umv(diffrz, 0)
    y0 = 0
    width = 360
    step  = 20
    step_size = 0.05
    nof_step = 2*amplitude/step_size
    #topotomo_marana(dataset_name, y0, step, width/step+1, diffry_value-amplitude, step_size, nof_step, exp_time)
    newdataset(dataset_name) # named as 'graisn2', 'grain5', ..., 'grain21'.
    for my_y  in np.arange( y0, y0+width+step/10., 2*step): # scan in two directions to save the time
        umv(diffrz, my_y)
        fscan(diffry,diffry_value-amplitude,step_size,nof_step,exp_time,scan_mode = 'TIME')
        umv(diffrz, my_y+step)
        fscan(diffry,diffry_value+amplitude,-step_size,nof_step,exp_time,scan_mode = 'TIME')

def topotomo_fscan(name, diffry_center, amplitude, step_size, acq_time, omega_step, scan_mode_str='TIME'):

    omega_start = 0
    omega_range = 360
    nof_step = 2*amplitude/step_size
    
    newdataset(name) # named as 'grain_0001...
    for omega  in np.arange(omega_start, omega_start+omega_range+0.01, 2*omega_step): # scan in two directions to save the time
        umv(diffrz, omega)
        fscan(diffry, diffry_center - amplitude, step_size, nof_step, acq_time, scan_mode = scan_mode_str)
        umv(diffrz, omega + omega_step)
        fscan(diffry, diffry_center + amplitude, -step_size, nof_step, acq_time, scan_mode = scan_mode_str)

def topotomo_fscan_flip(name, diffry_center, amplitude, step_size, acq_time, omega_step, scan_mode_str='TIME'):
    omega_start = diffrz.position
    if(omega_start > 180):
        omega_range = -360
        omega_step = -abs(omega_step)
    else:
        omega_range = 360
        omega_step = abs(omega_step)
    nof_step = 2*amplitude/step_size + 1
    newdataset(name) # named as 'grain_0001...
    for omega  in np.arange(omega_start, omega_start+omega_range+omega_step/10, 2*omega_step): # scan in two directions to save the time
        umv(diffrz, omega)
        fscan(diffry, diffry_center - amplitude, step_size, nof_step, acq_time, scan_mode = scan_mode_str)
        umv(diffrz, omega + omega_step)
        fscan(diffry, diffry_center + amplitude, -step_size, nof_step, acq_time, scan_mode = scan_mode_str)

def topotomo_marana(newdataset_name, omega_start, omega_stepsize, nof_omega, theta_start, theta_stepsize, nof_theta, exp_time):
    if type(newdataset_name) != str: # avoid forgetting to type dataset name
        print('Dataset name is wrong')
        return
    newdataset(newdataset_name)
    for iter_index in range(int((nof_omega + 1) / 2)): # make sure 2 * i_max >= nof_omega
        umv(diffrz, 2 * float(iter_index) * omega_stepsize + omega_start)
        fscan(diffry, theta_start, theta_stepsize, nof_theta, exp_time, 'CAMERA')
        if 2 * iter_index > nof_omega: # If nof_omega is odd, it should stop here in the end.
            break
        umv(diffrz, (2 * float(iter_index) + 1) * omega_stepsize + omega_start)
        fscan(diffry, theta_start + nof_theta * theta_stepsize, -theta_stepsize, nof_theta, exp_time, 'CAMERA')
    
def Ni_s1_0N_topotomo():
    user.maranain()
    #maranain()
    umv(nfdtx,80)
    #grain2
    diffry_value_2 = -4.02
    samrx_value_2 = -1.09
    samry_value_2 = -2.04
    d3tz_value_2 = 11.3
    dataset_name_2 = 'grain2'
    Ni_s1_0N_topotomo_singlegrainscan(diffry_value_2 , samrx_value_2 , samry_value_2 , d3tz_value_2 , dataset_name_2)
    #grain5
    diffry_value_5 = -4.64
    samrx_value_5 = -2.75
    samry_value_5 = 2.22
    d3tz_value_5 = 13.08
    dataset_name_5 = 'grain5'

    Ni_s1_0N_topotomo_singlegrainscan(diffry_value_5 , samrx_value_5 , samry_value_5 , d3tz_value_5 , dataset_name_5)
    ##grain6
    #diffry_value_6 = -4.02
    #samrx_value_6 = -1.003
    #samry_value_6 = -2.02
    #d3tz_value_6 = 11.3
    #dataset_name_6 = 'grain6'
    #Ni_s1_0N_topotomo_singlegrainscan(diffry_value_6 , samrx_value_6 , samry_value_6 , d3tz_value_6 , dataset_name_6)
    #grain7
    diffry_value_7 = -4.64
    samrx_value_7 = 4.20
    samry_value_7 = -3.08
    d3tz_value_7 = 13.08
    dataset_name_7 = 'grain7'
    Ni_s1_0N_topotomo_singlegrainscan(diffry_value_7 , samrx_value_7 , samry_value_7 , d3tz_value_7 , dataset_name_7, 4)
    ##grain17
    #diffry_value_17 = -4.64
    #samrx_value_17 = 4.05
    #samry_value_17 = -3.18
    #d3tz_value_17 = 13.08
    #dataset_name_17 = 'grain17'
    #Ni_s1_0N_topotomo_singlegrainscan(diffry_value_17 , samrx_value_17 , samry_value_17 , d3tz_value_17 , dataset_name_17)
    #grain21
    #diffry_value_21 = -4.64
    #samrx_value_21 = -2.82
    #samry_value_21 = 1.17
    #d3tz_value_21 = 13.08
    #dataset_name_21 = 'grain21'
    #Ni_s1_0N_topotomo_singlegrainscan(diffry_value_21 , samrx_value_21 , samry_value_21 , d3tz_value_21 , dataset_name_21)
    
    ##grain2 
    #umv(diffry,4.02,samrx,-1.09,samry,-2.04,d3tz,-3.2)
    #newdataset('grain2')
    #umv(diffrz, 0)
    #y0 = 0
    #width = 360
    #step  = 4
    #step_size = 0.05
    #nof_step = 20
    #for my_y  in np.arange( y0, y0+width+step/10., step):
    #    umv(diffrz, my_y)
    #    fscan(diffry,4.02-0.5,step_size,nof_step,0.1,'CAMERA')    
    ##grain5
    #umv(diffry,4.64,samrx,-2.75,samry,2.22,d3tz,-3.9)
    #newdataset('grain5')
    #umv(diffrz, 0)
    #y0 = 0
    #width = 360
    #step  = 4
    #for my_y  in np.arange( y0, y0+width+step/10., step):
    #    umv(diffrz, my_y)
    #    fscan(diffry,4.64-0.5,step_size,nof_step,0.1,'CAMERA')   
    ##grain6
    #umv(diffry,4.02,samrx,-1.003,samry,-2.02,d3tz,-3.2)
    #newdataset('grain6')
    #umv(diffrz, 0)
    #y0 = 0
    #width = 360
    #step  = 4
    #for my_y  in np.arange( y0, y0+width+step/10., step):
    #    umv(diffrz, my_y)
    #    fscan(diffry,4.02-0.5,step_size,nof_step,0.1,'CAMERA')   
    ##grain7
    #umv(diffry,4.64,samrx,4.20,samry,-3.08, d3tz, -3.9)
    #newdataset('grain7')
    #umv(diffrz, 0)
    #y0 = 0
    #width = 360
    #step  = 4
    #for my_y  in np.arange( y0, y0+width+step/10., step):
    #    umv(diffrz, my_y)
    #    fscan(diffry,4.64-0.5,step_size,nof_step,0.1,'CAMERA')   
    ##grain17 
    #umv(diffry,4.64,samrx,4.05,samry,-3.18, d3tz, -3.9)
    #newdataset('grain17')
    #umv(diffrz, 0)
    #y0 = 0
    #width = 360
    #step  = 4
    #for my_y  in np.arange( y0, y0+width+step/10., step):
    #    umv(diffrz, my_y)
    #    fscan(diffry,4.64-0.5
    ##grain21
    #umv(diffry,4.64,samrx,-2.82,samry,1.17, d3tz, -3.9) 
    #newdataset('grain21')
    #umv(diffrz, 0)
    #y0 = 0
    #width = 360
    #step  = 4
    #for my_y  in np.arange( y0, y0+width+step/10., step):
    #    umv(diffrz, my_y)
    #    fscan(diffry,4.64-0.5,step_size,nof_step,0.1,'CAMERA')   
     

#def test_topotomo():
    #umv(diffrz, 0)
    #y0 = 0
    #width = 90
    #step  = 4
    #for my_y  in np.arange( y0, y0+width+step/10., step):
    #    umv(diffrz, my_y)
        

def Ni_s1_XN_zseriesandffandtopotomo(newsample_name = None):
    if type( newsample_name ) == str:
        newsample( newsample_name )
    ## DCT
    umv(diffry, 0, samrx, 1.1, samry, -0.85, d3tz, 0)
    user.maranain()
    #maranain()
    start_position_dct = 0
    stepsize_dct = 0.05 #0.02
    nproj_dct = 7200 #18000
    exp_time_dct = 0.05
    ystep_dct = -1
    refon_dct = 7200 #18000
    nref_dct = 21
    scanmode_dct = 'CAMERA'
    # start to scan
    umv(nfdtx , 5.2)
    user.dct_marana('dct', start_position_dct, stepsize_dct, nproj_dct, exp_time_dct, ystep_dct, refon_dct, nref_dct, scanmode_dct)
    #dct_marana('dct', start_position_dct, stepsize_dct, nproj_dct, exp_time_dct, ystep_dct, refon_dct, nref_dct, scanmode_dct)
    umv(nfdtx , 80)
    ## Far-field
    user.farfieldin()
    #farfieldin()
    newdataset('ff')
    finterlaced(diffrz,0,0.5,360,0.15)
    ## Topotomo
    user.maranain()
    #maranain()
    user.Ni_s1_0N_topotomo()
    #Ni_s1_0N_topotomo()

       
    

#def crlin():
#    bigy=config.get('bigy')
#    tfoh1 = config.get("tfoh1")
#    umv(bigy, 0)
#    tfoh1.set(20,"Be") 
    
#def crlout():
#    bigy=config.get('bigy')
#    tfoh1 = config.get("tfoh1")
#    umv(bigy, 80)
#    tfoh1.set(0,"Be")
#    print("done")


def Ni_s3_zseries_ff():
    user.maranain()
    umv(diffrz, 0)
    umv(nfdtx, 6)
    start_position = 0
    stepsize = 0.05
    nproj = 7200
    exp_time = 0.05
    ystep = -1
    refon = 7200
    nref = 21
    scanmode = 'CAMERA'

    samtz_start = -0.75
    umv(samtz, samtz_start)
    user.dct_marana('new_dct1', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)

    umv(samtz, samtz_start + 0.25)
    user.dct_marana('new_dct2', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)

    umv(samtz, samtz_start + 0.5)
    user.dct_marana('new_dct3', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)

    umv(samtz, samtz_start + 0.75)
    user.dct_marana('new_dct4', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode) 

    umv(samtz, samtz_start + 1)
    user.dct_marana('new_dct5', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)
    
    umv(samtz, samtz_start + 1.25)
    user.dct_marana('new_dct6', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)

    umv(samtz, samtz_start + 1.5)
    user.dct_marana('new_dct7', start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode)
    umv(nfdtx,80)
    user.farfieldin()

    newdataset('Ni_s3_new_ff1')
    umv(samtz, samtz_start)
    finterlaced(diffrz,0,0.5,360,0.15)

    newdataset('Ni_s3_new_ff2')
    umv(samtz, samtz_start + 0.25)
    finterlaced(diffrz,0,0.5,360,0.15)

    newdataset('Ni_s3_new_ff3')
    umv(samtz, samtz_start + 0.5)
    finterlaced(diffrz,0,0.5,360,0.15)

    newdataset('Ni_s3_new_ff4')
    umv(samtz, samtz_start + 0.75)
    finterlaced(diffrz,0,0.5,360,0.15)

    newdataset('Ni_s3_new_ff5')
    umv(samtz, samtz_start + 1)
    finterlaced(diffrz,0,0.5,360,0.15)

    newdataset('Ni_s3_new_ff6')
    umv(samtz, samtz_start + 1.25)
    finterlaced(diffrz,0,0.5,360,0.15)

    newdataset('Ni_s3_new_ff7')
    umv(samtz, samtz_start + 1.5)
    finterlaced(diffrz,0,0.5,360,0.15)


def Ni_sX_zseries(difftz_cen, shift_step_size, nof_shifts, start_position = 0, stepsize = 0.1, nproj = 3600, exp_time = 0.1, ystep = -1, refon = 3600, nref = 41, scanmode1 = 'CAMERA', datasetname = 'dct'):
    shift_step_size = abs(shift_step_size)
    if(nof_shifts < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_difftz_pos = difftz_cen - (nof_shifts - 1) * shift_step_size / 2
    print('Central difftz:' + str(difftz_cen))
    if(offset_difftz_pos < difftz.low_limit or offset_difftz_pos + (nof_shifts - 1) * shift_step_size > difftz.high_limit):
        print('Exceed the limits of difftz')
        return('Exceed the limits of difftz')
    for iter_i in range(int(nof_shifts)):
        umv(difftz, iter_i * shift_step_size + offset_difftz_pos)
        user.dct_marana(datasetname + str(iter_i + 1), start_position, stepsize, nproj, exp_time, ystep, refon, nref, scanmode1)
    umv(difftz, difftz_cen)
    print('DCT Succeed')
    return('Succeed')

def zseries_Ni_ff(difftz_cen, shift_step_size, nof_shifts, start_position = 0, acqsize = 0.5, nof_points = 360, acqtime = 0.13, datasetname = 'ff'):
    shift_step_size = abs(shift_step_size)
    if(nof_shifts < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_difftz_pos = difftz_cen - (nof_shifts - 1) * shift_step_size / 2
    print('Central difftz:' + str(difftz_cen))
    if(offset_difftz_pos < difftz.low_limit or offset_difftz_pos + (nof_shifts - 1) * shift_step_size > difftz.high_limit):
        print('Exceed the limits of difftz')
        return('Exceed the limits of difftz')
    for iter_i in range(int(nof_shifts)):
        umv(difftz, iter_i * shift_step_size + offset_difftz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, start_position, acqsize, nof_points, acqtime)
    umv(difftz, difftz_cen)
    print('FF Succeed')
    return('Succeed')

def Ni_dct_marana_init():
    tfoh1 = config.get("tfoh1")
    tfoh1.set(0, "Be")
    tfoh1.set(0, "Al")
    marana.image.roi = [0, 0, 2048, 2048]

def Ni718_54_get_positions():
    umv(samtx, -0.137, samty, 0.106, samtz, 2.875)
    umv(samrx, -3.3, samry, -0.04285)
    umv(difftz, -3)
    
def AlLi_s1_get_positions():
    umv(samtx, -0.09048, samty, -1.014, samtz, 2.475)
    umv(samrx, 0.99977, samry, 0.90072)
    umv(difftz, -6)
    
def Ni718_84_get_positions():
    umv(samtx, -0.46784, samty, -1.101, samtz, 3.0467)
    umv(samrx, 2, samry, 1.52)
    umv(difftz, -3)
    
def Ni4_3_get_position():
    umv(samtx, -0.37938, samty, -2.74, samtz, -0.21)
    umv(samrx, -4.00026, samry, 1.25007)
    umv(difftz, -11.85)
