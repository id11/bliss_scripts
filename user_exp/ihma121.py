import numpy as np

rx = np.linspace(-0.4,-0.6,11)

def lunch_rx():
    for rxpos in rx:
        print(rxpos)
        umv(shrx,rxpos)
        sync(pz)
        plotselect('eiger:roi_counters:roi3_sum')
        dscan(pz,-15,15,20,0.1)
        sync(pz)
        goto_peak()
        #sync(pz)
        #dscan(pz,-2,2,40,0.1)
        #sync(pz)



"""
with no focussing, scan shry (should look same at rot 0 AND 180)
and shrx (should look same at 90 and 270)
move to central dip
scan heights at 0 and 180 and adjust shry until they coincide
scan heights at 90 and 270 and adjust shrx until they coincide

because of reflection etc, changing ry or rx at 0 or 90 can change pz scan a lot while not changing at 180 or 270
"""

def align_surface():
    for an in [0,90,180,270]:
        user.piezo_scanmode_off() 
        umv(rot,an)
        user.piezo_scanmode_on() 
        dscan(pz,-0.6,0.6,80,0.1)


def sn1177b_scan():
    newdataset('varypz')
    for zpos in np.linspace(55.5,57.0,26):
        print('Moving pz to',zpos)
        umv(pz,zpos)
        fscan(rot,0,0.2,1800,0.02)
