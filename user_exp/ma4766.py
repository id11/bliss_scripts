
tfoh1 = config.get('tfoh1')
bigy = config.get('bigy')

def imaging():
    ACTIVE_MG.enable("marana:image") 
    ACTIVE_MG.disable("frelon3:image")
    ACTIVE_MG.disable("frelon3:roi*")
    sheh3.close()
    umv(ffdtx1, 470 ) # first go back
    umv(ffdtz1, 150 ) # now up
    umv(d3ty, 0)      # now marana goes in
    umv(bigy, 25)
    tfoh1.set(0, "Be")
    print("ready to collect marana data") 
    print("Open the shutter!!!")
    
    
def diffraction():
    ACTIVE_MG.disable("marana:image") 
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    sheh3.close()
    umv(d3ty, 200)    # now marana goes out
    umv(ffdtx1, 170, ffdtz1, 0 ) # 
    tfoh1.set(16, "Be")    
    #umv(bigy, -0.358)
    print("ready to collect frelon3 data") 
    print("Open the shutter!!!")
    

import numpy
def map_first_break():
    sheh3.open()
    # umv(samtz, -1.96 )
    for myz in numpy.arange( -2.2, -1.6, 0.01): # 60 horizontal scans
        umv( samtz, myz)
        fscan(samty,-0.09,0.001,50,3)    # 2'30"
        
    sheh3.close()
        
