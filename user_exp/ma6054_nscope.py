import numpy as np
import time
from bliss.common import cleanup

tfoh1 = config.get('tfoh1')
d3ty = config.get('d3ty')
d3x = config.get('d3x')

def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)


def tdxrd_forward360(ymax,
          datasetname,
          ystep = 0.01,
          ycen = 14.501,
          ymin = 0.,
          rstep=0.8,
          expotime=0.08
          ):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ycen = central position 
    ymin = low y values to skip, for restarting from a crash
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.tdxrd_forward(0.5, 'toto', ystep=0.1, ycen=14.65)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 11:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 ) + ycen
    i = -1
    #
    num_proj = 360 / rstep
    diffty_airpad.on( 60*24*5 )
    with cleanup.cleanup( diffrz, restore_list=(cleanup.axis.POS,) ):
        while i < len(ypos)-1:
            i += 1        
            if ypos[i] < ymin: # skip positions already done
                continue
            pause_for_refill( 25 )  # time for one scan
            umv( diffty, ypos[i] )
            if (i % 2) == 0: # forwards
                 finterlaced( diffrz,   0,  rstep, num_proj, expotime, mode='FORWARD' )
            else:
                 finterlaced( diffrz, 720, -rstep, num_proj, expotime, mode='FORWARD' )
    umv(diffty, ycen)
    diffty_airpad.off()
    

    

class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )

        
            
        


def half1(ymax,
          datasetname,
          ystep=0.25,
          ymin=-1801,
          rstep=0.05,
          astart = -90,
          arange = 181,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        plotselect("eiger:roi_counters:roi1_max")
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)




def next_rstart( stepsize, speed, home_range=15. ):
    """ Computes the next start position to keep going """
    if abs(360 - (rot.dial % 360)) < home_range: # if you are within range, go home
        print("Homing because there is a home nearby")
        rot.home()
    elif rot.dial*rot.steps_per_unit > pow(2,30):
        print("Running out of steps so homing")
        rot.home()
    pnow = rot.position
    offset = speed * speed / ( 2 * rot.acceleration )
    next_start = ( int(np.floor( (pnow + np.sign(stepsize)*offset)/stepsize )) + 1 ) * stepsize
    return next_start
            
def full1(ymax,
          datasetname,
          ystep=0.25,
          ymin=-1801,
          rstep=0.05,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.full1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.full1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    NR = int(361/rstep)
    umv(dty, max(ypos[0], ymin) )
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=( cleanup.axis.POS, ) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                rstart = next_rstart( rstep, angular_velocity )
                fscan2d( dty, ypos[i], ystep, 1, rot, rstart, rstep, 361/abs(rstep), expotime )
                # 7 min 53s with 25 scans == 19 seconds per scan
                
    umv(dty, 0)
    
    
    

def goto_nscope_pct():
    sheh3.open()
    umv(atty,-10,attrz,0)
    umv(d3x, 100)
    umv(d3ty, 150)
    fsh.session = 'NSCOPE'
    tfoh1.set(0, 'Be')
    if abs(piny.position) <= 0.1:
        moveto_basler(pinhole_switch=True)
    else:
        moveto_basler(pinhole_switch=False)
    umv(aly,-8)
    sheh3.open()
    umv(s9hg, 1.5, s9vg, 1.5)
    ct(0.02)
    
def goto_nscope_eiger():
    sheh3.open()
    umvct(s9hg, 0.05, s9vg, 0.05)    
    umvct(aly, 0)
    moveto_eiger()
    tfoh1.set(12, 'Be')
    umv(atty,0,attrz,-10)
    ct(0.01)
    
def goto_marana3_pct():
    sheh3.open()
    umv(atty,-10,attrz,0)
    fsh.session = 'TDXRD'
    tfoh1.set(0, 'Be')
    assert d3x.position > 90.0
    umvct(d3ty, 0)
    sheh3.open()
    umvct(s7hg,1.5,s7vg,1.5,s8hg,1.8,s8vg,1.8)
    umvct(d3x, 20)
    
       
def pct_series():
    umvct(d3x,20)
    fulltomo.full_turn_scan('pct_20x_20mm')
    umvct(d3x,30)
    fulltomo.full_turn_scan('pct_20x_30mm')
    umvct(d3x,40)
    fulltomo.full_turn_scan('pct_20x_40mm')
    umvct(d3x,50)
    fulltomo.full_turn_scan('pct_20x_50mm')
    umvct(d3x,60)
    fulltomo.full_turn_scan('pct_20x_60mm')
    umvct(d3x,70)
    fulltomo.full_turn_scan('pct_20x_70mm')
    umvct(d3x,80)
    fulltomo.full_turn_scan('pct_20x_80mm')
     
    
    
def run_s3DXRD(ymax = 500, dset_name = 's3DXRD_slice', ystep = 1, rstep=0.05, astart = -90, expotime=0.002):
    umv(atty,0,attrz,-17)
    half1(ymax, dset_name, ystep, rstep = rstep, astart = astart, expotime = expotime)
    sheh3.close()
    umv(rot,0)
    umv(edoor,0)
    print('Done')  
    


def run_s3DXRD_subvol(ymax = 10, ystep = 1, rstep=0.05, astart = -90, expotime=0.002):
    umv(atty,0,attrz,-17)
    ntz0 = ntz.position
    pz0 = pz.position
    
    # region of interest 20*20*20 um^3
    zpositions = np.arrange(pz0 - ymax, pz0 + ymax, 1)
    for zpos in zpositions:
        umv(pz, zpos)
        half1(ymax, f'ROI_Z{int(zpos)}', ystep = ystep, rstep = rstep, astart = astart, expotime = expotime )
    
    umv(ntz, ntz0)
    umv(pz, pz0)
    sheh3.close()
    umv(rot,0)
    umv(edoor,0)
    print('Done') 
    
    
    
    






