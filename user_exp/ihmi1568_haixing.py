import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime

atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')

save_path = '/data/visitor/ihmi1568/id11/20240906/RAW_DATA/Liyang_Al_x_AB'
f_target = 25.6
load = f_target


def test_dct():
    dct = DCTScan()
    print(dct.pars)
    return dct.pars



def switch_to_marana3_pct():
    fsh.close()
    marana3_in()
    fsh.open()
    ACTIVE_MG.disable('marana1*')
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    tfoh1.set(0, 'Be')
    umvct(s7vg, 1.5, s7hg, 1.5, s8vg, 1.6, s8hg, 1.6)
    return 'Done'

def switch_to_ff():
    fsh.close()
    tfoh1.set(18,'Be')
    ff_in()
    umv(atty,-10,attrz,0)
    ACTIVE_MG.disable('marana3*')
    ACTIVE_MG.disable('marana1*')
    ACTIVE_MG.enable('frelon3*')
    fsh.open()
    slit(0.15, 0.15)
    ct(0.05)


def marana3_in():
    ff_out()
    assert d1tz.position > 90
    #assert d2tz.position > 100
    assert d1ty.position < -200
    assert d2ty.position < -40
    umv(nfdtx, 100)
    umv(d3ty, 0)
    umv(d3tz, 0)
    umvct(atty,-10,attrz,0)
    return 'Done'
    
def ff_in():
    marana3_out()
    assert d3ty.position > 110
    assert d2ty.position < -40
    umv(ffdtz1, 0)
    umv(ffdtx1, 300)

    
def marana3_out():
    umv(nfdtx,100)
    umv(d3ty,150)

def ff_out():
    umv(ffdtx1, 600)
    umv(ffdtz1, 400)


