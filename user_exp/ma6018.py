ox800 = config.get("ox800")

def collect_while_ramping(T_target, ramp_rate=6, T_tol=0.25, exp_time=0.5, n_points=100):
    # ramp_rate is in K/min
    dsetname = f"ramp_to_{int(T_target)}"
    newdataset(dsetname)
    
   
    # start heating or cooling
    # gets converted to K/hour for ox800 object
    ox800.ramprate = ramp_rate * 60
    for i in range(3):
        ox800.setpoint = T_target
        sleep(1)
    
    
    temp_reached = False
    while not temp_reached:
        loopscan(n_points, exp_time)
        T = ox800.read()
        print(f'Current temperature is: {T} K')
        if abs(T - T_target) <= T_tol:
            temp_reached = True
    
    print(f"Target temperature reached!")


def collect_up_down(T_max, T_final, ramp_rate=6, T_tol=0.25, exp_time=0.5, n_points=100):
    # ramp_rate is in K/min
    dsetname = f"ramp_up_{int(T_max)}_down_{int(T_final)}"
    newdataset(dsetname)
    
    # constant ramp rate for the whole time
    ox800.ramprate = ramp_rate * 60
    
    # heating up
    for i in range(3):
        ox800.setpoint = T_max
        sleep(1)
    
    temp_reached = False
    while not temp_reached:
        loopscan(n_points, exp_time)
        T = ox800.read()
        print(f'Current temperature is: {T} K')
        if abs(T - T_max) <= T_tol:
            temp_reached = True
    
    print(f"Max target temperature reached!")
    
    # cooling down
    for i in range(3):
        ox800.setpoint = T_final
        sleep(1)
    
    temp_reached = False
    while not temp_reached:
        loopscan(n_points, exp_time)
        T = ox800.read()
        print(f'Current temperature is: {T} K')
        if abs(T - T_final) <= T_tol:
            temp_reached = True
    
    print(f"Final target temperature reached!")



def collect_up_down_up(T_max, T_min, T_final, ramp_rate=6, T_tol=0.25, exp_time=0.5, n_points=100):
    # ramp_rate is in K/min
    dsetname = f"ramp_up_{int(T_max)}_down_{int(T_min)}_up_{int(T_final)}"
    newdataset(dsetname)
    
    # constant ramp rate for the whole time
    ox800.ramprate = ramp_rate * 60
    
    # heating up
    for i in range(3):
        ox800.setpoint = T_max
        sleep(1)
    
    temp_reached = False
    while not temp_reached:
        loopscan(n_points, exp_time)
        T = ox800.read()
        print(f'Current temperature is: {T} K')
        if abs(T - T_max) <= T_tol:
            temp_reached = True
    
    print(f"Max target temperature reached!")
    
    umvr(diffy,-0.2)
    
    # cooling down
    for i in range(3):
        ox800.setpoint = T_min
        sleep(1)
    
    temp_reached = False
    while not temp_reached:
        loopscan(n_points, exp_time)
        T = ox800.read()
        print(f'Current temperature is: {T} K')
        if abs(T - T_min) <= T_tol:
            temp_reached = True
    
    print(f"Min target temperature reached!")
    
    umvr(diffy,-0.2)
    
    # heating up again
    for i in range(3):
        ox800.setpoint = T_final
        sleep(1)
    
    temp_reached = False
    while not temp_reached:
        loopscan(n_points, exp_time)
        T = ox800.read()
        print(f'Current temperature is: {T} K')
        if abs(T - T_final) <= T_tol:
            temp_reached = True
    
    print(f"Final target temperature reached!")
    
    
    
def collect_fixed_T(T_target, ramp_rate=6, T_tol=0.25, exp_time=0.5, n_points=100):
    # ramp_rate is in K/min
    ox800.ramprate = ramp_rate * 60
    
    for t in T_target:

        dsetname = f"{int(t)}K"
        newdataset(dsetname)
    
        # start heating or cooling
        # gets converted to K/hour for ox800 object
        for u in range(3):
            ox800.setpoint = t
            sleep(2)
    
    
        temp_reached = False
        while not temp_reached:
            T = ox800.read()
            print(f'Current temperature is: {T} K', end='\r')
            sleep(2)
            if abs(T - t) <= T_tol:
                temp_reached = True
    
        print(f"Target temperature reached!")
        loopscan(n_points, exp_time)
