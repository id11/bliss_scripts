import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime


bigy = config.get('bigy')    
ly = config.get('ly')
lfyaw = config.get('lfyaw')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
dct_dist = 181
pct_dist = 50
ff_offset = 320
ff_z = 100
ff_dist = 105
d1_out = -160
d2_out = -398
d2_in = -375
d3_out = 199
samtz_offset = 0  

def sam_dct_pos():
    uctmv(samrx, samrx_offset, samry, samry_offset, diffry, 0)
    umv(samtx, 0, samty, 0, samtz, 0)

def lyin():
    umv(ly,-2.5)

def lyout():
    umv(ly, 0)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']

def maranain(pct_pars):
    lyout();
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    umv(bigy,22)
    umv(d1ty, d1_out, d2ty, d2_out)
    ACTIVE_MG.enable("marana:image") 
    ACTIVE_MG.disable("frelon*")
    umv(ffdtx1, pct_dist + ff_offset, nfdtx, pct_dist, ffdtz1, ff_z)
    umv(d3ty, 0)
    marana_pct(pct_pars)
    print("marana ready to collect pct data")

def read_tt_infos(gid):
    data_dir = '/data/visitor/ma4756/id11/DCT_Analysis'
    data_path = os.path.join(data_dir, 'grain_%04d.mat' % gid)
    data = loadmat(data_path)
    t = data['out'][0][0][0][0]
    values = t[0][0][0][0][0]
    d = {}
    d['gr_id'] = values[0][0][0]
    # double check that the grain id is right
    if d['gr_id'] != gid:
        raise(ValueError('grain id mismatch, check your matlab output in file %s' % data_path))
    d['nfdtx'] = values[1][0][0]
    d['d3tz'] = values[2][0][0]
    d['diffry'] = values[3][0][0]
    d['samrx'] = values[4][0][0]
    d['samry'] = values[5][0][0]
    d['samtx'] = values[6][0][0]
    d['samty'] = values[7][0][0]
    d['samtz'] = values[8][0][0]
    d['samrx_offset'] = values[9][0][0]
    d['samry_offset'] = values[10][0][0]
    d['samtx_offset'] = values[11][0][0]
    d['samty_offset'] = values[12][0][0]
    d['samtz_offset'] = values[13][0][0]
    d['diffrz_offset'] = values[14][0][0]
    d['int_factor'] = values[15][0][0]
    print(d)
    return d

def define_tt_pars():
    tt_pars={}
    tt_pars['diffrz_step'] = 5
    tt_pars['num_proj'] = 360 / tt_pars['diffrz_step']
    tt_pars['diffry_step'] = 0.04
    tt_pars['exp_time'] = 0.05
    tt_pars['search_range'] = 0.4
    tt_pars['scan_mode'] = 'TIME'
    tt_pars['image_roi'] = [510, 510, 900, 900]
    tt_pars['counter_roi'] = [300, 300, 300, 300]
    tt_pars['slit_hg'] = 0.3
    tt_pars['slit_vg'] = 0.3
    return tt_pars

def topotomo_tilt_grain(gr_id):
    """drives diffractometer to grain alignment position for
    """
    topotomo_params = read_tt_infos(gr_id)
    print(topotomo_params)
    if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
        return('Error')

    #umv(d2tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')
  
def frelon1in():
    umv(d3ty, d3_out)
    umv(ffdtx1, dct_dist + ff_offset, nfdtx, dct_dist, ffdtz1, ff_z)
    tfoh1.set(0,'Al')
    tfoh1.set(10,'Be')
    for i in range (3):
        umv(tfz, -0.4, tfy, 14.97)
    lyout();
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon3*")
    ACTIVE_MG.enable("frelon1:image")
    umv(diffrz, 0)
    umv(d1ty, 0, d1tz, 0, d2ty, d2_in)
    print("ready to collect dct data")
    
def ffin():
    lyout();
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon1:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out, d2ty, d2_out, d1ty, d1_out)
    umv(ffdtx1, ff_dist, nfdtx, pct_dist, ffdtz1, 0)
    print("ready to collect far-field data")
 


def marana_pct(pct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    marana.image.roi=[0,0,2048,2048]
    sct(pct_pars['exp_time'])
    umvct(s8vg, pct_pars['slit_vg'], s8hg, pct_pars['slit_hg'])






import os
import numpy as np


def define_abs_pars():
    abs_pars={}
    abs_pars['start_pos'] = 0
    abs_pars['step_size'] = 1
    abs_pars['num_proj'] = 360 / abs_pars['step_size']
    abs_pars['exp_time'] = 0.05
    abs_pars['refon'] = abs_pars['num_proj']
    abs_pars['ref_step'] = 4
    abs_pars['scan_mode'] = 'TIME'
    abs_pars['nref'] = 41
    abs_pars['slit_hg'] = 0.72
    abs_pars['slit_vg'] = 1
    abs_pars['dist'] = 181
    abs_pars['ref_mot'] = samy
    return abs_pars
    
def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.05
    dct_pars['num_proj'] = 360 / dct_pars['step_size']
    dct_pars['exp_time'] = 0.3
    dct_pars['refon'] = dct_pars['num_proj']
    dct_pars['ref_step'] = 0
    dct_pars['scan_mode'] = 'TIME'
    dct_pars['nref'] = 1
    dct_pars['slit_hg'] = 0.72
    dct_pars['slit_vg'] = 0.3
    dct_pars['dist'] = 181
    dct_pars['ref_mot'] = samy
    return dct_pars
        
def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 1
    ff_pars['num_proj'] = 180 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.8
    ff_pars['slit_vg'] = 0.5
    return ff_pars


def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = 0
    pct_pars['step_size'] = 0.2
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.05
    pct_pars['refon'] = pct_pars['num_proj']
    pct_pars['ref_step'] = -4
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 41
    pct_pars['ref_mot'] = samy
    pct_pars['slit_hg'] = 1.5
    pct_pars['slit_vg'] = 1.5
    return pct_pars


def load_ramp_by_ascan(start, stop, npoints, exp_time, loadstep):
    marana_abs()
    newdataset('loadramp_%d' %  loadstep)
    ascan(stress, start, stop, npoints, exp_time, stress_cnt, marana)
    return

def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def load_ramp_by_target(target, time_step=0.5):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    
    #marana_abs()
    current_load = stress_cnt.get_value()
    if abs(target - current_load) < 1:
        print(_get_human_time(),stress.position,current_load)
        return
    if target < current_load:
        stress_increment = 0.1
        while (target < current_load):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load)
    else:
        stress_increment = -0.1
        while (target > current_load):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load)
    return


def define_pars(load_list):
    """Produces default input parameters for a load sequence (see next function)
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    for load in load_list:
        step_pars['dct_pars'] = define_dct_pars()
        step_pars['pct_pars'] = define_pct_pars()
        step_pars['tt_pars'] = define_tt_pars()
        step_pars['target'] = load
        step_pars['load_step'] = step
        step = step + 1
        par_list=[par_list, step_pars]
    return par_list


def load_sequence(pct_pars, ff_pars, load_list):
    """Load Experiment Master Sequence.
    
    This function will perform scan sequences (pct, ff) for a list of target load values defined in "load_list" 

    """
    for load in load_list:
        ref_pos = load_step_sequence(pct_pars, ff_pars, target)
    return


def load_step_sequence(pct_pars, ff_pars, target, wire):
    """Performs a loadramp to the new target value and launches PCT and 3DXRD boxbeam scans at this new target load
    """
    maranain(pct_dist)
    marana_pct()
    load_ramp_by_target(target)
    wire = do_wire_zscans(wire)
    wire = do_wire_xyscans(wire)
    scan_name = 'pct_%dN_' % target
    tomo.full_turn_scan(scan_name)
    ffin()
    scan_name = 'ff_%dN_' % target
    tdxrd_box(scan_name, ff_pars)
    return wire

def dct_frelon1(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("finterlaced(diffrz, %6.2f, %3.2f, %d, %f)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time']))
        finterlaced(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], mode = 'REWIND')
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])

def abs_frelon1(scanname, abs_pars):
    newdataset(scanname)
    umv(d2tz, 2)
    umv(diffrz, abs_pars['start_pos'])
    num_im_grp = abs_pars['refon']
    for ref_grp in np.arange(0, abs_pars['num_proj']/abs_pars['refon'], 1):
        startpos_grp = ref_grp * abs_pars['refon'] * abs_pars['step_size'] + abs_pars['start_pos']
        ref_scan(abs_pars['ref_mot'], abs_pars['exp_time'], abs_pars['nref'], abs_pars['ref_step'], startpos_grp, abs_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f)" % (startpos_grp, abs_pars['step_size'], num_im_grp, abs_pars['exp_time']))
        fscan(diffrz, startpos_grp, abs_pars['step_size'], num_im_grp, abs_pars['exp_time'])
    ref_scan(abs_pars['ref_mot'], abs_pars['exp_time'], abs_pars['nref'], abs_pars['ref_step'], abs_pars['start_pos'] + abs_pars['num_proj']*abs_pars['step_size'], abs_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(abs_pars['ref_mot'], abs_pars['exp_time'], abs_pars['nref'], 0, abs_pars['start_pos'] + abs_pars['num_proj']*abs_pars['step_size'], abs_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, abs_pars['start_pos'])   


def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'])
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

    
def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time)
    umv(ref_mot, refpos)
    
def ref_scan_samy(exp_time, nref, ystep, omega, scanmode):
    umvr(samy, ystep)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umvr(samy, -ystep)
    
def ref_scan_difftz(exp_time, nref, zstep, omega, scanmode):
    difftz0 = difftz.position
    umv(difftz, difftz0 + zstep)
    print("fscan(diffrz, %6.2f, 0.1, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.1, nref, exp_time, scan_mode = scanmode)
    umv(difftz, difftz0)
    


def tomo_series(start, num_scans, target, sleep_time=0, pct_pars=None):
    for i in np.arange(start, start+num_scans):
        tomo.full_turn_scan(str(i))
        load_ramp_by_target(target, 0.05, 1)
	#dct_marana_dict(str(i), pct_pars)
        sleep(sleep_time)
    return

def difftomo():
    d0 = 14.0
    umv(samtz,1.29)
    newdataset('dtE')
    for ty in np.arange(-0.2,0.2001,0.005):
        umv(diffty, d0+ty)
        finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(diffty, d0 )
    print("all done")


def tdxrd_box(scanname, ff_pars):
    print("using slit_vg %f and slit_hg %f" % (ff_pars['slit_vg'], ff_pars['slit_hg']))
    umv(s8vg, ff_pars['slit_vg'], s8hg, ff_pars['slit_hg'])
    newdataset(scanname)    
    finterlaced(diffrz, ff_pars['start_pos'], ff_pars['step_size'], ff_pars['num_proj'], ff_pars['exp_time'], mode='ZIGZAG')

def tdxrd_box_short(scanname, ff_pars):
    print("using slit_vg %f and slit_hg %f" % (0.1, ff_pars['slit_hg']))
    umv(s8vg, 0.1, s8hg, ff_pars['slit_hg'])
    newdataset(scanname)    
    finterlaced(diffrz, ff_pars['start_pos'], ff_pars['step_size'], 360, ff_pars['exp_time'], mode='ZIGZAG')


def do_wire_zscans(wire):
    ACTIVE_MG.enable('marana:roi_counters:roi*')
    umv(diffrz, 0, bigy, 0)
    #lyin()
    umv(samtx, 0, samty, 0, samtz, wire['top_wire_z'])
    dscan(samtz, -0.04, 0.04, 80, 0.05)
    wire['top_wire_z'] = cen();
    umv(samtz, wire['bot_wire_z'])
    dscan(samtz, -0.04, 0.04, 80, 0.05)
    wire['bot_wire_z'] = cen();
    wire['samtz_distance'] =  (- wire['top_wire_z'] + wire['bot_wire_z'])
    print("bottom samtz position %g" % wire['bot_wire_z'])
    print("top samtz position %g" % wire['top_wire_z'])
    print("new samtz center position between wires %g" % wire['samtz_distance'])

    umv(bigy, 22, samtz, 0)
    #lyout()
    return wire

def define_wire_pars():
    wire={}
    wire['wire_x'] = 0.448
    wire['wire_y'] = 0.217
    wire['top_wire_z'] = -1.70
    wire['bot_wire_z'] = 1.53
    wire['hor_wire_z'] = -2
    wire['samtz_pos'] = 0
    return wire

def do_wire_xyscans(wire):
	umv(samtx, wire['wire_x'], samty, wire['wire_y'], samtz, wire['hor_wire_z']);
	umv(diffrz, 0, bigy, 0)
	lyin()
	dscan(samty, -0.02, 0.02, 40, 0.05)
	wire['wire_y'] = cen()
	print("new samty position of wire: %g'" % wire['wire_y'])
	umv(diffrz, 90)
	dscan(samtx, -0.02, 0.02, 40, 0.05)
	wire['wire_x'] = cen()
	print("new samtx position of wire: %g'" % wire['wire_x'])
	lyout()
	umv(bigy, 22)
	return wire

    
def scannningtest(scanname):
    diffty_airpad.on(60*24*5)
    newdataset(scanname+'_coarse')
    for ty in np.arange(-0.3,0.301,0.03):
        umv(diffty,14+ty)
        finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    newdataset(scanname+'_fine')
    for ty in np.arange(-0.025,0.02501,0.0025):
        umv(diffty,14+ty)
        finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(diffty,14)
    diffty_airpad.off()
    print("all done")

def nightscanning(scanname):
    diffty_airpad.on(60*24*5)
    
    newdataset(scanname+'_coarse_top')
    umvr(samtz, -0.015)
    for ty in np.arange(-0.3,0.301,0.03):
            umv(samy, ty)
            print("Scan pos: y ",ty)
            finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(samtz, 0)
                
    newdataset(scanname+'_fine')
    for tz in np.arange(-0.013,0.01301,0.001):
        umv(samtz, tz)
        for ty in np.arange(-0.015,0.01501,0.001):
            umv(samy, ty)
            print("Scan pos: z ",tz," y ",ty)
            finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )

    newdataset(scanname+'_coarse_bottom')
    umvr(samtz, 0.015)
    for ty in np.arange(-0.3,0.301,0.03):
            umv(samy, ty)
            print("Scan pos:  y ",ty)
            finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(samtz, 0)

    diffty_airpad.off()
    print("All done!")


def redo_finescan(scanname):
    diffty_airpad.on(60*24*5)
    
    newdataset(scanname+'_redofine_layer5')
    umvr(samtz, -0.011)
    for ty in np.arange(-0.015,0.01501,0.001):
        umv(samy, ty)
        print("Scan pos: y ",ty)
        finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(samtz, 0)

def tdxrd_series(scanname):
    newdataset(scanname)
    for tz in np.arange(0,1,0.2):
        umv(samtz, tz)
        finterlaced(diffrz, 0, 1, 180, 0.08)
        
        
def pointscan_single(scanname):
    diffty_airpad.on(60*24*5)
    newdataset(scanname)
    for ty in np.arange(-0.015,0.01501,0.002):
            umv(samy, ty)
            print("Scan pos: y ",ty)
            finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    diffty_airpad.off()
    print("All done!")

def tdxrd_heightscan(scanname):
    print("using slit_vg %f and slit_hg %f" % (0.02, ff_pars['slit_hg']))
    umv(s8vg, 0.02, s8hg, ff_pars['slit_hg'])
    newdataset(scanname)
    for tz in np.arange(-0.01,0.01001,0.002):
        umv(samtz, tz)
        finterlaced(diffrz, 0, 1, 180, 0.08, mode='ZIGZAG')
    umv(samtz, 0)
    umv(s8vg, 0.5, s8hg, ff_pars['slit_hg'])
    print("All done!")


def fridaynightfeverscanning(scanname):
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, 0)
    lyout()
    newdataset(scanname)
    for tz in np.arange(0,0.006001,0.003):
        umv(samtz, tz)
        for ty in np.arange(-0.249,0.25001,0.003):
            umv(samy, ty)
            print("Scan pos: z ",tz," y ",ty)
            finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(diffrz,0)            
    umv(samty, 0, samtz, 0)
    diffty_airpad.off()
    print("All done!")
    
   
def cross_section_scan(scanname):
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, 0)
    lyout()
    stage_diffty_zero = 14.0
    newdataset(scanname)
    for tz in np.arange(0,0.006001,0.003):
        umv(samtz, tz)
        for ty in np.arange(-0.249,0.24901,0.003):
            umv(diffty, stage_diffty_zero+ty)
            print("Scan pos: z ",tz," y ",ty)
            finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(diffrz,0)            
    umv(samty, 0, samtz, 0)
    umv(diffty, stage_diffty_zero)
    diffty_airpad.off()
    print("All done!")
    

def saturdaynightfever(scanname):
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, 0)
    lyout()
    stage_diffty_zero = 14.0
    
    #Thru-xsec coarse scan
    newdataset(scanname+'_coarse')
    for ty in np.arange(-0.4,0.4001,0.01):
        umv(diffty, stage_diffty_zero+ty)
        print("Scan pos: y ",ty)
        finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(diffrz,0)
    umv(samty, 0, samtz, 0)          
    umv(diffty, stage_diffty_zero)
    
    
    #Fine scan around the void
    newdataset(scanname+'_fine')
    for tz in np.arange(-0.01,0.01001,0.002):
        umv(samtz, tz)
        for ty in np.arange(-0.01,0.01001,0.002):
            umv(diffty, stage_diffty_zero+ty)
            print("Scan pos: z ",tz," y ",ty)
            finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(diffrz,0)
    umv(samty, 0, samtz, 0)          
    umv(diffty, stage_diffty_zero)
    umv(bigy,22)
    diffty_airpad.off()
    print("All done!")
    
def sundaymorningblues(scanname):
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, 0)
    lyout()
    stage_diffty_zero = 14.0
    
    newdataset(scanname+'_semicoarse')
    #Fine scan around the void
    for tz in np.arange(-0.01,0.01001,0.02):
        umv(samtz, tz)
        for ty in np.arange(-0.4,0.4,0.005):
            umv(diffty, stage_diffty_zero+ty)
            print("Scan pos: z ",tz," y ",ty)
            finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(diffrz,0)
    umv(samty, 0, samtz, 0)          
    umv(diffty, stage_diffty_zero)
    umv(bigy,22)
    diffty_airpad.off()
    print("All done!")

def sundaymorningblues_centerslice(scanname):
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, 0)
    lyout()
    stage_diffty_zero = 14.0
    
    newdataset(scanname+'_semicoarse_center')
    #Fine scan around the void
    for ty in np.arange(0.14,0.4,0.005):
            umv(diffty, stage_diffty_zero+ty)
            print("Scan pos: y ",ty)
            finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(diffrz,0)
    umv(samty, 0, samtz, 0)          
    umv(diffty, stage_diffty_zero)
    umv(bigy,22)
    diffty_airpad.off()
    print("All done!")
    
def mondaymorningblues_centerslice(scanname):
    
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, 0)
    lyout()
    stage_diffty_zero = 14.0
    scanstarty=-0.4  # in mm
    scanendy=0.4 
    scanstep=0.005
    
    newdataset(scanname+'_semicoarse_center')
    #Fine scan around the void
    for ty in np.arange(scanstarty,scanendy,scanstep):
            umv(diffty, stage_diffty_zero+ty)
            print("Scan pos: y ",ty)
            finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(diffrz,0)
    umv(samty, 0, samtz, 0)          
    umv(diffty, stage_diffty_zero)
    umv(bigy,22)
    diffty_airpad.off()
    print("All done!")
    
def sunday_loadtomo(scanname):
    load_0 = 38.0


    for i in range(8):
        load_start = load_0 + loadstep*i 
        load_final = load_0 + loadstep*(i+1)
        
        fname = list(str(float(load_final)))
        fname[-2] = 'p'
        fname="".join(fname)
        
        
        ascan(stress, load_start, load_final, 200,0.5,stress_cnt) # 200 steps of 0.5 s. leave stress_cnt as is
        
        tomo.full_turn_scan('pct_'+fname)
 

def collect_sam3_dct(dct_pars, abs_pars):
    umv(samtz, 0)
    dct_frelon1('dct_cen', dct_pars)
    umv(samtz, 0.25)
    dct_frelon1('dct_bot', dct_pars)
    umv(samtz, -0.25)
    dct_frelon1('dct_top', dct_pars)
    umv(samtz, 0, s8vg, 1)
    abs_frelon1('abs', abs_pars)
    
    
def mysundayfeeling(scanname):
    
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, 0)
    lyout()
    stage_diffty_zero = 14.0
    scanstarty=-0.36  # in mm
    scanendy=0.360001 
    scanstep=0.003
    
    newdataset(scanname+'_fine')
    
    for tz in np.arange(-0.003,0.003001,0.003):
        umv(samtz, tz)
        for ty in np.arange(scanstarty,scanendy,scanstep):
            umv(diffty, stage_diffty_zero+ty)
            print("Scan pos: z ",tz," y ",ty)
            finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )

    umv(diffrz,0)
    umv(samty, 0, samtz, 0)          
    umv(diffty, stage_diffty_zero)
    umv(bigy,22)
    diffty_airpad.off()
    print("All done!")    
    
    
def hr3dxrd_lite(scanname):
    # lenses should be in and focus on center
    # sample roughly centered on center of rotation
    # attenuator should be out
    # ffin()  should be in
    diffty_airpad.on(60*24*5)
    umv(bigy,0)
    umv(s8vg, 0.05, s8hg, 0.05)
    # lyout()
    stage_diffty_zero = 14.0
    scanstarty=-0.04  # in mm
    scanendy=0.01 
    scanstep=0.002
    
    tz_0 = 0.46
    
    om_start = -20
    om_step = 0.075
    num_proj = 40
    
    
    newdataset(scanname+'_hkl')
    for tz in np.arange(-0.004,0.004001,0.002):
        umv(samtz, tz_0+tz)
        for ty in np.arange(scanstarty,scanendy,scanstep):
            umv(diffty, stage_diffty_zero+ty)
            print("Scan pos: z ",tz," y ",ty)
            fscan( diffrz, om_start, om_step, num_proj, 0.1, scan_mode='CAMERA' )
    
    newdataset(scanname+'_minushkl')
    for tz in np.arange(-0.004,0.004001,0.002):
        umv(samtz, tz_0+tz)
        for ty in np.arange(scanstarty,scanendy,scanstep):
            umv(diffty, stage_diffty_zero+ty)
            print("Scan pos: z ",tz," y ",ty)
            fscan( diffrz, om_start+180, om_step, num_proj, 0.1, scan_mode='CAMERA' )
    
    umv(samtz,tz_0)
    umv(diffty, stage_diffty_zero)
    diffty_airpad.off()
    print("All done!")

def hr3dxrd_lite_minhkl(scanname):
    # lenses should be in and focus on center
    # sample roughly centered on center of rotation
    # attenuator should be out
    # ffin()  should be in
    diffty_airpad.on(60*24*5)
    umv(bigy,0)
    umv(s8vg, 0.05, s8hg, 0.05)
    # lyout()
    stage_diffty_zero = 14.0
    scanstarty=-0.04  # in mm
    scanendy=0.01 
    scanstep=0.002
    
    tz_0 = 0.46
    
    om_start = -20
    om_step = 0.075
    num_proj = 40

    newdataset(scanname+'_minushkl')
    for tz in np.arange(-0.004,0.004001,0.002):
        umv(samtz, tz_0+tz)
        for ty in np.arange(scanstarty,scanendy,scanstep):
            umv(diffty, stage_diffty_zero+ty)
            print("Scan pos: z ",tz," y ",ty)
            fscan( diffrz, om_start+180, om_step, num_proj, 0.1, scan_mode='CAMERA' )
    
    umv(samtz,tz_0)
    umv(diffty, stage_diffty_zero)
    diffty_airpad.off()
    print("All done!")
