import sys
import numpy as np
import time
import os
from datetime import datetime

    
ly = config.get('ly')
lfyaw = config.get('lfyaw')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
dct_dist = 6
ff_dist = 150
ff_pos_dct = 250
d3_out = 180
d2_out = -370

dct_pars={}
dct_pars['execute'] = 1
dct_pars['start_pos'] = 0
dct_pars['step_size'] = 0.1
dct_pars['num_proj'] = 360 / dct_pars['step_size']
dct_pars['exp_time'] = 0.1
dct_pars['refon'] = dct_pars['num_proj']
dct_pars['ref_step'] = -3.5
dct_pars['scan_mode'] = 'CAMERA'
dct_pars['nref'] = 41
dct_pars['slit_hg'] = 1
dct_pars['slit_vg'] = 0.10
dct_pars['ref_mot'] = samy

ff_pars={}
ff_pars['execute'] = 1
ff_pars['step_size'] = 0.25
ff_pars['exp_time'] = 0.08
ff_pars['slit_hg'] = 1
ff_pars['slit_vg'] = 0.25



# attenuation moves below:

def lyin():
    umv(ly,-10,lfyaw,20)

def lyout():
    umv(ly, 0, lfyaw, 0.1)

def pause_for_refill(t, num=1e4):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t, num)

def pause_for_flux(t, num=1e4):
    c = ct(1,pico4).get_data()['pico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,pico4).get_data()['pico4']

def farfieldin():
    lyin();
    tfoh1.set(12,'Be')
    ACTIVE_MG.disable("marana*") 
    ACTIVE_MG.disable("frelon2*")
    ACTIVE_MG.enable("frelon3:image")
    ct(0.05)

    umv(d3ty, d3_out)
    umv(ffdtx1, ff_dist, nfdtx, dct_dist)
    umv(s8vg, ff_pars['slit_vg'])
    umv(s8hg, ff_pars['slit_hg'])
    print("ready to collect farfield data") 

def maranain(dist):
    lyout();
    tfoh1.set(16,'Be')
    ACTIVE_MG.enable("marana:image") 
    ACTIVE_MG.disable("frelon2*")
    ACTIVE_MG.disable("frelon3*")

    umv(ffdtx1, ff_pos_dct + dist, nfdtx, dist)
    umv(d3ty, 0)
    umv(s8vg, dct_pars['slit_vg'])
    umv(s8hg, dct_pars['slit_hg'])
    print("ready to collect marana data") 


def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def load_ramp_by_target(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    write_to_file = to_file if to_file is not None else sys.stdout

    stress_incr = 0.25  # in volts 
    current_load = stress_cnt.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target < current_load:
        stress_increment = stress_incr
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -stress_incr
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return

def dct_marana_dict(scanname):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, dct_pars['start_pos'], dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)
    

def ff_dict(scan_name):
    # umv(s8hg, ff_pars['slit_hg'], s8vg, ff_pars['slit_vg'])
    newdataset(scan_name)
    num_proj = np.round(360 / ff_pars['step_size']).astype(int)
    if diffrz.position > 360:
        finterlaced(diffrz, 720, -ff_pars['step_size'], num_proj, ff_pars['exp_time'], mode="FORWARD")
    else:
        finterlaced(diffrz, 0, ff_pars['step_size'], num_proj, ff_pars['exp_time'], mode="FORWARD")


def zseries_ff_dict(cen_pos, shift_step_size, nof_shifts, scan_name):
    shift_step_size = abs(shift_step_size)
    if(nof_shifts < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_pos = cen_pos - (nof_shifts - 1) * shift_step_size / 2
    print('Central position:' + str(cen_pos))
    if(offset_pos < samtz.low_limit or offset_pos + (nof_shifts - 1) * shift_step_size > samtz.high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(nof_shifts)):
        umv(samtz, iter_i * shift_step_size + offset_pos)
        scanname = scan_name + "_" + str(iter_i + 1)
        ff_dict(scanname)
    umv(samtz, cen_pos)
    print('FF Succeed')
    return('Succeed')

def zseries_dct_dict(cen_pos, shift_step_size, nof_shifts, scan_name):
    shift_step_size = abs(shift_step_size)
    if(nof_shifts < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_pos = cen_pos - (nof_shifts - 1) * shift_step_size / 2
    print('Central position:' + str(cen_pos))
    if(offset_pos < samtz.low_limit or offset_pos + (nof_shifts - 1) * shift_step_size > samtz.high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(nof_shifts)):
        umv(samtz, iter_i * shift_step_size + offset_pos)
        scanname = scan_name + "_" + str(iter_i + 1)
        dct_marana_dict(scanname)
    umv(samtz, cen_pos)
    print('DCT Succeed')
    return('Succeed')


def boxscan( nsteps_y, nsteps_z, beamsize, scan_name ):
    newdataset(scan_name)
    farfieldin()
    tfoh1.set(4, "Be")
    # 14.6439
    z0 = samtz.position
    y0 = diffty.position
    fullrange_y = (nsteps_y-1) * beamsize
    fullrange_z = (nsteps_z-1) * beamsize
    elog_print("Starting boxscan %s z0=%f y0=%f N_y=%d N_z=%d size=%f"%(scan_name,z0,y0,nsteps_y,nsteps_z,beamsize))
    offsets_z = np.linspace( -fullrange_z/2, fullrange_z/2, nsteps_z )
    offsets_y = np.linspace( -fullrange_y/2, fullrange_y/2, nsteps_y )
    # wide slices across sample
    umv( s7vg, beamsize, s7hg, fullrange_y )
    umv( s8vg, beamsize+0.05, s8hg, fullrange_y+0.2 )
    for o in offsets_z:
        umv(samtz, z0 + o )
        pause_for_refill(80, num=1000)
        if diffrz.position < 360:
            finterlaced( diffrz,   0,  0.8, 450, 0.08, mode='FORWARD' )
        else:
            finterlaced( diffrz, 720, -0.8, 450, 0.08, mode='FORWARD' )
    umv( samtz, z0 )
    # tall slices
    umv( s7hg, beamsize, s7vg, fullrange_z )
    umv( s8hg, beamsize+0.05, s8vg, fullrange_z+0.2 )
    for o in offsets_y:
        umv(diffty, y0 + o )
        pause_for_refill(80, num=1000)
        if diffrz.position < 360:
            finterlaced( diffrz,   0,  0.8, 450, 0.08, mode='FORWARD' )
        else:
            finterlaced( diffrz, 720, -0.8, 450, 0.08, mode='FORWARD' )
    umv( diffty, y0 )
    umv(s7vg, 2)
    umv(s7hg, 2)
    elog_print("Ended boxscan %s"%(scan_name))
    
    
    
    
    
    
    
    
