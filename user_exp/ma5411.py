import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime


    
atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
bigy = config.get('bigy')
u22 = config.get('u22')
#rue
cpm18_tuned = 6.3558
u22_tuned = 8.3156

dct_dist = 8
pct_dist = 300
nfdtx_savedist = 4
ffdtx1_savedist = 770
ffdtx1_savedist_tt = 900
tt_x_offset = -28.64

# after loading adjust samtz according to surface features and reset samtz.position = 0
samtx_offset = 0
samty_offset = 0
samtz_offset = 0
samrx_offset = 0
samry_offset = 0

diffty_offset = 14.5

ff_offset = 450
tt_offset = 0
tt_dist = 11
ff_z = 100
ff_dist = 170

d1_out = -200
d1_in = 0
d3_bsty_in = 0
d3_bsty_out = 2000
d2_out = -200
d2_in = 0
d3_out = 200
d3tz_pos_dct = 0
d3tz_pos_tt = 0
bigy_in = -0.2917
bigy_out = 22

def furnace_in():
    umv(d3ty, 100, nfdtx, 187)
    umv(d2ty, 0)
    umv(d2tz, 0)

def furnace_out():
    umv(d2tz, 100)
    umv(d2ty, -45)
    
def marana_in(dist):
    assert(d2tz.position > 99)
    assert(d2ty.position < -40)
    umv(d3ty, 0, nfdtx, dist)
    
    
    
def attyin():
    umv(atty,-8.5)

def attyout():
    umv(atty, 5)

def bsin():
	umv(d3_bsty, d3_bsty_in)

def bsout():
	umv(d3_bsty, d3_bsty_out)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']

    
def ffin(ff_pars):
    sheh3.close()
    tfoh1.set(0,'Be')
    umv(bigy, bigy_out)
    umv(ffdtx1, 650, nfdtx, 200, ffdtz1, 100)
    #sam_dct_pos()
    #attyin();
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon1:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out, d2ty, d2_out, d1ty, d1_out)
    umvct(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umvct(s8hg, ff_pars['slit_vg'] + 0.2, s8hg, ff_pars['slit_hg'] + 0.2)
    umv(nfdtx, 100)
    umv(ffdtz1, 0, ffdtx1, ff_dist)
    print("ready to collect far-field data")
    sheh3.open()
    ct(0.02)
 
def scanning_ff():
    tfoh1.set(0,'Be')
    umv(bigy, bigy_in)
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon16:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out)
    umv(nfdtx, dct_dist, ffdtz1, 0)
    print("ready to collect far-field data")

def frelon1in(dct_pars):
    sheh3.close()
    umv(ffdtx1, 650, nfdtx, 200, ffdtz1, 100)
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon3*")
    ACTIVE_MG.enable("frelon1:image")
    umv(d3ty, d3_out, d1ty, 0, d2ty, 0)
    umv(nfdtx, dct_pars['dist'])
    umvct(s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umvct(s8vg, dct_pars['slit_vg'] + 0.1, s8hg, dct_pars['slit_hg'] + 0.1)
    sheh3.open()
    sct(dct_pars['exp_time'])
    print("ready to collect dct data")



def marana_pct(pct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    centeredroi(marana,pct_pars['roi_hv'][0],pct_pars['roi_hv'][1])
    #bsout()
    umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'])
    umvct(s8vg, pct_pars['slit_vg'] + 0.1, s8hg, pct_pars['slit_hg'] + 0.1)
    sct(pct_pars['exp_time'])

def marana_large_beam(pct_pars):
    tfoh1.set(64,'Al')
    tfoh1.set(0,'Be')
    #for i in range (3):
    #    umv(tfz, -0.46, tfy, 14.488)
    marana.image.roi=[0,0,2048,2048]
    sct(0.05)
    umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'], nfdtx, pct_pars['dist'], ffdtx1, pct_pars['dist'] + ff_offset)
    umvct(s8vg, pct_pars['slit_vg'] + 0.5, s8hg, pct_pars['slit_hg'] + 0.5)
    umvct(samtz, samtz_offset)
    ct(pct_pars['exp_time'])

def marana_dct(dct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    #for i in range (3):
    #    umv(tfz, -0.32, tfy, 14.445)
    marana.image.roi=[0,0,2048,2048]
    bsin()
    umvct(s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umvct(s8vg, dct_pars['slit_vg'] + 0.1, s8hg, dct_pars['slit_hg'] + 0.1)
    umvct(nfdtx, dct_pars['dist'])
    sct(dct_pars['exp_time'])



# -*- coding: utf-8 -*-
import os
import numpy as np

    
def define_dct_pars():
    dct_pars={}
    dct_pars['active'] = True
    dct_pars['start_pos'] = -22.9
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.05
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -1
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 11
    dct_pars['slit_hg'] = 0.55
    dct_pars['slit_vg'] = 0.25
    dct_pars['dist'] = 5
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 0
    dct_pars['shift_step_size'] = 0.225
    dct_pars['nof_shifts'] = 23
    dct_pars['scan_type'] = 'fscan'
    return dct_pars

def define_pct_pars():
    pct_pars={}
    pct_pars['active'] = True
    pct_pars['start_pos'] = 0
    pct_pars['step_size'] = 0.25
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.08
    pct_pars['refon'] = pct_pars['num_proj']
    pct_pars['ref_int'] = 10
    pct_pars['ref_step'] = -2.5
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 41
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = samtz
    pct_pars['samtz_cen'] = 0
    pct_pars['shift_step_size'] = 1
    pct_pars['nof_shifts'] = 2
    pct_pars['slit_hg'] = 1.2
    pct_pars['slit_vg'] = 1.
    pct_pars['dist'] = 250
    pct_pars['scan_type'] = 'fscan'
    pct_pars['roi_hv'] = [900, 820]
    return pct_pars
    
def define_ff_pars():
    ff_pars={}
    ff_pars['active'] = True
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 0.5
    ff_pars['num_proj'] = 180 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.65
    ff_pars['slit_vg'] = 1.5
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = 0
    ff_pars['shift_step_size'] = 0.225
    ff_pars['nof_shifts'] = 1
    ff_pars['cpm18_detune'] = 0
    return ff_pars
    
def define_sff_pars():
    sff_pars={}
    sff_pars['active'] = True
    sff_pars['start_pos'] = -2.5
    sff_pars['step_size'] = 0.5
    sff_pars['num_proj'] = 180 / sff_pars['step_size']
    sff_pars['exp_time'] = 0.08
    sff_pars['slit_hg'] = 0.1
    sff_pars['slit_vg'] = 0.1
    sff_pars['mode'] = 'ZIGZAG'
    sff_pars['cpm18_detune'] = 0.0
    return sff_pars
    
def texture_scan(start, num_scans, ff_pars, sleep_time=0):
    for i in np.arange(start, start+num_scans):
        newdataset(f'ff_{i:04}')
        finterlaced(diffrz, ff_pars['start_pos'], ff_pars['step_size'], ff_pars['num_proj'], ff_pars['exp_time'], mode='ZIGZAG')
        nanodacpool_induction.temp
        sleep(sleep_time)
    return

def load_ramp_by_ascan(start, stop, npoints, exp_time, loadstep, pct_pars):
    marana_large_beam(pct_pars)
    newdataset('loadramp_%d' %  loadstep)
    ascan(stress, start, stop, npoints, exp_time, stress_cnt, marana)
    return

def sff_one_grain(scanname, sff_pars, grain):
    if sff_pars['cpm18_detune'] > 0:
        user.cpm18_goto(cpm18_tuned + sff_pars['cpm18_detune'])
    umv(s7vg, 0.5, s7hg, 0.5, s8vg, 0.05, s8hg, 0.05) 
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, bigy_in)
    umv(samtx, grain['samtx'], samty, grain['samty'], samtz, grain['samtz'])
    stage_diffty_zero = diffty_offset

    scanstarty= -0.1  # in mm
    scanendy=0.101
    scanstepy=0.01
    
    scanstartz= -0.15  # in mm
    scanendz=0.1501 
    scanstepz=0.01    

    umvr(samtz, scanstartz)
    z_scans_number = (scanendz - scanstartz) / scanstepz
    newdataset(scanname)
    for i in range(int(z_scans_number)):
        umvr(samtz, scanstepz)
        for ty in np.arange(scanstarty,scanendy,scanstepy):
            umv(diffty, stage_diffty_zero+ty)
            print(f"Scan pos: y: {ty} couche {i}")
            finterlaced( diffrz, sff_pars['start_pos'], acq_size = sff_pars['step_size'], npoints = sff_pars['num_proj'], acq_time = sff_pars['exp_time'], mode = sff_pars['mode'])

    umv(diffrz,0)      
    umv(diffty, stage_diffty_zero)
    umv(bigy, bigy_out)
    diffty_airpad.off()
    user.cpm18_goto(cpm18_tuned)
    
    umv(samtz,0)
    print("All done!")
    

def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")



def define_pars(load_list):
    """Produces default input parameters for a load sequence (see next function)
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    for load in load_list:
        step_pars = {}
        step_pars['dct_pars'] = define_dct_pars()
        step_pars['pct_pars'] = define_pct_pars()
        step_pars['ff_pars'] = define_ff_pars()
        step_pars['sff_pars']= define_sff_pars()
        step_pars['target'] = load
        step_pars['load_step'] = step
        step = step + 1
        par_list.append(step_pars)        
    return par_list

def define_pars_one_load():
    """Produces default input parameters for one load
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    step_pars['dct_pars'] = define_dct_pars()
    step_pars['pct_pars'] = define_pct_pars()
    step_pars['tt_pars'] = define_tt_pars()
    step_pars['ff_pars'] = define_ff_pars()
    step_pars['sff_pars']= define_sff_pars()
    step_pars['target'] = load
    step_pars['load_step'] = step
    par_list.append(step_pars)
    return par_list


def load_sequence(par_list):
    """Load Experiment Master Sequence.
    
    This function will perform scan sequences (pct, dct, tt) for a list of target load values defined in "par_list" 
    Note: grain_list is currentatty produced by a matlab script and can be re-created / imported via:  grain_list = user.read_tt_info([list_of_grain_ids])
    par_list can be created via: par_list = user.define_pars()  and sub-parameters for dct, pct, tt can be adapted to cope with increasing mosaicity

    """
    for step_pars in par_list:
        dct_pars = step_pars['dct_pars']
        pct_pars = step_pars['pct_pars']
        ff_pars = step_pars['ff_pars']
        sff_pars = step_pars['sff_pars']
        target = step_pars['target']
        load_step = step_pars['load_step']
        load_step_sequence(dct_pars, pct_pars, ff_pars, sff_pars, target, load_step)
    return

def load_step_sequence(pct_pars=define_pct_pars(), 
                       dct_pars=define_dct_pars(), 
                       ff_pars=define_ff_pars(),
                       doPCT=True,
                       doDCT=True,
                       doFF=True):
    """Performs a loadramp to the new target value and launches PCT, DCT and a series of TT scans at this new target load
    """
    # move the furnace up
    umv(furnace_z, 140)
    
    
    ## Change setpoint of loadrig
    #regul_on(target)
    ## PCT
    if doPCT and pct_pars['active']:
        print("Start PCT acquisition")
        maranain(pct_pars['dist'])
        marana_pct(pct_pars)
        scan_name = 'pct_' 
        pct_zseries(pct_pars, datasetname = scan_name)
        
        #tomo_by_fscan_dict(scan_name, pct_pars)
        #fulltomo.full_turn_scan(scan_name, start_pos = pct_pars['start_pos'])
    ## DCT
    if doDCT and dct_pars['active']:
        print("Start DCT aquisition")
        frelon1in(dct_pars)
        scan_name = 'dct_'
        dct_zseries(dct_pars, datasetname = scan_name)
    ## 3DXRD
    if doFF and ff_pars['active']:
        print("Start FF acquisition")
        ffin(ff_pars)
        scan_name = 'ff_'   
        ff_zseries(ff_pars, scan_name)
     
    ## s-3DXRD
    #if sff_pars['active']:
    #    scan_name = 'sff_%dN_' % (target)
    #    tdxrd_pointscan(scan_name, sff_pars)
        #scan_name = 'sff_2d_%dN_g%d' % (target, grain_list[0]['gr_id'])
        #sff_one_grain(scan_name, sff_pars, grain_list[0])
    print("##########################################")
    print('finished scanning, putting marana back in')
    print("##########################################")
    maranain(pct_pars['dist'])
    marana_pct(pct_pars)
    return

import time

def temperature_ramp_pct(pct_pars=define_pct_pars()):

    sheh3.close()
    if d2ty.position >-195 or d1ty.position >-195:
        print('Frelon 1 in not in safe position!!!')
        return
    umv(ffdtx1, 650, nfdtx, 200, ffdtz1, 100)
    ACTIVE_MG.enable("marana:image")
    ACTIVE_MG.disable("frelon3*")
    ACTIVE_MG.disable("frelon1*")
    umv(nfdtx, pct_pars['dist'])
    umv(d3ty, 0)
    print("ready to collect marana data") 
    sheh3.open()
    ct(0.02)
    marana_pct(pct_pars)
    
    
    nanodac3.setpoint = 22
    dataset_name = 'pct1_RT' 
    newdataset(dataset_name)
    fulltomo.full_turn_scan(dataset_name, start_pos = pct_pars['start_pos'])
    
    print('Ramping to 380C at 10 C/min')
    nanodac3.ramprate = 10
    nanodac3.setpoint = 380
    i = 0
    while nanodac3_axis.position < 375:
        i = i + 1
        dataset_name = 'pct2_ramp380_%i'%i 
        newdataset(dataset_name) 
        fulltomo.full_turn_scan(dataset_name, start_pos = pct_pars['start_pos'])
    
    print('Plateau at 380C for 10 mins')
    start_time = time.time()
    i = 0
    while (time.time() - start_time) < (60*9):
        i = i + 1
        dataset_name = 'pct3_plateau380_%i'%i  
        newdataset(dataset_name)
        fulltomo.full_turn_scan(dataset_name, start_pos = pct_pars['start_pos'])
    
    print('Ramp to 550C at 1 C/min')
    start_time = time.time()
    nanodac3.ramprate = 1
    nanodac3.setpoint = 550
    i = 0
    while nanodac3_axis.position < 545:
        i = i + 1
        dataset_name = 'pct4_ramp550_%i'%i  
        newdataset(dataset_name)
        fulltomo.full_turn_scan(dataset_name, start_pos = pct_pars['start_pos'])
        
    print('Plateau at 550C for 10 mins')
    start_time = time.time()
    i = 0
    while (time.time() - start_time) < (60*9):
        i = i + 1
        dataset_name = 'pct5_plateau550_%i'%i  
        newdataset(dataset_name)
        fulltomo.full_turn_scan(dataset_name, start_pos = pct_pars['start_pos'])
        
    print('Ramping to 22C at 10 C/min')
    nanodac3.ramprate = 10
    nanodac3.setpoint = 22
    i = 0
    while nanodac3_axis.position > 35:
        i = i + 1
        dataset_name = 'pct6_cool_%i'%i  
        newdataset(dataset_name)
        fulltomo.full_turn_scan(dataset_name, start_pos = pct_pars['start_pos'])
 
 
def safe_temperature_ramp_pct(pct_pars=define_pct_pars()):
    try:
        temperature_ramp_pct(pct_pars)
    except:
        nanodac3.setpoint = 22 
        
        
def safe_temperature_ramp_ff(ff_pars=define_ff_pars()):
    try:
        temperature_ramp_ff(ff_pars)
    except:
        nanodac3.setpoint = 22
       
 
def temperature_ramp_ff(ff_pars=define_ff_pars()):

    sheh3.close()
    if d2ty.position >-195 or d1ty.position >-195:
        print('Frelon 1 in not in safe position!!!')
        return
    umv(d3ty, d3_out)
    umv(nfdtx, 100)
    umv(ffdtz1, 0, ffdtx1, ff_dist)
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.disable("frelon1:image")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umvct(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umvct(s8hg, ff_pars['slit_vg'] + 0.2, s8hg, ff_pars['slit_hg'] + 0.2)
    print("ready to collect far-field data")
    sheh3.open()
    ct(0.02)
    
    nanodac3.setpoint = 22
    dataset_name = 'ff1_RT' 
    newdataset(dataset_name)
    finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    
    print('Ramping to 380C at 10 C/min')
    nanodac3.ramprate = 10
    nanodac3.setpoint = 380
    i = 0
    while nanodac3_axis.position < 375:
        i = i + 1
        dataset_name = 'ff2_ramp380_%i'%i 
        newdataset(dataset_name)
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    
    print('Plateau at 380C for 10 mins')
    start_time = time.time()
    i = 0
    while (time.time() - start_time) < (60*9):
        i = i + 1
        dataset_name = 'ff3_plateau380_%i'%i 
        newdataset(dataset_name)
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    
    print('Ramp to 550C at 1 C/min')
    start_time = time.time()
    nanodac3.ramprate = 1
    nanodac3.setpoint = 550
    i = 0
    while nanodac3_axis.position < 545:
        i = i + 1
        dataset_name = 'ff4_ramp550_%i'%i 
        newdataset(dataset_name)
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
        
    print('Plateau at 550C for 10 mins')
    start_time = time.time()
    i = 0
    while (time.time() - start_time) < (60*9):
        i = i + 1
        dataset_name = 'ff5_plateau550_%i'%i 
        newdataset(dataset_name)
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
        
    print('Ramping to 22C at 10 C/min')
    nanodac3.ramprate = 10
    nanodac3.setpoint = 22
    i = 0
    while nanodac3_axis.position > 35:
        i = i + 1
        dataset_name = 'ff6_cool_%i'%i 
        newdataset(dataset_name)
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])



def loop_grains(grain_list, tt_pars, target):
    """Loops through a list of Topotomo grains  and performs refinement + TT scan_acquisition
    :param dict grain_list:  list of grain structures containing Topotomo alignment values
    :param dict tt_pars: Topotomo scan parameters
    :param int step: running number of load step in load-sequence  
    """
 
    for i in range(len(grain_list)):
        topotomo_tilt_grain_dict(grain_list[i])
        newdataset('grain_%04d_%dN_' % (grain_list[i]['gr_id'], target))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment_int(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time)
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time, step=target)
        sleep(1)
        update_grain_info(grain_list[i], target)
        print('values updated in tt_grain record')
        #n_acq_images = 10 
        #diffry_start = grain_list[i]['diffry'] - 0.25
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    sam_dct_pos()
    return grain_list

def marana_tt(grain_list, tt_pars, target):
    tfoh1.set(0,'Be')
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    umv(s8hg, tt_pars['slit_hg'] + 0.2, s8vg, tt_pars['slit_vg'] + 0.2)
    umv(d3tz, d3tz_pos_tt, nfdtx, tt_pars['dist'])
    marana.image.roi = tt_pars['image_roi']
    marana.roi_counters.set('roi1', tt_pars['counter_roi'])
    ACTIVE_MG.enable('marana:roi_counters:roi1_avg')
    for i in range(len(grain_list)):
        newdataset('grain_%04d_%dN_' % (grain_list[i]['gr_id'], target))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time, tt_pars['scan_mode'])
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'], tt_pars['scan_mode'])
        sleep(1)
        update_grain_info(grain_list[i], target)
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    sam_dct_pos()
    return grain_list

        
def refine_tt_alignment(tt_grain, ang_step=0.05, search_range=1, exp_time=0.5, scan_mode='TIME'):
    """Refine the topotomo samrx, samry alignment.
    
    This function runs 4 base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use (in degrees).
    :param float search_range: the base tilt angular search range in degrees.
    """
     # half base tilt range in degree
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/visitor/ma4925/id11/Inconel/'
    sample_name = 'Inconel'
    
    # open a script to gather all acquisition commands
    print(tt_grain)
    gid = tt_grain['gr_id']
    print('find range for grain %d\n' % gid)
    # check that diffry is negative (Bragg alignment)
    #if tt_grain['diffry'] > 0:
    #    raise(ValueError('diffry value should be negative, got %.3f, please check your data' % tt_grain['diffry']))
        
    # align our grain
    topotomo_tilt_grain_dict(tt_grain)

    # define the ROI automaticalatty as [710, 710, 500, 500]
    #frelon16.image.roi = [0, 0, 1920, 1920]
    #ct(0.1)
    if 'roi' in tt_grain:
        frelon16.roi_counters.set('roi1', tt_grain['roi'])
    else:
        frelon16.roi_counters.set('roi1', [200, 200, 400, 400])
    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    
    # first scan at 0 deg
    umvct(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_000, end_angle_000, cen_angle_000 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # second scan at 180 deg
    umv(diffrz, 180)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_180, end_angle_180, cen_angle_180 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samry by half the difference
    samry_offset = 0.5 * (cen_angle_000 - cen_angle_180)
    print("moving samry by %.3f, final position: %.3f" % (samry_offset, tt_grain['samry'] + samry_offset))
    umvr(samry, samry_offset)

    # third scan at 270 deg
    umv(diffrz, 270)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_270, end_angle_270, cen_angle_270 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # fourth scan at 90 deg
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_090, end_angle_090, cen_angle_090 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samrx by half the difference
    samrx_offset = 0.5 * (cen_angle_090 - cen_angle_270)
    print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    #samrx_offset =  tt_grain['diffry'] - cen_angle_090
    #print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    umvr(samrx, samrx_offset)

    # update values in tt_grain
    tt_grain['samrx'] += samrx_offset
    tt_grain['samry'] += samry_offset
    return tt_grain

def find_range(tt_grain, ang_step=0.05, search_range=0.3, exp_time=0.1, scan_mode='TIME', step=1):
    """Find the topotomo angular range.
    
    This function runs two base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition. Note that this function 
    assumes that the grain has been aligned already.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use.
    :param float search_range: the base tilt angular search range in degrees.
    """
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/visitor/ma4925/id11/'
    sample_name = 'Inconel'
    
    # open a script to gather all acquisition commands
    gid = tt_grain['gr_id']
    cmd_path = os.path.join(data_dir, 'tt_acq_grain_%d_step_%d.py' % (gid, step))
    f = open(cmd_path, 'w')
    f.write("def tt_acq():\n\n")
    f.write("    # activate ROI\n")
    f.write("    frelon16.image.roi = [561, 561, 800, 800]\n")
    f.write("    frelon16.roi_counters.set('roi1', [200, 200, 400, 400])\n")
    f.write("    ct(0.1)\n")
    f.write("    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')\n\n")
    
    print('find range for grain %d\n' % gid)
    
    # see if we need to create a new dataset or not
    #dataset = 'grain_%04d_checkrange_step_%d' % (gid, step)
    #newdataset(dataset)

    # define the ROI automaticalatty as [710, 710, 500, 500]
    #frelon16.roi_counters.set('roi1', [710, 710, 500, 500])
    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    #ACTIVE_MG.enable('marana:roi_counters:roi1_avg')
    # first scan at 90 deg
    datasetname = SCAN_SAVING.dataset_name
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_90, end_angle_90 = get_limits(fscan.get_data())
    #start_angle_90, end_angle_90 = get_limits_by_seg(fscan.get_data(), data_dir, sample_name, datasetname, step, scan_fold_ind = 5)
    diffry_range_90 = max(tt_grain['diffry'] - start_angle_90, end_angle_90 - tt_grain['diffry'])

    # second scan at 0 deg
    umv(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode= scan_mode)
    start_angle_00, end_angle_00 = get_limits(fscan.get_data())
    #start_angle_00, end_angle_00 = get_limits_by_seg(fscan.get_data(), data_dir, sample_name, datasetname, step, scan_fold_ind = 6)
    diffry_range_00 = max(tt_grain['diffry'] - start_angle_00, end_angle_00 - tt_grain['diffry'])

    # use the upper range value to define topotomo bounds
    print('diffry_range_00=%.3f - diffry_range_90=%.3f' % (diffry_range_00, diffry_range_90))
    diffry_range = max(diffry_range_00, diffry_range_90)

    # now find out the largest range to cover the grain
    diffry_start = tt_grain['diffry'] - diffry_range
    diffry_end = tt_grain['diffry'] + diffry_range
    n_acq_images = (diffry_end - diffry_start) / ang_step

    # write out the topotomo command for bliss
    f.write("    # create a new data set\n")
    f.write("    newdataset('grain_%04d_step_%d')\n\n" % (gid, step))
    f.write("    # acquisition for grain %d\n" % gid)
    f.write("    user.topotomo_tilt_grain(%d)\n" % gid)
    f.write("    umv(samrx, %.3f)\n" % tt_grain['samrx'])
    f.write("    umv(samry, %.3f)\n" % tt_grain['samry'])
    f.write("    fscan2d(diffrz, 0, 10, 36, diffry, %.3f, %.3f, %d, 0.1, scan_mode=tt_pars['scan_mode'])\n\n" % (diffry_start, ang_step, n_acq_images))
    f.close()
    return diffry_start, n_acq_images

def get_limits(scan_data, thres=0.1, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry = scan_data['diffry']
    # work out the angular step
    ang_step = diffry[1] - diffry[0]
    intensity = scan_data['frelon16:roi_counters:roi1_avg']
    # background correction
    bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) -1
    start_angle = diffry[start_pos] - padding * ang_step
    end_angle = diffry[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, number of images={}'.format(start_angle, end_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle
    
def get_limits_and_weighted_cen_angle(scan_data, thres=0.1, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry_vals = scan_data['diffry']
    # work out the angular step
    ang_step = diffry_vals[1] - diffry_vals[0]
    intensity = scan_data['frelon16:roi_counters:roi1_avg']
    # background correction
    #bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    bg = np.min(intensity)
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) - 1
    end_pos = max(start_pos + 1, end_pos)
    weighted_cen_pos = 0;
    for i_pos in range(start_pos, end_pos, 1):
        weighted_cen_pos += i_pos * intensity[i_pos]
    weighted_cen_pos = weighted_cen_pos / np.sum(intensity[start_pos:end_pos])
    # compute the angle using linear interpolation
    weighted_cen_angle = (diffry_vals[start_pos] * (end_pos - weighted_cen_pos) + diffry_vals[end_pos] * (weighted_cen_pos - start_pos)) / (end_pos - start_pos)
    start_angle = diffry_vals[start_pos] - padding * ang_step
    end_angle = diffry_vals[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, weighted_cen_angle={}, number of images={}'.format(start_angle, end_angle, weighted_cen_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle, weighted_cen_angle

import h5py
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, TextBox
from matplotlib.colors import ListedColormap
from scipy.ndimage import label
""" The object is used for the UI of determining the diffry limits in TT.
(used by get_limits_by_seg(...)) """
class DiffryRangeDeterminator:
    def __init__(self, axs, fig_handle, image_stack, downsampling_ratio = 32, padding = 10, thres_log = 1):
        self.padding = padding
        self.ax = axs
        self.fig = fig_handle
        self.dsampling = downsampling_ratio
        self.image_stack = image_stack
        self.slices, rows, cols = image_stack.shape
        image_stack = self.downsample_image_xy()
        self.ds_image_stack = image_stack
        self.grain_mask = 2 * (image_stack > 10**thres_log)
        self.threshold_log = thres_log
        self.threshold_former = 0
        self.selected_labels = []
        self.slice_ind = 0
        self.im = [0, 0]
        self.fresh_image()
        self.updateSlice(1)
    def downsample_image_xy(self):
        slices, rows, cols = self.image_stack.shape
        row_pads = rows % self.dsampling
        col_pads = cols % self.dsampling
        row_padshalf = row_pads // 2
        col_padshalf = col_pads // 2
        dsimgstack = np.pad(self.image_stack, ((0, 0), (row_padshalf, row_pads - row_padshalf), (col_padshalf, col_pads - col_padshalf)))
        dsimgstack = dsimgstack.reshape((slices, (rows + row_pads) // self.dsampling, self.dsampling, (cols + col_pads) // self.dsampling, self.dsampling))
        dsimgstack = np.sum(np.sum(dsimgstack, 4), 2)
        return dsimgstack
    def updateSlice(self, slice_ind):
        slice_ind = int(slice_ind - 1)
        self.im[0].set_data(self.image_stack[slice_ind, :, :])
        self.im[1].set_data(self.grain_mask[slice_ind, :, :])
        self.ax[0].set_xlabel('slice %d' % (slice_ind + 1))
        self.slice_ind = slice_ind
        self.im[0].axes.figure.canvas.draw()
        self.im[1].axes.figure.canvas.draw()
    def updateThres(self, thres):
        self.threshold_log = thres
        thres = 10**thres
        self.grain_mask = 2 * (self.ds_image_stack > thres)
        self.im[1].set_data(self.grain_mask[self.slice_ind, :, :])
        self.ax[1].set_xlabel('threshold %.6s' % thres)
        self.im[1].axes.figure.canvas.draw()
    def rightClickSelSpots(self, event):
        if event.button != 3:
            return
        try:
            x_ind = int(event.xdata)
            y_ind = int(event.ydata)
        except:
            return
        x_ind = max(x_ind, 0)
        y_ind = max(y_ind, 0)
        if self.grain_mask[self.slice_ind, y_ind, x_ind] > 0:
            print([x_ind, y_ind, self.slice_ind])
            if self.threshold_log != self.threshold_former:
                self.reset_labels()
                self.threshold_former = self.threshold_log
            new_label = self.labeled_mask[self.slice_ind, y_ind, x_ind]
            if new_label in self.selected_labels:
                self.selected_labels.remove(new_label)
                self.grain_mask = np.uint(self.grain_mask) + np.uint(self.labeled_mask == new_label)
            else:
                self.selected_labels.append(new_label)
                self.grain_mask = np.uint(self.grain_mask) - np.uint(self.labeled_mask == new_label)
            self.im[1].set_data(self.grain_mask[self.slice_ind, :, :])
            self.im[1].axes.figure.canvas.draw()
    def confirmandclose(self, event):
        plt.close(self.fig)
    def read_padding(self, text):
        self.padding = int(text)
    def change_downsampling(self, text):
        self.dsampling = int(text)
        self.ds_image_stack = self.downsample_image_xy()
        self.updateThres(self.threshold_log)
        self.reset_labels()
        self.fresh_image()
    def reset_labels(self):
        self.labeled_mask, nof_features = label(self.grain_mask)
        self.selected_labels = []
    def fresh_image(self):
        self.im[0] = self.ax[0].imshow(self.image_stack[self.slice_ind, :, :])
        cmap_3color = [[0., 0., 0.], [0.5, 0., 0.], [1., 1., 1.]]
        cmap_3color = ListedColormap(cmap_3color)
        self.im[1] = self.ax[1].imshow(self.grain_mask[self.slice_ind, :, :], cmap = cmap_3color)
    def getSliceBounds(self):
        if len(self.selected_labels):
            slice_bounds = np.sum(np.sum(self.labeled_mask == self.selected_labels[0], 2), 1)
            for ii_label in range(1, len(self.selected_labels), 1):
                slice_bounds += np.sum(np.sum(self.labeled_mask == self.selected_labels[ii_label], 2), 1)
            slice_bounds = np.nonzero(slice_bounds)
            return [slice_bounds[0][0], slice_bounds[0][-1]]
        else:
            return []
""" Determine the limits of diffry according to the mask of the spot """
def get_limits_by_seg(scan_data, data_dir, sample_name, datasetname, step, scan_fold_ind = 5, thres=0.1):
    diffry_array = scan_data['diffry']
    h5file_path = os.path.join(data_dir, sample_name, sample_name + '_' + datasetname, sample_name + '_' + datasetname + '.h5')
    scan_fold_ind = str(scan_fold_ind)
    flag_readfile = 'y'
    while(flag_readfile != 'n'):
        scan_fold_ind = input('\nThe index of scan: [' + scan_fold_ind + '] ') or scan_fold_ind
        h5file_path = input('The path of data: [' + h5file_path + '] ') or h5file_path
        flag_confirm = input('Path: ' + h5file_path + '\nScan index: ' + scan_fold_ind + '\nAre you satisfied with the path and the scan index? y/n [y] ')
        if flag_confirm == 'n':
            continue
        try:
            img = h5py.File(h5file_path, 'r')
            keys_measurement = list(img[str(scan_fold_ind)+'.1']['measurement'].keys())
            if 'frelon16' in keys_measurement:
                camera_name = 'frelon16'
            elif 'marana' in keys_measurement:
                camera_name = 'marana'
            img = img[str(scan_fold_ind)+'.1']['measurement'][camera_name]
            flag_readfile = 'n'
        except:
            flag_readfile = input('Failed to read file. Try again? y/n [y]')
            
    bg = np.median(img)
    img = img - bg
    nof_slices, nof_rows, nof_cols = img.shape
    fig, axs = plt.subplots(1, 2)
    range_determinator = DiffryRangeDeterminator(axs, fig, img, downsampling_ratio = 32, padding = 10, thres_log = np.log10(thres * img.max()))
    ax_slice = plt.axes([0.2, 0.02, 0.6, 0.03])
    simgslice = Slider(
    ax_slice, "Slice", 1, nof_slices,
    valinit=1, valstep=1)
    simgslice.on_changed(range_determinator.updateSlice)
    ax_thres = plt.axes([0.05, 0.2, 0.03, 0.6])
    simgthres = Slider(
    ax_thres, "Threshold", 0, np.log10(range_determinator.ds_image_stack.max()),
    valinit=0, orientation = 'vertical')
    simgthres.on_changed(range_determinator.updateThres)
    fig.canvas.mpl_connect('button_press_event', range_determinator.rightClickSelSpots)
    dstextboxax = plt.axes([0.6, 0.85, 0.2, 0.075])
    dstextbox = TextBox(dstextboxax, 'Downsampling', initial=str(range_determinator.dsampling))
    dstextbox.on_submit(range_determinator.change_downsampling)
    paddingtextboxax = plt.axes([0.1, 0.85, 0.2, 0.075])
    paddingtextbox = TextBox(paddingtextboxax, 'Padding', initial=str(range_determinator.padding))
    paddingtextbox.on_submit(range_determinator.read_padding)
    confirmax = plt.axes([0.8, 0.025, 0.1, 0.04])
    button_confirm = Button(confirmax, 'Ok', hovercolor='0.975')
    button_confirm.on_clicked(range_determinator.confirmandclose)
    plt.show()
    slice_bounds = range_determinator.getSliceBounds()
    if not len(slice_bounds):
        flag_continue = input('No limit is determined. Would you like to re-run the code? y/n [y]: ')
        start_angle = 'nan'
        end_angle = 'nan'
    else:
        padding = range_determinator.padding
        start_pos = slice_bounds[0]
        end_pos = slice_bounds[1]
        ang_step = diffry_array[1] - diffry_array[0]
        start_angle = diffry_array[start_pos] - padding * ang_step
        end_angle = diffry_array[end_pos] + padding * ang_step
        print('start_angle={}, end angle={}, number of images={} (padding={}, length of mask={})'.format(start_angle, end_angle, int((end_angle - start_angle) / ang_step + 1), padding, slice_bounds[1] - slice_bounds[0] + 1))
        flag_continue = input('Would you like to re-run the code? y/n [y]: ')
    if flag_continue != 'n':
        start_angle, end_angle = get_limits_by_seg(scan_data, data_dir, sample_name, step, scan_fold_ind)
    return start_angle, end_angle



import json
import time
import os

class NumpyEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self,obj)


def writedictionaryfile(dictname,filename):
    """to write dictionary to file"""
    datatowrite = json.dumps(dictname)
    with open('%s'%filename, 'w') as f:
        f.write(datatowrite)
    print("Dictionary written as %s"%filename)
    return

def appenddictionaryfile(dictname,filename):
    """to write dictionary to file"""
    if os.path.exists('%s'%filename):
        with open('%s'%filename, 'r+') as f:
            data_to_write = [json.load(f)]
            dictname['time'] = str(time.ctime())
            data_to_write.append(dictname)
            f.seek(0)
            json.dump(data_to_write, f)
    else:
        newdict = dictname
        newdict['time'] = str(time.ctime())
        with open('%s'%filename, 'w') as f:
            json.dump(newdict, f)
            #f.write(datatowrite)
    print("Dictionary appended to %s"%filename)
    return

def readdictionaryfile(filename):
    """to read dictionary from file, returns dictionary"""
    with open('%s'%filename, 'r') as f:
        readdata = [json.loads(f.read())]
    print("Dictionary read from %s"%filename)
    return readdata[-1]


def update_grain_info(grain, target):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    writedictionaryfile(grain,'%s/%s_g%s_%dN'%(basedir,samplename,grain['gr_id'],target))
    #appenddictionaryfile(grain,'%s_g%s_history'%(SCAN_SAVING.collection_name,grain['gr_id']))
    return

def get_json_grain_info(grain_number):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    return readdictionaryfile('%s/%s_g%s'%(basedir,samplename,str(grain_number)))

def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])
    
def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.2, s8hg, pars['slit_hg'] + 0.2)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def tomo_by_finterlaced_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.2, s8hg, pars['slit_hg'] + 0.2)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        #fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)

def ref_scan_samy(exp_time, nref, ystep, omega, scanmode):
    umvr(samy, ystep)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umvr(samy, -ystep)
    
def ref_scan_difftz(exp_time, nref, zstep, omega, scanmode):
    difftz0 = difftz.position
    umv(difftz, difftz0 + zstep)
    print("fscan(diffrz, %6.2f, 0.1, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.1, nref, exp_time, scan_mode = scanmode)
    umv(difftz, difftz0)
    
class LoadLoop:
    def __init__(self):
        self._task = None
        self._stop = False
        self._sleep_time = 1
        self._filepath = None
        
    def start(self,target,loadstep,filepath,sleep_time=1.,time_step=0.5):
        self._filepath = filepath
        if self._task:
            self._stop = True
            self._task.get()

        self._sleep_time = sleep_time
        self._stop = False
        self._task = gevent.spawn(self._run,target,loadstep,time_step)

    def stop(self):
        self._stop = True
        if self._task: 
           self._task.get()

    def _run(self,target,loadstep,time_step):
        newdataset('loadramp_%d' %  loadstep)
        with open(self._filepath,'a') as f:
            while not self._stop:
                load_ramp_by_target(target,0,loadstep,time_step,to_file=f)
                gevent.sleep(self._sleep_time)

def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        y_shift = np.tan(np.deg2rad(samrx.position))*dct_pars['zmot'].position
        x_shift = -np.tan(np.deg2rad(samry.position))*dct_pars['zmot'].position
        umv(samty, y_shift, samtx, x_shift) 
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'], samty, 0, samtx, 0)
    print('DCT Succeed')
    return('Succeed')
    
def ff_zseries(ff_pars, datasetname = 'ff'):
    #user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    #umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    #user.cpm18_goto(cpm18_tuned)
    #umv(u22, u22_tuned)
    print('ff_zseries succeed')
    return('Succeed')
    
def pct_zseries(pct_pars, datasetname = 'pct'):
    shift_step_size = abs(pct_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = pct_pars['samtz_cen'] - (pct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(pct_pars['samtz_cen']))
    if(offset_samtz_pos < pct_pars['zmot'].low_limit or offset_samtz_pos + (pct_pars['nof_shifts'] - 1) * shift_step_size > pct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(pct_pars['nof_shifts'])):
        umv(pct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        scan_name = datasetname + str(iter_i + 1)
        fulltomo.full_turn_scan(scan_name, start_pos = pct_pars['start_pos'])
    umv(pct_pars['zmot'], pct_pars['samtz_cen'])
    print('pct_zseries succeed')
    return('Succeed')

def tomo_series_cst_load(start, num_scans, target, sleep_time=0, pct_pars=None):
    for i in np.arange(start, start+num_scans):
        tomo.full_turn_scan(str(i))
        load_ramp_by_target(target, 0.05, 1)
	#dct_marana_dict(str(i), pct_pars)
        sleep(sleep_time)
    return

def ftomo_series(scanname, start, num_scans, sleep_time=0, pars=None):
    for i in np.arange(start, start+num_scans):
        newdataset(scanname + str(i))
        umv(diffrz, pars['start_pos']);
        fsh.disable()
        fsh.close()
        print("taking dark images")
        ftimescan(pars['exp_time'], pars['nref'],0)
        fsh.enable()
        if not(i%pars['ref_int']):
            print("taking flat images")
            ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'], pars['scan_mode'])
        else:
            print("skipping flat images - stay where we are")
            ftimescan(pars['exp_time'], 10,0)
        print("taking projections...")
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        print("resetting diffrz to 0")
        umv(diffrz, pars['start_pos']+360);
        diffrz.position=pars['start_pos'];
        diffrz.dial = diffrz.position
        sleep(sleep_time)
    return
    


def tdxrd_boxscan(scanname, ff_pars):
    print("using slit_vg %f and slit_hg %f" % (ff_pars['slit_vg'], ff_pars['slit_hg']))
    umv(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.2, s8hg, ff_pars['slit_hg'] + 0.2)
    newdataset(scanname)
    finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])

def tdxrd_pointscan(scanname, sff_pars):
    tfoh1.set(0,'Be')
    #if (sff_pars['cpm18_detune'] > 0):
    #    cpm18_goto(cpm18_tuned + sff_pars['cpm18_detune'])
    umv(s7vg, 0.5, s7hg, 0.5, s8vg, 0.05, s8hg, 0.05)
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, bigy_in)
    #umv(samtx, grain['samtx'], samty, grain['samty'], samtz, grain['samtz'])
    stage_diffty_zero = diffty_offset
    scanstarty= -0.36  # in mm
    scanendy=0.361 
    scanstep=0.005
    
    newdataset(scanname)

    for ty in np.arange(scanstarty,scanendy,scanstep):
        umv(diffty, stage_diffty_zero+ty)
        print(f"Scan pos: y: {ty} ")
        finterlaced( diffrz, sff_pars['start_pos'], acq_size = sff_pars['step_size'], npoints = sff_pars['num_proj'], acq_time = sff_pars['exp_time'], mode = sff_pars['mode'])

    umv(diffrz,0)      
    umv(diffty, stage_diffty_zero)
    umv(bigy, bigy_out)
    diffty_airpad.off()
    #if (sff_pars['cpm18_detune'] > 0):
    #    cpm18_goto(cpm18_tuned)
    print("All done!")
 
def update_flat():
    image_corr.take_dark()
    image_corr.take_flat()
    image_corr.dark_on()
    image_corr.flat_on()

def flat_on():
    image_corr.dark_on()
    image_corr.flat_on()

def flat_off():
    image_corr.dark_off()
    image_corr.flat_off()
    
##############DEFINED BY PEDRO IN 02/09/2022###################

def acquire_pca(dset_name, ndarks = 500, nflats = 1000, exp_time = 0.04):

    newdataset(dset_name)
    umvr(samy, -4)
    sheh3.open()
    #take flats
    ftimescan(exp_time, nflats)
    sheh3.close()
    #take darks
    ftimescan(exp_time, ndarks)
    umvr(samy, 4)
    sheh3.open()
    ct(exp_time)
    
def acquire_pca_tseries(dset_name, nacq = 10, sleep_time = 60, ndarks = 500, nflats = 1000, exp_time = 0.04):

    newdataset(dset_name)
    
    for _ in range(nacq):
		
	    sheh3.open()
	#take flats
	    ftimescan(exp_time, nflats)
	    sheh3.close()
	#take darks
	    ftimescan(exp_time, ndarks)
	    sleep(sleep_time)
    
    sheh3.open()
    ct(exp_time)
    
def g8_s1():
    
    newsample('g8_s1')
    sct(0.25)
    dists = [80, 100, 120, 150]
    
    for dist in dists:
        newdataset(f'pct_{dist}mm_corr')
        umvct(nfdtx, dist)
        fulltomo.full_turn_scan(f'pct_{dist}mm_corr', start_pos = -5)
        
    sheh3.close()
    
    
    
    
def refine_tt_alignment_int(tt_grain, ang_step=0.05, search_range=1, exp_time=0.5, scan_mode='TIME'):
    """Refine the topotomo samrx, samry alignments with interactive roi selection
    
    This function runs 4 base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use (in degrees).
    :param float search_range: the base tilt angular search range in degrees.
    """
     # half base tilt range in degree
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/visitor/ma4925/id11/Inconel/'
    sample_name = 'Inconel'
    
    gid = tt_grain['gr_id']
    print('find range for grain %d\n' % gid)
    
        
    # align our grain
    topotomo_tilt_grain_dict(tt_grain)
    
    # first scan at 0 deg
    umvct(diffrz, 0)
    if 'roi' in tt_grain:
        frelon16.roi_counters.set('roi1', tt_grain['roi'])
    else:
	    fscan(diffry, tt_grain['diffry'] - search_range, 2*search_range, 1, exp_time * search_range / ang_step, scan_mode = scan_mode)
	    edit_roi_counters(frelon16)
    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_000, end_angle_000, cen_angle_000 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # second scan at 180 deg
    umv(diffrz, 180)
    if 'roi' in tt_grain:
        frelon16.roi_counters.set('roi1', tt_grain['roi'])
    else:
	    fscan(diffry, tt_grain['diffry'] - search_range, 2*search_range, 1, exp_time * search_range / ang_step, scan_mode = scan_mode)
	    edit_roi_counters(frelon16)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_180, end_angle_180, cen_angle_180 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samry by half the difference
    samry_offset = 0.5 * (cen_angle_000 - cen_angle_180)
    print("moving samry by %.3f, final position: %.3f" % (samry_offset, tt_grain['samry'] + samry_offset))
    umvr(samry, samry_offset)

    # third scan at 270 deg
    umv(diffrz, 270)
    if 'roi' in tt_grain:
        frelon16.roi_counters.set('roi1', tt_grain['roi'])
    else:
	    fscan(diffry, tt_grain['diffry'] - search_range, 2*search_range, 1, exp_time * search_range / ang_step, scan_mode = scan_mode)
	    edit_roi_counters(frelon16)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_270, end_angle_270, cen_angle_270 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # fourth scan at 90 deg
    umv(diffrz, 90)
    if 'roi' in tt_grain:
        frelon16.roi_counters.set('roi1', tt_grain['roi'])
    else:
	    fscan(diffry, tt_grain['diffry'] - search_range, 2*search_range, 1, exp_time * search_range / ang_step, scan_mode = scan_mode)
	    edit_roi_counters(frelon16)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_090, end_angle_090, cen_angle_090 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samrx by half the difference
    samrx_offset = 0.5 * (cen_angle_090 - cen_angle_270)
    print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    #samrx_offset =  tt_grain['diffry'] - cen_angle_090
    #print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    umvr(samrx, samrx_offset)

    # update values in tt_grain
    tt_grain['samrx'] += samrx_offset
    tt_grain['samry'] += samry_offset
    tt_grain['roi'] = frelon16.roi_counters['roi1'].get_params()
    return tt_grain
    
    
def clicked_pos(x,y):
     c2mplot = flint().get_live_plot('default-curve')
     print("Select position in flint")
     pos = c2mplot.select_points(1)
     return pos[0][0]
    
    
###############################################################

def correct_lateral_drift():

    umvct(diffrz, 0)
    delta_samy_zero = np.tan(np.deg2rad(samrx.position))*samtz.position
    umvct(samy, delta_samy_zero)
    umvct(diffrz, 90)
    delta_samy_nnty = np.tan(np.deg2rad(samry.position))*samtz.position
    umvct(samy, delta_samy_nnty)


