
#rack support


def step_ramp_cryo():
    temps = [285,290,295]   #fill
    ramprate=360
    rest=20                #fill    K/hour
    
    for k in range(len(temps)):
        print('Setting temperature to',temps[k],'K \n')
        samplename='example'    #fill
        title = str(temps[k]) + 'K'
        ox800.ramprate = ramprate
        ox800.setpoint = temps[k]
        print('Going to',ox800.setpoint,' K at',ox800.ramprate,' K/hr \n')
        
    while ox800.is_ramping():
            #newdataset(samplename+'_rampscan_to_'+title)
            #print('Temperature is',ox800_axis.position,'K \n')
            #powder_scan()
            print('Temperature is',ox800_axis.position,'K \n')
            sleep(30)
        
    for s in range(rest,0,-1): 
            print(f"Stabilising temperature at {temps[k]}: {s} s ",end='\r')
            sleep(1)  
        print('Temperature is',ox800_axis.position,'K \n')
        newdataset(samplename+'_PDF_'+title)
        #loopscan(20, 0.2)




def step_ramp_blow():
    temps = [36, 30]     #fill
    ramprate=60                 #fill C/min
    threshold=0.5
    rest= 30
    
    for k in range(len(temps)):
        print('Setting temperature to',temps[k],'C \n')
        samplename='example'    #fill
        title = str(temps[k]) + 'C'
        euro1.ramprate = ramprate
        euro1.setpoint= temps[k]
        print('Going to',temps[k],' C at',ramprate,' C/min \n')
 
        while (abs(euro1_axis.position-temps[k]) > threshold):
            print('Temperature is',euro1_axis.position,'C \n')
            sleep(3)         
       
        for s in range(rest,0,-1): 
            print(f"Stabilising temperature at {temps[k]}: {s} s ",end='\r')
            sleep(1)  
        
        print('Temperature is',euro1_axis.position,'C \n')
        newdataset(samplename+'_PDF_'+title)
        #PDF_scan()




def PDF_scan(count_time=0.5, images=100): 
    print('predicted time in %f'%(images*count_time))
    loopscan(images, count_time)
    print('done...')


def powder_scan(count_time=0.5, images=2): 
    print('predicted time in %f'%(images*count_time))
    loopscan(images, count_time)
    print('done...')



def grid_scan():
    positions=[1,2,3]     #fill
    samples=['bla', 'ooo', 'kkk']     #fill
    for y in range(len(positions)):
        print('position is',positions[y],' for diffy \n')
        umv(diffy, positions[y])
        filename=samples[y]
        newdataset(filename)
        #hz or rock?
        PDF_scan()

    






