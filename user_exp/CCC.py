  
import os, sys
import numpy as np

import time
from bliss.common import cleanup






def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)




def switch2_marana3():
    ff_out()
    assert ffdtx1.position > 500
    assert ffdtz1.position > 300
    umv(nfdtx,100)
    umv(d3ty,0,d3tz,0)
    umv(atty,-10,attrz,0)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    
    
def switch2_ff():
    marana3_out()
    assert d3ty.position > 140
    umvct(s7hg,0.2,s7vg,0.2,s8vg,0.25)
    umv(atty,0,attrz,-6)
    umv(ffdtz1,0)
    umv(ffdtx1,400)
    ACTIVE_MG.disable('marana3*')
    ACTIVE_MG.enable('frelon3*')
    

def ff_out():
    umv(ffdtx1,600)
    umv(ffdtz1,350)

def marana3_out():
    umv(nfdtx,100)
    umv(d3ty,150,d3tz,80)
    
    
def goto_position_model(pos_in_model):
    ref_pos_lab = [-3.89, -18.99, 5.10668]
    
    pos_in_model = np.array(pos_in_model)
    ref_pos_model = np.array([7.787, -11.028, 26.502]) # x is - lab Y, y is lab X, z is lab Z
    pos_diff = pos_in_model - ref_pos_model
    # convert to lab system at ID11
    umvr(samtx, -pos_diff[1])
    umvr(samty, pos_diff[0])
    umvr(samtz, pos_diff[2])
    


def area_scan(ystep = 0.15, zstep = 0.15):
    umvct(samtx, -3.89, samty, -18.99, samtz, 5.553)
    difftz_ref = difftz.position
    samty_ref = samty.position
    
    difftz_positions = np.arange(-8.45, 16.95, zstep)
    samty_positions = np.arange(-18.99, 25.3, ystep)
    count = 0
    for samty_pos in samty_positions:
        umv(samty, samty_pos)
        for difftz_pos in difftz_positions:
            count += 1
            umv(difftz, difftz_pos)
            umv(diffrz, 0)
            dset_name = 'point' + str(count) + '_0deg'
            newdataset(dset_name)
            finterlaced(diffrz, -10, 0.2, 101, 0.085)
            umv(diffrz, 180)
            dset_name = 'point' + str(count) + '_180deg'
            newdataset(dset_name)
            finterlaced(diffrz, 170, 0.2, 101, 0.085)
    print('Done successfully !') 
   

def yunhui_line_scan(dset_prefix, zstep = 0.1, zrange = 2):
    # go to the top edge
    samtz_ref = samtz.position
    samty_ref = samty.position
    samtz_positions = np.arange(samtz_ref, samtz_ref + zrange, zstep)
    npoints = samtz_positions.shape[0]
    count = 0
    print(samtz_positions)
    print(samtz_positions.shape)
    for samtz_pos in samtz_positions:
        count += 1
        print(f'Measuring point {count} / {npoints} at 0 deg')
        umv(samtz, samtz_pos)
        umv(diffrz, -5)
        dset_name = dset_prefix + '_p' + str(count) + '_0deg'
        newdataset(dset_name)
        finterlaced(diffrz, -5, 0.25, 41, 0.085)
    
    count = 0
    for samtz_pos in samtz_positions:
        count += 1
        print(f'Measuring point {count} / {npoints} at 180 deg')
        umv(samtz, samtz_pos)
        umv(diffrz, 175)
        dset_name = dset_prefix + '_p' + str(count) + '_180deg'
        newdataset(dset_name)
        finterlaced(diffrz, 175, 0.25, 41, 0.085)
    umv(samtz, samtz_ref)
    print('Done successfully !')

def additive_area_scan(sub_sample_name, yrange = 0.1, ystep = 0.1, zrange = 10.5, zstep = 1.5, count_time = 2):
    difftz_ref = difftz.position
    samty_ref = samty.position
    
    samty_positions = np.arange(samty_ref, samty_ref + yrange + ystep/2, ystep)
    print(samty_positions)
    print(samty_positions.shape)
    ny = samty_positions.shape[0]
    nz = int(zrange / zstep) + 1
    umv(diffrz,0)
    count = 0
    for samty_pos in samty_positions:
        print(f'Measuring line {count} / {ny}')
        umv(samty, samty_pos)
        count += 1
        dset_name = sub_sample_name + '_l' + str(count)
        newdataset(dset_name)
        ascan(difftz, difftz_ref, difftz_ref + zrange, nz, count_time)
    umv(difftz, difftz_ref)
    print('Done successfully !')



def vertical_line_scan(dset_prefix, zstep1 = 0.2, zrange1 = 4, zstep2 = 1, zrange2 = 6):
    # go to the top edge
    umv(diffrz, 0)
    difftz_ref = difftz.position
    samty_ref = samty.position
    difftz_positions1 = np.arange(difftz_ref, difftz_ref + zrange1, zstep1)
    difftz_positions2 = np.arange(difftz_ref + zrange1 + zstep1, difftz_ref + zrange1 + zrange2, zstep2)
    
    difftz_positions = np.hstack([difftz_positions1, difftz_positions2])
    npoints = difftz_positions.shape[0]
    count = 0
    print(difftz_positions)
    print(difftz_positions.shape)
    for difftz_pos in difftz_positions:
        count += 1
        print(f'Measuring point {count} / {npoints} at 0 deg')
        umv(difftz, difftz_pos)
        umv(diffrz, -5)
        dset_name = dset_prefix + '_p' + str(count) + '_0deg'
        newdataset(dset_name)
        finterlaced(diffrz, -5, 0.25, 41, 0.085)
    
    count = 0
    for difftz_pos in difftz_positions:
        count += 1
        print(f'Measuring point {count} / {npoints} at 180 deg')
        umv(difftz, difftz_pos)
        umv(diffrz, 175)
        dset_name = dset_prefix + '_p' + str(count) + '_180deg'
        newdataset(dset_name)
        finterlaced(diffrz, 175, 0.25, 41, 0.085)
    umv(difftz, difftz_ref)
    print('Done successfully !')



def area_scan(yrange = 27.0, ystep = 1.8):
    difftz_ref = difftz.position
    samty_ref = samty.position
    
    samty_positions = np.arange(samty_ref, samty_ref + yrange + ystep/2, ystep)
    print(samty_positions)
    print(samty_positions.shape)
    ny = samty_positions.shape[0]
    count = 0
    for samty_pos in samty_positions:
        print(f'Measuring line {count} / {ny}')
        umv(samty, samty_pos)
        count += 1
        dset_prefix = 'area_l' + str(count)
        vertical_line_scan(dset_prefix)
    print('Done successfully !') 
    
    
    
def area_scan_continue(yrange = 27.0, ystep = 1.8):
    difftz_ref = difftz.position
    #samty_ref = samty.position
    
    #samty_positions = np.arange(samty_ref, samty_ref + yrange + ystep/2, ystep)
    
    samty_positions = np.array([15.46, 17.26, 19.06, 20.86, 22.66, 24.46])
    
    print(samty_positions)
    print(samty_positions.shape)
    ny = samty_positions.shape[0]
    count = 10
    for samty_pos in samty_positions:
        print(f'Measuring line {count} / {ny}')
        umv(samty, samty_pos)
        count += 1
        if count == 11:
            dset_prefix = 'areal_l' + str(count) + 'redo'
        else:
            dset_prefix = 'area_l' + str(count)
        vertical_line_scan(dset_prefix)
    print('Done successfully !') 
    
    
def wire_positions():
    # Order: # [difftz samtx samty samtz]
    # ST1_HT    
    # wise on the d0 region
    #wire0 = [-1.406, -4.08, -20.42, 6.4268]
    #wire1 = [-1.4060, -3.46, -17.60, 6.466620]
    #wire2 = [-1.4060,  -2.66,  -10.57, 6.606840]  # [difftz samtx samty samtz]
    #wire3 = [-1.4060, -1.54, -1.37, 6.606840]
    #wire4 = [-1.4060, -0.26, 12.12, 6.606840]
    #wire5 = [-1.4060, -0.71, 21.09, 6.506840]
    #wires = [wire0, wire1, wire2, wire3, wire4, wire5]
    
    # ST2_AB
    #wire0 = [-1.406, -1.06, -8.85, 10.0687]
    #wire1 = [-1.406,  0.84, -2.09, 10.0687]
    #wire2 = [-1.406,  1.49,  0.41,  9.9187]  
    #wire3 = [-1.406,  1.89,  4.71,  9.8187]
    #wire4 = [-1.406,  2.59,  9.36,  9.6187]
    #wire5 = [-1.406,  3.04, 14.31,  9.5187]
    #wire6 = [-1.406,  3.44, 20.69,  8.9187]
    
    #ST2_HT
    #wire0 = [-1.406, 0.74, -15.26, 9.7687]
    #wire1 = [-1.406,  1.29, -11.16, 10.0687]
    #wire2 = [-1.406,  2.09,  -6.86,  10.0687]  
    #wire3 = [-1.406,  2.64,  -0.73,  10.0187]
    #wire4 = [-1.406,  2.94,  6.22,  9.8187]
    #wire5 = [-1.406,  2.39, 17.34,  9.3687]



    #ST2__MA_HT
    #wire0 = [-1.406, 0.32, -14.76, 10.2953]
    #wire1 = [-1.406,  0.82, -9.36, 10.2953]
    #wire2 = [-1.406,  1.57,  -4.91,  10.0953]  
    #wire3 = [-1.406,  1.72,  3.99,  9.6953]
    #wire4 = [-1.406,  1.12,  13.74,  9.1953]
    
    #ST3__AB
    #wire0 = [7.594,  -3.41, -14.99,  9.1968]
    #wire1 = [7.594,  -1.61,  -7.29,  9.3968]
    #wire2 = [7.594,  -1.06,  -1.29,  9.2468]  
    #wire3 = [7.594,  -0.56,   4.48,  9.0468]
    #wire4 = [7.594,  -0.76,  15.18,  8.2468]
    
      #ST3__HT
    #wire0 = [15.0517,  -3.40, -6.89, 1.7576]
    #wire1 = [15.0517,  -2.4,  -1.19, 1.8076]
    #wire2 = [15.0517,  -1.4,  7.31, 1.8076]  
    #wire3 = [15.0517,  -0.65,   13.81, 1.6076]
    #wire4 = [15.0517,  -0.90,  23.81,  1.0076]
    
    #ST1_AB_wire
    wire0 = [-6.4968203,  -1.69, -11.74, 3.90]
    wire1 = [-6.4968203,  -0.44, -4.83, 3.7961]
    wire2 = [-6.4968203,  1.61, 14.78, 3.1961]
    wire3 = [-6.4968203,  1.61, 25.26, 3.00]
    
    
     
    wires = [wire0, wire1, wire2, wire3]
    return np.array(wires)
    
def find_pos_new(samty_pos):
    wires = wire_positions()
    nwires = wires.shape[0]
    pos_new = []
    if samty_pos <= wires[-1][2]:
        for i in range(nwires-1):
            if samty_pos > wires[i][2] and samty_pos <= wires[i+1][2]:
                ind_left = i
                ind_right = i+1
        
        ratio = (samty_pos - wires[ind_left][2]) / (wires[ind_right][2] - wires[ind_left][2])
        print(ind_left, ind_right, ratio)
        
        for i in range(4):
            pos_new.append(wires[ind_left][i] + ratio * (wires[ind_right][i] - wires[ind_left][i]))
    else:
        pos_new.append(wires[-1][0])
        pos_new.append(wires[-1][1])
        pos_new.append(samty_pos)
        pos_new.append(wires[-1][3])
    return pos_new

        
def area_scan_with_wire_positions(yrange = 28, ystep = 3.0):

    difftz_ref = difftz.position
    samty_ref = samty.position
    
    samty_positions = np.arange(samty_ref, samty_ref + yrange + ystep/2, ystep)
    if samty_positions[-1] >= 25.3:
        samty_positions[-1] = 25.1
    print(samty_positions)
    print(samty_positions.shape)
    ny = samty_positions.shape[0]
    count = 0
    for samty_pos in samty_positions:
        print(f'Measuring line {count} / {ny}')
        pos_new = find_pos_new(samty_pos)
        print(pos_new)
        # re-center the sample
        umv(difftz, pos_new[0])
        umv(samtx, pos_new[1])
        umv(samty, samty_pos)
        umvct(samtz, pos_new[3])
        count += 1
        dset_prefix = 'area_l' + str(count)
        print(dset_prefix)
        vertical_line_scan(dset_prefix, zstep1 = 0.2, zrange1 = 6, zstep2 = 1, zrange2 = 14)
    print('Done successfully !') 
    


def area_scan_with_wire_positions_continue(yrange = 20, ystep = 3.0):

    difftz_ref = difftz.position
    samty_ref = samty.position
    #samty_positions = np.arange(samty_ref, samty_ref + yrange + ystep/2, ystep)
#    samty_positions = np.array([-0.111,  2.889,  5.889,  8.889, 11.889, 14.889, 17.889, 20.889])
#samty_positions = np.array([-12.76,  -9.76,  -6.76,  -3.76, -0.76, 2.76, 5.76, 8.76, 11.76])
    samty_positions = np.array([-6.76,  -3.76, -0.76, 2.76, 5.76, 8.76, 11.76])
    
    if samty_positions[-1] >= 25.3:
        samty_positions[-1] = 25.1
    print(samty_positions)
    print(samty_positions.shape)
    ny = samty_positions.shape[0]
    count = 2
    for samty_pos in samty_positions:
        print(f'Measuring line {count} / {ny}')
        pos_new = find_pos_new(samty_pos)
        print(pos_new)
        # re-center the sample
        umv(difftz, pos_new[0])
        umv(samtx, pos_new[1])
        umv(samty, samty_pos)
        umvct(samtz, pos_new[3])
        count += 1
        if count == 3:
            dset_prefix = 'area_l' + str(count) + 'redo2'
        else:
            dset_prefix = 'area_l' + str(count)
        print(dset_prefix)
#        vertical_line_scan(dset_prefix, zstep1 = 0.2, zrange1 = 4, zstep2 = 1, zrange2 = 6)
        vertical_line_scan(dset_prefix, zstep1 = 0.2, zrange1 = 6, zstep2 = 1, zrange2 = 14)
    print('Done successfully !') 
    
    
def ST1_AB_wire():
    # the first line scan for d0
    samty_pos = -11.3
    pos_new = find_pos_new(samty_pos)
    print(pos_new)
    # re-center the sample
    umv(difftz, pos_new[0])
    umv(samtx, pos_new[1])
    umv(samty, samty_pos)
    umvct(samtz, pos_new[3])
    dset_prefix = 'l1'
    vertical_line_scan(dset_prefix, zstep1 = 0.2, zrange1 = 6, zstep2 = 1, zrange2 = 14)
    
    
    
    samty_pos = -4.3
    pos_new = find_pos_new(samty_pos)
    print(pos_new)
    # re-center the sample
    umv(difftz, pos_new[0])
    umv(samtx, pos_new[1])
    umv(samty, samty_pos)
    umvct(samtz, pos_new[3])
    dset_prefix = 'l2'
    vertical_line_scan(dset_prefix, zstep1 = 0.2, zrange1 = 6, zstep2 = 1, zrange2 = 14)
    
    # area scan
    pos_ref = [-6.4968203, -0.2215145, -2.74, 3.73215]
    umv(difftz, pos_ref[0])
    umv(samtx, pos_ref[1])
    umv(samty, pos_ref[2])
    umv(samtz, pos_ref[3])
#    samty_positions = np.array([-2.74,  0.26, 3.26, 6.26, 9.26, 12.26, 15.26, 18.26, 21.26, 24.26]) # 10 lines
    area_scan_with_wire_positions(yrange = 28, ystep = 3.0)
    
    
    print('Done successfully')
    sheh3.close()
    
    
    
    
    
    
    
    
    
    
    
