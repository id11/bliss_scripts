

def overnight():
    newdataset('x1flat_x2bent_nokb')
    while 1:
        fscan(xroty,269.702, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME")  
        fscan(xroty,275.046, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME")      
        sleep(60)
        
        
# Remove KB 
#    - moved bigy from 448.650 to 22
#    - moved s8z  from 10.8045 to 



def daytime():
#    newdataset('x1bent_x2flat_nokb_stab')
    p1 = 269.728-0.0025
    p2 = 275.052-0.0025
    while 1:
        fscan(xroty,p1-0.015, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME")  
        fscan(xroty,p2-0.015, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME")      
        sleep(60)
        
# Added dvolt to measure temperature in OH2
# added frelon3 far field diffraction.

def daytime2():
    newdataset('x1b_x2fl_CeO2')
    p1 = 269.735
    p2 = 275.064
    while 1:
        ACTIVE_MG.disable('frelon3')
        fscan(xroty,p1-0.015, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME")  
        fscan(xroty,p2-0.015, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME")      
        ACTIVE_MG.enable('frelon3')
        loopscan(6,10) # 60 seconds of frelon data


def sundayam():
    newdataset('x1f_x2fb_CeO2')
    p1 = 269.735
    p2 = 275.064
    while 1:
        ACTIVE_MG.disable('frelon3')
        fscan(xroty,p1-0.015, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME")  
        fscan(xroty,p2-0.015, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME")      
        ACTIVE_MG.enable('frelon3')
        loopscan(6,10) # 60 seconds of frelon data

def sundaypm():
    newdataset('x1b_x2b_CeO2')
    p1 = 87.596
    p2 = 92.92
    while 1:
        fscan(xroty,p1-0.015, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME")  
        fscan(xroty,p2-0.015, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME") 
        sleep(60)     
        
import numpy as np

def rockbragg1():
    #newdataset('x1bx2f_rockx1')
    p1 = 269.735
    p2 = 275.064
    llbragg1 = config.get('llbragg1')
    origin = llbragg1.position
    for dt in [0,] + list(np.linspace(-0.003,0.003,30))+[0,]:
        umv(llbragg1, dt + origin)
        umv(llbragg1, dt + origin)
        umv(llbragg1, dt + origin)
        fscan(xroty,p1-0.015, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME")  
        fscan(xroty,p2-0.015, 0.0001, 0.03/0.0001, 0.02, scan_mode="TIME")      
   
       
def fullrangenewxtal():
    newdataset('x1fx2b_newxtal_turns')
    vals = list(range(5))
    v0 = xroty.position
    for i in vals:
        fscan( xroty, v0 + i*360,   0.0005, 362/0.0005, 0.00025, scan_mode='TIME' ) 
    for i in vals[::-1]:
        fscan( xroty, v0 + i*360,  -0.0005, 362/0.0005, 0.00025, scan_mode='TIME' ) 
  
# Put xroty with closed loop on around 19h. Not sure if it was reading encoder before.
#     ... seems to be fixed? Closed loop alarm eventually. Try open loop but reading.

#  To Do: 
# OK   Send Henri vacuum screenshot from closed valves  
# OK   Full range scan with encoder (fullEnc dataset). Something is moving. Xtal?
# OK   Check the peak shape versus rocking curve position.
# OK   Effect of feclose/feopen ... try now ... 20:26 saturday, scan 970. No effect.
# OK      Sunday 10h27 ... effect of feclose with first xtal flat?
# multi turns: fail with closed loop off. Is software resetting?
#  try again with closed loop on.
#   ... check with Eric. Seems to slip.
# ...   Stability for both bent crystals
#      
# sunday am. x2flat was stable after 20 mins to settle: remount xtal later.
# go to x1 flat.
#


def check_speed_resonance():
    dt = 0.0002
    for v in 5,4,3,2,1,0.5,0.25,0.1:
        step = v * dt
        npts = int(30/dt) # steps
        fscan(xroty, 90, step, npts, dt )
        fscan(xroty, 90+npts*step, -step, npts, dt )
                

# next week:
#  Bandpass on nscope beamsize for flat xtal.



