
import time
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
        
        

def pdf_scan():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'Ce0.875Gd0.125O1.9375',  }
    loopscan(120,0.5)
    nanodacpool.ramprate = 5
    nanodacpool.setpoint = 750
    newdataset('heating')
    while nanodacpool_temp.read() < 749:
        loopscan(120,0.5)

    newdataset('750C')
    for i in range(12):
        loopscan(120,0.5)        
    sheh1.close()

        
# to reload
#  user_script_load("ch6894")
    
    
def pdf_scan_CO():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'Ce0.875Gd0.125O1.9375',  }

    newdataset('750C_CO')
    for i in range(85):
        loopscan(120,0.5)        


    nanodacpool.ramprate = 5
    nanodacpool.setpoint = 25

    newdataset('cooling_CO')
    while nanodacpool_temp.read() > 30:
        loopscan(120,0.5)

    
    sheh1.close()

# ATTENTION
# modify the number of loopscans from 30 to 85!
    
def pdf_scan_CO_RT():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'Ce0.875Gd0.125O1.9375',  }

    newdataset('CO_RT')
    loopscan(120,0.5) 

    sheh1.close()
    
    
def pdf_scan_CO_heating():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'Ce0.875Gd0.125O1.9375',  }
    nanodacpool.ramprate = 10
    nanodacpool.setpoint = 750
    newdataset('CO_heating')
    while nanodacpool_temp.read() < 749:
        loopscan(120,0.5)

    newdataset('750C_CO-bis')
    for i in range(135):
        loopscan(120,0.5)        
    
    
def pdf_scan_CO_cooling():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'Ce0.875Gd0.125O1.9375',  }

    nanodacpool.ramprate = 5
    nanodacpool.setpoint = 25

    newdataset('cooling_CO_bis')
    while nanodacpool_temp.read() > 30:
        print('waiting for temperature 30\n')
        loopscan(120,0.5)

    sheh1.close()

