import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime

d3_in = 0
d3_out = 100
bs_in = -105.3
bs_out = -155.3
ffx_in = 375
ffx_out = 375
ffz_in = 0
ffz_out = 200

    

ff_pars={}
ff_pars['start_pos'] = 0
ff_pars['step_size'] = 0.5
ff_pars['num_proj'] = 360 / ff_pars['step_size']
ff_pars['exp_time'] = 0.15
ff_pars['slit_hg'] = 0.3
ff_pars['slit_vg'] = 0.3
ff_pars['mode'] = 'ZIGZAG'
ff_pars['zmot'] = samtz

pct_pars={}
pct_pars['start_pos'] = 0
pct_pars['step_size'] = 0.2
pct_pars['num_proj'] = 360 / ff_pars['step_size']
pct_pars['exp_time'] = 0.1
pct_pars['slit_hg'] = 1.5
pct_pars['slit_vg'] = 1.5


def ffin():
    sheh3.close()
    ACTIVE_MG.disable("marana*")
    ACTIVE_MG.enable("frelon3:image")
    ACTIVE_MG.enable("frelon3:roi*")
    umv(d3ty, d3_out, d1ty, bs_in)
    umv(ffdtx1, ffx_in, ffdtz1, ffz_in)
    umv(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.2, s8hg, ff_pars['slit_hg'] + 0.2)
    print("ready to collect far-field data")
    sheh3.open()
    
def tomoin():
    sheh3.close()
    ACTIVE_MG.disable("frelon3*")
    ACTIVE_MG.enable("marana")
    umv(ffdtx1, ffx_out, ffdtz1, ffz_out)
    umv(d3ty, d3_in, d1ty, bs_out)
    umv(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'])
    umv(s8vg, pct_pars['slit_vg'] + 0.2, s8hg, pct_pars['slit_hg'] + 0.2)
    print("ready to collect near-field data")
    sheh3.open()
    
def ff_scan(name, target, exp_time, sleep_time, ramprate=10):
    newdataset(name)
    nanodac3.ramprate = ramprate
    nanodac3.setpoint = target
    while(1):
        loopscan(10, exp_time, sleep_time=sleep_time)
 
 

    

            
        
        
