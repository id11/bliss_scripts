
import time
import numpy as np
# 1.17 done
# sample top is at 1.07

def volscan():
    umv( shtz, 1.085 )

    for zpos in np.arange(1.085, 2.085, 0.04):
        umv( shtz, zpos )
        fscan2d(rot, 3060, -17, 181, dty,-800,5,321,.05, mode='ZIGZAG')
        umv( shtz, zpos + 0.02 )
        fscan2d(rot,    0,  17, 181, dty,-800,5,321,.05, mode='ZIGZAG')

def volscan2():
    # continue with larger range in new dataset
    for zpos in np.arange(1.365, 2.085, 0.04):
        umv( shtz, zpos )
        fscan2d(rot, 3060, -17, 181, dty,-1000,5,400,.05, mode='ZIGZAG')
        umv( shtz, zpos + 0.02 )
        fscan2d(rot,    0,  17, 181, dty,-1000,5,400,.05, mode='ZIGZAG')

def volscan3():
    # continue with larger range in new dataset
    for zpos in np.arange(1.3, 1.025, -0.04):
        umv( shtz, zpos )
        fscan2d(rot, 0, 17, 361, dty,-800,5,321,.05, mode='ZIGZAG')
        umv( shtz, zpos - 0.02 )
        fscan2d(rot,6120,  -17, 361, dty,-800,5,321,.05, mode='ZIGZAG')



def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

TESTING = False


def myfscan(*a,**k):
    print(a,k)
    if TESTING:
        return
    try:
        fscan(*a,**k)
    except: # pretty hopeless but why not ?
        print("Something has gone wrong!!! Retry scans!!")
        elog_print("Retry a scan !!!")
        print("Sleep a minute or two first")
        time.sleep(60)
        pause_for_refill(120)
        fscan(*a,**k)
        
def myumv(*a,**k):
    print(a,k)
    if TESTING:
        return
    umv(*a,**k)

def nscopedtylayerscan(y_start, y_step, ny, a_time, rstart, rstep, rend):
    eiger.camera.photon_energy = 65350.8
    for i,r in enumerate(np.arange(rstart,rend,rstep)):
        myumv(rot, r )
        pause_for_refill(ny*a_time+1)
        if i%2 == 0:
            myfscan(dty, y_start, y_step, ny, a_time, scan_mode='CAMERA')
        else:
            myfscan(dty, y_start+y_step*ny, -y_step, ny, a_time, scan_mode='CAMERA')



def vol35umRed():
    for zpos in np.arange(50,85,1): # 43 scans
        if int(zpos) < 84:
            continue
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -30, 1, 60, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -30, 1, 60, 0.3, 180, -2, -0.1 )
            

def vol35umGreen():
    # R.G.B is 
    for zpos in np.arange(34,72.1,1): # 43 scans
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -30, 1, 60, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -30, 1, 60, 0.3, 180, -2, -0.1 )
            

def vol35umBlue():
    # R.G.B is 31 -> 72
    for zpos in np.arange(32,73.1,1): # 43 scans
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -30, 1, 60, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -30, 1, 60, 0.3, 180, -2, -0.1 )

def vol35umBlueII():
    # R.G.B is 31 -> 72
    for zpos in np.arange(28,67.1,1): # 43 scans
        if zpos < 54:
            continue
        myumv( pz, zpos )
        newdataset("z2_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -30, 1, 60, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -30, 1, 60, 0.3, 180, -2, -0.1 )
            

def vol40umRed():
    # R.G.B is 31 -> 72
    for zpos in np.arange(40,83.1,1): # 43 scans
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -35, 1, 70, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -35, 1, 70, 0.3, 180, -2, -0.1 )

def vol40umGreen():
    # R.G.B is 26 -> 70
    for zpos in [35,62]+list(np.arange(43,58,1.5)) :
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -35, 1, 70, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -35, 1, 70, 0.3, 180, -2, -0.1 )
            
def vol40umBlue():
    # R.G.B is 20 -> 64
    for zpos in np.arange(20,64,2) :
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -35, 1, 70, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -35, 1, 70, 0.3, 180, -2, -0.1 )
