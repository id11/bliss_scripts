import sys
import numpy as np
import time
import os

atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')


def switch_to_marana3_pct():
    fsh.session = 'TDXRD'
    fsh.close()
    marana3_in()
    fsh.open()
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    #umvct(d3_bstz, 2000)
    tfoh1.set(0, 'Be')
    plotimagecenter(marana3)
    slit(1.5, 1.5)
    return 'Done'


def switch_to_ff(scan_bs=True):
    fsh.session = 'TDXRD'
    fsh.close()
    #tfoh1.set(18,'Be')
    ff_in(scan_bs)
    umv(atty,0,attrz,-4)
    ACTIVE_MG.disable('marana3*')
    ACTIVE_MG.enable('frelon3*')
    fsh.open()
    slit(0.95, 0.18)
    ct(0.05)


def marana3_in():
    ff_out()
    assert d1tz.position > 90
    #assert d2tz.position > 100
    assert d1ty.position < -200
    assert d2ty.position < -387
    assert ffdtx1.position > 440
    umv(nfdtx, 100)
    assert nfdtx.position > 95
    umv(d3ty, 0)
    umv(d3tz, 0)
    umvct(atty,-10,attrz,0)
    return 'Done'
    
    
def ff_in(scan_bs=True):
    marana3_out()
    assert d3ty.position > 169.0
    assert nfdtx.position < 55.0
    assert d2ty.position < -387
    umv(ffdtz1, 0)
    umv(ffdtx1, 200)
    if scan_bs:
        open_ffdoor(detector_name = 'frelon3')
    else:
        umv(ffdoor,-170)
        
    
def marana3_out():
    if furnace_z.position < 70:
        umv(d3ty,170)
        assert d3ty.position > 169.0
        umv(nfdtx,50)
    else:
        umv(d3ty,170)
        umv(nfdtx,50)
    

def ff_out():
    umv(ffdoor, 0)
    umv(ffdtx1, 450)
    umv(ffdtz1, 400)


def large_tomo_fov():
    delta = 0.4
    pos_list = [delta, delta+0.95, delta+0.95*2]
    
    diffty0 = 14.0115
    samtz0 = 2.13264
    
    umv(samtz,samtz0)
    for i, pos in enumerate(pos_list):
        umv(diffty, diffty0 + pos)
        fulltomo.full_turn_scan(f'pct_bot_{i}')
    
    
    umv(samtz,samtz0 - 0.95)
    for i, pos in enumerate(pos_list):
        umv(diffty, diffty0 + pos)
        fulltomo.full_turn_scan(f'pct_top_{i}')
    
    umvct(diffty, diffty0)
    print('Done')


def align_sample_center():
    newdataset('align')
    sct(0.5)
    diffty0 = 14.0115
    
    umvct(diffty, diffty0)
    # center at 0 deg by moving samty
    umvct(diffrz, 0)
    ascan(samty, 0, 21, 40, 0.5)
    # you should be able to see a sample edge, then try to put sample edge on the detector center
    samty_edge1 = samty.position
    
    # do another scan to find the other sample edge
    # you won't be able to put sample edge on the detector
    # but you can see the sample edge on the detector, then measure how much you need to virtually further move
    ascan(samty, samty_edge1, -24.7, 80, 0.5)
    # samty_edge2 = samty.position - delta_value
    
    samty_center = (samty_edge1 + samty_edge2) / 2
    
    # move to samty_center and put diffty back
    umvct(samty, samty_center, diffty, diffty0)
    
    
    # center at 90 deg by moving samtx + diffty
    samtx0 = 15.35
    
    umvct(diffrz, 90)
    ascan(samtx, 0, -23, 40, 0.5)
    # you should be able to see a sample edge, then try to put sample edge on the detector center
    samtx_edge1 = samtx.position
    
    # do another scan to find the other sample edge
    # you won't be able to put sample edge on the detector
    # then you need to move diffty to compensate the movement
    umvct(samtx, samtx0)
    #ascan(diffty, diffty0, diffty0+8, 30, 1, sleep_time = 0.2)
    umvct(diffty, diffty0+8)
    
    # hopefully you should see the sample edge
    # then try to move samtx again to put the sample edge on the detector center
    diffty_edge = diffty.position
    
    # calculate a virtual samtx_edge2
    samtx_edge2 = samtx0 + (diffty_edge - diffty0) + (samtx.position - samtx0)
    
    samtx_center = (samtx_edge1 + samtx_edge2) / 2
    
    # move to samtx_center and put diffty back
    umvct(samtx, samtx_center, diffty, diffty0)

    # now the sample center is centered on the rotation axis
    

## first_cell
#pos_center = [0.108, -2.172, 6.15426, -4.2084609] # samtx, samty, samtz, difftz)


# cell1
#pos_center = [0.792235, -2.98725999, 6.15426, 11.7915391] # samtx, samty, samtz, difftz)

#cell1_corrected
#pos_center = [0.8225, -2.996, 6.15426, 11.7915391] # samtx, samty, samtz, difftz)

#cell1_corrected the tilts
#pos_center = [0.655, -2.992, 6.15426, 11.7915391] # samtx, samty, samtz, difftz)

#cell1_corrected the tilts + fix the cables
#pos_center = [0.6895, -3.0165, 6.15426, 11.7915391] # samtx, samty, samtz, difftz)


#freshg_soc00
pos_center = [0.1690, -2.67350, 6.15426, 12.9915391] # samtx, samty, samtz, difftz)


# hstandard acquisition, diffty at x = 1024.1 (at the detector center)
def take_pct_series_standard_scan():
    # go to the sample center
    umvct(samtx, pos_center[0], samty, pos_center[1], samtz, pos_center[2], difftz, pos_center[3]) 
    diffty0 = 14.0115
    umvct(diffty, diffty0)
    # take PCT series
    delta0 = 2.0
    delta_step = 0.95
    sample_half = 23
    
#    pos_list = np.arange(pos_center[1]+delta0, sample_half-delta0, delta_step) # 23 ROIs
    pos_list = np.arange(sample_half-delta0-1, pos_center[1]+delta0, -delta_step) # 24 ROIs, from edge to center
#    pos_list = [5.0]
    for i, pos in enumerate(pos_list):
        umvct(samty, pos)
        umvct(diffrz, 0)
        fulltomo.full_turn_scan(f'pct_std_ROI_{i+1}')
#        fulltomo.full_turn_scan('pct_std_test_5mm')
    umvct(diffrz, 0)
    umvct(samtx, pos_center[0])
    umvct(samty, pos_center[1])
    umvct(samtz, pos_center[2])    
    umvct(diffty, diffty0)
    print('Done')


# half acquisition, diffty at x = 1932  (move relative by + 0.5 mm)
def take_pct_series_half_scan():
    # go to the sample center
    umvct(samtx, pos_center[0], samty, pos_center[1], samtz, pos_center[2], difftz, pos_center[3])
    diffty0 = 14.0115
    samry0 = samry.position
    umvct(diffty, diffty0+0.5)   # move cor to the edge sitting at 1932 pixels
    # take PCT series
    delta0 = 1.0
    delta_step = 0.95*2
    sample_half = 23
    
#    pos_list = np.arange(pos_center[1]+delta0, sample_half-delta0, delta_step) # 13 ROIs, from center to edge
    pos_list = np.arange(sample_half-delta0-1, pos_center[1]+delta0, -delta_step) # 12 ROIs, from edge to center
#    pos_list = pos_list[:-1]   # remove the last position which has only 0.25 mm sample in the beam
#    pos_list = [5.0,]
    for i, pos in enumerate(pos_list):
        umvct(samty, pos)
        umvct(diffrz, 0)
        fulltomo.full_turn_scan(f'pct_half_ROI_{i+1}')
#        fulltomo.full_turn_scan(f'pct_half_test_5mm')
    umvct(diffrz, 0)
    umvct(samtx, pos_center[0])
    umvct(samty, pos_center[1])
    umvct(samtz, pos_center[2])
    umvct(diffty, diffty0)
    print('Done')











