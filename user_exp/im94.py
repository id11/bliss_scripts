

import time, numpy as np

# def check_cpm18(pos=6.4158):
def check_cpm18(pos=7.3533):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03:
        user.cpm18_goto(pos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()


def fscanloop(lo, hi, step ):
    for i, y in enumerate( np.arange( lo, hi, step ) ):
        umv(dty, y )
        pause_for_refill( 30 )
        if i%2 == 0:
            fscan(rot, 0,    0.125, 180/0.125, 0.005, scan_mode = 'CAMERA' )
        else:
            fscan(rot, 180, -0.125, 180/0.125, 0.005, scan_mode = 'CAMERA' )

def Dwetcut1():
    newdataset( 'layer1')
    fscanloop( -1000, 1001, 5 )
    

        
def B5layer():
    newdataset( 'layer1')
    fscanloop( -3600, 3601, 7.5 )
    
