import numpy as np
import time
from bliss.common import cleanup

def goto_vertical_beam():
    umv(vly,0.03,hlz,-0.326)
    umv(shnee,90.06,axmo,64.81)
    umv(s9vg,0.2,s9hg,0.05)
    
def goto_horizontal_beam():
    umv(vly,1.03,hlz,0.674)
    umv(shnee,89.06,axmo,64.81)
    umv(s9vg,0.05,s9hg,0.2)
    
def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
        
class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )

def vertical_scan(z_start,z_end):
    z_vals = np.arange(z_start,z_end+0.5,0.5)
    for z_val in z_vals:
        umv(pz,z_val)
        half1(11,'Z%s'%int(10*z_val))
    print("Successfully done !!!")

         
def half1(ymax,
          datasetname,
          ystep=0.2,
          ymin=-11,
          rstep=0.125,
          astart = -90,
          arange = 181,
          expotime=0.005):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)
    
    
    
def haixing_scan():
    # slice 1
    umv(ntz,-7.63) # sample tip position ntz = -7.8248
    datasetname='top_0p2um'
    ystep=0.5
    ymax=100
    ymin=-100
    rstep=0.125
    astart=0
    arange=181
    expotime=0.005
    half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
    print("Successfully done !!!")
    
    # slice 2
    umv(ntz,-7.43) # sample tip position ntz = -7.8248
    datasetname='top_0p4um'
    ystep=0.5
    ymax=100
    ymin=-100
    rstep=0.125
    astart=0
    arange=181
    expotime=0.005
    half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
    print("Successfully done !!!")
    
def haixing_Al8Cu():
    # slice 1
    umv(ntz,-5.51) # sample tip position ntz = -5.71
    datasetname='top_200um'
    ystep=0.5
    ymax=260
    ymin=-260
    rstep=0.125
    astart=0
    arange=181
    expotime=0.005
    half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
    print("Successfully done !!!")
    
def haixing_FeAu_0p5_tR():
    # sample top surface position
    ntz0 = -8.55
    pz0 = 17.8499
    umv(ntz,ntz0)
    umv(pz,pz0)
    
    # parameters
    ystep=0.5
    ymax_series=[220, 260, 260, 200]
    ymin_series=[-220, -260, -260, -200]
    
    rstep=0.125
    astart=0
    arange=181
    expotime=0.005
    
    z_vals = np.arange(0,0.4,0.1)     # every 100 um, 4 slices
    for i, z_val in enumerate(z_vals):
        umv(ntz, ntz0 + z_val)
        datasetname = 'top_' + str(int(1000*z_val)) + 'um'
        print(datasetname)
        ymax = ymax_series[i]
        ymin = ymin_series[i]
        #print(ymax,ymin)
        half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
        
    print("Successfully done !!!")   
    sheh3.close()

    
    
def S7_deformed():
    # middle slice position
    ntz0 = -4.0383
    pz0 = 50.0
    umv(ntz,ntz0)
    umv(pz,pz0)
    
    # parameters
    ystep=2
    ymax_series=[270 for _ in range(18)]
    ymin_series=[-270 for _ in range(18)]
    
    rstep=0.125
    astart=0
    arange=181
    expotime=0.007
    
    z_vals = np.arange(-36,36,4)
    for i, z_val in enumerate(z_vals):
        #umv(ntz, ntz0 + z_val)
        umv(pz, pz0 + z_val)
        datasetname = 'sliceZ_' + str(int(i))
        print(datasetname)
        ymax = ymax_series[i]
        ymin = ymin_series[i]
        #print(ymax,ymin)
        half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
        
    print("Successfully done !!!")   
    sheh3.close()    
    
    
def S9_deformed():
    # middle slice position
    ntz0 = -1.8833
    pz0 = 58.0
    umv(ntz,ntz0)
    umv(pz,pz0)
    
    # parameters
    ystep=2
    ymax_series=[276 for _ in range(16)]
    ymin_series=[-276 for _ in range(16)]
    
    rstep=0.125
    astart=0
    arange=181
    expotime=0.005
    
    z_vals = np.arange(-12,52,4)
    for i, z_val in enumerate(z_vals):
        #umv(ntz, ntz0 + z_val)
        umv(pz, pz0 + z_val)
        datasetname = 'sliceZ_' + str(int(i))
        print(datasetname)
        ymax = ymax_series[i]
        ymin = ymin_series[i]
        #print(ymax,ymin)
        half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
        
    print("Successfully done !!!")   
    sheh3.close() 
    
def S9_deformed_ctd(zmin=7):
    # middle slice position
    ntz0 = -1.8833
    pz0 = 58.0
    umv(ntz,ntz0)
    umv(pz,pz0)
    
    # parameters
    ystep=2
    ymax_series=[276 for _ in range(16)]
    ymin_series=[-276 for _ in range(16)]
    
    rstep=0.125
    astart=0
    arange=181
    expotime=0.005
    
    z_vals = np.arange(-12,52,4)
    for i, z_val in enumerate(z_vals):
        #umv(ntz, ntz0 + z_val)
        if i <= zmin:
            continue
        umv(pz, pz0 + z_val)
        datasetname = 'sliceZ_a_' + str(int(i))
        print(datasetname)
        ymax = ymax_series[i]
        ymin = ymin_series[i]
        #print(ymax,ymin)
        half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
        
    print("Successfully done !!!")   
    sheh3.close()         


def S9_325C_10min():
    # middle slice position
    ntz0 = -2.1133
    pz0 = 43.0
    umv(ntz,ntz0)
    umv(pz,pz0)
    
    # parameters
    ystep=2
    ymax_series=[276 for _ in range(16)]
    ymin_series=[-276 for _ in range(16)]
    
    rstep=0.125
    astart=0
    arange=181
    expotime=0.005
    
    z_vals = np.arange(-12,52,4)
    for i, z_val in enumerate(z_vals):
        #umv(ntz, ntz0 + z_val)
        umv(pz, pz0 + z_val)
        datasetname = 'sliceZ_' + str(int(i))
        print(datasetname)
        ymax = ymax_series[i]
        ymin = ymin_series[i]
        #print(ymax,ymin)
        half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
        
    print("Successfully done !!!")   
    sheh3.close() 

def S9_325C_10min_bottom():
    # middle slice position
    ntz0 = -1.7533
    pz0 = 43.0
    umv(ntz,ntz0)
    umv(pz,pz0)
    
    # parameters
    ystep=2
    ymax_series=[276 for _ in range(21)]
    ymin_series=[-276 for _ in range(21)]
    
    rstep=0.125
    astart=0
    arange=181
    expotime=0.005
    
    z_vals = np.arange(-40,44,4)
    for i, z_val in enumerate(z_vals):
        #umv(ntz, ntz0 + z_val)
        umv(pz, pz0 + z_val)
        datasetname = 'sliceZ_-360um_' + str(int(i))
        print(datasetname)
        ymax = ymax_series[i]
        ymin = ymin_series[i]
        #print(ymax,ymin)
        half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
        
    print("Successfully done !!!")   
    sheh3.close() 
    
def S9_325C_10min_bottom_continue(zmin=8):
    # middle slice position
    ntz0 = -1.7533
    pz0 = 43.0
    umv(ntz,ntz0)
    umv(pz,pz0)
    
    # parameters
    ystep=2
    ymax_series=[276 for _ in range(21)]
    ymin_series=[-276 for _ in range(21)]
    
    rstep=0.125
    astart=0
    arange=181
    expotime=0.005
    
    z_vals = np.arange(-40,44,4)
    for i, z_val in enumerate(z_vals):
        if i <= zmin:
            continue
        #umv(ntz, ntz0 + z_val)
        umv(pz, pz0 + z_val)
        datasetname = 'sliceZ_-360um_a_' + str(int(i))
        print(datasetname)
        ymax = ymax_series[i]
        ymin = ymin_series[i]
        #print(ymax,ymin)
        half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
        
    print("Successfully done !!!")   
    sheh3.close()     



def S9_355C_10min():
    # middle slice position
    ntz0 = -1.7013
    pz0 = 46.0
    umv(ntz,ntz0)
    umv(pz,pz0)
    
    # parameters
    ystep=2
    ymax_series=[276 for _ in range(16)]
    ymin_series=[-276 for _ in range(16)]
    
    rstep=0.125
    astart=0
    arange=181
    expotime=0.005
    
    z_vals = np.arange(-12,52,4)
    for i, z_val in enumerate(z_vals):
        #umv(ntz, ntz0 + z_val)
        umv(pz, pz0 + z_val)
        datasetname = 'sliceZ_' + str(int(i))
        print(datasetname)
        ymax = ymax_series[i]
        ymin = ymin_series[i]
        #print(ymax,ymin)
        half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
        
    print("Successfully done !!!")   
    sheh3.close() 


def S9_355C_10min_bottom():
    # middle slice position
    ntz0 = -1.3413
    pz0 = 46.0
    umv(ntz,ntz0)
    umv(pz,pz0)
    
    # parameters
    ystep=2
    ymax_series=[276 for _ in range(18)]
    ymin_series=[-276 for _ in range(18)]
    
    rstep=0.125
    astart=0
    arange=181
    expotime=0.005
    
    z_vals = np.arange(-36,36,4)
    for i, z_val in enumerate(z_vals):
        #umv(ntz, ntz0 + z_val)
        umv(pz, pz0 + z_val)
        datasetname = 'sliceZ_-360um_' + str(int(i))
        print(datasetname)
        ymax = ymax_series[i]
        ymin = ymin_series[i]
        #print(ymax,ymin)
        half1(ymax,datasetname,ystep,ymin,rstep,astart,arange,expotime)
        
    print("Successfully done !!!")   
    sheh3.close()



