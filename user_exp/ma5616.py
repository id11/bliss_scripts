import numpy as np
from scipy.optimize import curve_fit

def findside(x, y):
    y = y-y.min()
    #cm = (x*y).sum()/y.sum()
    ny = np.clip(y, 0.1*y.max(), 0.9*y.max()) - (0.1*y.max()) 
    g = np.gradient(ny)
    m = g>0.008
    return np.mean(g[m])

def linefit(x, a, b):
    return  np.abs((a*x) + b)   
    
def _align_r(raxis,rrange=0.8,rsteps=7,pzrange=25,pzsteps=50,ctim=0.05):
    rcen = raxis.position
    nc.piezo_scanmode_off()
    if raxis == shrx:
        if np.abs((rot.position+90)%180)>1:
            umv(rot, 90)
    elif raxis == shry:
        if np.abs((rot.position)%180)>1:
            umv(rot, 0)
    nc.piezo_scanmode_on()
    ws = []
    rvals = np.linspace(-rrange,rrange,rsteps)
    for r in rvals:
        sync()
        umv(raxis, rcen+r)
        s = dscan(pz,-pzrange,pzrange,pzsteps,ctim)
        ws.append(findside( s.get_data('pz'), s.get_data('roi1_avg') ))
    print(ws)
    ol1 = np.max(ws[:np.argmax(ws)])
    ol2 = np.max(ws[np.argmax(ws)+1:])
    print(ol1,ol2)
    if ol1 > ol2: 
        rl = np.argmax(ws)
    elif ol2 > ol1: 
        rl = np.argmax(ws)+1
    print(rl)  
    p1, _ = curve_fit(linefit, rvals[:rl], ws[:rl], (0,0))
    p2, _ = curve_fit(linefit, rvals[rl:], ws[rl:], (-p1[0],p1[1]))
    rideal = (p2[1]-p1[1]) / (p1[0]-p2[0])
    print(rideal,rcen+rideal)
    nc.piezo_scanmode_off()
    if np.abs(rideal) < 0.8:
        sync()
        umv(raxis,rcen + rideal)
    return rcen + rideal
    

    
def align_r_rough(raxis):
    _align_r_mca(raxis,rrange=1,rsteps=5,pzrange=45,pzsteps=60,ctim=0.05)
    
def align_r_fine(raxis):
    _align_r_mca(raxis,rrange=0.4,rsteps=5,pzrange=20,pzsteps=80,ctim=0.05)
   
def _align_r_mca(raxis,rrange=0.8,rsteps=5,pzrange=40,pzsteps=60,ctim=0.05):
    plotselect("mca:all_det0")
    shiftpzto50(ctim, 30, npts=50)
    f = flint()
    p1 = f.get_plot("curve", name="align rotation scans", unique_name="ARS")
    p1.clear_data()
    rcen = raxis.position
    nc.piezo_scanmode_off()
    if raxis == shrx:
        if np.abs((rot.position+90)%180)>1:
            umv(rot, 90)
    elif raxis == shry:
        if np.abs((rot.position)%180)>1:
            umv(rot, 0)
    nc.piezo_scanmode_on()
    ws = []
    rvals = np.linspace(-rrange,rrange,rsteps)+rcen
    for r in rvals:
        sync()
        umv(raxis, r)
        s = dscan(pz,-pzrange,pzrange,pzsteps,ctim)
        p1.add_curve( s.get_data('axis:pz'), s.get_data('mca:all_det0'), legend='%f degrees'%r)
        ws.append(fwhm())
    print(ws)
    print(rvals)
    p2 = f.get_plot("curve", name="align rotation FWHM", unique_name="ARF")
    p2.clear_data()
    p2.add_curve( rvals, ws)
    print("Select minimum position")
    spt = p2.select_points(1)
    print(spt[0][0])
    umv(raxis,spt[0][0])
    dscan(pz,-pzrange,pzrange,pzsteps,ctim)
    nc.piezo_scanmode_off() 
   
    
    
    
def scanandcen(axis,rng,steps,ctim,ctr=mca):
    s = dscan(axis,-rng,rng,steps,ctim,ctr)
    x, y = s.get_data(axis.name), s.get_data('mca:all_det0')
    y = y-y.min()
    ny = np.clip(y, 0.1*y.max(), 0.9*y.max()) - (0.1*y.max()) 
    return (x*ny).sum()/ny.sum()

def roughzedge(ctim,rng,ctr=mca):
    s = dscan(shtz,-rng,+rng,20,ctim,ctr)
    x, y = s.get_data('shtz'), s.get_data('mca:all_det0')
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    edge = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    #edge = x[np.argmin(np.abs(y-( ( np.mean(y[25:20]) + np.mean(y[0:5]) )/2) ))]
    umv(shtz, edge)
    return edge

def pzedge(ctim,rng,npts=150,ctr=mca):
    s = ascan(pz,50-rng,50+rng,npts,ctim)
    x, y = s.get_data('pz'), s.get_data('mca:all_det0')
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    edge = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    #edge = x[np.argmin(np.abs(y-( ( np.mean(y[25:20]) + np.mean(y[0:5]) )/2) ))]
    #umv(pz, edge)
    return edge
    
def pzpeak(ctim,rng):
    s = ascan(pz,50-rng,50+rng,150,ctim)
    x, y = s.get_data('pz'), s.get_data('mca:all_det0')
    maxz = x[np.argmax(y)]
    umv(pz, maxz)
    return maxz
    
def pzcen(ctim,rng,npts=150):
    s = ascan(pz,50-rng,50+rng,npts,ctim)
    plotselect('mca:all_det0')
    cenz = cen()
    umv(pz, cenz)
    return cenz
    
def pzcom(ctim,rng):
    s = ascan(pz,50-rng,50+rng,150,ctim)
    comz = com()
    umv(pz, 50)
    return comz
    
def shiftpzto50(ctim, rng, npts=150):
    pzcen = pzedge(ctim, rng, npts)
    umv(pz,50)
    sync()
    umvr(shtz, (pzcen-50)/1e3)
  
def shiftpzto50cen(ctim, rng, npts=150):
    pzc = pzcen(ctim, rng, npts)
    umv(pz,50)
    sync()
    umvr(shtz, (pzc-50)/1e3)      
    
    
def film_cor(rng, npts, ctim, ctr='eiger:roi_counters:roi1_sum'):
    cens = []
    plotselect(ctr)
    for j,k in enumerate([0,90,180,270]):
        umv(rot, k)
        if j%2 == 0:
            s = fscan(dty, -rng, rng*2/(npts-1), npts, ctim)
        else:
            s = fscan(dty, rng, -rng*2/(npts-1), npts, ctim)
        A = fscan.get_data()
        x, y = A['dty'], A[ctr]
        pk = np.clip(y-np.min(y), 0, 1e9)
        cens.append( (x*pk).sum()/pk.sum() )
        
    pyerr = (cens[0]-cens[2])/2
    pxerr = (cens[1]-cens[3])/2
    dtyideal = np.mean(cens)
    if (py.position+pyerr)>10 and (py.position+pyerr)<90:
        umvr(py, pyerr)
    else:
        umvr(shty, (pyerr/1000))
    if (px.position+pxerr)>10 and (px.position+pxerr)<90:
        umvr(px, pxerr)
    else:
        umvr(shtx, (pxerr/1000))
    umv(dty, dtyideal, rot, 0)
    
def rotation_align_film():
    plotselect('mca:all_det0')
    sync()
    ctim=0.05
    umv(rot,0)
    #roughzedge(0.05,0.1)
    shiftpzto50(0.05,30,npts=50)
    umvr(shtz,0.01)
    align_r(shry, rrange=0.8, rsteps=7, pzrange=35, pzsteps=70, ctim=0.05 )
    ed = pzedge(ctim,20)
    umv(pz,ed)
    
    umv(rot,90)
    shiftpzto50(ctim,30,npts=50)
    umvr(shtz,0.01)
    align_r(shrx, rrange=0.8, rsteps=6, pzrange=35, pzsteps=70, ctim=0.05 )
    ed = pzedge(ctim,20)
    umv(pz,ed)
    umv(rot,0)
    

  
def shift_align_film(ctim=0.03,dran=200):
    plotselect('mca:all_det0')
    umv(rot, 0, shrx, 0, shry, 0)
    #roughzedge(0.05,0.15)
    shiftpzto50(ctim,30,npts=50)

    umv(dty,-dran)
    #ascan(pz,2,98,150,ctim)
    #c1 = com()
    #umv(dty,dran)
    #ascan(pz,98,2,150,ctim)
    #c2 = com()
    c1 = pzedge(ctim,20)
    umv(dty,dran)
    c2 = pzedge(ctim,20)
    srxoff = np.degrees(np.arctan((c1-c2)/(2*dran))) 
    print(c1,c2,srxoff)   
    umv(dty,0)  
    sync()
    umvr(shrx,srxoff)   

    umv(rot, 90)
    #roughzedge(0.05,0.15)
    shiftpzto50(ctim,30,npts=50)
    
    umv(dty,-dran)
    c1 = pzedge(ctim,20)
    umv(dty,dran)
    c2 = pzedge(ctim,20)
    sryoff = np.degrees(np.arctan((c1-c2)/(2*dran)))
    print(c1,c2,sryoff)   
    umv(dty,0)
    sync()
    umvr(shry,-sryoff) 
    
    sync()
    shiftpzto50(ctim,30,npts=50)
    ed = pzedge(ctim,15)
    umv(pz,ed)
    umv(rot,0)
    return srxoff, -sryoff
    

def ry_align_film(ctim=0.03,dran=200):
    plotselect('mca:all_det0')
    umv(rot, 90)
    #roughzedge(0.05,0.15)
    shiftpzto50(ctim,30,npts=50)
    
    umv(dty,-dran)
    c1 = pzedge(ctim,20)
    umv(dty,dran)
    c2 = pzedge(ctim,20)
    sryoff = np.degrees(np.arctan((c1-c2)/(2*dran)))
    print(c1,c2,sryoff)   
    umv(dty,0)
    sync()
    umvr(shry,-sryoff) 
    
    sync()
    shiftpzto50(ctim,30,npts=50)
    ed = pzedge(ctim,15)
    umv(pz,ed)
    umv(rot,0)
    return srxoff, -sryoff
  
  
    
    
def move_to_xrd_position():
    umv(edoor,0)
    umv(det2y,-225.1400,detz,4.2341,ebsy,-0.0075,ebsz,-2.3975)     
    if checkbs():
        where()
        umv(dioderx,90,edoor,185)
    else:
        print('Problem with beamstop!')   
       
def move_to_pdf_position():
    umv(edoor,0)
    umv(det2y,-161.1400,detz,29.2341,ebsy,-63.8475,ebsz,-27.3975)        
    if checkbs():
        where()
        umv(dioderx,90,edoor,185)
    else:
        print('Problem with beamstop!')   
    
    
   
def checkbs():
    umv(edoor,0,dioderx,0)
    dscan(ebsy,-3.5,3.5,90,0.5,pico7)
    print(fwhm())
    if 3.3-fwhm() > 0.1:
            print('Poor ebs alignment')
            return False
    else:
        return True   

    
def aquirepdfdata():
    pzstart = pz.position         
    newdataset('pdf_pz0')
    loopscan(100,18)
           
    umvr(pz,-0.2)
    newdataset('pdf_pz1')
    loopscan(100,18)
    umv(pz,pzstart)
    
      
def aquirexrddata():
    pzstart = pz.position         
    newdataset('xrd_pz0')
    fscan(rot,0,0.2,360/0.2,0.15)
           
    umvr(pz,-0.2)
    newdataset('xrd_pz1')
    fscan(rot,0,0.2,360/0.2,0.15)
    umv(pz,pzstart)
                
                                                                                            
def find_low_Si():
    nc.piezo_scanmode_off()
    plotselect('eiger:roi_counters:roi1_avg')
    fscan(rot,0,0.005,2000,0.01)    

    
    
    
