
import numpy as np
import time
from bliss.common import cleanup





def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)


def tdxrd_forward360(ymax,
          datasetname,
          ystep = 0.01,
          ycen = 14.501,
          ymin = 0.,
          rstep=0.8,
          expotime=0.08
          ):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ycen = central position 
    ymin = low y values to skip, for restarting from a crash
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.tdxrd_forward(0.5, 'toto', ystep=0.1, ycen=14.65)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 11:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 ) + ycen
    i = -1
    #
    num_proj = 360 / rstep
    diffty_airpad.on( 60*24*5 )
    with cleanup.cleanup( diffrz, restore_list=(cleanup.axis.POS,) ):
        while i < len(ypos)-1:
            i += 1        
            if ypos[i] < ymin: # skip positions already done
                continue
            pause_for_refill( 25 )  # time for one scan
            umv( diffty, ypos[i] )
            if (i % 2) == 0: # forwards
                 finterlaced( diffrz,   0,  rstep, num_proj, expotime, mode='FORWARD' )
            else:
                 finterlaced( diffrz, 720, -rstep, num_proj, expotime, mode='FORWARD' )
    umv(diffty, ycen)
    diffty_airpad.off()
    

    

class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )

        
            
        


def half1(ymax,
          datasetname,
          ystep=1.,
          ymin=-1801,
          rstep=0.05,
          astart = -90,
          arange = 361,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        plotselect("eiger:roi_counters:roi1_max")
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)




def next_rstart( stepsize, speed, home_range=15. ):
    """ Computes the next start position to keep going """
    if abs(360 - (rot.dial % 360)) < home_range: # if you are within range, go home
        print("Homing because there is a home nearby")
        rot.home()
    elif rot.dial*rot.steps_per_unit > pow(2,30):
        print("Running out of steps so homing")
        rot.home()
    pnow = rot.position
    offset = speed * speed / ( 2 * rot.acceleration )
    next_start = ( int(np.floor( (pnow + np.sign(stepsize)*offset)/stepsize )) + 1 ) * stepsize
    return next_start
            
def full1(ymax,
          datasetname,
          ystep=0.25,
          ymin=-1801,
          rstep=0.05,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    expotime = time for counting

    example:
       user.full1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.full1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    NR = int(361/rstep)
    umv(dty, max(ypos[0], ymin) )
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=( cleanup.axis.POS, ) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                rstart = next_rstart( rstep, angular_velocity )
                fscan2d( dty, ypos[i], ystep, 1, rot, rstart, rstep, 361/abs(rstep), expotime )
                # 7 min 53s with 25 scans == 19 seconds per scan
                
    umv(dty, 0)
