
#rack support
import time
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def step_ramp_cryo():
    temps = [350,300,250,200]   #fill
    ramprate=360
    rest=300                #fill    K/hour
    
    for k in range(len(temps)):
        print('Setting temperature to',temps[k],'K \n')
        samplename='cryo'    #fill
        title = str(temps[k]) + 'K'
        ox800.ramprate = ramprate
        ox800.setpoint = temps[k]
        print('Going to',ox800.setpoint,' K at',ox800.ramprate,' K/hr \n')
        
        while ox800.is_ramping():
            #newdataset(samplename+'_rampscan_to_'+title)
            #print('Temperature is',ox800_axis.position,'K \n')
            #powder_scan()
            print('Temperature is',ox800_axis.position,'K \n')
            sleep(30)
        
        for s in range(rest,0,-1): 
            print(f"Stabilising temperature at {temps[k]}: {s} s ",end='\r')
            sleep(1)  
        print('Temperature is',ox800_axis.position,'K \n')
        newdataset(samplename+'_PDF_'+title)
        #pause_for_refill(60)
        loopscan(200, 5)




def step_ramp_blow_500():
    temps = [500]     #fill
    ramprate=60                 #fill C/min
    threshold=0.5
    rest= 3600
    
    for k in range(len(temps)):
        print('Setting temperature to',temps[k],'C \n')
        samplename='blower'    #fill
        title = str(temps[k]) + 'C'
        euro1.ramprate = ramprate
        euro1.setpoint= temps[k]
        print('Going to',temps[k],' C at',ramprate,' C/min \n')
 
        while (abs(euro1_axis.position-temps[k]) > threshold):
            print('Temperature is',euro1_axis.position,'C \n')
            sleep(10)         
       
        for s in range(rest,0,-1): 
            print(f"Stabilising temperature at {temps[k]}: {s} s ",end='\r')
            sleep(1)  
        
        print('Temperature is',euro1_axis.position,'C \n')
        newdataset(samplename+'_PDF_'+title)
        pause_for_refill(60)
        loopscan(400,5)


def step_ramp_blow_several():
    temps = [ 601]     #fill
    ramprate=60                 #fill C/min
    threshold=0.5
    rest= 180
    
    for k in range(len(temps)):
        print('Setting temperature to',temps[k],'C \n')
        samplename='dehydration'    #fill
        title = str(temps[k]) + 'C'
        euro1.ramprate = ramprate
        euro1.setpoint= temps[k]
        print('Going to',temps[k],' C at',ramprate,' C/min \n')
 
        while (abs(euro1_axis.position-temps[k]) > threshold):
            print('Temperature is',euro1_axis.position,'C \n')
            sleep(10)         
       
        for s in range(rest,0,-1): 
            print(f"Stabilising temperature at {temps[k]}: {s} s ",end='\r')
            sleep(1)  
        
        print('Temperature is',euro1_axis.position,'C \n')
        newdataset(samplename+'_PDF_'+title)
        pause_for_refill(60)
        loopscan(100,10)


def step_ramp_blow_100():
    temps = [120]     #fill
    ramprate=60                 #fill C/min
    threshold=0.5
    rest= 3600

    for k in range(len(temps)):

        for t in range(8):
            print('Setting temperature to',temps[k],'C \n')
            samplename='blower'    #fill
            title = str(temps[k]) + 'C' 
            euro1.ramprate = ramprate
            euro1.setpoint= temps[k]
            print('Going to',temps[k],' C at',ramprate,' C/min \n')
 
            while (abs(euro1_axis.position-temps[k]) > threshold):
                print('Temperature is',euro1_axis.position,'C \n')
                sleep(10)         
       
            for s in range(rest,0,-1): 
                print(f"Stabilising temperature at {temps[k]}: {s} s ",end='\r')
                sleep(1)  
        
            print('Temperature is',euro1_axis.position,'C \n')
            newdataset(samplename+'_PDF_'+title)
            loopscan(400,5)
            

    euro1.setpoint=25    


def blower_total():
    step_ramp_blow_100()
    #step_ramp_blow_several()




def PDF_scan(count_time=0.5, images=100): 
    print('predicted time in %f'%(images*count_time))
    loopscan(images, count_time)
    print('done...')


def powder_scan(count_time=0.5, images=2): 
    print('predicted time in %f'%(images*count_time))
    loopscan(images, count_time)
    print('done...')



def grid_scan():
    positions=[1,2,3]     #fill
    samples=['bla', 'ooo', 'kkk']     #fill
    for y in range(len(positions)):
        print('position is',positions[y],' for diffy \n')
        umv(diffy, positions[y])
        filename=samples[y]
        newdataset(filename)
        #hz or rock?
        PDF_scan()

    






