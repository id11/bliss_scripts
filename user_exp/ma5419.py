

import time, numpy as np
user_script_load('optics.py')


def check_cpm18(pos=6.3558):
#def check_cpm18(pos=10.1):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()


def half1(ymax, datasetname, ystep=0.25, ymin=-1801):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.125, 1448, 0.005 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 181, -0.125, 1448, 0.005 )




def fridayLunch():
    start = time.time()
    half1( 6.5, 'Z520_100nm1', ystep=0.1 )
    print('timing',time.time()-start)
    half1( 6.5, 'Z520_100nm2', ystep=0.1 )
    print('timing, 2slice',time.time()-start)


def fridayNight():
    zpositions = np.arange(52,52.01, 0.1)
    for zpos in zpositions:
        umv(pz, zpos)
        half1( 7.0, '3D_Z%d_100nm2'%(zpos*10), ystep=0.1 )

def SaturdayNight():
    zpositions = np.arange(50,54, 0.1)
    for zpos in zpositions:
        umv(pz, zpos)
        half1( 6.0, '3D_Z%d_100nm2'%(zpos*10), ystep=0.1 )

def SundayNight():
    zpositions = np.arange(49,44.99, -0.1)
    for zpos in zpositions:
        umv(pz, zpos)
        half1( 7.0, '3D_Z%d_100nm'%(zpos*10), ystep=0.1 )


def mondaymorning():
    zpositions = np.arange(44.9,44.7, -0.1)
    for zpos in zpositions:
        umv(pz, zpos)
        half1( 7.0, '3D_Z%d_100nm'%(zpos*10), ystep=0.1 )


def CdTescan():
    zpositions = np.arange(40., 45.6, 0.1  )
    for zpos in zpositions:
        umv(pz, zpos)
        half1( 4.0, '3D_Z%d_1'%(zpos*10), ystep=0.1 )


def CdTescanTop():
    zpositions = np.arange(40., 39, -0.1  )
    for zpos in zpositions:
        umv(pz, zpos)
        half1( 4.0, '3D_Z%d_2'%(zpos*10), ystep=0.1 )        
