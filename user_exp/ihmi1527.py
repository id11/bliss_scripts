
import time, numpy as np


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

import time

def goto_temperature(t,r = 360, tol=0.2):
    print('send ramprate',r)
    ox700.ramprate = r
    sleep(2)
    print('send setpoint',t)
    ox700.setpoint = t
    while 1:
        time.sleep(2)
        tnow = ox700.input.read()
        terr = t - tnow
        print('tnow',tnow,'terr %.4f'%(terr))
        if ( abs( terr ) < tol ) and ( ox700.setpoint == t ):
            print('arrived at temperature')
            break
        if ox700.setpoint != t :
            ox700.setpoint = t


user_script_load( 'difftomomacro' )
            
def rt_test():
    prdef(user.half1)
    elog_add()
    elog_print("user.half1( 10, 'rt1', ystep=0.05, astart = 0, expotime=0.002 )")
    user.half1( 10, 'rt1', ystep=0.05, astart = 0, expotime=0.002 )
