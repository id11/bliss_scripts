
import time
import numpy as np
# 1.17 done
# sample top is at 1.07

def volscan():
    umv( shtz, 1.085 )

    for zpos in np.arange(1.085, 2.085, 0.04):
        umv( shtz, zpos )
        fscan2d(rot, 3060, -17, 181, dty,-800,5,321,.05, mode='ZIGZAG')
        umv( shtz, zpos + 0.02 )
        fscan2d(rot,    0,  17, 181, dty,-800,5,321,.05, mode='ZIGZAG')

def volscan2():
    # continue with larger range in new dataset
    for zpos in np.arange(1.365, 2.085, 0.04):
        umv( shtz, zpos )
        fscan2d(rot, 3060, -17, 181, dty,-1000,5,400,.05, mode='ZIGZAG')
        umv( shtz, zpos + 0.02 )
        fscan2d(rot,    0,  17, 181, dty,-1000,5,400,.05, mode='ZIGZAG')

def volscan3():
    # continue with larger range in new dataset
    for zpos in np.arange(1.3, 1.025, -0.04):
        umv( shtz, zpos )
        fscan2d(rot, 0, 17, 361, dty,-800,5,321,.05, mode='ZIGZAG')
        umv( shtz, zpos - 0.02 )
        fscan2d(rot,6120,  -17, 361, dty,-800,5,321,.05, mode='ZIGZAG')



def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

TESTING = False


def myfscan(*a,**k):
    print(a,k)
    if TESTING:
        return
    try:
        fscan(*a,**k)
    except: # pretty hopeless but why not ?
        print("Something has gone wrong!!! Retry scans!!")
        elog_print("Retry a scan !!!")
        print("Sleep a minute or two first")
        time.sleep(60)
        pause_for_refill(120)
        fscan(*a,**k)
        
def myumv(*a,**k):
    print(a,k)
    if TESTING:
        return
    umv(*a,**k)

def nscopedtylayerscan(y_start, y_step, ny, a_time, rstart, rstep, rend):
    eiger.camera.photon_energy = 65350.8
    for i,r in enumerate(np.arange(rstart,rend,rstep)):
        myumv(rot, r )
        pause_for_refill(ny*a_time+1)
        if i%2 == 0:
            myfscan(dty, y_start, y_step, ny, a_time, scan_mode='CAMERA')
        else:
            myfscan(dty, y_start+y_step*ny, -y_step, ny, a_time, scan_mode='CAMERA')



def vol35umRed():
    for zpos in np.arange(50,85,1): # 43 scans
        if int(zpos) < 84:
            continue
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -30, 1, 60, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -30, 1, 60, 0.3, 180, -2, -0.1 )
            

def vol35umGreen():
    # R.G.B is 
    for zpos in np.arange(34,72.1,1): # 43 scans
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -30, 1, 60, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -30, 1, 60, 0.3, 180, -2, -0.1 )
            

def vol35umBlue():
    # R.G.B is 31 -> 72
    for zpos in np.arange(32,73.1,1): # 43 scans
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -30, 1, 60, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -30, 1, 60, 0.3, 180, -2, -0.1 )

def vol35umBlueII():
    # R.G.B is 31 -> 72
    for zpos in np.arange(28,67.1,1): # 43 scans
        if zpos < 54:
            continue
        myumv( pz, zpos )
        newdataset("z2_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -30, 1, 60, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -30, 1, 60, 0.3, 180, -2, -0.1 )
            

def vol40umRed():
    # R.G.B is 31 -> 72
    for zpos in np.arange(40,83.1,1): # 43 scans
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -35, 1, 70, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -35, 1, 70, 0.3, 180, -2, -0.1 )

def vol40umGreen():
    # R.G.B is 26 -> 70
    for zpos in [35,62]+list(np.arange(43,58,1.5)) :
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -35, 1, 70, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -35, 1, 70, 0.3, 180, -2, -0.1 )
            
def vol40umBlue():
    # R.G.B is 20 -> 64
    for zpos in np.arange(20,64,2) :
        myumv( pz, zpos )
        newdataset("z_%d"%(zpos))
        if zpos%2 == 0:
            nscopedtylayerscan( -35, 1, 70, 0.3, 0, 2, 180.1 ) 
        else:
            nscopedtylayerscan( -35, 1, 70, 0.3, 180, -2, -0.1 )


def gotoVolt(V):
    hmc.output='ON' 
    hmc.voltage_setpoint=V
    newdataset('ramping_to_%d'%(V))
    loopscan(10000,2,nanodac_temp,hmc)
    
def goto0Volt():
    hmc.voltage_setpoint=0
    hmc.output='OFF' 
    newdataset('descending_to_0V')
    loopscan(10000,2,nanodac_temp,hmc)
    
def gotoVoltStop(V):
    hmc.output='ON' 
    hmc.voltage_setpoint=V
    newdataset('ramping_to_%d_and_stop'%(V))
    try:
       print('Stop heating by [Ctrl+C]')
       loopscan(10000,2,nanodac_temp,hmc)
    except:
       print('Stopped the heating. Measuring cooling.')
       print('Stop hmc by [Ctrl+C]')
       hmc.voltage_setpoint=.01
       loopscan(10000,2,nanodac_temp,hmc)
    finally:
       hmc.voltage_setpoint=0
       hmc.output='OFF'
       print('Turned hmc completely off.')
     

def map2d(n2steps):
    tz = 0
    for zpos in np.arange(-nsteps,nsteps,1) :
         tz += 1
         umvr( pz, 0.5 )
         if tz%2 == 1:
            fscan(dty,-nsteps,0.5,2*nsteps,0.1,scan_mode='CAMERA')
         else:
            fscan(dty,nsteps,-0.5,2*nsteps,0.1,scan_mode='CAMERA')


def rampscan():
       volt = [5.55, 8.925, 10.05, 11.625, 14.55, 17.925, 20.175]
       hmc.output='ON' 
       k=1
       for voltage in volt:
           k +=1
           hmc.voltage_setpoint=voltage
           newdataset("ramping_to_%03d"%(voltage))
           loopscan(300,1)
           newdataset('align_%03d'%(voltage))
           #user.aligncor....
           newdataset('tomo_%d'%(volt))
           nscopedtylayerscan(-25, 0.5, 100, 0.1, 0, 1.2,181.2) 

def detectorcorner():
   sheh3.close() 
   umv(det2y,-211.53,ebsy,-14.45)
   sheh3.open()
   ct(0.1)
   print('ready to take picture with eiger in a corner')



def detectorcenter():
   sheh3.close() 
   umv(det2y,-234.54,ebsy,8.55)
   sheh3.open()
   ct(0.1)
   print('ready to take picture with eiger in the center')


#fscan(dty,-3500,2,3500,0.01,scan_mode='CAMERA')

#fscan(dty,-3500,1,7000,0.01,scan_mode='CAMERA')
           
      
