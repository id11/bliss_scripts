
import numpy as np, time

def lunch():
    newdataset('zslices')
    for tz in np.arange( -0.15, 0.15, 0.02 ):
        umv(samtz, tz)
        finterlaced( diffrz, 0, 0.5, 720, 0.08, mode='FORWARD')
        umv(samtz, tz+0.01)
        finterlaced( diffrz, 720, -0.5, 720, 0.08, mode='FORWARD')

def tea():
    newdataset('zslices')
    for tz in np.arange( 2.285, 2.49, 0.02 ):
        umv(samtz, tz)
        finterlaced( diffrz, 0, 0.5, 720, 0.08, mode='FORWARD')
        umv(samtz, tz+0.01)
        finterlaced( diffrz, 720, -0.5, 720, 0.08, mode='FORWARD')        

        
def difftomo():
    d0 = 13.594
    umv(samtz,2.31)
    newdataset('dtE')
    for ty in np.arange(-0.2,0.2001,0.005):
        umv(diffty, d0+ty)
        finterlaced( diffrz, 0, 1, 180, 0.08, mode='ZIGZAG' )
    umv(diffty, d0 )
    sheh3.close()
    print("all done")
    
def interlaced_zscan(temperature):
    z0 = -0.470
    umv(samtz, z0)
    zpoints = np.arange(0,0.221,0.010)
    for z in zpoints:
        umv(samtz, z0+z)
        newdataset('insitu_%d_deg_z_%s'%(temperature,int(z*1000)))
        finterlaced(diffrz,-90,1,180,0.1)

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)


def temperature_scan():
    z0 = -0.470
    temps = np.arange(310, 421, 4 )
    for temp in temps:
        umv(samtz,z0)
        newdataset('zscans_T%d'%(temp))
        nanodacpool.setpoint=temp
        while nanodacpool.is_ramping():
            print('Temperature is',nanodacpool_temp.read(),'C')
            sleep(5)
        # high temperature scan for thermal expansion
        plotselect('frelon3:roi_counters:roi2_std')
        dscan(samtz,-0.15,0.45,40,0.1)
        nanodacpool.setpoint=300
        while nanodacpool.is_ramping():
            print('Temperature is',nanodacpool_temp.read(),'C')
            sleep(5)
        print('Now sleeping 60 seconds')
        sleep(60)
        pause_for_refill(20)
        dscan(samtz,-0.15,0.45,40,0.1)
        interlaced_zscan( temp )

