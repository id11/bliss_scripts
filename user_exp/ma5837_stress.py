#MA5837 stress

from datetime import datetime

def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")
      

def load_ramp_by_target(target, time_step=0.05, exp_time=0.2):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached
    """
    stress_regul.plot()
    current_load = stress_adc.get_value()
    if abs(target - current_load) < 1:
        print('target position is to close to current position - aborting !')
        print(_get_human_time(),stress.position,current_load)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            sct(exp_time)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            sct(exp_time)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load)
    return

