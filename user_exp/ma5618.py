def large_cell_scan():
    sheh3.open()
    diffty_airpad.on()
    diffty_pos = [14.859,14.859+1.1,14.859+2.2,14.859+3.3,14.859+4.4,14.859+5.5,14.859+6.6,14.859+7.7,14.859+8.8]
    for i,ypos in enumerate(diffty_pos):
        print(i,ypos,'PCT_%s'%i)
        umv(diffty,ypos)
        fulltomo.full_turn_scan('PCT_%s'%i)
    diffty_airpad.off()
    sheh3.close()
    
def large_cell_scan_rest():
    sheh3.open()
    diffty_airpad.on()
    diffty_pos = [14.859,14.859+1.1,14.859+2.2,14.859+3.3,14.859+4.4,14.859+5.5,14.859+6.6,14.859+7.7,14.859+8.8]
    for i,ypos in enumerate(diffty_pos):
        if diffty_pos[i]>14.859+7:
            print(i,ypos,'PCT_%s'%i)
            umv(diffty,ypos)
            fulltomo.full_turn_scan('PCT_%s'%i)
    diffty_airpad.off()
    sheh3.close()    
    
def z_layer_scan():
    #mid
    #newsample('PGA_middle_soc100_88kev_2')
    #umv(difftz,26.95)
    #large_cell_scan()
    #bottom
    newsample('PGA_bottom_soc100_88kev_3')
    umv(difftz,26.95+28.3)
    large_cell_scan_rest()
    #top
    #newsample('PGA_top_soc100_88kev')
    #umv(difftz,26.95-18.8)
    #large_cell_scan()
    
def loop_tomo(sample_name,xpos,ypos):
    newsample(sample_name)
    umv(diffty,14.859)
    umv(samtx,xpos,samty,ypos)
    for i in range(0,200):
        fulltomo.full_turn_scan('PCT_%s'%i)
        
def different_roi_tomo_scan():
    umv(diffty,14.859)
    #umv(samtx,0.45,samty,0.7-8.8)
    #fulltomo.full_turn_scan('pga_soc100_x0d45_my8d1_PCT_broad_FOV')    
    #umv(samtx,0.45+8.8,samty,0.7)
    #fulltomo.full_turn_scan('pga_soc100_x9d25_y0d7_PCT_broad_FOV')
    #umv(samtx,0.45-2.2,samty,0.7)
    #fulltomo.full_turn_scan('pga_soc100_mx1d75_y0d7_PCT_broad_FOV')
    #umv(samtx,0.45-4.5,samty,0.7)
    #fulltomo.full_turn_scan('pga_soc100_mx4d05_y0d7_PCT_broad_FOV')
    #umv(samtx,0.45-8.8,samty,0.7)
    #fulltomo.full_turn_scan('pga_soc100_mx8d35_y0d7_PCT_broad_FOV')
    #umv(samtx,0.45,samty,0.7-2.2)
    #fulltomo.full_turn_scan('pga_soc100_x0d45_my1d5_PCT_broad_FOV')
    #umv(samtx,0.45,samty,0.7-4.5)
    #fulltomo.full_turn_scan('pga_soc100_x0d45_my3d8_PCT_broad_FOV')
    #umv(samtx,0.45-4.5,samty,0.7-4.5)
    #fulltomo.full_turn_scan('pga_soc100_mx4d05_my3d8_PCT_broad_FOV')
    #umv(samtx,0.45+4.5,samty,0.7+4.5)
    #fulltomo.full_turn_scan('pga_soc100_x4d95_my5d2_PCT_broad_FOV')            
    umv(samtx,0.45-4.5,samty,0.7+4.5)
    fulltomo.full_turn_scan('pga_soc100_mx4d05_y5d2_PCT_broad_FOV')
    umv(samtx,0.45+4.5,samty,0.7-4.5)
    fulltomo.full_turn_scan('pga_soc100_x4d95_my3d8_PCT_broad_FOV')     

