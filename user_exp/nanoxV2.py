import time

tfoh1 = config.get("tfoh1")


"""Valeur du gain pour l'output"""
SCALEOUTPUT = 0.025

"""Valeurs pour dessiner le sinus de l'essai de fatigue"""
CYCLELENGTH = 3125 #3125 pts = 0.0625s
SINUSLENGTH = 3125
SINUSSTART = 0
SINUSCENTER = 1562

"""Valeurs de force (N) pour l'essai"""
Vmax = 107.6
Vmin = 10.8
amplitude = Vmax - Vmin

"""Nombre de cycles"""
Ncycles = 5000
Ncycles_total = 274000
"""Faire l'auto-zéro ?"""
do_zero = "no"


def sinus(amp, amp_offset, nb):
    """Function to create a sine wave signal, start the sine wave signal and grab images

    This function creates a sine wave with the specified parameters with the WAVE_SIN_P command
    The WSL command set the connection of Wave Table to Wave Generator
    The WGC command set the number of cycles
    The command is started with WGO

    @param float amp: the amplitude in Newton
    @param float amp_offset : the offset of the amplitude in Newton
    @param int nb : number of cycles to do
    """
    
    wavegenerator = 1
    wavetable = 1

    ctrl = stress.controller

    print(f'Set sine waveforms for wave table {wavetable}')
    print(f'nb of cycles: {nb}')
    ctrl.command(f"WAV {wavetable} X SIN_P {CYCLELENGTH} {amp} {amp_offset} {SINUSLENGTH} 0 {SINUSCENTER}")

    ctrl.command(f"WSL {wavegenerator} {wavetable}")
    ctrl.command(f"WGC {wavegenerator} {nb}")

    ctrl.command("SVO 1 1")

    # start = input('ready to go ? [yes/no] ')
    start = "yes"
    if start == "yes":
        ctrl.command(f"WGO {wavegenerator} 1")
        
        """Attention on utilise directement une commande bliss ici"""
        is_running = bool(int(ctrl.raw_state()))
        while is_running == True:
            time.sleep(1)
            is_running = bool(int(ctrl.raw_state()))
        
    else:
        print("No test done")
    print("Test Done")


def fatigue():
    '''This function set and start the fatigue test.'''
    
    # Vmax = float(input('Enter max value for amplitude (Newton) : '))
    # Vmin = float(input('Enter min value for amplitude (Newton) : '))
    # Ncycles = int(input('Enter number of cycles for the fatigue test : '))
    
    # amplitude = Vmax - Vmin

    sinus(amplitude, Vmin, Ncycles)


def set_controller():
    ctrl = stress.controller
    
    """Init output"""
    ctrl.command("CCL 1 advanced")
    ctrl.command("SPA 2 0x0A000003 2")
    ctrl.command("SPA 2 0x0A000004 1")
    ctrl.command(f"SPA 1 0x07001005 {SCALEOUTPUT}")

    """Init signal piezo/controller"""
    ctrl.command("SPA 3 0x02000200 0")
    ctrl.command("SPA 3 0x02000300 1.93")

    """Set P I D"""
    ctrl.command("SPA 1 0x07000300 2.25")#P
    ctrl.command("SPA 1 0x07000301 0.0155")#I
    ctrl.command("SPA 1 0x07000302 0")#D
    
    ctrl.command("SPA 1 0x08000100 50")#Notch frequency 1
    ctrl.command("SPA 1 0x08000200 0.2")#Notch rejection 1
    ctrl.command("SPA 1 0x08000300 1")#Notch bandwidth 1
    
    ctrl.command("SPA 1 0x08000101 150")#Notch frequecy 2
    ctrl.command("SPA 1 0x08000201 0.05")#Notch rejection 2
    ctrl.command("SPA 1 0x08000301 1")#Notch bandwidth 2
    
    ctrl.command("SPA 1 0x08000400 0")#Creep factor T1/sec
    ctrl.command("SPA 1 0x08000401 0")#Creep factor T2/sec
    
def run():
    ctrl = stress.controller
    set_controller()
    """Set velocity"""
    ctrl.command("VEL 1 20000")
    
    #do_zero = input("Do Auto-Zero ? [yes/no]")
    if do_zero == "yes":
        ctrl.command("ATZ 1 0")
        time.sleep(8)
        print("Auto-Zero done")
    else:
        fatigue()
        
def switch_to_pct():
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    close_ffdoor()
    print('do not forget to move beamstop d3_bsty by -2000 microns !')
    tfoh1.set(0,'Be')
    centeredroi(marana3,1024,1300)
    
    slit(1.4, 1.8)
    sct(0.1)
    umv(d3ty, 0)
    umvct(d3x, 50)
    
def switch_to_pct_hr():
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    close_ffdoor()
    print('do not forget to move beamstop d3_bsty by -2000 microns !')
    tfoh1.set(0,'Be')
    centeredroi(marana3, 2048, 2048)
    umv(atty, -10, attrz, 0)
    slit(1.4, 1.8)
    sct(0.1)
    umv(d3ty, 0)
    umvct(d3x, 50)

def switch_to_dct():
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    close_ffdoor()
    print('do not forget to move beamstop d3_bsty by +2000 microns !')
    tfoh1.set(24,'Be')
    centeredroi(marana3,2048,2048)
    umv(atty, -10, attrz, 0)
    umv(d3ty, 0)
    umvct(d3x, 9.5)
    slit(0.7, 0.25)
    
def switch_to_ff():
    umv(d3ty, 150)
    ACTIVE_MG.enable('frelon3*')
    ACTIVE_MG.disable('marana3*')
    tfoh1.set(0,'Be')
    slit(0.7, 0.5)
    umv(ffdoor, -170) 
    umv(atty, 0, attrz, -4)
    ct(0.01)
    # newdataset('ff_%06dcycle' % Ncycles_total)
    # finterlaced(diffrz, 0, 0.2, 1800, 0.185)
    
def run_sequence(n_seq, current_Ncycles, do_ff_on=[]):
    ctrl = stress.controller
    
    print(f"trying to run sequence with {Ncycles} cycles")
    print(f"starting sequence at {current_Ncycles}")
    for i in range(n_seq):
        # apply fatigue cycles
        run()
        current_Ncycles += Ncycles
        print(f"number of cycles is {current_Ncycles}")
        # slow down to avoid overshooting
        ctrl.command(f"VEL 1 50")
        ctrl.command(f"MOV 1 {Vmax}")
        time.sleep(5)
        print("Moved to Vmax")
        scanname = 'pct_%06dcycle' % current_Ncycles
        fulltomo.full_turn_scan(scanname)
        # check if we need to collect FF data for this one
        if i in do_ff_on:
            switch_to_ff()
            ff_scanname = '%06dcycle_ff' % current_Ncycles
            ff_zseries2(ff_scanname)
            switch_to_pct_hr()
        ctrl.command(f"MOV 1 {Vmin}")
        time.sleep(5)
        print("Moved back to Vmin")

def force():
    print(stress_adc.get_value())

def goto_Vmin():
    ctrl = stress.controller
    ctrl.command(f"VEL 1 50")
    ctrl.command(f"MOV 1 {Vmin}")
    time.sleep(2)
    print(stress_adc.get_value())

def goto_Vmax():
    ctrl = stress.controller
    ctrl.command(f"VEL 1 50")
    ctrl.command(f"MOV 1 {Vmax}")
    time.sleep(5)
    ctrl.command(f"VEL 1 20000")
    print(stress_adc.get_value())

def ff_zseries(ff_scanname='ff'):
    shift_step_size = 0.2
    samtz_cen = 0.96768
    nof_shifts = 3
    offset_samtz_pos = samtz_cen - (nof_shifts - 1) * shift_step_size / 2
    for iter_i in range(nof_shifts):
        umv(samtz, iter_i * shift_step_size + offset_samtz_pos)
        newdataset(ff_scanname + str(iter_i + 1))
        finterlaced(diffrz, 0., 0.2, 1800, 0.185)
    umv(samtz, samtz_cen)
    print('FF series scans done')

def ff_zseries2(ff_scanname='ff'):
    shift_step_size = 0.45
    samtz_cen = 0.93768
    nof_shifts = 2
    offset_samtz_pos = samtz_cen - (nof_shifts - 1) * shift_step_size / 2
    for iter_i in range(nof_shifts):
        umv(samtz, iter_i * shift_step_size + offset_samtz_pos)
        newdataset(ff_scanname + str(iter_i + 1))
        finterlaced(diffrz, -9.0, 0.2, 1800, 0.185)
    umv(samtz, samtz_cen)
    print('FF series scans done')

def tensile():
    ctrl = stress.controller
    ctrl.command("VEL 1 50")
    loads_MPa = [55, 700, 750, 800, 830, 860, 890, 920, 950, 980, 1010, 1040]
    section = 0.332 * 0.339
    loads_N = np.array(loads_MPa) * section
    for load in loads_N:
        print(f"moving to {int(load)} N")
        ctrl.command(f"MOV 1 {int(load)}")
        time.sleep(5)        
        # acquire PCT scan
        new_scanname = f"pct_{int(load)}N"
        fulltomo.full_turn_scan(new_scanname)
        # collect FF data
        switch_to_ff()
        ff_scanname = f"ff_{int(load)}N"
        newdataset(ff_scanname)
        finterlaced(diffrz, -13., 0.2, 1800, 0.185)
        switch_to_pct_hr()

def tensile_night_fever():
    ctrl = stress.controller
    ctrl.command("VEL 1 50")
    loads_MPa = [1070, 1100, 1130, 1160, 1190]
    section = 0.332 * 0.339
    loads_N = np.array(loads_MPa) * section
    for load in loads_N:
        print(f"moving to {int(load)} N")
        ctrl.command(f"MOV 1 {int(load)}")
        time.sleep(5)        
        # acquire PCT scan
        new_scanname = f"pct_{int(load)}N"
        fulltomo.full_turn_scan(new_scanname)
        # collect FF data
        switch_to_ff()
        ff_scanname = f"ff_{int(load)}N"
        newdataset(ff_scanname)
        finterlaced(diffrz, -13., 0.2, 1800, 0.185)
        switch_to_pct_hr()

def compliance_method(current_Ncycles):
    ctrl = stress.controller
    n_scans = 4
    for i in range(1, n_scans):
        amplitude = Vmax - Vmin
        chosen_stress = Vmax - amplitude * i / (n_scans - 1)
        ctrl.command("VEL 1 50")
        ctrl.command(f"MOV 1 {chosen_stress}")
        time.sleep(5)        
        new_scanname = f"pct_{current_Ncycles:06d}cycle_{int(chosen_stress)}N"
        fulltomo.full_turn_scan(new_scanname)


def stop():
    ctrl = stress.controller
    
    ctrl.command("WGO 1 0")
    ctrl.command("VEL 1 50")
    ctrl.command(f"MOV 1 {Vmin}")

    
