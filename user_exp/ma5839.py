
import sys
import numpy as np
import time
import os
from datetime import datetime


def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)


#user_script_load('difftomomacro', export_global='nc')

tfoh1 = config.get('tfoh1')


def switch_to_dct():
    sheh3.close()
    tfoh1.set(16,'Be')
    #tfoh1.set(64,'Al')
    marana_in()
    umv(atty,-10,attrz,0)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3:image')
    ACTIVE_MG.enable('marana3:roi*')
    sheh3.open()
    umvct(s7vg, 0.1, s7hg, 0.65, s8vg,0.2, s8hg, 0.8 )

def switch_to_pct():
    sheh3.close()
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    ff_out()
    umv(atty,-10,attrz,0)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3:image')
    ACTIVE_MG.enable('marana3:roi*')
    sheh3.open()
    umvct(nfdtx,100)   # if you need furnace increase this
    umvct(d3ty,0, d3tz, 0)
    umvct(s7vg, 2, s7hg, 2, s8vg,2.5, s8hg, 2.5)
    
    
def switch_to_ff():
    sheh3.close()
    tfoh1.set(0,'Be')
    ff_in()
    umv(atty,0,attrz,-5)  # strong attenuation  : y=0 is in the beam, y=-10 is out of bean
                          #  rz = 0  parallel to the beam. rz = -90 is perpendicular
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.enable('frelon3*')
    sheh3.open()
    umvct(s7vg, 0.1, s7hg, 0.65, s8vg,0.2, s8hg, 0.8)
   
def ff_out():
    sheh3.close()
    umv(ffdtx1, 530)
    umv(ffdtz1, 400)
    
def marana_in():
    ff_out()
    umv(d3ty, 0, d3tz, 0)
    umv(nfdtx, 100)

def marana_out():
    umv(nfdtx,100)
    umv(d3ty,130)
    
def ff_in():
    marana_out()
    umv(ffdtz1, 0)
    umv(ffdtx1, 130) 


def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0.0
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.1
    dct_pars['refon'] = dct_pars['num_proj']
    dct_pars['ref_step'] = -3
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.65
    dct_pars['slit_vg'] = 0.05
    dct_pars['dist'] = 8.0
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = -0.64 + 0.08
    # +0.08=40um*2=step_size*(nof_shifts-1)/2
    # NoX_samtz of top_scan centre(num of slice),
    # No9Re_3.85(11), No12Re_1.85(5), No10Re_-0.20_0.58(5)+0.78(7) ,No11Re_-0.68_-0.42(5),No22Re_-1.1_-0.9(5)
    # No19Re_-1.9_-1.62(5), No7Annealing15s_-2_-1.72(5) No3_15sAnnealling_-2.6_-2.12(5) 
    # No7Annealing30s_-2_-1.72(5) # No7Annealing60s_-2.3_-1.72(5) No21_Cu_Re_-1.5_-0.56(5)
    dct_pars['shift_step_size'] = 0.04
    dct_pars['nof_shifts'] = 5
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    from bliss.setup_globals import ftimescan
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)


    
def tomo_by_fscan_dict(scanname, pars):
    from bliss.setup_globals import fscan
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.05, s8hg, pars['slit_hg'] + 0.05)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])    

def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')


def ff_z_No22():
    z1 = 3.7504
#    for i,z in enumerate(3.7504, 3.7904
    for i in range(5):
        umv(samtz, z1 + i*0.04)
        newdataset(f"ff_zeries{i+1}")
        finterlaced(diffrz, 0, 0.1, 3600, 0.09)
    umv(samtz, 3.68044)
    newdataset("ff_z0_50um")
    finterlaced(diffrz, 0, 0.1, 3600, 0.09)
    
def ff_z_No12():
    z1 = 1.85
#    for i,z in enumerate(3.7504, 3.7904
    for i in range(5):
        umv(samtz, z1 + i*0.04)
        newdataset(f"ff_zeries{i+1}")
        finterlaced(diffrz, 0, 0.1, 3600, 0.09)

def ff_z_No21():
    z1 = -0.64
#    for i,z in enumerate(3.7504, 3.7904
    for i in range(5):
        umv(samtz, z1 + i*0.04)
        newdataset(f"ff_zeries{i+1}")
        finterlaced(diffrz, 0, 0.1, 3600, 0.09)


class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )

        


def half1(ymax,
          datasetname,
          ystep=0.25,
          ymin=-1801,
          rstep=0.05,
          astart = -90,
          arange = 181,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    from bliss.setup_globals import fscan2d
    from bliss.common import cleanup

    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)






def ns_z_series( name ):
    from bliss.setup_globals import nc
    zpos = np.arange( 10, 91, 10 )
    for i,z in enumerate(zpos):
        umv(pz, z)
        half1( 272, name + f"Z{z}", ystep=1.0, )
    
    
def ns_z_series_30um( name ):
    zpos = np.arange( 5, 96, 30 )
    z0 = -1.14
    umv(ntz,z0)
    for i,z in enumerate(zpos):
        umv(pz, z)
        half1( 262, name + f"Z{z}", ystep=1.0, )
    zpos = np.arange( 5, 96, 30 )
    umv(ntz,z0 + 0.09 + 0.03)
    for i,z in enumerate(zpos):
        umv(pz, z)
        half1( 262, name + f"Z{z}", ystep=1.0, )
    



