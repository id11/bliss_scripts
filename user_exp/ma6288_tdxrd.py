import sys
import numpy as np
import time
import os

atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')



def switch_to_marana3_pct():
    fsh.session = 'TDXRD'
    fsh.close()
    marana3_in()
    fsh.open()
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    #umvct(d3_bstz, 2000)
    tfoh1.set(0, 'Be')
    plotimagecenter(marana3)
    slit(1.5, 1.5)
    return 'Done'


def switch_to_ff(scan_bs=True):
    fsh.session = 'TDXRD'
    fsh.close()
    #tfoh1.set(18,'Be')
    ff_in(scan_bs)
    umv(atty,0,attrz,-4)
    ACTIVE_MG.disable('marana3*')
    ACTIVE_MG.enable('frelon3*')
    fsh.open()
    slit(0.95, 0.18)
    ct(0.05)


def marana3_in():
    ff_out()
    assert d1tz.position > 90
    #assert d2tz.position > 100
    assert d1ty.position < -200
    assert d2ty.position < -387
    assert ffdtx1.position > 440
    umv(nfdtx, 100)
    assert nfdtx.position > 95
    umv(d3ty, 0)
    umv(d3tz, 0)
    umvct(atty,-10,attrz,0)
    return 'Done'
    
    
def ff_in(scan_bs=True):
    marana3_out()
    assert d3ty.position > 169.0
    assert nfdtx.position < 55.0
    assert d2ty.position < -387
    umv(ffdtz1, 0)
    umv(ffdtx1, 200)
    if scan_bs:
        open_ffdoor(detector_name = 'frelon3')
    else:
        umv(ffdoor,-170)
        
    
def marana3_out():
    if furnace_z.position < 70:
        umv(d3ty,170)
        assert d3ty.position > 169.0
        umv(nfdtx,50)
    else:
        umv(d3ty,170)
        umv(nfdtx,50)
    

def ff_out():
    #umv(ffdoor, 0)
    umv(ffdtx1, 450)
    umv(ffdtz1, 400)


# OC4 sample
#pos_top = [0.925, -1.506, 7.79362, -11.662543]
#pos_bot = [0.625, -1.556, 7.79362, -0.1625430]

# SS316L sample
#pos_top = [-1.075, -1.676, 12.393620, -17.1625430]
#pos_bot = [-0.535, -1.526, 12.393620, -5.662543]

# IN625
# OLD:
# pos_top =[-0.777, -2.054, 12.283620, -20.662543]
# pos_bot = [-0.320, -2.055, 12.28362, -9.162543]
# NEW:

# OC4 sample in-situ
#pos_top =[1.196, -2.313, -0.466380, -49.40846]
#pos_bot = [1.021, -2.419, 0.533620, -38.90846]
#pos_bot = [0.971, -0.589, 0.533620, -38.90846]

#OC4_postmortem
#pos_top =[0.452, 0.239, 7.18362, -12.4084609]
#pos_bot =[0.937, -0.815, 7.183620, -0.9084609]

#IN625_cor
#pos_bot =[0.823, -0.981, 10.383620, -12.4084609]

#OC4_ESRFcor_postmortem
#pos_top =[0.696, -1.046, 10.47426, -16.7584609]
#pos_bot =[0.647, -1.332, 10.47426, -5.2584609]

#OC4_ESRFcor_postmortem nanoscope
pos_top =[-, -1.046, 10.47426, -16.7584609]
pos_bot =[0.647, -1.332, 10.47426, -5.2584609]


def run_insitu_pct_ff(offset_time = 773):
    t0 = time.time()
    dt = 0
    exit_flag = False
    i=0
    ff_start = 0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        print('Current temperature is {} C; Corrosion time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))
        
        # take pct
        switch_to_marana3_pct()
        goto_sample_bot()
        dt = time.time() - t0
        scanname = 'pct_bot_' + str(int(dt/60.0 + offset_time)) + 'min'
        take_pct(scanname=scanname)
        
        goto_sample_top()
        #t = time.time() - t0
        scanname = 'pct_top_' + str(int(dt/60.0 + offset_time)) + 'min'
        take_pct(scanname=scanname)
        
        
        # take ff
        switch_to_ff(scan_bs=False)
        if i==1:
            ff_start = dt
        goto_sample_top()
        dt = time.time() - t0
        scanname = 'ff_top_' + str(int(dt/60.0 + offset_time)) + 'min'
        take_ff_3DXRD(datasetname = scanname)            
            
        goto_sample_bot()
        dt = time.time() - t0
        scanname = 'ff_bot_' + str(int(dt/60.0 + offset_time)) + 'min'
        take_ff_3DXRD(datasetname = scanname)   
            
    print('Done successfully')


def take_dct_2regions():
    goto_sample_top()
    take_dct('top_dct')
    
    umvrct(d3x, 2)
    goto_sample_bot()
    umvrct(d3x, -2)
    take_dct('bot_dct')
    
    print('done')


def take_pct_2regions():
    tfoh1.set(0, 'Be')
    umvct(atty, -10, attrz, 0)
    slit(1.5, 1.5)

    goto_sample_top()
    fulltomo.full_turn_scan('pct_top')
    
    goto_sample_bot()
    fulltomo.full_turn_scan('pct_bot')
    
    print('done')


def take_ff_2regions():
    goto_sample_bot()
    take_ff_3DXRD('bot_ff')
    
    goto_sample_top()
    take_ff_3DXRD('top_ff')
    
    print('done')


def goto_sample_top(pos = pos_top):
    umv(samtx, pos[0])
    umv(samty, pos[1])
    umv(samtz, pos[2])
    umv(difftz, pos[3])


def goto_sample_bot(pos = pos_bot):
    umv(samtx, pos[0])
    umv(samty, pos[1])
    umv(samtz, pos[2])
    umv(difftz, pos[3])
    
def take_pct(scanname='pct'):
    tfoh1.set(0, 'Be')
    umvct(atty, -10, attrz, 0)
    slit(1.5, 1.5)
    newdataset(scanname)
    #fscan(diffrz, 0, 0.1, 3600, 0.05, scan_mode = 'CAMERA')
    #fscan(diffrz, 0, 0.1, 1900, 0.05, scan_mode = 'CAMERA')
    fscan(diffrz, 0, 0.05, 3800, 0.05, scan_mode = 'CAMERA')
    print('Done')
    
    
    
def take_dct(scanname='dct'):
    tfoh1.set(18, 'Be')
    #umvct(d3_bstz, 0)
    #umvct(d3x,6.8) # for OC4 and IN625 top
    #umvct(d3x,7.5)  # for IN625 bottom
    #umvct(d3x,8.0)  # for IN625 bottom
    umvct(d3x,7.0)  # for OC4_ESRFcor_postmortem 
    slit(0.95, 0.18) #for OC_4
    #slit(0.85, 0.08) # for IN625
    dct=DCTScan()
    dct.pars['nof_shifts'] = 6
    dct.pars['exp_time'] = 0.1
    dct.pars['ref_step'] = -2
    dct.pars['shift_step_size'] = s7vg.position - 0.03 # for OC_4
    #dct.pars['shift_step_size'] = s7vg.position - 0.01 # for IN625
    print('Updated dct parameters:')
    print(dct.pars)
    dct.run_zseries(scanname)



def take_ff_3DXRD(datasetname = 'ff'):
    slit(0.95, 0.18) #for OC_4, OC4_ESRFcor_postmortem
    #slit(0.85, 0.08) # for IN625
    ff_pars={}
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 0.25
    #ff_pars['num_proj'] = 180 / ff_pars['step_size']   # in the furnace
    ff_pars['num_proj'] = 360 / ff_pars['step_size']   # in the furnace
    ff_pars['exp_time'] = 0.185
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = samtz.position
    ff_pars['shift_step_size'] = s7vg.position - 0.03 #for OC_4
    #ff_pars['shift_step_size'] = s7vg.position - 0.01 #for IN625
    ff_pars['nof_shifts'] = 6
    
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    print('ff_zseries succeed')
    return('Succeed')


