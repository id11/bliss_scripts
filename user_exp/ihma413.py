import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime

atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')

# PS13_300C
#save_path = '/data/visitor/ihma413/id11/20231003/RAW_DATA/PS13_300C'
#loads = [10, 20, 30, 40, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150]
#loads = [80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150]
#loads = [140]

# PS13_RT
#save_path = '/data/visitor/ihma413/id11/20231003/RAW_DATA/PS13_RT'
#loads = [10, 20, 30, 40, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150]
#loads = [140]

# PS13_150C
save_path = '/data/visitor/ihma413/id11/20231003/RAW_DATA/PS13_150C'
#loads = [10, 30, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150]
loads = [140]

def switch_to_ff():
    fsh.close()
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    umv(nfdtx,100)
    umv(d3ty,100, d3tz, 0)
    # umv(atty,0, attrz,-12)
    umv(ffdtx1,200, ffdtz1,0)
    ACTIVE_MG.disable('marana*')
    ACTIVE_MG.enable('frelon3*')
    fsh.open()
    umvct(s7vg, 0.25, s7hg, 0.25, s8vg,0.4, s8hg, 0.4)

def switch_to_pct():
    tfoh1.set(96,'Al')
    tfoh1.set(18,'Be')
    fsh.close()
    umv(ffdtx1,350,ffdtz1,200)
    
    umv(d3ty,0,d3tz,0)
    umv(atty,-10,attrz,0)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana*')
    print("Ready to collect marana pct data") 
    fsh.open()
    #sct(0.05)
    umvct(s7vg, 2.5, s7hg, 2.5, s8vg,3, s8hg, 3)

def in_situ_ff_pct_loading(loads = loads, voltage_ramprate = 0.1, save_path = save_path, voltage_target = 120, tol = 0.15):
    sheh3.open()
    ACTIVE_MG.enable('stress*')
    print('Activated stress saving')
    stress_regul.plot()
    reach_max = False
    
    if voltage_target > 120:
        voltage_target = 120
        print('Maximum allowed voltage is 120')
        
    for i, load in enumerate(loads):
        initialize_stress_regul()
        print('Initialized stress regulation')
        
        newdataset('ff_ramp_up_' + str(int(load)) + 'N')
        switch_to_ff()
        
        print('Activating ramprate')
        regul_off(voltage_ramprate, voltage_target)
        print('Started voltage ramprate')
        
        file_name = 'load_ramp_up_' + str(int(load)) + 'N.txt'
        file_name = os.path.join(save_path, file_name)
        with open(file_name, 'w') as f:
            # ramping up the load
            t0 = time.time()
            dt = 0
            exit_flag = False
            while exit_flag == False:
                dt = time.time() - t0
                print('Waiting to reach the targe = {} N'.format(load))
                print('Current load is {} N; Ramping time is {} s'.format(stress_adc.get_value(), dt))
                print(_get_human_time(),stress.position,stress_adc.get_value(),file=f)
                
                if stress.position >= 119.8:
                    print('Reaching maximum !!!')
                    regul_off(voltage_ramprate, 119)
                    #initialize_stress_regul()
                    exit_flag = True
                    reach_max = True
                
                if np.abs(stress_adc.get_value() - load) <= tol or stress_adc.get_value() > load:
                    exit_flag = True
                    #initialize_stress_regul()
                sct(0.1)
#                finterlaced(diffrz,0,1,8,0.1)  # 8s
        
        initialize_stress_regul()
        # start pct measurement at constant load
        switch_to_pct()
        #fulltomo.full_turn_scan('pct_' + str(int(load)) + 'N')
        fulltomo.full_turn_scan('pct_' + str(int(stress_adc.get_value())) + 'N')
        
        if reach_max == True:
            print('Maximum voltage has reached! Finishing.')
            break
        if i > 3 and stress_adc.get_value() <= 1:
            regul_off(0, stress.position)
            print('Your sample is probably broken. Finishing.')
            break
        
    # load_ramp_by_target(0.5, time_step=0.5,to_file=f) 
    print('Finished')


def test_constant_ramping(save_path = save_path, loads = [30, 35, 40, 45, 50], voltage_ramprate = 0.1, voltage_target = 120, tol = 0.15):
    
    ACTIVE_MG.enable('stress*')
    print('Activated stress saving')
    stress_regul.plot()
    
    if voltage_target > 120:
        voltage_target = 120
        print('Maximum allowed voltage is 120')
        
    for i, load in enumerate(loads):
        initialize_stress_regul()
        print('Initialized stress regulation')
        
        print('Activating ramprate')
        regul_off(voltage_ramprate, voltage_target)
        print('Started voltage ramprate')
        
        file_name = 'load_ramp_up_' + str(int(load)) + 'N.txt'
        file_name = os.path.join(save_path, file_name)
        with open(file_name, 'w') as f:
            # ramping up the load
            t0 = time.time()
            dt = 0
            exit_flag = False
            while exit_flag == False:
                dt = time.time() - t0
                print('Waiting to reach the targe = {} N'.format(load))
                print('Current load is {} N; Ramping time is {} s'.format(stress_adc.get_value(), dt))
                print(_get_human_time(),stress.position,stress_adc.get_value(),file=f)
                if np.abs(stress_adc.get_value() - load) <= tol:
                    exit_flag = True
                    initialize_stress_regul()
                sleep(1)

        # start measurement at constant load
        newdataset('ff_' + str(int(load)) + 'N')
        ftimescan(0.1,100)
            
        if stress_adc.get_value() <= 1:
            regul_off(0, stress.position)
            print('Your sample is probably broken. Finishing.')
            break
        
    # load_ramp_by_target(0.5, time_step=0.5,to_file=f) 
    print('Finished')



def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def load_ramp_by_target(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    write_to_file = to_file if to_file is not None else sys.stdout
    #marana_large_beam(pct_pars)
    current_load = stress_adc.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_adc.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return
    
def load_and_unload(f_target):
    # f0 = stress_adc.get_value()
    f0 = 0.8
    if f_target > 1:
        load_ramp_by_target(f_target, time_step=0.5,to_file=None)
        load_ramp_by_target(f0, time_step=0.5,to_file=None)
    
def load_constant(f_target):
    if f_target > 1:
        load_ramp_by_target(f_target, time_step=0.5,to_file=None)

        
