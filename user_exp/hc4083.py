
def neg_pk( x, y ):
    pk = y.min()-y
    pk -= pk.min()/4
    m = pk>0
    return (x[m] * y[m]).sum()/y[m].sum()


def scan_hole( angle=30, rng=25., step = 0.25, expo=0.1):
    umv(rot,-angle)
    fscan(dty, -rng, step, int(2*rng/step), expo)
    x1 = fscan.get_data('dty')
    y1 = fscan.get_data("eiger:roi_counters:roi1_sum")
    #
    umv(rot,0)
    fscan(dty, -rng, step, int(2*rng/step), expo)
    x2 = fscan.get_data('dty')
    y2 = fscan.get_data("eiger:roi_counters:roi1_sum")
    #
    umv(rot,angle)
    fscan(dty, -rng, step, int(2*rng/step), expo)
    x3 = fscan.get_data('dty')
    y3 = fscan.get_data("eiger:roi_counters:roi1_sum")
    umv(rot,180)
    fscan(dty, -rng, step, int(2*rng/step), expo)
    x4 = fscan.get_data('dty')
    y4 = fscan.get_data("eiger:roi_counters:roi1_sum")
    umv(rot,0,dty,0)
    print(neg_pk( x1, y1 ))
    print(neg_pk( x2, y2 ))
    print(neg_pk( x3, y3 ))
    print(neg_pk( x4, y4 ))
