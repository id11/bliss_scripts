import numpy as np
import time
from bliss.common import cleanup
from datetime import datetime

def goto_vertical_beam():
    umv(vly,0.03,hlz,-0.326)
    umv(shnee,90.06,axmo,64.81)
    umv(s9vg,0.2,s9hg,0.05)
    
def goto_horizontal_beam():
    umv(vly,1.03,hlz,0.674)
    umv(shnee,89.06,axmo,64.81)
    umv(s9vg,0.05,s9hg,0.2)
    
def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
        
class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )
        
def half1(ymax,
          datasetname,
          ystep=0.2,
          ymin=-11,
          rstep=0.125,
          astart = -90,
          arange = 181,
          expotime=0.005):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)

def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")    

# file_name = '/data/visitor/ma5831/id11/20240227/PROCESSED_DATA/Sample4/temperature_log.dat'
# file_name = '/data/visitor/ma5831/id11/20240227/PROCESSED_DATA/Sample4/temperature_test.dat'
# file_name = '/data/visitor/ma5831/id11/20240227/PROCESSED_DATA/sample4_furnace/temperature_log.dat'
def heating_to_target(T_target, write_file = True, voltage_max = 28.0, step = 0.1, tol = 0.5, sleep_time = 3):
    exit_flag = False
    dt = 0
    t0 = time.time()
    if not write_file:
        while exit_flag == False:
            dt = time.time() - t0
            dt = dt/60.0
            print('*********Voltage = {} V, Setpoint of voltage = {} V'.format(hmc.voltage, hmc.voltage_setpoint))
            print('Current = {} mA, Setpoint of current = {} mA'.format(hmc.current, hmc.current_setpoint))
            print('Current temperature is {} C; Heating time is {} min'.format(nanodac1_temp.read(), dt))
            if nanodac1_temp.read() < T_target-tol:
                if hmc.voltage >= voltage_max-tol:
                    exit_flag = True
                    print('Voltage reached maximum!')
                else:
                    hmc.voltage_setpoint = hmc.voltage_setpoint + step
                sleep(sleep_time)
            else:
                exit_flag = True 
    else:
        with open(file_name, 'a') as f:
            while exit_flag == False:
                dt = time.time() - t0
                dt = dt/60.0
                print('*********Voltage = {} V, Setpoint of voltage = {} V'.format(hmc.voltage, hmc.voltage_setpoint))
                print('Current = {} mA, Setpoint of current = {} mA'.format(hmc.current, hmc.current_setpoint))
                print('Current temperature is {} C; Heating time is {} min'.format(nanodac1_temp.read(), dt))
                print(_get_human_time(),hmc.voltage,hmc.current,nanodac1_temp.read(),file=f)
                
                if nanodac1_temp.read() < T_target-tol:
                    if hmc.voltage >= voltage_max-tol:
                        exit_flag = True
                        print('Voltage reached maximum!')
                    else:
                        hmc.voltage_setpoint = hmc.voltage_setpoint + step
                    sleep(sleep_time)
                else:
                    exit_flag = True


def holding_temperature(T_target, hold_time = 5, write_file = True, voltage_max = 28.0, tol1 = 5, step1 = 0.1, tol2 = 2.5, step2 = 0.05,sleep_time = 3):
    exit_flag = False
    dt = 0
    t0 = time.time()
    if not write_file:
        while exit_flag == False:
            dt = time.time() - t0
            dt = dt/60.0
            print('*********Voltage = {} V, Setpoint of voltage = {} V'.format(hmc.voltage, hmc.voltage_setpoint))
            print('Current = {} mA, Setpoint of current = {} mA'.format(hmc.current, hmc.current_setpoint))
            print('Current temperature is {} C; Holding time is {} min'.format(nanodac1_temp.read(), dt))
            if nanodac1_temp.read() < T_target-tol1:
                if hmc.voltage >= voltage_max:
                    exit_flag = True
                    print('Voltage reached maximum!')
                else:
                    hmc.voltage_setpoint = hmc.voltage_setpoint + step1
            elif nanodac1_temp.read() < T_target-tol2:
                if hmc.voltage >= voltage_max:
                    exit_flag = True
                    print('Voltage reached maximum!')
                else:
                    hmc.voltage_setpoint = hmc.voltage_setpoint + step2
            elif nanodac1_temp.read() > T_target+tol1:
                if hmc.voltage >= voltage_max:
                    exit_flag = True
                    print('Voltage reached maximum!')
                else:
                    hmc.voltage_setpoint = hmc.voltage_setpoint - step1
            elif nanodac1_temp.read() > T_target+tol2:
                if hmc.voltage >= voltage_max:
                    exit_flag = True
                    print('Voltage reached maximum!')
                else:
                    hmc.voltage_setpoint = hmc.voltage_setpoint - step2          
            sleep(sleep_time)
            if dt >= hold_time:
                exit_flag = True
    else:
        with open(file_name, 'a') as f:
            while exit_flag == False:
                dt = time.time() - t0
                dt = dt/60.0
                print('*********Voltage = {} V, Setpoint of voltage = {} V'.format(hmc.voltage, hmc.voltage_setpoint))
                print('Current = {} mA, Setpoint of current = {} mA'.format(hmc.current, hmc.current_setpoint))
                print('Current temperature is {} C; Holding time is {} min'.format(nanodac1_temp.read(), dt))
                if nanodac1_temp.read() < T_target-tol1:
                    if hmc.voltage >= voltage_max:
                        exit_flag = True
                        print('Voltage reached maximum!')
                    else:
                        hmc.voltage_setpoint = hmc.voltage_setpoint + step1
                elif nanodac1_temp.read() < T_target-tol2:
                    if hmc.voltage >= voltage_max:
                        exit_flag = True
                        print('Voltage reached maximum!')
                    else:
                        hmc.voltage_setpoint = hmc.voltage_setpoint + step2
                elif nanodac1_temp.read() > T_target+tol1:
                    if hmc.voltage >= voltage_max:
                        exit_flag = True
                        print('Voltage reached maximum!')
                    else:
                        hmc.voltage_setpoint = hmc.voltage_setpoint - step1
                elif nanodac1_temp.read() > T_target+tol2:
                    if hmc.voltage >= voltage_max:
                        exit_flag = True
                        print('Voltage reached maximum!')
                    else:
                        hmc.voltage_setpoint = hmc.voltage_setpoint - step2          
                sleep(sleep_time)
                if dt >= hold_time:
                    exit_flag = True


def cooling_to_target(T_target = 18, write_file = True, voltage_max = 28.0, step = 0.2, tol = 0.05, sleep_time = 1):
    exit_flag = False
    dt = 0
    t0 = time.time()
    if not write_file:
        while exit_flag == False:
            dt = time.time() - t0
            dt = dt/60.0
            print('*********Voltage = {} V, Setpoint of voltage = {} V'.format(hmc.voltage, hmc.voltage_setpoint))
            print('Current = {} mA, Setpoint of current = {} mA'.format(hmc.current, hmc.current_setpoint))
            print('Current temperature is {} C; Cooling time is {} min'.format(nanodac1_temp.read(), dt))
            if nanodac1_temp.read() > T_target-tol:
                if hmc.voltage >= voltage_max-tol:
                    exit_flag = True
                    print('Voltage reached maximum!')
                else:
                    hmc.voltage_setpoint = hmc.voltage_setpoint - step
                sleep(sleep_time)
            else:
                exit_flag = True
    else:
        with open(file_name, 'a') as f:
            while exit_flag == False:
                dt = time.time() - t0
                dt = dt/60.0
                print('*********Voltage = {} V, Setpoint of voltage = {} V'.format(hmc.voltage, hmc.voltage_setpoint))
                print('Current = {} mA, Setpoint of current = {} mA'.format(hmc.current, hmc.current_setpoint))
                print('Current temperature is {} C; Cooling time is {} min'.format(nanodac1_temp.read(), dt))
                print(_get_human_time(),hmc.voltage,hmc.current,nanodac1_temp.read(),file=f)
                
                if nanodac1_temp.read() > T_target-tol:
                    if hmc.voltage >= voltage_max-tol:
                        exit_flag = True
                        print('Voltage reached maximum!')
                    else:
                        hmc.voltage_setpoint = hmc.voltage_setpoint - step
                    sleep(sleep_time)
                else:
                    exit_flag = True


def air_cooling(sleep_time = 5):
    hmc.voltage_setpoint = 0
    exit_flag = False
    dt = 0
    t0 = time.time()
    while exit_flag == False:
        dt = time.time() - t0
        dt = dt/60.0
        print('*********Voltage = {} V, Setpoint of voltage = {} V'.format(hmc.voltage, hmc.voltage_setpoint))
        print('Current = {} mA, Setpoint of current = {} mA'.format(hmc.current, hmc.current_setpoint))
        print('Current temperature is {} C; Cooling time is {} min'.format(nanodac1_temp.read(), dt))
        sleep(sleep_time)
        if dt >= 3:
            exit_flag = True
                
                

def get_current_pos():
    pos={}
    pos['ntx'] = ntx.position
    pos['nty'] = nty.position
    pos['ntz'] = ntz.position
    pos['px'] = px.position
    pos['py'] = py.position
    pos['pz'] = pz.position
    pos['rot'] = rot.position
    pos['dty'] = dty.position
    return pos


file_name = '/data/visitor/ma5831/id11/20240227/PROCESSED_DATA/sample4_furnace/temperature_log.dat'
def run_heating_holding_cooling(T_hot = 180, T_cool = 18, hold_time = 10):
    heating_to_target(T_hot, write_file = True)
    holding_temperature(T_target = T_hot, hold_time = hold_time, write_file = True) # holding time is in min
    #cooling_to_target(T_cool, write_file = True)
    air_cooling(sleep_time = 5)



def z_series(dset = 'z_series'):
    ymax = 1.8
    zpos = np.arange(20.0,26.0,0.2)
    
    for z in zpos:
        umv(pz,z)
        print(f"{dset}_{z:.4f}")
        half1(ymax, f"{dset}_{z:.4f}", ystep=0.2,rstep=0.125,astart = -90,arange = 181,expotime=0.005)

    print("Successfully done !!!")
    sheh3.close()








# in case you will have a lot of time until the next morning
def z_series_high_resolution(dset = 'z_series'):
    ymax = 1.8
    zpos = np.arange(34.2,37.7,0.1)
    for z in zpos:
        umv(pz,z)
        print(f"{dset}_{z:.4f}")
        half1(ymax, f"{dset}_{z:.4f}", ystep=0.1,rstep=0.125,astart = -90,arange = 181,expotime=0.005)

    print("Successfully done !!!")
    sheh3.close()
  
    

