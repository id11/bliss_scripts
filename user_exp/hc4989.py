import numpy as np
from scipy.optimize import curve_fit
       
def findside(x, y):
    y = y-y.min()
    #cm = (x*y).sum()/y.sum()
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    e1 = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    e2 = np.interp(0, -ny[np.argmax(y):], x[np.argmax(y):])
    return e1, e2
    
def linefit(x, a, b):
    return  np.abs((a*x) + b)   

def stepfit(x, cen, sig, am):
    x = x - cen
    a = np.exp(-np.power(x, 2.) / (2 * np.power(sig, 2.)))
    v = np.heaviside(x, 0.5)
    return am * np.convolve(a, v, mode='same')
    
def findwidth(x, y):
    p, _ = curve_fit(stepfit, x, y, (np.mean(x),0.02,max(y)))
    print(p)
    return p[1]
    
def oldalignry(raxis,rrange,rsteps,zrange,zsteps,ctim,ctr=eiger):
    rcen = raxis.position
    ws = []
    rvals = np.linspace(-rrange,rrange,rsteps)
    for r in rvals:
        umv(raxis, rcen+r)
        s = dscan(samtz,-0.8*zrange,zrange,zsteps,ctim,ctr)
        #fe = findside( s.get_data('samtz'), s.get_data('eiger:roi_counters:roi1_avg') )
        #ws.append(fe[1]-fe[0])
        fe = findwidth( s.get_data('samtz'), s.get_data('eiger:roi_counters:roi1_avg') )
        ws.append(fe)
    ws = np.array(ws)
    la = np.argmin(ws)
    if ws[la-1] < ws[la+1]:
        lal = la - 1  
        lar = la
    else:
        lar = la + 1
        lal = la
    ws[lar:] = -1*ws[lar:]
    p, _ = curve_fit(linefit, rvals, ws, (0,0))
    print(rvals)
    print(ws)
    print(p)
    rideal = -p[1]/p[0]
    print(rideal)
    if np.abs(rideal) < rrange:
        umv(raxis,rcen + rideal)
    return rcen + rideal
    
def alignry(rrange=4,rsteps=7,zrange=0.15,zsteps=100,ctim=0.1):
    raxis = samry
    rcen = raxis.position
    plotselect('eiger:roi_counters:roi1_avg')
    ed = []
    rvals = np.linspace(-rrange,rrange,rsteps)+rcen
    for r in rvals:
        umv(raxis, r)
        s = dscan(samtz,-0.8*zrange,zrange,zsteps,ctim)
        x, y = s.get_data('samtz'), s.get_data('roi1_avg')
        ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
        ny = ny - (ny.max()/2)
        edge = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
 
        ed.append(edge)
        print('samry values')
        print(rvals)
        print('edge position')
        print(ed)
    f = flint()
    p = f.get_plot("curve", name="align samry", unique_name="Asry")
    p.clear_data()
    p.add_curve( rvals, ed, legend='edge pos')
    print("Select position of miax")
    try:
        cs = p.select_points(1)
    except:
        print('Try again')
        cs = p.select_points(1)
    print(cs)
    umv(samry,cs[0][0])
    
def alignry_simple(r_start=-2, r_stop=2, rsteps=6, zrange=0.04, zsteps=100, ctim=0.1):
    plotselect('eiger:roi_counters:roi1_avg')
    rvals = np.linspace(r_start,r_stop,rsteps)
    for r in rvals:
        umv(samry, r)
        dscan(samtz,-0.8*zrange,zrange,zsteps,ctim)
    print('Choose the curve with the highest slope and set samrx to the corresponding value')

    
    
def zedge(ctim,rng,ctr=eiger):
    s = dscan(samtz,-rng,+rng,100,ctim)
    x, y = s.get_data('samtz'), s.get_data('roi1_avg')
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    edge = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    #edge = x[np.argmin(np.abs(y-( ( np.mean(y[25:20]) + np.mean(y[0:5]) )/2) ))]
    #umv(pz, edge)
    return edge
    
def oldalignrx(ctim=0.1,dran=2,zran=0.15):
    umvr(samty,-dran)
    c1 = zedge(ctim,zran)
    umvr(samty,2*dran)
    c2 = zedge(ctim,zran)
    srxoff = np.degrees(np.arctan((c1-c2)/(2*dran))) 
    print(c1,c2,srxoff)   
    umvr(samty,-dran)   
    umvr(samrx,srxoff)
    
def depth_profile(zdepth=0.1,zstep=0.001,diffrzstep=10,ctime=5,dset='depth_profile'):
    newdataset(dset)
    zstart = samtz.position
    zpos = np.arange(-0.01,zdepth,zstep)
    print(zpos+zstart)
    for i, z in enumerate(zpos):
        umv(samtz,zstart+z)
        if i%2 == 0:
            fscan(diffrz,-diffrzstep,diffrzstep,2,ctime)
        else:
            fscan(diffrz,diffrzstep,-diffrzstep,2,ctime)
    umv(samtz, zstart)

    
def scan_depth_and_move_Al(sidestep=0.3,zdepth=0.07,zstep=0.001,diffrzstep=20,ctime=5):
    samtystart = samty.position
    depth_profile(zdepth,zstep,diffrzstep,ctime,dset='depth_profile_1')
    umv(samty,samtystart+sidestep)
    depth_profile(zdepth,zstep,diffrzstep,ctime,dset='depth_profile_2')
    umv(samty,samtystart)
    
def scan_depth_and_move_Ni(sidestep=0.3,zdepth=0.05,zstep=0.001,diffrzstep=20,ctime=4):
    samtystart = samty.position
    depth_profile(zdepth,zstep,diffrzstep,ctime,dset='depth_profile_1')
    umv(samty,samtystart+sidestep)
    depth_profile(zdepth,zstep,diffrzstep,ctime,dset='depth_profile_2')
    umv(samty,samtystart+sidestep)
    depth_profile(zdepth,zstep,diffrzstep,ctime,dset='depth_profile_3')
    umv(samty,samtystart)
    
      
def scan_depth_and_move_Fe(sidestep=0.3,zdepth=0.05,zstep=0.001,diffrzstep=10,ctime=4):
    samtystart = samty.position
    depth_profile(zdepth,zstep,diffrzstep,ctime,dset='depth_profile_1')
    umv(samty,samtystart)
        
    
import time


def overnight():
    for zpos in [-0.002,-0.004,-0.030,-0.032,-0.034]:
        umv( samtz, zpos )
        newdataset('3dxrd_z%i'%(abs(zpos)*1000))
        layerscan(14.12, 14.55, 0.0013, 0.022, 0.022/11)


def pause_for_refill(t, cpos=6.3583, upos=8.3031):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    if abs(cpm18.position - cpos)>0.02 or abs(cpm18t.position - 0.075)>0.02:
        user.cpm18_goto(cpos)
    if abs(u22.position - upos)>0.02:
        umv(u22, upos)
                

def layerscan(y_start, y_end, y_step, rstep, a_time):
    print(np.arange(y_start,y_end,y_step))
    diffty_airpad.on(220)
    difftystart = diffty.position
    for i,d in enumerate(np.arange(y_start,y_end,y_step)):
        umv(diffty,  d )
        pause_for_refill(30)
        if i%2 == 0:
            myfscan(diffrz, 0, rstep, 180/rstep, a_time )
        else:
            myfscan(diffrz, 180, -rstep, 180/rstep, a_time )    
    umv(diffty,difftystart) 
    diffty_airpad.off()   

       
def myfscan(*a,**k):
    try:
        fscan(*a,**k)
    except:
        print("Something has gone wrong!!! Retry scans!!")
        elog_print("Retry a scan !!!")
        print("Sleep a minute or two first")
        time.sleep(60)
        pause_for_refill(120)
        fscan(*a,**k) 
        
def testscan():
    difftystart = diffty.position
    layerscan(0, 0.010, 0.0013, 0.022, 0.25/11)
    umv(diffty,difftystart)
    
    
    
