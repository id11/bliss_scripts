import os, sys
import numpy as np


import time
from bliss.common import cleanup


def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)




class scan_pars_id11_nscope:
    """ Puts the eiger in autosummation and disables mca if
    going faster than 5 milliseconds 
    """
    
    def __init__(self, expotime):
        self.expotime=expotime
        
    def __enter__(self):
        self.ctrs = set( ACTIVE_MG.enabled )
        if self.expotime < 0.005:
            ACTIVE_MG.disable('mca')
            print("Disabled mca: too fast scan")
            eiger.camera.auto_summation = 'OFF'

            
    def __exit__(self, *args, **kwds):
        # always put the eiger back
        eiger.camera.auto_summation = 'ON'
        # restore counters
        ctrs = set( ACTIVE_MG.enabled )
        if ctrs != self.ctrs:
            for c in self.ctrs:
                ACTIVE_MG.enable( c )



def half1(ymax,
          datasetname,
          ystep=2.0,
          ymin=-3000,
          rstep=0.05,
          astart = -180,
          arange = 361,
          expotime=0.002):
    """
    ymax = symmetric range for scanning dty -ymax -> ymax
    datasetname for new dataset (None if you keep old)
    ystep = step size for sinogram
    ymin = low y values to skip, for restarting
    rstep = rotation step size  
    astart = angle start position
    arange = angle range
    expotime = time for counting

    example:
       user.half1(1, 'toto', ystep=0.1)
    resume that from dty = 0.3:
       user.half1(1, None, ystep=0.1, ymin=0.3)

    """
    if datasetname is not None:
        newdataset(datasetname)
    angular_velocity = rstep / expotime
    if angular_velocity > 25:
        print("Scan is too fast, max rot speed is 25 deg/s")
        print("Try rstep=0.05, expotime=0.002 or rstep=0.125, expotime=0.005")
        return
    print("Rotation speed is",angular_velocity)
    # figure out steps
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    # ypositions to measure
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    #
    with scan_pars_id11_nscope(expotime):
        plotselect("eiger:roi_counters:roi1_max")
        with cleanup.cleanup( dty, restore_list=(cleanup.axis.POS,) ):
            while i < len(ypos)-1:
                i += 1        
                if ypos[i] < ymin: # skip positions already done
                    continue
                pause_for_refill( 25 )  # time for one scan
                if (i % 2) == 0: # forwards
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart,         rstep, arange/rstep, expotime )
                else:
                    fscan2d( dty, ypos[i], ystep, 1, rot, astart+arange, -rstep, arange/rstep, expotime )
    umv(dty, 0)



def move_lenses_in():
    umv(hlz, -0.331, vly, 0.047)
    if piny.position > 1.0:
        switch_pinhole()
    
def move_lenses_out():
    umv(hlz, -0.331+1, vly, 0.047-1)
    if piny.position < 1.0:
        switch_pinhole()

z0 = -1.60

def overnight_bigbeam():
    move_lenses_out()
    ntz_min = z0
    ntz_max = z0 + 3
    ntz_step = -0.05
    ntzs = np.arange(ntz_max, ntz_min, ntz_step)
    for i,ntz_pos in enumerate(ntzs):
        umv(ntz, ntz_pos)
        half1(ymax=2750, datasetname=f's3DXRD_bigbeam_z{i}', ystep=50.0, rstep=0.5, astart=-180, arange=361, expotime=0.02)
    print('Done')


def overnight_smallbeam():
    move_lenses_in()
    ntzs = [z0+0.485, z0+0.515]
    
    for i,ntz_pos in enumerate(ntzs):
        umv(ntz, ntz_pos)
        half1(ymax=1200, datasetname=f's3DXRD_smallbeam_z{i}', ystep=2.0, rstep=0.05, astart=-180, arange=361, expotime=0.002)
    print('Done')    
 







