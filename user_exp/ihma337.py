import numpy as np
from scipy.optimize import curve_fit


    
def gotoT(temp):
    vset = temptovolt(temp)
    print('Going to %f V for %f deg C'%(vset,temp))
    hmc.voltage_setpoint = vset
    hmc.output = 'ON'
    print('Heating to %s...'%str(temp))
    sleep(3)
    
def temptovolt(t):
    return 0.04913286596171511*t + 3.243259815561499
       
def findside(x, y):
    y = y-y.min()
    #cm = (x*y).sum()/y.sum()
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    e1 = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    e2 = np.interp(0, -ny[np.argmax(y):], x[np.argmax(y):])
    return e1, e2

def linefit(x, a, b):
    return  np.abs((a*x) + b)   
    
def align_r(raxis,rrange=0.8,rsteps=7,pzrange=25,pzsteps=50,ctim=0.05,ctr=mca):
    rcen = raxis.position
    if raxis == shrx:
        if np.abs((rot.position+90)%180)>1:
            umv(rot, 90)
    elif raxis == shry:
        if np.abs((rot.position)%180)>1:
            umv(rot, 0)
    ws = []
    rvals = np.linspace(-rrange,rrange,rsteps)
    for r in rvals:
        sync()
        umv(raxis, rcen+r)
        s = dscan(pz,-pzrange,pzrange,pzsteps,ctim,ctr)
        fe = findside( s.get_data('pz'), s.get_data('mca:all_det0') )
        ws.append(fe[1]-fe[0])
    p, _ = curve_fit(linefit, rvals[:2], ws[:2], (0,0))
    p, _ = curve_fit(linefit, rvals, ws, (p[0],-10))
    print(ws)
    rideal = -p[1]/p[0]
    if np.abs(rideal) < rrange:
        sync()
        umv(raxis,rcen + rideal)
    return rcen + rideal
    
def scanandcen(axis,rng,steps,ctim,ctr=mca):
    s = dscan(axis,-rng,rng,steps,ctim,ctr)
    x, y = s.get_data(axis.name), s.get_data('mca:all_det0')
    y = y-y.min()
    ny = np.clip(y, 0.1*y.max(), 0.9*y.max()) - (0.1*y.max()) 
    return (x*ny).sum()/ny.sum()

def roughzedge(ctim,rng,ctr=mca):
    s = dscan(shtz,-rng,+rng,20,ctim,ctr)
    x, y = s.get_data('shtz'), s.get_data('mca:all_det0')
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    edge = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    #edge = x[np.argmin(np.abs(y-( ( np.mean(y[25:20]) + np.mean(y[0:5]) )/2) ))]
    umv(shtz, edge)
    return edge

def pzedge(ctim,rng,npts=150,ctr=mca):
    s = ascan(pz,50-rng,50+rng,npts,ctim)
    x, y = s.get_data('pz'), s.get_data('mca:all_det0')
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    edge = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    #edge = x[np.argmin(np.abs(y-( ( np.mean(y[25:20]) + np.mean(y[0:5]) )/2) ))]
    #umv(pz, edge)
    return edge
    
def pzpeak(ctim,rng):
    s = ascan(pz,50-rng,50+rng,150,ctim)
    x, y = s.get_data('pz'), s.get_data('mca:all_det0')
    maxz = x[np.argmax(y)]
    umv(pz, maxz)
    return maxz
    
def pzcen(ctim,rng,npts=150):
    s = ascan(pz,50-rng,50+rng,npts,ctim)
    plotselect('mca:all_det0')
    cenz = cen()
    umv(pz, cenz)
    return cenz
    
def pzcom(ctim,rng):
    s = ascan(pz,50-rng,50+rng,150,ctim)
    comz = com()
    umv(pz, 50)
    return comz
    
def film_cor(rng, npts, ctim, ctr='eiger:roi_counters:roi1_sum'):
    cens = []
    plotselect(ctr)
    for j,k in enumerate([0,90,180,270]):
        umv(rot, k)
        if j%2 == 0:
            s = fscan(dty, -rng, rng*2/(npts-1), npts, ctim)
        else:
            s = fscan(dty, rng, -rng*2/(npts-1), npts, ctim)
        A = fscan.get_data()
        x, y = A['dty'], A[ctr]
        pk = np.clip(y-np.min(y), 0, 1e9)
        cens.append( (x*pk).sum()/pk.sum() )
        
    pyerr = (cens[0]-cens[2])/2
    pxerr = (cens[1]-cens[3])/2
    dtyideal = np.mean(cens)
    if (py.position+pyerr)>10 and (py.position+pyerr)<90:
        umvr(py, pyerr)
    else:
        umvr(shty, (pyerr/1000))
    if (px.position+pxerr)>10 and (px.position+pxerr)<90:
        umvr(px, pxerr)
    else:
        umvr(shtx, (pxerr/1000))
    umv(dty, dtyideal, rot, 0)
    
def very_old_align_film():
    plotselect('mca:all_det0')
    sync()
    ctim=0.05
    umv(rot,0)
    #roughzedge(0.05,0.1)
    shiftpzto50(0.05,30,npts=50)
    umvr(shtz,0.01)
    align_r(shry, rrange=0.8, rsteps=7, pzrange=35, pzsteps=70, ctim=0.05 )
    ed = pzedge(ctim,20)
    umv(pz,ed)
    
    umv(rot,90)
    shiftpzto50(ctim,30,npts=50)
    umvr(shtz,0.01)
    align_r(shrx, rrange=0.8, rsteps=6, pzrange=35, pzsteps=70, ctim=0.05 )
    ed = pzedge(ctim,20)
    umv(pz,ed)
    umv(rot,0)
    
    
def old_align_film(dran=300):
    plotselect('mca:all_det0')
    umv(rot, 0, shrx,0, shry,0)
    umv(pz,50)
    roughzedge(0.05,0.1)
    pzcen = pzedge(0.05,40)
    umv(pz,50)
    umvr(shtz, (pzcen-50)/1e3)
    
    umvr(shtz, 0.02)
    umv(dty,-dran)
    c1 = pzcom(0.05,48)
    umv(dty,dran)
    c2 = pzcom(0.05,48)
    srxoff = np.degrees(np.arctan((c1-c2)/(2*dran)))
    print(srxoff)   
    umv(dty,0)
    umvr(shrx,srxoff) 

    umv(rot, 90)
    umv(pz,50)
    pzcen = pzedge(0.05,30)
    umv(pz,50)
    umvr(shtz, (pzcen-50)/1e3)
    
    umv(dty,-dran)
    c1 = pzpeak(0.05,40)
    umv(dty,dran)
    c2 = pzpeak(0.05,40)
    sryoff = np.degrees(np.arctan((c1-c2)/(2*dran))) 
    print(sryoff)   
    umv(dty,0)  
    umvr(shry,-sryoff)   

  
def align_film(ctim=0.03,dran=200):
    plotselect('mca:all_det0')
    umv(rot, 0, shrx, 0, shry, 0)
    #roughzedge(0.05,0.15)
    shiftpzto50(ctim,30,npts=50)

    umv(dty,-dran)
    #ascan(pz,2,98,150,ctim)
    #c1 = com()
    #umv(dty,dran)
    #ascan(pz,98,2,150,ctim)
    #c2 = com()
    c1 = pzedge(ctim,20)
    umv(dty,dran)
    c2 = pzedge(ctim,20)
    srxoff = np.degrees(np.arctan((c1-c2)/(2*dran))) 
    print(c1,c2,srxoff)   
    umv(dty,0)  
    sync()
    umvr(shrx,srxoff)   

    umv(rot, 90)
    #roughzedge(0.05,0.15)
    shiftpzto50(ctim,30,npts=50)
    
    umv(dty,-dran)
    c1 = pzedge(ctim,20)
    umv(dty,dran)
    c2 = pzedge(ctim,20)
    sryoff = np.degrees(np.arctan((c1-c2)/(2*dran)))
    print(c1,c2,sryoff)   
    umv(dty,0)
    sync()
    umvr(shry,-sryoff) 
    
    sync()
    shiftpzto50(ctim,30,npts=50)
    ed = pzedge(ctim,15)
    umv(pz,ed)
    umv(rot,0)
    return srxoff, -sryoff
    

def ry_align_film(ctim=0.03,dran=200):
    plotselect('mca:all_det0')
    umv(rot, 90)
    #roughzedge(0.05,0.15)
    shiftpzto50(ctim,30,npts=50)
    
    umv(dty,-dran)
    c1 = pzedge(ctim,20)
    umv(dty,dran)
    c2 = pzedge(ctim,20)
    sryoff = np.degrees(np.arctan((c1-c2)/(2*dran)))
    print(c1,c2,sryoff)   
    umv(dty,0)
    sync()
    umvr(shry,-sryoff) 
    
    sync()
    shiftpzto50(ctim,30,npts=50)
    ed = pzedge(ctim,15)
    umv(pz,ed)
    umv(rot,0)
    return srxoff, -sryoff
  
  
def shiftpzto50(ctim, rng, npts=150):
    pzcen = pzedge(ctim, rng, npts)
    umv(pz,50)
    sync()
    umvr(shtz, (pzcen-50)/1e3)
    
def checkandscanZ():
    umv(rot,0,dty,0)
    roughzedge(0.05,0.1)
    pzcen = pzedge(0.05,20)
    umv(pz,50)
    umvr(shtz, (pzcen-50)/1e3)
    pzcen1 = pzedge(0.05,20)
    umv(pz, pzcen1)
    #umv(rot,90,dty,0)
    #roughzedge(0.05,0.1)
    #pzcen = pzedge(0.05,40)
    #umv(pz,50)
    #umvr(shtz, (pzcen-50)/1e3)
    #pzcen2 = pzedge(0.05,20)
    #umv(pz, np.max([pzcen1,pzcen2]))
    
def trackZ_at_temp(temp):
    gotoT(temp)
    umv(rot,0,dty,0)
    for i,ctim in enumerate([0.1,0.1,0.1,0.1,0.6]):
        print('Esitmated T = %i'%vitot(hmc.voltage,hmc.current))
        newdataset('align_%02d_%02d'%(temp,i))
        roughzedge(0.05,0.1)
        pzcen = pzedge(0.05,30)
        umv(pz,50)
        umvr(shtz, (pzcen-50)/1e3)
        pzedge(0.05,15)
        print('fscan_%02d_%02d'%(temp,i))
        print(ctim)
        newdataset('fscan_%02d_%02d'%(temp,i))
        fscan(rot,0,0.1,3600,ctim)

def trackZ_at_temp_continued(temp): 
    for i in range(5,15):
        print('Esitmated T = %i'%vitot(hmc.voltage,hmc.current))
        newdataset('align_%02d_%02d'%(temp,i))
        roughzedge(0.05,0.1)
        pzcen = pzedge(0.05,30)
        umv(pz,50)
        umvr(shtz, (pzcen-50)/1e3)
        pzedge(0.05,15)
        print('fscan_%02d_%02d'%(temp,i))
        newdataset('fscan_%02d_%02d'%(temp,i))
        fscan(rot,0,0.1,3600,0.1)
    
              
def trackZ_at_temp_overnight():
    temps = [350]
    try:
        for temp in temps:
            gotoT(temp)
            umv(rot,0,dty,0)
            for i in range(5):
                print(temp)
                print('Esitmated T = %i'%vitot(hmc.voltage,hmc.current))
                newdataset('align_%02d_%02d'%(temp,i))
                align_film()
                print('fscan_%02d_%02d'%(temp,i))
                newdataset('fscan_%02d_%02d'%(temp,i))
                fscan(rot,0,0.1,3600,0.1)
        hmc.output='OFF'
    except:
        hmc.output='OFF'
        
                
def tempramp():
    pzcen = pzedge(0.05,30)
    umv(pz,50)
    umvr(shtz, (pzcen-50)/1e3)
    pzpeak(0.05,15)
    newdataset('fscan_120_03')
    fscan(rot,0,0.1,3600,0.5)

    temps = np.arange(100,300,10)
    print(temps)
    try:
        for temp in temps:
            gotoT(temp)
            umv(rot,0,dty,0)
            for i,ctim in enumerate([0.1,0.5]):
                print(temp)
                newdataset('align_%02d_%02d'%(temp,i))
                if i == 0:
                    align_film()
                else:
                    pzcen = pzedge(0.05,30)
                    umv(pz,50)
                    umvr(shtz, (pzcen-50)/1e3)
                    pzpeak(0.05,15)
                print('fscan_%02d_%02d'%(temp,i))
                newdataset('fscan_%02d_%02d'%(temp,i))
                fscan(rot,0,0.1,3600,ctim)
        hmc.output='OFF'
    except:
        hmc.output='OFF'
        
  
def shiftpzto50cen(ctim, rng, npts=150):
    pzc = pzcen(ctim, rng, npts)
    umv(pz,50)
    sync()
    umvr(shtz, (pzc-50)/1e3)      

def uptoconstanttemp():
    temps = np.arange(100,470,20)
    temps = [20,*temps]
    print(temps)
    for temp in temps:
        print(temp)
        gotoT(temp)
        print('align_%02d'%temp)
        newdataset('align_%02d'%temp)
        umv(rot,0)
        shiftpzto50cen(0.05,30,120)
        sync()
        pzcen(0.05,10,100)
        
        print('fscan_%02d'%temp)
        newdataset('fscan_%02d'%temp)
        fscan(rot,0,0.1,3600,0.28)

    temp = 460
    for i in range(1,20):      
        print('align_%02d_%02d'%(temp,i))     
        newdataset('align_%02d_%02d'%(temp,i))
        umv(rot,0)
        shiftpzto50cen(0.05,30,120)
        sync()
        pzcen(0.05,10,100)
        
        print('fscan_%02d_%02d'%(temp,i))
        newdataset('fscan_%02d_%02d'%(temp,i))
        fscan(rot,0,0.1,3600,0.28)
        
    for i in np.arange(25,-1,0):
        hmc.voltage_setpoint = i
        print(i)
        sleep(15)
    hmc.output='OFF'
        
       
                       
def pristinescan():
    tfoh1.set(12, 'Be')
    newdataset('fscan_p2_00')
    fscan(rot,0,0.1,3600,0.02)
    newdataset('fscan_p2_01')
    fscan(rot,360,-0.1,3600,0.3)
    tfoh1.set(4, 'Be')


def temp_calib():        
    vs = list(range(16,21))
    print(vs)
    for v in vs:
        hmc.voltage_setpoint = v
        hmc.output = 'ON'
        umv(rot,0,dty,0)
        sleep(60)
        newdataset('%02dV'%(v))
        fscan(rot,0,180,2,20)
        
        
def lunchscan():
    #newdataset('fscan_flat')
    #fscan(rot,0,0.1,3600,0.3)
    
    newdataset('align_ry0p2')
    umv(rot,0)
    umvr(shry,-0.2)
    pzcen(0.05,40)
    
    newdataset('fscan_ry0p2')
    fscan(rot,0,0.1,3600,0.3)
    
    
def cooldown():
    vs = hmc.voltage_setpoint
    vs = np.arange(vs,0,int(vs)))
    print(vs)
    for v in vs:
        hmc.voltage_setpoint = v
        hmc.output = 'ON'
        sleep(20)
    hmc.output = 'OFF'

    
    
