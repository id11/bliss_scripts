  
import os, sys
import numpy as np

import time
from bliss.common import cleanup



def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)




def switch2_marana3():
    ff_out()
    assert ffdtx1.position > 500
    assert ffdtz1.position > 300
    umv(nfdtx,100)
    umv(d3ty,0,d3tz,0)
    umv(atty,-10,attrz,0)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    
    
def switch2_ff():
    marana3_out()
    assert d3ty.position > 140
    umvct(s7hg,0.2,s7vg,0.2,s8vg,0.25)
    umv(atty,0,attrz,-6)
    umv(ffdtz1,0)
    umv(ffdtx1,400)
    ACTIVE_MG.disable('marana3*')
    ACTIVE_MG.enable('frelon3*')
    

def ff_out():
    umv(ffdtx1,600)
    umv(ffdtz1,350)

def marana3_out():
    umv(nfdtx,100)
    umv(d3ty,150,d3tz,80)
    
    
def goto_position_model(pos_in_model):
    ref_pos_lab = [-3.89, -18.99, 5.10668]
    
    pos_in_model = np.array(pos_in_model)
    ref_pos_model = np.array([7.787, -11.028, 26.502]) # x is - lab Y, y is lab X, z is lab Z
    pos_diff = pos_in_model - ref_pos_model
    # convert to lab system at ID11
    umvr(samtx, -pos_diff[1])
    umvr(samty, pos_diff[0])
    umvr(samtz, pos_diff[2])
    


def area_scan(ystep = 0.15, zstep = 0.15):
    umvct(samtx, -3.89, samty, -18.99, samtz, 5.553)
    difftz_ref = difftz.position
    samty_ref = samty.position
    
    difftz_positions = np.arange(-8.45, 16.95, zstep)
    samty_positions = np.arange(-18.99, 25.3, ystep)
    count = 0
    for samty_pos in samty_positions:
        umv(samty, samty_pos)
        for difftz_pos in difftz_positions:
            count += 1
            umv(difftz, difftz_pos)
            umv(diffrz, 0)
            dset_name = 'point' + str(count) + '_0deg'
            newdataset(dset_name)
            finterlaced(diffrz, -10, 0.2, 101, 0.085)
            umv(diffrz, 180)
            dset_name = 'point' + str(count) + '_180deg'
            newdataset(dset_name)
            finterlaced(diffrz, 170, 0.2, 101, 0.085)
    print('Done successfully !') 
   


def vertical_line_scan(dset_prefix, zstep1 = 0.2, zrange1 = 7, zstep2 = 1, zrange2 = 13):
    """ go to the top edge"""
    difftz_ref = difftz.position
    samty_ref = samty.position
    difftz_positions1 = np.arange(difftz_ref, difftz_ref + zrange1, zstep1)
    difftz_positions2 = np.arange(difftz_ref + zrange1 + zstep1, difftz_ref + zrange1 + zrange2, zstep2)
    
    difftz_positions = np.hstack([difftz_positions1, difftz_positions2])
    npoints = difftz_positions.shape[0]
    count = 0
    print(difftz_positions)
    print(difftz_positions.shape)
    for difftz_pos in difftz_positions:
        count += 1
        print(f'Measuring point {count} / {npoints} at 0 deg')
        umv(difftz, difftz_pos)
        umv(diffrz, -5)
        dset_name = dset_prefix + '_p' + str(count) + '_0deg'
        newdataset(dset_name)
        finterlaced(diffrz, -5, 0.25, 41, 0.085)
    
    count = 0
    for difftz_pos in difftz_positions:
        count += 1
        print(f'Measuring point {count} / {npoints} at 180 deg')
        umv(difftz, difftz_pos)
        umv(diffrz, 175)
        dset_name = dset_prefix + '_p' + str(count) + '_180deg'
        newdataset(dset_name)
        finterlaced(diffrz, 175, 0.25, 41, 0.085)
    umv(difftz, difftz_ref)
    print('Done successfully !')



def area_scan(yrange = 27.0, ystep = 1.5):
    difftz_ref = difftz.position
    samty_ref = samty.position
    
    samty_positions = np.arange(samty_ref, samty_ref + yrange + ystep/2, ystep)
    print(samty_positions)
    print(samty_positions.shape)
    ny = samty_positions.shape[0]
    count = 0
    for samty_pos in samty_positions:
        print(f'Measuring line {count} / {ny}')
        umv(samty, samty_pos)
        count += 1
        dset_prefix = 'area_l' + str(count)
        vertical_line_scan(dset_prefix)
    print('Done successfully !')

 
