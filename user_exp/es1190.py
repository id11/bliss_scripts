
import time, numpy as np


# user_script_load("es1190.py")
user_script_load('optics.py')

def goto_slits(h,v):
    umv(s7hg,h,s7vg,v,s8hg,h+0.2,s8vg,v+0.2)


def goto_pct():
    # move out the far field detector
    umv(ffdtx1, 400, ffdtz1, 200)
    # move in the PCT detector
    umv(d3ty,0.13203)
    # switch the counter groups
    ACTIVE_MG.disable("frelon3")
    ACTIVE_MG.enable("marana")

    
def goto_diffraction():
    # move out the PCT detector
    umv(d3ty, 190)
    # move in the far field detector
    umv(ffdtx1, 200, ffdtz1, 0)
    # switch the counter groups
    ACTIVE_MG.enable("frelon3")
    ACTIVE_MG.disable("marana")
    

# def check_cpm18(pos=6.4158):
def check_cpm18(pos=10.2):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()


    
def difftomo360(dsname):
    goto_slits(0.05,0.05)
    d0 = 14.705
#    umv(samtz,6.68)
    newdataset(dsname)
    for ty in np.arange(-2.7, 2.701, 0.05):
        umv(diffty, d0+ty)
        pause_for_refill(80)
        finterlaced( diffrz, 0, 0.8, 450, 0.08, mode='ZIGZAG' )
    umv(diffty, d0 )
    print("all done")



def SI3():
    # done 6.68
    zp = [ 7.28, 3.68, 6.08, 4.28, 5.48, 4.88 ]
    for z in zp:
        umv(samtz, z)
        difftomo360( 'DT360Z%d'%(int(z*1000)) )
          


def realign_on_goldwire(z0=4.9):
    # top wire 4.58
    # bott 5.72
    #  ... 4.425  ... to  5.375
    goto_slits( 0.5, 0.05 )
    umv(diffrz, 90)
    dz = 1.14/2
    dz = 0.95/2
    zr = 0.2
    umv(samtz, z0)
    dscan(samtz, dz-zr, dz+zr, 80, 0.1 )
    c3b = cen(frelon3.counters.roi3_sum)
    c2b = cen(frelon3.counters.roi2_sum)
    dscan(samtz, -dz-zr, -dz+zr, 80, 0.1 )
    c3t = cen(frelon3.counters.roi3_sum)
    c2t = cen(frelon3.counters.roi2_sum)
    avg = np.mean((c3b,c2b,c3t,c2t))
    length = np.mean((c3b,c2b))-np.mean((c3t,c2t))
    print("positions",(c3b,c2b,c3t,c2t))
    elog_print("positions",(c3b,c2b,c3t,c2t))
    print("avg",avg,"length",length)
    elog_print("avg",avg,"length",length)
    return avg



def admetlayer():
    # around 6 minutes
    finterlaced( diffrz, -39.5, -0.1, 1044, 0.08 )
    finterlaced( diffrz, 35.3, 0.1, 1044, 0.08 )

def admetmeasure(name):
    newdataset(name)
    z0 = realign_on_goldwire( samtz.position )
    umv(samtz, z0)
    goto_slits(1.8,0.1)
    for dz in ( -0.2, 0.1, 0, 0.1, 0.2):
        umv( samtz, z0+dz )
        admetlayer()
    umv(samtz,z0)



def half1(ymax, datasetname, ystep=5, ymin=-1801):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.05, 3620, 0.002 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 181, -0.05, 3620, 0.002 )



def olivine_nscope():
    umv(pz,99)
    half1( 650, "DT1umZ99", ystep=1)
    umv(pz,79)
    half1( 650, "DT1umZ79", ystep=1)
    umv(pz,59)
    half1( 650, "DT1umZ59", ystep=1)
    
    umv( dty, 0)

def olivine_pristine_nscope():
    umv(pz,10)
    half1( 400, "DT1umZ10", ystep=1)
    umv(pz,90)
    half1( 680, "DT1umZ90", ystep=1)
    umv(pz,50)
    half1( 550, "DT1umZ50", ystep=1)
    umv( dty, 0)
