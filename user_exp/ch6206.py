
import time
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)


def gototemperatures(temp):
    
    ox800.setpoint = temp 
    ox800.ramprate=360
    newdataset(f'{temp}K')
    while(ox800.is_ramping() ):
        print('I am ramping at', ox800_axis.position, 'K \n'), sleep(2)
    print ('stabilizing..')
    sleep(180)
    pause_for_refill(60*3)
    fscan2d(samtz, 10.65, .3, 9, diffrz, 0, 2.5, 144, .2)
    fscan2d(samtz, 10.65, .3, 9, diffrz, 0, 2.5, 144, .2)     

def listtemperatures():
    for temp in (90, 100, 115, 140, 160, 180, 200, 220, 240, 260, 280, 300, 325, 350):
        gototemperatures(temp)
