import sys
import numpy as np
import time
import os
from datetime import datetime

    
ly = config.get('ly')
lfyaw = config.get('lfyaw')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')


dct_pars={}
dct_pars['execute'] = 1
dct_pars['start_pos'] = 0
dct_pars['step_size'] = 0.1
dct_pars['num_proj'] = 360 / dct_pars['step_size']
dct_pars['exp_time'] = 0.1
dct_pars['refon'] = dct_pars['num_proj']
dct_pars['ref_step'] = -3.5
dct_pars['scan_mode'] = 'CAMERA'
dct_pars['nref'] = 41
dct_pars['slit_hg'] = 1
dct_pars['slit_vg'] = 0.10
dct_pars['ref_mot'] = samy

ff_pars={}
ff_pars['execute'] = 1
ff_pars['step_size'] = 0.25
ff_pars['exp_time'] = 0.08
ff_pars['slit_hg'] = 1
ff_pars['slit_vg'] = 0.25


# attenuation moves below:

def lyin():
    umv(ly,-10,lfyaw,20)

def lyout():
    umv(ly, 0, lfyaw, 0.1)

def pause_for_refill(t, num=1e4):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t, num)

def pause_for_flux(t, num=1e4):
    c = ct(1,pico4).get_data()['pico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,pico4).get_data()['pico4']

def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")


def ff_dict(scan_name):
    newdataset(scan_name)
    num_proj = 408
    finterlaced(diffrz, -45, -0.25, num_proj, 0.08, mode="ZIGZAG")
    finterlaced(diffrz, 33, 0.25, num_proj, 0.08, mode="ZIGZAG")


def zseries_ff_dict(cen_pos, shift_step_size, nof_shifts, scan_name):
    shift_step_size = abs(shift_step_size)
    if(nof_shifts < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_pos = cen_pos - (nof_shifts - 1) * shift_step_size / 2
    print('Central position:' + str(cen_pos))
    if(offset_pos < samtz.low_limit or offset_pos + (nof_shifts - 1) * shift_step_size > samtz.high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(nof_shifts)):
        umv(samtz, iter_i * shift_step_size + offset_pos)
        scanname = scan_name + "_" + str(iter_i + 1)
        ff_dict(scanname)
    umv(samtz, cen_pos)
    print('FF Succeed')
    return('Succeed')

