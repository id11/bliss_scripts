
import numpy as np
import time
from scipy.io import loadmat

ly = config.get('ly')
lfyaw = config.get('lfyaw')
tfoh1 = config.get('tfoh1')
tfz = config.get('tfz')
tfy = config.get('tfy')
bigy = config.get('bigy')
u22 = config.get('u22')


# Detector positioning : becomes d1.IN, d1.OUT, d2, d3, d4
d1 = config.get('d1')
d2 = config.get('d2')
d3 = config.get('d3')
d4 = config.get('d4')

dct_dist = 9
pct_dist = 50
ff_offset = 350
tt_offset = 0
tt_dist = 10
ff_z = 100
ff_dist = 200
d1_out = -176
d2_out = -388
d2_in = 0
d3_out = 200
d3tz_pos_dct = 0
d3tz_pos_tt = 1.8

# Sample positionion
diffty_offset = 13.672
samtx_offset = -0.6394
samty_offset = -1.8589
samtz_offset = 0.
samrx_offset = -0.3
samry_offset = 0.1

# Optics / beam conditioning
# 55 keV 
cpm18_tuned = 7.7508
u22_tuned = 7.6693
bigy_in = -0.233
bigy_out = 22

def read_tt_infos(gid):
    data_dir = '/data/visitor/ma5179/id11/DCT_Analysis/NiTiNanox/NiTiNanox_DCT_1N'
    data_path = os.path.join(data_dir, 'grain_%04d.mat' % gid)
    data = loadmat(data_path)
    t = data['out'][0][0][0][0]
    values = t[0][0][0][0][0]
    d = {}
    d['gr_id'] = int(values[0][0][0])
    # double check that the grain id is right
    if d['gr_id'] != gid:
        raise(ValueError('grain id mismatch, check your matlab output in file %s' % data_path))
    d['nfdtx'] = float(values[1][0][0])
    d['d2tz'] = float(values[2][0][0])  # this needs automated detector selection !
    d['diffry'] = float(values[3][0][0])
    d['samrx'] = float(values[4][0][0])
    d['samry'] = float(values[5][0][0])
    d['samtx'] = float(values[6][0][0])
    d['samty'] = float(values[7][0][0])
    d['samtz'] = float(values[8][0][0])
    d['samrx_offset'] = float(values[9][0][0])
    d['samry_offset'] = float(values[10][0][0])
    d['samtx_offset'] = float(values[11][0][0])
    d['samty_offset'] = float(values[12][0][0])
    d['samtz_offset'] = float(values[13][0][0])
    d['diffrz_offset'] = float(values[14][0][0])
    d['int_factor'] = float(values[15][0][0])
    print(d)
    return d

def sam_dct_pos():
    """ sample position to run a DCT scan """ 
    umv(samrx, samrx_offset, samry, samry_offset, diffry, 0)
    umv(samtx, samtx_offset, samty, samty_offset, samtz, samtz_offset)

def lyin():
    ''' attenuator in '''
    umv(ly,-6)

def lyout():
    ''' attenuator out '''
    umv(ly, 2)

def flatbeam():
    # goto big beam
    tfoh1.set(0,'Be')  
    tfoh1.set(0,'Al')  
    umv(bigy, bigy_out)

   
def ffin():
    """ ff is d4  """
    sheh3.close()   # not to draw a line on the detector
    ACTIVE_MG.disable("marana*","frelon16*","frelon1*")
    ACTIVE_MG.enable("frelon3")
    # check others are out
    umv(d3ty, d3_out, d2ty, d2_out)
    umv(nfdtx, tt_dist)
    assert d1.position in ('OUT', 'PARK')
    assert d2.position in ('OUT', 'PARK') 
    assert d3.position in ('OUT', 'PARK') 
    # ff is d4    assert d4.position in 'OUT', 'PARK' 
    d4.IN()
    # Add an attenuator
    lyin()
    flatbeam()
    # sample position
#    sam_dct_pos()
    print("ready to collect far-field data")
    sheh3.open()



def ff_z_series():
    sheh3.open()
    umv(s7vg, 0.7, s7hg, 1, s8vg , 1.5, s8hg, 1.5)
    z0 = -1.78
    for tz in (-0.2,0.5):
        umv(samtz, tz+z0)
        finterlaced( diffrz, 0, 1, 180, 0.08 )
    umv(samtz, z0)


def maranain(dist):
    """ marana is d3 """
    sheh3.close()
    ACTIVE_MG.disable("frelon16*","frelon1*", "frelon3*" )
    ACTIVE_MG.enable("marana")
    assert d1.position in ('OUT', 'PARK') 
    assert d2.position in ('OUT', 'PARK') 
    #    assert d3.position in 'OUT', 'PARK' 
    assert d4.position in 'OUT', 'PARK' 
    d3.IN()
    lyout()
    #sam_dct_pos()
    umv( nfdtx, dist)
    print("ready to collect marana data") 
    sheh3.open()
    ct(0.02)
    
def d2in(theta, distance):
    # tfoh1.set(0,'Al')
    # tfoh1.set(0,'Be')
    #for i in range (3):
    #    umv(tfz, -0.39, tfy, 14.51)
    lyout()
    ACTIVE_MG.disable("marana","frelon3")
    ACTIVE_MG.enable("frelon16")
    frelon16.image.flip=[False, True]

    assert d1.position in 'OUT', 'PARK' 
    # assert d2.position in 'OUT', 'PARK' 
    assert d3.position in 'OUT', 'PARK' 
    assert d4.position in 'OUT', 'PARK' 

    d2tzpos = np.tan(2*np.deg2rad(theta))*distance
    umvct(d2tz, d2tzpos)
    umvct(nfdtx, distance )
    print("moving d2tz to target position %g"%d2tzpos)
    umv(d2ty, 0)
    ct(0.1)
    print("ready to collect topotomo data")    
    
def ref_scan(ref_mot, exp_time, nref, ref_step, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)


def CuAlNi_DCT0N():
    dct_pars={}
    dct_pars['start_pos'] = -14
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.05
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -3
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.9
    dct_pars['slit_vg'] = 0.7
    dct_pars['dist'] = 10
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 0.0
    dct_pars['shift_step_size'] = 0.28   # not used?
    dct_pars['nof_shifts'] = 5           # not used
    tomo_by_fscan_dict('DCT_0N_II', dct_pars)
    
def CuAlNi_DCT(name):
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.05
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -4
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.8
    dct_pars['slit_vg'] = 0.8
    dct_pars['dist'] = 9
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 0.0
    dct_pars['shift_step_size'] = 0.28   # not used?
    dct_pars['nof_shifts'] = 5           # not used
    tomo_by_fscan_dict(name, dct_pars)

def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        #finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])



        
def tt_tilt_check_ffbase(yrange=1.5, ystep=0.01, ctr='frelon16:roi_counters:roi1_avg'):
    r0 = diffry.position
    cens = []
    plotselect(ctr)
    angs = (0,90,180,270)
    for rz in angs:
        umv(diffrz, rz)
        fscan( diffry, r0-yrange, ystep, yrange*2/ystep, 0.08, scan_mode='TIME' )
        cens.append(cen())
    umv(diffry, r0)
    
    samrx_offset = 0.5 * (cens[1] - cens[3])
    samry_offset = 0.5 * (cens[0] - cens[2])
    average = np.mean(cens)
    for a,p in zip(angs, cens):
        print("diffrz",a,p)
    print("umvr(samrx,",samrx_offset,",samry,",samry_offset,")")
    print("center in diffry",average)
    

####
# SAMPLE 3
def allGrains_1N():
    # Grain 1
    # samrx      samry      samtx      samty     samtz
    # -8.1241   5.1956   0.00000    0.00000    0.7000
    # diffry -5.3841,
    user.d2in(5.4,9)
    newdataset('tt_g1_224_1N')
    y0 = -5.3841
    umv(diffry,y0,samrx,-8.1241,samry,5.1956)
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
             
    # Grain 2
    # samrx      samry      samtx      samty     samtz
    # 7.4283    5.5647   0.00000    0.00000    0.7000
    # diffry -5.3873,
    user.d2in(5.4,9)
    newdataset('tt_g2_224_1N')
    y0 = -5.3873
    umv(diffry,y0,samrx,7.4283,samry,5.5647)
    rng = 0.3
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0)
             
    # Grain 0
    # samrx      samry      samtx      samty     samtz
    # -3.4057   -7.6310    0.00000    0.00000    0.7000
    # diffry -6.9564,
    user.d2in(6.99,9)
    newdataset('tt_g0_206_1N')
    y0 = -6.9564
    umv(diffry,y0,samrx,-3.4057,samry,-7.6310)
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)

def allGrains_6N():
    # Grain 1
    # samrx      samry      samtx      samty     samtz
    # -8.36492   5.59963   0.00000    0.00000    0.7000
    # diffry -5.3820,
    user.d2in(5.4,9)
    newdataset('tt_g1_224_6N')
    y0 = -5.3820
    umv(diffry,y0,samrx,-8.36492,samry,5.59963)
    rng = 1.0
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0)
             
    # Grain 2
    # samrx      samry      samtx      samty     samtz
    # 6.89744    6.08061   0.00000    0.00000    0.7000
    # diffry -5.3806,
    user.d2in(5.4,9)
    newdataset('tt_g2_224_6N')
    y0 = -5.3806
    umv(diffry,y0,samrx,6.89744,samry,6.08061)
    rng = 0.6
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             1.5)
             
    # Grain 0
    # samrx      samry      samtx      samty     samtz
    # -3.65840   -7.25369    0.00000    0.00000    0.7000
    # diffry -6.9512,
    user.d2in(6.99,9)
    newdataset('tt_g0_206_6N')
    y0 = -6.9512
    umv(diffry,y0,samrx,-3.65840,samry,-7.25369)
    rng = 0.8
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0)   

def allGrains_1cycle_1N():
    # Grain 1
    # samrx      samry      samtx      samty     samtz
    # -8.39359   5.51772   0.00000    0.00000    0.7000
    # diffry -5.3831,
    user.d2in(5.4,9)
    newdataset('tt_g1_224_1N_unload')
    y0 = -5.3831
    umv(diffry,y0,samrx,-8.39359,samry,5.51772)
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
             
    # Grain 2
    # samrx      samry      samtx      samty     samtz
    # 7.15390    5.95827   0.00000    0.00000    0.7000
    # diffry -5.3864,
    user.d2in(5.4,9)
    newdataset('tt_g2_224_1N_unload')
    y0 = -5.3864
    umv(diffry,y0,samrx,7.15390,samry,5.95827)
    rng = 0.3
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0)
             
    # Grain 0
    # samrx      samry      samtx      samty     samtz
    # -3.61848   -7.29104    0.00000    0.00000    0.7000
    # diffry -6.9561,
    user.d2in(6.99,9)
    newdataset('tt_g0_206_1N_unload')
    y0 = -6.9561
    umv(diffry,y0,samrx,-3.61848,samry,-7.29104)
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)


def allGrains_cycle2_5N():
    # Grain 1
    # samrx      samry      samtx      samty     samtz
    # -8.3707   5.6021   0.00000    0.00000    0.7000
    # diffry -5.3829,
    user.d2in(5.4,9)
    newdataset('tt_g1_224_cycle2_5N')
    y0 = -5.3829
    umv(diffry,y0,samrx,-8.3707,samry,5.6021)
    rng = 0.8
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
             
    # Grain 2
    # samrx      samry      samtx      samty     samtz
    # 6.9956    6.12088   0.00000    0.00000    0.7000
    # diffry -5.4020,
    user.d2in(5.4,9)
    newdataset('tt_g2_224_cycle2_5N')
    y0 = -5.4020
    umv(diffry,y0,samrx,6.9956,samry,6.12088)
    rng = 1.6
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0)
             
    # Grain 0
    # samrx      samry      samtx      samty     samtz
    # -3.6486   -7.2545    0.00000    0.00000    0.7000
    # diffry -6.9526,
    user.d2in(6.99,9)
    newdataset('tt_g0_206_cycle2_5N')
    y0 = -6.9526
    umv(diffry,y0,samrx,-3.6486,samry,-7.2545)
    rng = 1.8
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.6)


def allGrains_unload2_1N():
    # Grain 1
    # samrx      samry      samtx      samty     samtz
    # -8.3888   5.5320   0.00000    0.00000    0.7000
    # diffry -5.3829,
    user.d2in(5.4,9)
    newdataset('tt_g1_224_unload2_1N')
    y0 = -5.3829
    umv(diffry,y0,samrx,-8.3888,samry,5.5320)
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
             
    # Grain 2
    # samrx      samry      samtx      samty     samtz
    # 7.1650    5.9861   0.00000    0.00000    0.7000
    # diffry -5.3825,
    user.d2in(5.4,9)
    newdataset('tt_g2_224_unload2_1N')
    y0 = -5.3825
    umv(diffry,y0,samrx,7.1650,samry,5.9861)
    rng = 0.3
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.6)
             
    # Grain 0
    # samrx      samry      samtx      samty     samtz
    # -3.5973   -7.2639    0.00000    0.00000    0.7000
    # diffry -6.9567,
    user.d2in(6.99,9)
    newdataset('tt_g0_206_unload2_1N')
    y0 = -6.9567
    umv(diffry,y0,samrx,-3.5973,samry,-7.2639)
    rng = 0.48
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)

def allGrains_cycle3_5N():
    # Grain 1
    # samrx      samry      samtx      samty     samtz
    # -8.3600   5.6123   0.00000    0.00000    0.7000
    # diffry -5.3825,
    user.d2in(5.4,9)
    newdataset('tt_g1_224_cycle3_5N')
    y0 = -5.3825
    umv(diffry,y0,samrx,-8.3600,samry,5.6123)
    rng = 0.8
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
             
    # Grain 2
    # samrx      samry      samtx      samty     samtz
    # 7.0102    6.1579   0.00000    0.00000    0.7000
    # diffry -5.3922,
    user.d2in(5.4,9)
    newdataset('tt_g2_224_cycle3_5N')
    y0 = -5.3922
    umv(diffry,y0,samrx,7.0102,samry,6.1579)
    rng = 1.6
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0)
             
    # Grain 0
    # samrx      samry      samtx      samty     samtz
    # -3.64203   -7.23774    0.00000    0.00000    0.7000
    # diffry -6.9512,
    user.d2in(6.99,9)
    newdataset('tt_g0_206_cycle3_5N')
    y0 = -6.9512
    umv(diffry,y0,samrx,-3.64203,samry,-7.23774)
    rng = 1.8
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.6)

def allGrains_unload3_1N():
    # Grain 1
    # samrx      samry      samtx      samty     samtz
    # -8.3742   5.5019   0.00000    0.00000    0.7000
    # diffry -5.3825,
    user.d2in(5.4,9)
    newdataset('tt_g1_224_unload3_1N')
    y0 = -5.3825
    umv(diffry,y0,samrx,-8.3742,samry,5.5019)
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
             
    # Grain 2
    # samrx      samry      samtx      samty     samtz
    # 7.22218    5.99431   0.00000    0.00000    0.7000
    # diffry -5.3838,
    user.d2in(5.4,9)
    newdataset('tt_g2_224_unload3_1N')
    y0 = -5.3838
    umv(diffry,y0,samrx,7.22218,samry,5.99431)
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.6)
             
    # Grain 0
    # samrx      samry      samtx      samty     samtz
    # -3.5994   -7.2969    0.00000    0.00000    0.7000
    # diffry -6.9559,
    user.d2in(6.99,9)
    newdataset('tt_g0_206_unload3_1N')
    y0 = -6.9559
    umv(diffry,y0,samrx,-3.5994,samry,-7.2969)
    rng = 0.48
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)

def allGrains_cycle4_5N():
    # Grain 0
    # samrx      samry      samtx      samty     samtz
    # -3.69641   -7.32541    0.00000    0.00000    0.7000
    # diffry -6.9509,
    user.d2in(6.99,9)
    newdataset('tt_g0_206_cycle4_5N')
    y0 = -6.9509
    umv(diffry,y0,samrx,-3.69641,samry,-7.32541)
    rng = 1.2
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.6) 
    # Grain 1
    # samrx      samry      samtx      samty     samtz
    # -8.4288   5.5283   0.00000    0.00000    0.7000
    # diffry -5.3825,
    user.d2in(5.4,9)
    newdataset('tt_g1_224_cycle4_5N')
    y0 = -5.3825
    umv(diffry,y0,samrx,-8.4288,samry,5.5283)
    rng = 0.8
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
             
    # Grain 2
    # samrx      samry      samtx      samty     samtz
    # 7.0102    6.1579   0.00000    0.00000    0.7000
    # diffry -5.3875,
    user.d2in(5.4,9)
    newdataset('tt_g2_224_cycle4_5N')
    y0 = -5.3875
    umv(diffry,y0,samrx,7.0102,samry,6.1579)
    rng = 1.4
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0)
             
        
####
# SAMPLE 5

def S5_g0_202_1N():
    # samrx      samry      samtx      samty     samtz
    # 3.4677  -2.22654  -0.45936   -1.68886  -0.27000
    # diffry -3.104614,
    newdataset('tt_g1_202_1N')
    y0 = -3.104614
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 2, 180,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
             
def S5_g0_202_1N_ht():
    # samrx      samry      samtx      samty     samtz
    # 2.66338  -2.23279  -0.78936   -1.65886  -0.27000
    # diffry -3.105545,
    newdataset('tt_g1_202_1N')
    y0 = -3.105545
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 2, 180,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)

def S5_g0_202_load1_10N():
    # samrx      samry      samtx      samty     samtz
    # 2.16065  -1.95425  -0.78936   -1.65886  -0.27000
    # diffry -3.1041,
    newdataset('tt_g1_202_load1_10N')
    y0 = -3.1041
    rng = 1.4
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
                   
def S5_g2_224_1N():
    # samrx      samry      samtx      samty     samtz
    # -6.4129  -7.20469  -0.78936   -1.65886  -0.27000
    # diffry -5.3845,
    newdataset('tt_g2_224_1N')
    y0 = -5.3845
    rng = 0.6
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)

def S5_g2_224_load1_10N():
    # samrx      samry      samtx      samty     samtz
    # -7.16737  -6.85620  -0.78936   -1.65886  -0.27000
    # diffry -5.3845,
    newdataset('tt_g2_224_load1_10N')
    y0 = -5.3845
    rng = 1.8
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
                          
def BothGrains_S5_unload_1N():
    # Grain 0/1
    # samrx      samry      samtx      samty     samtz
    # 2.59490  -2.18892  -0.78936   -1.65886  -0.27000
    # diffry -3.1049,
    user.d2in(3.1,9)
    newdataset('tt_g1_202_unload1_1N')
    y0 = -3.1049
    umv(diffry,y0,samrx,2.59490,samry,-2.18892)
    rng = 0.6
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
             
    # Grain 2
    # samrx      samry      samtx      samty     samtz
    # -6.49157  -7.18340  -0.78936   -1.65886  -0.27000
    # diffry -5.3847,
    user.d2in(5.4,9)
    newdataset('tt_g2_224_unload1_1N')
    y0 = -5.3847
    umv(diffry,y0,samrx,-6.49157,samry,-7.18340)
    rng = 0.6
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
             
def BothGrains_S5_load2_10N():
    # Grain 0/1
    # samrx      samry      samtx      samty     samtz
    # 2.24154  -1.99614  -0.78936   -1.65886  -0.27000
    # diffry -3.1044,
    user.d2in(3.1,9)
    newdataset('tt_g1_202_load2_10N')
    y0 = -3.1044
    umv(diffry,y0,samrx,2.24154,samry,-1.99614)
    rng = 1.4
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0)
             
    # Grain 2
    # samrx      samry      samtx      samty     samtz
    # -7.07173  -6.87004  -0.78934   -1.65886  -0.27000
    # diffry -5.3846,
    user.d2in(5.4,9)
    newdataset('tt_g2_224_load2_10N')
    y0 = -5.3846
    umv(diffry,y0,samrx,-7.07173,samry,-6.87004)
    rng = 1.8
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0)

def S5_g0_202_unload2_1N():
    # samrx      samry      samtx      samty     samtz
    # 2.60279  -2.20404  -0.78936   -1.65886  -0.27000
    # diffry -3.1055,
    newdataset('tt_g1_202_unload2_1N')
    y0 = -3.1055
    rng = 0.6
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)

def S5_g2_224_unload2_1N():
    # samrx      samry      samtx      samty     samtz
    # -6.50635  -7.17053  -0.78936   -1.65886  -0.27000
    # diffry -5.3850,
    newdataset('tt_g2_224_unload2_1N')
    y0 = -5.3850
    rng = 0.64
    step = 0.02
    fscan2d( diffrz, 0, 4, 90,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5)
             
####
# SAMPLE 10
def g0_206_0N():
    # samrx      samry      samtx      samty     samtz
    # 0.09536   -1.41549    0.00000   -0.02500  -1.99984
    # diffry -6.954723,
    newdataset('tt_g1_206_0N')
    y0 = -6.954723
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 2, 180,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0 )

def g0_206_4N():
    # samrx      samry      samtx      samty     samtz
    # 0.42047   -1.52794    0.00000   -0.02500  -1.99900
    # diffry -6.952253951,
    newdataset('tt_g1_206_4N')
    y0 = -6.952253951
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 2, 180,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0 )

def g0_206_6N():
    # samrx      samry      samtx      samty     samtz
    # 0.49046   -1.54754    0.00000   -0.12500  -1.98182
    # diffry -6.9491,
    newdataset('tt_g1_206_6N')
    y0 = -6.9491
    rng = 0.8
    step = 0.02
    fscan2d( diffrz, 0, 2, 180,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0 )
             
def g0_202_0N():
    # samrx      samry      samtx      samty     samtz
    #  2.81871    3.66263   0.00000   -0.02500  -1.84984

    # diffry -3.108
    newdataset('tt_g1_202_0N')
    y0 = -3.108
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 2, 180,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5 )
             
def g0_202_4N():
    # samrx      samry      samtx      samty     samtz
    #  3.14342    3.55943   0.00000   -0.02500  -1.84984

    # diffry -3.1045
    newdataset('tt_g0_202_4N')
    y0 = -3.1045
    rng = 0.4
    step = 0.02
    fscan2d( diffrz, 0, 2, 180,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5 )
             
def g0_202_6N():
    # samrx      samry      samtx      samty     samtz
    #  3.29892    3.54827   0.00000   -0.02500  -1.84984

    # diffry -3.10249
    newdataset('tt_g0_202_6N')
    y0 = -3.10249
    rng = 0.8
    step = 0.02
    fscan2d( diffrz, 0, 2, 180,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5 )             
             
def g0_202_7N():
    # samrx      samry      samtx      samty     samtz
    #  3.73408    3.48588   0.00000   -0.02500  -1.84984

    # diffry -3.10418
    newdataset('tt_g0_202_7N')
    y0 = -3.104283
    rng = 1.3
    step = 0.02
    fscan2d( diffrz, 0, 2, 180,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5 , fast_motor_mode='ZIGZAG')   
             
def g0_202_unload():
    # samrx      samry      samtx      samty     samtz
    # 4.44715    4.63300   0.00000   -0.12500  -1.84984
    # d2tz  0.52
    # diffry -3.1125
    newdataset('tt_g0_202_unload_fine')
    y0 = -3.1125
    rng = 0.8
    step = 0.01
    fscan2d( diffrz, 0, 2, 180,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5 , fast_motor_mode='ZIGZAG')  
             
    newdataset('tt_g0_202_unload')
def g0_202_unload_continue():
    y0 = -3.1125
    rng = 0.8
    step = 0.02
    fscan2d( diffrz, 0, 2, 180,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5 , fast_motor_mode='ZIGZAG')  


def g0_night():
    # samrx      samry      samtx      samty     samtz
    # 0.09536   -1.41549    0.00000   -0.02500  -1.99984
    # diffry -6.954723,
    newdataset('tt_g1_206_0N_fine')
    y0 = -7.05
    rng = 0.4
    step = 0.02
    astep = 0.45
    
    umv(d2tz, 2.37, nfdtx, 9)
    umv(samrx, 0.09536, samry, -1.41549)#, samtz, -1.99984)
    fscan2d( diffrz, 0, astep, 360/astep,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0 )

    # samrx      samry      samtx      samty     samtz
    #  2.81871    3.66263   0.00000   -0.02500  -1.84984
    # diffry -3.108
    newdataset('tt_g1_202_0N_fine')
    y0 = -3.108
    umv(diffry, y0)
    umv(samrx, 2.81871, samry, 3.66263, samtz, -1.84984)
    umv(d2tz, 0.52, nfdtx, 9)
    fscan2d( diffrz, 0, astep, 360/astep,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5 )
             
def g0_night_ctd():
    newdataset('tt_g1_202_0N_fine')
    y0 = -3.108
    rng = 0.4
    step = 0.02
    astep = 0.45

    umv(diffry, y0)
    umv(samrx, 2.81871, samry, 3.66263, samtz, -1.84984)
    umv(d2tz, 0.52, nfdtx, 9)
    fscan2d( diffrz, 0, astep, 360/astep,
             diffry, y0 - rng/2,  step, rng/step, 
             0.5 )
             
def g0_WedNight():
    # samrx      samry      samtx      samty     samtz
    # 0.99143   -1.54754    0.00000   -0.12500  -1.98182
    # diffry -6.9491,
    newdataset('tt_g1_206_6N_fine')
    y0 = -6.95
    rng = 2.0
    step = 0.02
    fscan2d( diffrz, 0, 1, 360,
             diffry, y0 - rng/2,  step, rng/step, 
             1.0 )

def ff_rock():
    while 1:
        fscan(diffrz,   0, 10, 2, 0.8 )
        fscan(diffrz, 20, -10, 2, 0.8 )
        


############## checked up to here, not below!!!!

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico4']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico4']

 

def marana_pct(pct_pars):
    flatbeam()
    marana.image.roi=[0,0,2048,2048]
    sct(pct_pars['exp_time'])
    umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'])




def marana_large_beam(pct_pars):
    tfoh1.set(64,'Al')
    tfoh1.set(0,'Be')
    #for i in range (3):
    #    umv(tfz, -0.46, tfy, 14.488)
    marana.image.roi=[512,0,1024,2048]
    sct(0.05)
    umvct(s7vg, pct_pars['slit_vg'], s7hg, pct_pars['slit_hg'], nfdtx, pct_pars['dist'], ffdtx1, pct_pars['dist'] + ff_offset)
    umvct(s8vg, pct_pars['slit_vg'] + 0.5, s8hg, pct_pars['slit_hg'] + 0.5)
    umvct(samtz, samtz_offset)
    ct(pct_pars['exp_time'])

def marana_dct(dct_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(10,'Be')
    #for i in range (3):
    #    umv(tfz, -0.32, tfy, 14.445)
    marana.image.roi=[0,0,2048,2048]
    sct(dct_pars['exp_time'])
    umvct(s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    umvct(s8hg, dct_pars['slit_vg'] + 0.5, s8hg, dct_pars['slit_hg'] + 0.5)
    umvct(nfdtx, dct_pars['dist'], ffdtx1, dct_pars['dist'] + ff_offset, d3tz, d3tz_pos_dct)


def frelon16in(theta, tt_pars):
    tfoh1.set(0,'Al')
    tfoh1.set(0,'Be')
    #for i in range (3):
    #    umv(tfz, -0.39, tfy, 14.51)
    lyout();
    ACTIVE_MG.disable("marana:image")
    ACTIVE_MG.disable("frelon3*") 
    ACTIVE_MG.enable("frelon16:image")
    frelon16.image.flip=[False, True]
    d2tzpos = np.tan(2*np.deg2rad(theta))*tt_dist
    print("moving nfdtx, ffdtx1 and ffdtz1 to save positions")
    umvct(nfdtx, tt_pars['dist'], ffdtx1, tt_pars['dist'] + ff_offset, ffdtz1, 200)
    print("moving d2tz to target position %g"%d2tzpos)
    umvct(d2tz, d2tzpos)
    umv(d2ty, 0, d3ty, d3_out)
    ct(0.1)
    print("ready to collect topotomo data")


def read_positions():
    g = {}   
    g['samrx']=samrx.position
    g['samry']=samry.position
    g['samtx']=samtx.position
    g['samty']=samty.position
    g['samtz']=samtz.position
    g['d2tz']=d2tz.position
    return g

def topotomo_tilt_grain(gr_id):
    """drives diffractometer to grain alignment position for
    """
    topotomo_params = read_tt_infos(gr_id)
    print(topotomo_params)
    if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
        return('Error')
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d3tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')

def topotomo_tilt_grain_json(gr_id):
    """drives diffractometer to grain alignment position for
    """
    topotomo_params = get_json_grain_info(gr_id)
    print(topotomo_params)
    if abs(topotomo_params['gr_id'] - gr_id) > 0.1:
        return('Error')
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d2tz, topotomo_params['d3tz'])
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')
    
def topotomo_tilt_grain_dict(topotomo_params):
    """drives diffractometer to current grain alignment position
    """
    print(topotomo_params)
    if abs(d3ty.position - d3_out) > 0.1:
        return('You first have to switch to topotomo detector system:  user.frelon16in() !')
    umv(nfdtx, topotomo_params['nfdtx'])
    umv(d2tz, topotomo_params['d2tz'])
    umv(diffrz, 90) #save position to change tilts for the diffractometer with Nanox during ma5178...
    umv(diffry, topotomo_params['diffry'], samrx,topotomo_params['samrx'], samry, topotomo_params['samry'])
    umv(samtx, topotomo_params['samtx'], samty, topotomo_params['samty'], samtz, topotomo_params['samtz'])
    return('Success')

# -*- coding: utf-8 -*-
import os
import numpy as np



def define_tt_pars():
    tt_pars={}
    tt_pars['diffrz_step'] = 4
    tt_pars['num_proj'] = 360 / tt_pars['diffrz_step']
    tt_pars['diffry_step'] = 0.05
    tt_pars['exp_time'] = 0.1
    tt_pars['search_range'] = 0.4
    tt_pars['scan_mode'] = 'CAMERA'
    tt_pars['image_roi'] = [0, 0, 2048, 2048] 
    tt_pars['counter_roi'] = [824, 824, 400, 400] 
    tt_pars['slit_hg'] = 0.4
    tt_pars['slit_vg'] = 0.4
    tt_pars['dist'] = 11
    return tt_pars
    
def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.05
    dct_pars['refon'] = dct_pars['num_proj']  
    dct_pars['ref_step'] = -1
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.8
    dct_pars['slit_vg'] = 0.5
    dct_pars['dist'] = 9
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 0.0
    dct_pars['shift_step_size'] = 0.28   # not used?
    dct_pars['nof_shifts'] = 5           # not used
    return dct_pars

def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = 0
    pct_pars['step_size'] = 0.4
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.2
    pct_pars['refon'] = pct_pars['num_proj']
    pct_pars['ref_step'] = -3.5
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 41
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = samtz
    pct_pars['slit_hg'] = 1.7
    pct_pars['slit_vg'] = 3.5
    pct_pars['dist'] = 50
    return pct_pars
    
def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 0
    ff_pars['step_size'] = 1
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.08
    ff_pars['slit_hg'] = 0.8
    ff_pars['slit_vg'] = 0.6
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = samtz
    ff_pars['samtz_cen'] = 0
    ff_pars['shift_step_size'] = 0
    ff_pars['nof_shifts'] = 1
    ff_pars['cpm18_detune'] = 0.15
    return ff_pars
    
def define_sff_pars():
    sff_pars={}
    sff_pars['start_pos'] = 0
    sff_pars['step_size'] = 1
    sff_pars['num_proj'] = 180 / sff_pars['step_size']
    sff_pars['exp_time'] = 0.08
    sff_pars['slit_hg'] = 0.1
    sff_pars['slit_vg'] = 0.1
    sff_pars['mode'] = 'ZIGZAG'
    sff_pars['cpm18_detune'] = 0.1
    return sff_pars

def load_grains_from_matlab(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = read_tt_infos(gr_id)
        grain_list.append(grain)
    return grain_list
    
def load_grains_from_json(grain_ids):
    grain_list = []
    for gr_id in grain_ids:
        grain = get_json_grain_info(gr_id)
        grain_list.append(grain)
    return grain_list

def load_ramp_by_ascan(start, stop, npoints, exp_time, loadstep, pct_pars):
    marana_large_beam(pct_pars)
    newdataset('loadramp_%d' %  loadstep)
    ascan(stress, start, stop, npoints, exp_time, stress_cnt, marana)
    return

def sff_one_grain(scanname, sff_pars, grain):
    user.cpm18_goto(cpm18_tuned + sff_pars['cpm18_detune'])
    umv(s7vg, 0.5, s7hg, 0.5, s8vg, 0.05, s8hg, 0.05) 
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, bigy_in)
    umv(samtx, grain['samtx'], samty, grain['samty'], samtz, grain['samtz'])
    stage_diffty_zero = diffty_offset

    scanstarty= -0.1  # in mm
    scanendy=0.101
    scanstepy=0.01
    
    scanstartz= -0.15  # in mm
    scanendz=0.1501 
    scanstepz=0.01    

    umvr(samtz, scanstartz)
    z_scans_number = (scanendz - scanstartz) / scanstepz
    newdataset(scanname)
    for i in range(int(z_scans_number)):
        umvr(samtz, scanstepz)
        for ty in np.arange(scanstarty,scanendy,scanstepy):
            umv(diffty, stage_diffty_zero+ty)
            print(f"Scan pos: y: {ty} couche {i}")
            finterlaced( diffrz, sff_pars['start_pos'], acq_size = sff_pars['step_size'], npoints = sff_pars['num_proj'], acq_time = sff_pars['exp_time'], mode = sff_pars['mode'])

    umv(diffrz,0)      
    umv(diffty, stage_diffty_zero)
    umv(bigy, bigy_out)
    diffty_airpad.off()
    user.cpm18_goto(cpm18_tuned)
    
    umv(samtz,0)
    print("All done!")
    

def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def load_ramp_by_target(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    write_to_file = to_file if to_file is not None else sys.stdout
    marana_large_beam(pct_pars)
    current_load = stress_cnt.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return


def define_pars(load_list):
    """Produces default input parameters for a load sequence (see next function)
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    for load in load_list:
        step_pars['dct_pars'] = define_dct_pars()
        step_pars['pct_pars'] = define_pct_pars()
        step_pars['tt_pars'] = define_tt_pars()
        step_pars['ff_pars'] = define_ff_pars()
        step_pars['sff_pars']= define_sff_pars()
        step_pars['target'] = load
        step_pars['load_step'] = step
        step = step + 1
        par_list.append(step_pars)        
    return par_list

def define_pars_one_load():
    """Produces default input parameters for one load
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    step_pars['dct_pars'] = define_dct_pars()
    step_pars['pct_pars'] = define_pct_pars()
    step_pars['tt_pars'] = define_tt_pars()
    step_pars['ff_pars'] = define_ff_pars()
    step_pars['sff_pars']= define_sff_pars()
    step_pars['target'] = load
    step_pars['load_step'] = step
    par_list.append(step_pars)
    return par_list


def load_sequence(par_list, grain_list):
    """Load Experiment Master Sequence.
    
    This function will perform scan sequences (pct, dct, tt) for a list of target load values defined in "par_list" 
    Note: grain_list is currently produced by a matlab script and can be re-created / imported via:  grain_list = user.read_tt_info([list_of_grain_ids])
    par_list can be created via: par_list = user.define_pars()  and sub-parameters for dct, pct, tt can be adapted to cope with increasing mosaicity

    """
    for step_pars in par_list:
        dct_pars = step_pars['dct_pars']
        pct_pars = step_pars['pct_pars']
        tt_pars = step_pars['tt_pars']
        ff_pars = step_pars['ff_pars']
        sff_pars = step_pars['sff_pars']
        target = step_pars['target']
        load_step = step_pars['load_step']
        grain_list = load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, ff_pars, sff_pars, target, load_step)
    return grain_list

def load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, ff_pars, sff_pars, target, loadstep):
    """Performs a loadramp to the new target value and launches PCT, DCT and a series of TT scans at this new target load
    """
    ## PCT
    maranain(pct_pars['dist'])
    marana_large_beam(pct_pars)
    #load_ramp_by_target(target)
    scan_name = 'pct_%dN_' % target
    tomo_by_fscan_dict(scan_name, pct_pars)
    ## DCT
    marana_dct(dct_pars)
    scan_name = 'dct_%dN_' % target
    dct_zseries(dct_pars, datasetname = scan_name)
    ## TT
    scan_name = 'tt_%dN_' % target
    marana_tt(grain_list, tt_pars, target)
    ##frelon16in(4, tt_pars)
    ##grain_list = loop_grains(grain_list, tt_pars, target)
    ## 3DXRD
    ffin()
    scan_name = 'ff_%dN_' % target    
    ff_zseries(ff_pars, scan_name)
    ## s-3DXRD
    #scan_name = 'sff_%dN_g%d' % (target, grain_list[0]['gr_id'])
    #tdxrd_pointscan(scan_name, sff_pars, grain)
    scan_name = 'sff_2d_%dN_g%d' % (target, grain_list[0]['gr_id'])
    sff_one_grain(scan_name, sff_pars, grain_list[0])
    return grain_list


def loop_grains(grain_list, tt_pars, target):
    """Loops through a list of Topotomo grains  and performs refinement + TT scan_acquisition
    :param dict grain_list:  list of grain structures containing Topotomo alignment values
    :param dict tt_pars: Topotomo scan parameters
    :param int step: running number of load step in load-sequence  
    """
    tfoh1.set(16,'Be')
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    frelon16.image.roi = tt_pars['image_roi'] # [510, 510, 900, 900]
    frelon16.roi_counters.set('roi1', tt_pars['counter_roi'])  #[300, 300, 300, 300])
    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    for i in range(len(grain_list)):
        newdataset('grain_%04d_%dN_' % (grain_list[i]['gr_id'], target))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time)
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'])
        sleep(1)
        #update_grain_info(grain_list[i])
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    sam_dct_pos()
    return grain_list

def marana_tt(grain_list, tt_pars, target):
    tfoh1.set(0,'Be')
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    umv(d3tz, d3tz_pos_tt, nfdtx, tt_pars['dist'])
    marana.image.roi = tt_pars['image_roi']
    marana.roi_counters.set('roi1', tt_pars['counter_roi'])
    ACTIVE_MG.enable('marana:roi_counters:roi1_avg')
    for i in range(len(grain_list)):
        newdataset('grain_%04d_%dN_' % (grain_list[i]['gr_id'], target))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time, tt_pars['scan_mode'])
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'], tt_pars['scan_mode'])
        sleep(1)
        update_grain_info(grain_list[i])
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    sam_dct_pos()
    return grain_list

        
def refine_tt_alignment(tt_grain, ang_step=0.05, search_range=0.5, exp_time=0.1, scan_mode = 'TIME'):
    """Refine the topotomo samrx, samry alignment.
    
    This function runs 4 base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use (in degrees).
    :param float search_range: the base tilt angular search range in degrees.
    """
     # half base tilt range in degree
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/visitor/ma5179/id11/NiTiNanox/'
    sample_name = 'NiTiNanox'
    
    # open a script to gather all acquisition commands
    print(tt_grain)
    gid = tt_grain['gr_id']
    print('find range for grain %d\n' % gid)
    # check that diffry is negative (Bragg alignment)
    #if tt_grain['diffry'] > 0:
    #    raise(ValueError('diffry value should be negative, got %.3f, please check your data' % tt_grain['diffry']))
        
    # align our grain
    topotomo_tilt_grain_dict(tt_grain)

    # define the ROI automatically as [710, 710, 500, 500]
    #frelon16.image.roi = [0, 0, 1920, 1920]
    #ct(0.1)
    #frelon16.roi_counters.set('roi1', [710, 710, 500, 500])
    #ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    #exp_time = exp_time / tt_grain['int_factor']
    # first scan at 0 deg
    umvct(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_000, end_angle_000, cen_angle_000 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # second scan at 180 deg
    umv(diffrz, 180)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_180, end_angle_180, cen_angle_180 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samry by half the difference
    samry_offset = 0.5 * (cen_angle_000 - cen_angle_180)
    print("moving samry by %.3f, final position: %.3f" % (samry_offset, tt_grain['samry'] + samry_offset))
    umvr(samry, samry_offset)

    # third scan at 270 deg
    umv(diffrz, 270)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_270, end_angle_270, cen_angle_270 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # fourth scan at 90 deg
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_090, end_angle_090, cen_angle_090 = get_limits_and_weighted_cen_angle(fscan.get_data())

    # correct samrx by half the difference
    samrx_offset = 0.5 * (cen_angle_090 - cen_angle_270)
    print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    #samrx_offset =  tt_grain['diffry'] - cen_angle_090
    #print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    umvr(samrx, samrx_offset)

    # update values in tt_grain
    tt_grain['samrx'] += samrx_offset
    tt_grain['samry'] += samry_offset
    update_grain_info(tt_grain)
    print('values updated in tt_grain record')
    return tt_grain

def find_range(tt_grain, ang_step=0.05, search_range=0.3, exp_time=0.1, scan_mode = 'TIME'):
    """Find the topotomo angular range.
    
    This function runs two base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition. Note that this function 
    assumes that the grain has been aligned already.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use.
    :param float search_range: the base tilt angular search range in degrees.
    """
    step = 1  # load step
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/visitor/ma5179/id11/NiTiNanox'
    sample_name = 'NiTiNanox'
    
    # open a script to gather all acquisition commands
    gid = tt_grain['gr_id']
    cmd_path = os.path.join(data_dir, 'tt_acq_grain_%d_step_%d.py' % (gid, step))
    f = open(cmd_path, 'w')
    f.write("def tt_acq():\n\n")
    f.write("    # activate ROI\n")
    f.write("    frelon16.image.roi = [710, 710, 500, 500]\n")
    f.write("    frelon16.roi_counters.set('roi1', [100, 100, 300, 300])\n")
    f.write("    ct(0.1)\n")
    f.write("    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')\n\n")
    
    print('find range for grain %d\n' % gid)
    
    # see if we need to create a new dataset or not
    #dataset = 'grain_%04d_checkrange_step_%d' % (gid, step)
    #newdataset(dataset)

    # define the ROI automatically as [710, 710, 500, 500]
    #frelon16.roi_counters.set('roi1', [710, 710, 500, 500])
    #ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    #ACTIVE_MG.enable('marana:roi_counters:roi1_avg')
    # first scan at 90 deg
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_90, end_angle_90 = get_limits(fscan.get_data())
    diffry_range_90 = max(tt_grain['diffry'] - start_angle_90, end_angle_90 - tt_grain['diffry'])

    # second scan at 0 deg
    umv(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode= scan_mode)
    start_angle_00, end_angle_00 = get_limits(fscan.get_data())
    diffry_range_00 = max(tt_grain['diffry'] - start_angle_00, end_angle_00 - tt_grain['diffry'])

    # use the upper range value to define topotomo bounds
    print('diffry_range_00=%.3f - diffry_range_90=%.3f' % (diffry_range_00, diffry_range_90))
    diffry_range = max(diffry_range_00, diffry_range_90)

    # now find out the largest range to cover the grain
    diffry_start = tt_grain['diffry'] - diffry_range
    diffry_end = tt_grain['diffry'] + diffry_range
    n_acq_images = (diffry_end - diffry_start) / ang_step

    # write out the topotomo command for bliss
    f.write("    # create a new data set\n")
    f.write("    newdataset('grain_%04d_step_%d')\n\n" % (gid, step))
    f.write("    # acquisition for grain %d\n" % gid)
    f.write("    user.topotomo_tilt_grain(%d)\n" % gid)
    f.write("    umv(samrx, %.3f)\n" % tt_grain['samrx'])
    f.write("    umv(samry, %.3f)\n" % tt_grain['samry'])
    f.write("    fscan2d(diffrz, 0, 10, 36, diffry, %.3f, %.3f, %d, 0.1, scan_mode=tt_pars['scan_mode'])\n\n" % (diffry_start, ang_step, n_acq_images))
    f.close()
    return diffry_start, n_acq_images

def get_limits(scan_data, thres=0.05, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry = scan_data['diffry']
    # work out the angular step
    ang_step = diffry[1] - diffry[0]
    intensity = scan_data['marana:roi_counters:roi1_avg']
    # background correction
    bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) -1
    start_angle = diffry[start_pos] - padding * ang_step
    end_angle = diffry[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, number of images={}'.format(start_angle, end_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle
    
def get_limits_and_weighted_cen_angle(scan_data, thres=0.05, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry = scan_data['diffry']
    # work out the angular step
    ang_step = diffry[1] - diffry[0]
    intensity = scan_data['frelon16:roi_counters:roi1_avg']
    # background correction
    #bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    bg = np.min(intensity)
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) - 1
    weighted_cen_pos = 0;
    for i_pos in range(start_pos, end_pos, 1):
        weighted_cen_pos += i_pos * intensity[i_pos]
    weighted_cen_pos = weighted_cen_pos / np.sum(intensity[start_pos:end_pos])
    # compute the angle using linear interpolation
    weighted_cen_angle = (diffry[start_pos] * (end_pos - weighted_cen_pos) + diffry[end_pos] * (weighted_cen_pos - start_pos)) / (end_pos - start_pos)
    start_angle = diffry[start_pos] - padding * ang_step
    end_angle = diffry[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, weighted_cen_angle={}, number of images={}'.format(start_angle, end_angle, weighted_cen_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle, weighted_cen_angle




import json
import time
import os

class NumpyEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self,obj)


def writedictionaryfile(dictname,filename):
    """to write dictionary to file"""
    datatowrite = json.dumps(dictname)
    with open('%s'%filename, 'w') as f:
        f.write(datatowrite)
    print("Dictionary written as %s"%filename)
    return

def appenddictionaryfile(dictname,filename):
    """to write dictionary to file"""
    if os.path.exists('%s'%filename):
        with open('%s'%filename, 'r+') as f:
            data_to_write = [json.load(f)]
            dictname['time'] = str(time.ctime())
            data_to_write.append(dictname)
            f.seek(0)
            json.dump(data_to_write, f)
    else:
        newdict = dictname
        newdict['time'] = str(time.ctime())
        with open('%s'%filename, 'w') as f:
            json.dump(newdict, f)
            #f.write(datatowrite)
    print("Dictionary appended to %s"%filename)
    return

def readdictionaryfile(filename):
    """to read dictionary from file, returns dictionary"""
    with open('%s'%filename, 'r') as f:
        readdata = [json.loads(f.read())]
    print("Dictionary read from %s"%filename)
    return readdata[-1]


def update_grain_info(grain):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    writedictionaryfile(grain,'%s/%s_g%s'%(basedir,samplename,grain['gr_id']))
    #appenddictionaryfile(grain,'%s_g%s_history'%(SCAN_SAVING.collection_name,grain['gr_id']))
    return

def get_json_grain_info(grain_number):
    samplename = SCAN_SAVING.collection_name
    basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,samplename)
    return readdictionaryfile('%s/%s_g%s'%(basedir,samplename,str(grain_number)))

def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])
    




def ref_scan_samy(exp_time, nref, ystep, omega, scanmode):
    umvr(samy, ystep)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umvr(samy, -ystep)
    
def ref_scan_difftz(exp_time, nref, zstep, omega, scanmode):
    difftz0 = difftz.position
    umv(difftz, difftz0 + zstep)
    print("fscan(diffrz, %6.2f, 0.1, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.1, nref, exp_time, scan_mode = scanmode)
    umv(difftz, difftz0)
    
class LoadLoop:
    def __init__(self):
        self._task = None
        self._stop = False
        self._sleep_time = 1
        self._filepath = None
        
    def start(self,target,loadstep,filepath,sleep_time=1.,time_step=0.5):
        self._filepath = filepath
        if self._task:
            self._stop = True
            self._task.get()

        self._sleep_time = sleep_time
        self._stop = False
        self._task = gevent.spawn(self._run,target,loadstep,time_step)

    def stop(self):
        self._stop = True
        if self._task: 
           self._task.get()

    def _run(self,target,loadstep,time_step):
        newdataset('loadramp_%d' %  loadstep)
        with open(self._filepath,'a') as f:
            while not self._stop:
                load_ramp_by_target(target,0,loadstep,time_step,to_file=f)
                gevent.sleep(self._sleep_time)

def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')


def tomo_series_cst_load(start, num_scans, target, sleep_time=0, pct_pars=None):
    for i in np.arange(start, start+num_scans):
        tomo.full_turn_scan(str(i))
        load_ramp_by_target(target, 0.05, 1)
	#dct_marana_dict(str(i), pct_pars)
        sleep(sleep_time)
    return

def ftomo_series(scanname, start, num_scans, sleep_time=0, pars=None):
    for i in np.arange(start, start+num_scans):
        newdataset(scanname + str(i))
        umv(diffrz, pars['start_pos']);
        fsh.disable()
        fsh.close()
        print("taking dark images")
        ftimescan(pars['exp_time'], pars['nref'],0)
        fsh.enable()
        print("taking flat images")
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'], pars['scan_mode'])
        print("taking projections...")
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], pars['scan_mode']))
        fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
        print("resetting diffrz to 0")
        umv(diffrz, pars['start_pos']+360);
        diffrz.position=pars['start_pos'];
        sleep(sleep_time)
    return

def tdxrd_boxscan(scanname, ff_pars):
    print("using slit_vg %f and slit_hg %f" % (ff_pars['slit_vg'], ff_pars['slit_hg']))
    umv(s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
    umv(s8vg, ff_pars['slit_vg'] + 0.5, s8hg, ff_pars['slit_hg'] + 0.5)
    newdataset(scanname)
    finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])

def tdxrd_pointscan(scanname, sff_pars, grain):
    user.cpm18_goto(cpm18_tuned + sff_pars['cpm18_detune'])
    umv(s7vg, 0.5, s7hg, 0.5, s8vg, 0.05, s8hg, 0.05) 
    diffty_airpad.on(60*24*5)
    umv(diffrz, 0, bigy, bigy_in)
    #umv(samtx, grain['samtx'], samty, grain['samty'], samtz, grain['samtz'])
    stage_diffty_zero = diffty_offset
    scanstarty= -0.2  # in mm
    scanendy=0.201 
    scanstep=0.01
    
    newdataset(scanname)

    for ty in np.arange(scanstarty,scanendy,scanstep):
        umv(diffty, stage_diffty_zero+ty)
        print(f"Scan pos: y: {ty} ")
        finterlaced( diffrz, sff_pars['start_pos'], acq_size = sff_pars['step_size'], npoints = sff_pars['num_proj'], acq_time = sff_pars['exp_time'], mode = sff_pars['mode'])

    umv(diffrz,0)      
    umv(diffty, stage_diffty_zero)
    umv(bigy, bigy_out)
    diffty_airpad.off()
    user.cpm18_goto(cpm18_tuned)
    print("All done!")
 
def update_flat():
    image_corr.take_dark()
    image_corr.take_flat()
    image_corr.dark_on()
    image_corr.flat_on()

def flat_on():
    image_corr.dark_on()
    image_corr.flat_on()

def flat_off():
    image_corr.dark_off()
    image_corr.flat_off()

