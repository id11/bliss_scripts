
import time
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
        
def measuredata(nframes, atime):
    from bliss.setup_globals import ftimescan
    ftimescan(atime, nframes)
    # loopscan(nframes, atime)

def quicktest():
    for i in range(3):
        measuredata(10, 0.5)

def pdf_scan_new():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'Ce0.0Gd1.0O1.5',  }
    measuredata(120,0.5)
    nanodacpool.ramprate = 5
    nanodacpool.setpoint = 750
    newdataset('heating')
    while nanodacpool_temp.read() < 748:
        measuredata(120,0.5)
      
    newdataset('750C')
    for i in range(25):
        measuredata(120,0.5)        

    nanodacpool.ramprate = 5
    nanodacpool.setpoint = 25
    newdataset('cooling')
    while nanodacpool_temp.read() > 27:
        measuredata(120,0.5)
    
    sheh1.close()


def pdf_scan():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'Ce0.625Gd0.375O1.8125',  }
    measuredata(120,0.5)
    nanodacpool.ramprate = 5
    nanodacpool.setpoint = 750
    newdataset('heating')
    while nanodacpool_temp.read() < 749:
        measuredata(120,0.5)

    newdataset('750C')
    for i in range(12):
        measuredata(120,0.5)        
    sheh1.close()


def pdf_cooling_new():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'CeO2',  }
    nanodacpool.ramprate = 5
    nanodacpool.setpoint = 25
    newdataset('cooling')
    while nanodacpool_temp.read() > 27:
        measuredata(120,0.5)
    sheh1.close()
        
# to reload
#  user_script_load("ch6894")
    
    
def pdf_scan_CO():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'Ce0.625Gd0.375O1.8125',  }

    newdataset('750C_CO ')
    for i in range(135):
        measuredata(120,0.5)        


    nanodacpool.ramprate = 5
    nanodacpool.setpoint = 25

    newdataset('cooling_CO')
    while nanodacpool_temp.read() > 30:
        measuredata(120,0.5)

    
    sheh1.close()

# ATTENTION
# modify the number of measuredatas from 30 to 85!
    
def pdf_scan_CO_RT():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'Ce0.625Gd0.375O1.8125',  }

    newdataset('CO_RT')
    measuredata(120,0.5) 

    sheh1.close()
    
    
def pdf_scan_CO_heating():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'Ce0.625Gd0.375O1.8125',  }
    nanodacpool.ramprate = 10
    nanodacpool.setpoint = 750
    newdataset('CO_heating')
    while nanodacpool_temp.read() < 749:
        measuredata(120,0.5)

    newdataset('750C_CO-bis')
    for i in range(135):
        measuredata(120,0.5)        
    
    
def pdf_scan_CO_cooling():
    sheh1.open()
    xrpd_processor.pdf_options={'composition': 'Ce0.625Gd0.375O1.8125',  }

    nanodacpool.ramprate = 5
    nanodacpool.setpoint = 25

    newdataset('cooling_CO_bis')
    while nanodacpool_temp.read() > 30:
        print('waiting for temperature 30\n')
        measuredata(120,0.5)

    sheh1.close()

