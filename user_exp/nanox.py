CONTROLLERNAME = 'E-754' 
STAGES = None  
REFMODES = None

SCALEOUTPUT = 0.025

CYCLELENGTH = 3125
SINUSLENGTH = 3125
SINUSSTART = 0
SINUSCENTER = 1562


def sinus(amp, amp_offset, nb):
	wavegenerator = 1
	wavetable = 1

	ctrl = stress.controller

	print(f'Set sine waveforms  for wave table {wavetable}')
	ctrl.command(f"WAV {wavetable} X SIN_P {CYCLELENGTH} {amp} {amp_offset} {SINUSLENGTH} 0 {SINUSCENTER}")

	ctrl.command(f"WSL {wavegenerator} {wavetable}")
	ctrl.command(f"WGC {wavegenerator} {nb}")

	ctrl.command(f"SVO 1 1")

	start = input('ready to go ? [yes/no] ')
	if start == "yes":
		ctrl.command(f"WGO {wavegenerator} 1")
	else:
		print("No test done")


def fatigue():
	Vmax = float(input('Enter max value for amplitude (Newton) : '))
	Vmin = float(input('Enter min value for amplitude (Newton) : '))
	Ncycles = int(input('Enter number of cycles for the fatigue test : '))
    
	amplitude = Vmax - Vmin

	sinus(amplitude, Vmin, Ncycles)


def run():
#Connect ...

	ctrl = stress.controller

#Init controller
	ctrl.command("CCL 1 advanced")
	ctrl.command("SPA 2 0x0A000003 2")
	ctrl.command("SPA 2 0x0A000004 1")
	ctrl.command(f"SPA 1 0x07001005 {SCALEOUTPUT}")


	ctrl.command("SPA 3 0x02000200 0")
	ctrl.command("SPA 3 0x02000300 4")

	do_zero = input("Do Auto-Zero ? [yes/no]")
	if do_zero == "yes":
		ctrl.command("ATZ 1 0")
		time.sleep(5)
	else:
		fatigue()

