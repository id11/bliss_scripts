import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime

atty = config.get('atty')
attrz = config.get('attrz')
tfoh1 = config.get('tfoh1')

save_path = '/data/visitor/ihma500/id11/20240519/RAW_DATA/'
f_target = 3
load = f_target

prefix_name = 'load_3N'
print(prefix_name)


def define_dct_pars():
    dct_pars={}
    dct_pars['start_pos'] = 0
    dct_pars['step_size'] = 0.1
    dct_pars['num_proj'] = 360 / dct_pars['step_size']  
    dct_pars['exp_time'] = 0.25
    dct_pars['refon'] = dct_pars['num_proj']
    dct_pars['ref_step'] = -1
    dct_pars['scan_mode'] = 'CAMERA'
    dct_pars['nref'] = 41
    dct_pars['slit_hg'] = 0.42
    dct_pars['slit_vg'] = 0.1
    dct_pars['dist'] = 8
    dct_pars['ref_mot'] = samy
    dct_pars['zmot'] = samtz
    dct_pars['samtz_cen'] = 5.49476
    dct_pars['shift_step_size'] = 0.08
    dct_pars['nof_shifts'] = 5
    dct_pars['scan_type'] = 'fscan'
    dct_pars['samrx_range'] = 3
    dct_pars['samry_range'] = 3
    dct_pars['num_tiltx'] = 3
    dct_pars['num_tilty'] = 3
    return dct_pars

def run_dct_overnight():
    # assum you start with dct condition
    # start DCT measurements
    dct_pars = define_dct_pars()
    print(dct_pars)
    umv(atty,-10,attrz,0)
    dct_zseries(dct_pars, 'dct')
       
    # switch back to DCT camera for alignment (CoR = 1111 pixels)
    fsh.close()
    umv(d3x,100)
    fsh.open()
    umvct(s7vg, 1.5, s7hg, 1.5, s8vg,1.6, s8hg, 1.6)
    sheh3.close()
    print('Done successfully')


def define_pct_pars():
    pct_pars={}
    pct_pars['start_pos'] = 0
    pct_pars['step_size'] = 0.1
    pct_pars['num_proj'] = 360 / pct_pars['step_size']
    pct_pars['exp_time'] = 0.05
    pct_pars['refon'] = pct_pars['num_proj']
    pct_pars['ref_step'] = -2
    pct_pars['scan_mode'] = 'CAMERA'
    pct_pars['nref'] = 41
    pct_pars['ref_mot'] = samy
    pct_pars['zmot'] = difftz #samtz
    pct_pars['samtz_cen'] = -32.9728
    pct_pars['slit_hg'] = 1.5
    pct_pars['slit_vg'] = 1.5
    pct_pars['nof_shifts'] = 1
    pct_pars['shift_step_size'] = 1
    pct_pars['dist'] = 100
    pct_pars['abs_dist'] = 10
    pct_pars['scan_type'] = 'fscan'
    return pct_pars
    
def define_ff_pars():
    ff_pars={}
    ff_pars['start_pos'] = 6.5
    ff_pars['step_size'] = 0.1
    ff_pars['num_proj'] = 360 / ff_pars['step_size']
    ff_pars['exp_time'] = 0.085
    ff_pars['slit_hg'] = 0.6
    ff_pars['slit_vg'] = 0.12
    ff_pars['mode'] = 'ZIGZAG'
    ff_pars['zmot'] = difftz # samtz
    ff_pars['samtz_cen'] = -32.9728
    ff_pars['shift_step_size'] = 0.12
    ff_pars['nof_shifts'] = 5
    ff_pars['cpm18_detune'] = 0
    ff_pars['ceo2_samtx'] = 0.1850
    ff_pars['ceo2_samty'] = -0.7652
    ff_pars['ceo2_samtz'] = 4.6154
    ff_pars['ceo2_difftz'] = -21.7648
    return ff_pars   
    
def run_in_situ_pct_ff_HT(offset_time = 4, prefix_name = prefix_name, load = load, tol = 0.5):
    # offset_time in minutes
    # starting condition: furnace in, load has applied and become stable
    stress_regul.plot()
    t0 = time.time() # launch at 19:08
    dt = 0
    exit_flag = False
    i=0
    ff_start = 0
    while exit_flag == False:
        i=i+1
        dt = time.time() - t0
        print('Current temperature is {} C; Creep time is {} s'.format(nanodac3_temp.read(), dt))
        print('Experimental time is {} s'.format(dt))
        
        switch_to_pct()
        # take 2 projections
        dt = time.time() - t0
        scanname = 'proj_550C_25p6N_' + str(int(dt/60.0 + offset_time)) + 'min'
        umv(diffrz, -9.75+90)
        newdataset(scanname)
        umvr(difftz,-0.2)
        sct(0.05)
        umvr(difftz,0.2*2)
        sct(0.05)
        umvr(difftz,-0.2)  # moving back to the initial position
        umv(diffrz, -9.75)
        
        # switch to pct measurements
        # switch_to_pct()
        dt = time.time() - t0
        scanname = 'pct_550C_25p6N_' + str(int(dt/60.0 + offset_time)) + 'min'
        pct_pars = define_pct_pars()
        print(pct_pars)
        #pct_by_fscan(scanname, pct_pars, scan_back = False)
        fulltomo.full_turn_scan(scanname)
        
        
        if stress_adc.get_value() < load - tol and stress_adc.get_value() > 10.0:
            load_ramp_by_target(load, time_step=0.5)   # come back to the initial load
            
        # switch the motors takes about 4 min
        # switch to 3DXRD measurements
        dt = time.time() - t0
        if i==1:
            ff_start = dt

        if (((dt-ff_start)/60) % 60.0) < 10.0:    # this ensures the ff scans taken every 1 hour
            switch_to_ff()
            ff_pars = define_ff_pars()
            dt = time.time() - t0
            scanname = 'ff_550C_25p6N_' + str(int(dt/60.0 + offset_time)) + 'min_'
            ff_zseries(ff_pars, f'{scanname}')
            if stress_adc.get_value() < load - tol and stress_adc.get_value() > 10.0:
                load_ramp_by_target(load, time_step=0.5)   # come back to the initial load
        
    print('Done successfully')

    
def run_dct_ff(prefix_name = prefix_name):
    # assum you start with dct condition
    # start DCT measurements
    switch_to_dct()
    dct_pars = define_dct_pars()
    print(dct_pars)
    umv(atty,-10,attrz,0)
    #dct_zseries(dct_pars)
    dct_zseries(dct_pars, f'{prefix_name}_dct')
    
    # switch to 3DXRD measurements
    switch_to_ff()
    ff_pars = define_ff_pars()
    ff_zseries(ff_pars, f'{prefix_name}_ff')
    #ff_zseries(ff_pars)
    
    # switch back to DCT camera for alignment (CoR = 1111 pixels)
    fsh.close()
    umv(atty,-10,attrz,0)
    tfoh1.set(0,'Be')
    tfoh1.set(0,'Al')
    ff_out()
    umv(d3ty, 0, d3tz, 0)
    umv(nfdtx,100)
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    fsh.open()
    umvct(s7vg, 1.5, s7hg, 1.5, s8vg,1.6, s8hg, 1.6)
    sheh3.close()
    print('Done successfully')



def switch_to_marana1_dct():
    marana1_in()
    ACTIVE_MG.disable('marana3*')
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana1*')
    #tfoh1.set(16, 'Be')
    umvct(s7vg, 1.5, s7hg, 1.5, s8vg, 1.6)
    return 'Done'

def switch_to_marana3_pct():
    marana3_in()
    ACTIVE_MG.disable('marana1*')
    ACTIVE_MG.disable('frelon3*')
    ACTIVE_MG.enable('marana3*')
    tfoh1.set(0, 'Be')
    umvct(s7vg, 1.5, s7hg, 1.5, s8vg, 1.6)
    return 'Done'

def switch_to_ff():
    fsh.close()
    tfoh1.set(0,'Be')
    ff_in()
    umv(atty,0,attrz,-10)
    ACTIVE_MG.disable('marana3*')
    ACTIVE_MG.disable('marana1*')
    ACTIVE_MG.enable('frelon3*')
    fsh.open()
    umvct(s7vg, 0.9, s7hg, 0.9, s8vg, 1.0)


def marana3_in():
    ff_out()
    marana1_out()
    assert d1tz.position > 90
    assert d2tz.position > 100
    assert d1ty.position < -240
    assert d2ty.position < -40
    umv(nfdtx, 100)
    umv(d3ty, 0)
    umv(d3tz, 0)
    umvct(atty,-10,attrz,0)
    return 'Done'
    
def marana1_in():
    ff_out()
    marana3_out()
    assert ffdtx1.position > 390
    assert ffdtz1.position > 300
    assert d3ty.position > 130
    assert nfdtx.position < 203 and nfdtx.position > 190
    umv(d1tz, 0, d2tz, 27.47)
    umv(d2ty, 203.625)
    umv(d1ty, 0)
    umvct(atty,-10,attrz,0)
    #umv(d1x, 5) # if the sample has not been centered, this will hit the sample

def ff_in():
    marana3_out()
    marana1_out()
    assert d3ty.position > 110
    assert d2ty.position < -40
    umv(ffdtz1, 0)
    umv(ffdtx1, 180)
    
def marana1_out():
    umv(nfdtx, 200)
    umv(d1ty, -245)
    umv(d2ty, -47)
    umv(d1tz, 100, d2tz, 126)
    
    
def marana3_out():
    umv(nfdtx,200)
    umv(d3ty,150)

def ff_out():
    umv(ffdtx1, 600)
    umv(ffdtz1, 350)     



def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'], s7vg, dct_pars['slit_vg'], s7hg, dct_pars['slit_hg'])
    #umv(s8vg, dct_pars['slit_vg'] + 0.05, s8hg, dct_pars['slit_hg'] + 0.05)
    #umv(s8vg, dct_pars['slit_vg'] + 0.05)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['num_proj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], dct_pars['ref_step'], dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(dct_pars['ref_mot'], dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['num_proj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])
    
def dct_zseries(dct_pars, datasetname = 'dct'):
    shift_step_size = abs(dct_pars['shift_step_size'])
    if(dct_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = dct_pars['samtz_cen'] - (dct_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(dct_pars['samtz_cen']))
    if(offset_samtz_pos < dct_pars['zmot'].low_limit or offset_samtz_pos + (dct_pars['nof_shifts'] - 1) * shift_step_size > dct_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(dct_pars['nof_shifts'])):
        umv(dct_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        tomo_by_fscan_dict(datasetname + str(iter_i + 1), dct_pars)
    umv(dct_pars['zmot'], dct_pars['samtz_cen'])
    print('DCT Succeed')
    return('Succeed')

def tomo_by_fscan_dict(scanname, pars):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
#    umv(s8vg, pars['slit_vg'] + 0.05, s8hg, pars['slit_hg'] + 0.05)
    #umv(s8vg, pars['slit_vg'] + 0.05)
    num_im_grp = pars['refon']
    for ref_grp in np.arange(0, pars['num_proj']/pars['refon'], 1):
        startpos_grp = ref_grp * pars['refon'] * pars['step_size'] + pars['start_pos']
        ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], startpos_grp, pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], pars['scan_mode']))
        if pars['scan_type'] == 'fscan':
            fscan(diffrz, startpos_grp, pars['step_size'], num_im_grp, pars['exp_time'], scan_mode = pars['scan_mode'])
        elif pars['scan_type'] == 'finterlaced':
            finterlaced(diffrz, startpos_grp, pars['step_size'], pars['num_proj'], pars['exp_time'], mode='REWIND')
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], pars['ref_step'], pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan(pars['ref_mot'], pars['exp_time'], pars['nref'], 0, pars['start_pos'] + pars['num_proj']*pars['step_size'], pars['scan_mode'])
    fsh.enable()
    umv(diffrz, pars['start_pos'])

def ref_scan(ref_mot, exp_time, nref, ref_step, omega, scanmode):
    refpos = ref_mot.position
    umv(ref_mot, refpos + ref_step)
    print("ftimescan(%f, %d, scan_mode=%s)" % (exp_time, nref, scanmode)) 
    ftimescan(exp_time, nref, scan_mode=scanmode)
    #fscan(diffrz, omega, 0.01, nref, exp_time, scan_mode = scanmode)
    umv(ref_mot, refpos)
    
def ff_zseries(ff_pars, datasetname = 'ff'):
    #user.cpm18_goto(cpm18_tuned + ff_pars['cpm18_detune'])
    #umv(u22, u22_tuned + ff_pars['cpm18_detune'])
    umv(diffrz, ff_pars['start_pos'], s7vg, ff_pars['slit_vg'], s7hg, ff_pars['slit_hg'])
#    umv(s8vg, ff_pars['slit_vg'] + 0.05, s8hg, ff_pars['slit_hg'] + 0.05)
    umv(s8vg, ff_pars['slit_vg'] + 0.05)
    shift_step_size = abs(ff_pars['shift_step_size'])
    if(ff_pars['nof_shifts'] < 1):
        print('The number of shifts is incorrect')
        return('The number of shifts is incorrect')
    offset_samtz_pos = ff_pars['samtz_cen'] - (ff_pars['nof_shifts'] - 1) * shift_step_size / 2
    print('Central samtz:' + str(ff_pars['samtz_cen']))
    if(offset_samtz_pos < ff_pars['zmot'].low_limit or offset_samtz_pos + (ff_pars['nof_shifts'] - 1) * shift_step_size > ff_pars['zmot'].high_limit):
        print('Exceed the limits of samtz')
        return('Exceed the limits of samtz')
    for iter_i in range(int(ff_pars['nof_shifts'])):
        umv(ff_pars['zmot'], iter_i * shift_step_size + offset_samtz_pos)
        newdataset(datasetname + str(iter_i + 1))
        finterlaced(diffrz, ff_pars['start_pos'], acq_size =ff_pars['step_size'], npoints = ff_pars['num_proj'], acq_time = ff_pars['exp_time'], mode = ff_pars['mode'])
    umv(ff_pars['zmot'], ff_pars['samtz_cen'])
    #user.cpm18_goto(cpm18_tuned)
    #umv(u22, u22_tuned)
    print('ff_zseries succeed')
    return('Succeed')

   
def pct_by_fscan(scanname, pars, scan_back = False):
    newdataset(scanname)
    umv(diffrz, pars['start_pos'], s7vg, pars['slit_vg'], s7hg, pars['slit_hg'])
    umv(s8vg, pars['slit_vg'] + 0.05, s8hg, pars['slit_hg'] + 0.05)
    if scan_back == False:
        fscan(diffrz, pars['start_pos'], pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
    else:
        fscan(diffrz, pars['start_pos'] + 360, -1 * pars['step_size'], pars['num_proj'], pars['exp_time'], scan_mode = pars['scan_mode'])
    print('Succeed !')
    
def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def loading(load, file_name, voltage_target = 120, voltage_ramprate = 0.1, tol = 0.1):
    initialize_stress_regul()
    print('Initialized stress regulation')
    print('Activating ramprate')
    regul_off(voltage_ramprate, voltage_target)
    print('Started voltage ramprate')
    with open(file_name, 'w') as f:
        # ramping up the load
        t0 = time.time()
        dt = 0
        exit_flag = False
        while exit_flag == False:
            dt = time.time() - t0
            #print('Waiting to reach the targe = {} N'.format(load))
            #print('Current load is {} N; Ramping time is {} s'.format(stress_adc.get_value(), dt))
            print(_get_human_time(),stress.position,stress_adc.get_value(),file=f)
            
            if stress.position >= 119.8:
                print('Reaching maximum !!!')
                regul_off(voltage_ramprate, 118)
                #initialize_stress_regul()
                exit_flag = True
                reach_max = True
            
            if np.abs(stress_adc.get_value() - load) <= tol:
                exit_flag = True

    initialize_stress_regul()

def unloading(load, file_name, voltage_target = 0, voltage_ramprate = 0.1, tol = 0.1):
    initialize_stress_regul()
    print('Initialized stress regulation')
    print('Activating ramprate')
    regul_off(voltage_ramprate, voltage_target)
    print('Started voltage ramprate')
    with open(file_name, 'w') as f:
        # ramping up the load
        t0 = time.time()
        dt = 0
        exit_flag = False
        while exit_flag == False:
            dt = time.time() - t0
            #print('Waiting to reach the targe = {} N'.format(load))
            #print('Current load is {} N; Ramping time is {} s'.format(stress_adc.get_value(), dt))
            print(_get_human_time(),stress.position,stress_adc.get_value(),file=f)
            
            if stress.position >= 119.8:
                print('Reaching maximum !!!')
                regul_off(voltage_ramprate, 118)
                #initialize_stress_regul()
                exit_flag = True
                reach_max = True
            
            if np.abs(stress_adc.get_value() - load) <= tol:
                exit_flag = True

    initialize_stress_regul()

def load_ramp_by_target(target = load, time_step=0.5,prefix_name = prefix_name):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    stress_regul.plot()
    file_name = prefix_name + '.txt'
    to_file = os.path.join(save_path, file_name)
    if to_file is not None:
        with open(to_file, 'a') as f:
            # write_to_file = to_file if to_file is not None else sys.stdout
            current_load = stress_adc.get_value()
            if abs(target - current_load) < 1:
                #print('target position is to close to current position - aborting !',
                #      file=write_to_file)
                print(_get_human_time(),stress.position,current_load,file=f)
                return
            if target > current_load:
                stress_increment = 0.1
                while (current_load < target):
                    mvr(stress, stress_increment)
                    current_load = stress_adc.get_value()
                    sleep(time_step)
                    print(_get_human_time(),stress.position,current_load,file=f)
            else:
                stress_increment = -0.1
                while (current_load > target):
                    mvr(stress, stress_increment)
                    current_load = stress_adc.get_value()
                    sleep(time_step)
                    print(_get_human_time(),stress.position,current_load,file=f)
    return
       
def load_ramp_by_target0(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    stress_regul.plot()
    if to_file is not None:
        with open(to_file, 'a') as f:
            # write_to_file = to_file if to_file is not None else sys.stdout
            current_load = stress_adc.get_value()
            if abs(target - current_load) < 1:
                #print('target position is to close to current position - aborting !',
                #      file=write_to_file)
                print(_get_human_time(),stress.position,current_load,file=f)
                return
            if target > current_load:
                stress_increment = 0.1
                while (current_load < target):
                    mvr(stress, stress_increment)
                    current_load = stress_adc.get_value()
                    sleep(time_step)
                    print(_get_human_time(),stress.position,current_load,file=f)
            else:
                stress_increment = -0.1
                while (current_load > target):
                    mvr(stress, stress_increment)
                    current_load = stress_adc.get_value()
                    sleep(time_step)
                    print(_get_human_time(),stress.position,current_load,file=f)
    return
    

def load_constant(f_target = f_target, prefix_name = prefix_name, save_path = save_path, time_step=0.5):
    current_load = stress_adc.get_value()
    if f_target > current_load:
        # file_name = 'load_to' + str(int(f_target)) + 'N.txt'
        file_name = prefix_name + '.txt'
        to_file = os.path.join(save_path, file_name)
        loading(f_target, to_file)
    else:
        # file_name = 'unload_to' + str(int(f_target)) + 'N.txt'
        file_name = prefix_name + '.txt'
        to_file = os.path.join(save_path, file_name)
        unloading(f_target, to_file)
    #if f_target > 1:
    #    load_ramp_by_target(f_target, time_step,to_file)
    #else:
    #    print('I do nothing because the load is smaller than 1 N')
    

# loading by changing set point
def loading_by_sp(load = f_target, prefix_name = prefix_name, ramp_rate = 0.5, tol = 0.25,save_path = save_path):
    file_name = os.path.join(save_path, prefix_name + '.txt')    
    initialize_stress_regul()
    print('Initialized stress regulation')
    print('Change force setpoint')
    stress_regul.ramprate = ramp_rate
    stress_regul.setpoint=load # [N]
    print('Started loading ...')
    with open(file_name, 'w') as f:
        # ramping up the load
        t0 = time.time()
        dt = 0
        exit_flag = False
        while exit_flag == False:
            dt = time.time() - t0
            #print('Waiting to reach the targe = {} N'.format(load))
            #print('Current load is {} N; Ramping time is {} s'.format(stress_adc.get_value(), dt))
            print(_get_human_time(),stress.position,stress_adc.get_value(),file=f)
            
            if stress.position >= 119.8:
                print('Reaching maximum !!!')
                regul_off(0.1, 118)
                #initialize_stress_regul()
                exit_flag = True
                reach_max = True
            
            if np.abs(stress_adc.get_value() - load) <= tol:
                exit_flag = True
    print('Reached the force targe {} N within a tolerance of {} N, exiting ...'.format(load, tol))
    initialize_stress_regul()


    

def diffty_test():
    for i in range(-50, 50):
        for tries in range(5):
            try:
                umv(diffty, diffty_0+i/1000)
                break
            except RunTimeError:
                sleep(3)
                print('axis error')
                diffty.reset_closed_loop()
                sleep(0.5)
            if tries == 4:
                raise Exception('diffty error')
        





  








