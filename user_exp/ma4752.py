import time
user_script_load("optics.py")


def check_cpm18(pos=9.005):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03:
        user.cpm18_goto(pos)



def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()


def run_diff_tomo():
    fscan2d.pars.latency_time = 0.0
    fscan2d.pars.scan_mode = "TIME"
    fscan2d.pars.fast_motor_mode='ZIGZAG'
    ACTIVE_MG.enable("eiger")
    ACTIVE_MG.enable("mca")
    for zpos in (70, 60, 50, 40, 30):
#        pause_for_refill(60)
        umv(pz, zpos)
        newdataset("DT4z%2d"%(zpos))
        fscan2d( dty, -75, 0.25, 601, rot, -1, 0.125, 2896, 0.005 )


def myfscan( *args, **kwds):
    for i in range(3):
        try:
            fscan(*args, **kwds)
        except Exception as e:
            elog_print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break

import numpy as np        
def my2d():
    fscan.pars.latency_time=0
    for i, ypos in enumerate(np.arange(-75,5.01,0.25)):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            myfscan(rot, -1, 0.125, 2896, 0.005, scan_mode='CAMERA' )
        else:
            myfscan(rot, 361, -0.125, 2896, 0.005, scan_mode='CAMERA' )
    
        

def run_diff_tomo_loop():

    ACTIVE_MG.enable("eiger")
    ACTIVE_MG.enable("mca")
    for zpos in (70, 60, 50, 40, 30):
#        pause_for_refill(60)
        umv(pz, zpos)
        newdataset("DTL1z%2d"%(zpos))
        # fscan2d( dty, -75, 0.25, 601, rot, -1, 0.125, 2896, 0.005 )
        my2d()
        
        

def run_diff_tomo_loop_cont():

    ACTIVE_MG.enable("eiger")
    ACTIVE_MG.enable("mca")
    for zpos in (60, 50, 40, 30):
#        pause_for_refill(60)
        umv(pz, zpos)
        newdataset("DTL2z%2d"%(zpos))
        # fscan2d( dty, -75, 0.25, 601, rot, -1, 0.125, 2896, 0.005 )
        my2d()
        
        
print("Load this script at least twice please!")
def my2d(y0):
    fscan.pars.latency_time=0
    for i, ypos in enumerate(np.arange(y0,5.01,0.25)):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            myfscan(rot, -1, 0.125, 2896, 0.005, scan_mode='CAMERA' )
        else:
            myfscan(rot, 361, -0.125, 2896, 0.005, scan_mode='CAMERA' )
    
        

def run_diff_tomo_loop():

    ACTIVE_MG.enable("eiger")
    ACTIVE_MG.enable("mca")
    for zpos in (80, 50, 20):
        umv(pz, zpos)
        newdataset("DTL3z%2d"%(zpos))
        my2d(-75)
 
def run_diff_tomo_loop_cont_sat():

    ACTIVE_MG.enable("eiger")
    ACTIVE_MG.enable("mca")
    newdataset("DTL2z50")
    my2d(y0=-41)
    newdataset("DTL2z20")
    umv( shtz , -0.325 - 0.03 )
    my2d(-100)

def run_diff_tomo_loop():

    ACTIVE_MG.enable("eiger")
    ACTIVE_MG.enable("mca")
    for zpos in (70, 75, 80, 85, 90, 95):
#        pause_for_refill(60)
        umv(pz, zpos)
        newdataset("DTL1z%2d"%(zpos))
        my2d(-130)
 
               
        
