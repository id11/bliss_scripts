import numpy as np

def goto_dctsetup():
    umv(ffdtx1, 400.00007)
    umv(ffdtz1, 200.000)
    umv(d1ty, 0, d1tz, -0.460) 
    umv(s8vg, 0.60000, s8hg, 1.40000, s7vg, 0.50000, s7hg, 1.40000)
    ACTIVE_MG.enable('frelon1:*')
    ACTIVE_MG.disable('frelon3*')
    
    
def goto_ffxrdsetup():
    umv(nfdtx, 192.20025)
    umv(d1ty, -80.00000)
    umv(ffdtz1, 0)
    umv(ffdtx1, 200.00007)
    umv(s8vg,0.2,s8hg,0.2)
    ACTIVE_MG.disable('frelon1*')
    ACTIVE_MG.enable('frelon3*')

def foam_scan():
    umv(diodez,38)
    
    
def insitu_scan(datasetname):
    newdataset(datasetname)
    sz_pos = np.linspace(-3.5, 0.5, 12)
    print('Scanning rotation at %i samtz positions'%len(sz_pos))
    for z in sz_pos:
        umv(samtz, z)
        finterlaced(diffrz,-50,2,50,0.15)  
              
def insitu_scan_continuous():
    count=0
    while count<10000:
        finterlaced(diffrz,-50,2,50,0.15)  
        count=count+1        
        
def dct_overnight():
    zcen = 0.14
    zrel = [-0.3,-0.1,0.1,0.3]
    for zr in zrel:
        umv(samtz, zcen+zr)
        user.tomo_by_fscan_dict('dct',dct_pars) 
        
def wire_middle():
    umv(diffty,24.25)
    umv(diffrz,0)
    umvr(samty,-0.6)
    startsamtz = samtz.position
    ascan(samtz,-4.75,-2.75,150,0.05)  #need to check range!!!
    topwire = cen()
    ascan(samtz,0.5,2.5,150,0.05)  #need to check range!!!
    botwire = cen()
    midwire = np.mean([topwire,botwire])
    print(topwire,botwire, midwire)
    print('Initial samtz position:', startsamtz,', new middle position:', midwire) 
    umv(samtz,midwire)
    umvr(samty,0.6)
    
    
    
    
import time

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan( *args, **kwds):
    for i in range(3):
        try:
            fscan(*args, **kwds)
        except Exception as e:
            print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break

def myfscan2d( *args, **kwds):
    for i in range(3):
        try:
            fscan2d(*args, **kwds)
        except Exception as e:
            print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break
      
       
def my2d():
    ycen = 24.25
    fscan.pars.latency_time=0
    yrange = 0.15
    ys = 0.0025
    exptime = 0.08
    astart = -92
    astop = 122
    astep = 1
    nframe = np.round((astop-astart) / astep).astype( int )
    ny = np.ceil( abs( yrange / ys ) ).astype( int )
    ypositions = np.linspace( -ny*ys, ny*ys, 2*ny+1 )
    for i, ypos in enumerate(ypositions):
        pause_for_refill(50)
        umv(diffty, ycen+ypos)
        finterlaced(diffrz, astart, astep, nframe, exptime, mode='ZIGZAG')
    
def layer_scans_overnight(samtzcen):
    zrel = np.arange(-80,82,20) #in microns!!
    print(zrel)
    for zr in zrel:
        print('layer_i_z%d' %(zr))
        print(samtzcen + (zr/1000))
        umv(samtz, samtzcen + (zr/1000)) #change zr from um to mm
        newdataset('layer_i_z%d' %(zr))
        my2d()
        
def layer_scans(samtzcen):
    zrel = np.arange(-60,62,20) #in microns!!
    print(zrel)
    for zr in zrel:
        print('layer_i_z%d' %(zr))
        print(samtzcen + (zr/1000))
        umv(samtz, samtzcen + (zr/1000)) #change zr from um to mm
        newdataset('layer_i_z%d' %(zr))
        my2d()
        
def my2d_sample2():
    ycen = 24.24353
    fscan.pars.latency_time=0
    yrange = 0.20 
    ys = 0.0025
    exptime = 0.08
    astart = -50
    astop = 210 #needs to be checked!!
    astep = 1
    nframe = np.round((astop-astart) / astep).astype( int )
    ny = np.ceil( abs( yrange / ys ) ).astype( int )
    ypositions = np.linspace( -ny*ys, ny*ys, 2*ny+1 )
    for i, ypos in enumerate(ypositions):
        pause_for_refill(50)
        umv(diffty, ycen+ypos)
        finterlaced(diffrz, astart, astep, nframe, exptime, mode='ZIGZAG')        
        
def layer_scans_sample2(samtzcen):
    diffty_airpad.on(60*24*5)
    zrel = np.arange(-60,62,20) #in microns!!  #need to change
    print(zrel)
    for zr in zrel:
        print('layer_i_z%d' %(zr))
        print(samtzcen + (zr/1000))
        umv(samtz, samtzcen + (zr/1000)) #changing zr from um to mm
        newdataset('layer_i_z%d' %(zr))
        my2d_sample2()  
        umv(diffty, 24.25)   
    diffty_airpad.off()  
    
    
def layer_scans_sample2_finish_scan(samtzcen):
    diffty_airpad.on(60*24*5)
    zrel = [60] #in microns!!  #need to change
    print(zrel)
    for zr in zrel:
        print('layer_i_z%d' %(zr))
        print(samtzcen + (zr/1000))
        umv(samtz, samtzcen + (zr/1000)) #changing zr from um to mm
        newdataset('layer_i_z%d' %(zr))
        my2d_sample2()  
        umv(diffty, 24.24353)   
    diffty_airpad.off()  
    
    
    
    
    
    
    
    
    
    
    
    
    
  
from bliss.common import plot as bl_plot
from scipy.optimize import curve_fit  
    
    
    

def sample_centre():
    rng=1
    npts=100 
    ctim=0.05 
    ctr='frelon3:roi_counters:roi1_sum'
    dataplot = bl_plot.plot(name='align COR')
    umv(diffrz, 0)
    x, y = fscan_interactive(samty, rng, npts, ctim, ctr)
    dataplot.add_curve( x, y)
    print("Select position at on samty")
    try:
        sy = dataplot.select_points(2)
    except:
        print('Try again')
        sy = dataplot.select_points(2)
    sycen = np.mean(sy[0][0],sy[1][0])
    print('samty cen',sycen)
    #umv(samty,sycen)
    umv(diffrz, 45)
    dataplot.clear_data()
    x, y = fscan_interactive(samtx, rng*1.5, npts*1.5, ctim, ctr)
    dataplot.add_curve( x, y)
    print("Select position at on samtx")
    try:
        sx = dataplot.select_points(2)
    except:
        print('Try again')
        sx = dataplot.select_points(2)
    sxcen = np.mean(sx[0][0],sx[1][0])
    print('samtx cen',sxcen)
    #umv(samtx, sxcen, diffrz, 0)
    


def fscan_interactive( mot, rng, npts, ctim, ctr):
    """ Uses dscan and returns data"""
    print(ctr)
    plotselect(ctr)
    plotinit(ctr)
    mstart = mot.position
    fscan( mot, mstart-rng, rng/npts*2, int(npts), ctim)
    lastscan = SCANS[-1]
    sleep(1)
    mot.sync_hard()
    A = lastscan.get_data( )
    m = A[ctr] < 1e9
    y = A[ctr][m]
    x = A[mot.name][m]
    mv(mot, mstart)
    return x, y

