
import os
user_script_load('makeesperanto')


euro_loop.ramprate = 10
euro_loop.deadband = 2

def thu_lunch():
    newdataset("200K_fast1")
    fscan(rot, 0, 0.5, 720, 0.025 )
    user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
    
    newdataset("200K_3minfine")
    fscan(rot, 0, 0.05, 7200, .025 )
    user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  

    newdataset("200K_3min")
    fscan(rot, 0, 0.5, 720, .25 )
    user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  

    newdataset("200K_30min")
    fscan(rot, 0, 0.5, 720, 2.5 )
    user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)   
    
    newdataset("200K_15min")
    fscan(rot, 0, 0.1, 3600, .25)
    user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  




def fri_night():
    t = 200
    while t >= 130:
        cryostream.rampto(t,360) 
        newdataset('%dK'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t-10
    t = 130
    while t > 124:
        cryostream.rampto(t,360) 
        newdataset('%dK'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t-1
    t=125
    while t <= 130:
        cryostream.rampto(t,360) 
        newdataset('%dK'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t+1
    t = 140
    while t <= 200:
        cryostream.rampto(t,360) 
        newdataset('%dK'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t+10
    
def fri_dinner():
    newdataset("200K_15min_0p1height")
    fscan(rot, 0, 0.1, 3600, .25)
    s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
    os.system("xterm -e sh "+s+" &")
    
    
    
def sat_afternoon():
    t = 40
    while t <= 300:
        umv(euro_loop_axis, t)
        newdataset('%dC'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t+10
    t = 350 
    while t <= 600:
        umv(euro_loop_axis,t)
        newdataset('%dC'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t+50
    umv(shtz,1.22)
    newdataset("bkg_600C")
    fscan(rot, 0, 0.1, 3600, .25)
    s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
    os.system("xterm -e sh "+s+" &")
    umv(euro_loop_axis,25)
    umv(shtz,1.52)
    newdataset("25C_cooling")
    fscan(rot, 0, 0.1, 3600, .25)
    s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)
    os.system("xterm -e sh "+s+" &")
    # goes to 50 without waiting
    #   euroloop.setpoint = 50
    #
    #umv( euroloop_axis, 50 )  # supposed to go there and wait to arrive
    

def sat_night():
    t = 260 
    while t <= 600:
        umv(euro_loop_axis,t)
        newdataset('%dC'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t+10
    umv(shtz,1.22)
    newdataset("bkg_600C")
    fscan(rot, 0, 0.1, 3600, .25)
    s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
    os.system("xterm -e sh "+s+" &")
    umv(euro_loop_axis,25)
    umv(shtz,1.52)
    newdataset("25C_cooling")
    fscan(rot, 0, 0.1, 3600, .25)
    s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)
    os.system("xterm -e sh "+s+" &")
    
def sun_morn():
    t = 40
    while t <= 400:
        umv(euro_loop_axis, t)
        newdataset('%dC'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t+10
    t = 425
    while t <= 600:
        umv(euro_loop_axis,t)
        newdataset('%dC'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t+25
    umv(euro_loop_axis,25)
    newdataset("25C_cooling")
    fscan(rot, 0, 0.1, 3600, .25)
    s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)
    os.system("xterm -e sh "+s+" &")
#user_script_load("hc4358.py")
#user.
    
def sun_night():
    t = 50
    while t <= 150:
        umv(euro_loop_axis, t)
        newdataset('%dC'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t+50
    t = 160
    while t <= 600:
        umv(euro_loop_axis,t)
        newdataset('%dC'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t+10
    umv(euro_loop_axis,25)
    newdataset("25C_cooling")
    fscan(rot, 0, 0.1, 3600, .25)
    s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)
    os.system("xterm -e sh "+s+" &")
    
def last_night():
    t = 50
    while t <= 200:
        umv(euro_loop_axis, t)
        newdataset('%dC'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t+50
    t = 220
    while t <= 640:
        umv(euro_loop_axis,t)
        newdataset('%dC'%(t))
        fscan(rot, 0, 0.1, 3600, .25)
        s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)  
        os.system("xterm -e sh "+s+" &")
        t=t+20
    umv(euro_loop_axis,25)
    newdataset("25C_cooling")
    fscan(rot, 0, 0.1, 3600, .25)
    s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)
    os.system("xterm -e sh "+s+" &")
    umvr(shtz,-0.5)
    newdataset("bkg25C_cooling")
    fscan(rot, 0, 0.1, 3600, .25)
    s = user.makesperanto( user.wvln(), 1054, 1124, frelx.position, 1, now=False)
    os.system("xterm -e sh "+s+" &")
