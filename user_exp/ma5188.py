import time

def vertical_scan(ctime,start_hz,end_hz,target_temp,ramprate = 10,time_palier = 1800):
    nanodacpool.ramprate = ramprate
    nanodacpool.setpoint = target_temp
    newdataset('vertical_scan_hz_%s_%s'%(start_hz,end_hz))
    while nanodacpool.is_ramping():
        start_time = time.time()
        ascan(hz,start_hz,end_hz,9,ctime)
        umv(hz,start_hz)
        sleep(-time.time()+start_time+30)
    time_after_ramp = time.time()
    while (time.time() - time_after_ramp) < time_palier:
        start_time = time.time()
        ascan(hz,start_hz,end_hz,9,ctime)
        umv(hz,start_hz)
        sleep(-time.time()+start_time+30)
    nanodacpool.ramprate = 25
    nanodacpool.setpoint = 25
    sleep(30)
    while nanodacpool.is_ramping():
        start_time = time.time()
        ascan(hz,start_hz,end_hz,9,ctime)
        umv(hz,start_hz)
        sleep(-time.time()+start_time+30)
