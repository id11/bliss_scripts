
import numpy as np
import time


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan(*a,**k):
    print(a,k)
    try:
        fscan(*a,**k)
    except: # pretty hopeless but why not ? 
        fscan(*a,**k)

def myumv(*a,**k):
    print(a,k)
    umv(*a,**k)
        
#Wlayer_dct_002 scan 87 might have some issues
#same for Top layer scan 468, 795,796
#Midlayer 654

W_in_Wlayer_xyz = [1.25739, -0.03201, 0.0285]

Wlayer_xyz = [1.25739+0.102, -0.03201+0.102, 0.0285]

other_layer_xyz = [1.25739+0.227, -0.03201+0.43, -0.8085+0.0924]

sh_cor_top_xyz = [1.5607,0.35167,-0.8085]

Mid_layer_xyz = [1.48439, 0.52427, -0.7131]   


def overnightscan():
    #newdataset('Wlayer_pct')
    #umv(shtx, Wlayer_xyz[0], shty, Wlayer_xyz[1], shtz, Wlayer_xyz[2])
    #layer_360(77)
    
    newdataset('Toplayer_pct2')
    umv(shtx, other_layer_xyz[0], shty, other_layer_xyz[1], shtz, other_layer_xyz[2])
    layer_360(100)

def overnightscan2():
    newdataset('Midlayer')
    umv(shtx, Mid_layer_xyz[0], shty, Mid_layer_xyz[1], shtz, Mid_layer_xyz[2])
    layer_360(200)

def finish_Toplayer():
    y0 = 0.0  # centre of rotation
    width = 105.8
    step  = 0.4
    ypos = np.arange(-width, -98, step)
    print(ypos)
    astep = 0.02
    anrange = 360.
    speed = 10
    eiger.camera.photon_energy=43370
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')



#Abis sample
Abis_Top_xyz = [1.25154, 0.79308, -1.07038]

Abis_Layer_xyz = [1.25154, 0.79308, -1.07038+0.097]

def overnightscan3():
    #newdataset('Layer_fine')
    #umv(shtx, Abis_Layer_xyz[0], shty, Abis_Layer_xyz[1], shtz, Abis_Layer_xyz[2])
    layer_180f(325,1)

    umvr(shnee,2)
    newdataset('Layer_coarse')
    layer_180c(800,2)

def layer_180f(mywidth, mystep):
    y0 = 0.0  # centre of rotation
    width = mywidth
    step  = mystep
    ypos = np.arange(-302, width+step/10, step)
    astep = 0.05
    anrange = 180.
    speed = 10
    eiger.camera.photon_energy=43370
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        try:
            myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        except:
            ct()
            myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        try:
            myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        except:
            ct()
            myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')

def layer_180c(mywidth, mystep):
    y0 = 0.0  # centre of rotation
    width = mywidth
    step  = mystep
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.05
    anrange = 180.
    speed = 10
    eiger.camera.photon_energy=43370
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        try:
            myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        except:
            ct()
            myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        try:
            myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        except:
            ct()
            myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    


def layer_360(mywidth):
    y0 = 0.0  # centre of rotation
    width = mywidth
    step  = 0.8
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.02
    anrange = 360.
    speed = 8
    eiger.camera.photon_energy=43370
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        try:
            myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        except:
            ct()
            myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        try:
            myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        except:
            ct()
            myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    

def testlayer():
    y0 = 0.0  # centre of rotation
    astep = 0.05
    anrange = 180.
    speed = 10
    eiger.camera.photon_energy=43370
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.enable("eiger:image")
    myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
