import numpy as np

def goto_pctsetup():
    umv(ffdtx1, 400)
    umv(ffdtz1, 200)
    umv(d3ty, 0, d3tz, 0) 
    umv(s8vg, 3, s8hg, 3 )
    ACTIVE_MG.enable('marana:i*')
    ACTIVE_MG.enable('marana:r*')
    ACTIVE_MG.disable('frelon3:*')
    
    
def goto_ffxrdsetup():
    umv(d3ty, 180)
    umv(ffdtz1, 0)
    umv(ffdtx1, 200)
    #umv(s8vg,0.02,s8hg,0.02)
    ACTIVE_MG.disable('marana:*')
    ACTIVE_MG.enable('frelon3:i*')
    ACTIVE_MG.enable('frelon3:r*')
    
def insitu_scan(datasetname):
    newdataset(datasetname)
    sz_pos = np.linspace(-3.5, 0.5, 12)
    print('Scanning rotation at %i samtz positions'%len(sz_pos))
    for z in sz_pos:
        umv(samtz, z)
        finterlaced(diffrz,-50,2,50,0.15)  
              
def insitu_scan_continuous():
    count=0
    while count<10000:
        finterlaced(diffrz,-50,2,50,0.15)  
        count=count+1        
        
def dct_overnight():
    zcen = 0.14
    zrel = [-0.3,-0.1,0.1,0.3]
    for zr in zrel:
        umv(samtz, zcen+zr)
        user.tomo_by_fscan_dict('dct',dct_pars) 
        
def wire_middle():
    umv(diffty,24.25)
    umv(diffrz,0)
    umvr(samty,-0.6)
    startsamtz = samtz.position
    ascan(samtz,-4.75,-2.75,150,0.05)  #need to check range!!!
    topwire = cen()
    ascan(samtz,0.5,2.5,150,0.05)  #need to check range!!!
    botwire = cen()
    midwire = np.mean([topwire,botwire])
    print(topwire,botwire, midwire)
    print('Initial samtz position:', startsamtz,', new middle position:', midwire) 
    umv(samtz,midwire)
    umvr(samty,0.6)
    
    
    
    
import time

def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan( *args, **kwds):
    for i in range(3):
        try:
            fscan(*args, **kwds)
        except Exception as e:
            print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break

def myfscan2d( *args, **kwds):
    for i in range(3):
        try:
            fscan2d(*args, **kwds)
        except Exception as e:
            print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break
      
       
def tdxrd_scan(): #TO MODIFY
    ycen = 13.61 #13.657 for concang5
    fscan.pars.latency_time=0
    yrange = 0.35
    ys = 0.02
    exptime = 0.08
    astart = 135
    astop = 135+180
    astep = 1
    nframe = np.round((astop-astart) / astep).astype( int )
    ny = np.ceil( abs( yrange / ys ) ).astype( int )
    ypositions = np.linspace( -ny*ys, ny*ys, 2*ny+1 )
    for i, ypos in enumerate(ypositions):
        pause_for_refill(50)
        umv(diffty, ycen+ypos)
        finterlaced(diffrz, astart, astep, nframe, exptime, mode='ZIGZAG')
    
def layer_scans(samtzcen):
    zrel = np.arange(380,-20,-20) #in microns!!
    print(zrel)
    for zr in zrel:
        print('layer_i_z%d' %(zr))
        print(samtzcen + (zr/1000))
        umv(samtz, samtzcen + (zr/1000)) #change zr from um to mm
        newdataset('tdxrd_load_08_01_layer_z%d' %(zr))
        tdxrd_scan()


def tdxrd_scan_ten_um(): #TO MODIFY
    ycen = 13.61 #13.657 for concang5
    fscan.pars.latency_time=0
    yrange = 0.25
    ys = 0.01
    exptime = 0.08
    astart = 135
    astop = 135+180
    astep = 1
    nframe = np.round((astop-astart) / astep).astype( int )
    ny = np.ceil( abs( yrange / ys ) ).astype( int )
    ypositions = np.linspace( -ny*ys, ny*ys, 2*ny+1 )
    for i, ypos in enumerate(ypositions):
        pause_for_refill(50)
        umv(diffty, ycen+ypos)
        finterlaced(diffrz, astart, astep, nframe, exptime, mode='ZIGZAG')
    
def layer_scans_ten_um(samtzcen):
    zrel = np.arange(190,-10,-10) #in microns!!
    print(zrel)
    for zr in zrel:
        print()
        print('layer_i_z%d' %(zr))
        print(samtzcen + (zr/1000))
        umv(samtz, samtzcen + (zr/1000)) #change zr from um to mm
        newdataset('tdxrd_load_05_01_layer_z%d' %(zr))
        tdxrd_scan_ten_um()

def single_layer_tdxrd_ten_um(): #TO MODIFY
    ycen = 13.61 #13.657 for concang5
    fscan.pars.latency_time=0
    yrange = 1.1
    ys = 0.01
    exptime = 0.08
    astart = 135
    astop = 135+180
    astep = 1
    nframe = np.round((astop-astart) / astep).astype( int )
    ny = np.ceil( abs( yrange / ys ) ).astype( int )
    ypositions = np.linspace( -ny*ys, ny*ys, 2*ny+1 )
    for i, ypos in enumerate(ypositions):
        pause_for_refill(50)
        umv(diffty, ycen+ypos)
        finterlaced(diffrz, astart, astep, nframe, exptime, mode='ZIGZAG')

def single_layer_full_width_scan_ten_um(samtzcen):
    zrel = [0] #in microns!!
    print(zrel)
    for zr in zrel:
        print()
        print('layer_i_z%d' %(zr))
        print(samtzcen + (zr/1000))
        umv(samtz, samtzcen + (zr/1000)) #change zr from um to mm
        newdataset('tdxrd_load_05_01_full_width_layer_z%d' %(zr))
        single_layer_tdxrd_ten_um()

#def layer_scans(samtzcen):
#    zrel = np.arange(-60,62,20) #in microns!!
#    print(zrel)
#    for zr in zrel:
#        print('layer_i_z%d' %(zr))
#        print(samtzcen + (zr/1000))
#        umv(samtz, samtzcen + (zr/1000)) #change zr from um to mm
#        newdataset('layer_i_z%d' %(zr))
#       my2d()
        
def my2d_sample2():
    ycen = 24.24353
    fscan.pars.latency_time=0
    yrange = 0.20 
    ys = 0.0025
    exptime = 0.08
    astart = -50
    astop = 210 #needs to be checked!!
    astep = 1
    nframe = np.round((astop-astart) / astep).astype( int )
    ny = np.ceil( abs( yrange / ys ) ).astype( int )
    ypositions = np.linspace( -ny*ys, ny*ys, 2*ny+1 )
    for i, ypos in enumerate(ypositions):
        pause_for_refill(50)
        umv(diffty, ycen+ypos)
        finterlaced(diffrz, astart, astep, nframe, exptime, mode='ZIGZAG')        
        
def layer_scans_sample2(samtzcen):
    diffty_airpad.on(60*24*5)
    zrel = np.arange(-60,62,20) #in microns!!  #need to change
    print(zrel)
    for zr in zrel:
        print('layer_i_z%d' %(zr))
        print(samtzcen + (zr/1000))
        umv(samtz, samtzcen + (zr/1000)) #changing zr from um to mm
        newdataset('layer_i_z%d' %(zr))
        my2d_sample2()  
        umv(diffty, 24.25)   
    diffty_airpad.off()  
    
    
def layer_scans_sample2_finish_scan(samtzcen):
    diffty_airpad.on(60*24*5)
    zrel = [60] #in microns!!  #need to change
    print(zrel)
    for zr in zrel:
        print('layer_i_z%d' %(zr))
        print(samtzcen + (zr/1000))
        umv(samtz, samtzcen + (zr/1000)) #changing zr from um to mm
        newdataset('layer_i_z%d' %(zr))
        my2d_sample2()  
        umv(diffty, 24.24353)   
    diffty_airpad.off()  
    
    
    
    
    
    
    
    
    
    
    
    
    
  
from bliss.common import plot as bl_plot
from scipy.optimize import curve_fit  
    
    
    

def sample_centre():
    rng=1
    npts=100 
    ctim=0.05 
    ctr='frelon3:roi_counters:roi1_sum'
    dataplot = bl_plot.plot(name='align COR')
    umv(diffrz, 0)
    x, y = fscan_interactive(samty, rng, npts, ctim, ctr)
    dataplot.add_curve( x, y)
    print("Select position at on samty")
    try:
        sy = dataplot.select_points(2)
    except:
        print('Try again')
        sy = dataplot.select_points(2)
    sycen = np.mean(sy[0][0],sy[1][0])
    print('samty cen',sycen)
    #umv(samty,sycen)
    umv(diffrz, 45)
    dataplot.clear_data()
    x, y = fscan_interactive(samtx, rng*1.5, npts*1.5, ctim, ctr)
    dataplot.add_curve( x, y)
    print("Select position at on samtx")
    try:
        sx = dataplot.select_points(2)
    except:
        print('Try again')
        sx = dataplot.select_points(2)
    sxcen = np.mean(sx[0][0],sx[1][0])
    print('samtx cen',sxcen)
    #umv(samtx, sxcen, diffrz, 0)
    


def fscan_interactive( mot, rng, npts, ctim, ctr):
    """ Uses dscan and returns data"""
    print(ctr)
    plotselect(ctr)
    plotinit(ctr)
    mstart = mot.position
    fscan( mot, mstart-rng, rng/npts*2, int(npts), ctim)
    lastscan = SCANS[-1]
    sleep(1)
    mot.sync_hard()
    A = lastscan.get_data( )
    m = A[ctr] < 1e9
    y = A[ctr][m]
    x = A[mot.name][m]
    mv(mot, mstart)
    return x, y

