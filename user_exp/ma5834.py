

# ma5834 ...


def ff_out():
    sheh3.close()
    umv(ffdtx1, 350)
    umv(ffdtz1, 300)
    ACTIVE_MG.disable('frelon3*')
    
def ff_in():
    marana_out()
    sheh3.close()
    umv(ffdtz1, 0)
    umv(ffdtx1, 150) # 165.75
    ACTIVE_MG.enable('frelon3')


def marana_out():
    umv(nfdtx,100)
    umv(d3ty,130)
    ACTIVE_MG.disable('marana3*')
    
def marana_in():
    ff_out()
    umv(d3ty, 0, d3tz, 0)
    umv(nfdtx,100)
    ACTIVE_MG.enable('marana3')


def senscurr( npts = 10, yrange=0.15, count_time = .1, voltage=1 ):
    kcurr.source_range = 500
    kcurr.auto_range = True
    kcurr.source_value = 0
    kcurr.source_enable = 1    # ?
    
    ACTIVE_MG.enable('frelon3')
    ACTIVE_MG.enable('kcurr')
    
    # measure at 0 volts at one position in space
    # loopscan(  npts, count_time )
    
    # scan in space: dscan = relative to current position
    dscan( samty, -yrange, yrange, npts, count_time )
    
    # measure at requested 
    kcurr.source_value = voltage
    # loopscan(  npts, count_time )
    dscan( samty, -yrange, yrange, npts, count_time )
    
    
def tuesdaylunch():
    for volts in (0, -10, -50, -100, -200):
        newdataset(f'bias{volts}V_10um')
        kcurr.source_value = volts
        fscan(samty, -0.1, 0.002, 150, 0.1)
        
def tuesdaydinner():
    for volts in range(0,201,50):
        newdataset(f'bias{volts}V_10um_20')
        kcurr.source_value = volts
        if volts > 0:
            sleep(300)
        fscan(samty, -0.15, 0.001, 0.3/0.001, 0.1)
    for volts in range(0,-201,-50):
        newdataset(f'bias{volts}V_10um_21')
        kcurr.source_value = volts
        sleep(300)
        fscan(samty, -0.15, 0.001, 0.3/0.001, 0.1)    
    volts = 0
    newdataset(f'bias{volts}V_10um_22')
    kcurr.source_value = volts
    sleep(300)
    fscan(samty, -0.15, 0.001, 0.3/0.001, 0.1)
    
    
def wednesdaylunch():
    for volts in range(0,201,50):
        newdataset(f'bias{volts}V_10um')
        kcurr.source_value = volts
        if volts > 0:
            sleep(120)
        fscan(samty, -0.15, 0.001, 0.3/0.001, 0.1)
    for volts in range(0,-201,-50):
        newdataset(f'bias{volts}V_10um')
        kcurr.source_value = volts
        sleep(120)
        fscan(samty, -0.15, 0.001, 0.3/0.001, 0.1)    
    volts = 0
    newdataset(f'bias{volts}V_10um_22')
    kcurr.source_value = volts
    sleep(120)
    fscan(samty, -0.15, 0.001, 0.3/0.001, 0.1)
    
    
    
def Rafikul():
    for volts in range(0,100,10):
        newdataset(f'bias{volts}V_10um_B-final2')
        kcurr.source_value = volts
        if volts > 0:
            sleep(60)
        fscan(samty, 0, 0.01, 2.3, 0.01)
    for volts in range(0,-100,-10):
        newdataset(f'bias{volts}V_10um_B-final2')
        kcurr.source_value = volts
        sleep(60)
        fscan(samty, 0, 0.01, 2.3, 0.01)    
    volts = 0
    newdataset(f'bias{volts}V_10um_B-final2')
    kcurr.source_value = volts
    sleep(60)
    fscan(samty, 0, 0.01, 2.3, 0.01)



def wednesdaydinner():
    for volts in range(0,201,200):
        newdataset(f'bias{volts}V_10um')
        kcurr.source_value = volts
        if volts > 0:
            sleep(300)
        fscan(samty, -0.2, 0.01, 0.4/0.01, 0.1)
    for volts in range(0,-201,-200):
        newdataset(f'bias{volts}V_10um')
        kcurr.source_value = volts
        sleep(300)
        fscan(samty, -0.2, 0.01, 0.4/0.01, 0.1)    
    volts = 0
    newdataset(f'bias{volts}V_10um_22')
    kcurr.source_value = volts
    sleep(300)
    fscan(samty, -0.2, 0.01, 0.4/0.01, 0.1)
    

def measure_rack():
    #  samty   samtx   diffty     
    y10 = -19.963; x10  = -0.09 #  13.966
    y0=  25.173  ; x0 =  1.57 #  13.963
    dy = (y10-y0)/9
    dx = (x10-x0)/9
    for i in range(10):
        print( f'samty {y0 + i * dy} samtx {x0 + i * dx}' )
        umv(samty, y0 + i * dy, samtx, x0 + i * dx)
        newdataset(f'{i}')
        fscan(diffrz, -30, 1, 60, 0.5,)
        fscan(diffrz, 180-30, 1, 60, 0.5,)
    newdataset('nextalign')
    


def pdf_ff_in():
    sheh3.close()
    umv(d3ty, 130, d3tz, 200)
    umv(ffdtz1, 42.0972)
    umv(ffdtx1, 130)
    
def pdf_pct_in():
    sheh3.close()
    umv(ffdtx1 , 400 )
    umv(ffdtz1 , 342.0972)
    umv(d3ty, 0, d3tz, 0)
    
    
def heat1():
    nanodac3.setpoint=360
    while 1: 
        fscan( diffrz, -90, 1, 180, 0.1)


        
def quench1():
    umv(furnace_z, 130)
    while 1: 
        nanodac3.setpoint=30
        fscan( diffrz, -90, 1, 180, 0.1)
        
        
def reheat1():
    nanodac3.setpoint=360
    umv(furnace_z, 0)
    while 1: 
        fscan( diffrz, -90,  1, 180, 0.1)
        fscan( diffrz,  90, -1, 180, 0.1)
        
        
def TF_scan():
    umv(diffry, -2)
    newdataset(f'bNC_IncAngle-1_10um')   
    dscan(samty, 0,0.2, 20, 0.01)

    umv(diffry, -1)
    newdataset(f'bNC_IncAngle-1_10um')   
    dscan(samty, 0, 0.2, 20, 0.01)
    
    umv(diffry, -0.5)
    newdataset(f'bNC_IncAngle-0.5_10um')   
    dscan(samty, 0, 0.2, 20, 0.01)
    
    umv(diffry, -0.3)
    newdataset(f'bNC_IncAngle-0.3_10um')   
    dscan(samty, 0, 0.2, 20, 0.01)
    
    umv(diffry, -0.1)
    newdataset(f'bNC_IncAngle-0.1_10um')   
    dscan(samty, 0, 0.2, 20, 0.01)
    
    umv(diffry, 0)
    newdataset(f'bNC_IncAngle0_10um')   
    dscan(samty, 0, 0.2, 20, 0.01)


def elecVert(height):
    y0 = 10.517
    z0 = 1.5
    y1 = 10.616
    z1 = -2
    pos = (height - z0)/(z1-z0)
    ypos = (y1-y0)*pos + y0
    umv(samtz, height, samty, ypos )
    
    
def FridayFun():
    newdataset(f'VertPreBiasV_800um_10um_1')
    dscan(samtz, -0.02, 0.02, 3, 0.1) 
    for volts in range(0,301,300):
        newdataset(f'Vertbias{volts}V_800um_10um_1')
        kcurr.source_value = volts
        if volts > 0:
            sleep(600)
        dscan(samtz, -0.02, 0.02, 3, 0.1)   
    volts = 0
    newdataset(f'Vert_bias{volts}V_800um_10um_1')
    sleep(100)
    kcurr.source_value = volts
    fscan(samtz, -0.02, 0.02, 3, 0.1)

    

