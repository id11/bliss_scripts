import numpy as np
import time
from bliss.common import cleanup

attrz = config.get('attrz')
atty = config.get('atty')
tfoh1 = config.get('tfoh1')


DIFFTY_CEN = 13.792


def safe_move_diffty(pos):
    for attempt in range(5):
        try:
            umv(diffty, pos)
            break
        except KeyboardInterrupt:
            raise
        except Exception as e:
            print(e)
            # elog_add()  # stupid stupid idea!
            
            
        diffty_airpad.on(60*24*5)
        time.sleep(1)
        diffty.reset_closed_loop()

        

def pause_for_refill(t):
    # FIXME: see how to put this in with eh3check
    pass
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)


def all_dets_out():
    sheh3.close()
    umv(ffdtz1, 200, d3ty, 150)
    print("Moved all detectors out")


def maranain():
    ACTIVE_MG.enable("marana3") 
    ACTIVE_MG.disable("frelon*")
    sheh3.close()
    umv(ffdtz1, 200, d3ty, 0)
    
    # pull out attenuator
    umv(attrz,0,atty,-10)
    
    # set slits to 1.5 x 1.5 for tomo
    umv(s7hg,1.5,s7vg,1.5)
    
    # ensure no lenses upstream
    tfoh1.set(0, 'Be')
    
    print("Ready to collect tomo data") 


def frelonin():
    ACTIVE_MG.disable("marana3") 
    ACTIVE_MG.enable("frelon3") 
    sheh3.close()
    umv(d3ty,150, ffdtz1, 0)
    print("Ready to collect diffraction data")


def collect_tomo(dset_prefix="PCT",
                 z_start=None,
                 y_start=DIFFTY_CEN):
    # check we are in measurement position
    maranain()
    
    # pull out attenuator
    umv(attrz,0,atty,-10)
    
    # set slits to 1.5 x 1.5 for tomo
    umv(s7hg,1.5,s7vg,1.5)
    
    # ensure no lenses upstream
    tfoh1.set(0, 'Be')
    
    # open the shutter
    sheh3.open()
    
    if z_start is None:
        z_start = samtz.position
  
    z_shift = 0.5
    y_shift = 0.5

    # move horizontally and vertically
    diffty.reset_closed_loop()
    umv(diffty, y_start - y_shift, samtz, z_start - z_shift)
    
    fulltomo.full_turn_scan(dset_prefix + "_z0")
    
    # then just move vertically
    umv(samtz, z_start + z_shift)
    
    # try to stop marana crashing
    newdataset('marana_anticrash')
    time.sleep(10)
    ct(0.04)
    time.sleep(10)
    
    fulltomo.full_turn_scan(dset_prefix + "_z1")
    
    diffty.reset_closed_loop()
    print("Finished, moving samtz and diffty back to start")
    umv(samtz, z_start, diffty, y_start)


def collect_box_beam(dsetname='FF'):
    # check we are in measurement position
    frelonin()
    
    # put in attenuator
    umv(attrz,-10.5,atty,0)
    
    # set slits to 2 x 2 for box beam
    umv(s7hg,2,s7vg,2)

    # set upstream lenses
    tfoh1.set(0, 'Be')
    
    # open the shutter
    sheh3.open()
    
    # set dataset name
    newdataset(dsetname)
    
    # collect box beam
    finterlaced(diffrz, 0, 0.1, 360/0.1, 0.09, mode='ZIGZAG')


def collect_letterbox_beam_zseries(z_start, dset_prefix='FF_lbox_z'):
    # check we are in measurement position
    frelonin()
    
    # put in attenuator
    umv(attrz,-12,atty,0)
    
    # set slits to 2 x 0.1 for letterbox beam
    umv(s7hg,2,s7vg,0.1)

    # set upstream lenses
    tfoh1.set(0, 'Be')
    
    # open the shutter
    sheh3.open()
    
    zpositions = np.array([-0.8, -0.4, 0.0, 0.4, 0.8])
    # zpositions = np.array([0.0])  # dummy!


    print(zpositions)
    for zpos in zpositions:
        zpos_str = str(zpos)[0:7].replace(".","p").replace('-','min')
        print(f'layer_{zpos_str}')
        print(z_start + zpos)
        umv(samtz, z_start + zpos)
        
        newdataset(f'{dset_prefix}_{zpos_str}')
        finterlaced(diffrz, 0, 0.1, 360/0.1, 0.09, mode='ZIGZAG')
    
    umv(samtz, z_start)


def _tdxrd_scan(y_start=DIFFTY_CEN,
                yrange = 1.0,
                ys = 0.025,
                # ys = 0.80,   # Dummy!
                exptime = 0.09,
                astart = 0,
                astop = 180,
                astep = 0.9):
    nframe = np.round((astop-astart) / astep).astype( int )
    ny = np.ceil( abs( yrange / ys ) ).astype( int )
    ypositions = np.linspace( -ny*ys, ny*ys, 2*ny+1 )
    diffty_airpad.on( 60*24*5 )
    with cleanup.cleanup( diffrz, restore_list=(cleanup.axis.POS,) ):
        for i, ypos in enumerate(ypositions):
            pause_for_refill(50)
            print(y_start + ypos)
            
            
            # diffty.reset_closed_loop()
            # umv(diffty, y_start + ypos)
            safe_move_diffty(y_start + ypos)
            finterlaced(diffrz, astart, astep, nframe, exptime, mode='ZIGZAG')
    
    # diffty.reset_closed_loop()
    # umv(diffty, y_start)
    safe_move_diffty(y_start)
    diffty_airpad.off()


def _layer_scans(z_start,
                y_start=DIFFTY_CEN,
                zrange=0.2,
                zs=0.025
                # zs=0.2  # Dummy!
                ):
    
    nz = np.ceil( abs( zrange / zs ) ).astype( int )
    zpositions = np.linspace( -nz*zs, nz*zs, 2*nz+1 )
                
    print(zpositions)
    for zpos in zpositions:
        zpos_str = str(zpos)[0:7].replace(".","p").replace('-','min')
        print(f'layer_{zpos_str}')
        print(z_start + zpos)
        umv(samtz, z_start + zpos)
        newdataset(f'scan_layer_{zpos_str}')
        # newdataset(f'test_scan_layer_{zpos_str}')  # dummy!
        _tdxrd_scan(y_start=y_start)
    
    umv(samtz, z_start)


def collect_scanning(z_start, y_start=DIFFTY_CEN):
    # check we are in measurement position
    frelonin()
    
    # pull out attenuator
    umv(attrz,0,atty,-10)
    
    # set slits to 25 um
    umv(s7hg,0.025,s7vg,0.025)
    
    # set upstream lenses
    tfoh1.set(8, 'Be')
    
    # open the shutter
    sheh3.open()
    
    # do a layer scan
    _layer_scans(z_start, y_start)


def measurement_loop(z_start, y_start=DIFFTY_CEN, loop_start=None):
    n_loops = 500
    sleep_time_sec = 10
    
    umv(samtz, z_start)
    
    if loop_start is not None:
        loop_range = range(loop_start, n_loops)
    else:
        loop_range = range(n_loops)
    
    for loop in loop_range:
        print(f"On loop {loop}")
    
        # collect box beam
        collect_box_beam(dsetname=f"FF_{loop}")
        
        # collect tomo
        collect_tomo(z_start=z_start, y_start=y_start, dset_prefix=f"PCT_{loop}")
        
        print("******")
        print("******")
        print("******")
        print("******")
        print("******")
        print(f"Sleeping for {sleep_time_sec} seconds, here is a good time to interrupt!")
        print("******")
        print("******")
        print("******")
        print("******")
        print("******")
        
        time.sleep(sleep_time_sec)
