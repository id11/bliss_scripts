
import numpy as np
import time
user_script_load("align_cor.py") 


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan(*a,**k):
    print(a,k)
    try:
        fscan(*a,**k)
    except: # pretty hopeless but why not ?
        print("fscan failed, try again in a minute")
        time.sleep(60)
        fscan(*a,**k)

def myumv(*a,**k):
    print(a,k)
    umv(*a,**k)
        

        
def layer_large_test():
    y0 = 0.0  # centre of rotation
    width = 4.
    step  = 0.3
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.05
    anrange = 360. #this is the max of range
    speed = 25.
    eiger.camera.photon_energy=43468.9
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')   #0 is currently start angle. change to 100
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')   #anrange is the max rotation position
    eiger.camera.auto_summation="ON"


def ref_sample(height):
    umv(dty, 0, pz, 52.5, rot, 0)
    ACTIVE_MG.enable("mca")
    ACTIVE_MG.disable("eiger:image")
    plotinit("Pt_det0")
    plotselect("Pt_det0")
    dscan(pz, -2 , 2, 100, .1)
    #goto_cen(counter=mca.counters.Pt_det0)
    umv(pz,height)
    user.align_cor_dty(10, 80, 0.03, ctr='mca:Cu_det0')
    ACTIVE_MG.enable("eiger:image")
    ACTIVE_MG.disable("eiger:image")
    plotinit("eiger:roi_counters:roi1_max")
    plotselect("eiger:roi_counters:roi1_max")
    
def layer_smaller_test():
    y0 = 0.0  # centre of rotation
    width = 4.5
    step  = 0.3
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.05
    anrange = 180.
    speed = 25.
    eiger.camera.photon_energy=43468.9
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"


def layer_smaller_360_test():
    y0 = 0.0  # centre of rotation
    width = 4.5
    step  = 0.3
    ypos = np.arange(-step, width+step/10, step)
    astep = 0.05
    anrange = 360.
    speed = 25.
    eiger.camera.photon_energy=43468.9
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"


def layer_smaller_360_test_CG():
    y0 = 0.0  # centre of rotation
    width = 250
    step  = 1
    ypos = np.arange(-step*12, width+(6*step), step)
    astep = 0.25
    anrange = 360.
    speed = 5
    eiger.camera.photon_energy=42200
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca*")
    eiger.camera.auto_summation="ON"
    ACTIVE_MG.enable("eiger:image")
    for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate, scan_mode='CAMERA')
        if (my_y + step/2) > width:
            break
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"
    


def volscan():
    pz0 = 53.
    k = 1
    for zpos in np.arange(0,13.5,0.15):
        if ( k % 10 ) == 0:
            ref_sample( zpos )
        k += 1
        umv( pz, pz0 + zpos )
        newdataset( "360h_Z%03d"%( k )  )
        layer_smaller_360_test()


def threelayers():
    pz0 = 53.1
    newdataset("3L_Z001")
    ref_sample(pz0+1)
    umv(pz, pz0+1)
    layer_large_test()
    
    newdataset("3L_Z002")
    ref_sample(pz0+6)
    umv(pz, pz0+6)
    layer_large_test()
    
    newdataset("3L_Z003")
    ref_sample(pz0+11)
    umv(pz, pz0+11)
    layer_large_test()
    

def volscan_continued_1():
    # machine bugged at pz==53.9 but scan was almost done
    # ... continue next slice.
    pz0 = 53.
    k = 1
    for zpos in np.arange(0,13.5,0.15):
        if ( k % 10 ) == 0:
            ref_sample( zpos )
        k += 1
        if k <= 8:
            continue
        umv( pz, pz0 + zpos )
        newdataset( "360h_Z%03d"%( k )  )
        layer_smaller_360_test()

def volscan_continued_2():
    # machine bugged at pz==53.9 but scan was almost done
    # ... continue next slice.
    pz0 = 53.
    k = 1
    for zpos in np.arange(0,13.5,0.15):
        if ( k % 10 ) == 0:
            ref_sample( zpos )
        k += 1
        if k <= 10:
            continue
        umv( pz, pz0 + zpos )
        newdataset( "360h_Z%03d"%( k )  )
        layer_smaller_360_test()

def volscan_continued_3():
    # machine bugged at pz==53.9 but scan was almost done
    # ... continue next slice.
    pz0 = 53.
    k = 1
    for zpos in np.arange(0,13.5,0.15):
        if ( k % 10 ) == 0:
            ref_sample( pz0 + zpos )
        k += 1
        if k <= 10:
            continue
        umv( pz, pz0 + zpos )
        newdataset( "361h_Z%03d"%( k )  )
        layer_smaller_360_test()
        
def volscan_continued_4():
    # machine bugged at pz==53.9 but scan was almost done
    # ... continue next slice.
    pz0 = 53.
    k = 1
    for zpos in np.arange(0,13.5,0.15):
        #if ( k % 10 ) == 0 and k>20:
        #    ref_sample( pz0 + zpos )
        k += 1
        if k <= 60:
            continue
        umv( pz, pz0 + zpos )
        if k>61:
            newdataset( "361h_Z%03d"%( k )  )
        layer_smaller_360_test()
        
# eiger.proxy.set_timeoutmillis( 60*1000 ) # !!!?!?!?





f2scanpars = {
    'motor': rot,
    'start_pos': 0,
    'step_size': -0.05,
    'step_time': 0.0,
    'slave_motor': dty,
    'slave_start_pos': -4.5,
    'slave_step_size': 4.1666666666666665e-05,
    'acq_time': 0.002,
    'npoints': 219600,
    'scan_mode': 'CAMERA',
    'gate_mode': 'TIME',
    'camera_signal': 'LEVEL',
    'save_flag': True,
    'display_flag': True,
    'sync_encoder': True,
    'home_rotation': True,
    'acc_margin': 0.0,
    'slave_acc_margin': 0.0,
    'sampling_time': 0.5,
    'latency_time': 0
} 


#March 2021 session; used functions from above: pause_for_refill; myfscan
######################################################################
from bliss.common import plot


def rotfix():
    """ sort out the rotation zero position """
    if abs(rot.position)>360:
        diff = rot.dial % 360
        if diff > 180:
            umvr(rot, diff-360-2)
        else:
            umvr(rot, diff-2)
        rot.home()
        
def ref_pz_height( pzref ):
    """ Try to find the top of the sample """
    rotfix()
    umv(rot, 0, dty, 0)
    umv( pz, pzref )
    ACTIVE_MG.enable("mca")
    pause_for_refill(40)
    sc = dscan( pz, -1, 1, 40, .1 )
    pzref = process_ref(sc)
    return pzref 

def process_ref(sc):
    """ fit a reference scan to find the top """
    z = sc.get_data('pz')
    y2= sc.get_data('mca:Pt_det0')
    y1= sc.get_data('mca:Mo_det0')
    angle = np.arctan2( y1 / y1.max() , y2 / y2.max() )
    f = flint()
    p = f.get_plot("plot1d", "hc4082", "hc4082_plot")
    
    p.plot( x=z, data=angle)
    ideal = np.arctan2(1, 1)
    i = abs(angle-ideal).argmin()
    print(z[i])
    zref = np.interp( ideal, angle[i-2:i+2], z[i-2:i+2] )
    print(zref, ideal)
    p.plot({'x': [zref, zref], 'COR': [ideal - 1, ideal + 1]}, linestyle='--')
    p.plot({'x': z, 'y1': y1/y1.max() })
    p.plot({'x': z, 'y2': y2/y2.max() })    
    return zref

def ref_pz_height_norot( pzref ):
    """ Try to find the top of the sample """
    umv(rot,180,dty,0)
    umv( pz, pzref )
    ACTIVE_MG.enable("mca")
    pause_for_refill(40)
    sc = dscan( pz, -1, 1, 40, .1 )
    pzref = process_ref(sc)
    return pzref 



def ref_pz_height_knife( pzref ):
    """ Try to find the top of the sample """
    rotfix() #added only for Mgpoly
    umv(rot,180,dty,0)
    umv( pz, pzref )
    ACTIVE_MG.enable("mca")
    pause_for_refill(40)
    sc = dscan( pz, -1, 1, 40, .2 )
    pzref = process_ref_knife(sc)
    return pzref

def process_ref_knife(sc):
    """ fit a reference scan to find the top """
    z = sc.get_data('pz')
    y = sc.get_data('mca:Pt_det0')
    
    f = flint()
    p = f.get_plot("plot1d", "hc4082", "hc4082_plot")
    
    p.plot( x=z, data=y)
    imax = y.argmax()
    ycut = y[imax:]   # assumes scan always goes the right way
    zcut = z[imax:]
    ideal = (ycut.max() + ycut.min())/2  # halfway up
    
    i = abs(ycut-ideal).argmin()
    print(zcut[i])
    lo = max( i-2, 0 )
    zref = np.interp( ideal, ycut[lo:i+2][::-1], zcut[lo:i+2][::-1] )
    print(zref, ideal)
    p.plot({'x': [zref, zref], 'COR': [ideal/2, ideal*3/2]}, linestyle='--')
    p.plot({'x': zcut, 'ycut': ycut })    
    return zref



def ref_pz_height_knife_insitu( pzref ):
    """ Try to find the top of the sample """
    umv(rot,190,dty,0)
    umv( pz, pzref )
    ACTIVE_MG.enable("mca")
    pause_for_refill(40)
    sc = dscan( pz, -1, 1, 40, .2 )
    pzref = process_ref_knife_insitu(sc)
    return pzref


def process_ref_knife_insitu(sc):
    """ fit a reference scan to find the top """
    z = sc.get_data('pz')
    y = sc.get_data('mca:Mo_det0')
    
    f = flint()
    p = f.get_plot("plot1d", "hc4082", "hc4082_plot")
    
    p.plot( x=z, data=y)
    imax = y.argmax()
    imin = y.argmin()
    ycut = y[imin:]   # assumes scan always goes the right way
    zcut = z[imin:]
    ideal = (ycut.max() + ycut.min())/2  # halfway up
    
    i = abs(ycut-ideal).argmin()
    print(zcut[i])
    lo = max( i-2, 0 )
    zref = np.interp( ideal, ycut[lo:i+2], zcut[lo:i+2] )
    print(zref, ideal)
    p.plot({'x': [zref, zref], 'COR': [ideal/2, ideal*3/2]}, linestyle='--')
    p.plot({'x': zcut, 'ycut': ycut })    
    return zref



def layer_f2scan(ny ):
    """ measure  a layer"""
    ACTIVE_MG.disable("mca")
    ACTIVE_MG.enable("eiger")
    eiger.camera.photon_energy = 43000
    eiger.camera.auto_summation = 'OFF'
    f2scan.pars.from_dict(  {
        'motor': rot,
        'start_pos': 0,
        'step_size': -0.05,       #rotation direction change. negative here
        'step_time': 0.0,
        'slave_motor': dty,
        'slave_start_pos': -0.45,
        'slave_step_size':  0.15 / 7200, #   4.1666666666666665e-05,
        'acq_time': 0.002,
        'npoints': 7200 * ny + 200 ,    # number of y steps here currently 30
        'scan_mode': 'CAMERA',
        'gate_mode': 'TIME',
        'camera_signal': 'LEVEL',
        'save_flag': True,
        'display_flag': True,
        'sync_encoder': True,
        'home_rotation': abs(rot.position) > 300,
        'acc_margin': 0.0,
        'slave_acc_margin': 0.0,
        'sampling_time': 0.5,
        'latency_time': 0,
    } )
    pause_for_refill( 1 )
    f2scan.run()


def ref_dty():
    rotfix()
    ACTIVE_MG.enable("mca")
    plotinit("Mo_det0")
    plotselect("Mo_det0")
    pause_for_refill( 100 )
    umv(dty, 0)
    user.align_cor_dty(5, 200, 0.05, ctr='mca:Mo_det0')
    print("Resetting dty to zero!!!")
    elog_print("ref_dty reset dty from %f to %f"%(dty.position,0))
    dty.position=0


    

    


#### new dty 
def ref_dty_alemnis():
    ACTIVE_MG.enable('mca')
    umv( rot, 100 )
    fscan( dty, -10, .05, 400, .05, scan_mode='TIME')
    s100 = fscan.scan
    # px*sin(100)+py*cos(100)
    umv( rot, 190 )
    fscan( dty, -10, .05, 400, .05, scan_mode='TIME')
    s190 = fscan.scan
    # px*sin(190)+py*cos(190)
    umv( rot, 280 )
    fscan( dty, -10, .05, 400, .05, scan_mode='TIME')
    s280 = fscan.scan
    fit_dty_alemnis( s100, s190, s280, domove=True)
    print("Resetting dty to zero!!!")
    elog_print("ref_dty reset dty from %f to %f"%(dty.position,0))
    dty.position=0
    

    
def fit_dty_alemnis( scan100, scan190, scan280, domove=False):
    cen100  = cen( mca.counters.Mo_det0, dty, scan100)
    cen190  = cen( mca.counters.Mo_det0, dty, scan190)
    cen280  = cen( mca.counters.Mo_det0, dty, scan280)
    # px*sin(280)+py*cos(280)

    dtycen = (cen100 + cen280)/2
    err = (cen100 - cen280 )/2
    # px*sin(100)+py*cos(100)
    #-px*sin(280)-py*cos(280)
    # ====
    # 2*px*sin(100)+2*py*cos(100) = cen100 - cen280
    # 
    err2 = cen190 - dtycen
    # px*sin(190)+py*cos(190)
    if domove:
        umv( dty, dtycen )
        umvr( px,  err * np.sin(np.radians(100))+ err2*np.sin(np.radians(190)))
        umvr( py,  err * np.cos(np.radians(100))+ err2*np.cos(np.radians(190)))
        
    else:
        print( dtycen, err * np.sin(np.radians(100)) + err2 * np.sin(np.radians( 190)), 
               err * np.cos( np.radians( 100 ))+ err2 * np.cos(np.radians( 190 )))
            


def ref_dty_Mg():
    ACTIVE_MG.enable('mca')
    rotfix()
    umv( rot, 100 )
    fscan( dty, -10, .05, 400, .05, scan_mode='TIME')
    s100 = fscan.scan
    # px*sin(100)+py*cos(100)
    umv( rot, 190 )
    fscan( dty, -10, .05, 400, .05, scan_mode='TIME')
    s190 = fscan.scan
    # px*sin(190)+py*cos(190)
    umv( rot, 280 )
    fscan( dty, -10, .05, 400, .05, scan_mode='TIME')
    s280 = fscan.scan
    fit_dty_Mg( s100, s190, s280, domove=True)
    print("Resetting dty to zero!!!")
    elog_print("ref_dty reset dty from %f to %f"%(dty.position,0))
    dty.position=0


def fit_dty_Mg( scan100, scan190, scan280, domove=False):
    cen100  = cen( mca.counters.Pt_det0, dty, scan100)
    cen190  = cen( mca.counters.Pt_det0, dty, scan190)
    cen280  = cen( mca.counters.Pt_det0, dty, scan280)
    # px*sin(280)+py*cos(280)

    dtycen = (cen100 + cen280)/2
    err = (cen100 - cen280 )/2
    # px*sin(100)+py*cos(100)
    #-px*sin(280)-py*cos(280)
    # ====
    # 2*px*sin(100)+2*py*cos(100) = cen100 - cen280
    # 
    err2 = cen190 - dtycen
    # px*sin(190)+py*cos(190)
    if domove:
        umv( dty, dtycen )
        umvr( px,  err * np.sin(np.radians(100))+ err2*np.sin(np.radians(190)))
        umvr( py,  err * np.cos(np.radians(100))+ err2*np.cos(np.radians(190)))
        
    else:
        print( dtycen, err * np.sin(np.radians(100)) + err2 * np.sin(np.radians( 190)), 
               err * np.cos( np.radians( 100 ))+ err2 * np.cos(np.radians( 190 )))



def volscan_S328():
    pz0 = 46.25
    k = 1
    
    for zpos in np.arange(0,11.551,0.15):
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82
            newdataset("yref")
            ref_dty( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Z%03d"%( k )  )
        # timing about 493
        layer_f2scan( ) #record changed, number y steps was 30


def volscan_S328_cont(klast):
    pz0 = 46.25
    k = 1
    
    for zpos in np.arange(0,11.551,0.15):
        if k < klast:
            k += 1
            continue
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("restart3_zref")
            pz0 = ref_pz_height( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82
            newdataset("restart3_yref")
            ref_dty( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "restart3_Z%03d"%( k )  )
        # timing about 493
        layer_f2scan( ) #record changed, number y steps was 30
        
def volscan_Mo4():
    pz0 = 47.62
    k = 0
    
    for zpos in np.arange(0,11.551,0.15):
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82 s
            newdataset("yref")
            ref_dty( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Z%03d"%( k )  )
        # timing about 493
        layer_f2scan(34)




def volscan_Mo4_cont6(klast):      #CHANGE CONT NUMBER +1       THIS IS THE NEGATIVE DIRECTION ROTATION VERSION
    pz0 = 46.863138 #####CHANGE TO LAST BEST  
    k = 0
    
    for zpos in np.arange(0,11.71,0.15):
        if k < klast:
            k += 1
            continue
       
        if ( k % 2 ) == 0:     # or (k == klast):   REMOVED THIS AS STOPPED INITIALISING?
            # timing about 51 s
            newdataset("Re6_zref")       #CHANGE CONT NUMBER +1
            pz0 = ref_pz_height( pz0 )
        
        if ( k % 10 ) == 0:      # or (k == klast):
            umv( pz , pz0+1 )
            # timing about 82 s
            newdataset("Re6_yref")         #CHANGE CONT NUMBER +1
            ref_dty( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Re6_Z%03d"%( k )  )         #CHANGE CONT NUMBER +1
        # timing about 493
        layer_f2scan(34)




def volscan_Mo4_contrev6(klast):       #CHANGE CONTREV NUMBER +1
    pz0 = 46.863138  #####CHANGE TO LAST BEST  
    k = 0
    
    for zpos in np.arange(0,11.551,0.15):
        if k < klast:
            k += 1
            continue
       
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("Rev6_zref")   #CHANGE CONTREV NUMBER +1
            pz0 = ref_pz_height( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82 s
            newdataset("Rev6_yref")   #CHANGE CONTREV NUMBER +1
            ref_dty( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Rev6_Z%03d"%( k )  )    #CHANGE CONTREV NUMBER +1
        # timing about 493
        layer_f2scanrev(34)

def layer_f2scanrev(ny ):
    """ measure  a layer"""
    ACTIVE_MG.disable("mca")
    ACTIVE_MG.enable("eiger")
    eiger.camera.photon_energy = 43000
    eiger.camera.auto_summation = 'OFF'
    f2scan.pars.from_dict(  {
        'motor': rot,
        'start_pos': 0,
        'step_size': 0.05,    #rotation direction change. positive here
        'step_time': 0.0,
        'slave_motor': dty,
        'slave_start_pos': -0.45,
        'slave_step_size':  0.15 / 7200, #   4.1666666666666665e-05,
        'acq_time': 0.002,
        'npoints': 7200 * ny + 200 ,    # number of y steps here currently 30
        'scan_mode': 'CAMERA',
        'gate_mode': 'TIME',
        'camera_signal': 'LEVEL',
        'save_flag': True,
        'display_flag': True,
        'sync_encoder': True,
        'home_rotation': abs(rot.position) > 300,
        'acc_margin': 0.0,
        'slave_acc_margin': 0.0,
        'sampling_time': 0.5,
        'latency_time': 0,
    } )
    pause_for_refill( 1 )
    f2scan.run()

def layer_f2scanrev_refine(ny ):   #FAILS TO EXECUTE: CANNOT SET VELOCITY FOR AXIS 31: OUT OF RANGE OF PARAMETERS
    """ measure  a layer"""
    ACTIVE_MG.disable("mca")
    ACTIVE_MG.enable("eiger")
    eiger.camera.photon_energy = 43000
    eiger.camera.auto_summation = 'OFF'
    f2scan.pars.from_dict(  {
        'motor': rot,
        'start_pos': 0,
        'step_size': 0.01,    #rotation direction change. positive here
        'step_time': 0.0,
        'slave_motor': dty,
        'slave_start_pos': -0.45,
        'slave_step_size':  0.15 / 36000, #   4.1666666666666665e-05,
        'acq_time': 0.002,
        'npoints': 36000 * ny + 200 ,    # number of y steps here currently 30
        'scan_mode': 'CAMERA',
        'gate_mode': 'TIME',
        'camera_signal': 'LEVEL',
        'save_flag': True,
        'display_flag': True,
        'sync_encoder': True,
        'home_rotation': abs(rot.position) > 300,
        'acc_margin': 0.0,
        'slave_acc_margin': 0.0,
        'sampling_time': 0.5,
        'latency_time': 0,
    } )
    pause_for_refill( 1 )
    f2scan.run()


def volscan_Mo3():
    pz0 = 46.312
    k = 0
    
    for zpos in np.arange(0,8.401,0.15):
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82 s
            newdataset("yref")
            ref_dty_small( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Z%03d"%( k )  )
        # timing about 493
        layer_f2scan(22)

def ref_dty_small():
    rotfix()
    ACTIVE_MG.enable("mca")
    plotinit("Mo_det0")
    plotselect("Mo_det0")
    pause_for_refill( 100 )
    umv(dty, 0)
    user.align_cor_dty(5, 300, 0.05, ctr='mca:Mo_det0')     #increased resolution of scan for small pillar width
    print("Resetting dty to zero!!!")
    elog_print("ref_dty reset dty from %f to %f"%(dty.position,0))
    dty.position=0


def volscan_Mo3_contrev5(klast):       #CHANGE CONTREV NUMBER +1
    pz0 = 47.0373
    k = 0
    
    for zpos in np.arange(0,8.401,0.15):
        if k < klast:
            k += 1
            continue
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("Rev5_zref")       #CHANGE CONTREV NUMBER +1
            pz0 = ref_pz_height( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82 s
            newdataset("Rev5_yref")      #CHANGE CONTREV NUMBER +1
            ref_dty_small( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Rev5_Z%03d"%( k )  )     #CHANGE CONTREV NUMBER +1
        # timing about 493
        layer_f2scanrev(22)

def volscan_Mo3_cont5(klast):       #CHANGE CONTREV NUMBER +1
    pz0 = 46.9965
    k = 0
    
    for zpos in np.arange(0,8.401,0.15):
        if k < klast:
            k += 1
            continue
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("Re5_zref")       #CHANGE CONTREV NUMBER +1
            pz0 = ref_pz_height( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82 s
            newdataset("Re5_yref")      #CHANGE CONTREV NUMBER +1
            ref_dty_small( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Re5_Z%03d"%( k )  )     #CHANGE CONTREV NUMBER +1
        # timing about 493
        layer_f2scan(22)




import time, os
def logwatch( fname = "/tmp/nscope_debug_on" ):
    started = False
    while 1:
        if os.path.exists(fname):
            started = True
            debugon( open( fname ).read().strip() )
        else:
            if started:
                started = False
                debugoff("*")
        time.sleep(1)

# gevent.spawn(logwatch)





def layer_alemnis( width):
    y0 = 0.0  # centre of rotation
    step  = 0.15
    ypos = np.arange(-width,  width+step/10, step)
    astep = 0.05   
    alo = 100
    ahi = 280
    npt = abs( ahi - alo ) / astep
    expo = 0.002
    eiger.camera.photon_energy=43000.
    fscan.pars.latency_time=0
    #### 
    assert (astep * expo)<26 , 'too fast'
    timest = npt * expo + 5
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    for k, my_y in enumerate( ypos ):
        myumv(dty, my_y)
        pause_for_refill(timest)
        if k % 2 == 0:
            myfscan(rot, alo, astep, npt, expo, scan_mode='CAMERA')
        else:
            myfscan(rot, ahi, -astep, npt, expo, scan_mode='CAMERA')


def layer_alemnis_refine( width):
    y0 = 0.0  # centre of rotation
    step  = 0.15
    ypos = np.arange(-width,  width+step/10, step)
    astep = 0.01   
    alo = 100
    ahi = 280
    npt = abs( ahi - alo ) / astep
    expo = 0.002
    eiger.camera.photon_energy=43000.
    #### 
    assert (astep * expo)<26 , 'too fast'
    timest = npt * expo + 5
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    for k, my_y in enumerate( ypos ):
        myumv(dty, my_y)
        pause_for_refill(timest)
        if k % 2 == 0:
            myfscan(rot, alo, astep, npt, expo, scan_mode='CAMERA')
        else:
            myfscan(rot, ahi, -astep, npt, expo, scan_mode='CAMERA')


def layer_Mg_coarsestep( width):
    y0 = 0.0  # centre of rotation
    step  = 0.2
    ypos = np.arange(-width,  width+step/10, step)
    astep = 0.05   
    alo = 100
    ahi = 280
    npt = abs( ahi - alo ) / astep
    expo = 0.002
    eiger.camera.photon_energy=43000.
    #### 
    assert (astep * expo)<26 , 'too fast'
    timest = npt * expo + 5
    ACTIVE_MG.disable("mca")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    for k, my_y in enumerate( ypos ):
        myumv(dty, my_y)
        pause_for_refill(timest)
        if k % 2 == 0:
            myfscan(rot, alo, astep, npt, expo, scan_mode='CAMERA')
        else:
            myfscan(rot, ahi, -astep, npt, expo, scan_mode='CAMERA')


            
def volscan_SSEPN7():    #two simple slices to check volscan
    pz0 = 44.73   #######set me
    k = 0
    width=4.5    #CHANGE WITH LOADING INCREASE
    for zpos in np.arange(0,0.151,0.15): #SET HEIGHT OF PILLAR 
        
        if ( k % 2 ) == 0:
          #   timing about 51 s
           newdataset("zref")
           pz0 = ref_pz_height_knife_insitu( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82 s
            newdataset("yref")
            ref_dty_alemnis( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos +1 ) # start 1 um below top for this trial script!!!!
        
        newdataset( "Z%03d"%( k )  )
        # timing about 493
        layer_alemnis(width) 




def volscan_SSEPN2_0pc_Re6(): ###RESTART FROM THE BEGINNING, DONT USE FOR RESUMING AFTER A CRASH
    pz0 = 43.21   #######set me!!!!
    k = 0
    width=4.95    #CHANGE WITH LOADING INCREASE
    l=0
    for zpos in np.arange(4,8.051,0.15): #SET HEIGHT OF PILLAR 
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("Re6_zref")
            pz0 = ref_pz_height_knife_insitu( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82 s
            newdataset("Re6_yref")
            ref_dty_alemnis( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Re6_Z%03d"%( k )  )
        # timing about 493
        layer_alemnis(width)

    for zpos2 in np.arange(5,7.001,1): 
        
        # timing about 51 s
        newdataset("refine_zref")
        pz0 = ref_pz_height_knife_insitu( pz0 )
        
        
        umv( pz , pz0+1 )
        # timing about 82 s
        newdataset("refine_yref")
        ref_dty_alemnis( )
        l += 1

                
        umv( pz, pz0 + zpos2 )
        
        newdataset( "refine_Z%03d"%( l )  )
        # timing about 493
        layer_alemnis_refine(width) 






def volscan_SSEPN2_0pc_Re7(klast): ###USE FOR RESUMING AFTER A CRASH
    pz0 = 43.21   #######set me!!!!
    k = 0
    width=4.95    #CHANGE WITH LOADING INCREASE
    l=0
    for zpos in np.arange(4,8.051,0.15): #SET HEIGHT OF PILLAR 
        if k < klast:
            k += 1
            continue
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("Re7_zref")
            pz0 = ref_pz_height_knife_insitu( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82 s
            newdataset("Re7_yref")
            ref_dty_alemnis( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Re7_Z%03d"%( k )  )
        # timing about 493
        layer_alemnis(width) 

    for zpos2 in np.arange(5,7.001,1): 
        
        # timing about 51 s
        newdataset("refine_zref")
        pz0 = ref_pz_height_knife_insitu( pz0 )
        
        
        umv( pz , pz0+1 )
        # timing about 82 s
        newdataset("refine_yref")
        ref_dty_alemnis( )
        l += 1

                
        umv( pz, pz0 + zpos2 )
        
        newdataset( "refine_Z%03d"%( l )  )
        # timing about 493
        layer_alemnis_refine(width) 

        

def volscan_SSEPN2_elasticstep1(): ###RESTART FROM THE BEGINNING, DONT USE FOR RESUMING AFTER A CRASH
    pz0 = 42.4278   #######set me!!!!
    k = 0
    width=4.5    #CHANGE WITH LOADING INCREASE
    l=0
    for zpos2 in np.arange(1,6.001,5): 
        
        # timing about 51 s
        newdataset("refine_zref")
        pz0 = ref_pz_height_knife_insitu( pz0 )
        
        
        umv( pz , pz0+1 )
        # timing about 82 s
        newdataset("refine_yref")
        ref_dty_alemnis( )
        l += 1

                
        umv( pz, pz0 + zpos2 )
        
        newdataset( "refine_Z%03d"%( l )  )
        # timing about 493
        layer_alemnis_refine(width) 


#NICOLO Magnesium     CHANGE TO FSCAN manual zigzag type
def volscan_Mgref_refine_Re1():
    pz0 = 44.387
    width=4.05    #CHANGE WITH LOADING INCREASE
    l=0
    for zpos2 in np.arange(8,8.151,0.15): 
        
        # timing about 51 s
        newdataset("refine_Re1_zref")
        pz0 = ref_pz_height_knife( pz0 )
        
        
        umv( pz , pz0-1 )
        # timing about 82 s
        newdataset("refine_Re1_yref")
        ref_dty_Mg( )
        l += 1

                
        umv( pz, pz0 + zpos2 )
        
        newdataset( "refine_Re1_Z%03d"%( l )  )
        # timing about 493
        layer_alemnis_refine(width) 

          
def volscan_MgTbar03():
    pz0 = 50.2257
    k = 0
    width=5.2
    l = 0
    for zpos in np.arange(22,30.001,0.2):
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height_knife( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0-1 )
            # timing about 82 s
            newdataset("yref")
            ref_dty_Mg( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Z%03d"%( k )  )
        # timing about 493
        layer_Mg_coarsestep(width) 

    for zpos2 in np.arange(32,32.001,0.2): 
        
        # timing about 51 s
        newdataset("refine_Re2_zref")
        pz0 = ref_pz_height_knife( pz0 )
        
        
        umv( pz , pz0-1 )
        # timing about 82 s
        newdataset("refine_Re2_yref")
        ref_dty_Mg( )
        l += 1

                
        umv( pz, pz0 + zpos2 )
        
        newdataset( "refine_Re2_Z%03d"%( l )  )
        # timing about 493
        layer_alemnis_refine(width) 

        
def volscan_MgTbarpoly_Re2():  ####DONT USE: NO COMPENSATION OF GAUGE TILT: USE NEXT FUNCTION
    pz0 = 26.11794   #set as appropriate
    k = 0
    width=7.05
    for zpos in np.arange(26,43.001,0.15):
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("Re2_zref")
            pz0 = ref_pz_height_knife( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0-0.3 )
            # timing about 82 s
            newdataset("Re2_yref")
            ref_dty_Mg( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Re2_Z%03d"%( k )  )
        # timing about 493
        layer_f2scanrev(51) 


        
def volscan_MgTbarpoly_Re8(klast):       ##### Xavier, USE THIS CODE TO RESUME NICOLO POLYXSTAL SAMPLE IF CRASH
    pz0 = 26.19435   #set as appropriate
    k = 0
    width=7.05
    for zpos in np.arange(26,43.001,0.15):
        if k < klast:
            k += 1
            continue
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("Re8_zref")
            pz0 = ref_pz_height_knife( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0-0.3 )
            # timing about 82 s
            newdataset("Re8_yref")
            ref_dty_Mg( )
            umvr(px,-2)    ###### THIS LINE COMPENSATES FOR TILT OF SAMPLE: OFFSETS RELATIVE TO Pt UP TOP
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Re8_Z%03d"%( k )  )
        # timing about 493
        layer_f2scanrev(51) 







        




        
        
###### Final 316L pillar: S1_36
def volscan_S136_Re4():
    pz0 = 46.29277
    k = 0
    width=3.9
    for zpos in np.arange(0,11.701,0.15):
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("Re4_zref")
            pz0 = ref_pz_height_norot( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82 s
            newdataset("Re4_yref")
            ref_dty_alemnis( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Re4_Z%03d"%( k )  )
        # timing about 493
        layer_alemnis(width)


def volscan_S136_cont56(klast):   ###USE FOR RESUMING AFTER A CRASH
    pz0 = 45.774
    k = 0
    width=3.9
    for zpos in np.arange(0,11.701,0.15):
        if k < klast:
            k += 1
            continue
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("Re6_zref")    #CHANGE CONT NUMBER +1
            pz0 = ref_pz_height_norot( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82 s
            newdataset("Re5_yref")   #CHANGE CONT NUMBER +1
            ref_dty_alemnis( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Re5_Z%03d"%( k )  )    #CHANGE CONT NUMBER +1
        # timing about 493
        layer_alemnis(width)




def volscan_S136_fullrot_cont10(klast):
    pz0 = 45.8781
    k = 0
        
    for zpos in np.arange(0,11.701,0.15):
        if k < klast:
            k += 1
            continue
        
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("fullrot_Re10_zref")
            pz0 = ref_pz_height( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82
            newdataset("fullrot_Re10_yref")
            ref_dty( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "fullrot_Re10_Z%03d"%( k )  )
        # timing about 493
        layer_f2scanrev(29)


        
def volscan_S136_fullrot_cont11_missedlayer():  #for Z030
    pz0 = 45.8781
    k = 30
    zpos=4.35
      

    newdataset("fullrot_Re11_zref")
    pz0 = ref_pz_height( pz0 )
        

    umv( pz , pz0+1 )
            # timing about 82
    newdataset("fullrot_Re11_yref")
    ref_dty( )

    newdataset("fullrot_Re11_zref")
    pz0 = ref_pz_height( pz0 )
                       
    umv( pz, pz0 + zpos )
        
    newdataset( "fullrot_Re11_Z%03d"%( k )  )
        # timing about 493
    layer_f2scanrev(29)


### Mo1: unstrained reference
def volscan_Mo1():
    pz0 = 48.443
    k = 0
    l=0
    for zpos in np.arange(6.5,7.551,0.15):
      
        if ( k % 2 ) == 0:
            # timing about 51 s
            newdataset("zref")
            pz0 = ref_pz_height( pz0 )
        
        if ( k % 10 ) == 0:
            umv( pz , pz0+1 )
            # timing about 82
            newdataset("yref")
            ref_dty( )
        k += 1
        if k <= -1: # skips
            continue
                
        umv( pz, pz0 + zpos )
        
        newdataset( "Z%03d"%( k )  )
        # timing about 493
        layer_f2scanrev(34)

    for zpos2 in np.arange(6.5,6.501,0.15): 
        
        # timing about 51 s
        newdataset("refine_zref")
        pz0 = ref_pz_height( pz0 )
        
        
        umv( pz , pz0+1 )
        # timing about 82 s
        newdataset("refine_yref")
        ref_dty( )
        l += 1

                
        umv( pz, pz0 + zpos2 )
        
        newdataset( "refine_Z%03d"%( l )  )
        # timing about 493
        layer_f2scanrev_refine(34)

    
def volscan_Mo1_refine_Re1():
    pz0 = 48.443
    k = 0
    l=0
    
    for zpos2 in np.arange(6.5,6.501,0.15): 
        
        # timing about 51 s
        newdataset("Re1_refine_zref")
        pz0 = ref_pz_height( pz0 )
        
        
        umv( pz , pz0+1 )
        # timing about 82 s
        newdataset("Re1_refine_yref")
        ref_dty( )
        l += 1

                
        umv( pz, pz0 + zpos2 )
        
        newdataset( "Re2_refine_Z001"  )
        # timing about 493
        layer_f2scanrev_refine(34)
