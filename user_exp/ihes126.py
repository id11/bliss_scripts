
import time, numpy as np

def ff_z_series(name, start=0):
    #z0 = samtz.position
    z0 = -1.90518
    for i, zoff in enumerate( np.arange(-0.5,0.51,0.1) ):
        if i < start:
            continue
        umv(samtz, z0 + zoff)
        newdataset( name + '_ff_Z%03d'%(i) )
        finterlaced(diffrz, 0, 0.25, 360/0.25, 0.085)
    umv(samtz, z0)


def wait_to_ramp_t(t, tol=2):
    tr = nanodac3.read()
    while abs(tr - t) > 2:
        print("Waiting for temperature",t,tr)
        time.sleep(10)
        tr = nanodac3.read()
    print("Stabilise")
    time.sleep(60)
    

        
def ff_in():
    sheh3.close()
    marana_out()
    umv(ffdtz1, 0, ffdtx1, 300)
    ACTIVE_MG.enable('frelon3')
    umv(s7vg,0.1,s8vg,0.2)
    sheh3.open()

def ff_out():
    sheh3.close()
    umv(ffdtx1, 530)
    umv(ffdtz1, 300)
    ACTIVE_MG.disable('frelon3')
    
def marana_in():
    ff_out()
    umv(d3ty, 0.0246, d3tz, 0.060)
    umv(nfdtx, 100)
    ACTIVE_MG.enable('marana3')
    umv(s7vg,1.3,s8vg,1.5)
    sheh3.open()
    
def marana_out():
    umv(nfdtx,100,d3ty,130)
    ACTIVE_MG.disable('marana3')


def t_series(start = 1):
    ts = np.linspace(25,800,6)
    for i,t in enumerate(ts):
        if i < start:
            continue
        nanodac3.setpoint = t
        wait_to_ramp_t(t)
        ff_z_series('T%d'%(t))

    t=25
    umv(furnace_z,130)
    nanodac3.setpoint = t
    time.sleep(60*9)
    marana_in()
    newsample('Per-MgO')
    fulltomo.full_turn_scan('qt10m-T%d'%(t))
    newsample('Per-MgO-diff')
    ff_in()
    ff_z_series('qt10m-T%d'%(t))
        
