

def goto80scanreturn():
    newdataset('RT')
    fscan(rot, 0, 1, 360, 1)
    umv(rot,0)
    ox800.ramprate = 360
    ox800.setpoint = 80
    print('Ramping to',ox800.setpoint,' K at',ox800.ramprate,' K/hr \n')
    while ox800.is_ramping():
        print('Temperature is',ox800_axis.position,'K \n')
        sleep(60)
    print('Temperature is',ox800_axis.position,'K \n')
    newdataset('80to360')
    scan80to360()
    print('Temperature is',ox800_axis.position,'K \n')
    ox800.ramprate = 360
    ox800.setpoint = 294
    print('Ramping to',ox800.setpoint,' K at',ox800.ramprate,' K/hr \n')
    while ox800.is_ramping():
        print('Temperature is',ox800_axis.position,'K \n')
        sleep(60)
    print('Temperature is',ox800_axis.position,'K \n')
    newdataset('RTback')
    fscan(rot, 0, 1, 360, 1)

def scan80to360():
    ox800.setpoint = 80
    sleep(60)
    while ox800.is_ramping():
        print('Temperature is',ox800_axis.position,'K \n')
        sleep(60)
    print('Temperature is',ox800_axis.position,'K \n')
    ox800.ramprate = 120
    ox800.setpoint = 360
    print('Ramping to',ox800.setpoint,' K at',ox800.ramprate,' K/hr \n')
    while ox800.is_ramping():
        fscan(rot, 0, 1, 360, 0.15)
        print('Temperature is',ox800_axis.position,'K \n')
        fscan(rot, 360, -1, 360, 0.15)
        print('Temperature is',ox800_axis.position,'K \n')

def cool360to80():
    ox800.setpoint = 360
    sleep(60)
    while ox800.is_ramping():
        print('Temperature is',ox800_axis.position,'K \n')
        sleep(60)
    print('Temperature is',ox800_axis.position,'K \n')
    ox800.ramprate = 120
    ox800.setpoint = 80
    print('Ramping to',ox800.setpoint,' K at',ox800.ramprate,' K/hr \n')
    while ox800.is_ramping():
        fscan(rot, 0, 1, 360, 0.15)
        print('Temperature is',ox800_axis.position,'K \n')
        fscan(rot, 360, -1, 360, 0.15)
        print('Temperature is',ox800_axis.position,'K \n')

def repeatramp80to360():
    newdataset('RT')
    fscan(rot, 0, 1, 360, 1)
    ox800.ramprate = 360
    ox800.setpoint = 80
    umv(rot,0)
    newdataset('80to360_r1')
    scan80to360()
    newdataset('360to80_r1')
    cool360to80()
    newdataset('80to360_r2')
    scan80to360()
    newdataset('360to80_r2')
    cool360to80()
    ox800.ramprate = 360
    ox800.setpoint = 294
    print('Ramping to',ox800.setpoint,' K at',ox800.ramprate,' K/hr \n')
    while ox800.is_ramping():
        print('Temperature is',ox800_axis.position,'K \n')
        sleep(60)
    print('Temperature is',ox800_axis.position,'K \n')
    newdataset('RTback')
    fscan(rot, 0, 1, 360, 1)

