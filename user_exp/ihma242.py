
def scansfee():
    fscan(rot,-180-45,45,2,15)
    fscan(rot,-180-45,0.1,900,1)


def scans_e(energyinev=44569):
    steps = [-1000,-100,0,100,1000]
    for s in steps:
        eiger.camera.photon_energy = (energyinev + s)
        newdataset('m%.3f_e%.3f'%(energyinev/1000, (energyinev+s)/1000))
        scansfee()
        
        
def foilscan(element):
    # taken from ll_edge_scan
    assert element in ll_foils_elements
    en = ll_foils_energies[ element ]
    tx, theta = ll_energy( en )
    umv( lltx1,  tx)
    umv( llbragg1, theta, llbragg2, theta+0.1 )
    f0 = LL_FOILS_X * tan( radians( 2*theta )) + ll_foils_position[ element ]
    d0 = LL_DIODE_X * tan( radians( 2*theta )) + 6
    umv( lldiode, d0, llfoils, f0)
    g18, g22 = guess_unds_edge( en )
    umv( cpm18, g18 )
    umv( u22, g22 )
    plotselect( pico0 )
    s = dscan( llbragg1, 0.07, -0.07, 100, 0.1, pico0 )
    xf = s.get_data('axis:llbragg1')
    yf = s.get_data('keithley:pico0')
    umvr(llfoils,5)
    s = dscan( llbragg1, 0.07, -0.07, 100, 0.1, pico0 )
    xn = s.get_data('axis:llbragg1')
    yn = s.get_data('keithley:pico0')
    umvr(llfoils,-10)
    return xf, yf, xn, yn
    
