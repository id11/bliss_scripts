
import numpy as np, time



# def check_cpm18(pos=6.4158):
def check_cpm18(pos=6.368):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03 or (abs(cpm18t.position - 0.075)>0.02):
        user.cpm18_goto(pos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()

def measure_layer_withbeamcheck( yrange, ystep, rspeed=25., exptime=0.002 ):
    ####-90 to 90 scan range, via zero, to avoid worse glitches
    eiger.camera.photon_energy = 44000.
    ACTIVE_MG.disable("mca")
    ACTIVE_MG.enable("eiger")
    rstep = rspeed * exptime
    if exptime < 0.003:
        # 16 bit data from the camera
        eiger.camera.auto_summation='OFF'   
    else:
        # 32 bit data
        eiger.camera.auto_summation='ON'
    pause_for_refill(1)
    fscan2d( dty, -yrange, ystep, 2 * yrange / ystep + 1,
             rot, -90, rstep, np.ceil(180 / rstep) + 1, exptime,
             scan_mode="CAMERA")
    # and clean up:
    eiger.camera.auto_summation = 'ON'
    umv(dty, 0)

def fscanloop360( yrange, ystep, rstep=0.05, rrange=360, rstart=4 ):
    nr = int(np.round(rrange / rstep))    
    for i, y in enumerate( np.arange( -yrange, yrange+ystep/2, ystep ) ):
        umv(dty, y )
        time.sleep(3) # sure - I agree
        pause_for_refill( 30 )
        if i%2 == 0:
            fscan(rot, rstart,  rstep, nr, 0.002, scan_mode = 'CAMERA' )
        else:
            fscan(rot, rstart+rstep*nr, -rstep, nr, 0.002, scan_mode = 'CAMERA' )

    
def myscan():
    newdataset("dt50um")
    y0 = diffty.position    
    for my_y  in np.arange(-0.75,0.76,0.05):
        print(my_y)
        umv(diffty, y0 + my_y)
        umv(diffty, y0 + my_y)
        finterlaced( diffrz, 0, 1, 360, 0.2 )

    sheh3.close()


def top( cut=0.2, highlim=1e9 ):
    s = fscan.scan
    y = s.get_data('eiger:roi_counters:roi2_sum')
    x = s.get_data('axis:dty')
    ymin = np.nanmin( y )
    y = np.where( y > 1e9, ymin, y )
    ymax = np.nanmax( y )
    m = y >= ( (ymax - ymin)*cut + ymin )
    return np.average( x[m], weights=abs( y[m] - cut ) )

def align90():
    eiger.camera.auto_summation='ON'
    plotselect(eiger.counters.roi2_sum)
    umv(rot, 90)
    fscan( dty, -20, 0.1, 401, 0.01, scan_mode='CAMERA' )
    cp = top( )
    umv(rot, 0)
    fscan( dty, -20, 0.1, 401, 0.01, scan_mode='CAMERA' )
    c0 = top( )
    umv(rot, -90)
    fscan( dty, -20, 0.1, 401, 0.01, scan_mode='CAMERA' )
    cm = top( )
    print(cp,c0,cm)
    umv( dty, (cm + cp)*0.5 )
    umvr(px, (cp - cm)*0.5 )
    umvr(py, c0 - (cm + cp)*0.5 )

    
def goldball():
    
    start = time.time()
    # up to 12 hours

    # 27 steps was 5 mins, guess 24 minutes for this
    # 12 hours will give ~25 layers.
    # do a 1 micron spacing
    zpos = 91
    # aim at 7h00                 h    m    s
    while time.time() - start < (12. * 60 * 60 ):
        umv( pz , zpos )

        newdataset( "r2_Z%03d"%(zpos) )

        umv(dty, 0)
        pause_for_refill( 120 )
        align90()
        
        measure_layer_withbeamcheck( 13, 0.2 )
        zpos = zpos - 1
        
        
def full1(ymax, datasetname, ystep=5, ymin=1801):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.125, 2888, 0.005 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 361, -0.125, 2888, 0.005 )
        
                  
    
def half1(ymax, datasetname, ystep=5, ymin=-1801):    
    if datasetname is not None:
        newdataset(datasetname)
    # full layer - will need to add a pause for refills ...
    ny = int( ymax // ystep )
    ylim = ny * ystep
    if ylim < ymax:
        ny += 1
        ylim += ystep
    ypos = np.linspace( -ylim, ylim, ny*2+1 )
    i = -1
    while i < len(ypos)-1:
        i += 1        
        if ypos[i] < ymin:
            continue
        pause_for_refill( 25 )
        if (i % 2) == 0: # forwards
            fscan2d( dty, ypos[i], ystep, 1, rot, 0,  0.125, 1448, 0.005 )
        else:
            fscan2d( dty, ypos[i], ystep, 1, rot, 181, -0.125, 1448, 0.005 )
        
def NbC():
    for zpos in (45,50,51,56):
        umv(pz, zpos)
        half1( 145, 'Z'+str(zpos), ystep=0.3 )
        
                          
def  ktype():
    for zpos in (10,20,30,40,50,60,70,80,90):
        umv(pz, zpos)
        half1( 300, 'bZ'+str(zpos), ystep=1)

def  ktype2():

    for zpos in (10,20,30,40,50,60,70,80,90):
        umv(pz, zpos)
        half1( 270, 'cZ'+str(zpos), ystep=1)

def  ktype3():

    for zpos in (10,20,30,40,50,60,70,80,90):
        umv(pz, zpos)
        half1( 280, 'cZ'+str(zpos), ystep=1)        
                
def check_tfoh1(rng=0.5, npt=100, ctime=0.1):
    tfoh1.set(0, 'Al')   
    for nbe in (16,12,8,4,0):
        tfoh1.set(nbe, 'Be')
        si = {'instrument': { 'CRL' : {'nBe':10, 'nAl': 0} } }
        dscan(py,-rng,rng,npt,ctime, save_images=False, scan_info=si)
        goto_cen()
        dscan(pz,-rng,rng,npt,ctime, save_images=False, scan_info=si)
        goto_cen()
        
              
              
def measure_beam(rng=1):
    for i in range(2):
        dscan(px, -rng, rng, 80, 0.1)
        goto_cen()
        dscan(pz, -rng, rng, 80, 0.1)
        goto_cen()



def YSZ():
    # about 8 or 9 at top (20)
    # about 12 in middle (50), pxerr 1.44
    # about 14 at bottom (90), pxerr 1.5
    for zpos in np.arange(12, 30, 0.15):
        umv(pz,zpos )
        # half1(ymax, datasetname, ystep=5, ymin=-1801):    
        half1( 4.95, 'Z%03d'%(zpos*10), ystep=0.15 )
    
