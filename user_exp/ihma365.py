import numpy as np
from scipy.optimize import curve_fit

def findside(x, y):
    y = y-y.min()
    #cm = (x*y).sum()/y.sum()
    ny = np.clip(y, 0.1*y.max(), 0.9*y.max()) - (0.1*y.max()) 
    g = np.gradient(ny)
    m = g>0.008
    return np.mean(g[m])

def linefit(x, a, b):
    return  np.abs((a*x) + b)   
    
def _align_r(raxis,rrange=0.8,rsteps=7,pzrange=25,pzsteps=50,ctim=0.05):
    rcen = raxis.position
    nc.piezo_scanmode_off()
    if raxis == shrx:
        if np.abs((rot.position+90)%180)>1:
            umv(rot, 90)
    elif raxis == shry:
        if np.abs((rot.position)%180)>1:
            umv(rot, 0)
    nc.piezo_scanmode_on()
    ws = []
    rvals = np.linspace(-rrange,rrange,rsteps)
    for r in rvals:
        sync()
        umv(raxis, rcen+r)
        s = dscan(pz,-pzrange,pzrange,pzsteps,ctim)
        ws.append(findside( s.get_data('pz'), s.get_data('roi1_avg') ))
    print(ws)
    ol1 = np.max(ws[:np.argmax(ws)])
    ol2 = np.max(ws[np.argmax(ws)+1:])
    print(ol1,ol2)
    if ol1 > ol2: 
        rl = np.argmax(ws)
    elif ol2 > ol1: 
        rl = np.argmax(ws)+1
    print(rl)  
    p1, _ = curve_fit(linefit, rvals[:rl], ws[:rl], (0,0))
    p2, _ = curve_fit(linefit, rvals[rl:], ws[rl:], (-p1[0],p1[1]))
    rideal = (p2[1]-p1[1]) / (p1[0]-p2[0])
    print(rideal,rcen+rideal)
    nc.piezo_scanmode_off()
    if np.abs(rideal) < 0.8:
        sync()
        umv(raxis,rcen + rideal)
    return rcen + rideal
    

    
def align_r_rough(raxis):
    _align_r_mca(raxis,rrange=1,rsteps=7,pzrange=45,pzsteps=60,ctim=0.05)
    
def align_r_fine(raxis):
    _align_r_mca(raxis,rrange=0.4,rsteps=7,pzrange=20,pzsteps=150,ctim=0.05)
       
def align_r_superfine(raxis):
    _align_r_mca(raxis,rrange=0.2,rsteps=5,pzrange=10,pzsteps=150,ctim=0.1)
   
def _align_r_mca(raxis,rrange=0.8,rsteps=5,pzrange=40,pzsteps=60,ctim=0.05):
    plotselect("mca:all_det0")
    shiftpzto50(ctim, 30, npts=50)
    f = flint()
    p1 = f.get_plot("curve", name="align rotation scans", unique_name="ARS")
    p1.clear_data()
    rcen = raxis.position
    nc.piezo_scanmode_off()
    if raxis == shrx:
        if np.abs((rot.position+90)%180)>1:
            umv(rot, 90)
    elif raxis == shry:
        if np.abs((rot.position)%180)>1:
            umv(rot, 0)
    nc.piezo_scanmode_on()
    ws = []
    rvals = np.linspace(-rrange,rrange,rsteps)+rcen
    for i,r in enumerate(rvals):
        sync()
        umv(raxis, r)
        if i%2 == 0:
            s = ascan(pz,50-pzrange,50+pzrange,pzsteps,ctim)
        else:
            s = ascan(pz,50+pzrange,50-pzrange,pzsteps,ctim)
        p1.add_curve( s.get_data('axis:pz'), s.get_data('mca:all_det0'), legend='%f degrees'%r)
        ws.append(fwhm())
    print(ws)
    print(rvals)
    p2 = f.get_plot("curve", name="align rotation FWHM", unique_name="ARF")
    p2.clear_data()
    p2.add_curve( rvals, ws)
    print("Select minimum position")
    spt = p2.select_points(1)
    print(spt[0][0])
    umv(raxis,spt[0][0])
    ascan(pz,50+pzrange,50-pzrange,pzsteps*2,ctim)
    nc.piezo_scanmode_off() 
   
    
    
    
def scanandcen(axis,rng,steps,ctim,ctr=mca):
    s = dscan(axis,-rng,rng,steps,ctim,ctr)
    x, y = s.get_data(axis.name), s.get_data('mca:all_det0')
    y = y-y.min()
    ny = np.clip(y, 0.1*y.max(), 0.9*y.max()) - (0.1*y.max()) 
    return (x*ny).sum()/ny.sum()

def roughzedge(ctim,rng,ctr=mca):
    s = dscan(shtz,-rng,+rng,20,ctim,ctr)
    x, y = s.get_data('shtz'), s.get_data('mca:all_det0')
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    edge = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    #edge = x[np.argmin(np.abs(y-( ( np.mean(y[25:20]) + np.mean(y[0:5]) )/2) ))]
    umv(shtz, edge)
    return edge

def pzedge(ctim,rng,npts=150,ctr=mca):
    s = ascan(pz,50-rng,50+rng,npts,ctim)
    x, y = s.get_data('pz'), s.get_data('mca:all_det0')
    ny = np.clip(y, 0.1*y.max(), 1*y.max()) - (0.1*y.max()) 
    ny = ny - (ny.max()/2)
    edge = np.interp(0, ny[:np.argmax(y)], x[:np.argmax(y)])
    #edge = x[np.argmin(np.abs(y-( ( np.mean(y[25:20]) + np.mean(y[0:5]) )/2) ))]
    umv(pz, edge)
    return edge
    
def pzpeak(ctim,rng,npts=150):
    s = ascan(pz,50-rng,50+rng,npts,ctim)
    x, y = s.get_data('pz'), s.get_data('mca:all_det0')
    maxz = x[np.argmax(y)]
    umv(pz, maxz)
    return maxz
    
def pzcen(ctim,rng,npts=150):
    s = ascan(pz,50-rng,50+rng,npts,ctim)
    plotselect('mca:all_det0')
    cenz = cen()
    umv(pz, cenz)
    return cenz
    
def pzcom(ctim,rng):
    s = ascan(pz,50-rng,50+rng,150,ctim)
    comz = com()
    umv(pz, 50)
    return comz
    
def shiftpzto50(ctim, rng, npts=150):
    pzcen = pzedge(ctim, rng, npts)
    umv(pz,50)
    sync()
    umvr(shtz, (pzcen-50)/1e3)
  
def shiftpzto50cen(ctim, rng, npts=150):
    pzc = pzcen(ctim, rng, npts)
    umv(pz,50)
    sync()
    umvr(shtz, (pzc-50)/1e3)   

def shiftpzto50peak(ctim, rng, npts=150):
    pzc = pzpeak(ctim, rng, npts)
    umv(pz,50)
    sync()
    umvr(shtz, (pzc-50)/1e3)      
    
    
def film_cor(rng, npts, ctim, ctr='eiger:roi_counters:roi1_sum'):
    cens = []
    plotselect(ctr)
    for j,k in enumerate([0,90,180,270]):
        umv(rot, k)
        if j%2 == 0:
            s = fscan(dty, -rng, rng*2/(npts-1), npts, ctim)
        else:
            s = fscan(dty, rng, -rng*2/(npts-1), npts, ctim)
        A = fscan.get_data()
        x, y = A['dty'], A[ctr]
        pk = np.clip(y-np.min(y), 0, 1e9)
        cens.append( (x*pk).sum()/pk.sum() )
        
    pyerr = (cens[0]-cens[2])/2
    pxerr = (cens[1]-cens[3])/2
    dtyideal = np.mean(cens)
    if (py.position+pyerr)>10 and (py.position+pyerr)<90:
        umvr(py, pyerr)
    else:
        umvr(shty, (pyerr/1000))
    if (px.position+pxerr)>10 and (px.position+pxerr)<90:
        umvr(px, pxerr)
    else:
        umvr(shtx, (pxerr/1000))
    umv(dty, dtyideal, rot, 0)
    

  

   
def move_to_position1():
    umv(det2y, -219.1400, ebsy, -5.8075, detz, 26.2341, ebsz, -24.3975)
       
def move_to_position2():
    umv(det2y, -216.1400, ebsy, -8.8075, detz, 16.2341, ebsz, -14.3975)
                


    
   
def checkbs():
    umv(edoor,0,dioderx,0)
    dscan(ebsy,-3.5,3.5,90,0.5,pico7)
    print(fwhm())
    if 3.3-fwhm() > 0.1:
            print('Poor ebs alignment')
            return False
    else:
        return True   


def temp_calib():        
    vs = list(range(0,26))
    print(vs)
    for v in vs:
        hmc.voltage_setpoint = v
        hmc.output = 'ON'
        umv(rot,0,dty,0)
        sleep(60)
        newdataset('%02dV'%(v))
        fscan(rot,0,180,2,15)

    
def gotoT(temp):
    vset = temptovolt(temp)
    print('Going to %f V for %f deg C'%(vset,temp))
    hmc.voltage_setpoint = vset
    hmc.output = 'ON'
    print('Heating to %s...'%str(temp))
    sleep(3)
    
def temptovolt(t):
    return 0.04913286596171511*t + 3.243259815561499
    
    
def uptoconstanttemp_2pos():
    temps = np.arange(110,470,10)
    #temps = [20,*temps]
    print(temps)
    nc.piezo_scanmode_off()
    for temp in temps:
        print(temp)
        gotoT(temp)
        print('align_%02d'%temp)
        newdataset('align_%02d'%temp)
        move_to_position1()
        umv(rot,0)
        tfoh1.set(0,'Be')
        nc.piezo_scanmode_on()
        shiftpzto50(0.05,30,120)
        sync()
        pzedge(0.05,10,200)
        umvr(pz,0.4)
        where()
        
        tfoh1.set(12,'Be')
        nc.piezo_scanmode_off()
        print('fscan_%02d_p1'%temp)
        newdataset('fscan_%02d_p1'%temp)
        fscan(rot,0,0.1,3600,0.15)
        move_to_position2()
        print('fscan_%02d_p2'%temp)
        newdataset('fscan_%02d_p2'%temp)
        fscan(rot,360,-0.1,3600,0.15)
        move_to_position1()
        tfoh1.set(0,'Be')

    temp = 460
    for i in range(1,8):      
        print('align_%02d_%02d'%(temp,i))     
        newdataset('align_%02d_%02d'%(temp,i))
        move_to_position1()
        umv(rot,0)
        tfoh1.set(0,'Be')
        nc.piezo_scanmode_on()
        shiftpzto50(0.05,30,120)
        sync()
        pzedge(0.05,10,200)
        umvr(pz,0.4)
        where()
        
        tfoh1.set(12,'Be')
        nc.piezo_scanmode_off()
        print('fscan_%02d_%02d_p1'%(temp,i))
        newdataset('fscan_%02d_%02d_p1'%(temp,i))
        fscan(rot,0,0.1,3600,0.15)
        move_to_position2()
        newdataset('fscan_%02d_%02d_p2'%(temp,i))
        fscan(rot,360,-0.1,3600,0.15)
        move_to_position1()
        tfoh1.set(0,'Be')
        
    cooldown()


   
def coolscan_2pos():
    print('align_cool')
    newdataset('align_cool')
    move_to_position1()
    umv(rot,0)
    tfoh1.set(0,'Be')
    nc.piezo_scanmode_on()
    shiftpzto50(0.05,30,120)
    sync()
    pzedge(0.05,10,200)
    umvr(pz,0.4)
    where()
        
    tfoh1.set(12,'Be')
    nc.piezo_scanmode_off()
    print('fscan_cool_p1')
    newdataset('fscan_cool_p1')
    fscan(rot,0,0.1,3600,0.15)
    move_to_position2()
    print('fscan_cool_p2')
    newdataset('fscan_cool_p2')
    fscan(rot,360,-0.1,3600,0.15)
    move_to_position1()
    tfoh1.set(0,'Be')   

   
def coolscan():
    print('align_cool')
    newdataset('align_cool')
    umv(rot,0)
    tfoh1.set(0,'Be')
    nc.piezo_scanmode_on()
    shiftpzto50(0.05,30,120)
    sync()
    pzedge(0.05,10,200)
    umvr(pz,0.4)
    where()
        
    tfoh1.set(12,'Be')
    nc.piezo_scanmode_off()
    print('fscan_cool')
    newdataset('fscan_cool')
    fscan(rot,0,0.1,3600,0.15)
    tfoh1.set(0,'Be')

 
def uptoconstanttemp():
    temps = np.arange(100,470,20)
    temps = [20,*temps]
    print(temps)
    nc.piezo_scanmode_off()
    for temp in temps:
        print(temp)
        gotoT(temp)
        print('align_%02d'%temp)
        newdataset('align_%02d'%temp)
        umv(rot,0)
        nc.piezo_scanmode_on()
        shiftpzto50peak(0.05,30,120)
        sync()
        pzpeak(0.05,10,200)
        where()
        
        nc.piezo_scanmode_off()
        tfoh1.set(12,'Be')
        print('fscan_%02d'%temp)
        newdataset('fscan_%02d'%temp)
        fscan(rot,0,0.1,3600,0.28)
        tfoh1.set(0,'Be')

    temp = 460
    for i in range(1,4):      
        print('align_%02d_%02d'%(temp,i))     
        newdataset('align_%02d_%02d'%(temp,i))
        umv(rot,0)
        nc.piezo_scanmode_on()
        shiftpzto50peak(0.05,30,120)
        sync()
        pzpeak(0.05,10,200)
        
        nc.piezo_scanmode_off()
        tfoh1.set(12,'Be')
        print('fscan_%02d_%02d'%(temp,i))
        newdataset('fscan_%02d_%02d'%(temp,i))
        fscan(rot,0,0.1,3600,0.28)
        tfoh1.set(0,'Be')
        
    cooldown()  
    sleep(100) 
    coolscan()
    
    
    
    
def cooldown():
    vs = hmc.voltage_setpoint
    vs = np.linspace(vs,0,int(vs)+1)
    print(vs)
    for v in vs:
        print(v)
        hmc.voltage_setpoint = v
        hmc.output = 'ON'
        sleep(20)
    hmc.output = 'OFF'
    print('cool!')
