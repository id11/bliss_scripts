import numpy as np

def dct_marana(scanname, startpos, stepsize, nproj, exp_time, ystep, refon, nref, scanmode):
    newdataset(scanname)
    max_im = 50000
    print("resetting samtx and samty positions to 0")
    #samtx.position = 0
    #samty.position = 0
    umv(diffrz, startpos)
    for ref_grp in np.arange(0, nproj/refon, 1):
        startpos_grp = ref_grp * refon * stepsize + startpos
        ref_scan_samy(exp_time, nref, ystep, startpos_grp, scanmode)
        num_im = max_im
        for scan_grp in np.arange(0, np.float(refon) / max_im, 1):
            startpos_scan = startpos_grp + scan_grp * max_im * stepsize
            if np.mod(refon, max_im):
                if (scan_grp == np.floor(refon / max_im)):
                    num_im = np.mod(refon, max_im)
            print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_scan, stepsize, num_im, exp_time, scanmode))
            fscan(diffrz, startpos_scan, stepsize, num_im, exp_time, scan_mode = scanmode)
    ref_scan_samy(exp_time, nref, ystep, startpos + nproj*stepsize, scanmode)
    fsh.disable()
    print("taking dark images")
    ref_scan_samy(exp_time, nref, 0, startpos + nproj*stepsize, scanmode)
    fsh.enable()
    umv(diffrz, startpos)

def dct_marana_dict(scanname, dct_pars):
    newdataset(scanname)
    umv(diffrz, dct_pars['start_pos'])
    num_im_grp = dct_pars['nproj']/dct_pars['refon']
    for ref_grp in np.arange(0, dct_pars['nproj']/dct_pars['refon'], 1):
        startpos_grp = ref_grp * dct_pars['refon'] * dct_pars['step_size'] + dct_pars['start_pos']
        ref_scan_samy(dct_pars['exp_time'], dct_pars['nref'], dct_pars['ystep'], startpos_grp, dct_pars['scan_mode'])
        print("fscan(diffrz, %6.2f, %3.2f, %d, %f, scan_mode=%s)" % (startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], dct_pars['scan_mode']))
        fscan(diffrz, startpos_grp, dct_pars['step_size'], num_im_grp, dct_pars['exp_time'], scan_mode = dct_pars['scan_mode'])
    ref_scan_samy(dct_pars['exp_time'], dct_pars['nref'], dct_pars['ystep'], dct_pars['start_pos'] + dct_pars['nproj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.disable()
    print("taking dark images")
    ref_scan_samy(dct_pars['exp_time'], dct_pars['nref'], 0, dct_pars['start_pos'] + dct_pars['nproj']*dct_pars['step_size'], dct_pars['scan_mode'])
    fsh.enable()
    umv(diffrz, dct_pars['start_pos'])

def dct_interlaced(scanname, startpos, stepsize, nproj, exp_time, ystep, refon, nref, scanmode):
    newdataset(scanname)
    print("resetting samtx and samty positions to 0")
    samtx.position = 0
    samty.position = 0
    
    for ref_grp in np.arange(0, nproj/refon, 1):
        startpos_grp = ref_grp * refon * stepsize + startpos
        ref_scan(exp_time, nref, ystep, startpos_grp, 'TIME')
        print("finterlaced(diffrz, %6.2f, %3.2f, %d, %f, mode=%s)" % (startpos_grp, stepsize, refon, exp_time, scanmode))
        finterlaced(diffrz, startpos_grp, stepsize, refon, exp_time, mode=scanmode)
    ref_scan(exp_time, nref, ystep, startpos + nproj*stepsize, 'TIME')
    fsh.disable()
    print("taking dark images")
    ref_scan(exp_time, nref, 0, startpos + nproj*stepsize, 'TIME')
    fsh.enable()
    umv(diffrz, startpos)

def ref_scan_samy(exp_time, nref, ystep, omega, scanmode):
    umvr(samy, ystep)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    umvr(samy, -ystep)
    
def ref_scan(exp_time, nref, ystep, omega, scanmode):
    samtx_pos = ystep * np.sin(np.deg2rad(omega))
    samty_pos = ystep * np.cos(np.deg2rad(omega))
    print("umv(samtx %6.4f samty %6.4f)" % (samtx_pos, samty_pos))
    umv(samtx, samtx_pos, samty, samty_pos)
    print("fscan(diffrz, %6.2f, 0.001, %d, %f, 0.0, scan_mode=%s)" % (omega, nref, exp_time, scanmode))
    fscan(diffrz, omega, 0.001, nref, exp_time, scan_mode = scanmode)
    print("umv(samtx %6.4f samty %6.4f)" % (0, 0))
    umv(samtx, 0, samty, 0)
