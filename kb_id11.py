

ECONST=12.398419843320028
KBM1_WALL = 920
KBM2_WALL = 480
# updated 8-3-2016 JPW
WALL_SAMPLE = 700
WALL_BPM=323
ML_XTAL = 20.95

from numpy import tan, arctan, arcsin, degrees

#offsets for the ll/bb-beam + kb-system 

def tantth_ml(energy):
   return(tan(2*arcsin(ECONST/energy/2.0/ML_XTAL)))

def _kb_beam_pos(energy,distance):
    return(distance*tantth_ml(energy))

def _kb_beam_angle(energy):
    return degrees(arctan( tantth_ml( energy )))

def _kb_RMv(energy):
    # sept2013 http://wikiserv.esrf.fr/id11/index.php/Multilayer
    return 144.226/energy + 0.2578


def _kb_RMh(energy):
    # sept2013 http://wikiserv.esrf.fr/id11/index.php/Multilayer
    return 146.559/energy + 0.1395


def kb_offset(energy):
    """ distances to offset at this energy """
    E2 = energy
    print("The beam offset at s8y is %f mm and at s8z is %f mm"%(
          _kb_beam_pos(E2,KBM2_WALL+WALL_BPM),
          _kb_beam_pos(E2,KBM1_WALL+WALL_BPM)))
    print("The beam offset at diffty is the monochromator offset plus  %f mm  and difftz is %f mm"%(
          _kb_beam_pos(E2,KBM2_WALL+WALL_SAMPLE),
          _kb_beam_pos(E2,KBM1_WALL+WALL_SAMPLE)))
    print("We think RMv and RMh at %f should be RMv %f  RMh %f"%( E2, _kb_RMv(E2),_kb_RMh(E2)))
    print("Uphill beam angle at %f is %f (negative diffry movement)"%(E2, _kb_beam_angle(E2)))

