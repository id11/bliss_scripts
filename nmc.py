

import time, numpy as np

# def check_cpm18(pos=6.4158):
def check_cpm18(pos=7.3533):
    cpm18.sync_hard()
    if abs( cpm18.position - pos ) > 0.03:
        user.cpm18_goto(pos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    check_cpm18()



def nmc3d():
    for z in np.arange(48,52.01,0.1):
        umv(pz, z)
        pause_for_refill( 30 )
        newdataset( 'Z%d'%(z*1000))
        fscan2d( dty, -2.3, 0.1, 2*2.3/0.1+1,
                 rot, -1, 0.125, 362/0.125, 0.005, )

def fscanloop( ):
    for i, y in enumerate( np.arange( -2.3, 2.301, 0.1 ) ):
        umv(dty, y )
        pause_for_refill( 30 )
        if i%2 == 0:
            fscan(rot, -90,  0.125, 180/0.125, 0.005, scan_mode = 'CAMERA' )
        else:
            fscan(rot,  90, -0.125, 180/0.125, 0.005, scan_mode = 'CAMERA' )

def nmc3d_loop():
    for z in np.arange(48,52.01,0.1):
        umv(pz, z)
        newdataset( 'loopZ%d'%(z*1000))
        fscanloop()

def nmc3d_loop2():
    for z in np.arange(49.4,52.01,0.1): 
        umv(pz, z)
        newdataset( 'loop3Z%d'%(z*1000))
        fscanloop()

def nmc3d_rpt():
    for z in np.arange(50.7, 45.99, -0.1): 
        umv(pz, z)
        newdataset( 'rptZ%d'%(z*1000))
        fscanloop()                

        
