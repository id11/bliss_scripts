#user_script_load('clicktomove',True)
import numpy as np


def afscan(mot, start, stop, npts, ctim, *counterargs, **kwargs):
    fscan(mot, start, (stop-start)/npts, npts, ctim, *counterargs, **kwargs)

def dfscan(mot, start, stop, npts, ctim, *counterargs, **kwargs):
    stpos = mot.position
    fscan(mot, mot.position+start, (stop-start)/npts, npts, ctim, *counterargs, **kwargs)
    umv(mot, stpos)


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def fmesh(s_mot, s_start, s_step, s_np, f_mot, f_start, f_step, f_np, ctim, ctr='roi1_avg', sinorecon=False):
    """
    fscans f_mot back and forth, stepping s_mot in between
    plots ctr in flint, updating after each scan
    if sinorecon=True, reconstructs at end of scan
    """
    f_pos = np.arange( f_start, f_start + (f_step * f_np), f_step )
    f_stop = f_pos[-1] + f_step
    estim = len(f_pos)*ctim
    s_pos = np.arange( s_start, s_start + (s_step * s_np), s_step )
    
    p = flint().get_plot("scatter", name='fmesh')
    
    fmesh, smesh = np.meshgrid(f_pos,s_pos)
    fmesh, smesh = np.ravel(fmesh), np.ravel(smesh)
    imesh = np.zeros(fmesh.shape)
    for i, sp in enumerate(s_pos):
        umv(s_mot, sp)
        pause_for_refill( estim )
        if (i % 2) == 0: # forwards
            fscan( f_mot, f_start, f_step, f_np, ctim )
        else: # backwards
            fscan( f_mot, f_stop, -f_step, f_np, ctim )
        A = fscan.get_data()
        fmesh[i*f_np:(i+1)*f_np] = A[f_mot.name]
        imesh[i*f_np:(i+1)*f_np] = A[ctr]
        p.set_data(fmesh, smesh, imesh)

    if sinorecon: # reconstruct
        h,a,y = np.histogram2d( fmesh, smesh, weights = imesh, bins=(len(f_pos),len(s_pos)) )
        recon = skimage.transform.iradon( h.T, f_pos, circle=True )
        reconplot = f.get_plot("image", name='fmesh recon')
        reconplot.side_histogram_displayed = False
        reconplot.set_data( recon )



"""
def easy_edge_energy(element,u22pos,cpm18pos):
    ll_edge_scan(element)
    plotselect('keithley:pico0')
    goto_click()
    dscan(llbragg1,0.01,-0.01,40,0.1,pico0)
    goto_click()
    where()
    ll_foils_and_diode()
    umv(llbragg2,llbragg1.position)
    undpeaks(ll_foils_energies[element])
    umv(u22,u22pos)
    cpm18_goto(cpm18pos)    
    lrock2(0.02)
    lrock2()
    dscan(u22,-0.1,0.1,20,0.1,pico0)
    goto_click()
    dscan(cpm18,-0.1,0.1,20,0.1,pico0)
    cpm18_goto(peak())
    """
    
