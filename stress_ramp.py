import sys
import numpy as np
import time
from datetime import datetime
import os

def _get_human_time():
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")

def hold_target(target, time_step=0.5, dead_band = 0.1):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    current_load = stress_adc.get_value()
    while True:
	    if target > current_load + dead_band:
		    stress_increment = 0.1
		    while (current_load < target):
			    mvr(stress, stress_increment)
			    current_load = stress_adc.get_value()
			    sleep(time_step)
			    print(_get_human_time(),stress.position,current_load)
	    else:
		    stress_increment = -0.1
		    while (current_load - dead_band > target):
		   	    mvr(stress, stress_increment)
		   	    current_load = stress_adc.get_value()
		   	    sleep(time_step)
		   	    print(_get_human_time(),stress.position,current_load)
    return

def load_ramp_by_target(target, time_step=0.5,to_file=None):
    """Increases / Decreases Nanox Piezo Voltage until taget load is reached 
       TO BE DONE: record load values during this ramp and save them into a file !
    """
    write_to_file = to_file if to_file is not None else sys.stdout
    marana_large_beam(pct_pars)
    current_load = stress_cnt.get_value()
    if abs(target - current_load) < 1:
        #print('target position is to close to current position - aborting !',
        #      file=write_to_file)
        print(_get_human_time(),stress.position,current_load,file=write_to_file)
        return
    if target > current_load:
        stress_increment = 0.1
        while (current_load < target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    else:
        stress_increment = -0.1
        while (current_load > target):
            mvr(stress, stress_increment)
            current_load = stress_cnt.get_value()
            sleep(time_step)
            print(_get_human_time(),stress.position,current_load,file=write_to_file)
    return
