import sys
import numpy as np
import time
import os
from scipy.io import loadmat
import gevent
from datetime import datetime

sample_name = SCAN_SAVING.collection_name
basedir = '%s/%s/%s/%s'%(SCAN_SAVING.base_path,SCAN_SAVING.proposal_name,SCAN_SAVING.beamline,sample_name)
tt_detector = frelon1

def refine_tt_alignment_int(tt_grain, ang_step=0.05, search_range=1, exp_time=0.5, scan_mode='TIME'):
    """Refine the topotomo samrx, samry alignments with interactive roi and scan range selection
    
    This function runs 2 base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. 
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use (in degrees).
    :param float search_range: the base tilt angular search range in degrees.
    """
    n_search_images = 2 * search_range / ang_step
    gid = tt_grain['gr_id']
    print('find range for grain %d\n' % gid)     
    # align our grain
    topotomo_tilt_grain_dict(tt_grain)
    # first scan at 90 deg
    umvct(diffrz, 90)
    if 'roi' in tt_grain:
        frelon16.roi_counters.set('roi1', tt_grain['roi'])
    else:
	    fscan(diffry, tt_grain['diffry'] - search_range, 2*search_range, 1, exp_time*search_range/ang_step/5, scan_mode = scan_mode)
	    edit_roi_counters(frelon16)
    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_090, end_angle_090, cen_angle_090 = get_limits_int(fscan.get_data())
    # refine samrx
    samrx_offset = cen_angle_090 - tt_grain['diffry']
    print("moving samrx by %.3f, final position: %.3f" % (samrx_offset, tt_grain['samrx'] + samrx_offset))
    umvr(samrx, samrx_offset)
    # second scan at 0 deg
    umv(diffrz, 0)
    if 'roi' in tt_grain:
        frelon16.roi_counters.set('roi1', tt_grain['roi'])
    else:
	    fscan(diffry, tt_grain['diffry'] - search_range, 2*search_range, 1, exp_time*search_range/ang_step/5, scan_mode = scan_mode)
	    edit_roi_counters(frelon16)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_000, end_angle_000, cen_angle_000 = get_limits_int(fscan.get_data())
    # refine samry
    samry_offset = cen_angle_000 - tt_grain['diffry']
    print("moving samry by %.3f, final position: %.3f" % (samry_offset, tt_grain['samry'] + samry_offset))
    umvr(samry, samry_offset)
    # in case of large sample tilts, we have to refine with sample_tilts
    umv(diffry, tt_grain['diffry'])
    reduced_range = (end_angle - start_angle) / 2
    dscan(samry, -reduced_range, reduced_range, 2*reduced_range/ang_step, exp_time)
    cen()
    goto_click(1)
    # refine samrx
    umv(diffrz, 90)
    reduced_range = (end_angle_090 - start_angle_090) / 2
    dscan(samrx, -reduced_range, reduced_range, 2*reduced_range/ang_step, exp_time)
    cen()
    goto_click(1)
    
    # update values in tt_grain
    tt_grain['samrx'] = samrx.position
    tt_grain['samry'] = samry.position
    tt_grain['roi'] = frelon16.roi_counters['roi1'].get_params()
    return tt_grain

def get_limits_int(scan_data):
    intensity = scan_data['frelon1:roi_counters:roi1_avg']
    pl = flint().get_live_plot("default-curve")
    pos = pl.select_points(2)
    start_pos = round(pos[0][0])
    end_pos   = round(pos[1][0])
    print(start_pos, end_pos)
    slope = (pos[1][1] - pos[0][1])/(end_pos - start_pos)
    print(slope)
    weighted_cen_pos = 0;
    signal=[0] * (end_pos+1)
    for i_pos in range(start_pos, end_pos+1, 1):
	    print(i_pos)
	    bg = pos[0][1] + slope * (i_pos - start_pos)
	    signal[i_pos] = intensity[i_pos] - bg
	    weighted_cen_pos += i_pos * signal[i_pos]
	    print(i_pos, bg, weighted_cen_pos)
    weighted_cen_pos = weighted_cen_pos / np.sum(signal[start_pos:end_pos+1])
    print(weighted_cen_pos)
    # compute the angle using linear interpolation
    #weighted_cen_angle = (diffry_vals[start_pos] * (end_pos - weighted_cen_pos) + diffry_vals[end_pos] * (weighted_cen_pos - start_pos)) / (end_pos - start_pos)
    #start_angle = diffry_vals[start_pos] - padding * ang_step
    #end_angle = diffry_vals[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, weighted_cen_angle={}, number of images={}'.format(start_angle, end_angle, weighted_cen_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle, weighted_cen_angle
   
    
def find_range(tt_grain, ang_step=0.05, search_range=0.3, exp_time=0.1, scan_mode='TIME', step=1):
    """Find the topotomo angular range.
    
    This function runs two base tilt scans at 90 deg rotation offset to figure 
    out the angular range needed to acquire a full topotono scan. The function 
    outputs the bliss commands to run the acquisition. Note that this function 
    assumes that the grain has been aligned already.
    
    :param dict tt_grain: a dictionnary containing the main topotomo 
    acquisition parameter for a given grain.
    :param float ang_step: the angular step to use.
    :param float search_range: the base tilt angular search range in degrees.
    """
    n_search_images = 2 * search_range / ang_step
    data_dir = '/data/visitor/ma4925/id11/'
    sample_name = 'Inconel'
    
    # open a script to gather all acquisition commands
    gid = tt_grain['gr_id']
    cmd_path = os.path.join(data_dir, 'tt_acq_grain_%d_step_%d.py' % (gid, step))
    f = open(cmd_path, 'w')
    f.write("def tt_acq():\n\n")
    f.write("    # activate ROI\n")
    f.write("    frelon16.image.roi = [561, 561, 800, 800]\n")
    f.write("    frelon16.roi_counters.set('roi1', [200, 200, 400, 400])\n")
    f.write("    ct(0.1)\n")
    f.write("    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')\n\n")
    
    print('find range for grain %d\n' % gid)
    
    # see if we need to create a new dataset or not
    #dataset = 'grain_%04d_checkrange_step_%d' % (gid, step)
    #newdataset(dataset)

    # define the ROI automatically as [710, 710, 500, 500]
    #frelon16.roi_counters.set('roi1', [710, 710, 500, 500])
    ACTIVE_MG.enable('frelon16:roi_counters:roi1_avg')
    #ACTIVE_MG.enable('marana3:roi_counters:roi1_avg')
    # first scan at 90 deg
    datasetname = SCAN_SAVING.dataset_name
    umv(diffrz, 90)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode = scan_mode)
    start_angle_90, end_angle_90 = get_limits(fscan.get_data())
    #start_angle_90, end_angle_90 = get_limits_by_seg(fscan.get_data(), data_dir, sample_name, datasetname, step, scan_fold_ind = 5)
    diffry_range_90 = max(tt_grain['diffry'] - start_angle_90, end_angle_90 - tt_grain['diffry'])

    # second scan at 0 deg
    umv(diffrz, 0)
    fscan(diffry, tt_grain['diffry'] - search_range, ang_step, n_search_images, exp_time, scan_mode= scan_mode)
    start_angle_00, end_angle_00 = get_limits(fscan.get_data())
    #start_angle_00, end_angle_00 = get_limits_by_seg(fscan.get_data(), data_dir, sample_name, datasetname, step, scan_fold_ind = 6)
    diffry_range_00 = max(tt_grain['diffry'] - start_angle_00, end_angle_00 - tt_grain['diffry'])

    # use the upper range value to define topotomo bounds
    print('diffry_range_00=%.3f - diffry_range_90=%.3f' % (diffry_range_00, diffry_range_90))
    diffry_range = max(diffry_range_00, diffry_range_90)

    # now find out the largest range to cover the grain
    diffry_start = tt_grain['diffry'] - diffry_range
    diffry_end = tt_grain['diffry'] + diffry_range
    n_acq_images = (diffry_end - diffry_start) / ang_step

    # write out the topotomo command for bliss
    f.write("    # create a new data set\n")
    f.write("    newdataset('grain_%04d_step_%d')\n\n" % (gid, step))
    f.write("    # acquisition for grain %d\n" % gid)
    f.write("    user.topotomo_tilt_grain(%d)\n" % gid)
    f.write("    umv(samrx, %.3f)\n" % tt_grain['samrx'])
    f.write("    umv(samry, %.3f)\n" % tt_grain['samry'])
    f.write("    fscan2d(diffrz, 0, 10, 36, diffry, %.3f, %.3f, %d, 0.1, scan_mode=tt_pars['scan_mode'])\n\n" % (diffry_start, ang_step, n_acq_images))
    f.close()
    return diffry_start, n_acq_images

def get_limits(scan_data, thres=0.1, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry = scan_data['diffry']
    # work out the angular step
    ang_step = diffry[1] - diffry[0]
    intensity = scan_data['frelon16:roi_counters:roi1_avg']
    # background correction
    bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) -1
    start_angle = diffry[start_pos] - padding * ang_step
    end_angle = diffry[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, number of images={}'.format(start_angle, end_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle
    
def get_limits_and_weighted_cen_angle(scan_data, thres=0.1, padding=10):
    """Process rocking curve scan data to find the start and end values.
    
    :param scan_data: scan data as obtained by fscan.get_data().
    :param float thres: threshold to use to find the limits of the rocking curve.
    :param int padding: number of images to add before and after the rocking curve limits.
    """
    diffry_vals = scan_data['diffry']
    # work out the angular step
    ang_step = diffry_vals[1] - diffry_vals[0]
    intensity = scan_data['frelon16:roi_counters:roi1_avg']
    # background correction
    #bg = (np.sum(intensity[:2]) + np.sum(intensity[-2:])) / 4
    bg = np.min(intensity)
    intensity -= bg
    int_max = intensity.max()
    start_pos = np.argmax(intensity > int_max * thres)
    end_pos = len(intensity) - np.argmax(intensity[::-1] > int_max * thres) - 1
    end_pos = max(start_pos + 1, end_pos)
    weighted_cen_pos = 0;
    for i_pos in range(start_pos, end_pos, 1):
        weighted_cen_pos += i_pos * intensity[i_pos]
    weighted_cen_pos = weighted_cen_pos / np.sum(intensity[start_pos:end_pos])
    # compute the angle using linear interpolation
    weighted_cen_angle = (diffry_vals[start_pos] * (end_pos - weighted_cen_pos) + diffry_vals[end_pos] * (weighted_cen_pos - start_pos)) / (end_pos - start_pos)
    start_angle = diffry_vals[start_pos] - padding * ang_step
    end_angle = diffry_vals[end_pos] + padding * ang_step
    print('start_angle={}, end angle={}, weighted_cen_angle={}, number of images={}'.format(start_angle, end_angle, weighted_cen_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle, weighted_cen_angle
    
def clicked_pos(x,y):
     c2mplot = flint().get_live_plot('default-curve')
     print("Select start and end position in flint")
     pos = c2mplot.select_points(2)
     return pos
 

def define_pars(load_list):
    """Produces default input parameters for a load sequence (see next function)
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    for load in load_list:
        step_pars['dct_pars'] = define_dct_pars()
        step_pars['pct_pars'] = define_pct_pars()
        step_pars['tt_pars'] = define_tt_pars()
        step_pars['ff_pars'] = define_ff_pars()
        step_pars['sff_pars']= define_sff_pars()
        step_pars['target'] = load
        step_pars['load_step'] = step
        step = step + 1
        par_list.append(step_pars)        
    return par_list

def define_pars_one_load():
    """Produces default input parameters for one load
       target = target load (N) for Nanox
       step = load step number (naming of scans)
    """
    step_pars = {}
    par_list=[]
    step = 1
    step_pars['dct_pars'] = define_dct_pars()
    step_pars['pct_pars'] = define_pct_pars()
    step_pars['tt_pars'] = define_tt_pars()
    step_pars['ff_pars'] = define_ff_pars()
    step_pars['sff_pars']= define_sff_pars()
    step_pars['target'] = load
    step_pars['load_step'] = step
    par_list.append(step_pars)
    return par_list


def load_sequence(par_list, grain_list):
    """Load Experiment Master Sequence.
    
    This function will perform scan sequences (pct, dct, tt) for a list of target load values defined in "par_list" 
    Note: grain_list is currently produced by a matlab script and can be re-created / imported via:  grain_list = user.read_tt_info([list_of_grain_ids])
    par_list can be created via: par_list = user.define_pars()  and sub-parameters for dct, pct, tt can be adapted to cope with increasing mosaicity

    """
    for step_pars in par_list:
        dct_pars = step_pars['dct_pars']
        pct_pars = step_pars['pct_pars']
        tt_pars = step_pars['tt_pars']
        ff_pars = step_pars['ff_pars']
        sff_pars = step_pars['sff_pars']
        target = step_pars['target']
        load_step = step_pars['load_step']
        grain_list = load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, ff_pars, sff_pars, target, load_step)
    return grain_list

def load_step_sequence(grain_list, dct_pars, pct_pars, tt_pars, ff_pars, sff_pars, target, loadstep):
    """Performs a loadramp to the new target value and launches PCT, DCT and a series of TT scans at this new target load
    """
    ## PCT
    maranain(pct_pars['dist'])
    marana_large_beam(pct_pars)
    #load_ramp_by_target(target)
    scan_name = 'pct_%dN_' % target
    tomo_by_fscan_dict(scan_name, pct_pars)
    ## DCT
    marana_dct(dct_pars)
    scan_name = 'dct_%dN_' % target
    dct_zseries(dct_pars, datasetname = scan_name)
    ## TT
    scan_name = 'tt_%dN_' % target
    marana_tt(grain_list, tt_pars, target)
    ##frelon16in(4, tt_pars)
    ##grain_list = loop_grains(grain_list, tt_pars, target)
    ## 3DXRD
    ffin()
    scan_name = 'ff_%dN_' % target    
    ff_zseries(ff_pars, scan_name)
    ## s-3DXRD
    #scan_name = 'sff_%dN_g%d' % (target, grain_list[0]['gr_id'])
    #tdxrd_pointscan(scan_name, sff_pars, grain)
    scan_name = 'sff_2d_%dN_g%d' % (target, grain_list[0]['gr_id'])
    sff_one_grain(scan_name, sff_pars, grain_list[0])
    return grain_list


def loop_grains(grain_list, tt_pars, target):
    """Loops through a list of Topotomo grains  and performs refinement + TT scan_acquisition
    :param dict grain_list:  list of grain structures containing Topotomo alignment values
    :param dict tt_pars: Topotomo scan parameters
    :param int step: running number of load step in load-sequence  
    """
    tfoh1.set(16,'Be')
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    marana3.image.roi = tt_pars['image_roi'] # [510, 510, 900, 900]
    marana3.roi_counters.set('roi1', tt_pars['counter_roi'])  #[300, 300, 300, 300])
    ACTIVE_MG.enable('marana3:roi_counters:roi1_avg')
    for i in range(len(grain_list)):
        newdataset('grain_%04d_%dN_' % (grain_list[i]['gr_id'], target))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time)
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'])
        sleep(1)
        #update_grain_info(grain_list[i])
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    sam_dct_pos()
    return grain_list



        




    print('start_angle={}, end angle={}, weighted_cen_angle={}, number of images={}'.format(start_angle, end_angle, weighted_cen_angle, int((end_angle - start_angle) / ang_step + 1)))    
    return start_angle, end_angle, weighted_cen_angle




def marana_tt(grain_list, tt_pars, target):
    tfoh1.set(0,'Be')
    umv(s7hg, tt_pars['slit_hg'], s7vg, tt_pars['slit_vg'])
    umv(d3tz, d3tz_pos_tt, nfdtx, tt_pars['dist'])
    marana3.image.roi = tt_pars['image_roi']
    marana3.roi_counters.set('roi1', tt_pars['counter_roi'])
    ACTIVE_MG.enable('marana3:roi_counters:roi1_avg')
    for i in range(len(grain_list)):
        newdataset('grain_%04d_%dN_' % (grain_list[i]['gr_id'], target))
        exp_time = tt_pars['exp_time'] / grain_list[i]['int_factor']
        grain_list[i] = refine_tt_alignment(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], exp_time, tt_pars['scan_mode'])
        diffry_start, n_acq_images = find_range(grain_list[i], tt_pars['diffry_step'], tt_pars['search_range'], tt_pars['exp_time'], tt_pars['scan_mode'])
        sleep(1)
        update_grain_info(grain_list[i])
        fscan2d(diffrz, 0, tt_pars['diffrz_step'], tt_pars['num_proj'], diffry, diffry_start, tt_pars['diffry_step'], n_acq_images, exp_time, scan_mode=tt_pars['scan_mode'])
    sam_dct_pos()
    return grain_list

