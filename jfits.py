
from bliss.scanning.scan_tools import get_selected_counter_name
#from bliss.common.plot import default_plot
import numpy as np

JFIT = {}

def jplot():
    #p = default_plot()
    #p.plot({'yraw' : JFIT['yraw'], 'x'=JFIT['xraw']} )
    #p.add_data( JFIT['y'], x=JFIT['x'] ) 
    import pylab as pl
    pl.plot( JFIT['xraw'], JFIT['yraw'], '-+')
    pl.plot( (JFIT['pos'], JFIT['pos']),
              (JFIT['yraw'].min(),  JFIT['yraw'].max()), 'k-')
#    pl.plot( JFIT['x'], JFIT['y']+JFIT['cut'], 'o')
#    pl.plot( (JFIT['pos'],JFIT['pos']),
#             (JFIT['yraw'].min(),JFIT['yraw'].max()),"k-")
#    pl.plot( (JFIT['x'].min(),JFIT['x'].max()),
#             (JFIT['cut'],JFIT['cut']),"-")
    pl.show()
    

def jcen( xarg, yarg ):
    """4th attempt"""
    clean = np.isfinite(xarg) & np.isfinite(yarg)
    cls = clean.sum()
    if cls < len(xarg) or cls < 2:
        if cls == 0:
            return xarg[0]
        if cls < 3:
            return xarg[clean].mean()
        x, y = xarg[clean], yarg[clean]
    else:
        x, y = xarg, yarg
    # check if x is sorted:
    xgt = x[1:] > x[:-1]
    if xgt.all() or (~xgt).all():
        pass  # no need to sort
    else:
        order = np.argsort(x)
        x = x[order]
        y = y[order]
    # Limits and center in y (susceptible to noise - can you do better?)
    iymax = np.argmax(y)
    iymin = np.argmin(y)
    if y[iymin] == y[iymax]:
        return x.mean()
    cut = 0.5*(y[iymax] + y[iymin])
    below = y < cut
    # crossing points, even or odd as classifier
    cross = below[1:] != below[:-1]
    crs = cross.sum()
    inds = np.arange( 1,len(x), dtype=int )[cross]
    if crs % 2 == 0: # even crossings, peak or dip
        if crs > 2:  # which peak to choose?
            if y[inds[0]-1] < y[inds[0]]:  # peak
                iyp = iymax
            else:                          # dip
                iyp = iymin
            ipk = np.searchsorted( inds, iyp )
            xpk = x[inds[ipk-1]:inds[ipk]]
            ypk = y[inds[ipk-1]:inds[ipk]] - cut 
        else:
            xpk = x[inds[0]:inds[1]]
            ypk = y[inds[0]:inds[1]] - cut
        pos = np.average( xpk, weights = ypk )
        JFIT['pos']=pos
        return pos                   
    # steps
    else:            # odd, step up or down
        if crs == 1:
#            print(cut, inds, y[inds])
            order = np.argsort(y[inds[0]-2:inds[0]+2])
#            print(           y[inds[0]-2:inds[0]+2][order])
#            print(           x[inds[0]-2:inds[0]+2][order])
            pos = np.interp( cut,
                             y[inds[0]-2:inds[0]+2][order],
                             x[inds[0]-2:inds[0]+2][order])
            JFIT['pos']=pos
            JFIT['cut']=cut
            return pos
        # several cuts, just try to average in the center
        yc = y[ inds[0] : inds[-1]]
        xc = x[ inds[0] : inds[-1]]
        p = np.polyfit( yc, xc, 3)
        pos = np.polyval( p, cut )
        JFIT['pos']=pos
        return pos



def jtop(xarg, yarg):
    x = xarg
    y = yarg
    ymin = np.nanmin(y)
    ymax = np.nanmax(y)
    if ymin == ymax:
        return np.nanmean(x)
    cut = 0.5*(ymin+ymax)
    m = y >= cut
    return np.average( x[m], weights=abs(y[m] - cut))


def jpot(xarg, yarg):
    x,y = xarg, yarg
    ymin = np.nanmin(y)
    ymax = np.nanmax(y)
    if ymin == ymax:
        return np.nanmean(x)
    cut = 0.5*(ymin+ymax)
    m = y <= cut
    return np.average( x[m], weights=abs(cut - y[m]))




def itoslope():
    


    x1=jpot( SCANS[-1].get_data(pz), SCANS[-1].get_data("roi1_avg") ) 
    x2=jpot( SCANS[-1].get_data(pz), SCANS[-1].get_data("roi2_avg") ) 
    x3=jpot( SCANS[-1].get_data(pz), SCANS[-1].get_data("roi3_avg") )
    print(x1,x2,x3)
    p = np.polyfit( [+50/3,0,-50/3], [x1,x2,x3], 1 )
    print(p[0],"rad", np.degrees(p[0]),"deg")
    

 
