import numpy as np
import time

def findbeta(dty,w,h,buf):
    dmax = np.sqrt((h/2+buf)**2 + (w/2+buf)**2)
    dr = (np.abs(dty)-(w/2+buf)) / (dmax)
    return np.abs(np.degrees(np.arccos(dr)))
    
def getbetas(dtys,w,h,buf):
    b = np.ones(len(dtys))*90
    for i,dty in enumerate(dtys):
        if np.abs(dty)>((w/2)+buf):
            b[i] = findbeta(dty,w,h,buf)
    return np.nan_to_num(b, nan=90)


def recf2scan_constant_r_step():
    w = 170   #short dimension of sample
    l = 435  #long dimension of sample  #440
    buf = 0  #buffer around edge of sample
    theta = 103  #angle to put long axis along y 
    ystep = 0.8  #step size of dty
    rstep = 0.2  #step size of rot
    ctim = 0.1  #count time

    #shouldn't need to edit below here
    d = np.sqrt(w**2 + l**2)
    dtys = np.arange( -((d/2)+buf), (d/2)+buf+(ystep/2), ystep)
    bs = getbetas(dtys, w, l, buf)

    #do the scans
    for i, ypos in enumerate(dtys):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            hg173_myfscan(rot, theta-bs[i], rstep, (bs[i]*2)/rstep+1, ctim, scan_mode='CAMERA' ) 
        else:
            hg173_myfscan(rot, theta+bs[i], -rstep, (bs[i]*2)/rstep+1, ctim, scan_mode='CAMERA') 


def recf2scan_same_r_npts():
    w = 3   #short dimension of sample
    l = 14  #long dimension of sample
    buf = 0.3  #buffer around edge of sample
    theta = 8  #rot angle at which long axis is along y 
    ystep = 0.2  #step size of dty
    rnpts = 180  #step size of rot
    ctim = 0.1  #count time

    #shouldn't need to edit below here
    d = np.sqrt(w**2 + l**2)
    dtys = np.arange( -((d/2)+buf), (d/2)+buf+(ystep/2), ystep)
    bs = getbetas(dtys, w, buf)

    #do the scans
    for i, ypos in enumerate(dty):
        rstep = bs[i]*2/rnpts
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            hg173_myfscan(rot, theta-bs[i], rstep, rnpts, ctim ) 
        else:
            hg173_myfscan(rot, theta+bs[i], -rstep, rnpts, ctim )

                          
def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
    #pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico6']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico6']

def hg173_myfscan( *args, **kwds):
    for i in range(3):
        try:
            fscan(*args, **kwds)
        except Exception as e:
            elog_print("ERROR!!! Retrying a scan !!! "+str(e))
            continue
        break

    
def hg173_2d(y0=-1000, ye=1000, ys=20):
    fscan.pars.latency_time=0
    for i, ypos in enumerate(np.arange(y0,ye,ys)):
        umv(dty, ypos)
        pause_for_refill(60)
        if i%2 == 0:
            hg173_myfscan(rot, -1, 0.5, 182/0.5, 0.05, scan_mode='CAMERA' ) 
        else:
            hg173_myfscan(rot, 181, -0.5, 182/0.5, 0.05, scan_mode='CAMERA' )


            
