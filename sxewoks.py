def sxscanewoks(
    step_size=-0.5,
    count_time=1,
    start=0 + 35,
    end=0 - 35,
):
    from bliss.setup_globals import fscan
    from bliss.scanning import scan_meta
    from bliss.setup_globals import SCANS
    if step_size >= 0:
        print("WARNING! CrysAlis cannot handle positive steps!! Changing to negative")
        step_size = -step_size
    if abs(start) > 180:
        print("start out of range")
        return
    nsteps = round((end - start) / step_size)
    end = nsteps * step_size + start
    if abs(end) > 180:
        print("end out of range high")
        return
    speed = step_size / count_time
    if speed > 25:  #: rot.velocity:
        print("speed is too fast")
        return

    scan_meta_obj = scan_meta.get_user_scan_meta()
    scan_meta_obj.instrument.set(
        "crysalis",  # category
        {"crysalis": {"distance": crysalis_writer.scan_parameters.distance, 
                      "xcenter": crysalis_writer.user_parameters.beam[0], 
                      "ycenter": crysalis_writer.user_parameters.beam[1], 
                      "wavelength": crysalis_writer.user_parameters.wavelength
                      }
        },
    )
    
    omega_start = start
    npts = ((end - omega_start) / step_size)
    # The scan must be centered around zero
    center = step_size * npts / 2 + omega_start 
    omega_start_symetric = omega_start - center
    
    crysalis_writer.user_parameters.omega = f"-{omega_start_symetric}-index*{step_size}" #POA 5/11/2024 there should be - sign in front of omega

    # plotbeamcenter(xc, yc)
    fscan(rot, start, step_size, nsteps, count_time, scan_mode="CAMERA")
    crysalis_writer.run_conversion(SCANS[-1])
    
