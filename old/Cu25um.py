
import numpy as np
import time
user_script_load("nc_bliss")


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)
#    pause_for_flux(t)

def pause_for_flux(t, num=1e6):
    c = ct(1,p201_21).get_data()['fpico6']
    while c < num:
        print("Waiting for counts",num)
        time.sleep(t/3.)
        c = ct(1,p201_21).get_data()['fpico6']

class refscanner(object):
    def __init__(self, refpos): # refpos is a dict
        self.refpos = refpos
    def measure(self, mot, rng, npt=20, tim=0.05):
        if 'rot' in self.refpos:
            if user.piezo_scanmode():
                user.piezo_scanmode_off()
            umv(rot, self.refpos['rot'])
            user.piezo_scanmode_on()
        umv(px, self.refpos['px'],
            py, self.refpos['py'],
            pz, self.refpos['pz'])
        scan = dscan( mot, -rng, rng, npt, tim, save_images=False)
        c = stepfit( scan.get_data(mot.name), scan.get_data("Cu_det0") )
        self.refpos[mot.name]=c
        umv(mot,c)
        where()
        print("moved",mot.name,"to",c,"and did a where!")
        return c
    def __getitem__(self, m):
        return self.refpos[m]
    def __setitem__(self, m, p):
        self.refpos[m] = p
            

def stepfit( x, yraw):
    y = yraw - yraw.min()
    m = (y > y.max()*0.1) & (y < y.max()*0.6)
    p  =np.polyfit( y[m], x[m], 1)
    print(y[m])
    print(x[m])
    print(p)
    print("STEPFIT")
    return p[1]
        
def sample_ref_fluo():
    # TODO : put these scans in a different dataset for easier plotting?
    # Compared to cross/surface go to:
    # pz -30, px -2.5 py -2.5
    ## Failed on a bufer overrun ?
    pause_for_refill(66)
    ACTIVE_MG.enable("mca")
    ACTIVE_MG.disable("eiger")
    plotselect("mca:Cu_det0")
    umv(dty,0)
    # measure y with z above
    oz = refscanner( { 'px': 59.93, 'py': 50.47, 'pz':93.4, 'rot' : 0 } )
#     46.61150    51.52340    9.98961
    oz.measure( pz, 4, npt=80 )
    # now measure z at the correct y, 10 below
    oy = refscanner( oz.refpos.copy() )
    oy['pz']-=83.4
    oy['py']-=12.64
    oy['px']-=12.64
    oy.measure( py, 4, npt=80 )
    ox = refscanner( oy.refpos.copy() )
    ox['rot'] = 90.
    ox.measure( px, 4, npt=80 )
    user.piezo_scanmode_off()
    ACTIVE_MG.disable("mca*")
    umvr(py, 12.64, px, 12.64)
    
def difftyscan():
    newproposal("blc12352")
    newsample("Cu25umDT3") # restarts here ###
    # fpico6
    pico6.auto_range = False
    pico6.range=2e-4
    
    ACTIVE_MG.disable_all()
    ACTIVE_MG.enable("frelon4:roi*")
    ACTIVE_MG.enable("frelon4:image")
    ACTIVE_MG.enable("*fpico6")
    
    y0 = 0.0  # centre of rotation
    width = 15
    step  = 0.2
    # 260 scans !
    ypos = np.arange(-step, width+step/10, step)
    for z in range(3):
        newdataset("Z%03d"%(z))
        sample_ref_fluo()
        ACTIVE_MG.enable("frelon4:roi*")
        ACTIVE_MG.enable("frelon4:image")
        ct(0.01)
        plotselect("frelon4:roi1_avg")
        for my_y in ypos:
            umv(dty, my_y)
            pause_for_refill(30)
            finterlaced(rot,  0,  1.0, 360, 0.08, mode="FORWARD")
            umv(dty, my_y+step/2)
            pause_for_refill(30)
            finterlaced(rot,720, -1.0, 360, 0.08, mode="FORWARD")



def difftyscanEiger():
    # newproposal("blc12407")
    newsample("Cu25umDT3") # restarts here ###
    newdataset("520Hz")
    # fpico6
    pico6.auto_range = False
    pico6.range=2.1e-5
    
    y0 = 0.0  # centre of rotation
    width = 15
    step  = 0.2
    # 260 scans !
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.025
    rate = 520
    assert (astep * rate)<25 , 'too fast'
    eiger.camera.auto_summation='OFF'
    for my_y in ypos:
        umv(dty, my_y)
        pause_for_refill(18)
        fscan(rot, 0, astep, 180/astep, 1/rate)
        umv(dty, my_y+step/2)
        pause_for_refill(18)
        fscan(rot, 180, -astep, 180/astep, 1/rate)
    eiger.camera.auto_summation='ON'

def difftyscanEiger2():
#    newproposal("blc12381")
#    newsample("Cu25um") # restarts here ###
#    newdataset("Z2")
    # fpico6
    pico6.auto_range = False
    pico6.range=2e-6
    ACTIVE_MG.disable_all()
    ACTIVE_MG.enable("p201*")
    width = 13
    step  = 0.2
    y0 = 0.
    ypos = np.arange(y0-width, y0+width+step/10, step)
    ACTIVE_MG.enable("eiger*")
    eiger.camera.photon_energy=40500.
    ct(0.01)
    plotselect("eiger:roi1_avg")
    for my_y in ypos:
            umv(dty, my_y)
            fscan(rot, 0, 0.05, 7200, 0.002)
            umv(dty, my_y+step/2)
            fscan(rot, 360, -0.05, 7200, 0.002)
    newdataset("next")

def difftyscanEiger2bis():
#    newproposal("blc12381")
#    newsample("Cu25um") # restarts here ###
#    newdataset("Z2")
    # fpico6
    pico6.auto_range = False
    pico6.range=2e-6
    ACTIVE_MG.disable_all()
    ACTIVE_MG.enable("p201*")
    ACTIVE_MG.enable("eiger:*")
    ACTIVE_MG.disable("eiger:bpm*")
    width = 13
    step  = 0.2
    y0 = 0.
    ypos = np.arange(y0-width, y0+width+step/10, step)
    ypos = np.arange(-3.8, y0+width+step/10, step)
    #ACTIVE_MG.enable("eiger*")
    eiger.camera.photon_energy=40500.
    ct(0.01)
    plotselect("eiger:roi1_avg")
    for my_y in ypos:
            umv(dty, my_y)
            fscan(rot, 0, 0.05, 7200, 0.002)
            umv(dty, my_y+step/2)
            fscan(rot, 360, -0.05, 7200, 0.002)
    newdataset("next")


