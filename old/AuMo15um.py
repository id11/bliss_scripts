
import numpy as np
import time
user_script_load("nc_bliss")


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan(*a,**k):
    print(a,k)
    fscan(*a,**k)

def myumv(*a,**k):
    print(a,k)
    umv(*a,**k)
        
def difftyscanEiger():
    newproposal("blc12407")
    newsample("MoAu15umDT1") # restarts here ###
    newdataset("interY")
    
    # fpico6
    pico6.auto_range = False
    pico6.range=2.1e-5
    
    y0 = 0.0  # centre of rotation
    width = 10
    step  = 0.2
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.05
    anrange = 360.
    speed = 20.
    rate = speed / astep
    assert (astep * rate)<21 , 'too fast'
    subscans = 4
    timest = anrange/astep * 1/rate + 1
    print(timest)

    for strt in range(subscans):
        for my_y in ypos[strt::subscans]:
            myumv(dty, my_y)
            pause_for_refill(timest)
            myfscan(rot, 0, astep, anrange/astep, 1/rate)
            myumv(dty, my_y + step/2)
            pause_for_refill(timest)
            myfscan(rot, anrange, -astep, anrange/astep, 1/rate)
    eiger.camera.auto_summation="ON"




