



def icepos_log():
    logname = current_session.name + time.strftime("%Y%m%dT%Hh%M") 
    with open(logname,"r") as f:
        for ax in global_map.get_axes_iter():
            if hasattr(ax.controller, "read_encoder_all_types" ):
                msg = "%s %s %f %f\n"%(
                    ax.name,
                    ax.controller.read_encoder_all_types(ax),
                    ax.position,
                    ax.dial )
                f.write( msg )
                print(msg,end=" ")
            
