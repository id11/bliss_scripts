def calorimetry():
    fsh.open()
    print('Do fsh.close() at end')
    for i in range(100000):
        limatake(0.1, 999, save=True)

def calorimetryfast():
    fsh.open()
    print('Do fsh.close() at end')
    for i in range(100000):
        limatake(0.001, 999, save=True)

def calorimetryW():
    fsh.open()
    print('Do fsh.close() at end')
    for i in range(100000):
        limatake(0.01, 999, save=True)



def calorimetryundramp():
    umv(cpm18, 7.4, u22, 9.33)
    tfoh1.set(26, 'Be')
    sheh3.open()
    a2scan(u22,9.33,8.3306, cpm18, 7.4, 6.398, 120, 0.1, sleep_time=2)



def calorimetrylenseshg():
    umv(s6vg,1)
    umv(s6hg,-0.1)
    tfoh1.set(26, 'Be')
    print('opening s6hg scan')
    ascan(s6hg,-0.1,0.6,70,5)
    sleep(10)
    print('closing s6hg scan')
    #ascan(s6hg,0.6,-0.1,70,5)
    #sleep(10)
    umv(s6hg,1)
    tfoh1.set(0, 'Be')


def calorimetrylensesvg():
    umv(s6hg,1)
    umv(s6vg,-0.1)
    tfoh1.set(26, 'Be')
    print('opening s6hg scan')
    ascan(s6vg,-0.1,0.6,70,5)
    sleep(10)
    #print('closing s6hg scan')
    #ascan(s6hg,0.6,-0.1,70,5)
    #sleep(10)
    tfoh1.set(0, 'Be')




def moveto_frelon1():
    ACTIVE_MG.enable('frelon1:image')
    ACTIVE_MG.disable('frelon3*')
    umv(d1ty,13.75)
    print('Ready to use Frelon1')


def moveto_frelon3():
    ACTIVE_MG.enable('frelon3:image')
    ACTIVE_MG.disable('frelon1*')
    umv(d1ty,-50)
    print('Ready to use Frelon3')
