import time

def check_bigy(tol = 0.1):
    indexer_pos = bigy._do_read_hw_position()
    encoder_pos = bigy.dial
    delta = abs(indexer_pos-encoder_pos)
    if delta >= tol:
        raise RuntimeError(f"Discrepency between indexer position {indexer_pos} and dial position {encoder_pos}, delta={delta}")


def bigy_goto( pos, tol=0.0025, max_try=10 ):
    check_bigy()
    bigy.sync_hard()
    check_bigy()
    umv(bigy, pos)
    time.sleep(2)
    bigy.sync_hard()
    success = False
    for i in range(max_try):
        mv(bigy, pos)
        time.sleep(0.5)
        bigy.sync_hard()
        print(f"Try {i}, Made it to {bigy.position}")
        if abs(bigy.position - pos)<tol:
            success = True
            break
    if success:
        print(f"Made it to {bigy.position}")
    else:
        print(f"Didn't make it, final position {bigy.position}")
