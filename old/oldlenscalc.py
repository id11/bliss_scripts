import numpy as np
import xraylib

RHO = {
    "Al": 2.7,
    "Be": 1.85,
    "Si": 2.329,
}

XLENS = {  # mm
    "tfoh1": 31.5e3,
    "slit3": 37.6e3,
    "eh1sample": 42e3,
    "slit6": 49.1e3,  # pinhole
    "tfcrl": 94.0e3,
    "eh3sample": 95.8e3,
    "tfsil": 99.0e3,
}

RLENS = {  # mm
    "tfoh1": 0.2,
    "tfcrl": 0.2,
    "tfsil": 0.0125,
}

#
SI_NLENS = [12, 28, 52, 80, 116, 160, 208, 264, 324, 392]


def crl_focal_length(crl, energy,
                     radius=0.2):
    """
    crl = bliss crl object
    energy in keV
    """
    # Compute the focal length
    # energy is in keV
    # radius units and focal distance will match
    # uses densities 
    # f = R / 2 N delta
    inv_f = 0
    for material in crl.materials:
        assert material in RHO
        n = crl.get(material)
        delta = 1 - xraylib.Refractive_Index_Re(material, energy,
                                                RHO[material])
        print(material, n)
        inv_f += (2 * n * delta) / radius
    return 1 / inv_f


def delta_to_e(delta, material):
    """ Guess energy from delta 
    delta proportional to 1/(e*e)
    """
    e0 = 50.
    d0 = 1 - xraylib.Refractive_Index_Re(material, e0, RHO[material])
    c0 = d0 * e0 * e0
    e1 = np.sqrt(c0 / delta)
    d1 = 1 - xraylib.Refractive_Index_Re(material, e1, RHO[material])
    c0 = d1 * e1 * e1
    return np.sqrt(c0 / delta)


def pinholemonotable(slits='slit6'):
    """
    Table of the Be lenses <-> energy to focus on pinhole
    """
    s1 = XLENS["tfoh1"]
    print("Aiming at slits", slits)
    s2 = XLENS[slits] - s1
    f = 1 / (1. / s1 + 1. / s2)
    print("Focal length should be:", f)
    for n in range(1, 33):  # Be
        # n = pow(2,i)
        delta = RLENS["tfoh1"] / 2 / n / f
        print("%2d Be  %.2f keV" % (n, delta_to_e(delta, "Be")), end=" ")
        if n in [1, 2, 4, 8, 16, 32]:
            print("<---")
        else:
            print()


def _sitable(energy):
    """ Focal distances of the si crl on nscope """
    delta = 1 - xraylib.Refractive_Index_Re("Si", energy, RHO["Si"])
    return [RLENS['tfsil'] / (2 * n * delta) for n in SI_NLENS]


def sitable(energy):
    """ Print the focal distances vs energy """
    print("Energy", energy, "keV\nRow   N   X/mm")
    f = _sitable(energy)
    for i in range(len(f)):
        print("%2d %4d  %.5g" % (i + 1, SI_NLENS[i], f[i]))


def siguess(energy):
    """ Figure out the lenses needed for nscope """
    hlo, hhi = 70, 170  # shnee.limits
    vlo, vhi = 25, 75  # axmo.limits
    sitable(energy)
    print("Perhaps you can try:")
    f = _sitable(energy)
    s2 = [1 / (1.0 / x - 1.0 / XLENS['tfsil']) for x in f]
    for i in range(len(s2)):
        if hlo < s2[i] < hhi:
            out = "Horizontal %d shnee %.2f " % (i + 1, s2[i])
            lo, hi = vlo + s2[i], vhi + s2[i]
            for j in range(len(s2)):
                if lo < s2[j] < hi:
                    print(out, "\tVert axmo %d %.2f" % (j + 1, s2[j] - s2[i]))


def crlguess():
    """ Figure out the lenses options for the 3dxrd """
    cx = config.get("cx")
    lo, hi = cx.limits

    print("Help!")


# todo : bilens options ? effect of ivt to make a virtual source    


def crl_scan_fity(scn=None):
    # 663 px/mm for frelon1 vs s7vo
    #    scn = dscan(s7vo, -0.15, 0.15, 30, 0.1 )
    if scn is None:
        scn = SCANS[-1]

    x = scn.get_data('s7vo')
    y = scn.get_data('frelon1:bpm:y')
    w = scn.get_data('frelon1:bpm:intensity')
    p = np.polyfit(x, y, 1, w=w)
    c = np.polyval(p, x)

    # add to plot
    # scn.get_plot( ) ?
    # plotselect('frelon1:bpm:y','frelon1:bpm:intensity')
    # plot( x, c, "-" ) # as a fit line

    print("pixels/mm", p[0])
    ycen = np.average(y, weights=w)
    vary = np.average((y - ycen) ** 2, weights=w)
    sigy = np.sqrt(vary)
    print("variance", vary, "stddev", sigy, "fwhm", sigy * 2.355)
    print("umvr(cx,", p[0] * cx.position / (663 + p[0]), ")")

# siguess(43.44)
