import numpy as np


def friedelrot( ypin, zpin, distance_px = 149.788/0.075, cy=1111, cz=1050 ):
    """
    distance_px = distance in pixels
    cy, cz = beam center in y/z
    yp, zp = spot position in y/z
    returns the rot angle to the Fridel pair at (2cy-yp, 2cz-zp)
    """
    yp = np.asarray(ypin)
    zp = np.asarray(zpin)
    assert yp.shape == zp.shape
    shape = yp.shape
    vout = np.array( (np.full( len(yp.ravel(),), distance_px), yp.ravel() - cy, zp.ravel() - cz ) )
    n = vout/np.linalg.norm( vout, axis=0 ) - np.array((1,0,0))[:,np.newaxis]
    r = np.degrees(np.arctan2( -n[0], n[1] ) * 2).reshape(shape)
    r = np.where( r > 180, r-360, r)
    r = np.where( r < -180, r+360, r)
    if shape != ():
        return r
    return float(r)


# todo : check sign of rotation angle vs +/- y

# todo : roi definitions. Given cy, cz and one roi, add the other 3 and compute the angles

def friedel_rois( roiname,
                  xc=1050,
                  yc=1111,
                  d=149.788,
                  detector='eiger', motor='rot' ):
    """
    Give an roi counter that is on a diffraction spot

    xc = horizontal beam center
    yc = vertical beam center
    distance = sample to detector distance in mm

    """
    det = config.get( detector )
    ctr = det.roi_counters[roiname]
    px = xc - ctr.width//2
    py = yc - ctr.height//2
    distance_pixels = d * 1e-3 / np.mean( det.proxy.camera_pixelsize )
    # 180 degrees : x = x, y = 2*cy-py
    det.roi_counters.set( 'r180',  (        ctr.x, 2*py - ctr.y, ctr.width, ctr.height ) )
    det.roi_counters.set( 'fr0',   ( 2*px - ctr.x ,2*py - ctr.y, ctr.width, ctr.height ) )
    det.roi_counters.set( 'fr180', ( 2*px - ctr.x,        ctr.y, ctr.width, ctr.height ) )

    domega = friedelrot( ctr.x + ctr.width/2, ctr.y + ctr.height/2, distance_pixels, xc, yc)
    angles = [ rot.position,
               rot.position + domega,
               rot.position + 180,
               rot.position + domega + 180 ]
    counters = [ ctr.name, 'fr0' , 'r180', 'fr180' ]
    cnames = ['%s:roi_counters:%s_sum'%(detector,name) for name in counters]
    print("Expected angles and rois:")
    args = {'angles' : angles, 'ctr': cnames }
    for i in range(4):
        a = angles[i]
        c = counters[i]
        print(f'rot = {a} ctr = {c} {det.roi_counters[c]}')
    return args

# check the rot angle by scanning ? Not sure if it will be 'good enough'
    
    

