import time

#ftimescan(0.002, 10000) to init musst vars

def test_musst():
    m = musst_nscope
    m.putget("RUN FTIMESCAN")
    last_value = 0.
    last_vtime = None
    while m.putget("?STATE") != "IDLE":
        eptr_str = m.putget("?EPTR")
        value = int(eptr_str.split()[0])
        vtime = time.time()
        if value < last_value:
            print("ERROR", eptr_str, last_value)
            continue
        rate = 0.0
        if last_vtime is not None and value > 0:
            rate = (vtime - last_vtime) / ((value - last_value)/8.)
            if rate < 0.001:
                print("ERROR", eptr_str, last_value, rate)
                continue
        last_value = value
        last_vtime = vtime
        print(f"EPTR {value} {rate:.4f}", end="\r")
        sleep(0.01)
    print()

def test_musst_loop():
    for idx in range(500):
        print(f"LOOP #{idx}")
        test_musst()
       
