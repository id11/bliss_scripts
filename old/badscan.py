

# dont do this. why would you do this? this is horrible.

# Why? This scan was slow:
# a2scan(llrz1, 1, 4, llrz2, 1, 4, 60000, 0., llrz1_enc, llrz2_enc)
# ... so we see how fast we can go

from bliss.controllers.motors.icepap import _ackcommand
import numpy as np

def badmv( cnt, m1, s1, m2, s2):
    """ moves to motors to positions in steps 
    on the same icepap controller"""
    x = "#MOVE %d %d %d %d"%(m1,s1,m2,s2)
    _ackcommand( cnt, x)

MOVING = pow(2,10)
READY =  pow(2,9)

def badwait(cnt, stat):
    """ see if controller says it is ready to move """
    ret = _ackcommand( cnt, stat )
    r0, r1 = [int(v,16) for v in  ret.split()]
    if r0&r1&READY:
        return False
    else:
        return True

def badct(c1, a1, a2  ):
    """ read the step counter and encoder input """
    ret1 = _ackcommand( c1, "?FPOS %d %d"%(a1,a2) )
    ret2 = _ackcommand( c1, "?ENC ENCIN %d %d"%(a1,a2) )
    return ret1,ret2

def badscan(startpos, endpos, nsteps, poll_time=0.001, lineprint=100):
    """ run the scan """
    # you lost the motor positions, so
    llrz1.apply_config(reload=True)
    llrz2.apply_config(reload=True)
    sync()
    # go to start position
    umv( llrz1, startpos, llrz2, startpos)
    c1 =  llrz1.controller._cnx
    assert llrz1.controller is llrz2.controller
    # read current steps
    s1 = int(_ackcommand(c1, "?FPOS %d"%(llrz1.address)))
    s2 = int(_ackcommand(c1, "?FPOS %d"%(llrz2.address)))
    # calc end position
    e1 = s1 + int((endpos-startpos)*llrz1.steps_per_unit)
    e2 = s2 + int((endpos-startpos)*llrz2.steps_per_unit)
    # motor addresses
    a1 = llrz1.address
    a2 = llrz2.address
    # status command
    stat = "?FSTATUS %d %d"%(a1,a2)
    # datafile
    f = open( "enc.dat" , "a")
    f.write("\n\n")
    f.write("# %s\n"%(time.ctime()))
    NPT = nsteps
    pos1 = np.linspace(s1,e1,NPT,dtype=np.int)
    pos2 = np.linspace(s2,e2,NPT,dtype=np.int)
    # motor full steps are 3200 per turn
    steps = (pos1[1:]-pos1[:-1]).mean()
    if abs(steps) < 3200: # less than half a step
        # 10 Hz spin
        print("Less than half a step, skip acceleration" )
        llrz1.velocity = 3200 / abs(llrz1.steps_per_unit)
        llrz2.velocity = 3200 / abs(llrz1.steps_per_unit)
        llrz1.acctime = 0.001
        llrz2.acctime = 0.001        
    t0 = time.time()
    for i in range(NPT):
        badmv( c1, a1, pos1[i], a2, pos2[i] )
        nw = 0
        epoch = time.time() - t0
        while badwait( c1, stat ):
            time.sleep(poll_time)
            nw += 1
        r = badct( c1, a1, a2 )
        s = "%d %s %s %12.4f %d"%(i,r[0],r[1],epoch,nw)
        if i % lineprint == 0:
            print(s, "\r", end="")
        f.write(s)
        f.write("\n")
    f.close()
    print()
    # put back
    llrz1.apply_config(reload=True)
    llrz2.apply_config(reload=True)
    llrz1.sync_hard()
    llrz2.sync_hard()
    wm(llrz1,llrz2)
    
    
    
    
    
