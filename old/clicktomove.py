import numpy as np

def goto_click(npts=1):
    """go to position selected in flint plot
    npts = 1 or 2 or 3 or 4
    if nbpts>1, mean of points will be taken"""
    if npts == 1:
        goto_custom(clicked_pos)
    if npts == 2:
        goto_custom(clicked_pos_2)
    if npts == 3:
        goto_custom(clicked_pos_3)
    if npts == 4:
        goto_custom(clicked_pos_4)

def clicked_pos(x,y):
    c2mplot = flint().get_live_plot("default-curve")
    print("Select position in flint")
    pos = c2mplot.select_points(1)
    print('Trying to move to',pos[0][0])
    return pos[0][0]

def clicked_pos_2(x,y):
    c2mplot = flint().get_live_plot("default-curve")
    print("Select 2 positions in flint")
    pos = c2mplot.select_points(2) 
    posave = np.mean([pos[0][0],pos[1][0]])
    print('Trying to move to',posave)
    return posave

def clicked_pos_3(x,y):
    c2mplot = flint().get_live_plot("default-curve")
    print("Select 3 positions in flint")
    pos = c2mplot.select_points(3)
    posave = np.mean([pos[0][0],pos[1][0],pos[2][0]])
    print('Trying to move to',posave)
    return posave

def clicked_pos_4(x,y):
    c2mplot = flint().get_live_plot("default-curve")
    print("Select 4 positions in flint")
    pos = c2mplot.select_points(4)
    posave = nnp.mean([pos[0][0],pos[1][0],pos[2][0],pos[3][0]])
    print('Trying to move to',posave)
    return posave


    
def mesh_goto_click():
    """go to position selected in flint scatter plot"""
    goto_custom(mesh_clicked_pos)


def mesh_clicked_pos(x,y):
    mplot = flint().get_live_plot("scatter")
    print("Select position in flint")
    pos = mplot.select_points(1)
    print('Trying to move to', pos)
    return pos[0][0]
    
"""
def image_centre_click(detector, pixelsize, npts=1, horizontal_only=False):
    #   could get pixel size from objective estimator
    #   what about image flips??
    
    fim = flint().get_live_plot(image_detector=detector)
    print('Select point(s) to centre in flint %s image'%detector.fullname)
    cn = np.array(fim.select_points(npts))
    cy = np.mean(cn[:,0])
    cz = np.mean(cn[:,1])
    roi = detector.image.roi #are 2 and 3 roi size or limits?
    mid = [roi[2]-roi[0], roi[1]- roi[3]]
    if horizontal_only:
        umvr(samy,(mid[0]-cy)*pixelsize)
    else:
        umvr(samtz, (mid[1]-cz)*pixelsize, samy,(mid[0]-cy)*pixelsize)
"""
