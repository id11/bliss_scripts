
import numpy as np
import time
user_script_load("nc_bliss")


def pause_for_refill(t):
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan(*a,**k):
    try:
        print(a,k)
        fscan(*a,**k)
    except:
        print("Scan failed, sleep then retry")
        time.sleep(60)
        fscan(*a,**k)

def myfscan2d(*a,**k):
    try:
        print(a,k)
        fscan2d(*a,**k)
    except:
        print("Scan failed, sleep then retry")
        time.sleep(60)
        fscan2d(*a,**k)        
        

def myumv(*a,**k):
    print(a,k)
    umv(*a,**k)

# FIXME : record with the dty glitches. ... series of fscan2d with 1 forward + back ?
    
def difftyscanEiger():
  # newproposal("blc12454")
   # newsample("WAu5um") # restarts here ###
    #newdataset("DT3")
    
    # fpico6
#    pico6.auto_range = False
 #   pico6.range=2.1e-5
    
    y0 = 0.0  # centre of rotation
    width = 4
    step  = 0.1
    ypos = np.arange(-width, width+step/10, step)
    astep = 0.05
    anrange = 360.
    speed = 25.
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    print(timest)
    eiger.camera.auto_summation='OFF'
    for z in range(4):
      newdataset("H%d_"%(z))  
      for my_y in ypos:
        myumv(dty, my_y)
        pause_for_refill(timest)
        myfscan(rot, 0, astep, anrange/astep, 1/rate)
        myumv(dty, my_y + step/2)
        pause_for_refill(timest)
        myfscan(rot, anrange, -astep, anrange/astep, 1/rate)
    eiger.camera.auto_summation="ON"




