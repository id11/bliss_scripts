
import numpy as np
import time
user_script_load("align_cor.py") 


def pause_for_refill(t):
    return
    while not machinfo.check_for_refill(t):
        print("Pausing for a refill")
        time.sleep(t/3.)

def myfscan(*a,**k):
    print(a,k)
    try:
        fscan(*a,**k)
    except: # pretty hopeless but why not ? 
        fscan(*a,**k)

def myumv(*a,**k):
    print(a,k)
    umv(*a,**k)
        
def layer():
    y0 = 0.0  # centre of rotation
    width = 10.
    ystep  = 0.1
    ypos = np.arange(-width, width+ystep/10, ystep)
    speed = 15.
    astep = speed*0.002
    anrange = 180.
    eiger.camera.photon_energy=4000.
    #### 
    rate = speed / astep
    assert (astep * rate)<26 , 'too fast'
    timest = anrange/astep * 1/rate + 1
    ACTIVE_MG.disable("mca*")
    eiger.camera.auto_summation="OFF"
    ACTIVE_MG.enable("eiger:image")
    for j, my_y in enumerate(ypos):
        myumv(dty, my_y)
        pause_for_refill(timest)
        if (j % 2) == 0: # forwards
            myfscan(rot, 0, astep,
                    anrange/astep, 1/rate, scan_mode='CAMERA')
        else:          # and back again
            myfscan(rot, anrange, -astep,
                    anrange/astep, 1/rate, scan_mode='CAMERA')
    eiger.camera.auto_summation="ON"

def dovol():
    for z in np.arange(40,60,0.1):
        umv(pz, z)
        layer()
