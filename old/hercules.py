

def runf2d():
    fscan2d(rot,0,2,90,dty,-6,0.1,120,0.1)
    ACTIVE_MG.enable('mca')
    fscan2d(rot,0,5,18,dty,-6,0.5,24,0.5) 

def hercules_scan():
    fscan2d(rot,0,2,90, dty,-4, 0.1,80,0.05) 
    

def layerscan(y_start=-580, y_end=581, y_step=20, rstep=2, a_time=0.06):
    print(np.arange(y_start,y_end,y_step))
    difftystart = dty.position
    for i,d in enumerate(np.arange(y_start,y_end,y_step)):
        umv(dty,  d )
        if i%2 == 0:
            fscan(rot, 180, rstep, 182/rstep, a_time )
        else:
            fscan(rot, 362, -rstep, 182/rstep, a_time )    
    umv(dty,difftystart)  

       
def myfscan(*a,**k):
    try:
        fscan(*a,**k)
    except:
        print("Something has gone wrong!!! Retry scans!!")
        elog_print("Retry a scan !!!")
        print("Sleep a minute or two first")
        time.sleep(60)
        pause_for_refill(120)
        fscan(*a,**k) 
