import numpy as np
from bliss.common import plot as bl_plot
from scipy.optimize import curve_fit

# from blissoda.id11.xrpd_processor import XrpdProcessor as _XrpdProcessor
# xrpd_processor = _XrpdProcessor()


def plotimagecenter(detector=marana3):
    roi = detector.image.roi
    xc, yc = roi[2] / 2, roi[3] / 2
    p = flint().get_live_plot(image_detector=detector.fullname)
    p.remove_marker("%scenter" % detector.fullname)
    p.update_marker(
        "%scenter" % detector.fullname, (xc, yc), text="Image Center", editable=False
    )


def centeredroi(detector, width, height):
    fullx, fully = detector.image.fullsize
    xs, ys = (fullx - width) / 2, (fully - height) / 2
    detector.image.roi = [xs, ys, width, height]


def plotbeamcenter(xc=None, yc=None):
    if xc is None:
        xc, yc = tdxrd_estimate_centrepixels(ffdty1.position, ffdtz1.position)
    p = flint().get_live_plot(image_detector="frelon3")
    p.update_marker("frelon3bc", (xc, yc), "Beam Center")


def tdxrd_estimate_centrepixels(dy, dz):
    """Returns estimate of beam centre on eiger in pixels as [x,y]"""
    return [995 + (20 * dy), 1007 - (20 * dz)]


def plotqvals(energy, xc=None, yc=None, pixelsize=0.05):
    xl = [1, 1 / np.sqrt(2), 0, -1 / np.sqrt(2), -1, -1 / np.sqrt(2), 0, 1 / np.sqrt(2)]
    yl = [0, -1 / np.sqrt(2), -1, -1 / np.sqrt(2), 0, 1 / np.sqrt(2), 1, 1 / np.sqrt(2)]
    lam = 12.39847 / energy
    if xc == None:
        xc, yc = tdxrd_estimate_centrepixels(ffdty1.position, ffdtz1.position)
    plotbeamcenter(xc, yc)
    p = flint().get_live_plot(image_detector="frelon3")
    for q in np.arange(5, 35, 5):
        tth = 2 * np.arcsin(q * lam / (4 * np.pi))
        rp = ffdtx1.position * np.tan(tth) / pixelsize
        for i in range(8):
            xp, yp = rp * xl[i] + xc, rp * yl[i] + yc
            if xp > 0 and xp < 2048 and yp > 0 and yp < 2048:
                p.update_marker("frelon3q%i_%i" % (q, i), (xp, yp), "q=%i" % q)


def removeqvals():
    print("This does not work, probably needs a bliss update...")
    print("Use the remove all markers option in flint instead")
    p = flint().get_live_plot(image_detector="frelon3")
    for q in np.arange(5, 35, 5):
        for i in range(8):
            p.remove_marker("frelon3q%i_%i" % (q, i))


def estimateqval(energy, xc=None, yc=None, pixelsize=0.05):
    lam = 12.39847 / energy
    if xc == None:
        xc, yc = tdxrd_estimate_centrepixels(ffdty1.position, ffdtz1.position)
    p = flint().get_live_plot(image_detector="frelon3")
    pix = p.select_points(1)[0]
    xp, yp = pix[0] - xc, pix[1] - yc
    rp = np.sqrt(xp ** 2 + yp ** 2)
    tth = np.arctan(rp * pixelsize / ffdtx1.position)
    q = np.sin(tth / 2) * 4 * np.pi / lam
    print("Estimated q value of (%.2f,%.2f) is %.2f AA^-1" % (xp, yp, q))


def align_cor_tdxrd(rng, npts, ctim, ctr="roi1_std", sig_sign=1):
    """
    Mainly for ID11 nscope as motors hard wired
    Try to align the center of rotation by scanning
    py and px and 0,90,180,270 degrees and fitting a counter
       rng = scan range (um)
       npts = points per scan
       ctim = counting time
       ctr = thing to plot
       sig_sign = positive or negative peak?
    """
    cens = []
    umv(diffrz, 0)
    cens.append(dscan_fit(samty, rng, npts, ctim, ctr, sig_sign))
    umv(diffrz, 90)
    cens.append(dscan_fit(samtx, rng, npts, ctim, ctr, sig_sign))
    umv(diffrz, 180)
    cens.append(dscan_fit(samty, rng, npts, ctim, ctr, sig_sign))
    umv(diffrz, 270)
    cens.append(dscan_fit(samtx, rng, npts, ctim, ctr, sig_sign))
    samtyideal = (cens[0] + cens[2]) / 2
    # umv(py, pyideal)
    samtxideal = (cens[1] + cens[3]) / 2
    # umv(px, pxideal)
    difftyerr = (cens[0] - cens[2] + cens[1] - cens[3]) / 4
    print("er1, er2", (cens[0] - cens[2]) / 2, (cens[1] - cens[3]) / 2)
    # umvr(dty, dtyerr)
    print(cens)
    print("umv(samtx,%f, samty,%f)" % (samtxideal, samtyideal))
    print("umvr(difftyerr,%f)" % (difftyerr))
    return cens


def align_cor_tdxrd_interactive(
    rng,
    npts,
    ctim,
    ctr="frelon3:roi_counters:roi1_avg",
    angles=[0, 90, 180],
    domove=True,
    alignto=None,
):
    """
    Mainly for ID11 tdxrd as motors hard wired.
    Try to align the center of rotation by scanning samy
    and using user input of the sample centres or edges
       rng = scan range (um)
       npts = points per scan
       ctim = counting time
       ctr = thing to plot
       domove = move to calculated position at end
       alignto = edges (e) or centre (c)? will be prompted if not given
    """
    # collect data
    diffty_start = diffty.position
    xdata = {}
    ydata = {}
    f = flint()
    p = f.get_plot("curve", name="align COR interactive", unique_name="ACI")
    p.clear_data()
    # diffty_airpad.on()
    for j, k in enumerate(angles):
        umv(diffrz, k)
        diffty.reset_closed_loop()
        diffty.sync_hard()
        if j % 2 == 0:
            xdata["%f degrees" % k], ydata["%f degrees" % k] = fscan_interactive(
                diffty, rng, npts, ctim, ctr, start=diffty_start
            )
        else:
            xdata["%f degrees" % k], ydata["%f degrees" % k] = fscan_interactive(
                diffty, -rng, npts, ctim, ctr, start=diffty_start
            )
        p.add_curve(
            xdata["%f degrees" % k], ydata["%f degrees" % k], legend="%f degrees" % k
        )
    # diffty_airpad.off()
    cens, _ = interactive_cor_fit(xdata, ydata, angles, alignto, p)
    print("centres =", cens)
    # find centres
    fitpars, _ = curve_fit(sinfit, angles, cens, [45, 1, 14.6])
    yideal = fitpars[1] * np.sin(np.radians(fitpars[0]))
    xideal = fitpars[1] * np.cos(np.radians(fitpars[0]))
    difftyerr = fitpars[2]
    print("samty =", yideal, ", samtx =", xideal, ", difftyerr =", difftyerr)
    sync(diffty, samtx, samty, samtz)
    if domove:
        umvr(samty, yideal, samtx, xideal)
        umv(diffty, difftyerr)
    else:
        print("umvr(samty,", yideal, ", samtx,", xideal, ")")
        print("umv(diffty,", difftyerr, ")")


def fscan_interactive(mot, rng, npts, ctim, ctr, start=0):
    """ Uses fscan and returns data"""
    from bliss.setup_globals import fscan

    mot.sync_hard()
    # plotselect('eiger:roi_counters:'+ctr)
    # plotinit(ctr)
    fscan(mot, start - rng, rng * 2 / (npts - 1), npts, ctim)
    sleep(1)
    mot.sync_hard()
    A = fscan.get_data()
    m = A[ctr] < 1e9
    y = A[ctr][m]
    x = A[mot.name][m]
    return x, y


def sinfit(x, a, b, c):
    """
    a is angle of max displacement
    b is magnitude of displacement
    c is centre of rotation
    """
    return b * np.sin(np.radians(x + a)) + c


def interactive_cor_fit(xdata, ydata, angles, alignto, p):
    # choose centre or edge alignment
    if alignto == None:
        alignto = input("Align using the centre (c) or edges (e)?")
    if alignto in ["c", "centre", "center", "C"]:
        nbp = 1
    elif alignto in ["e", "edge", "edges", "E"]:
        nbp = 2
    else:
        print("I didn't understand the alignto input, defaulting to centre")
        nbp = 1
    # input centres or edges
    cs = []
    c = [
        "b",
        "r",
        "g",
        "m",
        "y",
        "k",
        "b",
        "r",
        "g",
        "m",
        "y",
        "k",
        "b",
        "r",
        "g",
        "m",
        "y",
        "k",
    ]  # plot in same colour
    for i, r in enumerate(angles):
        p.clear_data()
        p.add_curve(
            xdata["%f degrees" % r],
            ydata["%f degrees" % r],
            legend="%f degrees" % r,
            color=c[i],
        )
        print("Select position at ", r, " degrees")
        try:
            cs.append(p.select_points(nbp))
        except:
            print("Try again")
            cs.append(p.select_points(nbp))
    p.clear_data()
    # replot the data
    try:
        for i, r in enumerate(angles):
            p.add_curve(
                xdata["%f degrees" % r],
                ydata["%f degrees" % r],
                legend="%f degrees" % r,
                color=c[i],
            )
    except:
        print("replotting did not work")
    # return dty values
    if nbp == 1:
        return [cs[k][0][0] for k in range(len(angles))], 0
    if nbp == 2:
        edges = []
        for k in range(len(angles)):
            if (cs[k][0][0] / np.abs(cs[k][0][0])) < 0:
                a1 = angles[k] + 180 if angles[k] < 180 else angles[k] - 180
                edges.append([a1, cs[k][0][0]])
            else:
                edges.append([angles[k], cs[k][0][0]])
            if (cs[k][1][0] / np.abs(cs[k][1][0])) < 0:
                a1 = angles[k] + 180 if angles[k] < 180 else angles[k] - 180
                edges.append([a1, cs[k][1][0]])
            else:
                edges.append([angles[k], cs[k][1][0]])

        return [np.mean([cs[k][1][0], cs[k][0][0]]) for k in range(len(angles))], edges


def samr_cor(mot, start, stop, npts, ctim, ctr):
    """displays summed images of samrx, samry scan to find COR"""
    lastscan = ascan(mot, start, stop, npts, ctim, ctr)
    sleep(1)
    A = lastscan.get_data()
    y = A["image"]
    summed = np.mean(y, axis=0)
    f = flint()
    dataplot1 = f.get_plot("image", name="align samr 1")
    dataplot1.side_histogram_displayed = False
    dataplot1.set_data(summed)
    summedm = summed - y[int(npts / 2)]
    dataplot2 = f.get_plot("image", name="align samr 2")
    dataplot2.side_histogram_displayed = False
    dataplot2.set_data(summedm)
    
# add on Sep 8th, 2024
def slit(hor, ver, delta=0.1, ctim=0.05):
    umv(s7hg, hor, s8hg, hor+delta, s7vg, ver, s8vg, ver+delta)
    ct(ctim)
    
    
########################################################################### add on Oct 22, 2024 after installation of ffdoor on far-field arm by Haixing Fang
def open_ffdoor(scanbeamstop = True, detector_name = 'frelon3'):
    #check slits
    if (s7vg.position > 1.5) or (s7hg.position > 1.5):
        ok = input('S7 is large, do you have the right beamsize for ff detector?')
        if ok[0] not in 'yY':
            return
    #check eiger beamstop position
    ffbsest = estimate_ffbspos(ffdty.position, ffdtz1.position)
    print(ffbsest)
    
    if (np.abs(ffbsest[0]-ffbsy.position) > 0.5) or (np.abs(ffbsest[1]-ffbsz.position) > 0.5):
        print('Beamstop does not seem to be in the right position')
    
    # move attenuator in
    umv(atty, 0, attrz, -5)
    print('Attenuatator in oh2 has been moved in.')
    
    #check counts on pico4
    sheh3.open()
    pico4.insert()
    s = ct(1, pico4).get_data('keithley:pico4')[0]
    if s > 1e7:
        print('Counts are high, caution before opening ffdoor')
    elif s < 1e5:
        print('Counts are very low, is there beam?')
    else:
        print('Counts seem reasonable, OK to open ffdoor') 
    
    #check estimated beam position
    cps = estimate_ff_centrepixels(ffdty.position, ffdtz1.position, detector_name)
    print(f'Estimated beam position: {cps} pixels')
    if cps[0] < -5 or cps[0] > 2200:
        print('Beam may be beyond detector edge, safe ffdoor opening will not work')
        return
        
    #scan beamstop
    if scanbeamstop:
        safetoopen = check_ffbeamstop()   
        if not safetoopen:
            print('Beam stop not in place, will not open edoor')
            return
    #move and check edoor with counts on eiger
    else:
        print('Partially opening ffdoor and check the counts as a function of attenuator rotation angles ...')
        attrzs = [-5, -8, -15]
        print('Partially opening edoor...')
        for ff_pos in [-5,-10,-15,-20]:
            umv(ffdoor, ff_pos)
            for attrz_pos in attrzs:
                if detector_name == 'eiger':
                    ffsafe = move_att_and_check_ffdoor_for_eiger(attrz_pos, cps)
                elif detector_name == 'frelon3':
                    ffsafe = move_att_and_check_ffdoor_for_frelon3(attrz_pos, cps)
            if ffsafe:
                continue
            else:
                return

    print('ffdoor is safe to open!')
    pico4.remove()
    ffdiode_out()
    umv(ffdoor, -170)
    umv(atty, -10, attrz, 0)
    print('Attenuatator in oh2 has been moved out.')
    
    
def check_ffbeamstop(auto_validate = False):
    umv(ffdoor, 0)
    ffdiode_in()
    dscan(ffbsy,-5,5,100,0.1,pico5)
    # JPW: goto_cen() is crazy dangerous because bliss cannot reliably find a peak
    # goto_cen()
    cen_pos = cen()
    where()
    real_pos = ffbsy.position
    if abs(cen_pos - real_pos) > 0.5:
        # problem. The beamstop is not aligned. A human needs to fix that.
        # please do not change this to try guess how to align a beamstop.
        # unless you are paying for the new detector!
        print("Error? The beamstop is not aligned")
        auto_validate = False
    print(fwhm())
    if not auto_validate:
        ok = input('Can you see a 5 mm beamstop there? (y/n)')
        if ok[0] not in 'yY':
            return False
    # IF the width is >5.3 we assume the height is OK
    if 5.3-fwhm() > 0.1:
        # else scan height
        dscan(ffbsz,-5,5,100,0.1,pico5)
        # problem. The beamstop is not aligned. A human needs to fix that.
        # please do not change this to try guess how to align a beamstop.
        # unless you are paying for the new detector!
        # goto_cen()
        where()
        ok = input('Can you see a 5 mm beamstop there? (y/n)')
        if ok[0] not in 'yY':
            return False
        if abs(fwhm()-5.1) > 0.3:
            print('Poor beamstop alignment')
            return False
        else: 
            return True
    elif 5.3-fwhm() > 0.1:
            print('Poor beamstop alignment')
            return False
    else:
        return True


def ffdiode_in():
    umv(ffdiode, 0)

def ffdiode_out():
    umv(ffdiode, 27)

def close_ffdoor():
    umv(ffdoor,0)
    
    
def estimate_ff_centrepixels(ffdty, ffdtz1, detector_name):
    """Returns estimate of beam centre on ff detector in pixels as [x,y]"""
    if detector_name == 'frelon3':
        return [1/0.046*ffdty + 1024.0, 1/0.046*ffdtz1 + 1024.0]
    elif detector_name == 'eiger':
        return [1/0.075*ffdty + 1100.0, 1/0.075*ffdtz1 + 1052.0]
    else:
        print(f'{detector_name} is not identified!')
        return None
     
def estimate_ffbspos(ffdty, ffdtz1):
    """Returns estimate of ffbs position as [ffbsy, ffbsz]"""
    return [1.0*ffdty, 1.0*ffdtz1]
    
def move_att_and_check_ffdoor_for_eiger(attrz_pos, cps):
    """Moves attenuator and checks roi around centre pixels (cps)"""
    cps = [int(cps[0]), int(cps[1])]
    umv(atty, 0)
    umv(attrz, attrz_pos)
    s = loopscan(1, 0.1, eiger).get_data('eiger:image')[0][cps[0]-120:cps[0]+120,cps[1]-120:cps[1]+120]
    bsroi = np.mean(s[s<3e9])
    if bsroi > 10:
        print('High counts on eiger, closing ffdoor')
        umv(ffdoor,0)
        return False
    else:
        print('Mean counts around beamstop:',bsroi)
        return True
        

def move_att_and_check_ffdoor_for_frelon3(attrz_pos, cps):
    """Moves attenuator and checks roi around centre pixels (cps)"""
    cps = [int(cps[0]), int(cps[1])]
    umv(atty, 0)
    umv(attrz, attrz_pos)
    s = loopscan(1, 0.1, frelon3).get_data('frelon3:image')[0][cps[0]-120:cps[0]+120,cps[1]-120:cps[1]+120]
    bsroi = np.mean(s[s<7e5])
    if bsroi > 10:
        print('High counts on frelon3, closing ffdoor')
        umv(ffdoor,0)
        return False
    else:
        print('Mean counts around beamstop:',bsroi)
        return True
