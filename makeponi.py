
import os
from math import sqrt, sin, radians
from bliss.scanning import scan_meta
import os
from pathlib import Path


def wvln():
    return 2*(5.34094/sqrt(3))*sin(radians(config.get('llbragg1').position))


def makeponi(calibrant = "CeO2"):
    """ Assume you just collected the data picture """
    s = SCANS[-1]

    cmd1 = "cd /data/id11/nanoscope/ponifiles"
    cmd2 = "pyFAI-calib2 %s::%d.1/instrument/eiger "%(
        s.scan_info['filename'], s.scan_info['scan_nb'] ) + \
          "-m /data/id11/nanoscope/Eiger/20200925.msk " + \
          "-c %s "%(calibrant)  + \
          "-D Eiger2_4M " + \
          "-w %f "%(wvln()) + \
          "-l %f "%(frelx.position)  + \
          "--poni1=%f "%( 1e-3*(detz.position + 75 )) + \
          "--poni2=%f "%( 1e-3*(311.5 + det2y.position ))
    # spatial ?
    uname = input("Enter your username for ssh")
    cmd = 'ssh -X %s@crunch "%s && %s"'%(uname, cmd1, cmd2)
    print(cmd)
    os.system("%s "%(cmd))
    loadponi( cmd = cmd  )


def loadponi(pname = None, cmd=""):
    if pname is None:
        pname = sorted(
        Path("/data/id11/nanoscope/ponifiles"
         ).iterdir(), key=os.path.getmtime)[-1]
    print(pname)
    if input("OK ?")[0] not in 'yY':
        print("So tell me which one to use please")
        return
    poni = open(pname,"r").read()
    scan_meta_obj = scan_meta.get_user_scan_meta()
    scan_meta_obj.instrument.set("poni",  # category
                                 { "poni" :
                                   {   "poni": poni,
                                       "filename" : str(pname) ,
                                       "cmd": cmd } } )
    print(poni)

    
