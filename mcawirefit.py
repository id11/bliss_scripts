
"""
Functions for fitting the fluo signal from wires and coated wires
"""

import numpy as np
from scipy.optimize import curve_fit

def wireatten(x, mu, r, a, xoff):
    """
    function describing attenuated signal 
    from fluorescence from a wire
    x is position
    mu is absorbtion of material
    r is radius of wire
    a is amplitude of signal
    xoff is centre of wire position
    """
    dx = x-xoff
    p = np.sqrt(np.where( abs(dx) > r, 0., r**2-dx**2 ))
    z = np.exp(mu*(dx-r))*p*(1 + p**2*mu/(6*r))
    return a*z 

def gaussian( x, position, fwhm ):
    """"gaussian function"""
    sigma2 = (0.5/np.sqrt(2.0*np.log(2.0)) * fwhm)**2
    xmp = x - position
    tmp = 0.5 * xmp * xmp / sigma2
    peak = np.exp( -tmp )
    return peak / peak.sum()

def wiregaussconv(x, mu, xoff, r, a, position, fwhm, bkg ):
    """convolution of gaussian beam and wireatten"""
    return np.convolve(wireatten(x, mu, r, a, xoff),gaussian(x, position, fwhm),'same') + bkg

def hollowwire(x, mu, r1, r2, a, xoff):
    dx = x-xoff
    p1 = np.sqrt(np.where( abs(dx) > r1, 0., r1**2-dx**2 ))
    z1 = np.exp(mu*(dx-r1))*p1*(1 + p1**2*mu/(6*r1))
    p2 = np.sqrt(np.where( abs(dx) > r2, 0., r2**2-dx**2 ))
    z2 = np.exp(mu*(dx-r2))*p2*(1 + p2**2*mu/(6*r2))
    return a*(z1 - z2)

def coatedwire(x, mu1, r1, r2, a, xoff, less):
    """
    function describing attenuated signal 
    from fluorescence from a coated wire
    x is position
    mu is absorbtion of material
    r1 is radius of wire
    r2 is radius of wire core (no coating)
    a is amplitude of signal
    xoff is centre of wire position
    less relates to the signal from the wire core
    """
    dx = x-xoff
    p1 = np.sqrt(np.where( abs(dx) > r1, 0., r1**2-dx**2 ))
    z1 = np.exp(mu1*(dx-r1))*p1*(1 + p1**2*mu1/(6*r1))
    p2 = np.sqrt(np.where( abs(dx) > r2, 0., r2**2-dx**2 ))
    z2 = less*np.exp(mu1*(dx-r2))*p2*(1 + p2**2*mu1/(6*r2))
    return a*(z1 - z2)

def coatedwireconv(x, mu1, xoff, r1, r2, a, less, position, fwhm, bkg ):
    """convolution of gaussian beam and coatedwire"""
    return np.convolve(coatedwire(x, mu1, r1, r2, a, xoff, less),gaussian(x, position, fwhm),'same') + bkg


def mca_wire_dscan_fit( mot, rng, npts, ctim, ctr, coated, mu, r, sig_sign=1 ):
    """
    dscans motor and fits signal with either
    coatedwireconv if coated = True, or
    wiregaussconv if coated = False
    """
    mot.sync_hard()
    plotselect(ctr)
    lastscan = dscan( mot, -rng, rng, npts, ctim)
    sleep(1)
    mot.sync_hard()
    A = lastscan.get_data( )
    y = sig_sign*A[ctr]
    x = A[mot.name]
    mid = x[np.argmax(y)]
    a = np.max(y)
    fitplot = bl_plot.plot_curve(name='wire attenuation fit')
    fitplot.plot({'x': x, 'data': y}, symbol='o', linestyle='')
    if coated:
        fitpars, fitcovs = curve_fit(coatedwireconv, x, y, ( mu, mid, r, r-0.1, a, 0.9, mid, 0.1, 1.0 ))
        fitplot.plot({'x': x, 'fit': coatedwireconv(x, *fitpars})
        print("cen: %.3f, mu: %.3f, radius1: %.3f, radius2: %.3f, beamfwhm: %.3f, bkg: %.3f"%(fitpars[1],fitpars[0]*10,fitpars[2],fitpars[3],fitpars[7],fitpars[8]))
    else:
        fitpars, fitcovs = curve_fit(wiregaussconv, x, y, ( mu, mid, r, a, mid, 0.05, 1.0 ))
        print("cen: %.3f, mu: %.3f, radius: %.3f, beamfwhm: %.3f, bkg: %.3f"%(fitpars[1],fitpars[0],fitpars[2],fitpars[5],fitpars[6]))
        fitplot.plot({'x': x, 'fit': wiregaussconv(x, *fitpars})
    return fitpars[1]


def mca_wire_align_cor(rng, npts, ctim, mu, radius, ctr='Cu_det0', sig_sign=1, coated=False):
    """
    Mainly for ID11 nscope as motors hard wired
    Try to align the center of rotation by scanning
    py and px and 0,90,180,270 degrees and fitting a counter
       rng = scan range (um)
       npts = points per scan
       ctim = counting time
       ctr = thing to plot
       sig_sign = positive or negative peak?
    """
    cens = []
    umv(rot, 0)
    cens.append(mca_wire_dscan_fit( py, rng, npts, ctim, ctr, coated, mu, radius, sig_sign ))
    py.sync_hard()
    px.sync_hard()
    umv(rot, 85)
    cens.append(mca_wire_dscan_fit( px, rng, npts, ctim, ctr, coated, mu, radius, sig_sign ))
    py.sync_hard()
    px.sync_hard()
    umv(rot, 180)
    cens.append(mca_wire_dscan_fit( py, rng, npts, ctim, ctr, coated, -mu, radius, sig_sign ))
    py.sync_hard()
    px.sync_hard()
    umv(rot, 265)
    cens.append(mca_wire_dscan_fit( px, rng, npts, ctim, ctr, coated, -mu, radius, sig_sign ))
    py.sync_hard()
    px.sync_hard()
    pyideal = (cens[0]+cens[2])/2
    #umv(py, pyideal)
    pxideal = (cens[1]+cens[3])/2
    #umv(px, pxideal)
    dtyerr =  (cens[0]-cens[2] + cens[1]-cens[3])/4
    print ('er1, er2',(cens[0]-cens[2])/2, (cens[1]-cens[3])/2)
    #umvr(dty, dtyerr)
    print(cens)
    print('pxideal, pyideal, dtyerr',pxideal,pyideal,dtyerr)
    return cens



def mca_wire_align_cor_dty(rng, npts, ctim, mu, radius, ctr='Cu_det0', sig_sign=1, coated=False):
    """
    Mainly for ID11 nscope as motors hard wired
    Try to align the center of rotation by scanning
    py and px and 0,90,180,270 degrees and fitting a counter
       rng = scan range (um)
       npts = points per scan
       ctim = counting time
       ctr = thing to plot
       sig_sign = positive or negative peak?
    """
    cens = []
    plotinit(ctr)
    umv(rot, 0)
    cens.append(mca_wire_dscan_fit( dty, rng, npts, ctim, ctr, coated, mu, radius, sig_sign ))
    umv(rot, 85)
    cens.append(mca_wire_dscan_fit( dty, rng, npts, ctim, ctr, coated, mu, radius, sig_sign ))
    umv(rot, 180)
    cens.append(mca_wire_dscan_fit( dty, rng, npts, ctim, ctr, coated, -mu, radius, sig_sign ))
    umv(rot, 265)
    cens.append(mca_wire_dscan_fit( dty, rng, npts, ctim, ctr, coated, -mu, radius, sig_sign ))
    dtyideal = numpy.mean(cens)
    pyerr = (cens[0]-cens[2])/2
    #umvr(py, pyerr)
    pxerr = (cens[1]-cens[3])/2
    print(cens)
    print('pxideal, pyideal, dtyerr',pxerr,pyerr,dtyideal)
    umvr(py, pyerr)
    umvr(px, pxerr)
    umv(dty, dtyideal)
    return cens



